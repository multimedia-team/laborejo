#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import sys, argparse
import laborejocore as api

parser = argparse.ArgumentParser(description="Updates the savefile version. Open an old lbjs file, force it to load, save and quit.")
parser.add_argument('infiles', help="One or more lbjs or lbj files to load.", nargs="*")

args = parser.parse_args()

#load command line files. A new empty file can also be opened, does not conflict with loading files.
if not args.infiles:
    print ("Give at least one .lbjs file")
    sys.exit(0)

for fileName in sorted(args.infiles):
    print ("Convert:" + fileName)
    ret = api.load(fileName, force = True)
    if ret:
        if not "tacet" in api._getMovement().defaultPerformanceSignature.dynamics:
            api._getMovement().defaultPerformanceSignature.dynamics["tacet"] = 0

        for track in api._getMovement():
            if type(track.smfChannel) is int:
                track.smfChannel = [track.smfChannel,track.smfChannel]

        for cursor in api._getMovement().cursors:
            #Old Performance Signatures had no "tacet". Add here:
            for obj in cursor.gen_right():
                if type(obj) is api.items.PerformanceSignature:
                    if not "tacet" in obj.dynamics:
                        obj.dynamics["tacet"] = 0

        api.save(fileName)
        api.close()

