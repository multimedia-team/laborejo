#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from laborejocore import * #This starts the backend and automatically creates a session. But not score/tracks.
new() #Adds an empty, unnamed score with an empty track which has a cursor at the position 1420 (c')
up() #up one step (d')
insert4() #insert a quarter note
insert2() #half note
up()
insert8()
up()
insert8()
up()
insert1() #barlines are added automatically

trackAdd() #new track. Switches the cursor as well. The pitch position remains the same
insertRest4()
insert2()
dotAdd() #adds the dot to the current note or the previous one if there is no current item, which is the case here.
insertRestM(measures=1)



trackUp() #we can go back. The cursor stays on the current position, which happens to be the same as after the last note.
tail() #we go to the end anyway, just to be sure and to be prepared for later edits of this script.

#Now lets enter notes in a different way

insertLySequence("c'4 d'8 e'8 f'2 c''1")

trackDown()
insertLySequence("g'4 a' b'2 c'1")


previewPDF(pdfviewer="xpdf") #opens the PDF viewer with a binary or xdg-open (no parameter) and shows the current lilypond output in a PDF
save("/tmp/laborejoExample.lbjs")

