Laborejo Manual
================
:encoding: utf-8
:author: Nils Gey info@laborejo.org
:doctype: book
:toc:
:icons:
:data-uri:
:lang: en

= Configuration and Keybindings

The Laborejo core (Python programs which import the Laborejo module) has no configuration and no saved state. Everything is set during runtime and can be changed during runtime.
However, the Qt GUI masks this behaviour and creates a saveable and configurable state for its own functions and for common core settings.

Since using the Qt GUI is the most common case the rest of this section concentrates on configuring the GUI.

== Config Files and Directory
Laborejo is configured through text files.

=== ~/.laborejo/
The hidden config directory is automatically created under ~ (the home directory) as ~/.laborejo when the program gets started the first time.
If you want to reset Laborejo to the "factory defaults" just delete that directory.

You can also use a temporary and alternative config directory by using the --config commandline option to a directory path of your choice.
If this directory does not exists it will be created with a short README file, just like the default directory.

Example:
--------------------------------------
laborejo-qt --config /home/user/laborejo-extra
--------------------------------------

There is also a Qt config directory in ~/.config/laborejo . You can safely ignore and delete it. It stores trivial GUI settings like the last window geometry, a list of recent files or the last content of the Laborejo mini terminal. This was the first and last mention of this directory in this document. You don't need it.

[[configTemplateDir]]
=== ~/.laborejo/templates/
By default this is empty except a README with minimal instructions. You can place <<exportTemplates, sexport templates>> here.

[[configScriptDir]]
=== ~/.laborejo/scripts/
Files with a .py extension will be indexed during program start and represented in the User menu.
See <<scripting, Scripting>>

You can also create shortcuts and toolbar entries for those in the <<configConfigFile, config file>>.
The action names, which you need for shortcuts and toolbar buttons, for the automatically created commands are prefixed wth "user-" and have no ".py" exention. Case of the filenames remains.

Example:
--------------------------------------
fakeAltoClef.py becomes user_fakeAltoClef
--------------------------------------

The scripts itself can be changed during runtime. They get parsed on every execution.

[[configConfigFile]]
=== ~/.laborejo/laborejo-qt.config
The main config file is <Laborejo-systemdir>/laborejoqt/config.py
It is read and executed during program start. Directly after that ~/.laborejo/laborejo-qt.config will be read and executed, overriding any default values.

That means you don't need to maintain a complete config file but you can do incremental updates in your user config file.

The config file is not written by Laborejo. Any changes you make here are permanent. You can safely explore all options in Laborejo and switch things on and off. It resets to your config once you restart the program.

If you want to know in detail what each setting does please read config.py directly, it is commented and has alternate example values.

==== Variable Settings
It consists of several pairs variableName = value. Everything behind a "#" is a comment and has only information value.
Each line holds one setting.

Example:
--------------------------------------
LYRICS = False #Show the Lyrics in the GUI. Trivial setting, can be toggled at any time in the program itself.
--------------------------------------

The value after the = is either a number, a "string in double quotes" or on of the two keywords True or False (case sensitive).

In the example above you could write LYRICS = True to enable the Lyric View on program start.

[[configDictionaries]]
==== Dictionary Settings
When Laborejo reads your personal config file there are already some so called "dictionaries" in place, created in config.py. They are used to bind groups of variables to values.
You could delete and rewrite a whole dictionary by simply overriding it with an empty dictionary {}.
--------------------------------------
shortcuts = {}
--------------------------------------

However, a better choice is to only overwrite certain values by creating a line like this in your ~/.laborejo/laborejo-qt.config
--------------------------------------
shortcuts["Tab"] = "actionFermata"
--------------------------------------

The line above deletes the old keybinding for the "Tab" key (add a dot to a chord) and binds the action to add a Fermata to this key. More about keybindings in the next sub-chapter.

The available dictionaries, and what they do can be seen in the commented laborejoqt/config.py

==== Keybindings
As explained in <<configDictionaries, Dictionary Settings>> all shortcuts are in in the "shortcuts" dict.

You can add or change one by using the following syntax:
--------------------------------------
shorcuts["KeyAsString"] = "actionOrFunctionAsString"
--------------------------------------

So, what does that mean?


=== Key-Strings

"KeyAsString" is the keyname. It has the form "Optional Modifier + 2nd opt. mod. + Keyname".
Modifiers are: Ctrl, Shift, Alt.  They can be combined as well:
--------------------------------------
"Ctrl+Shift+P"
--------------------------------------
It is possible to use the numpad as modifier but that requires different keynames, the Qt ones. As in: "QtCore.Qt.KeypadModifier+QtCore.Qt.Key_1". Please consult the Qt docs or read the laborejo config.py

Real key sequences, like the program emacs does, are possible as well, if you comma-separate them:
--------------------------------------
"Ctrl+A, R"
--------------------------------------
The keystring above would require the user to first press Ctrl+A, release the keys, and then press R.
However: These kind of shortcuts are mostly unknown and should not be used in a publically available function since it will confuse the users.

Some keys are not available in some languages and keyboard layouts. For example the "=" equal key. In English qwerty layout it is a simple key, but in German qwertz layout it is "Shift+0". Therefore "Shift+=" can not be used for some languages and users.


=== Action or Function for Shortcuts
The action string represents one of three types:

A menu action. You can find out about the menu actions by looking at the file laborejoqt/dictionaries.py in the dictionary "menus" or by reading the existing keybindings in config.py. However, not all actions have shortcuts, so you won't find a comprehensive list there.
Examples: actionFermata, actionZoom_In
There are actions which do not have the "action..." format, but they are just the same. Like: "modalcommands.commands.keys[1]"

User actions with a user_ prefix.
They are created from all files in ~/.laborejo/scripts/*.py.
Examples: the files "myScript.py" and "insertSecretHarmony.py" result in: user_myScript and user_insertSecretHarmony

Direct Laborejo functions.
you can use the same commands as you would do in Laborejo script Python files.  Prefix them with "api." but don't use () parenthesis
Examples: "api.insert4". See <<scripting, Scripting>> for more options.
However, most of the time these are not necessary since most functions are mapped to actions already and if not it is likely that you want to customize things, then you should go the script.py file route.

== An example custom config file
Some keybindings, some toolbars, a pdf viewer.


[[exportTemplates]]
= Templates
Most important are the Lilypond templates which will be used to create PDFs. There are several default lilypond templates in the <Laborejo-systemdir>/laborejocore/templates.
You can copy (and optionaly rename) these default .ly files to <<configTemplateDir, ~/.laborejo/templates/>>.

Basically these are Lilypond files with special Laborejo markers like:
--------------------------------------
%$$FILENAME$$
--------------------------------------
where generated music and content will be inserted.
These markers are always the first non-whitespace on a line so they will get ignored by Lilypond, even if they don't get exported. % is a Lilypond comment.

You can change, copy and rename those templates during run time. They are looked up before every export command.

There are three places where Laborejo looks for templates during export. From top to least priority:

* In the same directory as the .lbj or .lbjs file
* In ~/.laborejo/templates/
* In the <Laborejo-systemdir>/laborejocore/templates

This gives you the options to write custom templates and save or distribute them with your .lbj or .lbjs files. Or you can override a default template (basic.ly) by creating your own with the same name and place it in ~/.laborejo/templates/ which changes the look of all Laborejo scores which use the default basic.ly template.

== The Markers

explain all markers

== Laborejo Includes
And the difference to lilypond includes


[[scripting]]
= Scripting
How to create scripts.
What are scopes.
What is the start marker, so I can use scripts when no file is open.
