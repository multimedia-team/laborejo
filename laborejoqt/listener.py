# -*- coding: utf-8 -*-
import operator
import laborejocore as api
from laborejoqt import items, dictionaries
from laborejoqt.dynformcreator import fedit #Generate dialogs/formulars with a simple syntax.
from laborejoqt.constants import *
main = None   #will be filled in by the qt starting process.


class Listener(object):
    def __init__(self):
        pass

    def _savedFalse(self):
        """If the data has changed.
        This is not a backend listener, it is a custom gui one.
        The whole savestatus is GUI only"""
        #import traceback
        #print (traceback.extract_stack(limit=2)[-2][2])
        main.session.setSavestatusDirty()


    def _allItems(self, backendItem, methodAsString):
        """Update all items"""
        for i in main.session.currentGuiWorkspace.allItems[backendItem]:
            getattr(i, methodAsString)()
        self._savedFalse()

    def updateViewport(self):
        #TODO: A temp function that will go away once there is a better midi module in the backend.
        main.session.currentGuiWorkspace.workingAreaWidget.viewport().update()

    #def _updateStatusbar(self):
    #    main.session.currentGuiWorkspace.updateStatusBar()

class ListenerGlobal(Listener):
    def __init__(self):
        super(ListenerGlobal, self).__init__()

    def jackMidiInToggleMute(self, onOrOff):
        main.ui.actionMIDI_Note_Entry.setChecked(onOrOff)
        if onOrOff:
            main.prevailingMode()
        else:
            main.defaultModalCommands()

    def workspaceChanged(self, newActiveWorkspace):
        """Send the active workspace or None if no file is open.
        Convert to current GuiScore/TabPosition index and
        change the active Tab
        On the other hand, there is no way to change the workspace
        except through by the tabs."""
        #if newActiveWorkspace: #not none
            #print ("new active WS is", newActiveWorkspace, main.session.scenes[newActiveWorkspace])
            #(main.ui.Tab.currentWidget())
        pass

    def new(self, backendWorkspace):
        main.new(backendWorkspace)

    def cleanAgain(self):
        """The undo system reached a point where it got clean again"""
        main.session.setSavestatusClean()


class ListenerCollections(Listener):
    def __init__(self):
        super(ListenerCollections, self).__init__()

    def load(self, loadedCollection):
        pass

class ListenerRecord(Listener):
    """Midi in"""
    def __init__(self):
        super(ListenerRecord, self).__init__()

    def midiIn(self, pitch):
        """When midi in happens this can be queried by the gui to e.g.
        highlight a virtual keyboard or a simple LED graphic for midi-in"""
        pass

class ListenerCursor(Listener):
    def __init__(self):
        super(ListenerCursor, self).__init__()
        self.prevailingActions = {
                                    1536:main.ui.actionToolbarWhole,
                                    768:main.ui.actionToolbarHalf,
                                    384:main.ui.actionToolbarQuarter,
                                    192:main.ui.actionToolbarEighth,
                                    96:main.ui.actionToolbarSixteenth,
                                  }

    def setPitch(self, value = None):
        """Move the cursor that shows the current pitch, Y direction.
        If called without arguments the cursor will update itself.
        Use without argument after a track change for clef updates."""
        #TODO: The place to decide how pitch is diplayed on the current track is here. Normal staffs go up and down, tab staffs change a number or a string etc. All we get is a pitch number? Maybe we need something like a clef for tabs. Clef then is a string. Or is it a tuplet. String-Num and Pitch? This is shit, why are tabs so different? Later..
        if value:
            main.session.currentGuiWorkspace.cursorPitch.pitch = value
        main.session.currentGuiWorkspace.cursorPitch.update()

    def setPosition(self, backendWorkspace, trackIndex, horizontalIndex):
        """Set the position cursor.
        Horzizontal index is the backend flat cursor index.
        The unique position regardless ticks"""
        ws = main.session.scenes[backendWorkspace]
        ws.currentTrack.goToIndex(horizontalIndex)
        ws.cursorPosition.update() #calls cursor pitch update

    def prevailingDuration(self):
        """Signal that the cursor prevailing duration has changed"""
        cursor = api._getActiveCursor()
        if main.mode == "Prevailing":
            for dur, act in self.prevailingActions.items():
                act.setChecked(False)
            a = self.prevailingActions.get(cursor.prevailingDuration, None)
            if a:
                a.setChecked(True)
        main.ui.actionToggle_Prevailing_Dot.setChecked(cursor.prevailingDot)
        main.ui.actionToolbarNext_Becomes_Dotted.setChecked(cursor.oneTimePrevailingDot)

    def pitchIndex(self):
        """Signal that the cursor pitchindex has changed"""
        pass

class ListenerItem(Listener):
    """All track items including chords.
    Space of the item is derived from its duration.
    For chords leave the text blank."""
    def __init__(self):
        super(ListenerItem, self).__init__()

    def _insertProto(self, guiItem, backendItem, getContainer, standaloneInsert):
        """Inserting and Deleting in containers needs special care.
        Since we must insert/delete the gui items in every container
        instance without moving anything in the backend until it is
        finished and we are in sync with the backend again."""
        #contLen = len(getContainer) <= 3 #the first item was already inserted in the backend.
        #if getContainer and (len(getContainer) <= 3 or (getContainer[0] in main.session.currentGuiWorkspace.allItems and len(main.session.currentGuiWorkspace.allItems[getContainer[0]]) > 1)):  #we are in a container and the container is present more than once. OR the container is empty.
        if getContainer:  #we are in a container
            #main.session.currentGuiWorkspace.insertItemRelativeToContainerStart(guiItem, container = getContainer, howManySteps = api._getIndexFromContainerStart(), standaloneInsert = standaloneInsert)
            main.session.currentGuiWorkspace.insertItemRelativeToContainerStart(backendItem, container = getContainer, howManySteps = api._getIndexFromContainerStart(), standaloneInsert = standaloneInsert)
        else:  #contrary to popular belief we don't need any checking on file load. Load forces getContainer to None because on load everything is, from the GUI perspective, just item after item. There is no need to do the nonlinear update stuff.
            main.session.currentGuiWorkspace.currentTrack.insert(guiItem, standaloneInsert)
            main.session.currentGuiWorkspace.allItems.setdefault(backendItem, []).append(guiItem)
            main.session.currentGuiWorkspace.cursorPosition.update()
        self._savedFalse()


    def _convertBackendItemToGuiItem(self, item, itemType, forceSigs = None):
        """Take a backend item, return a gui item"""
        #Let the typechecking begin!  isinstance covers inheritance, type() not
        d = {api.items.Chord: items.GuiChord,
            api.items.Rest: items.GuiRest,
            api.items.PedalSustainChange: items.GuiPedalSustainChange,
            api.items.KeySignature: items.GuiKeySignature,
            api.items.TimeSignature: items.GuiTimeSignature,
            api.items.MultiMeasureRest: items.GuiMultiMeasureRest,
            api.items.Placeholder: items.GuiPlaceholder,
            api.items.DynamicSignature: items.GuiDynamicSignature,
            api.items.SubitoDynamicSignature: items.GuiDynamicSignature,
            api.items.WaitForChord: items.GuiWaitForChord,
            api.items.PerformanceSignature: items.GuiPerformanceSignature,
            api.items.Markup: items.GuiMarkup,
            api.items.Start: items.GuiContainerStart,
            api.items.End: items.GuiContainerEnd,
            api.items.Segno: items.GuiSegno,
            api.items.Coda: items.GuiCoda,
            api.items.Fine: items.GuiFine,
            api.items.GotoSegno: items.GuiGotoSegno,
            api.items.GotoCapo: items.GuiGotoCapo,
            api.items.GotoCoda: items.GuiGotoCoda,
            api.items.AlternateEnd: items.GuiAlternateEnd,
            api.items.AlternateEndClose: items.GuiAlternateEndClose,
            api.items.Upbeat: items.GuiUpbeat,
            api.items.TempoSignature: items.GuiTempoSignature,
            api.items.TempoModification: items.GuiTempoModification,
            api.items.ChannelChangeRelative: items.GuiChannelChange,
            api.items.ChannelChangeAbsolute: items.GuiChannelChange,
            api.items.ProgramChangeRelative: items.GuiProgramChange,
            api.items.ProgramChangeAbsolute: items.GuiProgramChange,
            api.items.InstrumentChange: items.GuiInstrumentChange,
            api.items.SlurOn: items.GuiSlurOn,
            api.items.SlurOff: items.GuiSlurOff,
            api.items.PhrasingSlurOn: items.GuiPhrasingSlurOn,
            api.items.PhrasingSlurOff: items.GuiPhrasingSlurOff,
            api.core.Container: items.GuiClosedContainer,
            }

        if itemType in d:
            return d[itemType](item, forceSigs = forceSigs, parentTrack = main.session.currentGuiWorkspace.currentTrack)

        elif itemType is api.items.Clef:
            creationData = dictionaries.clefs[item.clefString + item.octave]  #0: Clef-Glyph, 1:Pitch Offset #2:Graphic Offset
            return items.GuiClef(item, glyph = creationData[0] , pitch = creationData[1], offset = creationData[2], forceSigs = forceSigs)

        elif isinstance(item, api.items.SpecialBarline):
            return items.GuiSpecialBarline(item)

        else: #Unknown Item. Choose a default glyph.
            mainglyph = dictionaries.knownStandaloneStrings.get(item.exportLilypond(), item.exportLilypond()) #look in a abbreviation dict or use the lily strind directly
            fontScale = 0.5
            mainGlyphShift = 60
            if item.lilypond.startswith("\\bar"):
                mainglyph = dictionaries.barlines[item.lilypond.split('"')[1]]
                fontScale = 1.7
                mainGlyphShift = -20
            elif item.lilypond == "\\once \\override Staff . BarLine #'bar-size = #2 \\bar \"|\"": #special case
                mainglyph = dictionaries.barlines["half"]
                fontScale = 1.7
                mainGlyphShift = -20
            if item.duration:
                new = items.GuiItem(item, mainGlyph = mainglyph, mainGlyphShift = mainGlyphShift, fontScale = fontScale, forceSigs = forceSigs)
                line_vertical = QtGui.QGraphicsLineItem(QtCore.QLineF(0, 30, 0, 0))
                line_vertical.setPen(PEN_POSITION)
                line_vertical.setParentItem(new)
                line_vertical.setPos(12, 5)
                new.addToGroup(line_vertical)
                return new
            else:
                new = items.GuiItem(item, displacement = DISPLACE/2, mainGlyphShift = mainGlyphShift, mainGlyph = mainglyph, fontScale = fontScale , forceSigs = forceSigs)
                new.verticalLine()
                #No vertical line for barlines.
                #line_vertical = QtGui.QGraphicsLineItem(QtCore.QLineF(0, 30, 0, 0))
                #line_vertical.setPen(PEN_POSITION)
                #line_vertical.setParentItem(new)
                #line_vertical.setPos(12, 5)
                #new.addToGroup(line_vertical)
                return new

    def insertAtCursor(self, item, getContainer, standaloneInsert = True):
        """The items come in as backend item.
        The gui has to interpret them and create Gui Items.
        This way it is guaranteed that all backend commands work:
        Copy, Cut, Paste etc.
        It is better that the GUI does more and complicated work
        once on insert than constantly typechecking in every function
        in the backend.

        standaloneInsert means that after insert the gui does
        update right and all the stuff. If it is false it will just
        create the item and place it in the track so it can be
        repositioned later.
        """
        #Let the typechecking begin!  isinstance covers inheritance, type() not
        itemType = type(item)

        if itemType is api.core.Container and item.cursorWalk:
            for it in item:
                self.insertAtCursor(it, getContainer, standaloneInsert)
            main.session.currentGuiWorkspace.generateScoreContainerMenu()
        elif itemType is api.core.Container and not item.cursorWalk:
            self._insertProto(self._convertBackendItemToGuiItem(item, itemType), item, getContainer, standaloneInsert)
            main.session.currentGuiWorkspace.generateScoreContainerMenu()
        elif itemType is api.items.Appending:
            pass
        elif itemType is api.items.KeySignature:
            self._insertProto(self._convertBackendItemToGuiItem(item, itemType), item, getContainer, standaloneInsert)
            main.session.currentGuiWorkspace.currentTrack.keysigUpdate(item) #single item insert
        elif itemType is api.items.TimeSignature:
            main.session.currentGuiWorkspace.currentTrack.timesigUpdate(item) #Why is this before the insert? #TODO: This was the other way around before that. If we get any strange timesig errors we now know why.
            self._insertProto(self._convertBackendItemToGuiItem( item, itemType), item, getContainer, standaloneInsert)
        elif itemType is api.items.Clef:
            self._insertProto(self._convertBackendItemToGuiItem( item, itemType), item, getContainer, standaloneInsert)
            creationData = dictionaries.clefs[item.clefString + item.octave]  #0: Clef-Glyph, 1:Pitch Offset #2:Graphic Offset
            main.session.currentGuiWorkspace.currentTrack.clefUpdate(creationData[1])
            main.session.currentGuiWorkspace.cursorPitch.update()
        else:
            self._insertProto(self._convertBackendItemToGuiItem(item, itemType), item, getContainer, standaloneInsert)

    def deleteAtCursor(self, backendItem, getContainer):
        """Delete the exact instance and copy of the cursor item and
        nothing more.
        Deleting from main.session.currentGuiWorkspace.allItems[backendItem] is done inside
        the track function.

        deleteAtCursor is the exact opposite of insertAtCursor.
        It is NOT for selection.

        It is however for container.

        We check if the item is displaced here and then request an
        'update all tracks' command.

        Inserting and Deleting in containers needs special care.
        Since we must insert/delete the gui items in every container
        instance without moving anything in the backend until it is
        finished and we are in sync with the backend again.

        The backenditem still exists when the gui deletes.
        """

        if getContainer:  #we are in a container
            main.session.currentGuiWorkspace.deletetItemRelativeToContainerStart(backendItem, container = getContainer, howManySteps = api._getIndexFromContainerStart())
        else:
            disp = main.session.currentGuiWorkspace.allItems[backendItem][0].displacement
            main.session.currentGuiWorkspace.currentTrack.delete(backendItem) #This is not "delete track" but "delete cursor item".
            #main.session.currentGuiWorkspace.buildDisplacementMap() #don't do that. updateRight->FirstPassRight does it already.
            theNextItem = main.session.currentGuiWorkspace.currentTrack.getCurrentItem()
            if theNextItem: #not appending, which is just False here.
                forceDisplacement = (disp or theNextItem.displacement)
            else:
                forceDisplacement = disp
            main.session.currentGuiWorkspace.currentTrack.updateRight(forceDisplacementUpdatingOtherTracks = forceDisplacement)  #we need to check if the next item, after delete, was displaced as well. In this case the displacement changes as well, so updateRight needs to update the displacement map. This is especially true if you have only a note on the first position and a clef on the second and delete the note.
            main.session.currentGuiWorkspace.cursorPosition.update()

        self._savedFalse()


    def update(self, backendItem):
        """Redraw the item at cursor position.
        For changes which do not affect spacing on a track.
        This is the complete update, even if unnecessarry.
        For dynamic (pitches) call changePitch.
        For statics (clef, performanceSig-name) call updateStatic"""
        self._allItems(backendItem, "update")

    def updateStatic(self, backendItem):
        """Redraw the part of a guiItem which changes only seldomly.
        Like the displayed name of a performance sig.
        This is the "leftover" signal for anything which has no signal
        of their own. FiguredBass and ChordSymbol have."""
        self._allItems(backendItem, "updateStatic")

    def updateDuration(self, backendItem):
        """See also l_chord.updateRightFromItem.
        In a single track this is not much work since we are already  at
        the item position. the slow track.goToTickIndex has little to
        do.
        """
        #After updating get the (old) cachedTickIndex positions and choose the earlist one. This must be still the same since duration modification cannot modify the items own position. From here generate a new cached tick index so that repositioning afterwards will be correct.
        firstItemsInTracks = [min((x for x in main.session.currentGuiWorkspace.allItems[backendItem] if x.track == g), key=operator.attrgetter('cachedTickIndex')) for g in set(x.track for x in main.session.currentGuiWorkspace.allItems[backendItem])]
        for i in firstItemsInTracks:
            i.FirstPassRightFromThisItem()
        self._savedFalse()

    def updateDynamic(self, backendItem):
        self._allItems(backendItem, "updateDynamic")
        self.updateViewport()

    def updateVisibility(self, backendItem):
        """Update if the item is normal, transparent or hidden"""
        self._allItems(backendItem, "updateVisibility")

    def extensions(self, backenItem):
        #the real extensions signal is in listener chord.
        api.l_chord.extensions(backenItem)

    def select(self, backendItem):
        for i in main.session.currentGuiWorkspace.allItems[backendItem]:
            i.setSelected(True)

    def deselect(self, backendItem):
        for i in main.session.currentGuiWorkspace.allItems[backendItem]:
            i.setSelected(False)

    def smfChannel(self, backendItem):
        self._allItems(backendItem, "smfChannel") #Below the Item

    def smfProgram(self, backendItem):
        self._allItems(backendItem, "smfProgram") #Below the Item

    def smfTrigger(self, backendItem):
        self._allItems(backendItem, "smfTrigger")

class ListenerTrack(Listener):
    def __init__(self):
        super(ListenerTrack, self).__init__()

    def select(self):
        """select the complete current track"""
        main.session.currentGuiWorkspace.currentTrack.select()

    def deselect(self):
        """deselect the complete current track"""
        main.session.currentGuiWorkspace.currentTrack.deselect()

    def deleteContainerInstance(self, backendContainer):
        """Delete the instance of the most inner container.
        For the backend this is just one delete. For a gui it may
        be splitted in single guiItem deletes.

        When receiving this signal the backend-container is already
        gone. The strategy is going left until we find the next
        GuiContainerStart and delete everything until and including
        the matching GuiContainerEnd, deleting any nested container
        in the process.
        """
        main.session.currentGuiWorkspace.generateScoreContainerMenu()
        main.session.currentGuiWorkspace.currentTrack.deleteContainerInstance(backendContainer)
        main.session.currentGuiWorkspace.currentTrack.updateRight(forceDisplacementUpdatingOtherTracks = True)
        self._savedFalse()

class ListenerScore(Listener):
    """Add and remove tracks etc."""
    def __init__(self):
        super(ListenerScore, self).__init__()

    def addLastTrack(self, backendTrack):
        main.session.currentGuiWorkspace.addTrack(backendTrack)
        self._update()
        self._savedFalse()

    def deleteTrack(self, tracknumber):
        main.session.currentGuiWorkspace.deleteTrack(tracknumber, api._getTrackIndex())  #the current track index is already a different track than the one to delete.
        self._update()
        self._savedFalse()

    def trackChanged(self):
        """Signal that the current backend track changed"""
        ws = main.session.currentGuiWorkspace
        ws.updateTrackProperties()
        ws.updateLyricsFrame()
        #self._savedFalse()

    def trackDirectivesChanged(self):
        ws = main.session.currentGuiWorkspace
        ws.updateLilypondStaffGroups()

    def _update(self):
        main.session.currentGuiWorkspace.cursorPosition.position = 0
        main.session.currentGuiWorkspace.cursorPosition.update()
        self._savedFalse()

    def selectGroup(self, group): #group can be a list, set etc. Better it is a set.
        for i in group:
            for x in main.session.currentGuiWorkspace.allItems[i]:
                x.setSelected(True)

    def selectScore(self):
        for tr in main.session.currentGuiWorkspace.tracks:
            tr.select()

    def deselectScore(self):
        main.session.currentGuiWorkspace.scene.clearSelection()
        self._savedFalse()

    def trackSwapUp(self):
        main.session.currentGuiWorkspace.swapTrackWithAbove()
        self._update()
        self._savedFalse()

    def trackSwapDown(self):
        main.session.currentGuiWorkspace.swapTrackWithBelow()
        self._update()
        self._savedFalse()

    def updateTrackFromTickindex(self, trackIndex, tickindex):
        main.session.currentGuiWorkspace.tracks[trackIndex].updateRightFromTickindex(tickindex)

    def updateTrackFromIndex(self, trackIndex, flatIndex):
        if flatIndex != 0:
            flatIndex -= 1
        main.session.currentGuiWorkspace.tracks[trackIndex].updateRightFromIndex(flatIndex)


    def updateTrackFromItem(self, backendItem):
        """Take the earliest(tickindex) from each track and updateRight
        the track from there. Works for multi tracks and single tracks
        this way.firstPassRight
        In a single track this is not much work since we are already  at
        the item position. the slow track.goToTickIndex has little to
        do.
        See also l_item.updateDuration which is similar."""
        firstItemsInTracks = [min((x for x in main.session.currentGuiWorkspace.allItems[backendItem] if x.track == g), key=operator.attrgetter('cachedTickIndex')) for g in set(x.track for x in main.session.currentGuiWorkspace.allItems[backendItem])]
        for i in firstItemsInTracks:
            i.updateRightFromThisItem()
        main.session.currentGuiWorkspace.cursorPosition.update()

    def deleteByItem(self, backendItem):
        """This is not the opposite of insert. It is for deleting
        selections. Just remove the item from the data structures and
        screen.
        The problem are linked items. It is impossible to know
        if the user wants to delete ALL instaces of a linked item, by
        selecting one. Or if he want to delete just one.
        Realistic is that he wants to delete just some of them. This
        is where the trouble begins."""
        pass  #TODO

    def substitutionsChanged(self):
        """A substitution was added or removed from this score"""
        main.session.currentGuiWorkspace.generateScoreSubstitutionsMenu()

    def fullResetAndRedraw(self):
        """Trigger a full gui redraw, as if the file was loaded anew.
        It doesn't get much more unperformant than this.
        If your GUI is very very fast this is all you ever need, simply
        call it after every change and ignore all other drawing and
        update signals."""
        main.session.currentGuiWorkspace.fullReload()

    def playbackStart(self):
        """The playback has started"""
        #main.session.currentGuiWorkspace.playbackCursorSetVisible(True)
        for guiScore in main.session.scenes.values():
            guiScore.playbackCursor.playbackActive = False
            guiScore.playbackCursor.hide()
        main.session.currentGuiWorkspace.playbackCursor.playbackActive = True


    def playbackStop(self):
        """The playback has stopped"""
        #TODO: remove try/except when there is "stop playback on tab"
        try: #it is possible that no tab is open and playback is still running.
            main.session.currentGuiWorkspace.playbackCursorSetVisible(False)
        except:
            pass

    def playbackSetPosition(self, absoluteUnfoldedTicks):
        """Update the playhead/playback cursor position.
        This will only be called when playback is rolling.
        This receives the absolute, unfolded and global ticks.

        It is up to the gui to handle repeats, jumps and containers."""
        #TODO: remove try/except when there is "stop playback on tab"
        try: #it is possible that no tab is open and playback is still running.
            main.session.currentGuiWorkspace.playbackCursorSetVisible(True)
            main.session.currentGuiWorkspace.playbackCursorSetTicks(absoluteUnfoldedTicks)
        except:
            pass

    def playbackPanic(self):
        """A special midi panic signal. Also calls stop. If in doubt
        just call stop and leave it be"""
        self.playbackStop()

class ListenerFormGenerator(Listener):
    """Provide dynamic gui creation for the api"""
    def __init__(self):
        super(ListenerFormGenerator, self).__init__()

    def generateForm(self, dictionary, title, comment = "", autocomplete = []):
        #TODO: Doc widgets
        return fedit(dictionary, title, comment, autocomplete=autocomplete)

class ListenerNote(Listener):
    def __init__(self):
        super(ListenerNote, self).__init__()

    def finger(self, backendItem):
        """This is actually for all fingers and string numbers"""
        self._allItems(backendItem, "allFingers") #All Chordnotes

    def pitch(self):
        raise RuntimeError("Called Signal note.pitch. Should not happen")

class ListenerChord(Listener):
    """Used to send updates for one chord or rest.
    Like chordsymobls or figured Bass"""
    def __init__(self):
        super(ListenerChord, self).__init__()

    def extensions(self, backendItem):
        """All of them"""
        self.figuredBass(backendItem)
        self.chordSymbol(backendItem)
        self.directivePst(backendItem)
        self.tupletMarkers(backendItem)
        self.tremoloMarker(backendItem)

    def figuredBass(self, backendItem):
        self._allItems(backendItem, "figuredBass") #Below the Chord

    def chordSymbol(self, backendItem):
        self._allItems(backendItem, "chordSymbols") #Above the Chord and Rest

    def directivePst(self, backendItem):
        self._allItems(backendItem, "directivesPst") #Above the Chord

    def tupletMarkers(self, backendItem):
        self._allItems(backendItem, "tupletMarkers") #Above the Chord and Rest

    def tremoloMarker(self, backendItem):
        self._allItems(backendItem, "tremoloMarker") #Below the Chord

    def substitutionMarker(self, backendItem):
        self._allItems(backendItem, "substitutionMarker") #Below the Chord


def init():
    """Call after main has been set"""
    api.STANDALONE = False #Switch backend into slave mode.
    api.l_global = ListenerGlobal()
    api.l_cursor = ListenerCursor()
    api.l_item = ListenerItem()
    api.l_track = ListenerTrack()
    api.l_score = ListenerScore()
    api.l_form = ListenerFormGenerator()
    api.l_chord = ListenerChord()
    api.l_note = ListenerNote()
    api.l_record = ListenerRecord()
    #api.l_playback = ListenerPlayback() This is an exception. Don't redefine that. These are not GUI signals but playback signals. We activated this by setting STANDALONE to false already.
    #api.l_collections = ListenerCollections() #Not in use right now
