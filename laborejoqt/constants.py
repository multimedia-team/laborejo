# -*- coding: utf-8 -*-
from PyQt4 import QtGui, QtCore
from os import path
PITCH_MOD = 10

#Nearly all these values are trial and error to compensat different positioning of music fonts, text fonts, qt line objects etc.

TEXT_SCALE_CORRECTION = - 10
MUSIC_SCALE_CORRECTION = - 40
NOTE_MOD = 1.5
TRACK_SIZE = PITCH_MOD * 10 #*15 is safe.
FONT_SIZE = 24
MIDDLE_LINE = -5
DISPLACE = FONT_SIZE * 18
#DISPLACE_TRESHOLD = 192 # Everything that is smaller than 192 gets displacement.
PEN_LINE = QtGui.QPen(QtCore.Qt.black, 0, QtCore.Qt.SolidLine)
PEN_LINE.setCosmetic(True) #Prevents OpenGL zooming from eating lines.
PEN_LINETWO = QtGui.QPen(QtCore.Qt.black, 1, QtCore.Qt.SolidLine)
PEN_LINETWO.setCosmetic(True)
PEN_STEM = QtGui.QPen(QtCore.Qt.black, 2, QtCore.Qt.SolidLine)
PEN_STEM.setCosmetic(True)
PEN_BEAM = QtGui.QPen(QtCore.Qt.black, 3, QtCore.Qt.SolidLine)
PEN_POSITION = QtGui.QPen(QtCore.Qt.black, 3, QtCore.Qt.DashLine)
PEN_LINE.setColor(QtGui.QColor("black"))
PEN_STEM.setColor(QtGui.QColor("black"))
PEN_BEAM.setColor(QtGui.QColor("black"))
PEN_POSITION.setColor(QtGui.QColor("grey"))
PEN_LINETWO.setColor(QtGui.QColor("red"))
PEN_BARLINE = QtGui.QPen(QtCore.Qt.black, 1, QtCore.Qt.SolidLine)
PEN_BARLINE.setColor(QtGui.QColor("black"))
PEN_BARLINE.setCosmetic(True)


PEN_LINEFORBIDDEN = QtGui.QPen(QtCore.Qt.black, 4, QtCore.Qt.SolidLine)
PEN_LINEFORBIDDEN.setColor(QtGui.QColor("red"))

#Two fonts which are used in the QGraphicsScene. They are special because they don't scale with the DPI.
#the fontDB is part of qt, which makes it global. It does not need to get imported in other modules. import qt is enough.
fontDB = QtGui.QFontDatabase()
fid1 = fontDB.addApplicationFont(path.join(path.dirname(__file__), "resources", "euterpe.ttf"))
fid2 = fontDB.addApplicationFont(path.join(path.dirname(__file__), "resources", "freesans.ttf"))
if fid1 == -1 or fid2 == -1:
    raise ValueError("One of the two fonts euterpe.ttf and freesans.ttf were not loaded. This is critical. Did someone move the files around?")

MUSIC_FONT_STRING = "Euterpe"
TEXT_FONT_STRING = "FreeSans"


COLLAPSED_FACTOR = 0.5
COLLAPSED_TRANSFORM = QtGui.QTransform()
COLLAPSED_TRANSFORM.scale(1, COLLAPSED_FACTOR)

EXPANDED_TRANSFORM = QtGui.QTransform()
EXPANDED_TRANSFORM.scale(1, 1)
