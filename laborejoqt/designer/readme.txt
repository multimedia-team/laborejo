1) Add .ui and .py files to trans_de.pro
2) pypylupdate4 trans_de.pro   -  creates or updates trans_de.ts and does not delete already translated strings
3) linguist trans_de.ts (qt tool) - A gui to translate.
4) lrelease trans_de.ts - Creates a binary .qm file.

Thats it. the pyqt code is already configured to load that qm file. You don't need pyrcc4, as suggested in the following link.

see also http://www.riverbankcomputing.co.uk/static/Docs/PyQt4/html/i18n.html
