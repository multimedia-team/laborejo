<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="gui_main.py" line="1391"/>
        <source>Laborejo - Music Notation Workshop</source>
        <translation>Laborejo - Music Notation Workshop</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1392"/>
        <source>Score</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1447"/>
        <source>Lilypond</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1394"/>
        <source>Execute Command</source>
        <translation>Befehl ausführen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1395"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1396"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1397"/>
        <source>Export</source>
        <translation>Exportieren</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1398"/>
        <source>Recent</source>
        <translation>Zuletzt geöffnet</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1399"/>
        <source>Import</source>
        <translation>Importieren</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1400"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1401"/>
        <source>Objects</source>
        <translation>Objekte</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1402"/>
        <source>Ornaments</source>
        <translation>Ornamente</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1403"/>
        <source>Rhythm and Dynamics</source>
        <translation>Rythmus und Dynamik</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1404"/>
        <source>Embellishments</source>
        <translation>Verzierungen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1405"/>
        <source>Strings</source>
        <translation>Streicher</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1406"/>
        <source>Organ</source>
        <translation>Orgel</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1407"/>
        <source>Ancient</source>
        <translation>Antik</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1408"/>
        <source>Other</source>
        <translation>Sonstiges</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1409"/>
        <source>Note Duration</source>
        <translation>Notenlängen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1410"/>
        <source>Rests</source>
        <translation>Pausen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1411"/>
        <source>Dynamics</source>
        <translation>Dynamik</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1416"/>
        <source>Styles</source>
        <translation>Stile</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1413"/>
        <source>Slurs</source>
        <translation>Legatobögen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1415"/>
        <source>Ties</source>
        <translation>Haltebögen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1417"/>
        <source>Voice Presets</source>
        <translation>Stimmenauswahl</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1418"/>
        <source>Chord Symbols</source>
        <translation>Akkordsymbole</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1419"/>
        <source>Figured Bass</source>
        <translation>Bezifferter Bass</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1420"/>
        <source>Beaming</source>
        <translation>Balken</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1421"/>
        <source>Fingering</source>
        <translation>Fingersatz</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1422"/>
        <source>Fingering Style</source>
        <translation>Fingersatz-Stil</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1423"/>
        <source>String Number Style</source>
        <translation>Saitennummer-Stil</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1424"/>
        <source>Stroke Finger Style</source>
        <translation>Zupf/Schlag-Finger-Stil</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1479"/>
        <source>Barlines</source>
        <translation>Taktstriche</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1434"/>
        <source>Advanced</source>
        <translation>Extra Befehle</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1427"/>
        <source>Note Pitch</source>
        <translation>Melos</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1428"/>
        <source>Jumps and Endings</source>
        <translation>Sprünge und Enden</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1429"/>
        <source>Pedals</source>
        <translation>Pedale</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1430"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1431"/>
        <source>View</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1432"/>
        <source>Container</source>
        <translation>Container</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1433"/>
        <source>𝄔 Grouping (Print)</source>
        <translation>𝄔 Gruppierung(Druck)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1448"/>
        <source>Midi</source>
        <translation>Midi</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1436"/>
        <source>Lilypond Directives</source>
        <translation>Lilypond Direktiven</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1437"/>
        <source>Trigger</source>
        <translation>Trigger</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1439"/>
        <source>Mode</source>
        <translation>Modus</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1440"/>
        <source>User Commands</source>
        <translation>User Befehle</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1441"/>
        <source>toolBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1442"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1443"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1444"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1445"/>
        <source>Save As</source>
        <translation>Speichern unter</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1446"/>
        <source>PDF</source>
        <translation>PDF</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1449"/>
        <source>View PDF</source>
        <translation>PDF Vorschau</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1450"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1451"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1452"/>
        <source>Staccato</source>
        <translation>Staccato</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1453"/>
        <source>Fermata</source>
        <translation>Fermate</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1454"/>
        <source>Trill</source>
        <translation>Triller</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1455"/>
        <source>Downbow</source>
        <translation>Abstrich</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1456"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1457"/>
        <source>Cut</source>
        <translation>Ausschneiden</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1458"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1459"/>
        <source>Undo</source>
        <translation>Rückgängig</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1460"/>
        <source>Redo</source>
        <translation>Wiederholen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1461"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1462"/>
        <source>Laborejo Preferences</source>
        <translation>Laborejo Einstellungen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1463"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1464"/>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1465"/>
        <source>Manual</source>
        <translation>Handbuch</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1466"/>
        <source>Metadata (Title…)</source>
        <translation>Metadaten (Titel...)</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1467"/>
        <source>𝄞 Clefs</source>
        <translation>𝄞Schlüssel</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1468"/>
        <source>¾ Time Signatures</source>
        <translation>¾ Taktarten</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1469"/>
        <source>♮♯♭ Typical Key Signature</source>
        <translation>♮♯♭ Übliche Vorzeichen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1470"/>
        <source>Paste As Link</source>
        <translation>Einfügen als Link</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1471"/>
        <source>Delete Previous</source>
        <translation>Lösche Vorheriges</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1472"/>
        <source>Duplicate</source>
        <translation>Duplizieren</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1473"/>
        <source>Link</source>
        <translation>Verlinken</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1474"/>
        <source>Track Properties</source>
        <translation>Track Eigenschaften</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1475"/>
        <source>⊞ Add Track</source>
        <translation>⊞ Track hinzufügen</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1476"/>
        <source>⊟ Delete Current Track</source>
        <translation>⊟ Aktuellen Track Löschen</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1477"/>
        <source>⬆ Move Track Up</source>
        <translation>⬆ Track nach oben schieben</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1478"/>
        <source>⬇ Move Track Down</source>
        <translation>⬇ Track nach unten schieben</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1480"/>
        <source>Edit Current Object</source>
        <translation>Editiere aktuelles Objekt</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1481"/>
        <source>Triplet</source>
        <translation>Triole</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1482"/>
        <source>Custom Tuplet</source>
        <translation>N-tole</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1483"/>
        <source>Augment (×2)</source>
        <translation>Augmentieren (×2)</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1484"/>
        <source>Diminish (÷2)</source>
        <translation>Diminuieren (÷2)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1761"/>
        <source>Add Dot</source>
        <translation>Punktiert</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1486"/>
        <source>.</source>
        <translation>.</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1487"/>
        <source>Remove Dot</source>
        <translation>Entferne Punktierungen</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1488"/>
        <source>𝄩 Multi Measure Rest</source>
        <translation>𝄩  Mehrtaktpause</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1489"/>
        <source>𝄺 Full Measure Rest</source>
        <translation>𝄺 Ganztaktpause</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1490"/>
        <source>Clear Tuplets</source>
        <translation>N-tolen alle entfernen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1491"/>
        <source>Insert Blank Item</source>
        <translation>Füge Leeres Objekt ein</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1492"/>
        <source>Insert Wait-For-Chord Lilypond</source>
        <translation>Füge Wait-For-Chord Lilypond ein </translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1493"/>
        <source>Clear Object Directives</source>
        <translation>Setze Objekt Direktiven zurück</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1494"/>
        <source>Clear Notes Directives</source>
        <translation>Setze Noten Direktiven Zurück</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1495"/>
        <source>Clear All Directives</source>
        <translation>Setze alle Direktiven zurück</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1496"/>
        <source>Edit Lilypond</source>
        <translation>Bearbeite Lilypond</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1497"/>
        <source>Edit Wait-For-Chord Lilypond</source>
        <translation>Bearbeite Wait-For-Chord Lilypond</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1498"/>
        <source>Create Pre/Mid/Post Directives</source>
        <translation>Erschaffe Pre/Mid/Post Direktiven</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1499"/>
        <source>Edit Pre/Mid/Post Directives</source>
        <translation>Bearbeite Pre/Mid/Post Direktiven  </translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1500"/>
        <source>Edit Notes Directives</source>
        <translation>Bearbeite Noten-Direktiven</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1501"/>
        <source>Create Notes Directives</source>
        <translation>Erschaffe Noten-Direktiven</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1502"/>
        <source>♮♯♭ Custom Key Signature</source>
        <translation>♮♯♭ Spezielles Vorzeiches</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1503"/>
        <source>𝆏 piano</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1504"/>
        <source>𝆑 forte</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1505"/>
        <source>𝆐𝆏 mezzo piano</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1506"/>
        <source>𝆐𝆑 mezzo forte</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1507"/>
        <source>𝆑𝆑 fortissimo</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1508"/>
        <source>𝆑𝆑𝆑 fortississimo</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1509"/>
        <source>𝆑𝆑𝆑𝆑 4× forte</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1510"/>
        <source>𝆏𝆏 pianissimo</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1511"/>
        <source>𝆏𝆏𝆏 pianississimo</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1512"/>
        <source>𝆏𝆏𝆏𝆏 4x piano</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1513"/>
        <source>𝆏𝆏𝆏𝆏𝆏 5× piano</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1514"/>
        <source>𝆒 crescendo</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1515"/>
        <source>𝆓 decrescendo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1516"/>
        <source>end cresc/decresc</source>
        <translation>Beende cresc/decresc</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1517"/>
        <source>Above Track</source>
        <translation>Über Track</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1518"/>
        <source>Below Track</source>
        <translation>Unter Track</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1550"/>
        <source>Automatic</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1520"/>
        <source>Crescendo as &quot;cresc&quot;</source>
        <translation>Crescendo als &quot;cresc&quot;</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1521"/>
        <source>Crescendo as 𝆓</source>
        <translation>Crescendo als 𝆓</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1522"/>
        <source>Decrescendo as &quot;decr&quot;</source>
        <translation>Decrescendo als &quot;decr&quot;</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1523"/>
        <source>Decrescendo as 𝆒 </source>
        <translation>Decrescendo als 𝆒 </translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1524"/>
        <source>Decrescendo as &quot;dim&quot;</source>
        <translation>Decrescendo als &quot;dim&quot;</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1525"/>
        <source>Decrescendo as &quot;decresc&quot;</source>
        <translation>Decrescendo als &quot;decresc&quot;</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1526"/>
        <source>Slur Start</source>
        <translation>Legatobogen Start</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1527"/>
        <source>Slur End</source>
        <translation>Legatobogen Stop</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1528"/>
        <source>Phrasing Slur Start</source>
        <translation>Phrasierungsbogen Start</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1529"/>
        <source>Phrasing Slur End</source>
        <translation>Phrasierungsbogen Stop</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1530"/>
        <source>Slurs Up</source>
        <translation>Legatobögen Obenrum</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1531"/>
        <source>Slurs Down</source>
        <translation>Legatobögen Untenrum</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1532"/>
        <source>Slurs Automatic</source>
        <translation>Legatobögen Automatisch</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1533"/>
        <source>Dashed Slur</source>
        <translation>Gestrichelte Legatobögen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1534"/>
        <source>Dotted Slur</source>
        <translation>Gepunktete Legatobögen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1535"/>
        <source>Solid Slur</source>
        <translation>Durchgezogene Legatobögen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1536"/>
        <source>Double Slur On</source>
        <translation>Doppel-Legatobogen an</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1537"/>
        <source>Double Slur Off</source>
        <translation>Doppel-Legatobogen aus</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1538"/>
        <source>Phrasing Slurs Up</source>
        <translation>Phrasierungsbögen Obenrum</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1539"/>
        <source>Phrasing Slurs Down</source>
        <translation>Phrasierungsbögen Untenrum</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1540"/>
        <source>Phrasing Slurs Automatic</source>
        <translation>Phrasierungsbögen Automatisch</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1541"/>
        <source>Dashed Phrasing Slur</source>
        <translation>Gestrichelte Phrasierungsbögen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1542"/>
        <source>Dotted Phrasing Slur</source>
        <translation>Gepunktete Phrasierungsbögen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1543"/>
        <source>Solid Phrasing Slur</source>
        <translation>Durchgezogene Legatobögen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1544"/>
        <source>Tie (On/Off)</source>
        <translation>Haltebogen (an/aus)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1546"/>
        <source>Laissez Vibrer (On/Off)</source>
        <translation>Endlos Haltebogen (an/aus)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1547"/>
        <source>Tie One Note (On/Off)</source>
        <translation>Haltebogen (an/aus)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1548"/>
        <source>Up</source>
        <translation>Hoch</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1549"/>
        <source>Down</source>
        <translation>Runter</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1656"/>
        <source>Dotted</source>
        <translation>Gepunktet</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1657"/>
        <source>Dashed</source>
        <translation>Gestrichelt</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1553"/>
        <source>Solid</source>
        <translation>Durchgezogen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1554"/>
        <source>Wait for Note On</source>
        <translation>Warte auf nächste Note </translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1555"/>
        <source>Wait for Note Off</source>
        <translation>Warte nicht auf nächste Note</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1556"/>
        <source>𝄔 Piano Staff Start</source>
        <translation>𝄔 Klaviersystem Start</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1557"/>
        <source>𝄔 Grand Staff Start</source>
        <translation>𝄔 Doppelsystem Start</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1558"/>
        <source>𝄕 Orchestral Staff Start</source>
        <translation>𝄕 Orchestersystem Start</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1559"/>
        <source>𝄕 Choir Staff Start</source>
        <translation>𝄕 Choirsystem Start</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1560"/>
        <source>𝄕 Square Staff Start</source>
        <translation>𝄕 Eckiges System Start</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1561"/>
        <source>𝄔 Voice and Figured Bass Start</source>
        <translation>𝄔 Stimme und bezifferter Bass Start</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1562"/>
        <source>End One Group</source>
        <translation>Eine Gruppe beenden</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1563"/>
        <source>Clear All Groupings</source>
        <translation>Setze alle Gruppen zurück</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1564"/>
        <source>✇ Performance Signature</source>
        <translation>✇ Performance-Vorzeichen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1565"/>
        <source>Edit Performance Signature</source>
        <translation>Bearbeite Performance Vorzeichen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1566"/>
        <source>Movement Performance</source>
        <translatorcomment>That is not even used</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1567"/>
        <source>User Dynamic 1 (non-print)</source>
        <translation>User Dynamik 1 (nicht im Druck)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1568"/>
        <source>User Dynamic 2 (non-print)</source>
        <translation>User Dynamik 2 (nicht im Druck)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1569"/>
        <source>User Dynamic 3 (non-print)</source>
        <translation>User Dynamik 3 (nicht im Druck)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1570"/>
        <source>Upbeat</source>
        <translation>Auftakt</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1571"/>
        <source>Automatic / One Voice</source>
        <translation>Automatisch / Einzelstimme</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1572"/>
        <source>Voice One</source>
        <translation>Erste Stimme</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1573"/>
        <source>Voice Two</source>
        <translation>Zweite Stimme</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1574"/>
        <source>Voice Three</source>
        <translation>Dritte Stimme</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1575"/>
        <source>Voice Four</source>
        <translation>Vierte Stimme</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1576"/>
        <source>Beam Start</source>
        <translation>Balken Start</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1577"/>
        <source>Beam End</source>
        <translation>Balken Ende</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1578"/>
        <source>No Beam</source>
        <translation>Kein Balken</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1579"/>
        <source>Force Beam</source>
        <translation>Erzwinge Balken</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1580"/>
        <source>Export Options</source>
        <translation>Export Optionen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1581"/>
        <source>Edit Chord Symbol</source>
        <translation>Bearbeite Akkordsymbol</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1582"/>
        <source>Clear Chord Symbol</source>
        <translation>Setze Akkordsymbol zurück</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1583"/>
        <source>Edit Figured Bass</source>
        <translation>Bearbeite bezifferten Bass</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1584"/>
        <source>Clear Figured Bass</source>
        <translation>Setze bezifferten Bass zurück</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1585"/>
        <source>Mass Insert Chord Symbols</source>
        <translation>Füge Akkordsymbole in Reihe ein</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1586"/>
        <source>Mass Insert Figured Bass</source>
        <translation>Füge bezifferte Bässe in Reihe ein</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1587"/>
        <source>Lyrics</source>
        <translation>Gesangstext</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1588"/>
        <source>Paste as One-Track Stream</source>
        <translation>Einfügen als Ein-Track-Kette</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1589"/>
        <source>Split in Two</source>
        <translation>Teile in Zwei</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1590"/>
        <source>Split in Three</source>
        <translation>Teile in Drei</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1591"/>
        <source>Custom Split</source>
        <translation>Spezielles Teilen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1592"/>
        <source>Tremolo in Two</source>
        <translation>Tremolo in Zwei</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1593"/>
        <source>Tremolo in Four</source>
        <translation>Tremolo in Vier</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1594"/>
        <source>Custom Tremolo</source>
        <translation>Spezielles Tremolo</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1595"/>
        <source>Edit Default Performance Signature</source>
        <translation>Bearbeite Default Performance-Vorzeichen</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1596"/>
        <source>♺ Instrument Change</source>
        <translation>♺ Instrumentenwechsel</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1597"/>
        <source>Item Channel +</source>
        <translation>Objekt Channel +</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1598"/>
        <source>Item Channel -</source>
        <translation>Objekt Channel -</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1599"/>
        <source>Item Force Channel</source>
        <translation>Objet erzwinge Channel</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1600"/>
        <source>Widen</source>
        <translation>Abstände erweitern</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1601"/>
        <source>Shrinken</source>
        <translation>Abstände verringern</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1602"/>
        <source>Zoom In</source>
        <translation>Vergrößern (Zoom)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1603"/>
        <source>Zoom Out</source>
        <translation>Verkleinern (Zoom)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1604"/>
        <source>~/.laborejo/script.py</source>
        <translatorcomment>Not in use</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1605"/>
        <source>Prevailing Duration</source>
        <translation>Aktuell geltender Notenwert</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1606"/>
        <source>Insert Duration</source>
        <translation>Füge Notenwert ein</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1607"/>
        <source>Toggle Insert/Prevailing</source>
        <translation>Schalte zwischen Einfügen/Auswählen d. Notenwerte um</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1608"/>
        <source>Fingerings</source>
        <translation>Fingersätze</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1609"/>
        <source>String Numbers</source>
        <translation>Saitennummern</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1610"/>
        <source>Stroke Fingering</source>
        <translation>Zupf/Schlag-Fingersätze</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1611"/>
        <source>Item Clear Channel</source>
        <translation>Setze Objekt Channel zurück</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1612"/>
        <source>Prevailing Dot</source>
        <translation>Nächstes Objekt punktiert</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1613"/>
        <source>⌛ Tempo Signature</source>
        <translation>⌛ Geschwindigkeit/Tempo</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1614"/>
        <source>Clear All Fingerings</source>
        <translation>Setze alle Fingersätze zurück</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1615"/>
        <source>Custom Finger</source>
        <translation>Spezieller Fingersatz</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1616"/>
        <source>Finger 1</source>
        <translation>Finger 1</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1617"/>
        <source>Finger 2</source>
        <translation>Finger 2</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1618"/>
        <source>Finger 3</source>
        <translation>Finger 3</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1619"/>
        <source>Finger 4</source>
        <translation>Finger 4</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1620"/>
        <source>Finger 5</source>
        <translation>Finger 5</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1621"/>
        <source>Finger 6 :)</source>
        <translation>Finger 6 :)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1622"/>
        <source>Custom String Number</source>
        <translation>Spezielle Saitennummer</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1623"/>
        <source>String 1</source>
        <translation>Saite 1</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1624"/>
        <source>String 2</source>
        <translation>Saite 2</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1625"/>
        <source>String 3</source>
        <translation>Saite 3</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1626"/>
        <source>String 4</source>
        <translation>Saite 4</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1627"/>
        <source>String 5</source>
        <translation>Saite 5</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1628"/>
        <source>String 6</source>
        <translation>Saite 6</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1629"/>
        <source>String 7</source>
        <translation>Saite 7</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1630"/>
        <source>String 8</source>
        <translation>Saite 8</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1631"/>
        <source>String 9</source>
        <translation>Saite 9</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1632"/>
        <source>String 10/0</source>
        <translation>Saite 10/0</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1633"/>
        <source>Custom Right Hand / Stroke Finger</source>
        <translation>Spezielle Zupf/Schlagfinger (rechte Hand)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1634"/>
        <source>Stroke 1 &apos;p&apos;</source>
        <translation>Zupf/Schlag 1 &apos;p&apos;</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1635"/>
        <source>Stroke 2 &apos;i&apos;</source>
        <translation>Zupf/Schlag 2 &apos;i&apos;</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1636"/>
        <source>Stroke 3 &apos;m&apos;</source>
        <translation>Zupf/Schlag 3 &apos;m&apos;</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1637"/>
        <source>Stroke 4 &apos;a&apos;</source>
        <translation>Zupf/Schlag 4 &apos;a&apos;</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1638"/>
        <source>Stroke &apos;x&apos;</source>
        <translation>Zupf/Schlag &apos;x&apos;</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1639"/>
        <source>Fingering Direction</source>
        <translation>Fingersatz Ausrichtung</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1647"/>
        <source>Toggle Allow in Staff</source>
        <translation>Innerhalb d. Systems erlaubt (an/aus)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1641"/>
        <source>Fingering Add Stem Support</source>
        <translation>Fingersätze beachten Notenhälse</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1642"/>
        <source>Fingering Disable Stem Support</source>
        <translation>Fingersätze missachten Notenhälse</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1644"/>
        <source>String Add Stem Support</source>
        <translation>Saitennummern beachten Notenhälse</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1645"/>
        <source>String Disable Stem Support</source>
        <translation>Saitennummern missachten Notenhälse</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1646"/>
        <source>Stroke Directions</source>
        <translation>Zupf/Schlag Fingersätze Ausrichtung</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1648"/>
        <source>Stroke Add Stem Support</source>
        <translation>Zupf/Schlag Fingersätze beachten Notenhälse</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1649"/>
        <source>Stroke Disable Stem Support</source>
        <translation>Zupf/Schlag Fingersätze missachten Notenhälse</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1650"/>
        <source>Finger 0 / Thumb</source>
        <translation>Finger 0 / Daumen</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1651"/>
        <source>𝄁 Double</source>
        <translation>𝄁 Doppelstrich</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1652"/>
        <source>𝄃 Open</source>
        <translation>𝄃 Anfang</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1653"/>
        <source>𝄂 End</source>
        <translation>𝄂 Schluß</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1654"/>
        <source>Open|End</source>
        <translation>Anfang|Schluß</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1655"/>
        <source>𝄂𝄃 End|Open</source>
        <translation>𝄂𝄃 Schluß|Anfang</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1658"/>
        <source>Half Size</source>
        <translation>Halber Strich</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1659"/>
        <source>𝄆 Repeat Open</source>
        <translation>𝄆 Wiederholung Anfang</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1660"/>
        <source>𝄇 Repeat Close</source>
        <translation>𝄇 Wiederholung Schluß</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1661"/>
        <source>𝄇𝄆 Repeat Close|Open</source>
        <translation>𝄇𝄆 Wiederholung Schluß|Anfang</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1662"/>
        <source>End + Repeat Open</source>
        <translation>Schluß + Wiederholungsanfang</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1663"/>
        <source>Espressivo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1664"/>
        <source>Portato</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1665"/>
        <source>Staccatissimo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1666"/>
        <source>Tenuto</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1667"/>
        <source>Prall</source>
        <translation>Praller</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1668"/>
        <source>Mordent</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1669"/>
        <source>Prall Mordent</source>
        <translation>Praller Mordent</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1670"/>
        <source>Turn</source>
        <translation>Doppelschlag</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1671"/>
        <source>Upprall</source>
        <translatorcomment>TODO</translatorcomment>
        <translation>Praller hoch</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1672"/>
        <source>Downprall</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1673"/>
        <source>Upmordent</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1674"/>
        <source>Lineprall</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1675"/>
        <source>Prallprall</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1676"/>
        <source>Prall Up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1677"/>
        <source>Reverse Turn</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1678"/>
        <source>Short Fermata</source>
        <translation>Fermate kurz</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1679"/>
        <source>Very Long Fermata</source>
        <translation>Fermate sehr lang</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1680"/>
        <source>open</source>
        <translation>Offen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1681"/>
        <source>Stopped String</source>
        <translation>Gestoppte Saite</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1682"/>
        <source>Open String</source>
        <translation>Offene Saite</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1683"/>
        <source>Left Heel</source>
        <translation>Linke Verse</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1684"/>
        <source>Right Heel</source>
        <translation>Rechte Verse</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1685"/>
        <source>Left Toe</source>
        <translation>Linke Zehen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1686"/>
        <source>Right Toe</source>
        <translation>Rechte Zehen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1687"/>
        <source>Signum Congruentia</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1688"/>
        <source>Ictus</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1689"/>
        <source>Accentus</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1690"/>
        <source>Circulus</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1691"/>
        <source>Semi Circulus</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1692"/>
        <source>Augmentum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1693"/>
        <source>Pizzicato</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1694"/>
        <source>Snap Piz. (&apos;Bartok&apos;)</source>
        <translation>Bartok Pizzicato</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1695"/>
        <source>Accent</source>
        <translation>Akzent</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1696"/>
        <source>Marcato</source>
        <translation>Marcato</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1697"/>
        <source>Long Fermata</source>
        <translation>Fermate lang</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1698"/>
        <source>Downmordent</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1699"/>
        <source>Prall Down</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1700"/>
        <source>Upbow</source>
        <translation>Aufstrich</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1701"/>
        <source>𝄞</source>
        <translation>𝄞</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1702"/>
        <source>Treble Clef</source>
        <translation>Violinenschlüssel</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1703"/>
        <source>𝄢</source>
        <translation>𝄢</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1704"/>
        <source>Bass Clef</source>
        <translation>Bassschlüssel</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1705"/>
        <source>¾</source>
        <translation>¾</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1706"/>
        <source>3/4 Time Signature</source>
        <translation>3/4 Takt</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1707"/>
        <source>𝄴</source>
        <translation>𝄴</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1708"/>
        <source>4/4 Time Signature</source>
        <translation>4/4 Takt</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1709"/>
        <source>♮♯♭</source>
        <translation>♮♯♭</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1710"/>
        <source>Typical Key Signature</source>
        <translation>Übliche Vorzeichen</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1711"/>
        <source>⊟</source>
        <translation>⊟</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1712"/>
        <source>Delete Track</source>
        <translation>Lösche Track</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1713"/>
        <source>⊞</source>
        <translation>⊞</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1714"/>
        <source>Add Track</source>
        <translation>Füge Track hinzu</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1715"/>
        <source>𝄵</source>
        <translation>𝄵</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1716"/>
        <source>2/2 Time Signature</source>
        <translation>2/2 Takt</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1717"/>
        <source>✍ Free Text</source>
        <translation>✍ Beliebiger Text</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1718"/>
        <source>Eyeglasses</source>
        <translation>Brillenzeichen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1719"/>
        <source>Undress While Playing</source>
        <translation>Ausziehen während des Spielens</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1720"/>
        <source>to LilyBin.com</source>
        <translation>zu LilyBin.com</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1721"/>
        <source>Shift Up</source>
        <translation>Bewege nach oben</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1722"/>
        <source>Shift Down</source>
        <translation>Bewege nach unten</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1723"/>
        <source>Shift Note Up</source>
        <translation>Bewege Einzelnote nach oben </translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1724"/>
        <source>Shift Note Down</source>
        <translation>Bewege Einzelnote nach unten</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1725"/>
        <source>Shift Octave Up</source>
        <translation>Bewege Oktave nach oben</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1726"/>
        <source>Shift Octave Down</source>
        <translation>Bewege Oktave nach unten</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1727"/>
        <source>♯ Sharp Note</source>
        <translation>♯ Kreuz</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1728"/>
        <source>♭ Flat Note</source>
        <translation>♭ b / es</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1729"/>
        <source>Enharmonic Sharp</source>
        <translation>Enharmonisch Kreuzrichtung</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1730"/>
        <source>Enharmonic Flat</source>
        <translation>Enharmonisch B-Richtung</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1731"/>
        <source>Add Note to Chord</source>
        <translation>Füge Note zum Akkord hinzu</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1732"/>
        <source>Remove Note from Chord</source>
        <translation>Entferne Note vom Akkord</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1733"/>
        <source>Transpose</source>
        <translation>Transponiere</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1734"/>
        <source>Transpose Down</source>
        <translation>Transponiere abwärts</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1735"/>
        <source>Select Track</source>
        <translation>Selektiere Track</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1736"/>
        <source>Select All</source>
        <translation>Selektiere Alles</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1737"/>
        <source>𝅝</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1738"/>
        <source>Whole</source>
        <translation>Ganze</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1739"/>
        <source>𝅗𝅥</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1740"/>
        <source>Half</source>
        <translation>Halbe</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1741"/>
        <source>𝅘𝅥</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1742"/>
        <source>Quarter</source>
        <translation>Viertel</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1743"/>
        <source>𝅘𝅥𝅮</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1744"/>
        <source>Eigth</source>
        <translation>Achtel</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1745"/>
        <source>𝅘𝅥𝅯</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1746"/>
        <source>Sixteenth</source>
        <translation>Sechzehntel</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1747"/>
        <source>Clear Selection</source>
        <translation>Setze Selektion zurück</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1748"/>
        <source>𝄺</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1749"/>
        <source>Full Measure Rest</source>
        <translation>Ganztaktpause</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1750"/>
        <source>𝄻</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1751"/>
        <source>Whole Rest</source>
        <translation>Ganze Pause</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1752"/>
        <source>𝄼</source>
        <translation>𝄼</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1753"/>
        <source>Half Rest</source>
        <translation>Halbe Pause</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1754"/>
        <source>𝄽</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1755"/>
        <source>Quarter Rest</source>
        <translation>Viertelpause</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1756"/>
        <source>𝄾</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1757"/>
        <source>Eighth Rest</source>
        <translation>Achtelpause</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1758"/>
        <source>𝄿</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1759"/>
        <source>Sixteenth Rest</source>
        <translation>Sechzehntelpause</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1760"/>
        <source>𝅘𝅥𝅭</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1762"/>
        <source>Shift Note Octave Up</source>
        <translation>Bewege Einzelnote Oktave nach oben </translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1763"/>
        <source>Shift Note Octave Down</source>
        <translation>Bewege Einzelnote Oktave nach unten</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1764"/>
        <source>Clear Substitution from Item</source>
        <translation>Setze Substitution zurück</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1765"/>
        <source>Laborejo Substitutions</source>
        <translation>Laborejo Substitutions</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1766"/>
        <source>Movement Substitutions</source>
        <translation>Datei-Substitutionen</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1767"/>
        <source>Remove Movement Substitution</source>
        <translation>Lösche Datei-Substitution</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1768"/>
        <source>Create Substitution from Selection</source>
        <translation>Erschaffe Substition aus Selektion</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1769"/>
        <source>Create Substitutions from File</source>
        <translation>Erschafft Substituions aus Datei</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1770"/>
        <source>Insert Empty Container</source>
        <translation>Füge leeren Container ein</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1771"/>
        <source>Delete Container Instance</source>
        <translation>Lösche Containerinstanz</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1772"/>
        <source>Convert Selection to Container</source>
        <translation>Wandle Selektion in Container um</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1773"/>
        <source>Minimap</source>
        <translation>Minimap</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1774"/>
        <source>▶</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1775"/>
        <source>Play (F5)</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1776"/>
        <source>◼</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1777"/>
        <source>Stop (F6)</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1778"/>
        <source>𝄊 Da Capo</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1779"/>
        <source>𝄉 Dal Segno</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1780"/>
        <source>𝄋 Segno</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1781"/>
        <source>𝄌 Coda</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1782"/>
        <source>𝄂 Fine</source>
        <translation></translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1783"/>
        <source>𝄌 to Coda</source>
        <translation></translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1784"/>
        <source>Clear Trigger from Item</source>
        <translation>Setze Objekt-Trigger zurück </translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1785"/>
        <source>Only first time</source>
        <translation>Nur erstes mal</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1786"/>
        <source>Never first time</source>
        <translation>Nie erstes mal</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1787"/>
        <source>Edit Trigger</source>
        <translation>Bearbeite Trigger</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1788"/>
        <source>Add Octave to Chord</source>
        <translation>Füge Oktave hinzu</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1789"/>
        <source>Invert Selection</source>
        <translation>Invertiere Selektion</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1790"/>
        <source>Join to Cursor</source>
        <translation>Vereine zu Cursorposition</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1791"/>
        <source>Join to Chord</source>
        <translation>Vereine als Akkord</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1792"/>
        <source>Edit Container</source>
        <translation>Bearbeite Container</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1793"/>
        <source>Alternate End</source>
        <translation>Klammer N</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1794"/>
        <source>Alternate Ending 1</source>
        <translation>Klammer 1</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1795"/>
        <source>Alternate Ending 2</source>
        <translation>Klammer 2</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1796"/>
        <source>Custom Alternate Ending </source>
        <translation>Spezielle Klammer</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1797"/>
        <source>Alternate Ending Close</source>
        <translation>Schließe Klammern</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1798"/>
        <source>Tie from left side (repeat Tie)</source>
        <translation>Haltebogen von Links (wiederhole Haltebogen)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1799"/>
        <source>MIDI Note Entry</source>
        <translation>MIDI Noteneingabe (an/aus)</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1800"/>
        <source>𝆮 Sustain On</source>
        <translation>𝆮 Rechtes Pedal runter (Haltepedal)</translation>
    </message>
    <message utf8="true">
        <location filename="gui_main.py" line="1801"/>
        <source>𝆯 Sustain Off</source>
        <translation>𝆯 Rechtes Pedal weg (Haltepedal)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1802"/>
        <source>Una Corda / Soft On</source>
        <translation>Una Corda runter (Mittleres Pedal)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1803"/>
        <source>Una Corda / Soft Off</source>
        <translation>Una Corda weg (Mittleres Pedal)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1804"/>
        <source>Sostenuto On</source>
        <translation>Sostenuto an</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1805"/>
        <source>Sostenuto Off</source>
        <translation>Sostenuto aus</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1806"/>
        <source>Sustain Change (non-print)</source>
        <translation>Pedalwechsel (nicht im Druck)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1807"/>
        <source>Lilypond Text</source>
        <translation>LIlypond Text</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1808"/>
        <source>Open as Template</source>
        <translation>Öffne als Vorlage</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1809"/>
        <source>Midi Files per Track</source>
        <translation>Midi Dateien per Tracks</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1810"/>
        <source>Lisalo (Tracks per Channel)</source>
        <translation>Lisalo (Tracks per Channel)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1811"/>
        <source>Ardour</source>
        <translation>Ardour</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1812"/>
        <source>Channel Change Relative</source>
        <translation>Channel Wechsel relativ </translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1813"/>
        <source>Channel Change Absolute</source>
        <translation>Channel Wechsel absolut</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1814"/>
        <source>SM</source>
        <translatorcomment>Solo and Mute off. This is an icon. Don&apos;t translate.</translatorcomment>
        <translation>SM</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1815"/>
        <source>Unset all solo and mute</source>
        <translation>Setze alle Solo- und stummen Tracks zurück</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1816"/>
        <source>Lisalo (Tracks per Ports)</source>
        <translation>Lisalo (Tracks per Port)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1817"/>
        <source>Terminal</source>
        <translation>Terminal</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1818"/>
        <source>Small Terminal</source>
        <translation>Mini Terminal</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1819"/>
        <source>Chord Duration Factor +</source>
        <translation>Akkord &quot;Duration Factor&quot; +</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1820"/>
        <source>Chord Duration Factor -</source>
        <translation>Akkord &quot;Duration Factor&quot; -</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1821"/>
        <source>Chord Duration Factor Reset</source>
        <translation>Setz Akkord &quot;Duration Factor&quot; zurück </translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1822"/>
        <source>Tempo Change Relative</source>
        <translation>Relativer Tempowechsel</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1823"/>
        <source>Chord Velocity Factor +</source>
        <translation>Akkord &quot;Velocity Factor&quot; +</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1824"/>
        <source>Chord Velocity Factor -</source>
        <translation>Akkord &quot;Velocity Factor&quot; .-</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1825"/>
        <source>Chord Velocity Factor Reset</source>
        <translation>Setze Akkord &quot;Velocity Factor&quot; zurück </translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1826"/>
        <source>Toggle Extended Track View</source>
        <translation>Klappe extra Hilfslinien aus (nicht im Druck)</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1438"/>
        <source>Other Lilypond</source>
        <translation>Weiteres Lilypond</translation>
    </message>
    <message>
        <location filename="gui_main.py" line="1827"/>
        <source>Convert Single Voice Track to autochange Piano-Staff</source>
        <translation>Wandle Einzelstimmen-Track in automatisches Pianosystem um (nur Druck)</translation>
    </message>
</context>
<context>
    <name>musicTab</name>
    <message>
        <location filename="gui_musictab.py" line="502"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="503"/>
        <source>Lyrics</source>
        <translation>Gesangstext</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="504"/>
        <source>An empty line means a new verse. Shift+Space for &quot; -- &quot; syllabe connector. Underscore _ to skip a position.</source>
        <translation>Leerzeile startet neue Strophe. Shift+Leertaste für &quot; -- &quot; Silbentrenner. Unterstrich _ um eine Position zu überspringen. </translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="505"/>
        <source>Track</source>
        <translation>Track</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="506"/>
        <source>Print</source>
        <translation>Druck</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="507"/>
        <source>Track ID</source>
        <translation>Track ID</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="508"/>
        <source>Trackname</source>
        <translation>Trackname</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="509"/>
        <source>Track Group</source>
        <translation>Track Gruppe</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="510"/>
        <source>Instrument Name</source>
        <translation>Instrument</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="511"/>
        <source>Short Name</source>
        <translation>Instr. kurz</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="512"/>
        <source>Voice Preset</source>
        <translation>Stimmenauswahl</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="513"/>
        <source>Transposition</source>
        <translation>Transponieren</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="514"/>
        <source>Size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="515"/>
        <source>Number of Lines</source>
        <translation>Linienanzahl</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="516"/>
        <source>Merge w/ Above</source>
        <translation>ist Unterstimme</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="517"/>
        <source>Lyrics Above</source>
        <translation>Text obendrüber</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="518"/>
        <source>Don&apos;t Print</source>
        <translation>Nicht drucken</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="519"/>
        <source>Ambitus</source>
        <translation>Ambitus</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="520"/>
        <source>Edit Directives</source>
        <translation>Bearbeite Direktiven</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="521"/>
        <source>Solo</source>
        <translation>Solo</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="522"/>
        <source>Mute</source>
        <translation>Stumm</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="523"/>
        <source>Simple Playback</source>
        <translation>Simple Playback</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="524"/>
        <source>Instrument/Patch</source>
        <translation>Instrument/Patch</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="525"/>
        <source>Channel</source>
        <translation>Channel</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="526"/>
        <source>Midi Channel</source>
        <translation>Midi Channel</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="535"/>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="536"/>
        <source>Pan</source>
        <translation>Pan</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="537"/>
        <source>Transp.</source>
        <translation>Transp.</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="538"/>
        <source>Bank</source>
        <translation>Bank</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="531"/>
        <source>Jack Playback</source>
        <translation>Jack Playback</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="532"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="533"/>
        <source>Channel Range</source>
        <translation>Channel von-bis</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="534"/>
        <source>Patch</source>
        <translation>Patch</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="539"/>
        <source>JackL</source>
        <translation>JackL</translation>
    </message>
    <message>
        <location filename="gui_musictab.py" line="540"/>
        <source>JackR</source>
        <translation>JackR</translation>
    </message>
</context>
</TS>
