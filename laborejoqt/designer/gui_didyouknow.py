# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui_didyouknow.ui'
#
# Created: Tue May 28 19:21:07 2013
#      by: PyQt4 UI code generator 4.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_DidYouKnow(object):
    def setupUi(self, DidYouKnow):
        DidYouKnow.setObjectName(_fromUtf8("DidYouKnow"))
        DidYouKnow.resize(374, 436)
        DidYouKnow.setMinimumSize(QtCore.QSize(374, 436))
        DidYouKnow.setMaximumSize(QtCore.QSize(374, 436))
        self.verticalLayout = QtGui.QVBoxLayout(DidYouKnow)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.widget = QtGui.QWidget(DidYouKnow)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy)
        self.widget.setMinimumSize(QtCore.QSize(0, 151))
        self.widget.setMaximumSize(QtCore.QSize(16777215, 151))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout(self.widget)
        self.horizontalLayout_3.setMargin(0)
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.laborejologo = QtGui.QLabel(self.widget)
        self.laborejologo.setMinimumSize(QtCore.QSize(300, 151))
        self.laborejologo.setMaximumSize(QtCore.QSize(300, 151))
        self.laborejologo.setText(_fromUtf8(""))
        self.laborejologo.setAlignment(QtCore.Qt.AlignCenter)
        self.laborejologo.setObjectName(_fromUtf8("laborejologo"))
        self.horizontalLayout_3.addWidget(self.laborejologo)
        self.verticalLayout.addWidget(self.widget)
        self.layoutWidget = QtGui.QWidget(DidYouKnow)
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.formLayout = QtGui.QFormLayout(self.layoutWidget)
        self.formLayout.setContentsMargins(-1, -1, -1, 0)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.frame_2 = QtGui.QFrame(self.layoutWidget)
        self.frame_2.setMinimumSize(QtCore.QSize(0, 0))
        self.frame_2.setFrameShape(QtGui.QFrame.NoFrame)
        self.frame_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.frame_2.setLineWidth(0)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.frame_2)
        self.horizontalLayout.setMargin(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.numberLabel = QtGui.QLabel(self.frame_2)
        self.numberLabel.setObjectName(_fromUtf8("numberLabel"))
        self.horizontalLayout.addWidget(self.numberLabel)
        self.numberSlider = QtGui.QSlider(self.frame_2)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.numberSlider.sizePolicy().hasHeightForWidth())
        self.numberSlider.setSizePolicy(sizePolicy)
        self.numberSlider.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.numberSlider.setOrientation(QtCore.Qt.Horizontal)
        self.numberSlider.setObjectName(_fromUtf8("numberSlider"))
        self.horizontalLayout.addWidget(self.numberSlider)
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.frame_2)
        self.groupBox = QtGui.QGroupBox(self.layoutWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.groupBox.setMinimumSize(QtCore.QSize(0, 150))
        self.groupBox.setMaximumSize(QtCore.QSize(16777215, 150))
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.groupBox)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.didyouknowLabel = QtGui.QLabel(self.groupBox)
        self.didyouknowLabel.setWordWrap(True)
        self.didyouknowLabel.setOpenExternalLinks(True)
        self.didyouknowLabel.setTextInteractionFlags(QtCore.Qt.LinksAccessibleByMouse|QtCore.Qt.TextSelectableByMouse)
        self.didyouknowLabel.setObjectName(_fromUtf8("didyouknowLabel"))
        self.verticalLayout_2.addWidget(self.didyouknowLabel)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.groupBox)
        self.verticalLayout.addWidget(self.layoutWidget)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setContentsMargins(-1, 0, -1, -1)
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.showOnStartup = QtGui.QCheckBox(DidYouKnow)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.showOnStartup.sizePolicy().hasHeightForWidth())
        self.showOnStartup.setSizePolicy(sizePolicy)
        self.showOnStartup.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.showOnStartup.setChecked(True)
        self.showOnStartup.setObjectName(_fromUtf8("showOnStartup"))
        self.horizontalLayout_2.addWidget(self.showOnStartup)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.closeButton = QtGui.QDialogButtonBox(DidYouKnow)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.closeButton.sizePolicy().hasHeightForWidth())
        self.closeButton.setSizePolicy(sizePolicy)
        self.closeButton.setFocusPolicy(QtCore.Qt.WheelFocus)
        self.closeButton.setOrientation(QtCore.Qt.Horizontal)
        self.closeButton.setStandardButtons(QtGui.QDialogButtonBox.Close)
        self.closeButton.setCenterButtons(False)
        self.closeButton.setObjectName(_fromUtf8("closeButton"))
        self.horizontalLayout_2.addWidget(self.closeButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.donationLabel = QtGui.QLabel(DidYouKnow)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.donationLabel.sizePolicy().hasHeightForWidth())
        self.donationLabel.setSizePolicy(sizePolicy)
        self.donationLabel.setTextFormat(QtCore.Qt.RichText)
        self.donationLabel.setWordWrap(True)
        self.donationLabel.setOpenExternalLinks(True)
        self.donationLabel.setTextInteractionFlags(QtCore.Qt.LinksAccessibleByMouse|QtCore.Qt.TextSelectableByMouse)
        self.donationLabel.setObjectName(_fromUtf8("donationLabel"))
        self.verticalLayout.addWidget(self.donationLabel)

        self.retranslateUi(DidYouKnow)
        QtCore.QObject.connect(self.closeButton, QtCore.SIGNAL(_fromUtf8("accepted()")), DidYouKnow.accept)
        QtCore.QObject.connect(self.closeButton, QtCore.SIGNAL(_fromUtf8("rejected()")), DidYouKnow.reject)
        QtCore.QMetaObject.connectSlotsByName(DidYouKnow)

    def retranslateUi(self, DidYouKnow):
        DidYouKnow.setWindowTitle(_translate("DidYouKnow", "Laborejo - Welcome", None))
        self.numberLabel.setText(_translate("DidYouKnow", "Trick 0/10", None))
        self.groupBox.setTitle(_translate("DidYouKnow", "Did you know…?", None))
        self.didyouknowLabel.setText(_translate("DidYouKnow", "This is an example text. If it gets too long you get line breaks.", None))
        self.showOnStartup.setText(_translate("DidYouKnow", "show on startup", None))
        self.donationLabel.setText(_translate("DidYouKnow", "<html><head/><body><p align=\"center\">Support Laborejo by donating: <a href=\"http://www.laborejo.org/Donation\"><span style=\" text-decoration: underline; color:#0000ff;\">http://www.laborejo.org/Donation</span></a></p></body></html>", None))

