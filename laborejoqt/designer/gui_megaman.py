# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui_megaman.ui'
#
# Created: Thu Jun 28 18:31:52 2012
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(761, 666)
        self.gridLayout = QtGui.QGridLayout(Form)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.recentFile4 = QtGui.QPushButton(Form)
        self.recentFile4.setFlat(True)
        self.recentFile4.setObjectName(_fromUtf8("recentFile4"))
        self.gridLayout.addWidget(self.recentFile4, 3, 3, 1, 1)
        self.newFile = QtGui.QPushButton(Form)
        self.newFile.setFlat(True)
        self.newFile.setObjectName(_fromUtf8("newFile"))
        self.gridLayout.addWidget(self.newFile, 0, 1, 1, 1)
        self.recentFile2 = QtGui.QPushButton(Form)
        self.recentFile2.setFlat(True)
        self.recentFile2.setObjectName(_fromUtf8("recentFile2"))
        self.gridLayout.addWidget(self.recentFile2, 3, 1, 1, 1)
        self.recentFile1 = QtGui.QPushButton(Form)
        self.recentFile1.setFlat(True)
        self.recentFile1.setObjectName(_fromUtf8("recentFile1"))
        self.gridLayout.addWidget(self.recentFile1, 3, 0, 1, 1)
        self.openFile = QtGui.QPushButton(Form)
        self.openFile.setFlat(True)
        self.openFile.setObjectName(_fromUtf8("openFile"))
        self.gridLayout.addWidget(self.openFile, 0, 2, 1, 1)
        self.recentFile3 = QtGui.QPushButton(Form)
        self.recentFile3.setFlat(True)
        self.recentFile3.setObjectName(_fromUtf8("recentFile3"))
        self.gridLayout.addWidget(self.recentFile3, 3, 2, 1, 1)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(QtGui.QApplication.translate("Form", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.recentFile4.setText(QtGui.QApplication.translate("Form", "recent4", None, QtGui.QApplication.UnicodeUTF8))
        self.newFile.setText(QtGui.QApplication.translate("Form", "New", None, QtGui.QApplication.UnicodeUTF8))
        self.recentFile2.setText(QtGui.QApplication.translate("Form", "recent2", None, QtGui.QApplication.UnicodeUTF8))
        self.recentFile1.setText(QtGui.QApplication.translate("Form", "recent1", None, QtGui.QApplication.UnicodeUTF8))
        self.openFile.setText(QtGui.QApplication.translate("Form", "Open", None, QtGui.QApplication.UnicodeUTF8))
        self.recentFile3.setText(QtGui.QApplication.translate("Form", "recent3", None, QtGui.QApplication.UnicodeUTF8))

