# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
from operator import attrgetter #for sorting, max, min with a key.

import math, re
from hashlib import md5
import laborejocore as api
from laborejoqt import config
from laborejoqt import dictionaries
from laborejoqt.constants  import *

def stringToColor(st):
    """Convert a string to QColor. Same string, same color
    Is used for group coloring"""
    if st:
        c = md5(st.encode()).hexdigest()
        return QtGui.QColor(int(c[0:9],16) % 255, int(c[10:19],16) % 255, int(c[20:29],16)% 255, 255)
    else:
        return QtGui.QColor(255,255,255,255) #Return White

class GuiGlyph(QtGui.QGraphicsTextItem):
    """A non-standalone glyph to use in GuiItem"""
    def __init__(self, text='\u25C8', shift = MUSIC_SCALE_CORRECTION, fontScale = 1, pitch = MIDDLE_LINE):
        super(GuiGlyph, self).__init__(text)
        self.pitch = pitch
        self.font = fontDB.font(MUSIC_FONT_STRING, "", FONT_SIZE * fontScale)
        self.font.setPixelSize(FONT_SIZE * fontScale * 1.33) #It is very important to set the pixel size before setting the font to self
        self.setFont(self.font)
        self.shift = shift  #Shift up and down for misaligned glyphs in a font like uft8 noteheads.
        self.setDefaultTextColor(QtGui.QColor("black"))
        self.setOpacity(1)

class GuiText(QtGui.QGraphicsTextItem):
    """A non-standalone glyph to use in GuiItem"""
    def __init__(self, text='x', shift = 0, fontScale = 1, pitch = MIDDLE_LINE):
        super(GuiText, self).__init__(text)
        self.pitch = pitch
        self.font = fontDB.font(TEXT_FONT_STRING, "", FONT_SIZE / 2 * fontScale)
        self.font.setPixelSize(FONT_SIZE / 2 * fontScale) #It is very important to set the pixel size before setting the font to self
        self.setFont(self.font)
        self.shift = TEXT_SCALE_CORRECTION  #Shift up and down for misaligned glyphs in a font like uft8 noteheads.
        self.setDefaultTextColor(QtGui.QColor("black"))
        self.setOpacity(1)

class GuiNote(QtGui.QGraphicsTextItem):
    """A single graphical note to use in GuiItem.
    Does not have update, only init.
    Every notehead change is delete/recreate """
    def __init__(self, backendNote, chordParent):
        super(GuiNote, self).__init__(dictionaries.dur2notehead[chordParent.cachedBaseDuration])
        self.font = fontDB.font(MUSIC_FONT_STRING, "", FONT_SIZE * 1.5)
        self.font.setPixelSize(FONT_SIZE * 2) #It is very important to set the pixel size before setting the font to self
        self.setFont(self.font)
        self.data = backendNote
        self.shift = MUSIC_SCALE_CORRECTION
        self.translate(-3, -1)
        self.durationFactor = chordParent.data.durationFactor
        if self.durationFactor > 1: #to the right
            self.rectD = QtGui.QGraphicsRectItem(0, 0,  15*self.durationFactor -10, 3)
            self.rectD.setParentItem(self)
            self.rectD.setPos(20,63) #coming out of the notehead
            self.rectD.setBrush(QtGui.QColor("red"))
        elif self.durationFactor < 1:
            self.rectD = QtGui.QGraphicsRectItem(0, 0, -15*(2-self.durationFactor) +10 , 3) #backwards. Factor cannot go below 0.1.
            self.rectD.setParentItem(self)
            self.rectD.setPos(10,63) #coming out of the notehead
            self.rectD.setBrush(QtGui.QColor("red"))

        self.velocityFactor = chordParent.data.velocityFactor
        if self.velocityFactor > 1: #to the right
            self.rectV = QtGui.QGraphicsRectItem(0, 0,  15*self.velocityFactor -10, 3)
            self.rectV.setParentItem(self)
            self.rectV.setPos(20,66) #coming out of the notehead
            self.rectV.setBrush(QtGui.QColor("yellow"))
        elif self.velocityFactor < 1:
            self.rectV = QtGui.QGraphicsRectItem(0, 0, -15*(2-self.velocityFactor) +10 , 3) #backwards. Factor cannot go below 0.1.
            self.rectV.setParentItem(self)
            self.rectV.setPos(10,66) #coming out of the notehead
            self.rectV.setBrush(QtGui.QColor("yellow"))

        self.setDefaultTextColor(QtGui.QColor("black"))
        self.setOpacity(1)
        self.chord = chordParent #What chord am I in?
        self.dots = None
        self.usedAccidental = None
        self.cachedPitch = self.data.pitch
        self.finger = None
        self.righthandfinger = None
        self.stringnumber = None
        self.tie = None
        self.accidental()
        self.fingering()
        self.rightHandFingering()
        self.stringNumber()
        self.tieF()

    def fingering(self):
        """Update finger, right hand and string number for this note"""
        if self.data.finger:
            if not self.finger: #the first time.
                self.finger = GuiText(self.data.finger)
            else:
                self.finger.setPlainText(self.data.finger)
            self.finger.setParentItem(self)
            self.finger.setPos(20,50) #position right of the notehead. May collide with the notes right of it, but who cares...
            self.finger.setDefaultTextColor(QtGui.QColor("green"))
            self.finger.setZValue(7)

    def rightHandFingering(self):
        if "RightHandFinger" in self.data.directivePst:
            glyph = self.data.directivePst["RightHandFinger"].split("#")[-1]
            if not self.righthandfinger: #the first time.
                self.righthandfinger = GuiText(glyph, fontScale = 1)
            else:
                self.righthandfinger.setPlainText(glyph)
            self.righthandfinger.setParentItem(self)
            self.righthandfinger.setDefaultTextColor(QtGui.QColor("red"))
            self.righthandfinger.setPos(0,TRACK_SIZE / 2)

    def stringNumber(self):
        if "StringNumber" in self.data.directivePst:
            glyph = int(self.data.directivePst["StringNumber"].split("\\")[-1]) + 0x2460 - 1 #2460 in hex cicled 1 in unicode
            glyph = chr(glyph)
            if not self.stringnumber: #the first time.
                self.stringnumber = GuiText(glyph, fontScale = 1)
            else:
                self.stringnumber.setPlainText(glyph)
            self.stringnumber.setParentItem(self)
            self.stringnumber.setDefaultTextColor(QtGui.QColor("red"))
            self.stringnumber.setPos(0,TRACK_SIZE / 2)

    def tieF(self):
        if self.data.tie:
            if not self.tie: #the first time.
                self.tie = GuiText("~")
            else:
                self.tie.setPlainText("~")
            self.tie.setParentItem(self)
            self.tie.setScale(2)
            self.tie.setPos(20,40) #position right of the notehead. May collide with the notes right of it, but who cares...
            #self.tie.setDefaultTextColor(QtGui.QColor("black"))
            self.tie.setZValue(7)

    def accidental(self):
        """The accidentals are style independet or "Twelve-Tonal".
        If the note is in the keysig it has no accidental.
        If it is not in the keysig it gets an absolute accidental,
        even if it already was in the same measure."""

        scaleStat = self.data.scaleStatus(self.chord.cachedKeysigData)
        #Scalesat:
        #[0] = Pitchstatus. Difference to White, independent of keysig
        #[1] = Difference to keysig. feses in G-Major is -30
        #[2] = Keysig version. Complementary to [0], shows always the same number no matter which accidental

        if scaleStat[1] == 0: #no difference to keysig
            accidentalString = ""
        elif scaleStat[1] + scaleStat[2] == 0:
            accidentalString = "\u266E"
        else:
            diff = scaleStat[0]
            accidentalString = dictionaries.keysigDiff2accidental[diff]

        extraString = ""
        if "accidental" in self.data.directivePst:
            if self.data.directivePst["accidental"] == "!":
                extraString = "\uFE57" #this is a unicode small exclamation mark ﹗
            elif self.data.directivePst["accidental"] == "?":
                extraString = "\uFE56" #this is a unicode small question mark ﹖
            else:
                warnings.warning("Unrecognized accidental tag/string:", self.data.directivePst["accidental"])

        minusSpace = 8
        if accidentalString:  #Only if this needs an accidental.
            self.usedAccidental = GuiGlyph(accidentalString, fontScale = 1.2)
            self.usedAccidental.setParentItem(self)
            self.usedAccidental.setPos(-8, 12)
            minusSpace = 0

        if extraString:
            self.extraAccidental = GuiGlyph(extraString, fontScale = 0.7)
            self.extraAccidental.setDefaultTextColor(QtGui.QColor("darkred"))
            self.extraAccidental.setParentItem(self)
            self.extraAccidental.setPos(-20 + minusSpace, 37)

class GuiItem(QtGui.QGraphicsItemGroup):
    #class GuiItem(QtGui.QGraphicsItem):
    #http://www.riverbankcomputing.co.uk/static/Docs/PyQt4/html/qgraphicsitemgroup.html#details
    """Base item group. Can hold several glyphs."""
    def __init__(self, backendItem, displacement = 0, mainGlyph = None, mainGlyphShift = 0, fontScale = 1, forceSigs = None, parentTrack = None): #forceSigs is important for non-cursor input like Container
        super(GuiItem, self).__init__()
        self.breakClefLoop = False #instead of typesetting this is a break marker when track.clefUpdate() runs.
        self.breakKeysigLoop = False #instead of typesetting this is a break marker when track.keysigUpdate() runs.
        self.breakTimesigLoop = False #instead of typesetting this is a break marker when track.timesigUpdate() runs. Additionaly this is used by timesig index generation in firstPass
        self.breakContainerLoop = False #instead of typesetting this is an indicator.
        self.displacement = displacement
        self.data = self.dataOrg = backendItem
        self.noteheads = [] #prevent typechecking.
        #self.setHandlesChildEvents(False) #A mouseclick will select the whole Group, not a child.
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable)
        #self.mouseReleaseEvent =   #Override pyqt command
        self.mainGlyphObject = None
        if mainGlyph:
            mainGlyphItem = GuiGlyph(text = mainGlyph, pitch = MIDDLE_LINE, fontScale = fontScale)
            mainGlyphItem.setPos(0, -15 + mainGlyphShift)
            self.addToGroup(mainGlyphItem)
            self.mainGlyphObject = mainGlyphItem
        self.setZValue(5) #just setting the z value of the children if needed does not work.
        self.cachedDuration = backendItem.duration # later it is self.data.duration
        self.cachedBaseDuration = backendItem.base
        self.cachedTickIndex = 0 #Last known tickindex. This is not an init parameter because the tickindex is best known by track.insert. So it is set in insert, when the GuiItem was already created. And it is technically correct because on creation it has no position in a track and so it has no tickindex.
        if forceSigs: # [track, clefPitch, backendKeysig, backendTimesig]
            self.track = forceSigs[0]
            self.cachedClefPitch = forceSigs[1]
            self.cachedKeysigData = forceSigs[2]
            self.cachedTimesigData = forceSigs[3]
        else: #insert on the cursor position. Safely use the backend data and wait for track.insert() to set the rest.
            self.track = parentTrack #I am in that track. This is always unique. Even cut and paste in the backend is a delete and insert new in Laborejo. But it only valid from track.insert on because on creation it has no position in a track. So it is set in track.insert
            nowClef = api._getClef() #only for init. On item creation/insert.
            self.cachedClefPitch = dictionaries.clefs[nowClef.clefString + nowClef.octave][1] #Only for GuiChord. But prevents typechecking on update. Just save the number value. Will be much faster than accessing an instance var each time. Will also be much faster when updating a whole track if a clef is changed in front. It just needs to get the number once and then save it in any GuiChord item.
            self.cachedKeysigData = api._getKeysig() #A Backend keysig.
            self.cachedTimesigData = api._getTimesig() #A Backend timesig

        self.cachedMinMax = None #Is updated in self.update() Highest and lowest note pitch. Changes only in self.update()
        if self.cachedTimesigData.denominator <= 192: #for example 3/8
            self.cachedTimesigMod = self.cachedTimesigData.nominator / 2
        else:
            self.cachedTimesigMod = 1
        self.cachedGroupStemExtremePitches = [] #This is the flag if this is a beamgroup member. obviously only for GuiChord. Has the lowest[0] and highest[1] pitch of that group.
        self.group = 0 #Part of a beaming group? Only for chords, but it will be tested for all items while drawing.   #0 means no. 1=groupMember, 2= groupStart, 3=groupEnd.
        self.lastPassWasGroup = False #addition to self.group. Indicates if the stem/flag needs updating at all. if group and lastPassWasGroup are both false there is not even the need to delete and redraw the stem.
        self.widthDependentOnDisplacement = False  #widthDependentOnDisplacement is for intransparent containers that need to change if a disp. item is in the other track.

        self.hiddenCrossed = None #A line marker to indicate that this item is hidden

        #Extensions are child items.
        #DynamicDynamic extensions need updating when the pitch/clef changes.

        #Static extensions only need updating when they change. Very performant. That means they may collide but who cares, my duty ends here.
        self.tuplets = [] #static
        self.chordGlyph = None #static
        self.channelGlyph = None #static
        self.programGlyph = None #static
        self.triggerGlyph = None # static
        self.guiDirectivesPst = [] #static
        self.figuresGlyph = None #static
        self.tremoloGlyph = None #static (see GuiItem)
        self.substitutionGlyph = None #static (see GuiItem)

        self.aboveStatics = []
        self.belowStatics = []

        if not type(self) is GuiChord:
            self.extensions() #on load create all the stuff. This is partly redundant with chord.update()

        self.updateVisibility()

    def updateVisibility(self):
        """The backend can be normal, hidden or transparent"""
        if self.data.hidden:
            if self.hiddenCrossed:
                self.scene().removeItem(self.hiddenCrossed)
                self.scene().removeItem(self.hiddenCrossed2)
                self.hiddenCrossed = None
                self.hiddenCrossed2 = None

            line = QtGui.QGraphicsLineItem(QtCore.QLineF(0, TRACK_SIZE/2.1, 0, 0 ))
            line2 = QtGui.QGraphicsLineItem(QtCore.QLineF(0, TRACK_SIZE/2.1, 0, 0 ))

            line.setParentItem(self)
            line2.setParentItem(self)

            line.rotate(-20)
            line.setPos(3, -3)
            line.setPen(PEN_LINEFORBIDDEN)

            line2.rotate(20)
            line2.setPos(23, 5)
            line2.setPen(PEN_LINEFORBIDDEN)

            self.hiddenCrossed = line
            self.hiddenCrossed2 = line2

        else:
            if self.hiddenCrossed:
                self.scene().removeItem(self.hiddenCrossed)
                self.scene().removeItem(self.hiddenCrossed2)
                self.hiddenCrossed = None
                self.hiddenCrossed2 = None

        if self.data.transparent:
            self.setOpacity(0.5)
        else:
            self.setOpacity(1)

    def verticalLine(self):
        line_vertical = QtGui.QGraphicsLineItem(QtCore.QLineF(0, 30, 0, 0))
        line_vertical.setPen(PEN_POSITION)
        line_vertical.setParentItem(self)
        line_vertical.setPos(12, 5)
        self.addToGroup(line_vertical)


    def lineUpAboveStatics(self):
        offset = -TRACK_SIZE/4

        #the noteheads are sorted because the backend notesheads are sorted
        #and the GUI creates them one after another with a for x in list loop.
        #So the last notehead is the highest one.
        if self.noteheads:
            startY = self.noteheads[-1].pos().y()/3
        else:
            startY = 0
        for x in self.aboveStatics:
            if x: #not None
                x.setPos(5, startY + offset)
                #print (x.shape().controlPointRect().height())
                offset -= 8


    def lineUpBelowStatics(self):
        offset = TRACK_SIZE/2
        for x in self.belowStatics:
            if x: #not None
                x.setPos(0, offset)
                offset += 10

    def updateRightFromThisItem(self):
        """Calls the parent track to update the x positions"""
        self.track.updateRightFromTickindex(self.cachedTickIndex, forceDisplacementUpdatingOtherTracks = self.displacement)
        #self.track.updateRightFromStart(forceDisplacementUpdatingOtherTracks = self.displacement)

    def FirstPassRightFromThisItem(self):
        self.track.firstPassRightFromTickindex(self.cachedTickIndex)

    def getFirstNote(self):
        return self.data.notelist[0]

    def yFormulaPart1(self, pitch, clefPitch):
        return -1 * (pitch / PITCH_MOD) + clefPitch

    def yFormulaPart2(self, part1):
        return part1 + PITCH_MOD/2  #PM/2 is half of a glyph size. Normally glyphs would bottom align on the Y coordinate/staff-line so we shift half of the glyph so we have the glyph in the middle.

    def getY(self, guiNote, clefPitch):
        """Return the pitch of the complete Item.
        Can handle both a guiNote or a backendNote"""
        part1 = MIDDLE_LINE #If there is no note, for example a clef, the base line is the middle line.
        try:
            data = divmod(guiNote.data.pitch, 50)[0] * 50 + 20   #use white version of the pitch
        except:
            data = divmod(guiNote.pitch, 50)[0] * 50 + 20
        if self.getFirstNote().pitch:  #The backend.EmptyNote has 0 as pitch.
            part1 = self.yFormulaPart1(data, clefPitch)
        return self.yFormulaPart2(part1)

    def maxminFromNotelist(self, clefPitch):
        """It is important to save the highest and lowest note/pitch in
        the notelist. First because we want to know if stem up or down
        (compare how far away both notes are from the middle line.
        Second for ledger lines"""
        notelist = self.data.notelist #it is always ordered.

        maxi = notelist[-1].pitch
        mini = notelist[0].pitch

        if maxi == 20:
            return (0, 0)
        else:
            return (self.yFormulaPart2(self.yFormulaPart1(mini, clefPitch)) , self.yFormulaPart2(self.yFormulaPart1(maxi, clefPitch)))

    def hasMiddleLine(self, clefPitch):
        """Return true if this chord has a middle line position.
        c' in treble clef."""
        return 25 in [self.yFormulaPart2(self.yFormulaPart1(note.pitch, clefPitch)) for note in self.data.notelist]

    def updateDynamic(self):
        """Redraw only parts that move on the Y axis.
        Only chords and the keysig need this currently because they
        depend on the clef position.
        If your item needs a clef, use this."""
        self.cachedDuration = self.data.duration
        self.cachedBaseDuration = self.data.base
        if self.cachedTimesigData.denominator <= 192: #for example 3/8
            self.cachedTimesigMod = self.cachedTimesigData.nominator / 2
        else:
            self.cachedTimesigMod = 1

    def updateStatic(self):
        """Redraw the part of a guiItem which changes only seldomly.
        Like the displayed name of a performance sig.
        This is the "leftover" signal for anything which has no signal
        of their own. FiguredBass and ChordSymbol have."""
        pass

    def extensions(self):
        """Item specific optional. Reimplement if needed.
        Call super() first.
        This is for items which do NOT change the position
        if the pitch changes.
        Normally the extensions are functions of their own and this
        is just a central place to call them all at once for __init__
        and occassions when the item needs to be forcefully reset.
        If something is an extension it most likely has its
        own signal already. This is really only for load
        or special cases. Don't mis-use."""
        self.chordSymbols()
        self.tupletMarkers()
        self.directivesPst()
        self.figuredBass()
        self.tremoloMarker()
        self.substitutionMarker()
        self.smfTrigger()

    def update(self):
        """Redraw this item.
        This is the complete graphic update, even if unnecessarry.
        Avoid using it. """
        self.updateDynamic()
        self.updateStatic()
        self.extensions()
        self.setSelected(self.data.selected)

    def updateForIteration(self):
        """Super expensive function.
        Only for extreme special cases. Will be called for EVERY
        object after EVERY delete/insert/modification.
        In 99.9% of the cases this is just a pass function."""
        pass


    def tupletMarkers(self):
        if self.data.tuplets:
            if not self.tuplets: #the first time.
                self.tuplets = GuiText("") #just prepare
                self.tuplets.setParentItem(self)
                self.aboveStatics.append(self.tuplets)

            t = self.data.tuplets
            if t == [[2,3]]:
                marker = "3"
                #marker = "⅔" #triplet
            elif t == [[4,6]]:
                marker = "6"
            elif t == [[4,5]]:
                marker = "⅘"
            elif t == [[2,5]]:
                marker = "⅖"
            else: #build our own fraction
                marker = re.sub(r'\[|\]|\(|\)', "", str(self.data.tuplets).replace("),", ""))
                marker = marker.replace(", ", "/")
            self.tuplets.setPlainText(marker)
            self.lineUpAboveStatics()

        elif self.tuplets: #There was one before. We had a delete
            self.aboveStatics.remove(self.tuplets)
            self.scene().removeItem(self.tuplets)
            self.tuplets = None

    def figuredBass(self):
        if self.data.figures:
            if not self.figuresGlyph: #the first time.
                self.figuresGlyph = GuiText(self.data.figures.replace(' ', ','))
                self.figuresGlyph.setParentItem(self)
                self.belowStatics.append(self.figuresGlyph)
            else:
                self.figuresGlyph.setPlainText(self.data.figures.replace(' ', ','))
            self.lineUpBelowStatics()
            #self.figuresGlyph.setPos(0,TRACK_SIZE / 1.5)
        elif self.figuresGlyph: #There was one before. We had a delete/reset to 1.
            self.belowStatics.remove(self.figuresGlyph)
            self.scene().removeItem(self.figuresGlyph)
            self.figuresGlyph = None

    def chordSymbols(self):
        if self.data.chordsymbols:
            if not self.chordGlyph: #the first time.
                self.chordGlyph = GuiText(self.data.chordsymbols, fontScale = 1.5)
                self.chordGlyph.setParentItem(self)
                self.aboveStatics.append(self.chordGlyph)
            else:
                self.chordGlyph.setPlainText(self.data.chordsymbols)
            self.lineUpAboveStatics()
        elif self.chordGlyph: #There was one before. We had a delete
            self.aboveStatics.remove(self.chordGlyph)
            self.scene().removeItem(self.chordGlyph)
            self.chordGlyph = None

    def tremoloMarker(self):
        if self.data.split > 1:
            if not self.tremoloGlyph: #the first time.
                self.tremoloGlyph = GuiText("t" + str(self.data.split))
                self.belowStatics.append(self.tremoloGlyph)
                self.tremoloGlyph.setParentItem(self)
                self.tremoloGlyph.setDefaultTextColor(QtGui.QColor("blue"))
            else:
                self.tremoloGlyph.setPlainText("t" + str(self.data.split))
            self.lineUpBelowStatics()
            #self.tremoloGlyph.setPos(3, TRACK_SIZE / 1.6)

        elif self.tremoloGlyph: #There was one before. We had a delete/reset to 1.
            self.belowStatics.remove(self.tremoloGlyph)
            self.scene().removeItem(self.tremoloGlyph)
            self.tremoloGlyph = None

    def substitutionMarker(self):
        if self.data.substitution:
            string = "".join(self.data.substitution.split("_")[-1]).title()
            if not self.substitutionGlyph: #the first time.
                self.substitutionGlyph = GuiText(string)
                self.belowStatics.append(self.substitutionGlyph)
                self.substitutionGlyph.setParentItem(self)
                self.substitutionGlyph.setDefaultTextColor(QtGui.QColor("red"))
            else:
                self.substitutionGlyph.setPlainText(string)
            self.lineUpBelowStatics()
        elif self.substitutionGlyph: #There was one before. We have a delete.
            self.belowStatics.remove(self.substitutionGlyph)
            self.scene().removeItem(self.substitutionGlyph)
            self.substitutionGlyph = None

    def smfTrigger(self):
        #start gui drawing
        if self.data.triggerString:
            if not self.triggerGlyph: #the first time.
                self.triggerGlyph = GuiText("trg")
                self.triggerGlyph.setParentItem(self)
                self.belowStatics.append(self.triggerGlyph)
                self.triggerGlyph.setDefaultTextColor(QtGui.QColor("blue"))
            else:
                self.triggerGlyph.setPlainText("trg")
            self.lineUpBelowStatics()

        elif self.triggerGlyph: #There was one before. We had a delete
            self.belowStatics.remove(self.triggerGlyph)
            self.scene().removeItem(self.triggerGlyph)
            self.triggerGlyph = None


    def directivesPst(self):
        """Markers like staccato, fermata and tenuto"""
        for pstGlyph in self.guiDirectivesPst:
            self.aboveStatics.remove(pstGlyph)
            if self.scene():
                self.scene().removeItem(pstGlyph)
        self.guiDirectivesPst = []

        for pstDirective in self.data.directivePst.items():
            txt = dictionaries.directives[pstDirective[0]]
            if txt:  #Could have returned "None" -> Unknown directive
                pstGlyph = GuiGlyph(txt)
                pstGlyph.translate(0, -15)
            else: #If unknown use the first four chars of the key
                if pstDirective[0] in ["beam"]:  #one of these tags?
                    pstGlyph = GuiText(pstDirective[1], ) #Use Lilypond instead of the tag string
                elif pstDirective[0][0:4] == "usr_":
                    pstGlyph = GuiText(pstDirective[1][0:4], ) #Use the directive
                    pstGlyph.rotate(-90)
                    pstGlyph.translate(-20, 0) #this is actually the Y-direction. It moves the glyph downwards.
                else:
                    pstGlyph = GuiText(pstDirective[0][0:4], ) #Use the key
                    pstGlyph.rotate(-90)
                    pstGlyph.translate(-20, 0) #this is actually the Y-direction. It moves the glyph downwards.

            pstGlyph.setParentItem(self)
            self.aboveStatics.append(pstGlyph)
            self.guiDirectivesPst.append(pstGlyph)
            self.lineUpAboveStatics()

    def updateDots(self):
        pass

    def updateGroupStemAndFlag(self):
        pass

    def smfChannel(self):
        pass

    def smfProgram(self):
        pass

    def allFingers(self):
        raise RuntimeError("called allfinger for non chord. Error in API call")

class GuiClef(GuiItem):
    def __init__(self, backendItem, glyph, pitch, offset, forceSigs = None, parentTrack = None):  #offset is in note-position. 1 means one step in the notelines/between up. -1 down.
        super(GuiClef, self).__init__(backendItem, displacement = DISPLACE, forceSigs = forceSigs, parentTrack = parentTrack)
        self.pitch = pitch
        self.breakClefLoop = True #instead of typesetting this is a break marker when track.clefUpdate() runs.
        self.mainGlyph = GuiGlyph(text = glyph, fontScale = 1.6)
        self.mainGlyph.setPos(0, -35 + (-1 * offset * PITCH_MOD / 2))
        self.addToGroup(self.mainGlyph)
        self.setPos(0, 0)

class GuiChord(GuiItem):
    """One GuiChord holds several GuiNotes. It makes it possible to combine notehead(s) with a stem(s), directives etc."""
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiChord, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.stem = None
        self.flags = []
        #The invis glyph is to make selection work.
        self.invis = GuiGlyph(text = " ")
        self.noteheads = [] #dynamic (see GuiItem)
        self.ledgerlines = [] #dynamic
        self.addToGroup(self.invis)
        self.update()

    def allFingers(self):
        for head in self.noteheads:
            head.fingering()
            head.rightHandFingering()
            head.stringNumber()

    def extensions(self):
        super(GuiChord, self).extensions()
        self.smfChannel()
        self.smfProgram()

    def updateLedgerLines(self):
        #Draw Ledger Lines
        for line in self.ledgerlines:
            self.scene().removeItem(line)
        self.ledgerlines = []

        start_under, start_above, lower_boundary, upper_boundary, offset_below, offset_above, middle_line_special = self.track.ledgerLinesDataSetCurrent

        nrOfLedgerLinesUnder = 0
        nrOfLedgerLinesAbove = 0
        #Determine if we need ledger lines.
        if self.cachedMinMax[0] >= lower_boundary:
            nrOfLedgerLinesUnder = math.floor(self.cachedMinMax[0] / PITCH_MOD - 1.4 + offset_below)
            if not nrOfLedgerLinesUnder:
                nrOfLedgerLinesUnder = 1
        if self.cachedMinMax[1] <= upper_boundary:
            nrOfLedgerLinesAbove = math.floor(-1 * self.cachedMinMax[1] / PITCH_MOD - 2.4 + offset_above)
            if not nrOfLedgerLinesAbove:
                nrOfLedgerLinesAbove = 1
        #Now drawing the lines. We always draw both, for chords which spread wide.
        for nr in range(nrOfLedgerLinesUnder):
            lines_under = QtGui.QGraphicsLineItem(QtCore.QLineF(0, start_under + nr * 10 , 22, start_under + nr * 10))
            lines_under.setPen(PEN_LINE)
            lines_under.setParentItem(self)
            lines_under.setPos(2,0)
            self.ledgerlines.append(lines_under)
        for nr in range(nrOfLedgerLinesAbove):
            lines_above = QtGui.QGraphicsLineItem(QtCore.QLineF(0, start_above - nr * 10 , 22, start_above - nr * 10))
            lines_above.setPen(PEN_LINE)
            lines_above.setParentItem(self)
            lines_above.setPos(2,0)
            self.ledgerlines.append(lines_above)

        if middle_line_special and self.hasMiddleLine(self.cachedClefPitch): #In case of dual view the middle line gets no ledger line. We create one.
            lines_middle = QtGui.QGraphicsLineItem(QtCore.QLineF(0, 50, 22, 50))
            lines_middle.setPen(PEN_LINE)
            lines_middle.setParentItem(self)
            lines_middle.setPos(2,0)
            self.ledgerlines.append(lines_middle)

    def updateDynamic(self):
        """Create the noteheads and accidentals.
        Each update all heads get deleted and recreated.
        The chord is responsible to set the Y position of the heads
        the head does not know what pitch it is, it is just a head.

        Attached are all things that move with the chord. E.g.
        everything that depends on the highest/lowest note.
        """
        super(GuiChord, self).updateDynamic()
        previousIterationNotePitch = (0, False)  #The first comparison later will be predictable this way.
        self.cachedMinMax = self.maxminFromNotelist(self.cachedClefPitch)
        #First delete all old noteheads
        scene = self.scene()
        for head in self.noteheads:
            scene.removeItem(head)
        self.noteheads = []

        #Now draw new noteheads
        for backendNote in self.data.notelist:
            head = GuiNote(backendNote, self)
            headY = self.getY(head, self.cachedClefPitch) + head.shift
            pitch = backendNote.pitch
            head.setParentItem(self)
            self.noteheads.append(head)
            if self.data.instanceCount > 1: #If this is a link mark it.
                head.setDefaultTextColor(QtGui.QColor("magenta"))

            if not previousIterationNotePitch[1] and pitch - previousIterationNotePitch[0] < 80: #The notes are too near too each other. Shift one to the Right if the last note was not already shifted.
                head.setPos(FONT_SIZE / 2, headY)
                previousIterationNotePitch = (pitch, True)
            else:
                head.setPos(0, headY)
                previousIterationNotePitch = (pitch, False)
        if not self.group:
            self.updateStandaloneStemAndFlag()
        else:  #if this is called by l_item.updateDynamic it does not reposition the beam or stem direction.. However, it keeps the stem attached, which is enough to look ok until the next updateRight.
            self.updateGroupStemAndFlag()
        self.updateDots()
        self.updateLedgerLines()

        self.resetTransform() #resets the <=96 translation
        if self.data.base <= 96:
            self.scale(0.9,0.9)
            self.translate(-3, 3)


    def updateDots(self):
        if self.data.dots:
            dotsString = "\U0001D16D\u2006" * self.data.dots   #2006 is 1/4 em space
            for head in self.noteheads:
                if head.dots:
                    head.scene().removeItem(head.dots) #remove the old one first.
                head.dots = GuiGlyph(dotsString, fontScale = 1.5)
                head.dots.setParentItem(head)
                head.dots.setPos(FONT_SIZE, 0)

    def getStemDirection(self, lowest, highest):
        """Returns True for right/up and False for left/down
        Higher is the negative direction. These are Qt coordinates.
        All values include the given boundaries:

        higher than -5: Stem left/down
        -4 to 55: Stem right/up.

        If dual view:
        56 to 114: left/down
        116 to inf: right/up
        """
        val = lowest + highest
        return -4 <= val <=55  or (val >= -5 and not self.track.dualView) or (self.track.dualView and val >= 116)

    def updateStandaloneStemAndFlag(self):
        """Does all the calculation if and what stem to draw."""
        if self.stem:
            self.scene().removeItem(self.stem)
            self.stem = None
        for flag in self.flags:
            self.scene().removeItem(flag)
            self.flags = []
        if self.group:
            raise ValueError(self, self.data, self.data.exportLilypond(), "is part of the beaming group", self.group , "but was instructed for a standalone drawing")

        #Draw stem
        if not 1536 <= self.cachedBaseDuration < 6144:  #not smaller than a whole note and not higher than a longa -> not for whole and breve.
            rightStemUp = self.getStemDirection(self.cachedMinMax[0], self.cachedMinMax[1])

            if rightStemUp: #right stem, up
                stem = QtGui.QGraphicsLineItem(QtCore.QLineF(0, self.cachedMinMax[0] + TRACK_SIZE/4, 0, self.cachedMinMax[1] - PITCH_MOD))
                stem.setPos(18, 0)
            else: #left stem, down
                stem = QtGui.QGraphicsLineItem(QtCore.QLineF(0 , self.cachedMinMax[1] + TRACK_SIZE/3.8 , 0  , self.cachedMinMax[0] + PITCH_MOD * 6 ))
                stem.setPos(7, 0)
            self.stem = stem
            stem.setPen(PEN_STEM)
            stem.setParentItem(self)

            #Draw Flag
            if self.cachedBaseDuration <= 192:
                flag = dictionaries.dur2flags[self.cachedBaseDuration]
                if flag:
                    flag = GuiGlyph(flag, MUSIC_SCALE_CORRECTION, 1.5)
                    flag.setParentItem(self)
                    self.flags.append(flag)
                    if rightStemUp: #right stem, up. flag normal, on the right side.
                        flag.setPos(17, self.cachedMinMax[1] - TRACK_SIZE/2)
                    else: #left stem, down. flag upside down but still to the right.
                        flag.setPos(6, self.cachedMinMax[0] + TRACK_SIZE)
                        trans = QtGui.QTransform()
                        trans.scale (1, -1)
                        flag.setTransform(trans)

    def updateGroupStemAndFlag(self):
        """#0 means no group. 1=groupMember, 2= groupStart, 3=groupEnd"""
        if self.stem:
            self.scene().removeItem(self.stem)
            self.stem = None
        for flag in self.flags:
            self.scene().removeItem(flag)
            self.flags = []
        if not self.group:
            raise ValueError(self, self.data, self.data.exportLilypond(), "is NOT part of a beaming group or is in an incomplete group but was instructed for a group drawing.")

        if self.group == 3: #group endings get a small, overlapping beam to the left to work with rests before them
            beamDirection = -0.5 * (self.cachedDuration + self.displacement) * config.DURATION_MOD
        else:
            beamDirection = (self.cachedDuration + self.displacement) * config.DURATION_MOD

        #rightStemUp = self.cachedGroupStemExtremePitches[0] + self.cachedGroupStemExtremePitches[1] >= -5
        rightStemUp = self.getStemDirection(self.cachedGroupStemExtremePitches[0], self.cachedGroupStemExtremePitches[1])
        if rightStemUp: #right stem, up, defined by group
            stem = QtGui.QGraphicsLineItem(QtCore.QLineF(0,
                                                        self.cachedMinMax[0] + TRACK_SIZE/4, #lowest point. The individual notehead.
                                                        0,
                                                        self.cachedGroupStemExtremePitches[1] -10)) #highest point, defined by group
            stem.setPos(18, 0)
            self.stem = stem
            #Beaming above the notes:
            offset = 0
            for flag in range(dictionaries.dur2beams[self.cachedBaseDuration]):
                flag = QtGui.QGraphicsLineItem(QtCore.QLineF(0, 0, beamDirection, 0))
                flag.setPen(PEN_BEAM)
                flag.setParentItem(self)
                flag.setPos(18, self.cachedGroupStemExtremePitches[1] + offset -10)
                self.flags.append(flag)
                offset += 5


        else: #left stem, down
            stem = QtGui.QGraphicsLineItem(QtCore.QLineF(0 ,
                                                        self.cachedGroupStemExtremePitches[0] + 60,  #lowest point, defined by group
                                                        0 ,
                                                        self.cachedMinMax[1]+ TRACK_SIZE/4))  #highest point, the individual notehead.
            stem.setPos(7, 0)
            self.stem = stem
            #Beaming below the notes:
            offset = 0
            for flag in range(dictionaries.dur2beams[self.cachedBaseDuration]):
                flag = QtGui.QGraphicsLineItem(QtCore.QLineF(0, 0, beamDirection, 0))
                flag.setPen(PEN_BEAM)
                flag.setParentItem(self)
                flag.setPos(7, self.cachedGroupStemExtremePitches[0] + 60 - offset)
                self.flags.append(flag)
                offset += 5

        stem.setPen(PEN_STEM)
        stem.setParentItem(self)

    def smfChannel(self):
        #Build string from data
        channelString = ""
        if self.data.channelOffset > 0:
            channelString = "+" + str(self.data.channelOffset)
        elif self.data.channelOffset < 0:
            channelString = str(self.data.channelOffset)
        if self.data.forceChannel >= 0:
            channelString = str(1 + self.data.forceChannel) + channelString  #+1 to convert smf 0-15 to human 1-16

        #start gui drawing
        if channelString or channelString == "": #TODO: Workaround. Somehow delete below did not work.
            if not self.channelGlyph: #the first time.
                suffix = "c" if channelString else ""
                self.channelGlyph = GuiText(channelString+suffix)
                self.channelGlyph.setParentItem(self)
                self.belowStatics.append(self.channelGlyph)
                self.channelGlyph.setDefaultTextColor(QtGui.QColor("green"))
            else:
                suffix = "c" if channelString else ""
                self.channelGlyph.setPlainText(channelString+suffix)
            self.lineUpBelowStatics()

        elif self.channelGlyph: #There was one before. We had a delete
            self.belowStatics.remove(self.channelGlyph)
            self.scene().removeItem(self.channelGlyph)
            self.channelGlyph = None

    def smfProgram(self):
        #Build string from data
        programString = ""
        if self.data.programOffset > 0:
            programString = "+" + str(self.data.programOffset)
        elif self.data.programOffset < 0:
            programString = str(self.data.programOffset)
        if self.data.forceProgram >= 0:
            programString = str(1 + self.data.forceProgram) + programString  #+1 to convert smf 0-15 to human 1-16

        #start gui drawing
        if programString or programString == "": #TODO: Workaround. Somehow delete below did not work.
            if not self.programGlyph: #the first time.
                suffix = "p" if programString else ""
                self.programGlyph = GuiText(programString+suffix)
                self.programGlyph.setParentItem(self)
                self.belowStatics.append(self.programGlyph)
                self.programGlyph.setDefaultTextColor(QtGui.QColor("cyan"))
            else:
                suffix = "p" if programString else ""
                self.programGlyph.setPlainText(programString+suffix)
            self.lineUpBelowStatics()

        elif self.programGlyph: #There was one before. We had a delete
            self.belowStatics.remove(self.programGlyph)
            self.scene().removeItem(self.programGlyph)
            self.programGlyph = None

class GuiRest(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiRest, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.mainGlyph = GuiGlyph(text = "", fontScale = 1.6)
        if self.data.pitch == "s": #skips
            self.mainGlyph.setOpacity(0.5)
        self.dots = None
        self.updateDynamic()
        self.mainGlyph.setPos(0,-35)
        self.addToGroup(self.mainGlyph)
        self.setPos(0,0)


    def updateDots(self):
        dotsString = "\U0001D16D\u2006" * self.data.dots   #2006 is 1/4 em space
        if self.dots:
            self.dots.scene().removeItem(self.dots) #remove the old one first.
        self.dots = GuiGlyph(dotsString, fontScale = 1.5)
        self.dots.setParentItem(self)
        self.dots.setPos(FONT_SIZE, -50)

    def updateDynamic(self):
        super(GuiRest, self).updateDynamic()
        self.mainGlyph.setPlainText(dictionaries.dur2rest[self.cachedBaseDuration])
        self.updateDots()


class GuiClosedContainer(GuiItem):
    """A container with no visible content. The cursor treats this
    as one big item"""

    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiClosedContainer, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.widthDependentOnDisplacement = True

        self.startGlyph = GuiText(text = '\U0001D115')
        self.startGlyph.setScale(4)
        self.startGlyph.setPos(-15, -45)
        self.startGlyph.setDefaultTextColor(QtGui.QColor("green"))
        self.addToGroup(self.startGlyph)

        self.endGlyph = GuiText(text = '\U0001D115')
        self.endGlyph.setScale(-4)
        self.endGlyph.setDefaultTextColor(QtGui.QColor("green"))
        self.addToGroup(self.endGlyph)

        self.background = None
        self.brush = QtGui.QBrush(QtCore.Qt.SolidPattern)
        self.brush.setColor(stringToColor(self.data.uniqueContainerName))

        self.nameGlyph = GuiText(text="")
        #self.nameGlyph.setHtml("<span style='text-shadow: 5px 5px 5px #FF0000'>" + self.data.uniqueContainerName + "</span>")
        self.nameGlyph.setPos(17,-30)
        self.nameGlyph.setScale(1.5)
        self.addToGroup(self.nameGlyph)

        self.background = QtGui.QGraphicsRectItem(10, 0, 1, 40)
        self.background.setBrush(self.brush)
        self.background.setOpacity(0.4)
        self.addToGroup(self.background)
        self.background.setPos(0,0)

        self.updateStatic()
        self.updateDynamic()
        self.setPos(0,0)

    def updateStatic(self):
        self.brush.setColor(stringToColor(self.data.uniqueContainerName))
        if self.background:
            self.background.setBrush(self.brush)
        if self.data.repeatPercent > 1:
            self.nameGlyph.setPlainText(self.data.uniqueContainerName + "(" + str(self.data.repeatPercent) + "x)")
        else:
            self.nameGlyph.setPlainText(self.data.uniqueContainerName)

    def updateForIteration(self):
        """Depends on an up-to-date self.cachedIndex"""
        self.updateDynamic()

    def updateDynamic(self):
        """Regulates the graphical dimensions of this object"""
        if self.track:
            end = (self.data.duration + self.track.workspace.getDisplacement(self.data.duration + self.cachedTickIndex, includeSelf = False) - self.track.workspace.getDisplacement(self.cachedTickIndex, includeSelf = True)) * config.DURATION_MOD
            self.endGlyph.setPos(end + 10 , 85)
            self.background.setRect(10, 0, end -25 , 40)


class GuiMultiMeasureRest(GuiItem):
    """This is a trick item. We never check the backend in the gui
    to get the duration. We try to keep it in sync with the gui only.
    This has no effect on playback or lilypond.

    The problem is that the multiMeasureRest is dependent of the
    backend cursor system. Linked Items and containers do not work that
    way since the gui does not use the backend cursor. """

    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiMultiMeasureRest, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.dataOrg = self.data
        self.data = self
        self.mainGlyph = GuiGlyph(text = "\U0001D129", fontScale = 1.6)
        self.mainGlyph.setPos(0, -35)
        self.addToGroup(self.mainGlyph)
        if not self.dataOrg.lilysyntax == "R": #Lilypond Skip
            self.setOpacity(0.3)
        self.measures = GuiText (text = str(self.dataOrg.measures))
        self.addToGroup(self.measures)
        self.measures.setPos(FONT_SIZE, TEXT_SCALE_CORRECTION)
        self.setPos(0,0)

    @property
    def figures(self):
        return self.dataOrg.figures

    @property
    def chordsymbols(self):
        return self.dataOrg.chordsymbols

    @property
    def cachedDuration(self):
        return self.dataOrg.measures * self.cachedTimesigData.nominator * self.cachedTimesigData.denominator

    @cachedDuration.setter
    def cachedDuration(self, value):
        return True

    @property
    def duration(self):
        return self.dataOrg.measures * self.cachedTimesigData.nominator * self.cachedTimesigData.denominator

    @property
    def cachedBaseDuration(self):
        return 0

    @cachedDuration.setter
    def cachedBaseDuration(self, value):
        return True

    @property
    def base(self):
        return 0

    def updateDynamic(self):
        super(GuiMultiMeasureRest, self).updateDynamic()
        self.measures.setPlainText(str(self.dataOrg.measures))

class GuiPlaceholder(GuiMultiMeasureRest):
    """MMRest but with more space in Lilypond"""
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiPlaceholder, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.mainGlyph.setDefaultTextColor(QtGui.QColor("green"))

class GuiUpbeat(GuiItem):
    """This is a trick item. We never check the backend in the gui
    to get the duration. We try to keep it in sync with the gui only.
    This has no effect on playback or lilypond.
    See Multi Measure Rest"""

    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiUpbeat, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.dataOrg = self.data
        self.data = self
        try: #who knows what weird combination they want. better prepare for the unknown
            upbeatString = "Upbeat:\n" + str(self.cachedTimesigData.nominator) + "/" + api.lilypond.dur2ly[self.cachedTimesigData.denominator] + " - " + api.lilypond.dur2ly[self.dataOrg.base]
        except:
            upbeatString = "Upbeat"
        self.mainGlyph = GuiGlyph(text = upbeatString, fontScale = 0.5)
        self.mainGlyph.setPos(0, 0)
        self.addToGroup(self.mainGlyph)
        self.setPos(0,0)

    @property
    def cachedDuration(self):
        fullMeasureTick = self.cachedTimesigData.nominator * self.cachedTimesigData.denominator
        return fullMeasureTick - self.dataOrg.base

    @property
    def duration(self):
        fullMeasureTick = self.cachedTimesigData.nominator * self.cachedTimesigData.denominator
        return fullMeasureTick - self.dataOrg.base

    @cachedDuration.setter
    def cachedDuration(self, value):
        try:
            self.mainGlyph #cachedDuration.setter is ready before parent init() is executed so parent will already execute this here. The first time this will fail
            try: #who knows what weird combination they want. better prepare for the unknown
                upbeatString = "Upbeat:\n" + str(self.cachedTimesigData.nominator) + "/" + api.lilypond.dur2ly[self.cachedTimesigData.denominator] + " - " + api.lilypond.dur2ly[self.dataOrg.base]
            except:
                upbeatString = "Upbeat"
            self.mainGlyph.setPlainText(upbeatString)
        except:
            pass
        return True

    @property
    def cachedBaseDuration(self):
        return 0

    @cachedDuration.setter
    def cachedBaseDuration(self, value):
        return True

    @property
    def base(self):
        return 0

class GuiKeySignature(GuiItem):
    """We get the order and kind of accidentals from the backend.
    Numbers 0-6 for c to b/h.
    To decide on which pitch/line to draw the glyph we search for the
    first position from high to low which has no ledger line for the
    current clef."""
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiKeySignature, self).__init__(backendItem, displacement = DISPLACE, forceSigs = forceSigs, parentTrack = parentTrack)
        self.breakKeysigLoop = True #marks this item as keysig for track.keysigUpdate
        self.update()
        #TODO: less update, more static.

    def update(self):
        allNatural = True
        self.displacement = DISPLACE + FONT_SIZE #Was fuer ein Gefrickel...

        #Show the root, always.
        rootString = GuiText(text = api.lilypond.pitch2ly[self.data.root + 1050].title()) #three octaves higher is a plain Lilypond note string without octaves.
        self.addToGroup(rootString)
        rootString.setPos(0, -1 * MUSIC_SCALE_CORRECTION)

        #Generate accidentals
        posX = 0
        for x in self.data.keysigtuplet:
            if not self.data.explicit and x[1] == 0: #natural / C Major without "explicit" flag.
                pass
            else:
                startPosition = 2820 + x[0] * 50  #highest C (2820) + position of the accidental in pitch numbers. x[0] = 0:c, 1:d, 2:e etc.
                result = -1 * (startPosition / PITCH_MOD) + self.cachedClefPitch
                if result >= 25: #under the lines
                    while result >= 25:
                        result -= 35 #one octave up.
                elif result <= -35:    #above the lines
                    while result <= -35:
                        result += 35 #one octave down.
                new = GuiGlyph(dictionaries.keysigDiff2accidental[x[1]])
                self.addToGroup(new)
                new.setPos(posX, result - 15)  #G Major: [(3,10), (0,0), (4,0), (1,0), (5,0), (2,0), (6,0)] 10:Shartp, -10:Flat. #-15 to get the the accidentals up to their lines
                self.displacement += FONT_SIZE * 2
                posX += FONT_SIZE / 2
                allNatural = False

        if allNatural: #create a natural glyph that is as heigh as the whole track to show C Major or A Minor or whatever the scale is.
            self.mainGlyph = GuiGlyph(text="\u266E", fontScale= 2)
            self.displacement = DISPLACE
            self.mainGlyph.setPos(-3,  - 66)
            self.addToGroup(self.mainGlyph)

class GuiTimeSignature(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiTimeSignature, self).__init__(backendItem, displacement = DISPLACE/2 , forceSigs = forceSigs, parentTrack = parentTrack)
        #self.nominator = self.data.nominator   #example: x/4 Upper number
        #self.denominator = self.data.denominator #example: 384/x lower number
        self.breakTimesigLoop = True #instead of typesetting this is a break marker when track.timesigUpdate() runs. Additionaly this is used by timesig index generation in firstPass
        self.upper = GuiText(str(self.data.nominator), fontScale = 2)
        self.lower = GuiText(str(api.lilypond.dur2ly[self.data.denominator]), fontScale = 2)
        self.addToGroup(self.upper)
        self.addToGroup(self.lower)
        self.lower.setPos(0, PITCH_MOD)
        self.upper.setPos(0, -PITCH_MOD)
        self.setY(-PITCH_MOD)

class GuiDynamicSignature(GuiItem):
    """Take the expression string and place it into the scene"""
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiDynamicSignature, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.displacement = DISPLACE/2
        self.mainGlyph = GuiText(text = "", fontScale = 1.5)
        self.mainGlyph.setPos(5, TRACK_SIZE/2.5)
        self.verticalLine()
        self.addToGroup(self.mainGlyph)
        self.setPos(0,0)
        self.updateStatic()

    def updateStatic(self):
        self.mainGlyph.setHtml("<b><i>" + str(self.data._expression) + "</i></b>")


class GuiPerformanceSignature(GuiItem):
    """Take the expression string and place it into the scene"""
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiPerformanceSignature, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.displacement = DISPLACE/2
        self.mainGlyph = GuiText(text = "\u2707", fontScale = 2)
        self.addToGroup(self.mainGlyph)
        self.mainGlyph.setPos(5, 0)
        self.name = GuiText(text = self.data.name, fontScale = 1)
        self.addToGroup(self.name)
        self.name.setPos(-5, -25)
        self.setPos(0,0)

    def updateStatic(self):
        self.name.setPlainText(self.data.name)

class GuiWaitForChord(GuiItem):
    """The difference to GuiDynamicSignature is that this uses the
    lilystring variable and strips any backslashes.
    Different font as well."""
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiWaitForChord, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.displacement = DISPLACE
        self.mainGlyph = GuiText(text = self.data.lilystring.lstrip('\\'))
        self.mainGlyph.setPos(0, TRACK_SIZE/2.5)
        self.addToGroup(self.mainGlyph)
        self.setPos(0,0)

class GuiContainerStart(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiContainerStart, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, False)
        self.breakContainerLoop = True #instead of typechecking this is a break marker when adding items in containers.
        self.displacement = DISPLACE/2
        self.mainGlyph = GuiText(text = '\U0001D115')
        self.mainGlyph.setScale(4)
        self.mainGlyph.setPos(-10, -45)
        self.mainGlyph.setDefaultTextColor(QtGui.QColor("green"))
        self.addToGroup(self.mainGlyph)

        self.nameGlyph = GuiText(text = "")
        #self.nameGlyph.rotate(-90)
        self.nameGlyph.setPos(3, -28)
        #self.nameGlyph.setScale(1.5)
        self.addToGroup(self.nameGlyph)
        self.updateStatic()
        self.setPos(0,0)

    def updateStatic(self):
        if self.data.parentContainer.repeatPercent > 1:
            self.nameGlyph.setPlainText(self.data.parentContainer.uniqueContainerName + "(" + str(self.data.parentContainer.repeatPercent) + "x)")
        else:
            self.nameGlyph.setPlainText(self.data.parentContainer.uniqueContainerName)

class GuiContainerEnd(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None): #, dataContainer):   #dataContainer is not needed currently. And if it becomes needed don't forget to forward it in insertRelativeToContainerBegin
        super(GuiContainerEnd, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        #self.dataContainer = dataContainer #is set directly on GUI insert.
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable, False)
        self.breakContainerLoop = None #! important. All other items have False. This is needed in insertRelativeToContainerBegin to prevent typechecking
        self.widthDependentOnDisplacement = True
        self.displacement = DISPLACE/2
        self.mainGlyph = GuiText(text = '\U0001D115')
        self.mainGlyph.setScale(-4)
        self.mainGlyph.setPos(30,85)
        self.mainGlyph.setDefaultTextColor(QtGui.QColor("green"))
        self.addToGroup(self.mainGlyph)

        #self.brush = QtGui.QBrush(QtCore.Qt.SolidPattern)
        #self.background = QtGui.QGraphicsRectItem(10, 0, 1, 40)
        #self.background.setBrush(self.brush)
        #self.background.setOpacity(0.5)
        #self.addToGroup(self.background)

        #self.updateStatic()
        #self.updateDynamic()

    """
    def updateStatic(self):
        print ("s")
        self.brush.setColor(stringToColor(self.data.parentContainer.uniqueContainerName))
        self.background.setBrush(self.brush)

    def updateForIteration(self):
        print ("i")
        #Depends on an up-to-date self.cachedIndex
        self.updateDynamic()

    def updateDynamic(self):
        print ("d")
        if self.track:
            completeDur = self.data.parentContainer.duration
            rep =  self.data.parentContainer.repeatPercent
            repeatRestDuration = completeDur / rep * (rep -1)
            if repeatRestDuration:
                end = repeatRestDuration + self.track.workspace.getDisplacement(repeatRestDuration + self.cachedTickIndex, includeSelf = False)
                end = end * config.DURATION_MOD - 30
            else:
                end = 1
            self.background.setRect(10, 0, end-10, 40)
    """


class GuiMarkup(GuiItem):
    """The difference to GuiDynamicSignature is that this uses the
    lilystring variable and strips any backslashes.
    Different font as well."""
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiMarkup, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.displacement = DISPLACE/2
        self.mainGlyph = GuiText(text = self.data.lilystring[:11])
        self.mainGlyph.rotate(-90)
        self.mainGlyph.setPos(0, TRACK_SIZE/2)
        self.addToGroup(self.mainGlyph)
        self.verticalLine()
        self.setPos(0,0)
        self.updateStatic()
    def updateStatic(self):
        self.mainGlyph.setHtml("<span style='background-color:yellow'>" + self.data.lilystring[:11] + "</span>")


class GuiSegno(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiSegno, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.displacement = DISPLACE/4
        self.mainGlyph = GuiText(text = '\U0001D10B')
        self.mainGlyph.setScale(4)
        self.mainGlyph.setPos(-20, -55)
        self.verticalLine()
        self.addToGroup(self.mainGlyph)
        self.setPos(0,0)

class GuiCoda(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiCoda, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.displacement = DISPLACE/4
        self.mainGlyph = GuiText(text = '\U0001D10C')
        self.mainGlyph.setScale(4)
        self.mainGlyph.setPos(-20, -55)
        self.verticalLine()
        self.addToGroup(self.mainGlyph)
        self.setPos(0,0)

class GuiFine(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiFine, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.displacement = DISPLACE/2
        self.mainGlyph = GuiText(text = 'Fine')
        self.mainGlyph.setScale(2)
        self.mainGlyph.rotate(-90)
        self.mainGlyph.setPos(-10, 50)
        self.verticalLine()
        self.addToGroup(self.mainGlyph)
        self.setPos(0,0)

class GuiGotoSegno(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiGotoSegno, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.displacement = DISPLACE/4
        self.mainGlyph = GuiText(text = '\U0001D109')
        self.mainGlyph.setScale(4)
        self.mainGlyph.setPos(-30, -50)
        self.verticalLine()
        self.addToGroup(self.mainGlyph)
        self.setPos(0,0)

class GuiGotoCapo(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiGotoCapo, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.displacement = DISPLACE/4
        self.mainGlyph = GuiText(text = '\U0001D10A')
        self.mainGlyph.setScale(4)
        self.mainGlyph.setPos(-30, -50)
        self.verticalLine()
        self.addToGroup(self.mainGlyph)
        self.setPos(0,0)

class GuiGotoCoda(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiGotoCoda, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.displacement = DISPLACE/4
        self.mainGlyph = GuiText(text = '\u21B7\U0001D10C')
        self.mainGlyph.setScale(4)
        self.mainGlyph.setPos(-30, -50)
        self.addToGroup(self.mainGlyph)
        self.setPos(0,0)

class GuiPedalSustainChange(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiPedalSustainChange, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.displacement = DISPLACE/10
        self.mainGlyph = GuiText(text = '𝆰')
        self.mainGlyph.setScale(3)
        self.mainGlyph.setPos(-10, 30)
        self.verticalLine()
        self.addToGroup(self.mainGlyph)
        self.setPos(0,0)

class GuiSpecialBarline(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiSpecialBarline, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.repeatGlyph = None
        self.mainGlyph = GuiText(text = dictionaries.barlines[self.data.lilypond.split('"')[1]])
        self.mainGlyph.setScale(4)
        self.mainGlyph.setPos(-15, -45)
        self.displacement = DISPLACE/2
        self.addToGroup(self.mainGlyph)
        self.setPos(0,0)
        self.updateStatic()

    def updateStatic(self):
        if self.repeatGlyph:
            self.scene().removeItem(self.repeatGlyph)
        self.repeatGlyph = None
        repeat = self.data.repeat
        if repeat > 1:
            self.repeatGlyph = GuiText(text = "(" +  str(repeat ) + ")", fontScale = 1)
            self.addToGroup(self.repeatGlyph)
            self.repeatGlyph.setPos(-5, 45)

class GuiAlternateEnd(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiAlternateEnd, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.endingNumbers = None
        self.mainGlyph = GuiText(text = self.data.text)
        #self.mainGlyph.setScale(1)
        self.mainGlyph.setPos(10, -40)
        self.displacement = DISPLACE/2
        self.addToGroup(self.mainGlyph)

        line_vertical = QtGui.QGraphicsLineItem(QtCore.QLineF(0, 55, 0, 0))
        line_vertical.setPen(PEN_BEAM)
        line_vertical.setParentItem(self)
        line_vertical.setPos(7, -20)
        self.addToGroup(line_vertical)

        line_horizontal = QtGui.QGraphicsLineItem(QtCore.QLineF(0, 0, 60, 0))
        line_horizontal.setPen(PEN_BEAM)
        line_horizontal.setParentItem(self)
        line_horizontal.setPos(7, -20)
        self.addToGroup(line_horizontal)

        self.setPos(0,0)

    def updateStatic(self):
        self.mainGlyph.setPlainText(self.data.text)

class GuiAlternateEndClose(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiAlternateEndClose, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.displacement = DISPLACE/2
        self.mainGlyph = QtGui.QGraphicsLineItem(QtCore.QLineF(0, 55, 0, 0))
        self.mainGlyph.setPen(PEN_BEAM)
        self.mainGlyph.setParentItem(self)
        self.mainGlyph.setPos(7, -20)
        self.addToGroup(self.mainGlyph)

        line_horizontal = QtGui.QGraphicsLineItem(QtCore.QLineF(0, 0, -60, 0))
        line_horizontal.setPen(PEN_BEAM)
        line_horizontal.setParentItem(self)
        line_horizontal.setPos(7, -20)
        self.addToGroup(line_horizontal)
        self.setPos(0,0)

class GuiTempoSignature(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiTempoSignature, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.endingNumbers = None
        self.mainGlyph = GuiText(text = "") #"" for now
        self.mainGlyph.setPos(0, -40)
        self.displacement = DISPLACE/2
        self.verticalLine()
        self.addToGroup(self.mainGlyph)
        self.setPos(0,0)
        self.updateStatic()

    def updateStatic(self):
        self.mainGlyph.setPlainText("⌛" + self.data.tempostring + " " + api.lilypond.dur2ly[self.data.referenceTicks] + "=" + str(self.data.beatsPerMinute))

class GuiTempoModification(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiTempoModification, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.endingNumbers = None
        self.mainGlyph = GuiText(text = "") #"" for now
        self.mainGlyph.setPos(0, -40)
        self.displacement = DISPLACE/4
        self.addToGroup(self.mainGlyph)
        self.verticalLine()
        self.setPos(0,0)
        self.updateStatic()

    def updateStatic(self):
        self.mainGlyph.setPlainText("⌛ " + str(self.data.bpmModificator))
    updateDynamic = updateStatic # the api signal for augment is dynamic.

class GuiChannelChange(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiChannelChange, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.mainGlyph = GuiText(text = "") #"" for now
        self.mainGlyph.setScale(1.5)
        self.displacement = DISPLACE/2
        self.mainGlyph.setPos(-5, -30)
        self.verticalLine()
        self.addToGroup(self.mainGlyph)
        self.setPos(0,0)
        self.updateStatic()
        self.mainGlyph.setDefaultTextColor(QtGui.QColor("green"))

    def updateStatic(self):
        if type(self.data) is api.items.ChannelChangeAbsolute:
            self.mainGlyph.setPlainText("C" + str(self.data.number +1))
        else: #relative
            if self.data.number > 0:
                extra = "C+"
            else:
                extra = "C" #the minus is added by the number
            self.mainGlyph.setPlainText(extra + str(self.data.number))

class GuiProgramChange(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiProgramChange, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.mainGlyph = GuiText(text = "") #"" for now
        self.mainGlyph.setScale(1.5)
        self.displacement = DISPLACE/2
        self.mainGlyph.setPos(-5, -30)
        self.verticalLine()
        self.addToGroup(self.mainGlyph)
        self.setPos(0,0)
        self.updateStatic()
        self.mainGlyph.setDefaultTextColor(QtGui.QColor("cyan"))

    def updateStatic(self):
        if type(self.data) is api.items.ProgramChangeAbsolute:
            self.mainGlyph.setPlainText("P" + str(self.data.number +1))
        else: #relative
            if self.data.number > 0:
                extra = "P+"
            else:
                extra = "P" #the minus is added by the number
            self.mainGlyph.setPlainText(extra + str(self.data.number))

class GuiInstrumentChange(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiInstrumentChange, self).__init__(backendItem, forceSigs = forceSigs, parentTrack = parentTrack)
        self.displacement = DISPLACE/2
        self.mainGlyph = GuiText(text = "♺", fontScale = 2)
        self.mainGlyph.setPos(5, 0)
        self.addToGroup(self.mainGlyph)

        self.name = GuiText(text = "", fontScale = 1) #empty until updateStatic
        self.addToGroup(self.name)
        self.name.setPos(-5, -25)


        self.setPos(0,0)
        self.updateStatic()

    def updateStatic(self):
        self.name.setPlainText(self.data.shortInstrumentName + "(Prg:" + str(self.data.smfPatch +1 ) + ")")


class GuiSlurOn(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiSlurOn, self).__init__(backendItem, displacement = DISPLACE/4, mainGlyph = "(", forceSigs = forceSigs, parentTrack = parentTrack)

class GuiSlurOff(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiSlurOff, self).__init__(backendItem, displacement = DISPLACE/4, mainGlyph = ")", forceSigs = forceSigs, parentTrack = parentTrack)
        self.setPos(4,0)

class GuiPhrasingSlurOn(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiPhrasingSlurOn, self).__init__(backendItem, displacement = DISPLACE/2, mainGlyph = "((", forceSigs = forceSigs, parentTrack = parentTrack)


class GuiPhrasingSlurOff(GuiItem):
    def __init__(self, backendItem, forceSigs = None, parentTrack = None):
        super(GuiPhrasingSlurOff, self).__init__(backendItem, displacement = DISPLACE/2, mainGlyph = "))", forceSigs = forceSigs, parentTrack = parentTrack)
