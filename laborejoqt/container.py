# -*- coding: utf-8 -*-
from PyQt4 import QtGui, QtCore
from collections import deque
from hashlib import md5
import laborejocore as api

from laborejoqt import config, dictionaries
from laborejoqt.items import GuiTimeSignature, GuiText, GuiMultiMeasureRest, GuiUpbeat, GuiChord, GuiSlurOn, GuiSlurOff
from laborejoqt.constants import *


#These two must be independent (memory) objects.
DEFAULT_TIMESIG = GuiTimeSignature(api.items.DEFAULT_TIMESIG, forceSigs=[0, 162, api.items.DEFAULT_KEYSIG, api.items.DEFAULT_TIMESIG]) #4/4 - Lilypond Default  #the forced sigs do not matter here. Just get them out of the way so we can start the program. #if forceSigs: # [track, clefPitch, backendKeysig, backendTimesig]
ENDMARKER_TIMESIG = GuiTimeSignature(api.items.DEFAULT_TIMESIG, forceSigs=[0, 162, api.items.DEFAULT_KEYSIG, api.items.DEFAULT_TIMESIG]) #4/4 - Lilypond Default  #the forced sigs do not matter here. Just get them out of the way so we can start the program. #if forceSigs: # [track, clefPitch, backendKeysig, backendTimesig]


class Pseudo(object):
    """A general purpose empty item which is needed for iterating cases.
    For example beaming groups look at the previous items, and there
    must be one before the first, Pseudo"""

    class Pos(object):
        def x(self):
            return 0

        def y(self):
            return 0

    def __init__(self):
        self.cachedDuration = 0
        self.cachedBaseDuration = 0
        self.group = 0
        self.data = False
        self.dataOrg = False
        self.cachedMinMax = [float("inf"), -1 * float("inf")] #out of range for any pitch.
        self.data = self
        self.noteheads = False
        self.posObj = self.Pos()
        self.displacement = 0
        self.track = None

    def nothing(self, *args):
        pass

    def pos(self):
        return self.posObj


    setX = setY = tremoloMarker = chordSymbols = tupletMarkers = smfChannel = directivesPst = updateDots = updateGroupStemAndFlag = figuredBass = updateRightFromThisItem = updateStatic = extensions = updateDynamic = update = setSelected = nothing
    def exportLilypond(self):
        return "Pseudo"

PSEUDO = Pseudo()

class GuiIncipit(QtGui.QGraphicsItemGroup):
    """Each GUI Track has an Incipit which is always visible to the left
    of the tracks.
    The incipit view width is 50 pixel.
    An object with 10 width which shall be on the right edge has to
    start at pixel position 40"""
    def __init__(self, guiTrack):
        super(GuiIncipit, self).__init__()
        self.guiTrack = guiTrack
        self.data = self.guiTrack.data

        #"\\new " + what + " <<\n" + extra
        self.glyphes = { "\\new GrandStaff <<\n" : "𝄔",
                    "\\new PianoStaff <<\n" : "𝄔",
                    "\\new ChoirStaff <<\n\\set ChoirStaff.systemStartDelimiter = #'SystemStartBrace \n" : "𝄔", #groupVoiceAndFiguredBassStart
                    #Squared:
                    "\\new ChoirStaff <<\n" : "𝄕",
                    "\\new StaffGroup <<\n\\set StaffGroup.systemStartDelimiter = #'SystemStartSquare \n" : "𝄕", #groupSquareStart
                    "\\new StaffGroup <<\n" : "𝄕", }

        #Colored Group Marker
        self.backendGroupMarker = QtGui.QGraphicsRectItem(0, 0, 9, 40)
        self.addToGroup(self.backendGroupMarker)
        self.backendGroupMarker.setParentItem(self)
        self.backendGroupMarker.setPos(70,0)
        self.changeGroupColor()

        #Name
        self.nameMarker = QtGui.QGraphicsTextItem("")
        self.addToGroup(self.nameMarker)
        self.nameMarker.setParentItem(self)
        self.nameMarker.setPos(5,0)
        self.nameMarker.setDefaultTextColor(QtGui.QColor("black"))
        self.changeNameMarker()

        #Lilypond Group Marker
        self.lilypondGroupMarkers = []
        self.changeLilypondStaffGroupMarkers()
        self.updateMarkerSize()

    def stringToColor(self, st):
        """Convert a string to QColor. Same string, same color
        Is used for group coloring"""
        if st:
            c = md5(st.encode()).hexdigest()
            return QtGui.QColor(int(c[0:9],16) % 255, int(c[10:19],16) % 255, int(c[20:29],16)% 255, 255)
        else:
            return QtGui.QColor(255,255,255,255) #Return White

    def changeLilypondStaffGroupMarkers(self):
        #delete old
        for mark in self.lilypondGroupMarkers:
            self.scene().removeItem(mark)
        self.lilypondGroupMarkers = []

        for k,v in sorted(self.data.directivePre.items()) + sorted(self.data.directivePst.items()):
            if k.startswith("StaffGroupEnd"):
                marker = QtGui.QGraphicsTextItem(")")
                marker.setParentItem(self)
                marker.setScale(3)
                marker.setDefaultTextColor(QtGui.QColor("black"))
                self.addToGroup(marker)
                marker.translate(0, 25)
                self.lilypondGroupMarkers.append(marker)
            elif k.startswith("StaffGroup"):
                marker = QtGui.QGraphicsTextItem(self.glyphes[v])
                marker.setParentItem(self)
                marker.setScale(3.5)
                marker.setDefaultTextColor(QtGui.QColor("black"))
                self.addToGroup(marker)
                self.lilypondGroupMarkers.append(marker)

        offset = 43
        for x in self.lilypondGroupMarkers:
            if x: #not None
                x.setPos(offset, -39)
                offset -= 7

    def updateMarkerSize(self):
        height = 101 if self.guiTrack.dualView else 41
        self.backendGroupMarker.setRect(0, 0, 9, height)

    def changeGroupColor(self):
        self.cachedGroupColor = self.stringToColor(self.data.group)
        self.backendGroupMarker.setBrush(self.cachedGroupColor)

    def changeNameMarker(self):
        self.nameMarker.setPlainText(self.data.shortInstrumentName)

class GuiTrack(QtGui.QGraphicsItemGroup):
    """A list which holds one track of Guitems. Also holds one cursor
    which can go left and right. Internal cursor only. Has little to do
    with the api or gui cursor."""

    def __init__(self, workspace, backendTrack):
        super(GuiTrack, self).__init__()
        self.dualView = False
        self.data = backendTrack
        self.incipit = GuiIncipit(self)
        self.leftPart = deque([])
        self.leftPartTicks = 0
        self.cursorTempDisplacement = [[]]  #If there is more than one displaced item in a row this will keep track of how many there are.
        self.rightPart = deque([])
        self.barlines = []
        self.lines = []
        self.lyrics = []
        self.workspace = workspace #I am in that workspace / Gui score
        self.setHandlesChildEvents(False) #A mouseclick will select the a child, not the whole Group.
        self.setEnabled(True)
        self.displacements = set() #a shortcut to all items guiItem with displacement
        self.cachedTimeSignatures = {DEFAULT_TIMESIG:(-1, -1)} #a shortcut to all time signatures. There is one key 'None' which is the end of the tracks in ticks.
        self.cachedTrackTicks = 0 #the last time this track was updated it had this many ticks.
        self.cachedClefPitch = 162
        self.cachedVerseCount = 0
        self.size = TRACK_SIZE #Y Dimension
        self.cachedTrackWidth = 0 #in pixel
        self.collapsed = False


        #The data set determines where ledger lines up and down start and where they will be drawn.
        #The first two are start under and start above. They are the graphical positioning above/below the track of the first ledger line. 10 points is one step from line to line.
        #The second two are the boundary. If the note is below/above that we start drawing ledger lines. These are not pixels but pitch units. Or better: minMax units
        #start_under, start_above, lower_boundary, upper_boundary, offset_below, offset_above, middle_line_special = ledgerLinesDataSet
        self.ledgerLinesDataSetSingleView = (50, -10, 23, -33, 0, 0, False)
        self.ledgerLinesDataSetDualView = (50+60, -10, 23+60, -33, -5.8, 0, True)
        self.ledgerLinesDataSetCurrent = self.ledgerLinesDataSetSingleView

    @property
    def offsetFactor(self):
        """For GUI space calculation"""
        dualFactor = 1.6 if self.dualView else 1  #2.2 is 11 lines. 5 lines equals factor 1, double size +1 line more.
        collapsedFactor = COLLAPSED_FACTOR/2  if self.collapsed else 1
        return collapsedFactor * dualFactor

    def toggleDualView(self):
        """A Gui-Only mode that shows one system with 11 lines. The
        middle line is invisible and is the middle c' in both violin
        and bass clef (if the main clef is a violin clef anyway.)
        But it is not dependent on clefs. Everything works normally.
        Technically you just get more lines and space so that
        ledger lines are easier to see."""

        if self.dualView:  #we are already in Dual Mode
            self.dualView = False
            self.ledgerLinesDataSetCurrent = self.ledgerLinesDataSetSingleView
        else:
            #self.offset = TRACK_SIZE + TRACK_SIZE/5 #double size plus space for one more line.
            self.dualView = True
            self.ledgerLinesDataSetCurrent = self.ledgerLinesDataSetDualView
        self.drawLines(self.cachedTrackWidth)
        self.drawLyrics()
        self.incipit.updateMarkerSize()
        self.updateBarlines()
        self.workspace.main.ui.actionToggle_Extended_Track_View.setChecked(self.dualView)
        self.workspace.repositionTracks()

    def toggleCollapse(self, repositionTracks = True):
        if self.collapsed:
            self.expandView(repositionTracks)
        else:
            self.collapseView(repositionTracks)
        self.drawLyrics()

    def collapseView(self, repositionTracks = True):
        """Scale the Y height to 50%"""
        self.setTransform(COLLAPSED_TRANSFORM)
        self.incipit.setTransform(COLLAPSED_TRANSFORM)
        self.incipit.nameMarker.scale(1,2) #counter effect
        self.collapsed = True
        self.setOpacity(0.5)
        if repositionTracks: #a performance tweak
            self.workspace.repositionTracks()
            self.workspace.main.ui.actionToggle_Collapse_Track.setChecked(True)

    def expandView(self, repositionTracks = True):
        """Scale the Y height to 100%"""
        self.setTransform(EXPANDED_TRANSFORM)
        self.incipit.setTransform(EXPANDED_TRANSFORM)
        self.incipit.nameMarker.scale(1,0.5) #counter effect
        self.collapsed = False
        self.setOpacity(1)
        if repositionTracks: #a performance tweak
            self.workspace.repositionTracks()
            self.workspace.main.ui.actionToggle_Collapse_Track.setChecked(False)

    def select(self):
        for i in self.leftPart:
            i.setSelected(True)
        for i in self.rightPart:
            i.setSelected(True)

    def deselect(self):
        """Never used""" #TODO: remove? no.. why, looks nice.
        for i in self.leftPart:
            i.setSelected(False)
        for i in self.rightPart:
            i.setSelected(False)

    def getCurrentItem(self):
        if self.rightPart:
            return self.rightPart[0]
        else:
            return False

    def getIndex(self):
        """This is the same as the backend cursor flat index"""
        return len(self.leftPart)

    def reset(self):
        #import pprint
        #Move all items to self.rightPart and empty left part
        self.leftPart.extend(self.rightPart)
        self.rightPart = self.leftPart
        self.leftPart = deque([])

        #Reset all parameters
        self.leftPartTicks = 0
        self.cursorTempDisplacement = [[]]

    def goToIndex(self, indexGoal):
        """Always goes to the right"""
        start = self.getIndex()
        if indexGoal ==  start : #already correct
            return True
        else:
            if not indexGoal > start: #the item is not on the right side?
                self.reset()

            while indexGoal != self.getIndex():
                if not self._right(): #this moves right
                    return False #track is shorter than goal.
            return True

    def goToTickIndex(self, ticksGoal):
        """Always go to the earliest item with this tickindex.
        If there is a clef and a chord on tick 0, then we seek the clef"""
        if ticksGoal <= self.leftPartTicks: #the item is left. search from the beginning
            self.reset()

        while self.rightPart and ticksGoal > self.rightPart[0].cachedTickIndex:
            if not self._right(): #this moves right
                return False
        return True

    def insert(self, guiItem, standaloneInsert):
        """standaloneInsert means that after insert the gui does
        update right and all the stuff. If it is false it will just
        create the item and place it in the track so it can be
        repositioned later."""
        self.rightPart.appendleft(guiItem)
        guiItem.track = self
        guiItem.setParentItem(self)
        guiItem.setPos(0,0) #Needed to update guiItems position to the parents/track coordinates, after it was created.
        guiItem.cachedTickIndex = self.leftPartTicks #TODO It is wrong for linked items and gets update by firstPass right after insert anyway. But we need it immediatly for buildDisplacementMap()
        #guiItem.cachedIndex is not necessary here since there is no displacement calculation with that.
        disp = guiItem.displacement
        if disp:  #extra space created by this item?
            self.displacements.add(guiItem)
        #self.workspace.buildDisplacementMap() #updateRight->firstPassRight does this.
        if standaloneInsert:
            self.updateRight(forceDisplacementUpdatingOtherTracks = disp) #include the current item in the update process.
        self._right() #place the current item in self.leftPart

    def drawLines(self, end):
        """The drawing method differs for even and odd numbers of lines.
        Odd numbers are centered around 'b on a line (Treble clef)
        Even numbers are centered around 'b between to lines.

        No no no. I don't care. We don't need number of lines here."""
        #Remove old track lines
        for line in self.lines:
            self.scene().removeItem(line)
        self.lines[:] = []

        if self.data.uniqueContainerName.lower() == "master":
            normalLines = False
        else:
            normalLines = True

        gapToNextLine = 0
        end *= config.DURATION_MOD

        #nr_of_lines = (TRACK_SIZE + self.offset) / (TRACK_SIZE/5)  #track size is always only for 5 lines.
        if self.dualView:
            nr_of_lines = 11
        else:
            nr_of_lines = 5


        for x in range(int(nr_of_lines)):
            line = QtGui.QGraphicsLineItem(QtCore.QLineF(0, 0, end + 50, 0))
            line.setEnabled(False)
            self.addToGroup(line)
            line.setParentItem(self)
            if normalLines:
                line.setPen(PEN_LINE)
            else:
                line.setPen(PEN_LINETWO)
            if x == 5: #one special case
                line.setOpacity(0)
            self.lines.append(line)
            line.setPos(0, gapToNextLine)
            gapToNextLine += PITCH_MOD


    def _removeLyrics(self):
        for lyrics in self.lyrics:
            self.scene().removeItem(lyrics)
        self.lyrics[:] = []

    def drawLyrics(self):
        memberset = set((GuiChord, GuiSlurOn, GuiSlurOff))
        if config.LYRICS: #Lyrics can be switched off
            self._removeLyrics()
            yStartPosition = self.size/1.5 * self.offsetFactor
            backendLyrics = self.data.getLyricsAsList()  #Returns a list of list. Each list for one verse.
            if backendLyrics:
                count = 0
                self.cachedVerseCount = 0
                #Get all chords and slurs
                rawData = [x for x in list(self.leftPart) + list(self.rightPart) if type(x) in memberset]
                #Now delete everything that is not a a standalone chord or the beginning of a slurred group
                chordsForSyllabes = []
                gen = iter(rawData)
                group = False #A switch for the loop. Are we in a slur-group?
                for x in gen:
                    ty = type(x)
                    if ty is GuiSlurOn:
                        group = True
                        chordsForSyllabes.append(next(gen))
                        continue
                    elif ty is GuiSlurOff:
                        group = False
                        continue
                    if not group:
                        chordsForSyllabes.append(x)

                #chordsForSyllabes is now a list with exactly all chords that get syllabes.

                for verse in backendLyrics:
                    for guiChord, syllabe in zip(chordsForSyllabes, verse):
                        #zip makes sure that we only iterate as long there are both items. No need for a break condition.
                        lItem = GuiText(text='', shift = 0, fontScale = 1, pitch = MIDDLE_LINE)
                        lItem.setHtml("<p style='color: #000000; background-color: #ffffff'>" + syllabe + "</p>")
                        self.addToGroup(lItem)
                        lItem.setParentItem(self)
                        #lItem.setDefaultTextColor(QtGui.QColor("blue"))
                        lItem.setPos(guiChord.pos().x(), yStartPosition + count) #just under the track. Since it is above all (zValue) and has background it will be visible but most likely hide notes.
                        lItem.setZValue(200) #on top.
                        self.lyrics.append(lItem)
                    count += FONT_SIZE / 1.5
                    self.cachedVerseCount += 1

    def _storeExtremePitches(self, guiItem, returnlst):
        """When beaming we need to know what is the highest and what
        is the lowest pitch in the group to position stems and the beam."""
        low = guiItem.cachedMinMax[0]
        high = guiItem.cachedMinMax[1]
        if low < returnlst[0]:
            returnlst[0] = low #lowest note  pitch
        if high > returnlst[1]:
            returnlst[1] = high #highest note pitch
        return returnlst


    def firstPassRight(self, forceDisplacementUpdatingOtherTracks = False):
        """Update cached GuiItem values.

        Displacement information is not gathered.
        This means that you cannot add displacement to items
        which had non before (changing an existing >0 value is fine).
        If you do so you have to add it to track.displacements
        in the changing function.
        """
        #First get the previous item from the left part. it may need modification if it was the end of a beam group the last time.
        try:
            previousGuiItem = self.leftPart[-1]
        except IndexError:
            previousGuiItem = PSEUDO #This will be reset several times. Do not use it for anything else than beaming groups. It really means the last item which was part of a beaming group.
        if previousGuiItem.data and previousGuiItem.group == 0 and 0 < previousGuiItem.cachedBaseDuration <= 192:
            previousGuiItem.group = 2
        positionUntilNow = self.leftPartTicks
        indexUntilNow = len(self.leftPart)
        currentBeamGroup = []
        currentExtremePitches = [float("inf"), -1 * float("inf")] #out of range for any pitch.

        if self.rightPart:
            startItem = self.rightPart[0]
        else:
            startItem = PSEUDO

        for guiItem in reversed(self.leftPart):
            if guiItem.noteheads:
                if startItem.cachedBaseDuration == guiItem.cachedBaseDuration and (guiItem.group == 3 or guiItem.group == 1):  #We are in a group
                    self._storeExtremePitches(guiItem, currentExtremePitches)
                    currentBeamGroup.append(guiItem)
                elif startItem.cachedBaseDuration == guiItem.cachedBaseDuration and guiItem.group == 2: #Beginning of a group. We don't need to go further/backwards
                    self._storeExtremePitches(guiItem, currentExtremePitches)
                    currentBeamGroup.append(guiItem)
                    break
                elif guiItem.group == 2: #old beginning of a group but the new duration does not match the old. we have a group here where one member got augmented or similar.
                    guiItem.group = 0
                    guiItem.updateStandaloneStemAndFlag()
                    guiItem.lastPassWasGroup = False
                    break
                elif guiItem.group == 1: #old member of a group but the new duration does not match the old. we have a group here where one member got augmented or similar.
                    guiItem.group = 0
                    startItem = PSEUDO
                    guiItem.updateStandaloneStemAndFlag()
                    guiItem.lastPassWasGroup = False
                else: #group = 0 or different duration
                    break
            else: #group = 0 or different duration
                 break
        for i in currentBeamGroup:
            i.cachedGroupStemExtremePitches = currentExtremePitches

        #Loop right to update cached values
        thereWasDisplacement = False
        for guiItem in self.rightPart:
            guiItem.cachedDuration = guiItem.data.duration
            guiItem.cachedBaseDuration = guiItem.data.base
            if guiItem.breakTimesigLoop: #it's a timesig!
                self.cachedTimeSignatures[guiItem] = (positionUntilNow, len(self.leftPart))  #we can NOT use the backend data directly to make it easier later. backend items are unique, so are dict keys. If it is two times in a track we loose data in our dict.
            if not thereWasDisplacement and (guiItem.displacement or guiItem.widthDependentOnDisplacement): #widthDependentOnDisplacement is for intransparent containers that need to change if a disp. item is in the other track.
                thereWasDisplacement = positionUntilNow
            guiItem.cachedTickIndex = positionUntilNow
            guiItem.cachedIndex = indexUntilNow

            #prepare stems and beaming groups which will be used in self.updateRight(). Depends on correct timesig information.
            #0 means no group. 1=groupMember, 2= groupStart, 3=groupEnd.
            metricalPosition = divmod(positionUntilNow / guiItem.cachedTimesigData.denominator, guiItem.cachedTimesigData.nominator)[1] / guiItem.cachedTimesigMod
            guiItem.group = 0 #reset group
            if guiItem.noteheads and  0 < guiItem.cachedBaseDuration <= 192: #under 8th but not zero and the same value as the last note?
                if guiItem.cachedBaseDuration == previousGuiItem.cachedBaseDuration and not metricalPosition % 1 == 0: #metrical main position? Then it is a new group.
                    guiItem.group = 3 #Each is considered the Group End first. This will change eventually in the next steps when comparing the last with the current one.
                    if previousGuiItem.noteheads and previousGuiItem.data: #it is a real guiItem with the correct duration, not the pseudo which is any other item.
                        if previousGuiItem.group == 3: #The last item has the same duration and is marked as an EndGroup. Change it to be a normal groupmember which wanders in the middle.
                            previousGuiItem.group = 1
                        elif previousGuiItem.group == 0: #The last item was not the same duration (or wrong metrical pos) as the item before it so it was not given a group. This becomes the group start
                            previousGuiItem.group = 2
                            currentBeamGroup.append(previousGuiItem)
                            self._storeExtremePitches(previousGuiItem, currentExtremePitches)
                    self._storeExtremePitches(guiItem, currentExtremePitches)
                    currentBeamGroup.append(guiItem)
                else: #not a group member but still a duration which could become a group. That is why here are two different "else". One for the duration, one if it is the same duration as the last item.
                    for i in currentBeamGroup:
                        i.cachedGroupStemExtremePitches = currentExtremePitches
                    currentBeamGroup = []
                    currentExtremePitches = [float("inf"), -1 * float("inf")] #out of range for any pitch.
                previousGuiItem = guiItem

            else: #wrong duration or tpye
                previousGuiItem = PSEUDO
                #This else: marks the end of a group or no group at all. We can now iterrate over currentBeamGroup if it is not empty.
                #Find out the stem and beam configuration for this group and cache it so that updateRight can instruct them to draw later.
                for i in currentBeamGroup:
                    i.cachedGroupStemExtremePitches = currentExtremePitches
                currentBeamGroup = []
                currentExtremePitches = [float("inf"), -1 * float("inf")] #out of range for any pitch.

            positionUntilNow += guiItem.cachedDuration
            indexUntilNow += 1

        #the loop is over. The parameter of the last group were not set yet. Take the last set values for that, which is the last group.
        for guiItem in currentBeamGroup:
            guiItem.cachedGroupStemExtremePitches = currentExtremePitches

        self.cachedTimeSignatures[ENDMARKER_TIMESIG] = (positionUntilNow +1, len(self.leftPart) + 1) #it has this format because drawing barlines needs this as delimiter timesig. The DEFAULT_TIMESIG is just random, just use the latest item. It is not used. We just need the same data structure. + 1 because it is always after appending. + 1 cannot trigger one barline too much. This is just for drawing the gui, so it is safe anyway.
        #If we had displacement we need to update all other tracks from the first tick position of a changed displacement item. This is done by updateRight or whatever called this function.
        if forceDisplacementUpdatingOtherTracks or thereWasDisplacement: #either false or the tickindex of the first displaced item.
            self.workspace.buildDisplacementMap()
        return thereWasDisplacement

    def updateRightFromTickindexOnlyNewDisplacement(self, tickIndex):
        """This is a light variant of updateRight.
        It's only purpose is to reposition the items.
        No beaming, no recalulation, no firstPassRight."""
        self.goToTickIndex(tickIndex)
        positionUntilNow = self.leftPartTicks

        tempDisplacement = 0 #for encountering displaced items in a row. The global dictionary only stores the sum of all displacements for one tickindex and not how they are divided in a track.

        for z in reversed(self.leftPart):  #First look left: Check if we are in displacement group. If yes add to tempDisplacement so we start on the correct position updating right for the current item first item.
            if z.displacement:
                tempDisplacement += z.displacement
            else:
                break

        #Set new X positions for each item from here on until the End of the Track.
        for x in self.rightPart:
            if x.displacement:
                x.setX((positionUntilNow + tempDisplacement + self.workspace.getDisplacement(positionUntilNow, includeSelf = False)) * config.DURATION_MOD)
                tempDisplacement += x.displacement
            else:
                x.setX((positionUntilNow + self.workspace.getDisplacement(positionUntilNow, includeSelf = True)) * config.DURATION_MOD)
                tempDisplacement = 0
            positionUntilNow += x.cachedDuration

        wholeLength = positionUntilNow + self.workspace.getDisplacement(positionUntilNow, includeSelf = True) + 50 # * config.DURATION_MOD  #the last is a cosmetic addition.
        self.drawLines(wholeLength)
        self.cachedTrackWidth = wholeLength
        self.updateBarlines()
        self.drawLyrics() #most of the time this will be deactivated.

    def updateRight(self, forceDisplacementUpdatingOtherTracks = False):
        """Recalculate all item positions right of the current position.
        Does not touch the items except for beaming, stems and container
        width modifications."""
        updateAllOther = self.firstPassRight(forceDisplacementUpdatingOtherTracks) #If we had displacement we need to update all other tracks from the first tick position of a changed displacement item, which is returned by firstPassRight. At the end of this updateRight we tell all other staves to reposition themselves.
        positionUntilNow = self.leftPartTicks

        #Before updating right find out in which beaming group we are and if there is displacement going on.
        tempDisplacement = 0 #for encountering displaced items in a row. The global dictionary only stores the sum of all displacements for one tickindex and not how they are divided in a track.

        for z in reversed(self.leftPart):  #First look left: Check if we are in displacement group. If yes add to tempDisplacement so we start on the correct position updating right for the current item first item.
            if z.displacement:
                tempDisplacement += z.displacement
            else:
                break

        for y in reversed(self.leftPart):
            if y.noteheads and y.lastPassWasGroup and (not y.group == 3) and (not self.rightPart[0].cachedBaseDuration == y.cachedBaseDuration) and  not self.rightPart[0].group: #the item before that changed from beeing in a group to no group. Redraw the stem/flag to be a standalone chord
                y.updateStandaloneStemAndFlag()
                y.lastPassWasGroup = False
                break
            elif y.group == 3:  #We are at a group end
                y.lastPassWasGroup = True
                break
            elif y.group == 1:  #We are in a group
                y.updateGroupStemAndFlag()
                y.lastPassWasGroup = True
            elif y.group == 2: #Beginning of a group. We don't need to go further/backwards
                y.updateGroupStemAndFlag()
                y.lastPassWasGroup = True
                break
            else: #group = 0 or different duration
                break

        #Set new X positions for each item from here on until the End of the Track.
        for x in self.rightPart:
            if x.displacement:
                x.setX((positionUntilNow + tempDisplacement + self.workspace.getDisplacement(positionUntilNow, includeSelf = False)) * config.DURATION_MOD)
                tempDisplacement += x.displacement
            else:
                x.setX((positionUntilNow + self.workspace.getDisplacement(positionUntilNow, includeSelf = True)) * config.DURATION_MOD)
                tempDisplacement = 0

           #beaming and stems #0 means no group. 1=groupMember, 2= groupStart, 3=groupEnd.
            if x.group: #it is part of a group. Draw accordingly
                x.updateGroupStemAndFlag()
                x.lastPassWasGroup = True
            elif x.noteheads and x.lastPassWasGroup and (not x.group):  #the item changed from beeing in a group to no group. Redraw the stem/flag to be a standalone chord
                x.updateStandaloneStemAndFlag()
                x.lastPassWasGroup = False
            else: #not a group at all.
                x.lastPassWasGroup = False
            x.updateForIteration()
            positionUntilNow += x.cachedDuration

        if forceDisplacementUpdatingOtherTracks or updateAllOther:
            tmp = self.workspace.tracks[:]
            tmp.remove(self)
            for tr in tmp:
                #TODO:  updateRightFromTickindexOnlyNewDisplacement is more performant (unmeasured, so it is a guess). But is inaccurate because it does not get the correct starting point when inserting new items in a track so that the discplament items moves to the right.
                #tr.updateRightFromTickindexOnlyNewDisplacement(updateAllOther)
                tr.updateRightFromTickindexOnlyNewDisplacement(0)

        wholeLength = positionUntilNow + self.workspace.getDisplacement(positionUntilNow, includeSelf = True)
        self.drawLines(end = wholeLength)
        self.updateBarlines()
        self.drawLyrics() #most of the time this will be deactivated.
        #Send updated values to get the correct sceneRect. We don't know all track widths from here. So we call the parent score method to reset it all:
        self.cachedTrackWidth = wholeLength
        self.workspace.sendMaxTrackWidthToMain() #TODO: this can be optimized by placing this function not here but after all queued actions are done. If you insert 10 items with copy and paste we have 10 screen rects updates. However: If the system is intelligent it will not call updateRight 10 times then. So updating the sceneRect might be the smallest problem when it comes to this.

    def deleteBarlines(self):
        """This is its own function becauses toggleBarlines
        can just switch them off without redrawing"""
        for line in self.barlines:
            self.scene().removeItem(line)
        self.barlines = []

    def updateBarlines(self):
        """
        The strategy is to make use of a list of timesigs and their
        tickpositions.
        We can then draw all barlines on their position without crawling
        through the track objects.

        Update barlines redraws all barlines. It cannot redraw from
        the current position because we do not store the tickindexes of
        barlines. Anyway, this would result in for loops with if and
        tick score, which is also slow. For performance it is
        better to just draw all of them.
        """
        if config.BARLINES:  #Barlines can be switched off.
            self.deleteBarlines()

            #The cachedTimesigs stored their beginning, but not their ending. We skip the first round and then use lastRoundTimesig and not the current one. The current one indicates the end timesig.
            #timesiglist = sorted([(v,k.data) for k,v in self.cachedTimeSignatures.items()])  #create a list which is sorted after their tickindex which is [n][0]. [n][1] is the backend timesignature
            #timesiglist = sorted([(v[0],v[1], k.data) for k,v in self.cachedTimeSignatures.items()], key=lambda x: x[0])  #create a list which is sorted after their tickindex which is [n][0]. [n][1] is the backend timesignature . v[0] is the tickPositon  v[1] the index as item count
            #timesiglist = sorted((v[0],v[1], k.data) for k,v in self.cachedTimeSignatures.items())  #create a list which is sorted after their tickindex which is [n][0]. [n][1] is the backend timesignature . v[0] is the tickPositon  v[1] the index as item count
            #The version above, without key=, can't handle two timesigs in a row. Probably I should filter them out first?
            #TODO: but we had that version already. Why was that commented out and replaced without a non-key version?
            timesiglist = sorted(((v[0],v[1], k.data) for k,v in self.cachedTimeSignatures.items()), key=lambda x: x[0])  #create a list which is sorted after their tickindex which is [n][0]. [n][1] is the backend timesignature . v[0] is the tickPositon  v[1] the index as item count


            #IF SOMETHING IS WRONG WITH THE BARLINES PRINT OUT THE TIMESIGLIST!

            lastRoundTimesig = timesiglist[0][2]
            ticksAlreadyProcessed = 0
            modFactor = 2.2 if self.dualView else 1 #for 11 lines.
            for tickIndex, index, timesig in timesiglist[1:]:
                completeMeasureTick = lastRoundTimesig.nominator * lastRoundTimesig.denominator
                currentChunk = tickIndex - ticksAlreadyProcessed
                #print ("currentChunk", currentChunk)#, "tickIndex", tickIndex, "completeMeasureTick", completeMeasureTick, "ticksAlreadyProcessed", ticksAlreadyProcessed)
                nrOfMeasures = currentChunk / completeMeasureTick   #this gets the number of barlines/measure for the current chunk. In a common piece this could be the only chunk. One track 4/4.
                #print ("nrOfMeasures", nrOfMeasures, int(nrOfMeasures, ))

                #For each timesignature we have a chunk now which can be looped over
                for measure in range(int(nrOfMeasures)):
                    #graphical creation
                    line = QtGui.QGraphicsLineItem(QtCore.QLineF(0, (TRACK_SIZE * modFactor)/2 -10 , 0, 0 )) #-10 is cosmetic
                    line.setPen(PEN_BARLINE)
                    line.setEnabled(False) #Don't react to the mouse.
                    line.setParentItem(self)
                    self.addToGroup(line) #part of the track. The barlines gets its Y-root from here.

                    #parent and storing
                    self.barlines.append(line)

                    #position
                    #measure +1 is not the absolute measure but the order in the current chunk
                    linePos = (ticksAlreadyProcessed + (measure + 1)  * completeMeasureTick + self.workspace.getDisplacement((measure +1) * completeMeasureTick + ticksAlreadyProcessed, includeSelf = True)) * config.DURATION_MOD +1 #+1 is cosmetic
                    line.setPos(linePos, 0)

                    #Measure numbers are attached to barlines as child items.
                    guiNum = GuiText(text = str(measure+2))
                    guiNum.setParentItem(line)
                    guiNum.setPos(-6, -20)


                ticksAlreadyProcessed += currentChunk
                lastRoundTimesig = timesig

    def firstPassRightFromTickindex(self, tickindex):
        """First pass is to correct cached ticks so that
        _left and _right and updateRight work correctly after the
        backend changed durations/ticks not on the cursor position.
        We cannot use _left and _right here because they would
        work on the wrong values.

        But everything is correct until the first modified item.
        Which is the tickindex parameter. We don't need any update
        until then. """ #TODO: If it turns out that this is the performance bottle neck go to to 0/reset, then goToTickIndex(tickindex) and firstPass from there. But this might be even slower because it uses many ifs in goToTickIndex.
        oldindex = self.getIndex()
        self.reset() #this uses go to index 0, not tickindex!
        self.firstPassRight()
        self.goToIndex(oldindex)

    def updateRightFromTickindex(self, tickindex, forceDisplacementUpdatingOtherTracks = False):
        """Reposition all items right of tickindex.
        All cached durations must be correct"""
        oldindex = self.getIndex()
        self.goToTickIndex(tickindex)
        self.updateRight(forceDisplacementUpdatingOtherTracks)
        self.goToIndex(oldindex)

    def updateRightFromIndex(self, flatIndex, forceDisplacementUpdatingOtherTracks = False):
        oldindex = self.getIndex()
        self.goToIndex(flatIndex)
        self.updateRight(forceDisplacementUpdatingOtherTracks)
        self.goToIndex(oldindex)

    def updateRightFromStart(self, forceDisplacementUpdatingOtherTracks = False):
        oldindex = self.getIndex()
        self.reset()
        self.updateRight(forceDisplacementUpdatingOtherTracks)
        self.goToIndex(oldindex)

    def signatureReset(self):
        """From the beginning to the end update all cached signatures
        and clefs.
        This is essentially for load file. If you want to use it
        anywhere else you are creating a critical performance problem.
        This depends on having the backend cursor in the right track,
        which is the case in score.load() (Reminder: The only place
        where it matters)
        No cursor needs to be set here, eventhough load is at the
        beginning anyway."""
        #All backend data
        backendStatus = api._getActiveCursor()
        #self.prevailingTimeSignature = [TimeSignature(4, 384)] # A stack to hold tuplets of (Tickindex, TimeSig) pairs. Default 4/4, as Lilypond dictates. The default one never gets deleted if every stack modification is done by the cursor.
        #self.prevailingKeySignature = [KeySignature(20, [(3,0), (0,0), (4,0), (1,0), (5,0), (2,0), (6,0)])] #C Major by default, as Lilypond dictates. The first one never gets deleted if every stack modification is done by the cursor.
        #self.prevailingTempoSignature = [TempoSignature(120, 384)] #A stack to hold the tempo and relative to what duration. Defaults to 120 quarter notes per minute. The other stacks above for more info. #TODO: Technically it's possible to have different tempos in different tracks. Thats not how time works.
        #self.prevailingPerformanceSignature = [workspace.defaultPerformanceSignature] # A stack to hold performance data. How loud is "Forte" at the moment? How short a staccato note?
        #self.prevailingDynamicSignature = [DynamicSignature("mf")] #A stack to hold the currently valid dynamic. Which is itself just a reference to the currenctly valid PerformanceSignature
        #self.prevailingClef = [Clef("treble", "")] #For convenience. Has no usage in the backend, only in importers or other api based programs.
        timesigData = backendStatus.prevailingTimeSignature[0]
        keysigData = backendStatus.prevailingKeySignature[0]
        clefPitch = dictionaries.clefs[backendStatus.prevailingClef[0].clefString + backendStatus.prevailingClef[0].octave][1]

        for x in self.leftPart:
            if x.breakClefLoop: #a clef. no typeChecking
                clefPitch = x.pitch
                continue
            elif x.breakKeysigLoop: #a keysig. no typeChecking
                keysigData = x.data
                continue
            elif x.breakTimesigLoop: #a timesig. no typeChecking
                timesigData = x.data
                continue

            x.cachedTimesigData = timesigData
            if x.cachedTimesigData.denominator <= 192: #for example 3/8
                x.cachedTimesigMod = x.cachedTimesigData.nominator / 2
            else:
                x.cachedTimesigMod = 1
            x.cachedKeysigData = keysigData
            x.cachedClefPitch = clefPitch
            x.updateDynamic()

        for x in self.rightPart:
            if x.breakClefLoop: #a clef. no typeChecking
                clefPitch = x.pitch
                continue
            elif x.breakKeysigLoop: #a keysig. no typeChecking
                keysigData = x.data
                continue
            elif x.breakTimesigLoop: #a timesig. no typeChecking
                timesigData = x.data
                continue

            x.cachedTimesigData = timesigData
            if x.cachedTimesigData.denominator <= 192: #for example 3/8
                x.cachedTimesigMod = x.cachedTimesigData.nominator / 2
            else:
                x.cachedTimesigMod = 1
            x.cachedKeysigData = keysigData
            x.cachedClefPitch = clefPitch
            x.updateDynamic()

    def clefUpdate(self, clefPitch):
        """Update clef pitches to the next clef
        Just set all items to the clef pitch. We don't need to
        check if it is a chord or not, it doesn't matter and makes
        the program slower"""
        for x in self.rightPart:
            if x.breakClefLoop: #a clef. no typeChecking
                break
            x.cachedClefPitch = clefPitch
            x.updateDynamic() #only things that move with the pitches up and down
        self.cachedClefPitch = clefPitch #The pitch cursors needs this

    def keysigUpdate(self, keysigData):
        """Update chord cache keysigs with a new backend-keysig.
        Same principle as track.clefUpdate()"""
        for x in self.rightPart:
            if x.breakKeysigLoop: #a keysig. no typeChecking
                break
            x.cachedKeysigData = keysigData
            x.updateDynamic() #only things that move with the pitches up and down

    def timesigUpdate(self, timesigData):
        """Update item cache with a new backend-timesig.
        Same principle as track.clefUpdate()"""
        for x in self.rightPart:
            if x.breakTimesigLoop: #a timesig. no typeChecking
                return 1
            x.cachedTimesigData = timesigData
            if x.cachedTimesigData.denominator <= 192: #for example 3/8
                x.cachedTimesigMod = x.cachedTimesigData.nominator / 2
            else:
                x.cachedTimesigMod = 1
        #we do not need to do any other updates here. timesigUpdate is called by insert or delete (a timesig) which is always followed by updateRight. We have anything we need in there.

    def delete(self, backendItem):
        """Update right is done by the signal-wrapper.
        This way delete selection and in containers
        does not trigger an update right every time.
        Same is true for buildDisplacementMap"""
        if self.rightPart:
            guiItem = self.rightPart.popleft()
            #This is too strict: guiItem.data is different for upbeat and and multiMeasureRest
            #if not guiItem.data is backendItem:
            #    raise ValueError("GuiItem to delete does not match given backendItem")
            if guiItem.displacement:  #also delete from the dict
                self.displacements.remove(guiItem)
            #self.workspace.buildDisplacementMap() #don't do that now. let the signal/wrapper do it.
            guiItem.scene().removeItem(guiItem)
            if guiItem.breakClefLoop: #Trigger a re-cleffing if the deleted item was a clef.
                nowClef = api._getClef()
                self.clefUpdate(dictionaries.clefs[nowClef.clefString + nowClef.octave][1])
            elif guiItem.breakKeysigLoop: #Trigger re-accidentals if deleted item was a keysig.
                self.keysigUpdate(api._getKeysig())
            elif guiItem.breakTimesigLoop: #Trigger re-timesig if deleted item was a time sig
                del self.cachedTimeSignatures[guiItem]
                self.timesigUpdate(api._getTimesig())
            #Remove from global dict. This handles deleting of links and containers.
            self.workspace.allItems[backendItem].remove(guiItem)
            if not self.workspace.allItems[backendItem]:  #if this was the last item for this backendItem, delete the whole dict entry
                del self.workspace.allItems[backendItem]
            return guiItem

        else: #Nothing to delete
            return False

    def deleteContainerInstance(self, backendContainer):
        """Delete the instance of the most inner container.
        For the backend this is just one delete. For a gui it may
        be splitted in single guiItem deletes.

        When receiving this signal the backend-container is already
        gone. The strategy is going left until we find the next
        GuiContainerStart and delete everything until and including
        the matching GuiContainerEnd, deleting any nested container
        in the process.

        We cannot move anything in the backend during the deletion.
        """
        #First find the next container start
        containerBackendStartItem = backendContainer[0]
        while True:
            currentItem = self.getCurrentItem()
            if currentItem.breakContainerLoop and currentItem.data == containerBackendStartItem:
                break
            self._left()

        #print ("\nBefore deleting. Left:", self.leftPart, "right:", self.rightPart)

        #Now we can delete which conveniently gives us the next item automatically
        containerStack = [] #delete over nested containers
        #Begin by deleting the first ContainerStart outside the loop so the containerStack does not catch it.
        self.delete(self.getCurrentItem().data)
        while True:
            cur = self.getCurrentItem()
            typ = type(cur)
            if typ is GuiMultiMeasureRest or typ is GuiUpbeat: #some GuiItems don't work with the backendData but are synced manually. Multimeasure Rests and Upbeats.
                lastDeletedItem = self.delete(cur.dataOrg)
            else:
                lastDeletedItem = self.delete(cur.data)
            if (not containerStack) and lastDeletedItem.breakContainerLoop is None: #not false.
                break #everything including the correct ContainerEnd was deleted.
            elif lastDeletedItem.breakContainerLoop: #Nested ContainerStart
                containerStack.append(True)
            elif lastDeletedItem.breakContainerLoop is None: #Nested container End
                containerStack.pop()
        #The cursor will be corrected outside of this function.

    def _right(self):
        """Cut the first item of the right part and insert it as last
        item of the left part.
        Save various kind of data when this happens."""
        try:
            cutItem = self.rightPart.popleft()
            self.leftPartTicks += cutItem.cachedDuration
            self.leftPart.append(cutItem)
            if cutItem.displacement :
                self.cursorTempDisplacement[-1].append(cutItem.displacement)
            else:
                if not self.cursorTempDisplacement[-1] == []:
                    self.cursorTempDisplacement.append([])
            return cutItem
        except:
            return False #End of Track

    def _left(self):
        """Cut the last item of the left part and insert it as first
        item of the right part"""
        cutItem = self.leftPart.pop()
        if cutItem.displacement:
            if self.cursorTempDisplacement[-1] == []: #The end/last member of a displacement-group was detected.
                self.cursorTempDisplacement.pop() #get rid of the empty group to enter the displacement group
            try:
                self.cursorTempDisplacement[-1].pop()
            except:
                pass
        self.leftPartTicks -= cutItem.cachedDuration
        self.rightPart.appendleft(cutItem)

        if self.leftPart: #after stepping left check if there is an item left.
            return cutItem
        else:
            #self.leftPartTicks = 0
            self.cursorTempDisplacement = [[]]
            return False #Beginning of Track
