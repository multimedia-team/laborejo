# -*- coding: utf-8 -*-
import laborejocore as api

class Commands(object):
    """Change _key_x, not the function:
    Qt shortcuts do not hold a reference to a function but it copies
    the function content. So we need an additional step in between which
    can be re-defined. Putting the api. methods directly into
    key_one(self) will work but is unchangable.

    That is because we bind the functions in here to an action and do
    not create the shortcut directly.

    Other modal systems, like the numpad, use actions themselves
    and disconnect/reconnect each time the palette changes
    """
    def __init__(self):
        self.defaults()

        #Add rebindable layer functions
        self.keys = dict((number, lambda number=number: self._keys[number]()) for number, function in self._keys.items())
        self.shifts = dict((number, lambda number=number: self._shifts[number]()) for number, function in self._shifts.items())
        self.alts = dict((number, lambda number=number: self._alts[number]()) for number, function in self._alts.items())
        self.altShifts = dict((number, lambda number=number: self._altShifts[number]()) for number, function in self._altShifts.items())


    def defaults(self):
        self._keys = {
             0 : api.nothing,
             1 : api.insert1,
             2 : api.insert2,
             3 : api.insert4,
             4 : api.insert8,
             5 : api.insert16,
             6 : api.insert32,
             7 : api.insertBreve,
             8 : api.insertLonga,
             9 : api.nothing,
            }

        self._shifts = {
            0 : api.nothing,
            1 : api.insertRest1,
            2 : api.insertRest2,
            3 : api.insertRest4,
            4 : api.insertRest8,
            5 : api.insertRest16,
            6 : api.insertRest32,
            7 : api.insertRestBreve,
            8 : api.insertRestLonga,
            9 : api.nothing,
            }

        #Do not use the alts. Used by normal commands.
        self._alts = {
            0 : api.nothing,
            1 : api.prevailing1,
            2 : api.prevailing2,
            3 : api.prevailing4,
            4 : api.prevailing8,
            5 : api.prevailing16,
            6 : api.prevailing32,
            7 : api.prevailingBreve,
            8 : api.prevailingLonga,
            9 : api.nothing,
            }

        self._altShifts = {
            0 : api.nothing,
            1 : api.insertSkip1,
            2 : api.insertSkip2,
            3 : api.insertSkip4,
            4 : api.insertSkip8,
            5 : api.insertSkip16,
            6 : api.insertSkip32,
            7 : api.insertSkipBreve,
            8 : api.insertSkipLonga,
            9 : api.nothing,
            }

    def prevailing(self):
        self._keys = {
            0 : api.nothing,
            1 : api.prevailing1,
            2 : api.prevailing2,
            3 : api.prevailing4,
            4 : api.prevailing8,
            5 : api.prevailing16,
            6 : api.prevailing32,
            7 : api.prevailingBreve,
            8 : api.prevailingLonga,
            9 : api.nothing,
            }

commands = Commands()
