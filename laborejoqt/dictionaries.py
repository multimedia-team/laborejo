# -*- coding: utf-8 -*-
from collections import defaultdict
import laborejocore as api

def dur2noteheadMissing():
    return "\U0001D156"

dur2notehead = defaultdict(dur2noteheadMissing,
    [
    (12288 , "\U0001D15C"), # Maxima #WRONG
    (6144 , "\U0001D15C"), # Longa
    (3072 , "\U0001D15C"), # Breve round
    (1536 , "\U0001D15D"), # Whole
    (768 , "\U0001D157"), #Half
    (384 , "\U0001D158"), #Quarter
    (192 , "\U0001D158"), #Eighth
    (96 , "\U0001D158"), #Sixteenth
    (48 , "\U0001D158"), #1/32
    (24 , "\U0001D158"), #1/64
    (12 , "\U0001D158"), #1/128
    (6 , "\U0001D158"), #1/256
    ])

def dur2restMissing():
    return "\u258D"  #UTF8 "MultiRest" symbol. But not the multi measure rest. TODO: maybe replace with something unmusical.

dur2rest = defaultdict(dur2restMissing,
    [
    (12288 , "\U0001D13A"), # Maxima #WRONG (Breve)
    (6144 , "\U0001D13A"), # Longa  #WRONG (Breve) #TODO. I tried to use it 2 times, but it did not show.
    (3072 , "\U0001D13A"), # Breve
    (1536 , "\U0001D13B"), # Whole
    (768 , "\U0001D13C"), #Half
    (384 , "\U0001D13D"), #Quarter
    (192 , "\U0001D13E"), #Eighth
    (96 , "\U0001D13F"), #Sixteenth
    (48 , "\U0001D140"), #1/32
    (24 , "\U0001D141"), #1/64
    (12 , "\U0001D142"), #1/128
    (6 , "\U0001D143"), #1/256
    ])

def dur2flagsMissing():
    return 0

dur2flags = defaultdict(dur2flagsMissing,
    [
    (192 , "\U0001D16E"), #Eighth
    (96 , "\U0001D16F"), #Sixteenth
    (48 , "\U0001D170"), #1/32
    (24 , "\U0001D171"), #1/64
    (12 , "\U0001D172"), #1/128
    (6 , "\U0001D172"), #1/256  #Wrong.
    ])

dur2beams = defaultdict(dur2flagsMissing,
    [
    (192 , 1), #Eighth
    (96 , 2), #Sixteenth
    (48 , 3), #1/32
    (24 , 4), #1/64
    (12 , 5), #1/128
    (6 , 6), #1/256
    ])


keysigDiff2accidental = {
   None : None,  #no accidental
   0  : "\u266E", #natural
   -10 : "\u266D", #flat
   10  : "\u266F", #sharp is wrongly aligned in the unicode font. But # is even worse
   20 : "\U0001D12A", #double sharp
   -20 : "\U0001D12B", #double flat
   }


def clefsMissing():
    """Non-matching strings are returned themselves""" #TODO. do
    return None

 #Octave can be _8 or _15, ^8 or ^15

clefs = defaultdict(clefsMissing,
    #The third parameter is the graphical offset. -1 is one note-position down.
    [
    (None , None),
    ("french" , ('\U0001D11E', 172, -2)), #G Clef on the first line
    ("treble" , ('\U0001D11E', 162, 0)), #Standard G Clef
    ("treble^8" , ('\U0001D11F', 197, 0)),
    ("treble_8" , ('\U0001D120', 127, 0)),
    ("baritone" , ('\U0001D121', 112, 4)), #C Clef on the fifth line
    ("tenor" , ('\U0001D121', 122, 2)), #C Clef on the fourth line
    ("alto" , ('\U0001D121', 132, 0)), #C Clef on the middle line
    ("mezzosoprano" , ('\U0001D121', 142, -2)), #C clef on the second line
    ("soprano" , ('\U0001D121', 152, -4)), #C clef on the first line
    ("bass" , ('\U0001D122', 102, 0)),  #F Clef on the fourth line
    ("bass^8" , ('\U0001D123', 137, 0 )),
    ("bass_8" , ('\U0001D124', 67, 0)),
    ("varbaritone" , ('\U0001D122', 92, 2)),  #F Clef on the middle line
    ("subbass" , ('\U0001D122', 112, -2)),  #F Clef on the fifth line
    ("fakeDrum" , ('\U0001D125', 102, 0)), #Percussion clef in the middle, but bass clef in reality. Used for Midi.
    ("percussion" , ('\U0001D125', 132, 0)), #Percussion clef in the middle
    ])

def directivesMissing():
    return None

directives = defaultdict(directivesMissing,
    [
    #("accent", "\U0001D17B"),
    ("accent", ">"),
    #("espressivo", "\U000"),
    ("marcato", "\U0001D17F"),
    #("portato", "\U000"),
    ("staccatissimo", "\U0001D17E"),
    #("staccato", "\U0001D17C"),
    ("staccato", "•"),
    ("tenuto", "-"),
    #("prall", "\U000"),
    #("mordent", "\U000"),
    #("prallmordent", "\U000"),
    ("turn", "\U0001D197"),
    #("upprall", "\U000"),
    #("downprall", "\U000"),
    #("upmordent", "\U000"),
    #("downmordent", "\U000"),
    #("lineprall", "\U000"),
    #("prallprall", "\U000"),
    #("pralldown", "\U000"),
    #("prallup", "\U000"),
    ("reverseturn", "\U0001D198"),
    ("trill", "\U0001D196"),
    ("shortfermata", "\U0001D110-"),
    ("fermata", "\U0001D110"),
    ("longfermata", "\U0001D110+"),
    ("verylongfermata", "\U0001D110++"),
    ("upbow", "\U0001D1AB"),
    ("downbow", "\U0001D1AA"),
    ("tie", "~"),
    ])


knownStandaloneStrings = {
    "\\set suggestAccidentals = ##t" : "sugAcc-On",
    "\\set suggestAccidentals = ##f" : "sugAcc-Off",
    }

def barlinesMissing():
    return "\u2759"

barlines = defaultdict(barlinesMissing,
    [
    ("", "\u2006"), #empty space
    ("||", "\U0001D101"), #double
    ("|.", "\U0001D102"), #end
    (".|", "\U0001D103"), #open
    (".|.", "\U0001D103\U0001D102"), #open-end
    ("|.|", "\U0001D102\U0001D103"), #end-open
    (":", "\U0001D108"), #dots only
    ("dashed", "\U0001D104"), #dashed
    ("half", "\U0001D105"), # half line. this is NOT derived from the lilypond string. the item insert signal explicitly looks for "\\once \\override Staff . BarLine #'bar-size = #2 \\bar \"|\""
    ("||:", "\U0001D106"), #On Staffbreak: Outputs a double barline on the end of a line and the open repeat on the next
    ("|:", "\U0001D106"),  #normal repeat open. Should not happen since the gui only provides the special barline
    (":|", "\U0001D107"), #repeat close
    (":|:", "\U0001D107\U0001D106"), #repeat close, open. We don't use this in the gui
    (":|.|:", "\U0001D107\U0001D106"), #RepeatClose_EndOpen_RepeatOpen #This is better looking than RepeatCloseOpen. In the gui we use the same glyphes anyway.
    (":|.:", "\U0001D108\U0001D102\U0001D108"), #insertBarRepeatClose_End_RepeatOpen
    ])

voicePresets = ["", "voiceOne", "voiceTwo", "voiceThree", "voiceFour", "oneVoice",]

didYouKnow = [
    #Make the first three words matter!
    #Do not start them all with "You can..." or "...that you can", in response to the Did you know? title.
    _("You can find help within the <a href='http://laborejo.org/Support_and_Community'>Laborejo Communiy</a>"),
    _("<p>Most commands work in the appending position (last position in a track) and apply to the item before it.</p><p>Use it to apply dots, sharps and flats on the item you just inserted without moving the cursor back and forth.</p>"),
    _("<p>Learn the keyboard shortcuts! Laborejo is designed to work with the keyboard alone and with midi instruments for full speed.</p>Everytime you grab your mouse you loose concentration, precision and time."),
    _("<p>Create multi-voice staffs by adding multiple Tracks and activate the 'Merge with Above' checkbox in the track properties print section.</p><p>You better set a Voice Preset (same section) if you use merging, too.</p>"),
    _("<p>The Track Properties are a powerful tool. Make sure to learn all sub-sections.</p><p>However, if you need the screen-space hide them with Ctrl+E (View Menu)</p>"),
    _("<p>Set the unique track name (ID) to 'master' and everything in this track will be used by any track.</p> Use it for repeats, tempo signatures etc."),
    _("<p>Begin a textfield, like the metadata or a Tracks instrument name, with Lilyponds \\markup syntax for advanced Lilypond formating:</p><pre>\\markup { \"Hello\" \\sharp \"World\" }</pre>"),
    _("<p>The \"gaming cursor keys\" W, A, S, D are used to modify existing notes.</p><p>A+D (\"left/right\") to augment or diminish notes</p><p>W+S (\"up/down\") to shift them in scale. Hold Shift for shifting in octaves</p>"),
    _("<p>Laborejo itself does not show the final output.</p><p>Use PDF Preview (F3) or Export (File->Export) to see how it really looks.</p>"),
    _("<p>There is a custom export dialog in File->Export that lets you create several PDFs for individual instruments and groups</p>"),
    _("<p>Zoom with Ctrl+Mousewheel or Ctrl with Plus and Minus.</p><p>Spread/shrink the space between notes by additionaly holding the Shift key.</p>"),
    _("<p>To look around drag the screen with the middle mouse button (wheel) pressed down.</p>"),
    _("<p>Scrolling the score can be done by using Alt+Mousewheel.</p>"),
    _("<p>Temporary Shortcuts (until you close the program) can be created by hovering the mouse cursor over a menu entry and pressing a free key.</p><p>F9, F10, F11, F12 will be always free for hover shortcuts</p>"),
    _("<p>Most commands can be applied to single notes and selections equally.</p><p>Use Shift + movement-keys or the mouse rubber band to create selections.<br>Deselect with the Space Bar.</p>"),
    _("<p>There are two sets playback settings: Internal for convenience & rapidity and JACK midi for professional production.</p><p>You can switch at any time between with the toolbar-button</p>"),
    _("<p>Start Laborejo through the command line and use parameters. Check:</p><p><pre>./laborejo-qt --help</pre></p>"),
    _("<p>Every lbjs score is also a template for a new score.</p><p>Use File->Open as Template</p>"),
    _("<p>Return to the notation view at any time by pressing Escape.</p>Use this to return from editing Track Properties"),
    _("<p>There are no empty measures / bars.</p><p>Use Multi Measure Rests, Skips or Placeholders (Alt+M) instead</p>"),
    _("<p>Most keyboard shortcuts follow a pattern:</p><p>Normal key inserts or toggles.</p><p>Together with Shift it is a variant or cleans.</p><p>With Alt you get a GUI dialog with more options.</p>"),
    _("<p>For transposing instruments it is best practice to enter all notes into Laborejo how they should sound in the end.</p><p>Use the track properties transposition setting for the printout.</p>"),
    _("<p>The numpad palette is a powerful tool!</p><p>Trigger the commands by pressing the matching key from the numpad layout</p><p>Cycle through the palette with Numpad-Plus and -Minus</p>"),
    _("<p>Begin a track in the middle of a measure/bar by inserting Objects->Upbeat (Shortcut: u)</p>"),
    _("<p>For voltas aka. multiple endings just insert a Full Ending Sequence (Alt+E) and edit.</p><p>Don't fiddle around by constructing multiple endings manually.</p>"),
    _("<p>Use Repeats for the big structure(> 8 bars)</p></p>Containers for medium sized snippets (1-8 bars) or disconnected repeats (e.g. refrains)</p><p>Substitutions are good for micro-repeats, ornaments and patterns</p>"),
    _("<p>Most objects can be made transparent or hidden (for educational worksheets). Use the H key alone or with Shift and Alt.</p><p>Use Objects->Print/Lilypond to hide entire sections</p>"),
    _("<p>Toggle the insert mode to choose durations instead of inserting them (F4, Insert Mode menu).</p><p>You'll need this for MIDI input</p>"),
    _("<p>Override any objects Lilypond output code completely with<br><pre>Advanced->Lilypond Directives->Edit Lilypond</pre></p><p>Insert a blank item with Advanced->Insert Blank Item to create your custom Lilypond objects.</p>"),
    _("<p>Hide a track completely through Track Properties' Print section: 'Don't Print'</p><p>Use this to create playback only tracks.</p>"),
    ]


menus = {
    #self is the gui main.
    #in lambda functions it is "main."

    #Toolbar Only
    "actionToolbarPlayStop" : "lambda: api.play(jackMode = config.JACKMODE, keepRolling = config.TRANSPORT_KEEP_ROLLING)",
    "actionToolbarPlayFromStart" : "lambda: api.play(startTick=0, jackMode = config.JACKMODE, keepRolling = config.TRANSPORT_KEEP_ROLLING)",
    "actionToolbarEmergencyPlaybackStop" : "api.stop",
    "actionToolbarUnsetSoloMute" : "api.unsetSoloMute",
    "actionPlaySolo" : "lambda: api.playCurrentTrack(jackMode = config.JACKMODE, keepRolling = config.TRANSPORT_KEEP_ROLLING)",
    "actionPlayGroup" : "lambda: api.playCurrentGroup(jackMode = config.JACKMODE, keepRolling = config.TRANSPORT_KEEP_ROLLING)",
    "actionToolbarJackModeToggle" : "self.toggleJackMode",
    "actionToolbarJackTransportKeepRolling" : "self.toggleJackTransportKeepRolling",

    #File
    "actionView_PDF": "self.previewPDF",
    "actionQuick_View_PDF_Export_Group": "self.previewPDFSingle",
    "actionNew": "api.new",
    "actionOpen": "self.load",
    "actionOpen_Collection": "self.loadCollection",
    "actionOpen_as_Template": "self.loadAsTemplate",
    "actionSave": "self.save",
    "actionSave_As": "self.saveAs",
    "actionClose": "self.closeTabMenu", #close tab
    "actionQuit": "self.close", #close program,

    #Non Session Alternatives to the File Menu
    "actionNew_to_Session_2": "self.nsmNewToSession",
    "actionImport_to_Session": "self.nsmImportToSession",
    "actionImport_to_Session_as_Template": "self.nsmImportToSessionAsTemplate",
    "actionExport_from_Session": "self.nsmExportFromSession",
    "actionRemove_from_Session": "self.nsmRemoveFromSession",

    #Export
    "actionLilypond": "self.exportLilypond",
    "actionLilyBin" : "api.exportLilyBin",
    "actionPdf": "lambda: main.exportPDF(parts = None)",
    "actionMidi": "self.exportMidi",
    "actionArdour": "self.exportArdour",
    "actionMidi_Jack": "self.exportMidiJack",
    "actionCustom_Export": "self.customExport",

    #Import
    "actionImportLisalo": "self.importLisalo",
    "actionImportLisaloBus": "self.importLisaloBus",

    #View, Global
    "actionLilypond_Text" : "self.viewLilypondText",
    "actionSmall_Terminal" : "self.toggleSmallTerminal",
    "actionBarlines" : "self.toggleBarlines",

    #View. Per Score.
    "actionZoom_In" : "lambda: main.session.currentGuiWorkspace.zoomIn()",
    "actionZoom_Out" : "lambda: main.session.currentGuiWorkspace.zoomOut()",
    "actionWiden" : "lambda: main.session.currentGuiWorkspace.widen()",
    "actionShrinken" : "lambda: main.session.currentGuiWorkspace.shrinken()",
    #"actionCenterOnCursor" : "lambda: main.session.currentGuiWorkspace.centerOnCursor()",
    "actionLyrics" : "lambda: main.toggleVisible(\"lyrics\", main.ui.actionLyrics)",
    "actionSidebar" : "lambda: main.toggleVisible(\"sidebar\", main.ui.actionSidebar)",
    "actionMinimap" : "lambda: main.toggleVisible(\"minimap\", main.ui.actionMinimap)",
    "actionToggle_Collapse_Track" : "lambda: main.session.currentGuiWorkspace.currentTrack.toggleCollapse()",
    "actionCollapse_all_Tracks" : "lambda: main.session.currentGuiWorkspace.collapseAllTracks()",
    "actionCollapse_empty_Tracks" : "lambda: main.session.currentGuiWorkspace.collapseEmptyTracks()",
    "actionExpand_all_Tracks" : "lambda: main.session.currentGuiWorkspace.expandAllTracks()",
    "actionInvert_Collapsing" : "lambda: main.session.currentGuiWorkspace.invertCollapsing()",

    #Mode
    "actionMIDI_Note_Entry" : "api.jackMidiInToggleMute",

    #Edit
    "actionEdit_Current_Object": "api.editGui",
    "actionCopy": "api.copy",
    "actionCut": "api.cut",
    "actionPaste": "api.paste",
    "actionPaste_replace_Selection": "api.pasteReplaceSelection",
    "actionPaste_As_Link": "api.pasteAsLink",
    "actionPaste_as_One_Track_Stream": "api.pasteAsStreamWithoutTrackBreaks",
    "actionDelete": "api.delete",
    "actionDelete_Previous": "api.backspace",
    "actionDuplicate": "api.duplicate",
    "actionLink": "api.link",
    "actionMetadata": "api.metadataGui",
    "actionSelect_Track": "api.selectTrack",
    "actionSelect_All": "api.selectScore",
    "actionInvert_Selection": "api.invertSelection",
    "actionClear_Selection": "api.deselectScore",
    "actionUndo": "api.undo",
    "actionRedo": "api.redo",


    #Menu Container
    "actionAdd_Track": "api.trackAdd",
    "actionDelete_Current_Track": "lambda: api.message(message = \"Are you sure you want to delete this track? It cannot be undone.\", title = \"Delete Track\", function = api.trackDelete)",
    "actionMove_Track_Up": "api.trackSwapUp",
    "actionMove_Track_Down": "api.trackSwapDown",
    "actionToggle_Extended_Track_View": "lambda: main.session.currentGuiWorkspace.currentTrack.toggleDualView()",

    "actionPiano_Staff_Start": "api.groupPianoStart",
    "actionGrand_Staff_Start": "api.groupGrandStart",
    "actionOrchestral_Staff_Start": "api.groupOrchestralStart",
    "actionChoir_Staff_Start": "api.groupChoirStart",
    "actionSquare_Staff_Start": "api.groupSquareStart",
    "actionVoice_and_Figured_Bass_Start": "api.groupVoiceAndFiguredBassStart",
    "actionEnd_One_Group": "api.groupEnd",
    "actionClear_All_Groupings": "api.groupDeleteAll",

    "actionInsert_Empty_Container": "api.insertContainer",
    "actionInsert_Existing_Container": "api.guiChooseContainer",
    "actionDelete_Container_Instance": "api.deleteContainerInstance",
    "actionEdit_Container": "api.editContainer",
    "actionEdit_Container_Content": "self.editContainerContent",
    #"actionConvert_Selection_to_Container": "api.convertSelectionToContainer",


    #Menu Palette
    "actionPaletteNext": "self.numpadNext",
    "actionPalettePrevious": "self.numpadPrevious",

    #Menu Objects
            #Main
    "actionUpbeat": "api.insertUpbeat",
    "actionClefs": "api.insertClefGui",
    "actionKey_Signatures": "api.insertTypicalKeyGui",
    "actionCustom_Key_Signature": "api.insertKeyGui",
    "actionTime_Signatures": "api.insertTimeGui",
    "actionPerformance_Signature": "api.insertPerformanceSignature",
    "actionInstrument_Change": "api.insertInstrumentChange",
    "actionTempo_Signature": "api.insertTempo",
    "actionFree_Text": "api.markup",
        #Barlines
    "actionDouble": "api.insertBarDouble",
    "actionBarOpen": "api.insertBarOpen",
    "actionEnd": "api.insertBarEnd",
    "actionEnd_Open": "api.insertBarEndOpen",
    "actionRepeat_Open": "api.insertBarRepeatSpecialOpen",
    "actionRepeat_Close": "api.insertBarRepeatClose",
    "actionRepeat_Close_Open": "api.insertBarRepeatClose_EndOpen_RepeatOpen",
    "actionDashed": "api.insertBarDashed",
    "actionDotted": "api.insertBarDotted",
    "actionHalf": "api.insertBarHalf",
    "actionEnd_Repeat_Open": "api.insertBarRepeatClose_End_RepeatOpen",
    "actionOpen_End": "api.insertBarOpenEnd",

        #Rests
    "actionFullMeasureRest": "lambda: api.insertRestM(1)",
    "actionMultiMeasureRest": "api.insertRestM",

        #voicePresets
    "actionVoiceOne": "api.insertVoiceOne",
    "actionVoiceTwo": "api.insertVoiceTwo",
    "actionVoiceThree": "api.insertVoiceThree",
    "actionVoiceFour": "api.insertVoiceFour",
    "actionVoiceAutomatic": "api.insertVoiceAutomatic",
        #NoteDuration
    "actionSplit_in_Two": "lambda: api.splitDuplicate(2)",
    "actionSplit_in_Three": "lambda: api.splitDuplicate(3)",
    "actionCustom_Split": "api.splitDuplicate",
    "actionAdd_Dot": "api.dotAdd",
    "actionRemove_Dot": "api.dotRemove",
    "actionToggle_Prevailing_Dot" : "api.togglePrevailingDot",
    "actionToolbarNext_Becomes_Dotted": "api.toggleNextChordBecomesDotted",
    "actionAugment": "api.augment",
    "actionDiminish": "api.diminish",
    "actionTriplet": "api.toggleTriplet",
    "actionCustom_Tuplet": "api.tuplet",
    "actionClear_Tuplets": "api.tupletRemove",
    "actionTremolo_in_Two": "lambda: api.tremolo(2)",
    "actionTremolo_in_Four": "lambda: api.tremolo(4)",
    "actionCustom_Tremolo": "api.tremolo",
    "actionClean_Tremolo": "lambda: api.tremolo(1)",
        #NotePitch
    "actionAdd_Note_to_Chord": "api.addCursorNoteToChord",
    "actionRemove_Note_from_Chord": "api.deleteCursorNote",
    "actionRemove_lowest_Note_from_Chord": "api.deleteOuterNote",
    "actionRemove_highest_Note_from_Chord": "lambda: api.deleteOuterNote(lowest=False)",

    "actionAdd_Octave_to_Chord": "api.addOctaveToChord",
    "actionShift_Octave_Up": "api.shiftChordOctaveUp",
    "actionShift_Octave_Down": "api.shiftChordOctaveDown",
    "actionShift_Up": "api.shiftChordUp",
    "actionShift_Down": "api.shiftChordDown",
    "actionShift_Note_Up": "api.shiftNoteUp",
    "actionShift_Note_Down": "api.shiftNoteDown",
    "actionShift_Note_Octave_Up": "api.shiftNoteOctaveUp",
    "actionShift_Note_Octave_Down": "api.shiftNoteOctaveDown",

    "actionSharp": "api.sharpen",
    "actionFlat": "api.flatten",

    "actionReminder": "api.toggleNoteReminderAccidental",
    "actionCautionary": "api.toggleNoteCautionaryAccidental",
    "actionSuggestion_Accidentals_On": "api.insertSuggestedAccidentalsStyleOn",
    "actionSuggestion_Accidentals_Off": "api.insertSuggestedAccidentalsStyleOff",
    "actionEnharmonic_Sharp": "api.noteEnharmonicSharp",
    "actionEnharmonic_Flat": "api.noteEnharmonicFlat",


    "actionJoin_to_Chord_prevalent_duration": "api.joinToChordPrevalentDurations",
    "actionJoin_to_Chord_sum_durations": "api.joinToChordSumDurations",
    "actionTranspose": "api.transpose",
    "actionMirror": "api.mirror",

        #Rests
        #Ornaments
            #Rhythm and Dynamic
    "actionStaccato": "api.toggleStaccato",
    "actionStaccatissimo": "api.toggleFermata",
    "actionAccent": "api.toggleAccent",
    "actionEspressivo": "api.toggleEspressivo",
    "actionTenuto": "api.toggleTenuto",
    "actionMarcato": "api.toggleMarcato",
    "actionPortato": "api.togglePortato",
    "actionShortfermata": "api.toggleShortfermata",
    "actionFermata": "api.toggleFermata",
    "actionLong_Fermata": "api.toggleLongfermata",
    "actionVerylongfermata": "api.toggleVerylongfermata",
    "actionPizzicato": "api.toggleFermata",
    #"actionSnap_Piz_Bartok": "api.toggleFermata",
            #Embellishments
    "actionTrill": "api.toggleTrill",
    "actionPrall": "api.togglePrall",
    "actionMordent": "api.toggleMordent",
    "actionPrallmordent": "api.togglePrallmordent",
    "actionTurn": "api.toggleTurn",
    "actionUpprall": "api.toggleUpprall",
    "actionDownprall": "api.toggleDownprall",
    "actionUpmordent": "api.toggleUpmordent",
    "actionDownmordent": "api.toggleDownmordent",
    "actionLineprall": "api.toggleLineprall",
    "actionPrallprall": "api.togglePrallprall",
    "actionPrallup": "api.togglePrallup",
    "actionPrall_Down": "api.togglePralldown",
    "actionReverseturn": "api.toggleReverseturn",
            #Strings
    "actionUpbow": "api.toggleUpbow",
    "actionDownbow": "api.toggleDownbow",
    "actionOrnament_open": "api.toggleOpen",
    "actionStopped": "api.toggleStopped",
            #Organ
    "actionLeftheel": "api.toggleLheel",
    "actionRightheel": "api.toggleRheel",
    "actionLefttoe": "api.toggleLtoe",
    "actionRighttoe": "api.toggleRtoe",
            #Ancient
    "actionSignum_Congruentia": "api.togglesSignumcongruentiae",
    "actionIctus": "api.toggleIctus",
    "actionAccentus": "api.toggleAccentus",
    "actionCirculus": "api.toggleCirculus",
    "actionSemi_Circulus": "api.toggleSemicirculus",
    "actionAugmentum": "api.toggleAugmentum",
            #Other
    "actionEyeglasses": "api.toggleEyeglasses",
    "actionUndress_While_Playing": "api.toggleUndressWhilePlaying",
        #Dynamics
    "action5xpiano": "api.insertDynamicPPPPP",
    "action4xpiano": "api.insertDynamicPPPP",
    "actionPianissimo": "api.insertDynamicPP",
    "actionPiano": "api.insertDynamicP",
    "actionMezzo_piano": "api.insertDynamicMP",
    "actionMezzo_forte": "api.insertDynamicMF",
    "actionForte": "api.insertDynamicF",
    "actionFortissimo": "api.insertDynamicFF",
    "actionFortississimo": "api.insertDynamicFFF",
    "action4xforte": "api.insertDynamicFFFF",
    "actionCrescendo": "api.insertDynamicCrescendo",
    "actionDecrescendo": "api.insertDynamicDecrescendo",
    "actionEnd_cresc_decresc": "api.insertDynamicEndCrescendo",
    "actionTacet": "api.insertDynamicTacet",
    "actionUser_Dynamic_1": "api.insertDynamicUser1",
    "actionUser_Dynamic_2": "api.insertDynamicUser2",
    "actionUser_Dynamic_3": "api.insertDynamicUser3",

    "actionFp": "api.insertSubitoDynamicFP",
    "actionSf": "api.insertSubitoDynamicSF",
    "actionSff": "api.insertSubitoDynamicSFF",
    "actionSp": "api.insertSubitoDynamicSP",
    "actionSpp": "api.insertSubitoDynamicSPP",
    "actionSfz": "api.insertSubitoDynamicSFZ",
    #"actionRfz": "api.insertSubitoDynamicRFZ",

           #Dynamics Styles
    "actionDynamicStyle_Above_Track": "api.insertDynamicStyleUp",
    "actionDynamicStyle_Below_Track": "api.insertDynamicStyleDown",
    "actionDynamicStyle_Neutral": "api.insertDynamicStyleNeutral",
    "actionDynamicStyle_Crescendo_as_text": "api.insertDynamicStyleCrescendoText_Cresc",
    "actionDynamicStyle_Crescendo_as_hairpin": "api.insertDynamicStyleCrescondoHairpin",
    "actionDynamicStyle_Decrescendo_as_text": "api.insertDynamicStyleDecrescendoText_Decr",
    "actionDynamicStyle_Decrescendo_as_hairpin": "api.insertDynamicStyleDecrescondoHairpin",
    "actionDynamicStyle_Decrescendo_as_Diminuendo": "api.insertDynamicStyleDecrescendoText_Dim",
    "actionDynamicStyle_Decrescendo_as_Decresc": "api.insertDynamicStyleDecrescendoText_Decresc",
        #Slurs
    "actionSlur_Start_On_Off": "api.insertSlurOpen",
    "actionSlur_End_On_Off": "api.insertSlurClose",
    "actionPhrasing_Slur_Start_On_Off": "api.insertPhrasingSlurOpen",
    "actionPhrasing_Slur_End_On_Off": "api.insertPhrasingSlurClose",
            #Slur Styles
    "actionSlurStyleUp": "api.insertSlurStyleUp",
    "actionSlurStyleDown": "api.insertSlurStyleDown",
    "actionSlurStyleNeutral": "api.insertSlurStyleNeutral",
    "actionSlurStyleDashed": "api.insertSlurStyleDashed",
    "actionSlurStyleDotted": "api.insertSlurStyleDotted",
    "actionSlurStyleSolid": "api.insertSlurStyleSolid",
    "actionSlurStyleDoubleOn": "api.insertSlurStyleDoubleOn",
    "actionSlurStyleDoubleOff": "api.insertSlurStyleDoubleOff",
    "actionPhrasingSlurStyleUp": "api.insertPhrasingSlurStyleUp",
    "actionPhrasingSlurStyleDown": "api.insertPhrasingSlurStyleDown",
    "actionPhrasingSlurStyleNeutral": "api.insertPhrasingSlurStyleNeutral",
    "actionPhrasingSlurStyleDashed": "api.insertPhrasingSlurStyleDashed",
    "actionPhrasingSlurStyleSolid": "api.insertPhrasingSlurStyleDotted",
    "actionPhrasingSlurStyleDotted": "api.insertPhrasingSlurStyleSolid",
        #Ties
    #"actionTie_On_Off": "api.toggleTie",
    "actionLaissez_Vibrer_On_Off_Chord": "api.toggleLaissezVibrer",
    "actionTie_One_Note_On_Off": "api.toggleNoteTie",
    "actionTie_from_left_side_repeat_Tie": "api.repeatTie",
    #"actionLaissez_Vibrer_One_Note_On_Off": "api.toggleNoteLaissezVibrer",
        #Tie Styles
    "actionTieStyleUp": "api.insertTieStyleUp",
    "actionTieStyleDown": "api.insertTieStyleDown",
    "actionTieStyleNeutral": "api.insertTieStyleNeutral",
    "actionTieStyleDotted": "api.insertTieStyleDotted",
    "actionTieStyleDashed": "api.insertTieStyleDashed",
    "actionTieStyleSolid": "api.insertTieStyleSolid",
    "actionTieStyleWaitForNoteOn": "api.insertTieSetStyleWaitForNoteOn",
    "actionTieStyleWaitForNoteOff": "api.insertTieSetStyleWaitForNoteOff",
        #Beaming
    "actionBeam_Start": "api.toggleBeamOpen",
    "actionBeam_End": "api.toggleBeamClose",
    "actionNo_Beam": "api.toggleBeamForceNone",
    "actionForce_Beam": "api.toggleBeamForce",
        #Chord Symbols
    "actionEdit_Chord_Symbol": "api.chordSymbol",
    "actionClear_Chord_Symbol": "api.chordSymbolDelete",
    "actionMass_Insert_Chord_Symbols": "api.chordSymbolMassInsert",
        #Figured Bass
    "actionEdit_Figured_Bass": "api.figuredBass",
    "actionClear_Figured_Bass": "api.figuredBassDelete",
    "actionMass_Insert_Figured_Bass": "api.figuredBassMassInsert",

        #Fingerings
    "actionClear_All_Fingerings" : "api.fingeringAllDelete",
            #Left Hand
    "actionCustom_Finger" : "api.finger",
    "actionFinger_0" : "lambda: api.finger('t')",
    "actionFinger_1" : "lambda: api.finger(1)",
    "actionFinger_2" : "lambda: api.finger(2)",
    "actionFinger_3" : "lambda: api.finger(3)",
    "actionFinger_4" : "lambda: api.finger(4)",
    "actionFinger_5" : "lambda: api.finger(5)",
    "actionFingering_Toggle_Allow_in_Staff" : "api.toggleFingeringOverrideAllowInStaff",
    "actionFingering_Direction" : "api.insertFingeringDirection",
    "actionFingering_Add_Stem_Support" : "api.insertFingeringOverrideAddStemSupport",
    "actionFingering_Disable_Stem_Support" : "api.insertFingeringOverrideDisableStemSupport",
            #String Number
    "actionCustom_String_Number" : "api.stringNumber",
    "actionString_1" : "lambda: api.stringNumber(1)",
    "actionString_2" : "lambda: api.stringNumber(2)",
    "actionString_3" : "lambda: api.stringNumber(3)",
    "actionString_4" : "lambda: api.stringNumber(4)",
    "actionString_5" : "lambda: api.stringNumber(5)",
    "actionString_6" : "lambda: api.stringNumber(6)",
    "actionString_7" : "lambda: api.stringNumber(7)",
    "actionString_8" : "lambda: api.stringNumber(8)",
    "actionString_9" : "lambda: api.stringNumber(9)",
    "actionString_0" : "lambda: api.stringNumber(10)",
    "actionString_Toggle_Allow_in_Staff" : "api.toggleStringNumberOverrideAllowInStaff",
    "actionString_Add_Stem_Support" : "api.insertStringNumberAddStemSupport",
    "actionString_Disable_Stem_Support" : "api.insertStringNumberDisableStemSupport",
            #Right Hand
    "actionCustom_Right_Hand_Stroke_Finger" : "api.rightHandFinger",
    "actionStroke_1" : "lambda: api.rightHandFinger(1)",
    "actionStroke_2" : "lambda: api.rightHandFinger(2)",
    "actionStroke_3" : "lambda: api.rightHandFinger(3)",
    "actionStroke_4" : "lambda: api.rightHandFinger(4)",
    "actionStroke_x" : "lambda: api.rightHandFinger(5)",
    "actionStroke_Toggle_Allow_in_Staff" : "api.toggleRightHandFingerOverrideAllowInStaff",
    "actionStroke_Directions" : "api.toggleStringNumberOverrideAllowInStaff",
    "actionStroke_Add_Stem_Support" : "api.insertRightHandFingeringDirection",
    "actionStroke_Disable_Stem_Support" : "api.insertRightHandFingerDisableStemSupport",

        #Jumps
    "actionDa_Capo": "api.daCapo",
    "actionDal_Segno": "api.dalSegno",
    "actionTo_Coda": "api.toCoda",
    "actionSegno": "api.segno",
    "actionFine": "api.fine",
    "actionCoda": "api.coda",
    "actionAlternate_Ending_1": "lambda: api.alternateEnd(1)",
    "actionAlternate_Ending_2": "lambda: api.alternateEnd(2)",
    "actionCustom_Alternate_Ending": "api.alternateEnd",
    "actionAlternate_Ending_Close": "api.alternateEndClose",
    "actionFull_Dual_Ending_Sequence": "api.alternateEndingFullSequence",

        #Pedals
    "action_Sustain_On": "api.insertPedalSustainOn",
    "action_Sustain_Off": "api.insertPedalSustainOff",
    "actionSustain_Change_non_print": "api.insertPedalSustainChange",
    "actionUna_Corda_Soft_On": "api.insertPedalUnaCordaOn",
    "actionUna_Corda_Soft_Off": "api.insertPedalUnaCordaOff",
    "actionSostenuto_On": "api.insertPedalSostenutoOn",
    "actionSostenuto_Off": "api.insertPedalSustainChange",

        #Print / Lilypond
    "actionHide_Notes_Start" : "api.insertHideNotesOn",
    "actionHide_Notes_Stop" : "api.insertHideNotesOff",
    "actionHide_Barlines_Start" : "api.insertHideBarlinesOn",
    "actionHide_Barlines_Stop" : "api.insertHideBarlinesOff",


    #Menu Advanced
    "actionToggleHidden": "api.toggleHidden",
    "actionToggleTransparent": "api.toggleTransparent",
    "actionCleanHiddenTransparent": "api.cleanHiddenTransparent",

        #Substitutions
    "actionCreate_Substitutions_from_File": "self.substitutionsScoreFromFile",
    "actionSave_Substitutions_to_File": "self.substitutionsScoreToFile",
    "actionCreate_Substitution_from_Selection": "api.substitutionScoreFromSelection",
    "actionRemove_Substitution": "api.substitutionDelete",
    "actionClear_All_Directives": "api.deleteAllDirectives",
    "actionDelete_Score_Substitutions": "api.substitutionsScoreDelete", #delete with a gui.
    "actionUnfold_Substitution_permanently": "api.substitutionUnfold",

        #Standalone and WaitForChord
    "actionInsert_Lilypond": "api.insertStandalone",
    "actionInsert_Wait_For_Chord_Lilypond": "api.insertStandaloneWaitForNextChord",
    "actionEdit_Wait_For_Chord_Lilypond": "api.editStandaloneWaitForNextChord",
    "actionEdit_Lilypond": "api.editStandalone",
        ##Item Directives
    "actionCreate_Pre_Mid_Post_Directives": "api.createDirectivesGui",
    "actionEdit_Pre_Mid_Post_Directives": "api.editDirectives",
    "actionClear_Object_Directives": "api.deleteAllItemDirectives",
        ##Notes Directives
    #TODO: actionCreate_Notes_Directives
    #TODO: actionEdit_Note_Directives
    "actionClear_Notes_Directives": "api.deleteAllNoteDirectives",

        #Debug and Test
    "actionReset_and_Redraw_GUI" : "lambda: main.session.currentGuiWorkspace.fullReload()",

        #Rearrange
    "actionReverse": "api.rearrangeReverse",
    "actionReverse_Pitch": "api.reversePitch",
    "actionReverse_Rythm": "api.reverseRhythm",
    "actionShuffle": "api.rearrangeShuffle",
    "actionSort_Ascending": "api.rearrangeSortAscending",
    "actionSort_Descending": "api.rearrangeSortDescending",

        #Experimental
    "actionAutoChange": "api.experimental_addAutoChange",
    "actionSplitChordTrackToVoices": "api.experimental_splitChordTrackToSingleVoiceTrack",
    "actionSpeedUpPlayback": "lambda: api.scaleTempo(2)",
    "actionExtract_and_render_chordsymbols_to_a_new_track_with_real_chords": "api.extractAndRenderChordSymbols",

        #Midi
    "actionEdit_Default_Performance_Signature": "lambda: api.editPerformanceSignature(default=True)",
    "actionTempo_Change_Relative"  : "api.insertTempoModification",
    "actionChannel_Plus" : "api.smfChannelPlus",
    "actionChannel_Minus" : "api.smfChannelMinus",
    "actionChannel_Force"  : "api.smfChannelForce",
    "actionChannel_Clear"  : "api.smfChannelReset",
    "actionChannel_Change_Relative"  : "api.insertChannelChangeRelative",
    "actionChannel_Change_Absolute"  : "api.insertChannelChangeAbsolute",

    "actionProgram_Plus" : "api.smfProgramPlus",
    "actionProgram_Minus" : "api.smfProgramMinus",
    "actionProgram_Force"  : "api.smfProgramForce",
    "actionProgram_Clear"  : "api.smfProgramReset",
    "actionProgram_Change_Relative"  : "api.insertProgramChangeRelative",
    "actionProgram_Change_Absolute"  : "api.insertProgramChangeAbsolute",

    "actionChord_Duration_Factor_Plus"  : "api.durationFactorPlus",
    "actionChord_Duration_Factor_Minus"  : "api.durationFactorMinus",
    "actionChord_Duration_Factor_Reset"  : "api.resetDurationFactor",
    "actionChord_Velocity_Factor_Plus"  : "api.velocityFactorPlus",
    "actionChord_Velocity_Factor_Minus"  : "api.velocityFactorMinus",
    "actionChord_Velocity_Factor_Reset"  : "api.resetVelocityFactor",

        #Trigger
    "actionEdit_Trigger"  : "api.editTrigger",
    "actionClear_Trigger_from_Item"  : "api.triggerDelete",
    "actionTriggerOnlyFirst"  : "api.triggerOnlyFirstTime",
    "actionTriggerNeverFirst"  : "api.tiggerNeverFirstTime",

    #Help
    "actionAbout" : "lambda: fedit({}, 'About', 'Laborejo - Music Notation Workshop\\n\\n2011/2012 Nils Gey - http://www.laborejo.org')",

    #TODO: this is only a placeholder. Replace when we get either a nice keyboard/shortcut manager or a nicer "Did you know...?" widget?
    "actionManual" : ("lambda: fedit({}, 'Manual', '<p><b>Keys</b></p> <p>Movement<ul>  <li>Cursor Keys - Move Cursor</li> <li>Ctrl and Cursor Keys - Move measure right/left or Octave up/down</li> <li>Home - Move to track beginning</li> <li>End - Move to track end</li> <li>Page Up/Down - Move Track up or down.</li> </ul>  Any movement can be combined with the \"Shift\" Key to create a selection.</p> <p> Notes and Pitches<ul> <li>1...9 insert Note durations (1 = whole, 2 = quarter ... until 6 = Breve, 7 = Longa)</li> <li>Shift and Number - Same as above, but as rest</li> <li>Alt and Number - Choose Prevailing Duration </li> <li>Alt and Shift and Number - Insert invisible rest </li> </ul> </p>')"),
    "actionDid_you_know" : "self.showDidYouKnow",


    #Toolbar
    "actionToolbarClefTreble" : "api.insertClefTreble",
    "actionToolbarClefBass" : "api.insertClefBass",
    "actionToolbarTrackAdd" : "api.trackAdd",
    "actionToolbarTrackDelete" : "api.trackDelete",
    "actionToolbarTimeCommon" : "api.insertTime4_4",
    "actionToolbarTimeThreeFour" : "api.insertTime3_4",
    "actionToolbarTwoTwo" : "api.insertTime2_2",
    "actionToolbarKeysig" : "api.insertTypicalKeyGui",
        #Duration
    "actionToolbarWhole" : "modalcommands.commands.keys[1]",
    "actionToolbarHalf" : "modalcommands.commands.keys[2]",
    "actionToolbarQuarter" : "modalcommands.commands.keys[3]",
    "actionToolbarEighth" : "modalcommands.commands.keys[4]",
    "actionToolbarSixteenth" : "modalcommands.commands.keys[5]",
    "actionToolbarFullMeasureRest" : "lambda: api.insertRestM(1)",
    "actionToolbarWholeRest" : "api.insertRest1",
    "actionToolbarHalfRest" : "api.insertRest2",
    "actionToolbarQuarterRest" : "api.insertRest4",
    "actionToolbarEighthRest" : "api.insertRest8",
    "actionToolbarSixteenthRest" : "api.insertRest16",
    "actionToolbarToggleDot" : "api.dotToggle",
}
