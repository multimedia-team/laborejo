# -*- coding: utf-8 -*-

import sys
import subprocess, operator, os, warnings, inspect, math
from random import random, randint, shuffle
from collections import defaultdict
from re import match
from shutil import copyfile, move
from datetime import datetime
from random import choice
localepath = os.path.join(os.path.dirname(__file__), 'locale')

from PyQt4 import QtCore, QtGui
def customTranslate(one, theString, three, four):
    #self.actionForte.setText(QtGui.QApplication.translate("MainWindow", "𝆑 forte", None, QtGui.QApplication.UnicodeUTF8))
    x =  __translateOrg(one, theString, three, four)
    return x.strip("\x00")  #there are \x00 in the strings which need to removed before display. This is most likely a pyqt/Python3 bug

__translateOrg = QtGui.QApplication.translate
QtGui.QApplication.translate = customTranslate #replace Qt function with our own "translate".

import laborejocore as api #This starts the backend and automatically creates a session. But not a score/score/tracks.

#Start the Gui
from laborejoqt.designer.gui_main import Ui_MainWindow
from laborejoqt.designer.gui_stacked_musictab import Ui_stackedMusicTab
from laborejoqt.designer.gui_export import Ui_ExportDialog
from laborejoqt.designer.gui_didyouknow import Ui_DidYouKnow
#from designer.gui_megaman import Ui_Form
app = QtGui.QApplication(sys.argv) #Start qt as early as possible so that item creation on program startup works. Otherwise the instances create segfaults because qt is not ready to create QGraphicsTextItem (in items.py)
if QtCore.QLocale().languageToString(QtCore.QLocale().language()) == "German":
    translator = QtCore.QTranslator()
    translator.load("trans_de.qm", "laborejoqt/designer") #German
    app.installTranslator(translator)

from laborejoqt import config, dictionaries, modalcommands, listener
from laborejoqt import items as guiitems
from laborejoqt.constants import *
from laborejoqt.container import GuiTrack, PSEUDO
from laborejoqt.dynformcreator import fedit #Generate dialogs/formulars with a simple syntax.

#QtGui.QApplication.setFont(fontDB.font(TEXT_FONT_STRING, "", 10))  this is only for unicode symbols, the fake menu icons. Better replace them with vector images


class GuiStatusBar(QtGui.QStatusBar):
    def __init__(self, parent):
        super(GuiStatusBar, self).__init__(parent)
        self.permanent = QtGui.QLabel("Hallo Welt")
        self.addPermanentWidget(self.permanent)

    def showMessage(self, message):
        super(GuiStatusBar, self).showMessage(message)

class StartQT4(QtGui.QMainWindow):
    def __init__(self, parent=None, geometry=None, nonSessionManager = False):
        QtGui.QWidget.__init__(self, parent)
        self.nonSessionManager = nonSessionManager #Are we under control of non session manager or not?

        config.reloadPersonalConfig()  #override global qt-config data with user data.
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        #Session Management menu dependent action hiding or not
        self.notSessionActions = [self.ui.actionNew, self.ui.actionOpen, self.ui.actionOpen_Collection, self.ui.actionOpen_as_Template, self.ui.menuRecent_2, self.ui.actionClose, self.ui.actionQuit, self.ui.actionSave_As]
        self.sessionActions = [self.ui.actionNew_to_Session_2, self.ui.actionImport_to_Session , self.ui.actionImport_to_Session_as_Template , self.ui.actionExport_from_Session , self.ui.actionRemove_from_Session, ]
        if self.nonSessionManager:
            self.ui.menuFile.removeAction(self.ui.menuRecent_2.menuAction())
            self.ui.Tab.setTabsClosable (False)
            for a in self.notSessionActions:
                a.setVisible(False)
        else:
            for a in self.sessionActions:
                a.setVisible(False)

        #Now customize the window
        self.settings = QtCore.QSettings("laborejo", "laborejo")
        if geometry and match("\d+x\d+\+\d+\+\d+\Z", geometry):  #geometry is 1920x1080+0+0
            w, (h, x, y) = geometry.split("x")[0], geometry.split("x")[1].split("+")      #all strings
            self.setGeometry(int(x), int(y), int(w), int(h), )

        elif geometry and match("\d+x\d+\Z", geometry): #or geometry is 1024x768
            w,h = geometry.split("x")[0], geometry.split("x")[1]
            self.setGeometry(0, 0, int(w), int(h))

        elif self.settings.contains("geometry"): #or we have a stored one
            self.restoreGeometry(self.settings.value("geometry"))

        self.ui.actionToolbarJackModeToggle.setChecked(config.JACKMODE)
        self.ui.actionToolbarJackTransportKeepRolling.setChecked(not config.TRANSPORT_KEEP_ROLLING)
        self.ui.actionMIDI_Note_Entry.setChecked(True)

        #self.statusBar = GuiStatusBar(parent = self)

        self.ui.Tab.clear()
        self.ui.toolBar.setFont(fontDB.font(MUSIC_FONT_STRING, "", 9)) #Force a music font for the toolbar to get good looking note shapes

        self.substitutionsMenu = None

        self.session = Session()  #This is all the data.
        self.more = ""  #used in the status bar.
        self.mode = "No Mode"
        self.genericKeyActions = []  # All actions that have a shortcut like Return or Backspace. It filled automatically during shortcut creation
        self.shortcutDict = {} #for all static shortcuts.
        self.hoverShortcutDict = {} #for temporary hover shortcuts
        self.lastHover = None #the last time the mouse hovered over a menu it was this Qt.Action.
        self.recentFilesMenuActions = [] #temp storage. Only menu actions.
        self.allActions = [] #A quick access point for all actions.
        self.colors = [] #All used TabColors. this is never cleared. Who cares, there are enough colors.

        #A lilypond window that can be closed with Escape
        self.ui.lilyfloat = QtGui.QTextBrowser()  #somehow it must be part of main.ui, no matter the name. Is hidden at first.
        lilyEscapeAction = QtGui.QAction(QtGui.QIcon(), '', self)
        lilyEscapeAction.setShortcut("Escape")
        self.ui.lilyfloat.addAction(lilyEscapeAction)
        QtCore.QObject.connect(lilyEscapeAction,QtCore.SIGNAL("triggered()"), self.ui.lilyfloat.hide)

        #init small terminal
        self.ui.smallTerminal.hide()
        self.ui.actionSmall_Terminal.setChecked(False)
        if self.settings.contains("smallTerminal"):
             self.ui.terminal.setPlainText(self.settings.value("smallTerminal"))

        def terminalKeypress(event):
            if event.modifiers() == QtCore.Qt.ControlModifier and event.key() == 16777220: #Ctrl Return
                self.processSmallTerminalInput()
            else:
                terminalOriginalKeypress(event)

        terminalOriginalKeypress = self.ui.terminal.keyPressEvent
        self.ui.terminal.keyPressEvent = terminalKeypress


        #Tabs
        ######
        self.ui.Tab.tabCloseRequested.connect(self.closeTabWarning)
        self.ui.Tab.currentChanged.connect(self.tabChangeEvent)

        #Menus
        ######
        #Remember the last hovered menu action
        def temphover(action):
            self.lastHover = action
        self.ui.menubar.hovered.connect(temphover)
        self.ui.menubar.keyPressEvent = self.menuKeyPressInterceptor

        qMenumouseReleaseEvent = QtGui.QMenu.mouseReleaseEvent

        def menuMiddleMouseButtonInterceptor(menuobject, event):
            #main already exists when this is used by the mouse.
            if event.button() == QtCore.Qt.MiddleButton:
                self.ui.toolBar.addAction(self.lastHover)
            else: #not middle button. Execute.
                qMenumouseReleaseEvent(menuobject, event)

        def toolbarMiddleMouseButtonInterceptor(toolbarAction, event):
            if event.button() == QtCore.Qt.MiddleButton:
                self.ui.toolBar.removeAction(self.ui.toolBar.actionAt(event.pos()))

        QtGui.QMenu.mouseReleaseEvent = menuMiddleMouseButtonInterceptor
        QtGui.QToolBar.mousePressEvent = toolbarMiddleMouseButtonInterceptor


        #Create Numpad Widget
        self.paletteActions = [] #dynamically created menu actions.
        #All buttons in all instances (per score) are actually the same buttons.
        #Since we save them in a different widget/.ui file we need to sync them on creation in GuiScore().__init__()
        self.ui.numpadAction1 = QtGui.QAction("", self)
        self.ui.numpadAction2 = QtGui.QAction("", self)
        self.ui.numpadAction3 = QtGui.QAction("", self)
        self.ui.numpadAction4 = QtGui.QAction("", self)
        self.ui.numpadAction5 = QtGui.QAction("", self)
        self.ui.numpadAction6 = QtGui.QAction("", self)
        self.ui.numpadAction7 = QtGui.QAction("", self)
        self.ui.numpadAction8 = QtGui.QAction("", self)
        self.ui.numpadAction9 = QtGui.QAction("", self)
        self.ui.numpadAction0 = QtGui.QAction("", self)
        self.numpadWidgetActions = [self.ui.numpadAction0, self.ui.numpadAction1, self.ui.numpadAction2, self.ui.numpadAction3, self.ui.numpadAction4, self.ui.numpadAction5, self.ui.numpadAction6, self.ui.numpadAction7, self.ui.numpadAction8, self.ui.numpadAction9,]
        for numpadAction in self.numpadWidgetActions:
            #init the actions so we have them in self.allActions and they do proper display updates
            self.connectMenuAction(numpadAction, self.nothing)

        #Create Numpad Palette commands and menus
        self.numpadCreateMenu()
        self.numpadChoosePalette(keypadItems = config.palettes['dynamics']) #Set default#

        #Create User commands and menus
        self.generateUserCommands()
        self.generateRecentFilesMenu()

        #Create Extra Toolbar Buttons
        self.generateToolbar()

        #Connect all preconfigured menu actions (not generated, not user) with backend functions
        for qtAction, function in dictionaries.menus.items():
           self.connectMenuAction(eval("self.ui." + qtAction) , eval(function))

        #Create All Shortcuts
        #ACHTUNG! All keys except the numpad are lowercase. If you test for them, test for lowercase.
        #If you need an exception from this lowercase mechanism: The lowercase is produced in config.reloadPersonalConfig()

        for key, value in config.shortcuts.items(): #key is really the keyboard key. value is the action name.
            if not "lambda" in value: #this should eleminate anything with real paths, like midi export.
                value = value.replace("/", "µ") #The user is allowed to give / as action names because it fits the user dir for user scripts in ~/.laborejo/scripts. But we can't use those as variable names, we use µ instead.

            if "QtCore.Qt.KeypadModifier" in key:
                #for config: "QtCore.Qt.KeypadModifier + QtCore.Qt.Key_5" : "api.tail",
                key2 = QtGui.QKeySequence(eval(key))
                keypad = True
            else:
                key2 = key.title()
                keypad = False

            #Try to get the functiond/object. This is not an execution, just checking if it is there.
            try:
                #Non-menu actions.
                #These are mostly navigational keys like cursor,
                #home/end as well as some duplicate shortcuts like AddDot
                #which exists twice, once as menu and once on the Tab
                #key because it is near the note entry numbers

                #We need to create an action for these. Only actions can have shortcuts.
                eval(value)  #The action exists without the "self.ui." prefix.
                action = QtGui.QAction(QtGui.QIcon(), '', self)
                action.setShortcut(key2)
                self.addAction(action)
                if not keypad:
                    self.shortcutDict[key2] = action
                self.connectMenuAction(action, eval(value)) #despite the name this is not only for menu functons

            except NameError:
                #Menu Actions
                #The action already exists, we don't need to create a new one.
                action = eval("self.ui." + value)
                action.setParent(self.ui.Tab)
                action.setShortcut(key2)
                if not keypad:
                    self.shortcutDict[key2] = action

            if key in ["Return", "Tab", "Backspace", "Shift+Tab", "Enter", "Left", "Right", "Up", "Down", "Escape"]:
                self.genericKeyActions.append(action)

        #Score Substitutions Empty Menu
        self.placeholderScoreSubstitutions = QtGui.QMenu()
        tmp = self.ui.menuAdvanced.addMenu(self.placeholderScoreSubstitutions)
        tmp = self.ui.menuAdvanced.insertMenu(self.ui.actionRemove_Substitution, self.placeholderScoreSubstitutions )
        tmp.setText('Score Substitutions')

        #Score Container Empty Menu
        self.placeholderScoreContainers = QtGui.QMenu()
        tmp2 = self.ui.menuContainer.insertMenu(self.ui.actionInsert_Empty_Container, self.placeholderScoreContainers )
        tmp2.setText('Score Container')


        #Make Escape a global key
        QtGui.QShortcut(QtGui.QKeySequence("Escape"), self, self.focusOnActiveScore)

        self.prevailingActions = {
                                    1536:self.ui.actionToolbarWhole,
                                    768:self.ui.actionToolbarHalf,
                                    384:self.ui.actionToolbarQuarter,
                                    192:self.ui.actionToolbarEighth,
                                    96:self.ui.actionToolbarSixteenth,
                                  }

        #Virtual Piano
        self.ui.virtualPiano.orgFocusIn = self.ui.virtualPiano.focusInEvent
        self.ui.virtualPiano.orgFocusOut = self.ui.virtualPiano.focusOutEvent
        self.ui.virtualPiano.focusInEvent = self.customVirtualPianoFocusIn
        self.ui.virtualPiano.focusOutEvent = self.customVirtualPianoFocusOut

        #Shift interceptor
        self.virtualPianoShiftDown = False
        self.virtualPianoDuringChordEntry = False
        self.virtualPianoOriginalKeyPressEvent = self.ui.virtualPiano.keyPressEvent
        self.ui.virtualPiano.keyPressEvent = self.virtualPianoInterceptor
        self.virtualPianoOriginalKeyReleaseEvent = self.ui.virtualPiano.keyReleaseEvent
        self.ui.virtualPiano.keyReleaseEvent = self.virtualPianoReleaseInterceptor

        #Build the Qt Actions and Shortcuts.
        for key, value in config.virtualPiano.items():
            virtAction = QtGui.QAction(QtGui.QIcon(), '', self.ui.virtualPiano)
            virtAction.setShortcut(key)
            virtAction.setShortcutContext(QtCore.Qt.WidgetShortcut)
            virtAction.triggered.connect(eval(value))
            self.ui.virtualPiano.addAction(virtAction)
            #self.allActions.append(virtAction)

        #Hide the piano initially. Not only does it distract but it also acts the wrong way. The piano must get focus on activation. But if we just let it appear on startup it doesn't.
        self.ui.virtualPianoGroup.hide()


        #Create midi CC shortcut functions
        self.numpadNextPreviousMidiController = self.upDownCCFactory(ccGotHigherFunction = self.numpadNext, ccGotLowerFunction = self.numpadPrevious)

        #Show the welcome tab with a splash screen for recent files and new/load
        #TODO
        #self.welcomeTab = WelcomeTab()
        #self.ui.Tab.addTab(self.welcomeTab, "Welcome")

        #Finally disable all actions for an empty program state and show the Qt Window
        self.switchActionsOff()

        if self.settings.contains("geometry"):
            self.show()
        else:
            self.showMaximized()

        if (not self.settings.contains("DidYouKnow")) or self.settings.value("DidYouKnow") == "2":
            self.ui.DidYouKnow = DidYouKnow(parent = self, parentPyClass = self)

    def nothing(self, *args):
        pass

    def showDidYouKnow(self):
        self.ui.DidYouKnow = DidYouKnow(parent = self, parentPyClass = self)

    def numpadCreateMenu(self):
        """Generate the palettes menu.
        Normally this function is only executed on program start."""
        #First remove the old menu actions
        for menuAction in self.paletteActions:
            self.ui.menuPalette.removeAction(menuAction)
        self.paletteActions = []

        #Create a new palette menu from the config dict
        for identString, paletteDict in sorted(config.palettes.items()):
            prettyName = paletteDict["title"]
            menuAction = self.ui.menuPalette.addAction(prettyName)
            if "shortcut" in paletteDict:
                menuAction.setShortcut(paletteDict["shortcut"])
            paletteDict["action"] = menuAction #save for later
            self.paletteActions.append(menuAction)
            self.connectMenuAction(menuAction, lambda identString=identString: self.numpadChoosePalette(keypadItems = config.palettes[identString]))


    def numpadNext(self):
        reverseDict = dict((v["action"], v) for k,v in config.palettes.items())

        if self.numpadCurrentAction is self.paletteActions[-1]:  #we are already at the last position
            newAction = self.paletteActions[0]
        else:
            idx = self.paletteActions.index(self.numpadCurrentAction)
            newAction = self.paletteActions[idx+1]
        self.numpadChoosePalette(reverseDict[newAction])


    def numpadPrevious(self):
        idx = self.paletteActions.index(self.numpadCurrentAction)
        reverseDict = dict((v["action"], v) for k,v in config.palettes.items())

        if idx <= 0:  #we are already at the first position.
            newAction = self.paletteActions[-1]
        else:
            newAction = self.paletteActions[idx-1]
        self.numpadChoosePalette(reverseDict[newAction])

    def clearNumpadWidget(self):
        for action in self.numpadWidgetActions:
            action.setText("")
            try:
                action.triggered.disconnect()
            except TypeError: #qt stuff. can't disconnect if there is no connection and no way to test if there is a connection
                pass
            action.setToolTip("")
            action.setWhatsThis("")

    def numpadChoosePalette(self, keypadItems):
        """Sets a complete new keypad-palette. Unspecified values
        default to no command.

        the parameters are dicts with an int as key (literally)
        and a tuple of 5 as value:
        (function,
        QIcon-name or a very very short text,
        tooltip, #use _("translated")!
        whatsThis,
        )

        Complete dict parameter Example for one key:
        keypadItems = {1:(api.insertClefTreble, "\U0001D11E", _("Insert Treble-Clef"), "")}

        You can use unicode directly as second parameter, if your
        texteditor supports it. This is a treble clef: 𝄞
        """
        self.clearNumpadWidget()

        for numberKey, (functionString, icon, tooltip, whatsThis) in ((k,v) for k,v in keypadItems.items() if type(k) is int):
            try: #not a menu action
                eval(functionString)
                function = eval(functionString)
            except: #a menu action
                function = eval("self.ui." + functionString).trigger

            action = self.numpadWidgetActions[numberKey]
            action.setText(icon)
            action.triggered.connect(function)
            action.setToolTip(tooltip)
            action.setWhatsThis(whatsThis)

        #action is still the last action now.
        #That is enough to get all parent widgets, which are eachs tabs numpad widget
        for toolButton in action.associatedWidgets():
            toolButton.parent().setTitle(" ".join(["Numpad:", keypadItems["title"]]))
        self.numpadCurrentAction = keypadItems["action"]

    def createMidiShortcuts(self):
        assert self.session.playbackEngine == True # Only call this when we have a playback engine
        #GUI-exclusive midi bindings which have no binding to the backend and signals.
        for midiCC, functionString in config.midiCCShortcuts.items():
            try: #not a function here in self.
                eval(functionString)
                function = eval(functionString)
            except: #a function here in self.
                function = eval(".".join(["self",functionString]))
            api.cboxSmfCompatibleModule.executeCC[midiCC] = function

    def upDownCCFactory(self, ccGotHigherFunction = api.nothing, ccGotLowerFunction = api.nothing, ccStayedTheSameFunction = api.nothing):
        def f(midiVelocity, previousMidiVelocity):
            if midiVelocity > previousMidiVelocity:
                ccGotHigherFunction()
            elif midiVelocity < previousMidiVelocity:
                ccGotLowerFunction()
            else: #the same. Does happen when the value is already 0 or 127 and goes in the same direction again.
                ccStayedTheSameFunction()
        return f

    def virtualPianoInterceptor(self, qKeyEvent):
        if qKeyEvent.modifiers() == QtCore.Qt.ShiftModifier:
            self.virtualPianoShiftDown = True
        self.virtualPianoOriginalKeyPressEvent(qKeyEvent)

    def virtualPianoReleaseInterceptor(self, qKeyEvent):
        if self.virtualPianoShiftDown and qKeyEvent.modifiers() == QtCore.Qt.NoModifier: #a modifier was released?
            self.virtualPianoShiftDown = False

            if  self.virtualPianoDuringChordEntry:
                self.virtualPianoDuringChordEntry = False
                api.right()

        self.virtualPianoOriginalKeyReleaseEvent(qKeyEvent)

    def virtualPianoInsertNote(self, pitch):
        absolutePitch = api.insertNearestPrevailingNote(pitch, toScale = True)
        api.l_playback.playNote(absolutePitch, jackMode = config.JACKMODE) #always 0.5 seconds long

    def virtualPianoInsertChord(self, pitch):
        if self.virtualPianoDuringChordEntry: #there was a note before the current one with a shift down. We are in a chord adding process
            absolutePitch = api.addNearestNoteToChord(pitch, toScale = True)
        else:
            #First note of a chord
            self.virtualPianoDuringChordEntry = True
            absolutePitch = api.insertNearestPrevailingNote(pitch, toScale = True) #goes right! so we need to go left to get to the current chord
            api.left() #virtualPianoReleaseInterceptor for shift up does the right() function in the end
        api.l_playback.playNote(absolutePitch, jackMode = config.JACKMODE) #always 0.5 seconds long


    def customVirtualPianoFocusIn(self, event):
        """Is called automatically if the focus is gained by the virtual
        keyboard
        Enabling and disabling actions only works because the moment you
        click on a menu entry VirtualPiano looses the focus and all
        actions get activated again.
        """
        self.switchActionsOff()
        self.switchActionsOn(virtualPianoMode = True)
        self.originalMode = self.mode
        self.prevailingMode() #does not change self.mode
        self.ui.virtualPiano.orgFocusIn(event)

    def customVirtualPianoFocusOut(self, event):
        """Is called automatically if the focus exits from the virtual
        keyboard
        Enabling and disabling actions only works because the moment you
        click on a menu entry VirtualPiano looses the focus and all
        actions get activated again."""
        self.switchActionsOff()
        self.switchActionsOn(virtualPianoMode = False)
        self.setMode(self.originalMode)
        self.ui.virtualPiano.orgFocusOut(event)

    def processSmallTerminalInput(self):
        """A user triggered action to execute what is written in the
        small terminal text field"""
        text = self.ui.terminal.toPlainText()
        self.settings.setValue("smallTerminal", text)

        if text:
            exec(text)
            self.session.currentGuiWorkspace.workingAreaWidget.viewport().update() #Update the grahipcsview manually

    def switchActionsOff(self):
        """Deactivate nearly all Menu Actions and Shortcuts
        as long as no file is open.
        (Except recent files which are not in this list on purpose)"""
        for a in self.allActions:
            a.setEnabled(False)
        allowed = [self.ui.actionNew, self.ui.actionOpen, self.ui.actionOpen_Collection, self.ui.actionOpen_as_Template, self.ui.actionQuit, self.ui.actionChat, self.ui.actionManual, self.ui.actionAbout, self.ui.actionImportLisalo, self.ui.actionImportLisaloBus, self.ui.actionSmall_Terminal, self.ui.actionFocusOnActiveScore, self.ui.actionDid_you_know]

        for a in allowed:
            a.setEnabled(True)

    def switchActionsOn(self, virtualPianoMode = False):
        def allValidActions():
            #If playback is not running we do not need the playback related buttons:
            if not self.session.playbackEngine:
                playbackActions = set([self.ui.actionPlayGroup, self.ui.actionToolbarJackTransportKeepRolling, self.ui.actionMIDI_Note_Entry, self.ui.actionToolbarJackModeToggle, self.ui.actionToolbarPlayStop, self.ui.actionPlaySolo, self.ui.actionToolbarPlayFromStart, self.ui.actionToolbarEmergencyPlaybackStop, self.ui.actionRebind_external_Jack_Midi_connections])
            else:
                playbackActions = ()

            if virtualPianoMode:
                return (a for a in self.allActions if not (a.shortcut().toString().lower() in config.virtualPiano or a in playbackActions))
            else:
                return (a for a in self.allActions if not a in playbackActions)

        for a in allValidActions():
                a.setEnabled(True)

    def toggleBarlines(self):
        """Switch GUI barlines on or off.
        This alone is only a relay station and is not a switch in itself
        toggleBarlines in main decides what to do"""
        config.BARLINES = not config.BARLINES
        for backendWs, guiWs in self.session.scenes.items():
            guiWs.toggleBarlines(onOff = config.BARLINES)

        self.ui.actionBarlines.setChecked(config.BARLINES)

    def toggleVisible(self, qwidgetString, menu):
        qwidget = eval("self.session.currentGuiWorkspace.ui." + qwidgetString)

        #Toggle Off
        if qwidget.isVisible():
            if qwidgetString is "lyrics":  #delete all lyric items for the whole musicTab
                config.LYRICS = False
                for tr in self.session.currentGuiWorkspace.tracks:
                    tr._removeLyrics()
                self.session.currentGuiWorkspace.repositionTracks()
            qwidget.hide()
            menu.setChecked(False)
            self.session.currentGuiWorkspace.workingAreaWidget.setFocus(True)
            return False

        #Toggle On
        else:
            if qwidgetString is "lyrics":  #recreate all lyric items for the whole musicTab
                config.LYRICS = True
                for tr in self.session.currentGuiWorkspace.tracks:
                    tr.drawLyrics()
                self.session.currentGuiWorkspace.repositionTracks()
            qwidget.show()
            menu.setChecked(True)
            qwidget.setFocus(True)
            return True

    def toggleSmallTerminal(self):
        qwidget = self.ui.smallTerminal
        menu = self.ui.actionSmall_Terminal
        if qwidget.isVisible():
            qwidget.hide()
            menu.setChecked(False)
            self.focusOnActiveScore()
            return False
        else:
            qwidget.show()
            menu.setChecked(True)
            self.ui.terminal.setFocus(True)
            return True

    """
    def toggleVirtualPiano(self):
        qwidget = self.ui.virtualPianoGroup
        menu = self.ui.actionVirtual_Piano
        if qwidget.isVisible():
            qwidget.hide()
            menu.setChecked(False)
            self.focusOnActiveScore()
            return False
        else:
            qwidget.show()
            menu.setChecked(True)
            self.ui.virtualPiano.setFocus(True) #not the group. The piano itself gets focus.
            return True
    """

    def toggleJackMode(self):
        """Toggle the global jack playback mode"""
        config.JACKMODE = not config.JACKMODE
        self.ui.actionToolbarJackModeToggle.setChecked(config.JACKMODE)

    def toggleJackTransportKeepRolling(self):
        config.TRANSPORT_KEEP_ROLLING = not config.TRANSPORT_KEEP_ROLLING
        self.ui.actionToolbarJackTransportKeepRolling.setChecked(not config.TRANSPORT_KEEP_ROLLING)
        api.calfboxSetTransportKeepRolling(config.TRANSPORT_KEEP_ROLLING) #live update.


    def focusOnActiveScore(self):
        """A function called by several others that focusses on the main
        working widget, the score, of the current opened file"""
        self.session.currentGuiWorkspace.workingAreaWidget.setFocus(True)
        self.session.currentGuiWorkspace.centerOnCursor()

    def tabChangeEvent(self):
        """Is called when a tab is changed by the user or by load/new"""
        if api.core.session.workspace and isinstance(self.ui.Tab.currentWidget(), MusicTab):
            api.core.session.workspace = self.ui.Tab.currentWidget().data
            guiWs = main.session.scenes[api.core.session.workspace]
            self.ui.actionSidebar.setChecked(guiWs.ui.sidebar.isVisible())
            self.ui.actionMinimap.setChecked(guiWs.ui.minimap.isVisible())
            self.ui.actionLyrics.setChecked(guiWs.ui.lyrics.isVisible())
            guiWs.generateScoreSubstitutionsMenu()
            guiWs.generateScoreContainerMenu()
            guiWs.cursorPitch.pitch = api._getCursorPitch()
            guiWs.cursorPitch.update()
            if self.ui.lilyfloat.isVisible():
                self.ui.lilyfloat.clear()
                self.ui.lilyfloat.setText(api.generateLilypond())

    def menuKeyPressInterceptor(self, qKeyEvent):
        """If you press a key while hovering with your mouse
        over the entry it sets a new shortcut for this item.

        No modifiers allowed
        """
        if not QtGui.QApplication.keyboardModifiers() == QtCore.Qt.NoModifier:
            #Combinations of modifiers are not allowed. Also only ctrl, shift and alt.
            return None

        key = qKeyEvent.key()
        print (key)
        if (not qKeyEvent.text().title() in self.shortcutDict) and (not key in [16777236, 16777235, 16777234, 16777237, 16777222, 16777223,16777253, 16777219, 16777264, 16777265, 16777266, 16777267, 16777268, 16777269, 167772610, 167772611]): #left and right exta. #Only if the hover key is not in the standard shortcuts
            if key in self.hoverShortcutDict: #clear the old shortcut first
                self.hoverShortcutDict[key].setShortcut(QtGui.QKeySequence())
            self.hoverShortcutDict[key] = self.lastHover
            self.lastHover.setShortcut(key)
        else:
            return None


    def connectMenuAction(self, action, function, append = True):
        """Connect the menu action with the backend function,
        or qt function in a few cases like Save and Open.
        The actions already exist in their places and
        no shortcuts are getting set here.

        Also update the GraphicsView/Viewport because we deactivated
        auto update. This prevents in-between score of cursors,
        screenjumping and an over-all feeling of sickness :) """
        #self.session.currentGuiWorkspace.workingAreaWidget.setViewportUpdateMode(QtGui.QGraphicsView.MinimalViewportUpdate)
        #self.session.currentGuiWorkspace.workingAreaWidget.setViewportUpdateMode(QtGui.QGraphicsView.SmartViewportUpdate)
        #self.session.currentGuiWorkspace.workingAreaWidget.setViewportUpdateMode(QtGui.QGraphicsView.NoViewportUpdate)
        #self.session.currentGuiWorkspace.workingAreaWidget.setViewportUpdateMode(QtGui.QGraphicsView.FullViewportUpdate)
        if append:
            self.allActions.append(action)
        def funcWrap():
            function()
            try:
                self.session.currentGuiWorkspace.workingAreaWidget.viewport().update() #Update the grahipcsview manually
                self.session.currentGuiWorkspace.updateStatusBar()
            except:
                pass
        action.triggered.connect(funcWrap)
        #QtCore.QObject.connect(action, QtCore.SIGNAL("triggered()"), funcWrap)


    def defaultModalCommands(self):
        """The opposite of prevailingMode"""
        for dur, act in self.prevailingActions.items():
            act.setChecked(False)
            act.setCheckable(False)
        self.mode = "Insert"
        modalcommands.commands.defaults()

    def prevailingMode(self):
        """Number keys choose prevailing duration, alt mod inserts.
        The opposite of defaultModalCommands"""
        self.mode = "Prevailing"

        for dur, act in self.prevailingActions.items():
            act.setCheckable(True)
            act.setChecked(False)
        self.prevailingActions[api._getActiveCursor().prevailingDuration].setChecked(True)
        modalcommands.commands.prevailing()

    def setMode(self, modeString):
        if modeString == "Prevailing":
            self.prevailingMode()
        else:
            self.defaultModalCommands() #Default number keys

    def toggleMode(self):
        if self.mode == "Prevailing":
            self.defaultModalCommands() #Default number keys
        else:
            self.prevailingMode()


    def generateToolbar(self):
        """Read the toolbar dict from the config and user config file
        and convert it to toolbar buttons"""
        for key, value in config.toolbar.items():
            value = value.replace("/", "µ") #The user is allowed to give / as action names because it fits the user dir for user scripts in ~/.laborejo/scripts. But we can't use those as variable names, we use µ instead.
            #unlike shortcuts we only accept real menu actions here.
            self.ui.toolBar.addAction(eval("self.ui." + value))
            (eval("self.ui." + value)).setText(key.title())
            #self.allActions.append(eval("self.ui." + value))

    def addRecentFiles(self, fileName):
        """Add one file to the recent save file. If there are too much
        in that file delete the oldest entry"""
        if not self.nonSessionManager:
            recentList = self.settings.value("recentFilesList")
            if not recentList:
                recentList = []

            if fileName in recentList:
                return False
            else:
                recentList.append(fileName)
                if len(recentList) >6:
                    recentList.pop(0)

                self.settings.setValue("recentFilesList", recentList)
                self.generateRecentFilesMenu()

    def generateRecentFilesMenu(self):
        """Generate the recent files menu.
        This is the step after self.addRecentFiles.

        This always addes the complete recent files file to the menu."""
        if not self.nonSessionManager:

            #First remove the old files menu actions
            for menuAction in self.recentFilesMenuActions:
                self.ui.menuRecent_2.removeAction(menuAction)
            self.recentFilesMenuActions = []

            #Create a new recent files menu from the save file.
            nameList = self.settings.value("recentFilesList")
            if not nameList:
                nameList = []
            nameList.reverse()
            for fString in nameList:
                fString = fString.rstrip()
                menuAction = self.ui.menuRecent_2.addAction(fString)
                self.recentFilesMenuActions.append(menuAction)
                self.connectMenuAction(menuAction, lambda fString = fString: self.load(directLoad = fString), append = False) #Don't append to the activate/deactivate list.


    def generateUserCommands(self):
        """Parse ~/.laborejo/scripts/ and create a menu structure
        which will execute the files in there.
        This is done by creating actions and BEFORE setting shortcuts.
        So the user can bind their own shortcuts in qt-lab.config.
        The name of the action is "user_" plus the name of the path
        without  ~/.laborejo/scripts/ and the tailing ".py"
        Any / is substituted with µ
        Example:
        ~/.laborejo/scripts/somedir/temp.py would be
        self.ui.user_somedirµtemp
        or just "user_somedirµtemp" for the config/shortcut file
        """
        userScriptDir = api._getConfigDir() + "scripts/"

        allowedChars = [c for c in "abcdefghijklmnoprstuvwyxzABCDEFGHIJKLMNOPRSTUVWXYZ/"]

        def execfile(filepath):
            global_namespace = {"__file__": filepath, "__name__": "__main__"}
            with open(filepath, 'rb') as file:
                exec(compile("import laborejocore as api\n".encode() + file.read(), filepath, 'exec'), global_namespace)

        if os.path.isdir(userScriptDir):
            #menu = self.ui.menuAdvanced.addMenu('User Commands')
            #menu = self.ui.menubar.addMenu('User Commands')
            menu = self.ui.menuUser_Commands
            subs = {"":menu,} #all submenus. Starts only with the scripts/ menu which has no extra name.

            for rootOriginal, dirs, files in os.walk(userScriptDir):
                root = rootOriginal.replace(api._getConfigDir() + "scripts/", '')
                current = subs[root.split("/")[-1]]

                if dirs:
                    for submenu in dirs:
                        newSubMenu = current.addMenu(submenu)
                        subs[submenu] = newSubMenu

                for name in files:
                    if name.endswith((".py")):
                        pathOriginal = rootOriginal + "/" + name

                        path = pathOriginal.replace(".py", "")
                        path = path.replace("//", "/")
                        path = "".join([c for c in path if c in allowedChars])
                        path = path.replace(api._getConfigDir() + "scripts/", '')
                        actionName = "user_" + path
                        actionName = actionName.replace("/", "µ") #replace with a seldom symbol. / is not allowed in variable names.
                        entry = current.addAction(actionName)  #just to avoid collisions
                        entry.setText(name.replace(".py", "")) #but use a pretty name.
                        #self.connectMenuAction(entry, lambda pathOriginal=pathOriginal: exec(open(pathOriginal, encoding="utf-8").read()))
                        self.connectMenuAction(entry, lambda pathOriginal=pathOriginal: execfile(pathOriginal))
                        #self.connect(entry,QtCore.SIGNAL('triggered()'), lambda pathOriginal=pathOriginal: exec(open(pathOriginal).read()))
                        exec("self.ui." + actionName + " = entry") #build the variable name from the strings so we can assign shortcuts to it later with our normal method.

                        with open(pathOriginal, 'r', encoding="utf-8") as f: #peak into the first line of the file
                            first_line = f.readline()

                        #Only if this is a normal script file add it to the allActions list which is used to deactivate actions on program start.
                        #If you add #START as first line you can use your script anyway.
                        if not first_line.strip().startswith("#START"):
                            self.allActions.append(eval("self.ui." + actionName))
                        #vars()["self.ui." + actionName] = entry    #somehow this does not work in this scope.
        else:
            warnings.warn("Script dir not found. Creating empty " + userScriptDir + " with README file.")
            os.makedirs(userScriptDir)
            #Create small readme
            f2 = open(api._getConfigDir() + 'scripts/README', "w", encoding="utf-8")
            f2.write("Place script files with .py extensions here. You can use nested directories.\n\nThe script files can use the 'api.foobar()' namespace and the complete python syntax.\nThey work with the visible user cursor. This means what a user can do, a script can do as well.\nConsult the Laborejo manual for more information or read the api.py file for commands.\n\nExample:\nfor i in range(8):\n    api.insert4()\n    api.up()\n")
            f2.close()

    def closeTabMenu(self):
        """Close the tab through the menu or by a shortcut.
        But not by clicking with the mouse on an [x]"""
        idx = self.ui.Tab.currentIndex()
        return self.closeTabWarning(idx)

    def closeTabWarning(self, tabIndex):
        """Check if there is unsaved data in a score before closing
        the tab."""
        t = self.ui.Tab
        t.setCurrentIndex(tabIndex)
        if not tabIndex == t.currentIndex():
            raise IndexError("Tab to close is not current tab eventhough it was changed beforehand")
        GuiWs = self.session.scenes[self.ui.Tab.currentWidget().data]
        if self.session.isCurrentClean() or GuiWs.trackEdit: #It is maybe just a track/container edit. This needs no saving at all.
            del self.session.scenes[self.ui.Tab.currentWidget().data]
            api.close()
            t.removeTab(tabIndex)
            if GuiWs.trackEdit:
                self.ui.Tab.setCurrentWidget(GuiWs.parentTabWidget)
                if GuiWs.data.originalStandaloneContainer.cursorWalk: #Can we see the content inside the container?
                    GuiWs.parentGuiWorkspace.fullReload() #Since we expect container edits we must update the whole thing. Maybe slow right now, but better safe than sorry
                else: #we don't see the content. Just need to adjust the visible duration.
                    api.l_send(lambda: api.l_item.updateDynamic(GuiWs.data.originalStandaloneContainer))
                    api.l_send(lambda: api.l_item.updateDuration(GuiWs.data.originalStandaloneContainer))
                    api.l_send(lambda: api.l_score.updateTrackFromItem(GuiWs.data.originalStandaloneContainer))

                GuiWs.parentGuiWorkspace.setCurrentIndex(0) #Switch to Main View
                self.focusOnActiveScore()
        else:
            tabname = t.tabText(tabIndex)
            reply = QtGui.QMessageBox.question(self, 'Unsaved Data', "Unsaved Data in " + tabname + ".\nHow to proceed?", QtGui.QMessageBox.Save | QtGui.QMessageBox.Discard | QtGui.QMessageBox.Cancel , QtGui.QMessageBox.Cancel)
            if reply == QtGui.QMessageBox.Discard:
                del self.session.scenes[self.ui.Tab.currentWidget().data]
                api.close()
                t.removeTab(tabIndex) #Just close
            elif reply == QtGui.QMessageBox.Save:
                if self.save(): #This calls the gui dialog if there is no filename. Could be canceled as well.
                    del self.session.scenes[self.ui.Tab.currentWidget().data]
                    self.session.setSavestatusClean()
                    api.close()
                    t.removeTab(tabIndex)
                else: #If canceled just safely return to the unsaved data.
                    return False
            else: #Cancel
                return False
        if not self.session.scenes: #it is empty? No files left open?
            self.switchActionsOff()
            self.placeholderScoreSubstitutions.clear()
            self.placeholderScoreContainers.clear()
            self.session.saveStatus = set() #Force all scores to 'clean'
        return True #no return up to this point means the tab was really closed.

    def closeEvent(self, event): #does not need to be assigned or connected. This overwrites a default qt method.
        """Check if there is any unsaved data in the session before
        closing the program"""
        #Save the window geometry, even if the program is not closed because of unsaved files, it does not hurt to save it now.

        #Luckily this function gets not called at all when we use the NSM Stop button. Only when the Window Manager closes the program. We avoid an infinite loop.
        if self.nonSessionManager:
            return event.ignore()
            #os.kill(os.getpid(), SIGTERM) #Close ourselves through sigterm which is handled by NSM already and knows how to close calfbox.
            #I am unsure if this ever gets called. But just in case:
            #return True

        self.settings.setValue("geometry", self.saveGeometry())
        if self.session.saveStatus: #There is dirty data.
            #Offer to cancel, Quit Anyway or SaveAll and then Quit.
            reply = QtGui.QMessageBox.question(self, 'Unsaved Data', "One or more files are unsaved. How to proceed?\nWarning: Unnamed Files will be discarded.", QtGui.QMessageBox.SaveAll | QtGui.QMessageBox.Discard | QtGui.QMessageBox.Cancel , QtGui.QMessageBox.Cancel)

            if reply == QtGui.QMessageBox.Discard:
                event.accept() #Just quit
            elif reply == QtGui.QMessageBox.SaveAll:
                api.saveAll() #this never fails. It has no user interaction. No test necessarry.
                event.accept()
            else: #Cancel
                event.ignore()
        else: #Everything is clean and saved. Good Bye.
            event.accept()

    #Save, Load, Export
    #####
    def new(self, backendWorkspace):
        """create a new tab with an empty, unsaved score
        this function gets called by the backend/api. Just send
        api.new and this will be executed automatically.
        DON'T call it directly from the GUI."""
        if backendWorkspace.lastKnownFilename:
            fileName = backendWorkspace.lastKnownFilename
        else:
            fileName = "unnamed"
        workspace = GuiScore(backendWorkspace)
        self.session.scenes[backendWorkspace] = workspace #This must be done early. loadBackendData already depends on existing session.foo vars.
        workspace.loadBackendData() #This is not load a file but create the gui from the backend data.
        workspace.fileName = fileName
        if self.nonSessionManager:
            tabIndex = self.ui.Tab.addTab(workspace, os.path.basename(fileName))
        else:
            tabIndex = self.ui.Tab.addTab(workspace, fileName)
        self.ui.Tab.setCurrentIndex(tabIndex)
        c = self.randomColor()
        pixmap = QtGui.QPixmap(20, 20)
        pixmap.fill(QtGui.QColor(c))
        workspace.icon = QtGui.QIcon(pixmap)
        self.ui.Tab.tabBar().setTabIcon(tabIndex, workspace.icon)
        workspace.updateGui() #Update Track Properties etc.
        workspace.cursorPitch.update()
        workspace.cursorPosition.update()
        self.session.setSavestatusClean()
        self.switchActionsOn() #We have at least one file now, everything is allowed.

    def editContainerContent(self):
        #Replace the current scene with a temporary editing chain
        #to edit a single container, be it an atomic container or single-
        #track edit.
        mainGuiWorkspace = self.session.currentGuiWorkspace
        w = self.ui.Tab.currentWidget()
        self.session.currentGuiWorkspace.setCurrentIndex(1) #Switch to Container View
        backendWorkspace = api.createContainerWorkspace(container = None, fullLoad = True)
        tempGuiWorkspace = GuiScore(backendWorkspace) #This is a full GuiScore.
        self.session.scenes[backendWorkspace] = tempGuiWorkspace #This must be done early. loadBackendData already depends on existing session.foo vars.

        tempGuiWorkspace.loadBackendData() #This is not load a file but create the gui from the backend data.
        tempGuiWorkspace.fileName = ""

        tempGuiWorkspace.trackEdit = True #Makes closing a one-click action regardless of unsaved data or not.
        tempGuiWorkspace.parentTabWidget = w
        tempGuiWorkspace.parentGuiWorkspace = mainGuiWorkspace

        tabIndex = self.ui.Tab.addTab(tempGuiWorkspace, tempGuiWorkspace.data.score.container[0].uniqueContainerName + " (" + mainGuiWorkspace.data.lastKnownFilename + ")")
        self.ui.Tab.setCurrentIndex(tabIndex)

        tempGuiWorkspace.updateGui() #Update Track Properties etc.
        tempGuiWorkspace.cursorPitch.update()
        tempGuiWorkspace.cursorPosition.update()
        self.focusOnActiveScore()

    def load(self, directLoad = None, asTemplate = False, fixedColor = None, newTabName = None):
        """Present the gui load dialog,
        ask the api to open the filepath"""

        if directLoad:
            if type(directLoad) is list:
                fileNameList = directLoad
            else:
                fileNameList = [directLoad]
        else:
            fileNameList = QtGui.QFileDialog.getOpenFileNames(self, "Open File(s)", self.session.last_open_dir, "Laborejo Score Files (*.lbjs);;All Files (*.*)")

        if fileNameList: #not cancelled
            atLeastOneFileOpened = False

            #Special Case: There is only one file open, it is unnamed but savestatus is clean. This means it is a new, unmodified file.
            #In that case close that empty file.
            if len(self.session.scenes.items()) == 1 and (not self.session.saveStatus) and self.session.currentGuiWorkspace.fileName is "":
                self.closeTabWarning(0) #despite the name this will not result in any GUI or warning. SaveStatus is all good.

            for fileName in fileNameList:
                fileName = os.path.abspath(fileName)
                #first check if this file is already open in this GUI. Templates can always be opened.
                if self.session.isFilenameAvailable(fileName) or asTemplate:
                    backendWorkspace = api.load(fileName, force = False, asTemplate = asTemplate) #this returns False if the user chose a non-laborejo file. Only use for testing or emergency.
                    if backendWorkspace:
                        workspace = GuiScore(backendWorkspace)
                        self.session.scenes[backendWorkspace] = workspace #This must be done early. loadBackendData already depends on existing session.foo vars.
                        workspace.loadBackendData()
                        if asTemplate:
                            # The backend does not know about templates. It just has open and new. New has no filename, open has one. A template is a gui construct which gives a "new" file to the user which already has content.
                            workspace.fileName = ""
                            backendWorkspace.lastKnownFilename = "" #We switch the backend name to "new" here.
                            if self.nonSessionManager:
                                n = newTabName if newTabName else os.path.basename(fileName)
                                tabIndex = self.ui.Tab.addTab(workspace, n)
                            else:
                                tabIndex = self.ui.Tab.addTab(workspace, "unnamed")
                        else:
                            workspace.fileName = fileName
                            if self.nonSessionManager:
                                tabIndex = self.ui.Tab.addTab(workspace, os.path.basename(fileName))
                            else:
                                tabIndex = self.ui.Tab.addTab(workspace, fileName)
                            self.addRecentFiles(fileName)
                        self.ui.Tab.setCurrentIndex(tabIndex)
                        c = fixedColor if fixedColor else self.randomColor()
                        pixmap = QtGui.QPixmap(20, 20)
                        pixmap.fill(QtGui.QColor(c))
                        workspace.icon = QtGui.QIcon(pixmap)
                        self.ui.Tab.tabBar().setTabIcon(tabIndex, workspace.icon)
                        workspace.updateGui()
                        workspace.cursorPitch.update()
                        workspace.cursorPosition.update()
                        self.session.setSavestatusClean()
                        atLeastOneFileOpened = True
                        self.session.last_open_dir = os.path.dirname(fileName)
                else:
                    warnings.warn("File " +  fileName +  "is already open")
                    return False

            if atLeastOneFileOpened:
                self.switchActionsOn() #We have at least one file now, everything is allowed.
                return True

    def loadAsTemplate(self, directLoad = None):
        self.load(directLoad, asTemplate = True)

    def loadCollection(self, lbjPathList = None):
        """Load one ore more lbj files as seperate lbjs workspaces"""

        if lbjPathList:
            lbjPathList = lbjPathList
        else:
            lbjPathList = QtGui.QFileDialog.getOpenFileNames(self, "Open File(s)", self.session.last_open_dir, "Laborejo Collection Files (*.lbj);;All Files (*.*)")

        for lbj in lbjPathList:
            lbjsFiles = api.col_getAllFilepaths(lbj)
            fixedColor = self.randomColor()
            for lbjs in lbjsFiles:
                self.load(directLoad = lbjs, asTemplate = False, fixedColor = fixedColor)

    def previewPDF(self):
        api.previewPDF(pdfviewer = config.pdfviewer)

    def previewPDFSingle(self):
        api.previewPDFSingle(pdfviewer = config.pdfviewer)

    def substitutionsScoreFromFile(self):
        fileName = QtGui.QFileDialog.getOpenFileName(self, "Choose File to convert to Substitutions", self.session.last_subst_dir, "Laborejo Files (*.lbj *.lbjs);;All Files (*.*)")
        if fileName:
            api.substitutionsScoreFromFile(fileName)
            self.session.last_subst_dir = os.path.dirname(fileName)

    def substitutionsScoreToFile(self):
        fileName = QtGui.QFileDialog.getSaveFileName(self, "Save Laborejo Substitutions as Score.", self.session.last_save_dir , "Laborejo Score (*.lbjs)")
        if fileName:
            self.session.last_subst_dir = os.path.dirname(fileName)
            api.substitutionsScoreToFile(filepath = fileName)

    def save(self, saveAs = False):
        """The gui save dialog to get a filepath or direct save.
        Save is always for the current active workspace/score"""
        currentWs = self.session.currentGuiWorkspace
        if (not currentWs.fileName) and currentWs.data.lastKnownFilename:
            currentWs.fileName = currentWs.data.lastKnownFilename
        if saveAs or not currentWs.data.lastKnownFilename: #TODO: was just currentWs.filename
            fileName = QtGui.QFileDialog.getSaveFileName(self, "Save Laborejo Score file.", self.session.last_save_dir , "Laborejo Score (*.lbjs)")
            if fileName: #Not pressed cancel?
                if not os.path.splitext(fileName)[1]: #no file extension given?
                    fileName = fileName + ".lbjs"
                x = api.save(fileName)
                currentWs.fileName = fileName
                self.ui.Tab.setTabText(self.ui.Tab.currentIndex(), fileName)
                self.addRecentFiles(fileName)
                self.session.setSavestatusClean() #The currently active file will be set clean.
                self.session.last_save_dir = os.path.dirname(fileName)
                return True
            else: #User pressed cancel
                return False
        elif currentWs.fileName:
            api.save(currentWs.fileName)
            self.session.setSavestatusClean() #The currently active file will be set clean.
            if self.nonSessionManager and not self.session.saveStatus: #no unclean files left?
                self.session.ourNsmClient.setDirty(False)
            return True

    def saveAs(self):
        self.save(saveAs = True)

    def customExport(self):
        """A gui go click around which results in a variety of export
        parameters."""
        dialog = ExportDialog() #TODO: does this need the refenrece to the current backend score or is it enough to use api. Api. should be enough since you can't change the active score while the export dialog is active
        dialog.exec_() #show as modal (blocking) dialog. Works with tiling and dynamic window managers as well.

    def exportMidi(self):
        fileName = QtGui.QFileDialog.getSaveFileName(self, "Export Score as midi. 'Simple' Track Settings will be used. Give full name including file extension.", self.session.last_export_dir, "*.mid *.midi *.MID *.MIDI;;All Files (*.*)")
        if fileName:
            api.exportMidi(fileName)
            self.session.last_export_dir = os.path.dirname(fileName)

    def exportArdour(self):
        dirName = QtGui.QFileDialog.getExistingDirectory(self, caption = "Export Score as Ardour 3 Session. Choose a directory where ANOTHER directory will be created.", directory = self.session.last_export_dir)
        if dirName:
            api.exportArdourThree(dirName)
            self.session.last_export_dir = dirName

    def exportMidiJack(self):
        fileName = QtGui.QFileDialog.getSaveFileName(self, "Export Score as midi. 'Jack' Track Settings will be used. Give full name including file extension.", self.session.last_export_dir, "*.mid *.midi *.MID *.MIDI;;All Files (*.*)")
        if fileName:
            api.exportMidi(fileName, jackMode = True)
            self.session.last_export_dir = os.path.dirname(fileName)

    def exportLilypond(self):
        fileName = QtGui.QFileDialog.getSaveFileName(self, "Export complete Laborejo Score as Lilypond. Give full name including file extension.", self.session.last_export_dir, "*.ly;;All Files (*.*)")
        if fileName:
            api.exportLilypond(fileName)
            self.session.last_export_dir = os.path.dirname(fileName)

    def exportPDF(self, parts):
        partsString = " " + parts.upper() + " Parts will be derived from the filename." if parts else ""
        fileName = QtGui.QFileDialog.getSaveFileName(self, "Export complete Laborejo Score as PDF (Lilypond)"+partsString, self.session.last_export_dir, "*.pdf;;All Files (*.*)")
        if fileName:
            api.exportPDF(fileName, parts = parts)
            self.session.last_export_dir = os.path.dirname(fileName)

    def viewLilypondText(self):
        if self.ui.lilyfloat.isVisible():
            self.ui.lilyfloat.hide()
        else:
            self.ui.lilyfloat.setText(api.generateLilypond())
            self.ui.lilyfloat.show()


    def importWrapperSingleFile(self, function):
        """Without the session manager this returns False.
        WIth nsm and Cancel it returns None

        The given function must be a api. function that creates
        and switches to a new file so we can call api.save() directly

        This functions add one parameter, the new save path.
        So expect it in your lambda. For standalone mode this will be
        None.
        """
        if self.nonSessionManager:
            ret = api.l_form.generateForm({"name" : (" ", "")}, _("Score Name"), _("Enter a name for the new score"))
            if ret:
                filepath = os.path.join(self.session.ourNsmClient.states.pathBeginning, os.path.splitext(ret["name"])[0] + ".lbjs")
                if os.path.exists(filepath):
                    filepath = os.path.join(self.session.ourNsmClient.states.pathBeginning, os.path.splitext(ret["name"])[0] + str(datetime.now()) + ".lbjs")
                r = function(filepath)
                if r:
                    api._getWorkspace().lastKnownFilename = filepath
                    api.save(filepath)
                return r
            else:
                return False
        else:
            return function(None)

    def importLisalo(self):
        fileName = QtGui.QFileDialog.getOpenFileName(self, "Import Lisalo File", self.session.last_import_dir, "Lisalo Files (*.lsl);;All Files (*.*)")
        if fileName:
            self.session.last_import_dir = os.path.dirname(fileName)
            return self.importWrapperSingleFile(lambda newSavePath: api.importLisalo(fileName, newSavePath = newSavePath))

    def importLisaloBus(self):
        fileName = QtGui.QFileDialog.getOpenFileName(self, "Import Lisalo File", self.session.last_import_dir, "Lisalo Files (*.lsl);;All Files (*.*)")
        if fileName:
            self.session.last_import_dir = os.path.dirname(fileName)
            return self.importWrapperSingleFile(lambda newSavePath: api.importLisalo(fileName, trackPerBus = True, newSavePath = newSavePath))

    def nsmLoad(self, pathBeginning, clientId):
        """This is the init load. It happens exactly once per instance
        and we get the clientId which must be saved immediatly because
        the laborejo-qt starter, which is still active at this point
        in runtime, depends on it and uses it to give jack prefixes
        to the backend when creating calfbox"""
        self.session.nsmClientId = clientId #will be used for later to create jack ports
        potentialPath = os.path.join(pathBeginning, "nsm.lbjs")
        #check if the core file is there. If not we assume it is a new session and create a file with this name.
        self.nsmWhatToLoad = None
        if os.path.exists(potentialPath):
            self.nsmWhatToLoad = "existing"
            #There is at least one lbjs file. Load them all.
            #self.load(directLoad = [os.path.join(pathBeginning, score) for score in os.listdir(pathBeginning) if score.endswith(".lbjs")])
            return True, "*.lbjs"
        else:
            if os.path.isdir(pathBeginning): #The directory already exists but there is no nsm.lbjs?!
                if os.listdir(pathBeginning):
                    return False, "There was no 'nsm.lbjs' in " + pathBeginning + " but other files were present. Suspecting modification by instance not under session control. Abort. Please clean directory manually first."
                #else: #The directory is already there but was empty. That is uncommon but not wrong. Go on.
                #    pass
            else: #this is indeed the first time for Laborejo in this session. Everything is fine. Create dir
                os.makedirs(pathBeginning)

        #If we came to this point there is an empty directory pathBeginning. We create a new, empty, lbjs file there.
        self.nsmWhatToLoad = "new"
        #api.new(filepath = potentialPath)
        return True, "nsm.lbjs"

    def nsmActuallyLoadFirstFiles(self):
        """This is a separate function than nsmLoad because
        we load in two stages. First the initial load which remembers
        which gets the client id and then we actually restore all the
        files
        """
        assert self.nsmWhatToLoad
        if self.nsmWhatToLoad == "existing":
            self.load(directLoad = [os.path.join(self.session.ourNsmClient.states.pathBeginning, score) for score in os.listdir(self.session.ourNsmClient.states.pathBeginning) if score.endswith(".lbjs")])
        elif self.nsmWhatToLoad == "new":
            api.new(filepath = os.path.join(self.session.ourNsmClient.states.pathBeginning, "nsm.lbjs"))
        else:
            raise ValueError


    def nsmSave(self, pathBeginning):
        ret = api.saveAll()
        if ret:
            self.session.setAllClean() #NSM clean status is done by nsmclient.py
            return True, "*.lbjs"
        else:
            return False, " ".join(["Not all files in", pathBeginning, "were saved. Reason unknown."])

    def nsmRemoveFromSession(self):
        """Remove the current tab/file from the session.
        But make a copy in a sub-backup-dir.
        """
        removedDir = os.path.join(self.session.ourNsmClient.states.pathBeginning, "removed")
        if not os.path.exists(removedDir):
            os.makedirs(removedDir)

        oldFilepath = self.session.currentGuiWorkspace.data.lastKnownFilename #A full path

        if self.closeTabMenu(): #including all the standalone stuff like warning for unsaved changes. But asking for a filename will never happen since all nsm-laborejo files have names.
            oldfile = os.path.join(removedDir, os.path.basename(oldFilepath))
            try:
                move(oldFilepath, os.path.join(removedDir, str(datetime.now())+"."+os.path.basename(oldFilepath)))
            except IsADirectoryError: #happens if you remove/close a container edit tab. That never was a real file.
                pass
            except FileNotFoundError: #if the initial nsm.lbjs was never saved
                pass

            #Do not allow nsm.lbjs to be closed. It is the critical marker file.
            mainFilepath = os.path.join(self.session.ourNsmClient.states.pathBeginning, "nsm.lbjs")
            if oldFilepath == mainFilepath: #nsm.lbjs was closed.
                api.new(filepath = mainFilepath)
                api.save(filepath = mainFilepath)
        #else:
            #user cancelled. Nothing happens.

    def nsmImportToSession(self):
        """Import an existing lbjs file to the session and its directory
        First let the user select a file, copy that to our nsm dir.
        After that use the normal load mechanism, as in nsmLoad.
        """
        openedFileNames = QtGui.QFileDialog.getOpenFileNames(self, "Import Lbjs Score File", self.session.last_import_dir, "Laborejo Score (*.lbjs);;All Files (*.*)")
        importList = []
        if openedFileNames:
            for srcfileName in openedFileNames:
                newFilepath = os.path.join(self.session.ourNsmClient.states.pathBeginning, os.path.basename(srcfileName))
                copyfile(srcfileName, newFilepath)
                importList.append(newFilepath)
            #We now have a copies in our session dir. We can open them directly.
            self.load(directLoad = importList)
            self.session.last_import_dir = os.path.dirname(srcfileName)
            return True
        else:
            return False

    def nsmExportFromSession(self):
        """Export the current tab/file to a file outside the session dir
        Changes nothing in the session. Does not even open the exported
        file in a new tab. Fire and forget.
        """
        currentWs = self.session.currentGuiWorkspace
        fileName = QtGui.QFileDialog.getSaveFileName(self, "Export Laborejo Score file.", self.session.last_export_dir , "Laborejo Score (*.lbjs)")
        if fileName: #Not pressed cancel?
            if not os.path.splitext(fileName)[1]: #no file extension given?
                fileName = fileName + ".lbjs"
            api.save(fileName)
            self.session.last_export_dir = os.path.dirname(fileName)
            return True
        else: #User pressed cancel
            return False

    def nsmNewToSession(self):
        """Create an empty file in the session dir and open it.
        We can already depend on the existing laborejo session dir
        since self.nsmLoad() was called"""
        ret = api.l_form.generateForm({"name" : (" ", "")}, _("Score Name"), _("Enter a name for the score"))
        if ret:
            filepath = os.path.join(self.session.ourNsmClient.states.pathBeginning, os.path.splitext(ret["name"])[0] + ".lbjs")
            api.new(filepath)

    def nsmImportToSessionAsTemplate(self):
        """same as ImportToSession, only as template"""
        openedFileNames = QtGui.QFileDialog.getOpenFileNames(self, "Import Lbjs Score File", self.session.last_import_dir, "Laborejo Score (*.lbjs);;All Files (*.*)")
        if openedFileNames:
            for srcfileName in openedFileNames:
                self.importWrapperSingleFile(lambda newSavePath: self.load(directLoad = srcfileName, asTemplate = True, newTabName = os.path.splitext(os.path.basename(newSavePath))[0] + ".lbjs"))
            self.session.last_import_dir = os.path.dirname(srcfileName)
            return True
        else:
            return False

    def randomColor(self):
        """Generates a nice pastell color"""
        def newColor():
            red, green, blue = randint(1, 255), randint(1, 255), randint(1, 255)
            #red, green, blue = 205 + random() * 50, 205 + random() * 50, 205 + random() * 50,
            alpha = 91 + random() * 100
            return (red, green, blue, alpha)
        c = newColor()
        while c in self.colors:
            c = new()
        self.colors.append(c)
        return QtGui.QColor(c[0], c[1], c[2], 255)

class ExportDialog(QtGui.QDialog):
    def __init__(self):
        super(ExportDialog, self).__init__()
        self.ui = Ui_ExportDialog()  #self.ui is the node to all qt widgets in this tab.
        self.ui.setupUi(self)
        self.ui.warningLabel.hide()

        #self.setWindowFlags(QtCore.Qt.Dialog)
        #self.setModal(True)

        self.saveOriginalSettings()

        #Set initial tab status and connect format radio buttons to switch the tab.
        #indices: 0-lilypond, 1-midi, 2-other
        self.ui.pdf.clicked.connect(lambda: self.ui.settingsTab.setCurrentIndex(0))
        self.ui.pdf.toggled.connect(lambda: self.ui.preview.setEnabled(self.ui.pdf.isChecked()))
        self.ui.lilypond.clicked.connect(lambda: self.ui.settingsTab.setCurrentIndex(0))
        self.ui.lilybin.clicked.connect(lambda: self.ui.settingsTab.setCurrentIndex(0))
        self.ui.midijack.clicked.connect(lambda: self.ui.settingsTab.setCurrentIndex(1))
        self.ui.midisimple.clicked.connect(lambda: self.ui.settingsTab.setCurrentIndex(1))

        #The preview buttons gets a hover to indicate that you don'T need a filepath
        self.ui.preview.setEnabled(self.ui.pdf.isChecked())

        def leavePreview(event):
            if self.ui.preview.isEnabled(): self.ui.path.setEnabled(True)
            event.accept()

        def enterPreview(event):
            if self.ui.preview.isEnabled(): self.ui.path.setEnabled(False)
            event.accept()

        self.ui.preview.leaveEvent = leavePreview
        self.ui.preview.enterEvent = enterPreview

        #Parts Radio Button:
        self.ui.pdf.clicked.connect(self.populatePathComboBox)
        self.ui.lilypond.clicked.connect(self.populatePathComboBox)
        self.ui.lilybin.clicked.connect(self.populatePathComboBox)
        self.ui.midijack.clicked.connect(self.populatePathComboBox)
        self.ui.midisimple.clicked.connect(self.populatePathComboBox)

        #Auto-Update the filepath extension when changing the format
        self.ui.pdf.clicked.connect(self.correctCurrentPathExtension)
        self.ui.lilypond.clicked.connect(self.correctCurrentPathExtension)
        self.ui.lilybin.clicked.connect(self.correctCurrentPathExtension)
        self.ui.midijack.clicked.connect(self.correctCurrentPathExtension)
        self.ui.midisimple.clicked.connect(self.correctCurrentPathExtension)

        #Path
        self.ui.lilybin.toggled.connect(lambda: self.ui.path.setEnabled(not self.ui.lilybin.isChecked()))
        self.ui.lilybin.toggled.connect(lambda: self.ui.pathOpen.setEnabled(not self.ui.lilybin.isChecked()))

        self.ui.path.setDuplicatesEnabled(False)

        self.ui.pathOpen.clicked.connect(self.openPathButton) #Open button

        #Initiliaze safed values
        self.lilypondTemplates = list(api.getTemplateList(extension = "ly"))
        self.ardourTemplates = list(api.getTemplateList(extension = "ardour"))
        self.ui.lytemplate.addItems(self.lilypondTemplates)
        self.ui.ardourTemplate.addItems(self.ardourTemplates)

        #It is possible that you got an lbjs file without the template.
        warning = []
        if api._getWorkspace().lytemplate in self.lilypondTemplates:
            self.ui.lytemplate.setCurrentIndex(self.lilypondTemplates.index(api._getWorkspace().lytemplate))
        else: #don't choose an index
            warning.append("Lilypond-Template: " + api._getWorkspace().lytemplate)
        if api._getWorkspace().ardourtemplate in self.ardourTemplates:
            self.ui.ardourTemplate.setCurrentIndex(self.ardourTemplates.index(api._getWorkspace().ardourtemplate))
        else: #don't choose an index
            warning.append("Ardour Template: " + api._getWorkspace().ardourtemplate)

        if warning:
            warning= _(" and ").join(warning)
            warning = _("Warning: ") + warning  + _(" could not be found! Choose a different one now or avoid to safe.")
            self.ui.warningLabel.setText("<span style='background-color:yellow'>" + warning + "</span>")
            self.ui.warningLabel.show()

        self.ui.transpositionFrom.addItems(api.lilypond.sortedNoteNameList)
        self.ui.transpositionTo.addItems(api.lilypond.sortedNoteNameList)
        self.ui.transpositionFrom.setCurrentIndex(api.lilypond.sortedNoteNameList.index(api.lilypond.pitch2ly[api._getScore().transposition[0]]))
        self.ui.transpositionTo.setCurrentIndex(api.lilypond.sortedNoteNameList.index(api.lilypond.pitch2ly[api._getScore().transposition[1]]))
        self.ui.transpositionJack.setValue(api._getScore().jackTransposition)
        self.ui.transpositionSimple.setValue(api._getScore().smfTransposition)
        self.ui.fontsize.setValue(api._getWorkspace().lyFontSize)
        self.ui.splitByToHeader.setChecked(api._getScore().splitByToHeader)

        #Export, Close, Save
        self.ui.close.clicked.connect(self.accept)
        self.ui.exportnow.clicked.connect(self.export)
        self.ui.preview.clicked.connect(self.preview)
        self.ui.savesettings.clicked.connect(lambda: self.saveSettings(permanent=True))

        #This also sets the right tab, populates the path combo box etc.
        self.ui.pdf.click()
        self.ui.none.click()
        if main.session.last_export_splitBy_radioButton: #not first start?. extension is set to something else before this check.
            getattr(self.ui, main.session.last_export_extension_radioButton).click()
            getattr(self.ui, main.session.last_export_splitBy_radioButton).click()

    def saveOriginalSettings(self):
        """And finally: When clicking Export! the backend uses the
        internal, saved values from the export settings. So we need to
        override them on each Export!, eventhough the GUI asures you
        that the data will not be saved. No matter if cancel, close,
        accept or whatever is used to close the dialog,
        in the end we restore the original data.
        That does no harm
        a) combined with saveSettings() since these overwrite
        the original settings.
        b) if the program crashes it might be in a temporary
        state regarding the exportOptions, but the original
        save file is still in the original form. """
        self.orgLytemplate = api._getWorkspace().lytemplate
        self.orfLyFontSize = api._getWorkspace().lyFontSize
        self.orgTransposition = api._getScore().transposition
        self.orgjackTransposition = api._getScore().jackTransposition
        self.orgsmfTransposition = api._getScore().smfTransposition
        self.orgArdourTempalte = api._getWorkspace().ardourtemplate
        self.orgSplitByToHeader = api._getScore().splitByToHeader


    def restoreOriginalSettings(self):
        """This function is save to call at the end of the dialog
        no matter what happend since self.saveSettings overrides the
        originalSettings as well"""
        api._getWorkspace().lytemplate = self.orgLytemplate
        api._getWorkspace().lyFontSize = self.orfLyFontSize
        api._getScore().transposition = self.orgTransposition
        api._getScore().jackTransposition = self.orgjackTransposition
        api._getScore().smfTransposition = self.orgsmfTransposition
        api._getScore().splitByToHeader = self.orgSplitByToHeader
        api._getWorkspace().ardourtemplate = self.orgArdourTempalte


    def saveSettings(self, permanent):
        """Save all export options in the lbj file.
        SplitBy and Format are not saved in the file but in the session
        and this is done by the Export button"""
        #The tab self.ui.settingsTab
            #ly
        api._getWorkspace().lytemplate = self.ui.lytemplate.currentText() #combobox
        api._getWorkspace().lyFontSize = self.ui.fontsize.value() #spinbox
        api._getScore().transposition = (api.lilypond.ly2pitch[self.ui.transpositionFrom.currentText()], api.lilypond.ly2pitch[self.ui.transpositionTo.currentText()])
        api._getScore().splitByToHeader = self.ui.splitByToHeader.checkState() #Checkbox
            #midi
        api._getScore().jackTransposition = self.ui.transpositionJack.value()  #spinbox
        api._getScore().smfTransposition = self.ui.transpositionSimple.value()  #spinbox
            #other
        api._getWorkspace().ardourtemplate = self.ui.ardourTemplate.currentText()  #combobox
        api._getWorkspace().ardourtemplate = self.ui.ardourTemplate.currentText()  #combobox


        if permanent:
            self.saveOriginalSettings()

    def _getFunctionAndExtension(self, preview = False):
        if self.ui.pdf.isChecked():
            if preview:
                function = api.previewPDF
            else:
                function = api.exportPDF
            extension = "pdf"
            main.session.last_export_extension_radioButton = "pdf"
        elif self.ui.lilybin.isChecked():
            function = api.exportLilyBin
            extension = "bin"
            main.session.last_export_extension_radioButton = "lilybin"
        elif self.ui.lilypond.isChecked():
            function = api.exportLilypond
            extension = "ly"
            main.session.last_export_extension_radioButton = "lilypond"
        elif self.ui.midijack.isChecked():
            function = lambda filepath, parts: api.exportMidi(filepath = filepath, workspace = None, jackMode = True, parts=parts)
            extension = "jack"
            main.session.last_export_extension_radioButton = "midijack"
        elif self.ui.midisimple.isChecked():
            function = api.exportMidi
            extension = "smf"
            main.session.last_export_extension_radioButton = "midisimple"
        else:
            raise ValueError("Not possible. One of the format radio buttons has to be clicked. Perhaps your Qt Version is different or broken.")
        return function, extension

    def export(self, preview = False):
        """Export! button.
        Also saves paths, SplitBy and Format in the Gui session"""

        def startLyExportProcess(string):
            def reset():
                api.core.lilypond.callExternalCustom = api.core.lilypond._callExternalCustom
            #Override the lilypond export function in core so that exporting does not block the gui and we can route output to self.ui.output (QTextEdit)
            process = QtCore.QProcess(self)
            process.setProcessChannelMode(QtCore.QProcess.MergedChannels)
            process.readyReadStandardOutput.connect(lambda: self.write(str(process.readAllStandardOutput(), encoding='utf-8')))
            process.finished.connect(reset)
            process.start(string)
            process.waitForFinished()
            return process.exitCode()


        api.core.lilypond.callExternalCustom = startLyExportProcess

        self.ui.warningLabel.hide()
        #Format radio buttons
        function, extension = self._getFunctionAndExtension(preview)

        #SplitBy radio buttons
        if self.ui.none.isChecked():
            splitby = None
            main.session.last_export_splitBy_radioButton = "none"
        elif self.ui.exportparts.isChecked():
            splitby = "exportExtractPart"
            main.session.last_export_splitBy_radioButton = "exportparts"
        elif self.ui.group.isChecked():
            splitby = "group"
            main.session.last_export_splitBy_radioButton = "group"
        elif self.ui.track.isChecked():
            splitby = "_uniqueContainerName"
            main.session.last_export_splitBy_radioButton = "track"
        else:
            raise ValueError("Not possible. One of the Split By radio buttons has to be clicked. Perhaps your Qt Version is different or broken.")

        filepath = self.ui.path.currentText()

        self.updatePathComboBoxSessionDatabase()
        self.saveSettings(permanent=False)
        self.ui.output.clear()
        if extension == "bin" or preview or filepath:
            self.sysOrg = sys.stdout
            sys.stdout = self
            if preview:
                function(parts=splitby, pdfviewer = config.pdfviewer)
            else:
                function(filepath=filepath, parts=splitby)

            sys.stdout = self.sysOrg
        else:
            warning = _("You must provide a filepath!")
            self.ui.warningLabel.setText("<span style='background-color:orange'>" + warning + "</span>")
            self.ui.warningLabel.show()
            return False

    def preview(self):
        """Is only active when PDF is checked, so we only need this"""
        self.export(preview = True)

    def write(self, txt):
        """Emulates sys.stdout and sys.stderr behaviour.
        Write the stdout and stderror to the output text edit"""
        self.ui.output.insertPlainText(txt)
        #self.sysOrg.write(txt)

    def openPathButton(self):
        doesntmatter, extension = self._getFunctionAndExtension()
        realFileExtensions = {"pdf":("pdf", "Portable Document Format(PDF) (*.pdf)"), "bin":("bin", "LilyBin.com "), "ly":("ly", "Lilypond Text (*.ly)"), "jack":("mid", "Midi (*.mid)"),  "smf":("mid", "Midi (*.mid)")}

        fileName = QtGui.QFileDialog.getSaveFileName(self, _("Export file name. Split Export will derive their own filen names."), main.session.last_export_dir, realFileExtensions[extension][1]+";;All Files (*.*)")
        if fileName:
            main.last_export_dir = os.path.dirname(fileName)
            self.ui.path.setEditText(fileName)
            self.correctCurrentPathExtension()

    def correctCurrentPathExtension(self):
        """Corrects the current path according to the chosen radio
        button, in place."""
        current = self.ui.path.currentText()
        if current:
            realFileExtensions = {"pdf":"pdf", "bin":"bin", "ly":"ly", "jack":"mid", "smf":"mid"}
            doesntmatter, extension = self._getFunctionAndExtension()
            withoutextension = os.path.splitext(current)[0]
            self.ui.path.setEditText(withoutextension+"."+realFileExtensions[extension])

    def populatePathComboBox(self):
        #Populate the export fields
        #main.session.last_export_files = {"pdf":()[], "ly":[], "jack":[], "smf":[], "bin":[]}
        function, extension = self._getFunctionAndExtension()
        current = self.ui.path.currentText()
        self.ui.path.clear()
        if main.session.last_export_files[extension] and  current == main.session.last_export_files[extension][0]:
            self.ui.path.addItems(main.session.last_export_files[extension])
        else:
            self.ui.path.addItems([current] + main.session.last_export_files[extension])

    def updatePathComboBoxSessionDatabase(self):
        function, extension = self._getFunctionAndExtension()
        if self.ui.path.currentText() in main.session.last_export_files[extension] or not self.ui.path.currentText():
            return False #abort. Don't create duplicate entries.
        main.session.last_export_files[extension].append(self.ui.path.currentText())

    def accept(self):
        """There is no difference between acccept and reject, but
        it is implemented anyway and mapped to the "Close" button.
        Maybe we need it later"""
        super(ExportDialog, self).accept()

    def reject(self):
        """Window manager close, Escape key"""
        super(ExportDialog, self).reject()

    def done(self, var):
        """Called by accept and reject"""
        self.restoreOriginalSettings()
        super(ExportDialog, self).done(var)

class DidYouKnow(QtGui.QDialog):
    """A help window with useful tips.
    Also the nagscreen for Donations."""

    def __init__(self, parent, parentPyClass):
        #super(DidYouKnow, self).__init__(parent, QtCore.Qt.WindowStaysOnTopHint|QtCore.Qt.CustomizeWindowHint|QtCore.Qt.X11BypassWindowManagerHint)
        #super(DidYouKnow, self).__init__(parent, QtCore.Qt.FramelessWindowHint)
        super(DidYouKnow, self).__init__(parent)
        self.ui = Ui_DidYouKnow()  #self.ui is the node to all qt widgets in this tab.
        self.ui.setupUi(self)
        self.parentPyClass = parentPyClass

        self.ui.didyouknowLabel.setText(choice(dictionaries.didYouKnow))
        self.index = dictionaries.didYouKnow.index(self.ui.didyouknowLabel.text())

        self.ui.numberSlider.setMaximum(len(dictionaries.didYouKnow)-1)
        self.ui.numberSlider.setValue(self.index)

        self.ui.numberLabel.setText("Trick " + str(self.index+1) + "/" + str(len(dictionaries.didYouKnow)))

        self.ui.numberSlider.valueChanged.connect(self.moveSlider)

        #Image: 300x151

        if self.palette().window().color().lightness() > 100: # light
            pixmap = QtGui.QPixmap(os.path.join(os.path.dirname(__file__), "resources", "logo300_black.png"))
        else:
            pixmap = QtGui.QPixmap(os.path.join(os.path.dirname(__file__), "resources", "logo300_white.png"))
        pixmap_scaled = pixmap.scaled(self.ui.laborejologo.size(), QtCore.Qt.KeepAspectRatio)

        self.ui.laborejologo.setPixmap(pixmap_scaled)

        if not self.parentPyClass.settings.contains("DidYouKnow"):
            #A trick. The don't show on startup checkbox only appears after the second start
            self.ui.showOnStartup.hide()
            self.parentPyClass.settings.setValue("DidYouKnow", "2") #for the next time.

        self.ui.showOnStartup.stateChanged.connect(self.saveStartupState)

        """
        QAbstractSlider.SliderNoAction  0
        QAbstractSlider.SliderSingleStepAdd 1
        QAbstractSlider.SliderSingleStepSub 2
        QAbstractSlider.SliderPageStepAdd   3
        QAbstractSlider.SliderPageStepSub   4
        QAbstractSlider.SliderToMinimum 5
        QAbstractSlider.SliderToMaximum 6
        QAbstractSlider.SliderMove  7
        """
        self.shortcut("right", lambda: self.ui.numberSlider.triggerAction(1))
        self.shortcut("left", lambda: self.ui.numberSlider.triggerAction(2))
        self.shortcut("up", lambda: self.ui.numberSlider.triggerAction(1))
        self.shortcut("down", lambda: self.ui.numberSlider.triggerAction(2))


        self.ui.numberSlider.wheelEvent = self.mouseWheelEventCustom #Deactivate the normal number slider wheel event
        self.wheelEvent = self.mouseWheelEventCustom #Use a window wide one that is easier to control

        self.ui.showOnStartup.setCheckState(int(self.parentPyClass.settings.value("DidYouKnow")))

        self.ui.closeButton.setFocus(True)
        self.show()


    def mouseWheelEventCustom(self, ev):
        if ev.delta() > 0:
            self.ui.numberSlider.triggerAction(1)
        else:
            self.ui.numberSlider.triggerAction(2)

    def shortcut(self, key, function):
        act = QtGui.QAction(QtGui.QIcon(), '', self)
        act.setShortcut(key)
        act.triggered.connect(function)
        self.addAction(act)

    def saveStartupState(self):
        self.parentPyClass.settings.setValue("DidYouKnow", self.ui.showOnStartup.checkState())

    def moveSlider(self):
        nowIdx = self.ui.numberSlider.value()
        self.ui.didyouknowLabel.setText(dictionaries.didYouKnow[nowIdx])
        self.ui.numberLabel.setText("Trick " + str(nowIdx+1) + "/" + str(len(dictionaries.didYouKnow)))


class MusicTab(QtGui.QStackedWidget):
    """The ui part of a GuiScore"""

    def __init__(self):
        super(MusicTab, self).__init__()
        self.setParent(main.ui.Tab)
        self.ui = Ui_stackedMusicTab()  #self.ui is the node to all qt widgets in this tab.
        self.ui.setupUi(self)
        self.main = main #a shortcut so that tracks and other items can access the main menu if they need.

        self.workingAreaWidget = self.ui.score #The QGraphicsView

        #Connect the numpad buttons, which are per MusicTab(), to the global actions.
        self.numpadWidgetToolbuttons = [self.ui.numpad0, self.ui.numpad1, self.ui.numpad2, self.ui.numpad3, self.ui.numpad4, self.ui.numpad5, self.ui.numpad6, self.ui.numpad7, self.ui.numpad8, self.ui.numpad9,]
        for globalAction, localButton in zip(main.numpadWidgetActions, self.numpadWidgetToolbuttons):
            localButton.setDefaultAction(globalAction)

        self.tracks = [] #this is just for updateViewRect when MusicTab is initialized the first time. the real self.tracks in in GuiScore

        self.rubberBandOrigin = None #the temporary rubberband origin
        self.rubberBand = None #the temporary rubberband
        self.icon = None # Groups and Scores use the same icon/color.

        self.orgFocusIn = self.workingAreaWidget.focusInEvent
        self.orgFocusOut = self.workingAreaWidget.focusOutEvent
        self.workingAreaWidget.focusInEvent = self.customFocusIn
        self.workingAreaWidget.focusOutEvent = self.customFocusOut
        self.workingAreaWidget.setFocusPolicy(QtCore.Qt.WheelFocus)

        #TODO: self.workingAreaWidget.setFocus(True) Does not work in the beginning. We need to click in the score with the mouse at least once!

        #Setup the main notation view, the score, by adding a Scene.
        self.graphicsViewRect = [0, 0, 0 , 0] #x, y, width, height
        self.scene = QtGui.QGraphicsScene()
        self.scene.setBackgroundBrush(QtGui.QColor("white"))
        self.workingAreaWidget.setScene(self.scene)
        self.updateSceneRect()

        self.wheelEventOrg = self.workingAreaWidget.wheelEvent

        self.workingAreaWidget.mousePressEvent = self.mousePressEventCustom
        self.workingAreaWidget.mouseMoveEvent = self.mouseMoveEventCustom
        self.workingAreaWidget.wheelEvent = self.mouseWheelEventCustom

        self.scene.mouseReleaseEvent = self.mouseReleaseEventCustom
        self.scene.mouseDoubleClickEvent = self.mouseReleaseEventCustom
        self.scene.contextMenuEvent = self.contextMenuEventCustom
        self.middleClickOrigin = None

        #Setup the synced incipit frame left of the score for group and other meta data.
        self.incipitScene = QtGui.QGraphicsScene()
        self.incipitScene.setBackgroundBrush(QtGui.QColor("white"))
        self.ui.incipit.setScene(self.incipitScene)

        #Sync the vertical incipit scrollbar (which is never shown) with the score scrollbar.
        self.incipitVerticalBar = self.ui.incipit.verticalScrollBar()
        #self.scoreVerticalBar =  self.workingAreaWidget.verticalScrollBar()
        self.workingAreaWidget.setVerticalScrollBar(self.incipitVerticalBar)

        #Initial Zoom Levels
        newTransform = QtGui.QTransform()
        newTransform.scale(0.7, 0.7)
        self.workingAreaWidget.setTransform(newTransform)
        newTransform2 = QtGui.QTransform()
        newTransform2.scale(1, 0.7)
        self.ui.incipit.setTransform(newTransform2)

        #Minimap
        self.ui.minimap.hide() #hidden by default
        self.ui.minimap.setScene(self.scene)
        self.scene.sceneRectChanged.connect(lambda newRect: self.ui.minimap.fitInView(newRect, QtCore.Qt.KeepAspectRatio))
        self.ui.minimap.setViewportUpdateMode(QtGui.QGraphicsView.FullViewportUpdate)

        #Deactivate minimap mouse buttons which were originally for the whole scene.
        def nothingMouse(ev):
            pass
        self.ui.minimap.mouseReleaseEvent = lambda ev: self.workingAreaWidget.centerOn(self.ui.minimap.mapToScene(ev.pos()))
        self.ui.minimap.mousePressEvent = nothingMouse
        self.ui.minimap.contextMenuEvent = nothingMouse
        self.ui.minimap.mouseDoubleClickEvent = nothingMouse

        #we update the viewport manually
        #but some actions trigger an update, like scrollbars.
        self.workingAreaWidget.setViewportUpdateMode(QtGui.QGraphicsView.NoViewportUpdate)
        self.workingAreaWidget.horizontalScrollBar().valueChanged.connect(self.workingAreaWidget.viewport().update)
        self.workingAreaWidget.verticalScrollBar().valueChanged.connect(self.workingAreaWidget.viewport().update) #slider move is the manual slider score.


        #Track Properties
        self.ui.trackUniqueName.textEdited.connect(lambda: api.trackName(self.ui.trackUniqueName.text())) #The api function will send back the change to the trackProperties listener which updates the gui.
        self.ui.trackGroup.textEdited.connect(lambda: self.changeGroup(self.ui.trackGroup.text()))
        self.ui.trackExportPart.textEdited.connect(lambda: api.trackChangeAttribute("exportExtractPart", self.ui.trackExportPart.text()))
        self.ui.trackLongInstrumentName.textEdited.connect(lambda: api.trackChangeAttribute("longInstrumentName", self.ui.trackLongInstrumentName.text()))
        self.ui.trackShortInstrumentName.textEdited.connect(lambda: self.changeNameMarker(self.ui.trackShortInstrumentName.text()))
        self.ui.trackVoice.stateChanged.connect(lambda: api.trackChangeAttribute("mergeWithUpper", self.ui.trackVoice.checkState()))
        self.ui.trackDirectives.clicked.connect(lambda: api.directivesGui(api._getTrack()))
        self.ui.trackAmbitus.stateChanged.connect(lambda: api.trackChangeAttribute("ambitus", self.ui.trackAmbitus.checkState()))
        self.ui.trackLyricsAbove.stateChanged.connect(lambda: api.trackChangeAttribute("forceLyricsAbove", self.ui.trackLyricsAbove.checkState()))
        self.ui.trackHideLilypond.stateChanged.connect(lambda: api.trackChangeAttribute("hideLilypond", self.ui.trackHideLilypond.checkState()))
        self.ui.trackNrOfLines.valueChanged.connect(lambda: api.trackChangeAttribute("nrOfLines", self.ui.trackNrOfLines.value()))

        self.ui.trackTranposeFrom.addItems(api.lilypond.sortedNoteNameList)
        self.ui.trackTranposeTo.addItems(api.lilypond.sortedNoteNameList)
        self.ui.trackTranposeFrom.view().setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum) #make sure the list is visible, always.
        self.ui.trackTranposeTo.view().setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)#make sure the list is visible, always.
        self.ui.trackTranposeFrom.setCurrentIndex(api.lilypond.sortedNoteNameList.index(api.lilypond.pitch2ly[api._getTrack().transposition[0]]))
        self.ui.trackTranposeTo.setCurrentIndex(api.lilypond.sortedNoteNameList.index(api.lilypond.pitch2ly[api._getTrack().transposition[1]]))
        self.ui.trackTranposeFrom.currentIndexChanged.connect(lambda: api.trackChangeAttribute("transposition", (api.lilypond.ly2pitch[api.lilypond.sortedNoteNameList[self.ui.trackTranposeFrom.currentIndex()]], api.lilypond.ly2pitch[api.lilypond.sortedNoteNameList[self.ui.trackTranposeTo.currentIndex()]]))) # :)
        self.ui.trackTranposeTo.currentIndexChanged.connect(lambda: api.trackChangeAttribute("transposition", (api.lilypond.ly2pitch[api.lilypond.sortedNoteNameList[self.ui.trackTranposeFrom.currentIndex()]], api.lilypond.ly2pitch[api.lilypond.sortedNoteNameList[self.ui.trackTranposeTo.currentIndex()]]))) # ;)

        self.ui.trackSize.addItems(["normal", "big", "small", "tiny"]) #Sizes List
        self.ui.trackSize.view().setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        self.ui.trackSize.currentIndexChanged.connect(lambda: api.trackChangeAttribute("lilypondSize", self.ui.trackSize.currentText()))

        self.ui.trackVoicePreset.addItems(dictionaries.voicePresets) #Voice Presets List
        self.ui.trackVoicePreset.currentIndexChanged.connect(lambda: api.trackChangeAttribute("voicePreset", self.ui.trackVoicePreset.currentText()))

        self.ui.trackMute.stateChanged.connect(lambda: api.trackChangeAttribute("smfMute", self.ui.trackMute.checkState()))
        self.ui.trackSolo.stateChanged.connect(lambda: api.trackChangeAttribute("smfSolo", self.ui.trackSolo.checkState()))

        self.ui.trackChannelMin.valueChanged.connect(lambda: api.trackChangeAttribute("smfChannel", [self.ui.trackChannelMin.value() -1, self.ui.trackChannelMin.value() -1])) #In the GUI we keep both values seperate, but the backend safes both as list [min, max]
        self.ui.trackMidiTransposition.valueChanged.connect(lambda: api.trackChangeAttribute("smfTransposition", self.ui.trackMidiTransposition.value()))
        self.ui.trackVolume.valueChanged.connect(lambda: api.trackChangeAttribute("smfVolume", self.ui.trackVolume.value() -1))
        self.ui.trackPan.valueChanged.connect(lambda: api.trackChangeAttribute("smfPan", self.ui.trackPan.value() -1))
        self.ui.trackBank.valueChanged.connect(lambda: api.trackChangeAttribute("smfBank", self.ui.trackBank.value() -1))
        self.ui.trackInstrument.addItems(api.playback.instrumentList) #Instrument List
        self.ui.trackInstrument.view().setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)

        self.ui.trackInstrument.currentIndexChanged.connect(lambda: api.trackChangeAttribute("smfPatch", self.ui.trackInstrument.currentIndex()))
        self.ui.trackInstrumentNumber.valueChanged.connect(lambda: api.trackChangeAttribute("smfPatch", self.ui.trackInstrumentNumber.value() -1))
        #Crosslink:
        self.ui.trackInstrumentNumber.valueChanged.connect(lambda: self.ui.trackInstrument.setCurrentIndex(self.ui.trackInstrumentNumber.value() -1))
        self.ui.trackInstrument.currentIndexChanged.connect(lambda: self.ui.trackInstrumentNumber.setValue(self.ui.trackInstrument.currentIndex() +1))

        self.ui.jackChannelMin.valueChanged.connect(lambda: api.trackChangeAttribute("jackChannel", [self.ui.jackChannelMin.value() -1, self.ui.jackChannelMax.value() -1])) #A change of one frontend channel parameter changes both backend min, max values.
        self.ui.jackChannelMax.valueChanged.connect(lambda: api.trackChangeAttribute("jackChannel", [self.ui.jackChannelMin.value() -1, self.ui.jackChannelMax.value() -1])) #A change of one frontend channel parameter changes both backend min, max values.
        self.ui.jackPatch.valueChanged.connect(lambda: api.trackChangeAttribute("jackPatch", self.ui.jackPatch.value() -1))
        self.ui.jackBank.valueChanged.connect(lambda: api.trackChangeAttribute("jackBank", self.ui.jackBank.value() -1))
        self.ui.jackVolume.valueChanged.connect(lambda: api.trackChangeAttribute("jackVolume", self.ui.jackVolume.value() -1))
        self.ui.jackPan.valueChanged.connect(lambda: api.trackChangeAttribute("jackPan", self.ui.jackPan.value() -1))
        self.ui.jackTranspose.valueChanged.connect(lambda: api.trackChangeAttribute("jackTransposition", self.ui.jackTranspose.value()))

        #Lyric Frame / TextEdit
        self.ui.lyrics.hide() #Lyrics are hidden by default
        def changeLyrics(): #What happens when the lyric frame is changed.
            api._getTrack().lyrics = self.ui.lyricTextEdit.toPlainText()
            self.currentTrack.drawLyrics() #eventhough we are in MusicTab we are also in GuiScore since it is a child class. The only child class.
            self.workingAreaWidget.viewport().update() #Update the grahipcsview manually

        self.ui.lyricTextEdit.textChanged.connect(changeLyrics)
        self.lyricTextEditOriginalKeyPressEvent = self.ui.lyricTextEdit.keyPressEvent
        self.ui.lyricTextEdit.keyPressEvent = self.lyricKeyPressInterceptor

        #Substitutions
        self.scoreSubstitutionsMenu = None #an everchanging menu of substitutions
        self.scoreContainerMenu = None #an everchanging menu of container

    def customFocusIn(self, event):
        """Entering the main working area, the score"""
        for action in main.genericKeyActions:
            action.setEnabled(True)
        self.orgFocusIn(event)

    def customFocusOut(self, event):
        """Leaving the main working area, the score"""
        for action in main.genericKeyActions:
            action.setDisabled(True)
        self.orgFocusOut(event)

    def lyricKeyPressInterceptor(self, qKeyEvent):
        if qKeyEvent.nativeModifiers() and qKeyEvent.nativeVirtualKey() == 32: #shift and space
            self.ui.lyricTextEdit.insertPlainText(" -- ")
        self.lyricTextEditOriginalKeyPressEvent(qKeyEvent)

    def updateGui(self):
        """Update several frame and widgets that derive their appearance
        from backend data"""
        self.updateTrackProperties()
        self.updateLyricsFrame()

    def updateStatusBar(self):
        cursor = api._getActiveCursor()
        msg = ("Pitch: " + api.lilypond.pitch2ly[api.pitch.toScale(cursor.pitchindex, cursor.prevailingKeySignature[-1])]
            + " || " + "Track: " + str(api._getTrackIndex() +1 )
            + " Bar: " + str(cursor.measureNumber + 1)
            +  " Pos: " + str(round(cursor.metricalPosition +1, 4))
            + ". Sec: " + str(round(cursor.timeindex, 4))
            + " TickIndex: " + str(cursor.tickindex))
        main.statusBar().showMessage(msg + " || " + main.more)

    def updateTrackProperties(self):
        """Uses the (backend) cursor position.
        Each of the following updates will trigger a change signal,
        defined in MusicTab.__init__(), which will in return trigger
        a re-set of the backend value.
        In most cases this is just a waste of resources, in cases like
        JackMidiChannel where a single backend value influences two
        frontend values this leads to value errors.

        We can't prevent the Qt Signal flow but we can prevent the value
        changing in the backend by temporarily deactivating the api
        function.
        """

        #GUI Track changes
        main.ui.actionToggle_Extended_Track_View.setChecked(self.currentTrack.dualView)
        main.ui.actionToggle_Collapse_Track.setChecked(self.currentTrack.collapsed)

        #Change the Track Strip with backend data
        #If we don't do this we get an endless loop of changing between backend and gui event signals
        org = api.trackChangeAttribute
        def empty(*args):
            pass
        api.trackChangeAttribute = empty  #At the end of this function the original function gets restored.

        track = api._getTrack() #backend instance
        if track.group:
            nameLabel =str(api._getTrackIndex() +1 ) + ": " +  track.uniqueContainerName + "(" + track.group + ")"
        else:
            nameLabel = str(api._getTrackIndex() +1 ) + ": " +  track.uniqueContainerName
        self.ui.trackPropertiesLabel.setText(nameLabel)
        self.ui.trackNrOfLines.setValue(track.nrOfLines)

        #Trackname is different from the rest of the properties. It needs checking after every char added/deleted if the name is unique or not. So we have to update the gui field after each modification which lets the cursor jump to the end. Workaround that:
        nameCursorPos = self.ui.trackUniqueName.cursorPosition()
        self.ui.trackUniqueName.setText(track.uniqueContainerName)
        self.ui.trackUniqueName.setCursorPosition(nameCursorPos)

        self.ui.trackGroup.setText(track.group)
        self.ui.trackExportPart.setText(track.exportExtractPart)
        self.ui.trackShortInstrumentName.setText(track.shortInstrumentName)
        self.ui.trackLongInstrumentName.setText(track.longInstrumentName)
        self.ui.trackTranposeFrom.setCurrentIndex(api.lilypond.sortedNoteNameList.index(api.lilypond.pitch2ly[track.transposition[0]]))
        self.ui.trackTranposeTo.setCurrentIndex(api.lilypond.sortedNoteNameList.index(api.lilypond.pitch2ly[track.transposition[1]]))
        self.ui.trackHideLilypond.setChecked(track.hideLilypond)
        self.ui.trackLyricsAbove.setChecked(track.forceLyricsAbove)
        self.ui.trackVoice.setChecked(track.mergeWithUpper)
        self.ui.trackMute.setChecked(track.smfMute)
        self.ui.trackSolo.setChecked(track.smfSolo)
        self.ui.trackSize.setCurrentIndex(["normal", "big", "small", "tiny"].index(track.lilypondSize))
        self.ui.trackVoicePreset.setCurrentIndex(dictionaries.voicePresets.index(track.voicePreset))
        directives = len(track.directivePre) + len(track.directiveMid) + len(track.directivePst) + len(track.instructionPre) + len(track.instructionPst) #total number of directives
        self.ui.trackDirectives.setText(str(directives) +  " Directives")
        self.ui.trackAmbitus.setChecked(track.ambitus)

        self.ui.trackVolume.setValue(track.smfVolume + 1)
        self.ui.trackPan.setValue(track.smfPan +1)
        self.ui.trackInstrument.setCurrentIndex(track.smfPatch)
        self.ui.trackInstrumentNumber.setValue(track.smfPatch + 1)
        self.ui.trackChannelMin.setValue(track.smfChannel[0] + 1)
        self.ui.trackMidiTransposition.setValue(track.smfTransposition)
        self.ui.trackBank.setValue(track.smfBank + 1)

        self.ui.jackChannelMin.setValue(track.jackChannel[0] + 1)
        self.ui.jackChannelMax.setValue(track.jackChannel[1] + 1)
        self.ui.jackPatch.setValue(track.jackPatch + 1)
        self.ui.jackVolume.setValue(track.jackVolume + 1)
        self.ui.jackPan.setValue(track.jackPan + 1)
        self.ui.jackTranspose.setValue(track.jackTransposition)
        self.ui.jackBank.setValue(track.jackBank + 1)

        api.trackChangeAttribute = org

    def updateLyricsFrame(self):
        """Called at each track change"""
        track = api._getTrack() #backend instance
        self.ui.lyrics.setTitle("Lyrics - Track " + track.uniqueContainerName)
        self.ui.lyricTextEdit.setPlainText(track.lyrics)

    def updateLilypondStaffGroups(self):
        self.currentTrack.incipit.changeLilypondStaffGroupMarkers()

    def updateSceneRect(self):
        """setSceneRect(x, y, width, height)"""
        mod = len(self.tracks) * 11
        self.workingAreaWidget.setSceneRect(self.graphicsViewRect[0], -60, self.graphicsViewRect[2] + 100, self.graphicsViewRect[3] + mod)
        self.ui.incipit.setSceneRect(self.graphicsViewRect[0], -59, 50, self.graphicsViewRect[3] + mod) #50 because the view is fixed to 50 pixels
        #Yes, -60 and -59. The one pixel has its purposes. GUI Finetuning.
        self.workingAreaWidget.viewport().update()

    def visibleRect(self):
        topLeft = QtCore.QPointF(self.workingAreaWidget.mapToScene( 0, 0 ))
        bottomRight = QtCore.QPointF(self.workingAreaWidget.mapToScene(self.workingAreaWidget.viewport().width() - 1, self.workingAreaWidget.viewport().height() - 1 ))
        rect = QtCore.QRectF(topLeft, bottomRight)
        return rect

    def findGuiItemsFromPosCrawlLeft(self, parameterPos):
        #init
        clickPos = parameterPos
        x = clickPos.x()
        y = clickPos.y()
        directHit = False
        #First find the track.
        #tupl = (lowest pos, highest pos, gui-track) #'lowest' means highest in gui coordinates. 0 is the top scene border
        guiTrack = [tupl for tupl in self.cachedTrackPositions if tupl[0] < y < tupl[1]][0][2]

        #Create a rectangle/beam that crawls left and returns all items within.
        #We take the last one
        qRectangleF = QtCore.QRectF(x-200, y-20, 200, 45)   #shift-x, shift-y, width, height  . If you shift the middle of the max height/width you have the cursor in the middle.

        #global draw
        #try: self.scene.removeItem(draw)
        #except: pass
        #draw = QtGui.QGraphicsRectItem(qRectangleF)
        #self.scene.addItem(draw)

        foundItems = self.scene.items(qRectangleF)
        foundItemsFilter = list(filter(lambda item: isinstance(item, guiitems.GuiItem) and item.track is guiTrack, foundItems))

        if list(filter(lambda item: isinstance(item, guiitems.GuiItem), self.scene.items(x-3, y-10, 6, 30))):
            directHit = True

        if foundItemsFilter: #Item left of the click found.
            foundItem  = sorted(foundItemsFilter, key=lambda x: x.cachedIndex)[-1] #the highest cached flat cursor index is the topmost right item.
            foundItemIndex = foundItem.cachedIndex
        elif len(guiTrack.data.container) == 1: #empty track, only appending. Hopefully the backend never changes "Only Appending = Empty".
            foundItem = PSEUDO #Appending
            foundItemIndex = 0
        else:
            return (None, None, None, None)

        return (foundItem, foundItemIndex, guiTrack.data, directHit)

    def mouseReleaseEventCustom(self,ev):
        """Create a virtual "big" cursor that is not a point
        but a rectangle.
        Step by step increase the search rectangle until you find
        something or the left border, which is 0."""
        modifiers = QtGui.QApplication.keyboardModifiers()
        if ev.button() == QtCore.Qt.LeftButton:
            self.rubberBand.hide()

            #Rubberband selection
            #does not move the cursor.
            if self.rubberBand.size().height() * self.rubberBand.size().width() > 100: #treshhold to not trigger a rubberband selection by accident
                viewCoordinatesRect = self.rubberBand.geometry()
                if modifiers == QtCore.Qt.ControlModifier:
                    self.deselectInRectangle((self.workingAreaWidget.mapToScene(viewCoordinatesRect)).boundingRect())
                else:
                    self.selectInRectangle((self.workingAreaWidget.mapToScene(viewCoordinatesRect)).boundingRect())

            #Single Click
            #Not a rubberband.
            else:
                foundItem, foundItemIndex, backendTrack, directHit = self.findGuiItemsFromPosCrawlLeft(parameterPos = ev.scenePos())
                if backendTrack:
                    #Ctrl+Click. Add one Item to the selection
                    if modifiers == QtCore.Qt.ControlModifier:
                        try: #items like the MultiMeasureRest and Upbeat hide their data for the caching engine to work.
                            api.remoteToggleSelect(foundItem.dataOrg, backendTrack)
                        except:
                            api.remoteToggleSelect(foundItem.data, backendTrack)


                    #Shift+Click. Add a rectangle of items to the selection
                    elif modifiers == QtCore.Qt.ShiftModifier:
                        #We create a rectangle from the old cursor position and the new. From then:
                        #Is the same as rectangle selection plus moving the cursor to the target position afterwards.
                        #this way we get a multitrack selection. Something that the api alone cannot do.
                        originCursorPosX = self.cursorPosition.pos().x()
                        originCursorPosY = self.cursorPosition.pos().y()

                        api.cursorFlatPosition((api._getIndexByTrack(backendTrack), foundItemIndex))
                        if not directHit:
                            api.right() #we search left but want in the space 'between' the notes. This is already protected against going right at the end of the track.

                        targetCursorPosX = self.cursorPosition.pos().x()
                        targetCursorPosY = self.cursorPosition.pos().y()

                        if originCursorPosX <= targetCursorPosX: #the rectangle goes from left to right
                            if originCursorPosY <= targetCursorPosY: #from top to bottom
                                self.selectInRectangle(QtCore.QRectF(originCursorPosX+20, originCursorPosY, targetCursorPosX-originCursorPosX-20, originCursorPosY + targetCursorPosY + 80)) #build a rectangle from the current and old position and let the function create a selection from it.
                            else: #bottom to top
                                self.selectInRectangle(QtCore.QRectF(originCursorPosX+20, targetCursorPosY, targetCursorPosX-originCursorPosX-20, originCursorPosY + targetCursorPosY + 80)) #build a rectangle from the current and old position and let the function create a selection from it.
                        else: #backwards
                            if originCursorPosY <= targetCursorPosY: #from top to bottom
                                self.selectInRectangle(QtCore.QRectF(targetCursorPosX+20, targetCursorPosY, originCursorPosX-targetCursorPosX-20, 80))
                            else: #bottom to top
                                self.selectInRectangle(QtCore.QRectF(targetCursorPosX+20, targetCursorPosY, originCursorPosX-targetCursorPosX-20, originCursorPosY + targetCursorPosY + 80)) #build a rectangle from the current and old position and let the function create a selection from it.

                    #No modifiers. Move the cursor
                    else:
                        api.cursorFlatPosition((api._getIndexByTrack(backendTrack), foundItemIndex))
                        if not directHit:
                            api.right() #we search left but want in the space 'between' the notes. This is already protected against going right at the end of the track.

                self.updateStatusBar()
                #User clicked outside of any reasonable perimeter.
                #else:
                #    return None #nothing happens.

        #elif ev.button() == QtCore.Qt.RightButton:
        elif ev.button() == QtCore.Qt.MidButton:
            #nothing happens. Context menu is already done on press, not release.
            #if not self.middleClickOrigin:
            #    api.deselectScore()
            self.middleClickOrigin = None
            self.middleClickSrollBarOrigin = None
        self.workingAreaWidget.viewport().update() #Update the grahipcsview manually


    def mouseWheelEventCustom(self, ev):
        modifiers = QtGui.QApplication.keyboardModifiers()
        if modifiers == QtCore.Qt.ControlModifier | QtCore.Qt.ShiftModifier:
            if ev.delta() > 0:
                self.widen()
            else:
                self.shrinken()
        elif modifiers == QtCore.Qt.ControlModifier:
            if ev.delta() > 0:
                self.zoomIn()
            else:
                self.zoomOut()
        else:
            self.wheelEventOrg(ev)
        self.workingAreaWidget.viewport().update()

    def mousePressEventCustom(self, ev):
        """Mouse Press is either the beginning of a 'single click'
        to set the cursor position or to toggleSelect a single item.
        Or it is the beginning of a rubberband """
        if ev.button() == QtCore.Qt.LeftButton:
            self.rubberBandOrigin = ev.pos()
            self.rubberBand = QtGui.QRubberBand(QtGui.QRubberBand.Rectangle, parent = self.workingAreaWidget)
            self.rubberBand.setGeometry(QtCore.QRect(self.rubberBandOrigin, QtCore.QSize()))
            self.rubberBand.show()
        elif ev.button() == QtCore.Qt.MidButton:
            #self.middleClickOrigin = self.workingAreaWidget.mapToScene(evvpos()) #QPointF
            self.middleClickOrigin = ev.pos()
            self.middleClickSrollBarOrigin = (self.workingAreaWidget.horizontalScrollBar().value(), self.workingAreaWidget.verticalScrollBar().value())

    def mouseMoveEventCustom(self, ev):
        if self.rubberBand:
            self.rubberBand.setGeometry(QtCore.QRect(self.rubberBandOrigin, ev.pos()).normalized())
        if self.middleClickOrigin:
            #Custom Middle Button ScrollDrag
            difference = self.middleClickOrigin - ev.pos()
            x = difference.x()
            y = difference.y()
            if abs(x) > 20 or abs(y) > 20:
                self.workingAreaWidget.horizontalScrollBar().setValue(self.middleClickSrollBarOrigin[0] + difference.x() * 2 )
                self.workingAreaWidget.verticalScrollBar().setValue(self.middleClickSrollBarOrigin[1] + difference.y() * 2)

    def contextMenuEventCustom(self, ev):
        foundItem, foundItemIndex, backendTrack, directHit = self.findGuiItemsFromPosCrawlLeft(parameterPos = ev.scenePos())
        if backendTrack and not foundItem is PSEUDO:
            api.cursorFlatPosition((api._getIndexByTrack(backendTrack), foundItemIndex))
            if not directHit:
                api.right() #we search left but want in the space 'between' the notes. This is already protected against going right at the end of the track.
        else: #User clicked outside of any reasonable perimeter.
            return None #nothing happens.

        #We are now at the cursor position. Decide which context menu is correct.
        cur = self.currentTrack.getCurrentItem()
        if type(cur) is guiitems.GuiChord:
            tmpMenu = QtGui.QMenu()
            tmpMenu.addMenu(self.scoreContainerMenu)
            tmpMenu.addSeparator()
            tmpMenu.addMenu(main.placeholderScoreSubstitutions)
            tmpMenu.addMenu(main.substitutionsMenu)
            tmpMenu.addAction(main.ui.actionCreate_Substitution_from_Selection)
            tmpMenu.addAction(main.ui.actionRemove_Substitution)
            #tmpMenu.exec_(self.workingAreaWidget.mapToGlobal(ev.scenePos().toPoint()))  #this needs the cursor position to be correct or a selection. Normal backend behaviour, but prevents us from using a hypothetical substitutionsRemote() which is maybe better that way. The cursor should always move.
            tmpMenu.exec_(ev.screenPos())  #this needs the cursor position to be correct or a selection. Normal backend behaviour, but prevents us from using a hypothetical substitutionsRemote() which is maybe better that way. The cursor should always move.
        else:
            return None

    def selectInRectangle(self, qRectangleF):
        """Used in eventFilter for rubberband selection and
        mouseButtonPressed for Shift+Press"""
        #draw = QtGui.QGraphicsRectItem(qRectangleF)
        #self.scene.addItem(draw)
        rectangledItems = self.scene.items(qRectangleF, mode = QtCore.Qt.IntersectsItemShape)
        filtered = list(filter(lambda item: isinstance(item, guiitems.GuiItem), rectangledItems))
        for guiItem in filtered:
            try: #items like the MultiMeasureRest and Upbeat hide their data for the caching engine to work.
                api.remoteSelect(guiItem.dataOrg, guiItem.track.data)
            except:
                api.remoteSelect(guiItem.data, guiItem.track.data)

    def deselectInRectangle(self, qRectangleF):
        """Used in eventFilter for rubberband selection and
        mouseButtonPressed for Shift+Press"""
        #draw = QtGui.QGraphicsRectItem(qRectangleF)
        #self.scene.addItem(draw)
        rectangledItems = self.scene.items(qRectangleF, mode = QtCore.Qt.IntersectsItemShape)
        filtered = list(filter(lambda item: isinstance(item, guiitems.GuiItem), rectangledItems))
        for guiItem in filtered:
            api.remoteDeselect(guiItem.dataOrg, guiItem.track.data)

    def zoomIn(self):
        x = self.workingAreaWidget.transform().m11() #Y is the same
        newTransform = QtGui.QTransform()
        newTransform.scale(round(x+0.1, 2), round(x+0.1, 2))
        self.workingAreaWidget.setTransform(newTransform)
        #self.workingAreaWidget.centerOn(self.cursorPosition.pos())
        #The order of setIncipit Transform and cursor position is critical! I don't know why, but else zooming both views in sync does not work.
        newTransform2 = QtGui.QTransform()
        newTransform2.scale(1, round(x+0.1, 2))
        self.ui.incipit.setTransform(newTransform2)
        self.workingAreaWidget.viewport().update #it is possible that scrollbars have to appear. Update is needed for this.

    def zoomOut(self):
        x = self.workingAreaWidget.transform().m11() #Y is the same
        newTransform = QtGui.QTransform()
        newTransform.scale(round(x-0.1, 2), round(x-0.1, 2))
        self.workingAreaWidget.setTransform(newTransform)
        newTransform2 = QtGui.QTransform()
        newTransform2.scale(1, round(x-0.1, 2))
        self.ui.incipit.setTransform(newTransform2)
        #The order of setIncipit Transform and cursor position is critical! I don't know why, but else zooming both views in sync does not work.
        #It is exactly the opposite of zoomIn here.
        #self.workingAreaWidget.centerOn(self.cursorPosition.pos())
        self.workingAreaWidget.viewport().update #it is possible that scrollbars have to appear. Update is needed for this.

    def zoomNull(self):
        x = self.workingAreaWidget.transform().m11() #Y is the same
        newTransform = QtGui.QTransform()
        newTransform.scale(1,1)
        self.workingAreaWidget.setTransform(newTransform)
        self.ui.incipit.setTransform(newTransform)

    def _stretchXCoordinates(self, factor):
        config.DURATION_MOD *= factor
        for backendItem, item in self.allItems.items():
            for i in item:
                i.setX(i.pos().x() * factor) #adjust all track items
                if i.group: #adjust beam
                    i.updateGroupStemAndFlag()
        for tr in self.tracks:
            for barline in tr.barlines: #adjust barlines
                barline.setX(barline.pos().x() * factor)
            for trline in tr.lines: #adjust stafflines
                new = trline.line()
                new.setLength(new.length() * factor)
                trline.setLine(new)
        self.sendMaxTrackWidthToMain() #new scrollbars
        self.cursorPosition.update()

    def widen(self):
        self._stretchXCoordinates(1*1.2) #2 is good

    def shrinken(self):
        self._stretchXCoordinates(1/1.2) #0.5 is good

    def centerOnCursor(self):
        self.cursorPosition.centerOn()

    def collapseAllTracks(self):
        for trk in self.tracks:
            trk.collapseView(repositionTracks = False)
        self.repositionTracks()

    def collapseEmptyTracks(self):
        for trk in self.tracks:
            if trk.data.isEmpty(): #this is not very performant, but collapseEmptyTracks is not supposed to be a frequent command anyway...
                trk.collapseView(repositionTracks = False)
        self.repositionTracks()

    def expandAllTracks(self):
        for trk in self.tracks:
            trk.expandView(repositionTracks = False)
        self.repositionTracks()

    def invertCollapsing(self):
        for trk in self.tracks:
            trk.toggleCollapse(repositionTracks = False)
        self.repositionTracks()

    def connectMenuAction(self, action, function, append = True):
        """Connect the menu action with the backend function,
        or qt function in a few cases like Save and Open.
        The actions already exist in their places and
        no shortcuts are getting set here."""
        def funcWrap():
            function()
            self.workingAreaWidget.viewport().update()

        self.connect(action, QtCore.SIGNAL("triggered()"), funcWrap)

    def generateScoreSubstitutionsMenu(self):
        """Look up all score substitutions and generate a menu for
        them. This is self updating. Call it again if new substitutions
        are added to the score."""
        mainMenu = main.placeholderScoreSubstitutions
        mainMenu.clear()

        if self.scoreSubstitutionsMenu:
            self.scoreSubstitutionsMenu.clear()
            self.scoreSubstitutionsMenu = None

        self.scoreSubstitutionsMenu = mainMenu

        alreadyProcessedGroups = {} #each group shall only create one submenu. No nested menus.

        for key, value in sorted(api._getScore().substitutions.items()):  #key is the name string, value a backend Container.
            group = value.group
            if group and not group in alreadyProcessedGroups:
                newSubMenu = self.scoreSubstitutionsMenu.addMenu(group)
                alreadyProcessedGroups[group] = newSubMenu
            pretty = " ".join(key.split("_")).title()
            if group:
                entry = alreadyProcessedGroups[group].addAction(key)
            else:
                entry = self.scoreSubstitutionsMenu.addAction(key) #use the real name.
            entry.setText(pretty) #but display a pretty name.
            #self.connect(entry, QtCore.SIGNAL('triggered()'), lambda key=key: api.substitution(key))
            self.connectMenuAction(entry, lambda key=key: api.substitution(key))

    def generateSubstitutionsMenu(self):
        """Generate the program wide substitutions.
        No filespecific data.
        Runs only once add the beginning of the program."""
        if not main.substitutionsMenu:
            main.substitutionsMenu = QtGui.QMenu()
            #tmp = main.ui.menuAdvanced.addMenu(main.substitutionsMenu, )
            tmp = main.ui.menuAdvanced.insertMenu(main.ui.actionRemove_Substitution, main.substitutionsMenu )

            tmp.setText('Laborejo Substitutions')

            alreadyProcessedGroups = {} #each group shall only create one submenu. No nested menus.
            for key, value in sorted(api._getWorkspace().substitutiondatabase.items()):  #key is the name string. value is a tuple. (optional group, core.container). We don't care about the container here.
                group = value.group
                if group and not group in alreadyProcessedGroups:
                    newSubMenu = main.substitutionsMenu.addMenu(group)
                    alreadyProcessedGroups[group] = newSubMenu

                key2 = key.replace("subst_", "")
                pretty = " ".join(key2.split("_")).title()
                if group:
                    entry = alreadyProcessedGroups[group].addAction(key)
                else:
                    entry = main.substitutionsMenu.addAction(key) #use the real name.
                main.allActions.append(entry)
                entry.setText(pretty) #but display a pretty name.
                #main.connect(entry,QtCore.SIGNAL('triggered()'), lambda key=key: api.substitution(key))
                self.connectMenuAction(entry, lambda key=key: api.substitution(key))


    def generateScoreContainerMenu(self):
        """Look up all score container and generate a menu for
        them. This is self updating. Call it again if new container
        are added to the score."""

        mainMenu = main.placeholderScoreContainers
        mainMenu.clear()

        if self.scoreContainerMenu:
            self.scoreContainerMenu.clear()
            self.scoreContainerMenu = None

        self.scoreContainerMenu = mainMenu

        alreadyProcessedGroups = {} #each group shall only create one submenu. No nested menus.
        for key, value in api._getScore().nonTrackContainers.items():  #key is the backend container, value is the instance count and not important.
            group = key.group
            if group and not group in alreadyProcessedGroups:
                newSubMenu = self.scoreContainerMenu.addMenu(group)
                alreadyProcessedGroups[group] = newSubMenu
            pretty = " ".join(key.uniqueContainerName.split("_")).title()
            if group:
                entry = alreadyProcessedGroups[group].addAction(key.uniqueContainerName)
            else:
                entry = self.scoreContainerMenu.addAction(key.uniqueContainerName)
            entry.setText(pretty) #but display a pretty name.
            self.connectMenuAction(entry, lambda key=key: api.insertContainer(key))
            #self.connect(entry,QtCore.SIGNAL('triggered()'), lambda key=key: api.insertContainer(key))

    def toggleBarlines(self, onOff):
        """Switch GUI barlines on or off.
        This alone is only a relay station and is not a switch in itself
        toggleBarlines in main decides what to do"""
        #switching on
        if onOff:
            for tr in self.tracks:
                tr.updateBarlines()
        #switching off
        else:
            for tr in self.tracks:
                tr.deleteBarlines()

    def changeGroup(self, groupText):
        """Changes the current active Track group and its color"""
        api.trackChangeAttribute("group", groupText)
        self.currentTrack.incipit.changeGroupColor()

    def changeNameMarker(self, nameText):
        """Changes the current active Track group and its color"""
        api.trackChangeAttribute("shortInstrumentName", nameText)
        self.currentTrack.incipit.changeNameMarker()

class WelcomeTab(QtGui.QWidget):
    """The first tab. Give direct control to mouse users"""
    def __init__(self):
        super(WelcomeTab, self).__init__()
        self.ui = Ui_Form()  #self.ui is the node to all qt widgets in this tab.
        self.ui.setupUi(self)

class GuiCursorPosition(QtGui.QGraphicsRectItem):
    def __init__(self, parent):
        super(GuiCursorPosition, self).__init__(0, 0,  3, 40)
        self.setOpacity(1)
        self.setBrush(QtGui.QColor("red"))
        self.pitch = api._getCursorPitch()
        self.setZValue(0)
        self.parent = parent # I belong to this parent score/Workspace
        self.position = 0  #current X position, in pixel/guiTicks. Needed for the pitch cursor up and down without changing X.
        self.posX = 0 # a cache value.
        #self.highlightEffect = QtGui.QGraphicsColorizeEffect() #conveniently this is a unique effect. apply it one item and it deletes itself from all other items. But this is also non-convenient because it removes from all linked items.

    def update(self):
        #self.position = api._getFlatCursorIndex() #get the position directly from the backend
        #self.parent.currentTrack
        currentGuiItem = self.parent.currentTrack.getCurrentItem()
        posY = self.parent.currentTrack.pos().y()
        if currentGuiItem:
            self.posX = currentGuiItem.pos().x()
        else: #appending
            self.posX = self.parent.currentTrack.cachedTrackWidth * config.DURATION_MOD
        self.setPos(self.posX, posY)
        if self.parent.currentTrack.collapsed:
            self.setTransform(COLLAPSED_TRANSFORM)
        else:
            self.setTransform(EXPANDED_TRANSFORM)

        self.parent.cursorPitch.update()
        self.makeVisible()

    def makeVisible(self):
        """Check if the cursor is outside the visible working area.
        if not make it visible."""
        scenePos = self.parent.workingAreaWidget.mapFromScene(self.pos())
        cursorViewPosX = scenePos.x() #the cursor position in View coordinates
        cursorViewPosY = scenePos.y() #the cursor position in View coordinates. 0 is the view beginning,  has nothing to do with the geometry/main window boundaries.
        geo = self.parent.workingAreaWidget.geometry()

        if cursorViewPosY + TRACK_SIZE > geo.height (): #pagebreak down
            bar = self.parent.workingAreaWidget.verticalScrollBar()
            bar.setValue(self.pos().y() * self.parent.workingAreaWidget.transform().m11() - self.parent.workingAreaWidget.viewport().height() /2)
        elif cursorViewPosY < 10: #pagebreak down
            bar = self.parent.workingAreaWidget.verticalScrollBar()
            bar.setValue(bar.value() - self.parent.workingAreaWidget.viewport().height() /2 + 50)

        if cursorViewPosX + 50 >= geo.width(): #pagebreak forward
            bar = self.parent.workingAreaWidget.horizontalScrollBar()
            bar.setValue((self.posX-8) * self.parent.workingAreaWidget.transform().m11())
        elif cursorViewPosX < 10: #pagebreak backwards
            bar = self.parent.workingAreaWidget.horizontalScrollBar()
            bar.setValue(bar.value() - self.parent.workingAreaWidget.viewport().width() + 50)
            #last resort. The cursor is still not visible. We had a go to head etc.
            if self.parent.workingAreaWidget.mapFromScene(self.pos()).x() < 10:
                self.parent.workingAreaWidget.centerOn(self.posX, self.pos().y())

        self.parent.workingAreaWidget.viewport().update()


    def centerOn(self):
        """Forces the cursor position to a centered location,
        even if it already was visible somehow.
        Intended for manual repositioning."""
        self.parent.workingAreaWidget.centerOn(self.posX, self.parent.currentTrack.pos().y())

class GuiCursorPitch(QtGui.QGraphicsRectItem):
    def __init__(self, parent):
        super(GuiCursorPitch, self).__init__(5, 0,  20, 4)
        self.setOpacity(1)
        self.setBrush(QtGui.QColor("red"))
        self.pitch = api._getCursorPitch()
        self.setZValue(10)
        self.parent = parent # I belong to this parent score/Workspace
        self.ledgerlines = []

    def updateLedgerLines(self):
        #Draw Ledger Lines
        for line in self.ledgerlines:
            self.scene().removeItem(line)
        self.ledgerlines = []

        start_under = 50
        start_above = -10

        #Determine if we need ledger lines.
        if self.pitch == 20:
            var = 0
        else:
            part1 = -1 * (self.pitch / PITCH_MOD) + self.parent.currentTrack.cachedClefPitch
            var = part1 + part1 + PITCH_MOD/2  #PM/2 is half of a glyph size. Normally glyphs would bottom align on the Y coordinate/staff-line so we shift half of the glyph so we have the glyph in the middle.

        if self.parent.currentTrack.dualView and var >= 45: #Only if we are going down and have a gui-dual staff active
            var -= 115
            start_under = 110

        if var >= 45: #ledger lines below
            for nr in range(math.floor(var / 2 / PITCH_MOD) - 1):
                lines_under = QtGui.QGraphicsLineItem(QtCore.QLineF(0, start_under + nr * 10 , 22, start_under + nr * 10))
                lines_under.setPen(PEN_LINE)
                lines_under.setParentItem(self.parent.currentTrack)
                lines_under.setPos(2+self.parent.cursorPosition.posX,0)
                self.ledgerlines.append(lines_under)

        elif var <= -75: #ledger lines above
            for nr in range(math.floor(-1 * var / 2 / PITCH_MOD -2.5)):
                lines_above = QtGui.QGraphicsLineItem(QtCore.QLineF(0, start_above - nr * 10 , 22, start_above - nr * 10))
                lines_above.setPen(PEN_LINE)
                lines_above.setParentItem(self.parent.currentTrack)
                lines_above.setPos(3+self.parent.cursorPosition.posX,0)
                self.ledgerlines.append(lines_above)

    def update(self):
        self.pitch = api._getCursorPitch() #why is this not needed for standard update? But ledgerlines needs it.
        x = self.parent.cursorPosition.posX
        y = self.parent.currentTrack.pos().y() + -1*(self.pitch / PITCH_MOD) + self.parent.currentTrack.cachedClefPitch

        if self.parent.currentTrack.collapsed:
            self.hide()
        else:
            self.show()
        self.setPos(x, y + 28)
        self.updateLedgerLines()


class GuiCursorPlayback(QtGui.QGraphicsRectItem):
    """One for each GuiTrack"""
    def __init__(self, parent):
        super(GuiCursorPlayback, self).__init__(0, 0,  0, 0)
        self.parent = parent # I belong to this parent GuiTrack
        self.setOpacity(1)
        self.setBrush(QtGui.QColor("blue"))
        self.setPen(QtCore.Qt.transparent)
        self.setZValue(90)
        self.hide()
        self.playbackActive = False



    def update(self):
        """Collaps transform is done automatically since this is part
        of the track Item Group"""
        self.setRect(0,0,3,self.parent.graphicsViewRect[3])

    def makeVisible(self):
        self.parent.workingAreaWidget.centerOn(self.pos().x(), self.parent.currentTrack.pos().y())
        self.parent.workingAreaWidget.viewport().update()

class GuiScore(MusicTab):
    """A gui score holds all guiItems in plain python data types
    but it also the access point to the qgraphicsview and scene.
    The GuiScore itself is also the Widget inside one Tab
    through inherited MusicTab"""

    def __init__(self, backendWorkspace):
        super(GuiScore, self).__init__()
        self.tracks = [] #GuiTracks
        self.cursorPitch = GuiCursorPitch(self)
        self.scene.addItem(self.cursorPitch)
        self.cursorPosition = GuiCursorPosition(self)
        self.scene.addItem(self.cursorPosition)
        self.playbackCursor = GuiCursorPlayback(parent = self)
        self.scene.addItem(self.playbackCursor)
        self.data = backendWorkspace
        self.fileName = None
        self.trackEdit = False #Is this a solo container edit? Normally not.
        self.displacementMap = [] # A sorted list of pairs (tick-position, displacement) which contains the maximum displacement from all tracks. A very dynamic variable which gets changed and calculated all the time.
        def appendingItem():
            return [PSEUDO]
        self.allItems=defaultdict(appendingItem,[]) # All items. backend-instance as key, list of gui-items as value.  There is only one instance for each unique backend item. A linked item is twice the same. So we can actually use the instance as key in this dict to find all gui items based on that backend-item.
        self.updateStatusBar()

    def insertItemRelativeToContainerStart(self, backendItem, container, howManySteps, standaloneInsert):
        """This is a gui only update process. No backend data,
        especially not their cursors, are changed
        For each track with a containerStart item crawl from the
        beginning to the end (lefPart + rightPart) and stop on
        each containerStartItem, go right the howManySteps, insert
        the item. Continue to search right. Thank you, next track.

        In the end go back to the backend cursor position which did not
        change during the whole process.
        Then update the changes in the gui.

        This insert assumes that there are no recursive containers.

        We need to create new items from the backendItem here. One
        for each container instance.
        """
        itemType = type(backendItem)
        #typ = type(guiItem)
        #if typ is GuiMultiMeasureRest or typ is GuiUpbeat: #some GuiItems don't work with the backendData but are synced manually. Multimeasure Rests and Upbeats.
        #    data = guiItem.dataOrg
        #else:
        #    data = guiItem.data

        realSteps =  howManySteps -2 #-1 for 1vs0 counting. additional -1 because we are already at the container start.
        containerBackendStartItem = container[0] # the backend container start.
        allTracks = set()
        for tr in self.allItems[containerBackendStartItem]: #The list of guiItems connected to this backend Item. All container start in the current score.
            allTracks.add(tr.track) #the guiTracks
        for tra in allTracks:
            tra.reset() #all items to track.rightPart
            #steps in nested containers do not count. The whole nested container counts as 1 step.
            countdown = realSteps
            while True:
                containerStack = [] #register to keep track of nested containers in the gui, where they are just items in a row, not real nested like in the backend.
                currentItem = tra._right() #returns the current item or false.
                if not currentItem:
                    break #Track End

                #print ("[InsertRelative]Found Match Start at", len(tra.rightPart), ". Items left:", len(tra.leftPart))  #len() returns counting from 1
                if currentItem.breakContainerLoop and currentItem.data == containerBackendStartItem: #we have found a containerStart and it is the same. Start a sub-loop.
                    while countdown > 0:
                        innerItem = tra.getCurrentItem()
                        if innerItem.breakContainerLoop: #container Start()!
                            containerStack.append(True)
                        elif innerItem.breakContainerLoop is None: #and not False! Container End()
                            #print (innerItem, innerItem.data, innerItem.data.exportLilypond())
                            containerStack.pop()

                        if not containerStack: #we are not in a nested container
                            countdown -= 1
                        tra._right()
                    else: #we reached 0 steps.
                        countdown = realSteps
                        before = tra.leftPart[-1]
                        #print (before.data.exportLilypon(), before.
                        forceSigs  = [tra, before.cachedClefPitch, before.cachedKeysigData, before.cachedTimesigData]
                        new = api.l_item._convertBackendItemToGuiItem(backendItem, itemType, forceSigs = forceSigs)  #the big typechecking machine
                        #new = typ(data) #create a new for each container. The gui only knows unique items. Essentialy that means we copy, and forget the original one.
                        #new = copy.copy(guiItem) #QGraphicsItems are NOT copy-able. Qt simply does not allow it.
                        if new.breakContainerLoop is None and tra.getCurrentItem().breakContainerLoop: #and not False! #a quick bugfix for container ends. Move one further. Else container start and end are reversed.
                            tra._right()
                        #Achtung: Inserting an empty container into a container end may result in two reversed containerEnd in the GUI. For the user it makes no difference. Only when we begin to utilize the containerEnd for something else, like drawing backgrounds, this will confuse the display.
                        tra.insert(new, standaloneInsert = standaloneInsert) #Insert at cursor position. Goes right after inserting.
                        self.allItems.setdefault(backendItem, []).append(new)

                #Continue looping, there may be more container in the same track. Actually that is very likely and the normal case.

        #finally update all necessary track durations and positioning.
        api.l_send(lambda: api.l_score.updateTrackFromItem(backendItem)) #update cursor position is done in here also

    def deletetItemRelativeToContainerStart(self, backendItem, container, howManySteps):
        """Based on the insert variant"""
        realSteps =  howManySteps -1 #-1 for 1vs0 counting.
        containerBackendStartItem = container[0] # the backend container start.
        allTracks = set()
        for tr in self.allItems[containerBackendStartItem]: #The list of guiItems connected to this backend Item. All container start in the current score.
            allTracks.add(tr.track) #the guiTracks
        for tra in allTracks:
            tra.reset() #all items to track.rightPart
            #steps in nested containers do not count. The whole nested container counts as 1 step.
            countdown = realSteps
            while True:
                containerStack = [] #register to keep track of nested containers in the gui, where they are just items in a row, not real nested like in the backend.
                currentItem = tra._right() #returns the current item or false.
                if not currentItem:
                    break #Track End

                if currentItem.breakContainerLoop and currentItem.data == containerBackendStartItem: #we have found a containerStart and it is the same. Start a sub-loop.
                    while countdown > 0:
                        innerItem = tra._right()
                        if innerItem.breakContainerLoop: #container start!
                            containerStack.append(True)
                        elif innerItem.breakContainerLoop is None: #and not False!
                            containerStack.pop()
                        if not containerStack: #we are not in a nested container
                            countdown -= 1
                    else: #we reached 0 steps
                        countdown = realSteps
                        tra.delete(backendItem)

                #Continue looping, there may be more container in the same track. Actually that is very likely and the normal case.

        #finally update all necessary track durations and positioning.
        api.l_send(lambda: api.l_score.updateTrackFromItem(containerBackendStartItem)) #update cursor position is done in here also

    def fullReload(self):
        api.trackFirst()
        api.head()
        self.loadBackendData()

    def loadBackendData(self):
        """Read in the backend score and create GUI items.
        Used on start and load file.
        This is the only time where items are not inserted at
        the backendcursor position. So we have to make a few update runs
        """
        main.ui.centralwidget.hide()
        for num, tr in enumerate(self.tracks): #it is possible to call this function more than once, in this case we need to get rid of old items first
            self.incipitScene.removeItem(tr.incipit)
            self.scene.removeItem(tr)
        self.repositionTracks()
        self.tracks = []

        for track in api._getScore(): #First pass create all tracks. Otherwise api.trackDown() in the next step will trigger a gui track down which will not know where to go.
            self.addTrack(track)
        for track in api._getScore():
            for item in track:
                api.l_item.insertAtCursor(item, getContainer =None, standaloneInsert = False) #don't update after every item insert. . #we are assuming a non-container based entry. This is safe on load (in opposite to cursor insert) because if there is a container it is already complete needs no checking if the cursor is in front of a ContainerStart etc.  insertAtCursor will take care of the recursive container insert and force getContainer to True
            api.trackDown()
        api.trackFirst()
        api.head()
        self.buildDisplacementMap()
        for track in self.tracks:
            track.signatureReset()
            track.updateRightFromStart()  #This creates the correct positioning. Until now all items were just added to 0,0 so that we don't get a gui update after each insert

        api.trackFirst()
        api.head()
        self.generateScoreSubstitutionsMenu()
        self.generateSubstitutionsMenu()
        self.generateScoreContainerMenu()
        main.ui.centralwidget.show()

    def buildDisplacementMap(self):
        """Return a unified list for all tracks.
        Contains the maximum displacement for a tickposition
        from all tracks.
        It modifies an instance variable which is basically a chache."""
        combined_dicts = []
        for track in self.tracks:
            #Goal: [(384, 10), (384, 15), (768, 15)] => [(384,25), (768, 15)]
            d = defaultdict(int) #int = 0. with a default dict we later can overwrite AND create a key with += instead of having to check for existence first.
            for guiItem in track.displacements:
                d[guiItem.cachedTickIndex] += guiItem.displacement
            combined_dicts.append(d)

        result = {}
        for d in combined_dicts:
            result.update((k, v) for k, v in d.items() if v > result.get(k, 0))
        self.displacementMap = sorted(result.items())

    def getDisplacement(self, tickindex, includeSelf):
        """Return the complete displacement up to a certain tickindex
        while you can choose to include the tickindex position itself
        or not"""
        complete = 0
        if includeSelf:
            for position, displacement in self.displacementMap:
                if position > tickindex:
                    break
                complete += displacement
        else:
            for position, displacement in self.displacementMap:
                if position == tickindex:
                    break
                complete += displacement

        return complete

    @property
    def currentTrackIndex(self):
        return api._getTrackIndex()

    @property
    def currentTrack(self):
        return self.tracks[self.currentTrackIndex]

    def swapTrackWithAbove(self):
        """Move the complete Track up which results in the upper Track moving down"""
        #When now is defined it is already the new situation because we derive tracknumber from the api
        above = self.currentTrackIndex
        now = above + 1
        self.tracks[now].setPos(0, above * TRACK_SIZE)
        self.tracks[above].setPos(0, now * TRACK_SIZE)
        self.tracks.insert(above , self.tracks.pop(now)) #swap the track
        self.repositionTracks()

    def swapTrackWithBelow(self):
        """Move the complete Track down which results in the lower Track moving up"""
        #When now is defined it is already the new situation because we derive tracknumber from the api
        below = self.currentTrackIndex
        now = below - 1
        self.tracks[now].setPos(0, below * TRACK_SIZE)
        self.tracks[below].setPos(0, now * TRACK_SIZE)
        self.tracks.insert(below , self.tracks.pop(now)) #swap the track
        self.repositionTracks()

    def addTrack(self, backendTrack):
        #Let's assume the backend tells us to create a track in the last position if api.addTrack() is called. So this is only for creating a gui track.
        track = GuiTrack(workspace = self, backendTrack = backendTrack)
        self.tracks.append(track)
        self.scene.addItem(track)
        self.incipitScene.addItem(track.incipit)
        self.repositionTracks()
        track.updateRight() #Do it once for the empty track. This shows the stafflines and costs nothing

    def deleteTrack(self, trackNumberToDelete, newBackendTrackIndex):
        """delete all the items and lines"""
        currentTrack = self.tracks[trackNumberToDelete]
        self.incipitScene.removeItem(currentTrack.incipit)
        self.scene.removeItem(currentTrack)
        del self.tracks[trackNumberToDelete]
        self.repositionTracks()
        if not self.tracks:  #was this the last track?
            self.addTrack(api._getTrack()) #there is only one left in the backend.

    def sendMaxTrackWidthToMain(self):
        self.graphicsViewRect[2] = max([trk.cachedTrackWidth for trk in self.tracks])  * config.DURATION_MOD
        #no need for incipitRectView updates here
        self.updateSceneRect()

    def repositionTracks(self):
        """Reposition all tracks according to their index.
        Very fast."""
        offsetsSoFar = 0
        trackPositions = [] # (highest, lowest, gui-track)
        oneLyricSpace = FONT_SIZE/1.5 if config.LYRICS else 0
        lowerBoundary = 0
        for trk in self.tracks:
            highestPos = lowerBoundary #from the last time

            trk.setPos(0, highestPos)
            trk.incipit.setPos(0, highestPos)

            lowerBoundary += (trk.cachedVerseCount*oneLyricSpace + trk.size) * trk.offsetFactor  + 10/trk.offsetFactor

            trackPositions.append((highestPos, lowerBoundary, trk)) #upper boundary, lower boundary, track-object

        self.graphicsViewRect[3] = len(self.tracks) * TRACK_SIZE #TODO: doesn't this make the viewport too small if we have mostly double sized tracks?! There is an ugly +safetyPixels elsewhere to prevent that! Better correct here.
        #no need for incipitGraphicsViewRect update here.

        #Uncomment to visualize track boundaries
        """
        try:
            for r in self.rects:
                self.scene.removeItem(r)
        except:
            pass
        self.rects = []
        for ob, un, tr in trackPositions:
            rect = QtGui.QGraphicsRectItem(0, ob, 100, un-ob)
            #rect = QtGui.QGraphicsRectItem(tr.sceneBoundingRect())
            rect.setBrush(QtGui.QColor("pink"))
            self.scene.addItem(rect)
            self.rects.append(rect)
        """

        #The outer tracks have no boundaries.
        if len(trackPositions) >= 2:
            trackPositions[0] = (float("-inf"), trackPositions[0][1], trackPositions[0][2])
            trackPositions[-1] = (trackPositions[-1][0], float("inf"), trackPositions[-1][2])
        elif len(trackPositions) == 1:
            trackPositions = [(float("-inf"), float("inf"), self.tracks[0])]

        self.cachedTrackPositions = trackPositions #is used by the mouse click
        self.updateSceneRect()


    def playbackCursorSetVisible(self, boolean):
        """Called by signal playbackSetPosition"""
        if self.playbackCursor.playbackActive: #only for the current tab
            if boolean:
                self.playbackCursor.makeVisible()
                self.playbackCursor.update() #adjust to track height etc.
                self.playbackCursor.show()
            else:
                self.playbackCursor.hide()
            self.workingAreaWidget.viewport().update()

    def playbackCursorSetTicks(self, ticks):
        if self.playbackCursor.playbackActive: #only for the current tab
            #TODO: this will possibly block the GUI. Needs performance tests. Is it possible to do in parallel?
            linePos = (ticks + self.getDisplacement(ticks, includeSelf = True)) + 96 #Small trick. It is always a 32th note ahead. has a better "during the note" playback feel.
            self.playbackCursor.setX(linePos * config.DURATION_MOD)
            self.playbackCursor.makeVisible()
            #TODO: Make visible? Option to follow cursor or not? Scroll Lock!

class Session(object):
    """All open files with their QGraphicScenes etc.
    Also manages the saveStatus warning. The savestatus system is purely
    a GUI one.
    If the saveStatus set is empty/clean there are no unsaved changes.
    So only the "save" function can call setSavestatusClean while
    a lot of functions can call it dirty.
    Load does not need any saveStatus access because initially loaded
    data is clean."""
    def __init__(self):
        self.scenes = {} # key is backend Workspace, value is the current workspace.
        self.saveStatus = set() #A set of all changed and unsaved backend workspaces. Scores check with "if self in saveStatus" and the program checks with "if saveStatus".
        self.playbackEngine = None #initiated through laborejo-qt
        self.ourNsmClient = None #In case we are under session control
        self.nsmClientId = None

        #Last Known Dirs. Remember for convenience
        self.last_open_dir = HOME
        self.last_save_dir = HOME
        self.last_export_dir = HOME
        self.last_import_dir = HOME
        self.last_subst_dir = HOME
        self.last_export_files = {"pdf":[], "ly":[], "jack":[], "smf":[], "bin":[], }
        self.last_export_extension_radioButton = None
        self.last_export_splitBy_radioButton = None
        self.palette = QtGui.QPalette()

    @property
    def currentGuiWorkspace(self):
        if api._getWorkspace() in self.scenes:
            return self.scenes[api._getWorkspace()]
        else:
            return None

    def backendWorkspaceToGuiTabIndex(self, backendWs):
        if backendWs in self.scenes:
            return main.ui.Tab.indexOf(self.scenes[backendWs]) #from 0 to n
        else:
            raise BaseException("Backend-Workspace is not in the scene database. Please write down what you did to load this file and contact the developers. This is critical.")
            return None

    def isFilenameAvailable(self, newFileName):
        l = set()
        for backendMov, guiMov in self.scenes.items():
            l.add(guiMov.fileName)
        if newFileName in l:
            return False
        else:
            return True

    def isCurrentClean(self):
        """Check if the current active workspace/score is clean"""
        return not api._getWorkspace() in self.saveStatus #return True if it is not in saveStatus, which means there is no unsaved Data which is good and therefore true.

    def setSavestatusClean(self):
        """Clean the save status for the current active workspace."""
        ws = api._getWorkspace()
        if ws in self.saveStatus:
            self.saveStatus.remove(ws)
            main.ui.Tab.tabBar().setTabTextColor(self.backendWorkspaceToGuiTabIndex(ws), self.palette.color(self.palette.WindowText))

    def setAllClean(self):
        for ws in self.saveStatus:
            main.ui.Tab.tabBar().setTabTextColor(self.backendWorkspaceToGuiTabIndex(ws), self.palette.color(self.palette.WindowText))
        self.saveStatus.clear()

    def setSavestatusDirty(self):
        """Add the current score to the set of unclean data"""
        ws = api._getWorkspace()
        if not ws in self.saveStatus: #no point in making something dirty even dirtier.
            self.saveStatus.add(ws)
            main.ui.Tab.tabBar().setTabTextColor(self.backendWorkspaceToGuiTabIndex(ws), self.palette.color(self.palette.LinkVisited))
            if self.ourNsmClient:
                self.ourNsmClient.setDirty(True)


HOME = os.path.expanduser("~") #lin and win
