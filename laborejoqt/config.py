# -*- coding: utf-8 -*-
import sys
from os import path
import laborejocore as api

JACK = True #Switch on/off the whole playback engine. Critical setting. Needs restarting for change.
JACKMODE = False #Determine if you want jackmode on playback or not. Trivial setting, can be toggled at any time in the program itself.
BARLINES = True #Show the barlines in the GUI. Trivial setting, can be toggled at any time in the program itself.
LYRICS = False #Show the Lyrics in the GUI. Trivial setting, can be toggled at any time in the program itself.
DURATION_MOD = 0.15 #Default: 0.15 #Bigger means more space between all notes and objects
MIDI_TIMER = 34 #miliseconds update period. Used for midi-in detection and playback cursor movement smoothness. Default 34 is equal to around 30 frames/updates per second. The lower it is the less latency you experience for midi in. Low values may drag down performance. Try to stay above 10ms.
AUDIO_FEEDBACK = True #Change to False if you want to start the GUI without Audio feedback (e.g. flats/sharps, virtual Piano). Trivial setting, can be toggled at any time in the program itself.
TRANSPORT_KEEP_ROLLING = False #True lets the jack transport system, aka playback, rolling even after the song in Laborejo is over. Trivial setting, can be toggled at any time in the program itself.

#Laborejo uses external programs for some of its functions. It has default names for these but they can be changed.
#You can give a executable name to use a specific progra version (either in your PATH or absolute path).
#Use an empty string "" for the default value
lilypondbinary = "" #Default: "lilypond". Alternative example: "/usr/local/bin/lilypond"
pdfviewer = "" #Default: "xdg-open". Alternative example: "/usr/bin/xpdf"
midiinport = "" #Jack Midi input port for note insert. "client:port". No ALSA. Default: no connection. Alternative example: "jack-keyboad:midi_out"

#Shortcuts
shortcuts = {
        #Non-menu
        #Modal Commands
        "1" : "modalcommands.commands.keys[1]",
        "2" : "modalcommands.commands.keys[2]",
        "3" : "modalcommands.commands.keys[3]",
        "4" : "modalcommands.commands.keys[4]",
        "5" : "modalcommands.commands.keys[5]",
        "6" : "modalcommands.commands.keys[6]",
        "7" : "modalcommands.commands.keys[7]",
        "8" : "modalcommands.commands.keys[8]",
        "9" : "modalcommands.commands.keys[9]",
        #"0" : "modalcommands.commands.keys[0]",  #0 is used as emacs shortcut for direct duration insert.

        "Shift+1" : "modalcommands.commands.shifts[1]",
        "Shift+2" : "modalcommands.commands.shifts[2]",
        "Shift+3" : "modalcommands.commands.shifts[3]",
        "Shift+4" : "modalcommands.commands.shifts[4]",
        "Shift+5" : "modalcommands.commands.shifts[5]",
        "Shift+6" : "modalcommands.commands.shifts[6]",
        "Shift+7" : "modalcommands.commands.shifts[7]",
        "Shift+8" : "modalcommands.commands.shifts[8]",
        "Shift+9" : "modalcommands.commands.shifts[9]",
        "Shift+0" : "modalcommands.commands.shifts[0]",

        #"Alt+1" : "modalcommands.commands.alts[1]",
        #"Alt+2" : "modalcommands.commands.alts[2]",
        #"Alt+3" : "modalcommands.commands.alts[3]",
        #"Alt+4" : "modalcommands.commands.alts[4]",
        #"Alt+5" : "modalcommands.commands.alts[5]",
        #"Alt+6" : "modalcommands.commands.alts[6]",
        #"Alt+7" : "modalcommands.commands.alts[7]",
        #"Alt+8" : "modalcommands.commands.alts[8]",
        #"Alt+9" : "modalcommands.commands.alts[9]",
        #"Alt+0" : "modalcommands.commands.alts[0]",

        "Alt+Shift+1" : "modalcommands.commands.altShifts[1]",
        "Alt+Shift+2" : "modalcommands.commands.altShifts[2]",
        "Alt+Shift+3" : "modalcommands.commands.altShifts[3]",
        "Alt+Shift+4" : "modalcommands.commands.altShifts[4]",
        "Alt+Shift+5" : "modalcommands.commands.altShifts[5]",
        "Alt+Shift+6" : "modalcommands.commands.altShifts[6]",
        "Alt+Shift+7" : "modalcommands.commands.altShifts[7]",
        "Alt+Shift+8" : "modalcommands.commands.altShifts[8]",
        "Alt+Shift+9" : "modalcommands.commands.altShifts[9]",
        "Alt+Shift+0" : "modalcommands.commands.altShifts[0]",

        #Keypad / Numpad Widget
        "QtCore.Qt.KeypadModifier+QtCore.Qt.Key_1" : "numpadAction1",
        "QtCore.Qt.KeypadModifier+QtCore.Qt.Key_2" : "numpadAction2",
        "QtCore.Qt.KeypadModifier+QtCore.Qt.Key_3" : "numpadAction3",
        "QtCore.Qt.KeypadModifier+QtCore.Qt.Key_4" : "numpadAction4",
        "QtCore.Qt.KeypadModifier+QtCore.Qt.Key_5" : "numpadAction5",
        "QtCore.Qt.KeypadModifier+QtCore.Qt.Key_6" : "numpadAction6",
        "QtCore.Qt.KeypadModifier+QtCore.Qt.Key_7" : "numpadAction7",
        "QtCore.Qt.KeypadModifier+QtCore.Qt.Key_8" : "numpadAction8",
        "QtCore.Qt.KeypadModifier+QtCore.Qt.Key_9" : "numpadAction9",
        "QtCore.Qt.KeypadModifier+QtCore.Qt.Key_0" : "numpadAction0",
        "QtCore.Qt.KeypadModifier+QtCore.Qt.Key_Plus" : "actionPaletteNext",
        "QtCore.Qt.KeypadModifier+QtCore.Qt.Key_Minus" : "actionPalettePrevious",

        #Non-Modal Commands
        "Alt+1" : "actionClefs",
        "Alt+2" : "actionTime_Signatures",
        "Alt+3" : "actionKey_Signatures",
        "Alt+4" : "actionPerformance_Signature",
        "Alt+5" : "actionTempo_Signature",
        "Alt+6" : "actionInstrument_Change",
        #"Alt+7" : "",
        #"Alt+8" : "",
        #"Alt+9" : "",
        #"Alt+0" : "",

        "F2" : "actionScript",
        "F3" : "actionView_PDF",
        "Shift+F3" : "actionQuick_View_PDF_Export_Group",

        "F5" : "actionToolbarPlayStop",
        "Ctrl+F5" : "actionToolbarPlayFromStart",
        "Shift+F5" : "actionPlaySolo",
        "Alt+F5" : "actionPlayGroup",
        "F6" : "actionToolbarEmergencyPlaybackStop",
        "F7" : "actionToolbarJackModeToggle",
        "F8" : "actionMIDI_Note_Entry",

        #Navigation
        "Left" : "api.left",
        "Right" : "api.right",
        "Shift+Left" : "api.selectLeft",
        "Shift+Right" : "api.selectRight",
        "Ctrl+Left" : "api.measureLeft",
        "Ctrl+Right" : "api.measureRight",
        "Ctrl+Shift+Left" : "api.selectMeasureLeft",
        "Ctrl+Shift+Right" : "api.selectMeasureRight",
        "Ctrl+Shift+Up" : "api.selectMeasure",
        "Ctrl+Shift+Down" : "api.selectMeasure",
        "Ctrl+Up" : "api.upOctave",
        "Ctrl+Down" : "api.downOctave",
        "Home" : "api.head",
        "Shift+Home" : "api.selectHead",
        "End" : "api.tail",
        "Shift+End" : "api.selectTail",
        "Up" : "api.up",
        "Down" : "api.down",
        "Page Up" : "api.trackUp",
        "Page Down" : "api.trackDown",
        "Ctrl+Home" : "api.trackFirst",
        "Ctrl+End" : "api.trackLast",

        #Basic Modification
        "W" : "actionShift_Up",
        "S" : "actionShift_Down",
        "Shift+W" : "actionShift_Octave_Up",
        "Shift+S" : "actionShift_Octave_Down",

        "Alt+W" : "actionShift_Note_Up",
        "Alt+S" : "actionShift_Note_Down",
        "Alt+Shift+W" : "actionShift_Note_Octave_Up",
        "Alt+Shift+S" : "actionShift_Note_Octave_Down",

        "Return" : "actionAdd_Note_to_Chord",
        "Shift+Return" : "actionAdd_Octave_to_Chord",
        "Shift+Delete" : "actionRemove_Note_from_Chord",

        "+" : "actionSharp",
        "-" : "actionFlat",
        "?" : "actionReminder",
        #"Shift+#" : "actionCautionary",
        #Beware of the + and - keys. Locale Alert!
        #"Alt+-" : "", reserved for micro-tonality
        #"Alt++" : "", reserved for micro-tonality

        #Menu

        #File
        #With a small python trick. Dictionaries are case sensitive, but Qt does not care about the case for shortcuts.
        #So we assign the same shortcuts to mutually exclusive menu entries.
        "Ctrl+O" : "actionOpen",
        "Ctrl+o" : "actionImport_to_Session",
        "Ctrl+Shift+O" : "actionOpen_as_Template",
        "Ctrl+Shift+o" : "actionImport_to_Session_as_Template",
        "Ctrl+S" : "actionSave",
        "Ctrl+Shift+S" : "actionSave_As",
        "Ctrl+Shift+s" : "actionExport_from_Session",
        "Ctrl+N" : "actionNew",
        "Ctrl+n" : "actionNew_to_Session_2",
        "Ctrl+Q" : "actionQuit",
        "Ctrl+W" : "actionClose",

        #Edit
        "Ctrl+a" : "actionSelect_Track",
        "Ctrl+Shift+a" : "actionSelect_All",
        "Ctrl+i" : "actionInvert_Selection",
        "Space" : "actionClear_Selection",
        "Ctrl+C" : "actionCopy",
        "Ctrl+X" : "actionCut",
        "Ctrl+V" : "actionPaste",
        "Ctrl+Z" : "actionUndo",
        "Ctrl+Shift+Z" : "actionRedo",
        "Delete" : "actionDelete",
        "Ctrl+Shift+V" : "actionPaste_replace_Selection",
        "Backspace" : "actionDelete_Previous",
        "E" : "actionDuplicate",
        "Shift+E" : "actionLink",

        #View
        "Ctrl+E" : "actionSidebar",
        "Ctrl+L" : "actionLyrics",
        "Ctrl+M" : "actionMinimap",
        "Ctrl+B" : "actionBarlines",
        "Ctrl+Shift++" : "actionWiden",
        "Ctrl+Shift+-" : "actionShrinken",
        "Ctrl++" : "actionZoom_In",
        "Ctrl+-" : "actionZoom_Out",
        "F1" : "actionSmall_Terminal",

        #Container
        #"Alt+Shift+Page Up" : "actionDelete_Current_Track",
        #"Alt+Shift+Page Down" : "actionAdd_Track",
        "Insert" : "actionInsert_Empty_Container",
        "Shift+Insert" : "actionInsert_Existing_Container",
        "Alt+Insert" : "actionEdit_Container_Content",
        "Alt+Page Up" : "actionMove_Track_Up",
        "Alt+Page Down" : "actionMove_Track_Down",
        #
        "Alt+Return" : "actionEdit_Current_Object",
        "T" : "actionTriplet",
        "Alt+T" : "actionCustom_Tuplet",
        "D" : "actionAugment",
        "A" : "actionDiminish",

        "Tab" : "actionAdd_Dot", #This is a duplicate entry. Dot is in twice and the menu version shows "."
        "Shift+Tab" : "actionRemove_Dot", #This is a duplicate entry. Dot is in twice and the menu version shows "Shift+."
        "." : "actionToolbarNext_Becomes_Dotted",

        "Alt+." : "actionToggle_Prevailing_Dot",
        "Alt+M" : "actionMultiMeasureRest",
        "M" : "actionFullMeasureRest",
        "Shift+T" : "actionClear_Tuplets",
        "F" : "actionFermata",
        "B" : "actionBeam_Start",
        "Shift+B" : "actionBeam_End",
        "G" : "actionSlur_Start_On_Off",
        "Shift+G" : "actionSlur_End_On_Off",
        "C" : "actionEdit_Chord_Symbol",
        "Shift+C" : "actionClear_Chord_Symbol",
        "Alt+C" : "actionMass_Insert_Chord_Symbols",
        "P" : "actionEdit_Figured_Bass",
        "Shift+P" : "actionClear_Figured_Bass",
        "Alt+P" : "actionMass_Insert_Figured_Bass",
        "Q" : "actionSplit_in_Two",
        "Shift+Q" : "actionSplit_in_Three",
        "Alt+Q" : "actionCustom_Split",
        "I" : "actionTie_One_Note_On_Off",
        "U" : "actionUpbeat",
        "R" : "actionTrill",
        "," : "actionStaccato",
        "_" : "actionTenuto",
        "x" : "actionFree_Text",
        "j" : "actionJoin_to_Chord_prevalent_duration",
        "Shift+j" : "actionJoin_to_Chord_sum_durations",
        "z" : "actionSustain_Change_non_print",
        "Shift+z" : "action_Sustain_On",
        "Alt+z" : "action_Sustain_Off",
        "Alt+E" : "actionFull_Dual_Ending_Sequence",

        #Advanced
        "h": "actionToggleTransparent",
        "Alt+h" : "actionToggleHidden",
        "Shift+h" : "actionCleanHiddenTransparent",

        #Force new duration emacs shortcut
        "0, ., 1" : "lambda: api.putDurations(4*384, dots = 1)",
        "0, ., 2" : "lambda: api.putDurations(2*384, dots = 1)",
        "0, ., 3" : "lambda: api.putDurations(384, dots = 1)",
        "0, ., 4" : "lambda: api.putDurations(192, dots = 1)",
        "0, ., 5" : "lambda: api.putDurations(96, dots = 1)",
        "0, ., 6" : "lambda: api.putDurations(48, dots = 1)",
        "0, ., 7" : "lambda: api.putDurations(8*384, dots = 1)",
        "0, ., 8" : "lambda: api.putDurations(16*384, dots = 1)",

        "0, 1" : "lambda: api.putDurations(4*384)",
        "0, 2" : "lambda: api.putDurations(2*384)",
        "0, 3" : "lambda: api.putDurations(384)",
        "0, 4" : "lambda: api.putDurations(192)",
        "0, 5" : "lambda: api.putDurations(96)",
        "0, 6" : "lambda: api.putDurations(48)",
        "0. 7" : "lambda: api.putDurations(8*384)",
        "0, 8" : "lambda: api.putDurations(16*384)",
        }

virtualPiano = {
    "c" : "lambda: main.virtualPianoInsertNote(20)",
    "d" : "lambda: main.virtualPianoInsertNote(70)",
    "e" : "lambda: main.virtualPianoInsertNote(120)",
    "f" : "lambda: main.virtualPianoInsertNote(170)",
    "g" : "lambda: main.virtualPianoInsertNote(220)",
    "a" : "lambda: main.virtualPianoInsertNote(270)",
    "b" : "lambda: main.virtualPianoInsertNote(320)",
    "h" : "lambda: main.virtualPianoInsertNote(320)",

    #We need the shift versions because they block the global shift+char shortcuts. Even if we didn't need the function call itself.
    "shift+c" : "lambda: main.virtualPianoInsertChord(20)",
    "shift+d" : "lambda: main.virtualPianoInsertChord(70)",
    "shift+e" : "lambda: main.virtualPianoInsertChord(120)",
    "shift+f" : "lambda: main.virtualPianoInsertChord(170)",
    "shift+g" : "lambda: main.virtualPianoInsertChord(220)",
    "shift+a" : "lambda: main.virtualPianoInsertChord(270)",
    "shift+b" : "lambda: main.virtualPianoInsertChord(320)",
    "shift+h" : "lambda: main.virtualPianoInsertChord(320)",
}

#Key = string, value = dict of keynumber and a tuple with information for the button: function, label, toolTip, whatIsThis
#Achtung! The palettes have shortcuts. It is not checked if a shorcut is duplicated for the palette versus normale shortcuts. The result if two shorctus are assigned could be random.

palettes = {
    "dynamics": {
        "title": "Dynamics",
        "shortcut" : "Alt+D",
        0:("actionTacet", "tacet", _("Tacet (Silence)"), ""),
        1:("actionPianissimo", "𝆏𝆏", _("Pianissimo"), ""),
        2:("actionPiano", "𝆏", _("Piano"), ""),
        3:("actionMezzo_piano", "𝆐𝆏", _("Mezzo Piano"), ""),
        4:("actionMezzo_forte", "𝆐𝆑", _("Mezzo Forte"), ""),
        5:("actionForte", "𝆑", _("Forte"), ""),
        6:("actionFortissimo", "𝆑𝆑", _("Fortissimo"), ""),
        7:("actionCrescendo", "𝆒", _("Crescendo "), ""),
        8:("actionEnd_cresc_decresc", "|", _("Stop Cresc/Decresc"), ""),
        9:("actionDecrescendo", "𝆓", _("Decrescendo"), ""),
        },

    "barlines": {
        "title": "Barlines",
        "shortcut" : "Alt+B",
        1:("actionBarOpen", "𝄃", _("Open"), ""),
        2:("actionEnd_Open", "𝄂𝄃", _("End-Open"), ""),
        3:("actionEnd", "𝄂", _("End"), ""),
        4:("actionRepeat_Open", "𝄆", _("Repeat Open"), ""),
        5:("actionRepeat_Close_Open", "𝄇𝄆", _("Repeat Close-Open"), ""),
        6:("actionRepeat_Close", "𝄇", _("Repeat Close"), ""),
        7:("actionDouble", "𝄁", _("Double "), ""),
        8:("actionHalf", "𝄅", _("Half"), ""),
        9:("actionDashed", "𝄄", _("Dashed"), ""),
        },

    "fingerings": {
        "title": "Fingerings",
        "shortcut" : "Alt+F",
        0:("actionFinger_0", "0", _("Thumb"), ""),
        1:("actionFinger_1", "1", _("Finger 1"), ""),
        2:("actionFinger_2", "2", _("Finger 2"), ""),
        3:("actionFinger_3", "3", _("Finger 3"), ""),
        4:("actionFinger_4", "4", _("Finger 4"), ""),
        5:("actionFinger_5", "5", _("Finger 5"), ""),
        7:("actionCustom_Finger", "✎", _("Custom Finger "), ""),
        9:("actionClear_All_Fingerings", "✗", _("Remove Fingering"), ""),
        },

    "right hand fingering": {
        "title": "Stroke Finger",
        1:("actionStroke_1", "p", _("Stroke Finger p/1"), ""),
        2:("actionStroke_2", "i", _("Stroke Finger i/2"), ""),
        3:("actionStroke_3", "m", _("Stroke Finger m/3"), ""),
        4:("actionStroke_4", "a", _("Stroke Finger a/4"), ""),
        5:("actionStroke_x", "x", _("Stroke Finger x/5"), ""),
        7:("actionCustom_Right_Hand_Stroke_Finger", "✎", _("Custom Finger "), ""),
        9:("actionClear_All_Fingerings", "✗", _("Remove Fingering"), ""),
        },

    "string numbers": {
        "title": "String Nums",
        1:("actionString_1", "①", _("String 1"), ""),
        2:("actionString_2", "②", _("String 2"), ""),
        3:("actionString_3", "③", _("String 3"), ""),
        4:("actionString_4", "④", _("String 4"), ""),
        5:("actionString_5", "⑤", _("String 5"), ""),
        6:("actionString_6", "⑥", _("String 6"), ""),
        7:("actionCustom_String_Number", "|", _("Custom String Number"), ""),
        9:("actionClear_All_Fingerings", "✗", _("Remove Fingering"), ""),
        },

    "voice presets": {
        "title": "Voice Presets",
        0:("actionVoiceAutomatic", "Auto", _("Automatic voice from here"), ""),
        1:("actionVoiceOne", "v1", _("Voice one, stems up, from here"), ""),
        2:("actionVoiceTwo", "v2", _("Voice one, stems down, from here"), ""),
        3:("actionVoiceThree", "v3", _("Voice three, stems up, from here"), ""),
        4:("actionVoiceFour", "v4", _("Voice four, stems down, from here"), ""),
        },

    "midi fine tuning": {
        "title": "Fine Tuning",
        "shortcut" : "Alt+R",
        4:("actionChord_Duration_Factor_Minus", "↤", _("Less Duration (shorter)"), ""),
        5:("actionChord_Duration_Factor_Reset", "✗", _("Reset Duration Finetuning"), ""),
        6:("actionChord_Duration_Factor_Plus", "↦", _("More Duration (longer)"), ""),
        7:("actionChord_Velocity_Factor_Minus", "↧", _("Less Velocity (softer) "), ""),
        8:("actionChord_Velocity_Factor_Reset", "✗", _("Reset Velocity Finetuning"), ""),
        9:("actionChord_Velocity_Factor_Plus", "↥", _("More Velocity (louder)"), ""),
        },


    "channels and programs": {
        "title": "Chan + Prog",
        0:("actionChannel_Clear", "Ch clear", _("Clear Channel Offset"), ""),
        1:("actionChannel_Minus", "Ch-", _("Channel Offset +1"), ""),
        2:("actionChannel_Force", "Ch?", _("Force Channel"), ""),
        3:("actionChannel_Plus", "Ch+", _("Channel Offset -1"), ""),
        8:("actionProgram_Clear", "PrX", _("Clear Program Offset"), ""),
        4:("actionProgram_Minus", "Pr-", _("Program Offset +1"), ""),
        5:("actionProgram_Force", "Pr?", _("Force Program"), ""),
        6:("actionProgram_Plus", "Pr+", _("Program Offset -1"), ""),

        },

    "playback effects": {
        "title": "Effects",
        0:("actionClean_Tremolo", "Clear", _("Clear Tremolo"), ""),
        1:("actionTremolo_in_Two", "Trm2", _("Tremolo in two"), ""),
        2:("actionCustom_Tremolo", "Trm?", _("Custom Tremolo"), ""),
        3:("actionTremolo_in_Four", "Trm4", _("Tremolo in four"), ""),
        },
}

midiCCShortcuts = {
    #Scope is autodetected. main. or laborejoqt directliy (so you can use main functions or api.foo())
    #All these functions need to accept two parameters: the second midi byte (e.g. velocity) and the same byte but right before this function was called. So you can compare the new value to the old value
    10 : "numpadNextPreviousMidiController",  # in main. to control numpadNext and numpadPrevious
}

#Add an action to the toolbar
toolbar = {
        #"titleString":"actionName",
}

#Load a personal config file which can overide the default config.
def reloadPersonalConfig():
    """It is important to execute this function before any other gui
    functions like generating toolbars or user menus."""
    try:
        f = open(api._getConfigDir() + "laborejo-qt.config")
        extras = compile(f.read(), '<string>', 'exec')
        f.close()
        exec(extras, locals(), globals()) #override the existing variables.
    except:
        pass #personal config file does not exist


    #Make all keys lowercase except for the numpad keys
    #new = dict((k.lower(), v) for k,v in globals()["shortcuts"].items() if not "KeypadModifier" in k)
    #shortcuts.update(new)
    #globals()["shortcuts"] = dict((k.lower(), v) for k,v in globals()["shortcuts"].items())
    #globals()["virtualPiano"] = dict((k.lower(), v) for k,v in globals()["virtualPiano"].items())

    #Set initial playback feedback based on listener status
    api.l_playback.enabled = globals()["AUDIO_FEEDBACK"]
