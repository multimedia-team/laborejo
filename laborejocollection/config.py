# -*- coding: utf-8 -*-
from os import path
import laborejocore as api

lilypondbinary = "" #use the default
pdfviewer = "" #use the default

#Load a personal config file which can overide the default config.
def reloadPersonalConfig():
    """It is important to execute this function before any other gui
    functions like generating toolbars or user menus."""
    try:
        f = open(api._getConfigDir() + "laborejo-qt.config")
        for line in f:
            if "lilypondbinary" in line or "pdfviewer" in line:
                extras = compile(line, '<string>', 'exec')
                exec(extras, locals(), globals()) #override the existing variables.
        f.close()
    except:
        pass #personal config file does not exist

