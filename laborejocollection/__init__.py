# -*- coding: utf-8 -*-

import os, sys, warnings
localepath = os.path.join(os.path.dirname(__file__), 'locale')

from PyQt4 import QtCore, QtGui
def customTranslate(one, theString, three, four):
    #self.actionForte.setText(QtGui.QApplication.translate("MainWindow", "𝆑 forte", None, QtGui.QApplication.UnicodeUTF8))
    x =  __translateOrg(one, theString, three, four)
    return x.strip("\x00")  #there are \x00 in the strings which need to removed before display. This is most likely a pyqt/Python3 bug

__translateOrg = QtGui.QApplication.translate
QtGui.QApplication.translate = customTranslate #replace Qt function with our own "translate".

#import laborejocore as api #This starts the backend and automatically creates a session. But not a score/score/tracks.
#Start the Gui
from laborejocollection.designer.gui_collectionEditor import Ui_MainWindow
from laborejocollection.designer.gui_tabscrollcontent import Ui_TabContent

app = QtGui.QApplication(sys.argv)

import laborejocore as api #This starts the backend and automatically creates a session. But not score/tracks. If there is already an instance of Laborejo open it doesn't matter. This is an enitrely different system and namespace.
from laborejocollection import listener, config


class StartQT4(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        config.reloadPersonalConfig()  #override global qt-config data with user data.
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.settings = QtCore.QSettings("laborejo", "laborejo-collection-editor")
        if self.settings.contains("geometry"):
             self.restoreGeometry(self.settings.value("geometry"))

        #Last Known Dirs. Remember for convenience
        self.last_open_dir = HOME
        self.last_save_dir = HOME
        self.last_export_dir = HOME
        self.last_import_dir = HOME
        self.last_export_files = {"pdf":[], "ly":[], "jack":[], "smf":[], "bin":[], }

        self.clearSaveStatus = {} #key is backend Collection, value is True for clean save status or False for unsaved Data
        self.recentFilesMenuActions = [] #temp storage. Only menu actions.
        self.allActions = []

        self.ui.tabWidget.clear()
        self.ui.tabWidget.tabCloseRequested.connect(self.closeTabWarning)

        #Connect all menu actions to functions
        actions = {
            self.ui.actionNew : api.newCollection,
            self.ui.actionOpen : self.loadGuiDialog,
            self.ui.actionSave : lambda: self._saveProto(saveAs = False),
            self.ui.actionSave_As : lambda: self._saveProto(saveAs = True),
            self.ui.actionClose_Tab : self.closeTab,
            self.ui.actionQuit : self.close,

            self.ui.actionNew_Part : lambda: self.currentTabWidget().createEmptyPart() ,
            self.ui.actionMove_Part_up : lambda: self.currentTabWidget().up() ,
            self.ui.actionMove_Part_down : lambda: self.currentTabWidget().down() ,
            self.ui.actionNew_Score_Movement : lambda: self.currentTabWidget().createEmptyScore() ,
            self.ui.actionEdit : lambda: self.currentTabWidget().edit(),
            self.ui.actionDelete : lambda: self.currentTabWidget().delete(),
        }

        for action, function in actions.items():
            self.allActions.append(action)
            QtCore.QObject.connect(action, QtCore.SIGNAL("triggered()"), function)

        #Since we start with no files open we need to disable all file related menu functions like export.
        self.switchActionsOff()
        self.generateRecentFilesMenu()

        #Start
        self.show()

    def currentTabWidget(self):
        """Return the current gui collection.
        There is no current backendCollection. They are all just in
        memory and can be accessed directly."""
        return self.ui.tabWidget.currentWidget()

    def currentBackendCollection(self):
        return self.currentTabWidget().backendCollection

    #Save, Load, Export
    #####
    def loadBackend(self, backendCollection):
        """This is new and load. We just get a backend collection"""
        editor = EditTab(backendCollection)
        self.fileName = backendCollection.lastKnownFilename if backendCollection.lastKnownFilename else "unnamed"  #this is only the tab name, not the file name. The internal filename is still "" so save() knows what to do.
        tabIndex = self.ui.tabWidget.addTab(editor, self.fileName)
        self.ui.tabWidget.setCurrentIndex(tabIndex)
        self.clearSaveStatus[backendCollection] = True #initial status is the saved, clean one
        self.switchActionsOn() #We have at least one file now, everything is allowed.

        if not backendCollection.lastKnownFilename:
            editor.createEmptyScore(name = "Score 1")
            self.clearSaveStatus[backendCollection] = True
        editor.ui.items.setFocus(True)

    def loadGuiDialog(self, directFileList = None):
        """Present the gui load dialog,
        ask the api to open the filepath"""
        if directFileList:
            files = directFileList
        else:
            files = QtGui.QFileDialog.getOpenFileNames(self, "Open File(s)", self.last_open_dir, "Laborejo Collection Files (*.lbj);;All Files (*.*)")

        for fileName in files:
            fileName = os.path.abspath(fileName)
            #first check if this file is already open in this GUI.
            if self.isFilenameAvailable(fileName):
                backendCollection = api.loadCollection(fileName) #this returns False if the user chose a non-laborejo file. Only use for testing or emergency.
                if backendCollection:
                    self.addRecentFiles(fileName)
                    self.last_open_dir = os.path.dirname(fileName)
            else:
                warnings.warn("File " +  fileName +  "is already open")

    def _saveProto(self, saveAs):
        """The gui save dialog to get a filepath or direct save.
        Save is always for the current active workspace/score

        Savefunction gets two parameters, the backend collection and a filename-"""
        collection = self.currentBackendCollection()
        if saveAs or not collection.lastKnownFilename:
            fileName = QtGui.QFileDialog.getSaveFileName(self, "Save Laborejo Collection file.", self.last_save_dir , "Laborejo Collection (*.lbj)")
            if fileName: #Not pressed cancel?
                if not os.path.splitext(fileName)[1]: #no file extension given?
                    fileName = fileName + ".lbj"
                collection.lastKnownFilename = fileName
            else: #User pressed cancel
                return False
        if collection.lastKnownFilename:
            fileName = collection.lastKnownFilename
            api.saveCollection(fileName, collection)
            self.addRecentFiles(fileName)
            self.last_save_dir = os.path.dirname(fileName)
            self.ui.tabWidget.setTabText(self.ui.tabWidget.currentIndex(), fileName)

            tab = self.currentTabWidget()
            ly = tab.ui.templat.currentText()
            ard = tab.ui.ardourTemplate.currentText()

            tab.ui.templat.clear()
            tab.ui.ardourTemplate.clear()
            tab.backendCollection.ardourTemplate = ard
            tab.backendCollection.lytemplate = ly

            tab.ui.templat.addItems(list(api.getTemplateList(tab.backendCollection, extension = "ly"))) #not a typo, it really is "templat"
            tab.ui.templat.setCurrentIndex(tab.ui.templat.findText(ly))

            tab.ui.ardourTemplate.addItems(list(api.getTemplateList(tab.backendCollection, extension = "ardour")))
            tab.ui.ardourTemplate.setCurrentIndex(tab.ui.ardourTemplate.findText(ard))

            self.clearSaveStatus[collection] = True

            return collection.lastKnownFilename
        else:
            raise RuntimeError("not possible")

    #Simple/Quick Exports. No parts.

    def switchActionsOn(self):
        """Switch menu actions related to an open file on"""
        for a in self.allActions:
            a.setEnabled(True)

    def switchActionsOff(self):
        """Switch menu actions related to an open file off.
        Program starts in this state"""
        for a in self.allActions:
            a.setEnabled(False)
        allowed = [self.ui.actionOpen, self.ui.actionNew, self.ui.actionQuit, ]
        for a in allowed:
            a.setEnabled(True)

    def closeTab(self):
        """Close the tab through the menu or by a shortcut.
        But not by clicking with the mouse on an [x]"""
        idx = self.ui.tabWidget.currentIndex()
        self.closeTabWarning(idx)

    def closeTabWarning(self, tabIndex):
        """Check if there is unsaved data in a score before closing
        the tab."""
        t = self.ui.tabWidget
        collection = self.currentBackendCollection()
        t.setCurrentIndex(tabIndex)

        if not tabIndex == t.currentIndex():
            raise IndexError("Tab to close is not current tab eventhough it was changed beforehand")

        if self.clearSaveStatus[collection]:
            del self.clearSaveStatus[collection]
            api.closeCollection(collection)
            t.removeTab(tabIndex)
        else:
            tabname = t.tabText(tabIndex)
            reply = QtGui.QMessageBox.question(self, 'Unsaved Data', "Unsaved Data in " + tabname + ".\nHow to proceed?", QtGui.QMessageBox.Save | QtGui.QMessageBox.Discard | QtGui.QMessageBox.Cancel , QtGui.QMessageBox.Cancel)
            if reply == QtGui.QMessageBox.Discard:
                del self.clearSaveStatus[collection]
                api.closeCollection(collection)
                t.removeTab(tabIndex)
            elif reply == QtGui.QMessageBox.Save:
                if self._saveProto(saveAs = False): #This calls the gui dialog if there is no filename. Could be canceled as well.
                    del self.clearSaveStatus[collection]
                    api.closeCollection(collection)
                    t.removeTab(tabIndex)
                else: #If canceled just safely return to the unsaved data.
                    pass
            else: #Cancel
                pass
        if not self.clearSaveStatus: #it is empty? No files left open?
            self.switchActionsOff()

    def closeEvent(self, event): #does not need to be assigned or connected. This overwrites a default qt method.
        """Check if there is any unsaved data in the session before
        closing the program"""
        #Save the window geometry, even if the program is not closed because of unsaved files, it does not hurt to save it now.
        self.settings.setValue("geometry", self.saveGeometry())
        if not self.allSaveStatusClean(): #There is dirty data.
            #Offer to cancel, Quit Anyway or SaveAll and then Quit.
            reply = QtGui.QMessageBox.question(self, 'Unsaved Data', "One or more files are unsaved. How to proceed?\nWarning: Unnamed Files will be discarded.", QtGui.QMessageBox.SaveAll | QtGui.QMessageBox.Discard | QtGui.QMessageBox.Cancel , QtGui.QMessageBox.Cancel)

            if reply == QtGui.QMessageBox.Discard:
                event.accept() #Just quit
            elif reply == QtGui.QMessageBox.SaveAll:
                api.saveAllCollection() #this never fails. It has no user interaction. No test necessarry.
                event.accept()
            else: #Cancel
                event.ignore()
        else: #Everything is clean and saved. Good Bye.
            event.accept()

    def editTabIndex(self, backendCollection):
        """Return the tab which has a certain backend collection"""
        for editTabIndex in range(self.ui.tabWidget.count()): #number of tabs
           if backendCollection is self.ui.tabWidget.widget(editTabIndex).backendCollection:
               return editTabIndex

    def editTab(self, backendCollection):
        """This normally should result in the currently active
        GUI tab. But this is the direct, certain way"""
        return self.ui.tabWidget.widget(self.editTabIndex(backendCollection))

    def isFilenameAvailable(self, newFileName):
        if newFileName in [col.lastKnownFilename for col in api._getSession().collections]:
            return False
        else:
            return True

    def allSaveStatusClean(self):
        return all([value for key, value in self.clearSaveStatus.items()]) #check if all values are True, which means all data is clean and saved.

    def addRecentFiles(self, fileName):
        """Add one file to the recent save file. If there are too much
        in that file delete the oldest entry"""
        recentList = self.settings.value("recentFilesList")
        if not recentList:
            recentList = []

        if fileName in recentList:
            return False
        else:
            recentList.append(fileName)
            if len(recentList) >6:
                recentList.pop(0)

            self.settings.setValue("recentFilesList", recentList)
            self.generateRecentFilesMenu()

    def generateRecentFilesMenu(self):
        """Generate the recent files menu.
        This is the step after self.addRecentFiles.
        This always addes the complete recent files file to the menu."""

        #First remove the old files menu actions
        for menuAction in self.recentFilesMenuActions:
            self.ui.menuRecent.removeAction(menuAction)
        self.recentFilesMenuActions = []

        #Create a new recent files menu from the save file.
        nameList = self.settings.value("recentFilesList")
        if not nameList:
            nameList = []
        nameList.reverse()
        for fString in nameList:
            fString = fString.rstrip()
            menuAction = self.ui.menuRecent.addAction(fString)
            self.recentFilesMenuActions.append(menuAction)
            menuAction.triggered.connect(lambda a, fString = fString: self.loadGuiDialog(directFileList = [fString]))

def nothing(*args):
    pass

class EditTab(QtGui.QScrollArea):
    """The ui part of a GuiScore"""
    def __init__(self, backendCollection):
        super(EditTab, self).__init__()
        self.setParent(main.ui.tabWidget)
        self.ui = Ui_TabContent()  #self.ui is the node to all qt widgets in this tab.
        self.ui.setupUi(self)
        self.main = main #a shortcut so that tracks and other items can access the main menu if they need.
        self.backendCollection = backendCollection
        self.ui.items.setSelectionMode(0) # 3 ExtendedSelection
        self.ui.items.itemChanged.connect(self.itemWasEdited) #gets one parameter, the item.

        self.ui.warningLabel.hide()
        self.ui.warningLabel_Lower.hide()

        #Set initial tab status and connect format radio buttons to switch the tab.
        #indices: 0-lilypond, 1-midi, 2-other
        self.ui.pdf.clicked.connect(lambda: self.ui.settingsTab.setCurrentIndex(0))
        self.ui.lilypond.clicked.connect(lambda: self.ui.settingsTab.setCurrentIndex(0))
        self.ui.lilybin.clicked.connect(lambda: self.ui.settingsTab.setCurrentIndex(0))
        self.ui.midijack.clicked.connect(lambda: self.ui.settingsTab.setCurrentIndex(1))
        self.ui.midisimple.clicked.connect(lambda: self.ui.settingsTab.setCurrentIndex(1))


        #Auto-Update the filepath extension when changing the format
        self.ui.pdf.clicked.connect(self.correctCurrentPathExtension)
        self.ui.lilypond.clicked.connect(self.correctCurrentPathExtension)
        self.ui.lilybin.clicked.connect(self.correctCurrentPathExtension)
        self.ui.midijack.clicked.connect(self.correctCurrentPathExtension)
        self.ui.midisimple.clicked.connect(self.correctCurrentPathExtension)

        #Parts Radio Button:
        self.ui.pdf.clicked.connect(self.populatePathComboBox)
        self.ui.lilypond.clicked.connect(self.populatePathComboBox)
        self.ui.lilybin.clicked.connect(self.populatePathComboBox)
        self.ui.midijack.clicked.connect(self.populatePathComboBox)
        self.ui.midisimple.clicked.connect(self.populatePathComboBox)

        #Path
        self.ui.lilybin.toggled.connect(lambda: self.ui.path.setEnabled(not self.ui.lilybin.isChecked()))
        self.ui.lilybin.toggled.connect(lambda: self.ui.pathOpen.setEnabled(not self.ui.lilybin.isChecked()))

        self.ui.path.setDuplicatesEnabled(False)

        self.ui.pathOpen.clicked.connect(self.openPathButton) #Open button

        #Metadata
        self.metaFields = {
            self.ui.copyright : "copyright",
            self.ui.title : "title",
            self.ui.subtitle : "subtitle",
            self.ui.subsubtitle : "subsubtitle",
            self.ui.instruments : "instrument",
            self.ui.tagline : "tagline",
            self.ui.meter : "meter",
            self.ui.opus : "opus",
            self.ui.dedication : "dedication",
            self.ui.composer : "composer",
            self.ui.poet : "poet",
            self.ui.arranger : "arranger",
        }

        #Populate the fields
        self.buildList()
        self.buildMetadata()
        self.ui.fontsize.setValue(self.backendCollection.lyFontSize)
        self.ui.numbering.addItems(["", "piece"] + sorted(self.metaFields.values()))
        self.ui.numbering.setCurrentIndex(self.ui.numbering.findText(self.backendCollection.numbering))
        self.ui.startnumber.setValue(self.backendCollection.startnumber)
        self.ui.templat.addItems(list(api.getTemplateList(self.backendCollection, extension = "ly"))) #not a typo, it really is "templat"
        self.ui.templat.setCurrentIndex(self.ui.templat.findText(self.backendCollection.lytemplate))
        self.ui.ardourTemplate.addItems(list(api.getTemplateList(self.backendCollection, extension = "ardour")))
        self.ui.ardourTemplate.setCurrentIndex(self.ui.ardourTemplate.findText(self.backendCollection.ardourtemplate))
        self.ui.printallheader.setCurrentIndex(self.backendCollection.printallheader) #printallheader combobox indices: 0-Template default 1-Yes 2-No
        self.ui.smfTransposition.setValue(self.backendCollection.smfTransposition)
        self.ui.jackTransposition.setValue(self.backendCollection.jackTransposition)
        self.ui.splitByToHeader.setChecked(self.backendCollection.splitByToHeader)

        self.ui.transposeFrom.addItems(api.lilypond.sortedNoteNameList)
        self.ui.transposeTo.addItems(api.lilypond.sortedNoteNameList)
        self.ui.transposeFrom.view().setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum) #make sure the list is visible, always.
        self.ui.transposeTo.view().setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)#make sure the list is visible, always.
        self.ui.transposeFrom.setCurrentIndex(api.lilypond.sortedNoteNameList.index(api.lilypond.pitch2ly[self.backendCollection.transposition[0]]))
        self.ui.transposeTo.setCurrentIndex(api.lilypond.sortedNoteNameList.index(api.lilypond.pitch2ly[self.backendCollection.transposition[1]]))


        #Connect the buttons
        self.ui.up.clicked.connect(self.up)
        self.ui.down.clicked.connect(self.down)
        self.ui.addpart.clicked.connect(self.createEmptyPart)
        self.ui.newscore.clicked.connect(lambda: self.createEmptyScore())
        self.ui.delete_2.clicked.connect(self.delete)
        self.ui.edit.clicked.connect(self.edit)

        #Connect the metadata
        for field, backendKey in self.metaFields.items():
            field.textEdited.connect(lambda field=field, backendKey=backendKey: self.backendCollection.header.data.__setitem__(backendKey, field))

        #Connect fontsize, template and other value edits.
        self.ui.fontsize.valueChanged.connect(lambda: setattr(self.backendCollection, "lyFontSize", self.ui.fontsize.value()))
        self.ui.fontsize.valueChanged.connect(lambda: main.clearSaveStatus.__setitem__(self.backendCollection, False))

        self.ui.templat.currentIndexChanged.connect(lambda: setattr(self.backendCollection, "lytemplate", self.ui.templat.currentText()))
        self.ui.templat.currentIndexChanged.connect(lambda: main.clearSaveStatus.__setitem__(self.backendCollection, False))

        self.ui.ardourTemplate.currentIndexChanged.connect(lambda: setattr(self.backendCollection, "ardourtemplate", self.ui.ardourTemplate.currentText()))
        self.ui.ardourTemplate.currentIndexChanged.connect(lambda: main.clearSaveStatus.__setitem__(self.backendCollection, False))

        self.ui.numbering.currentIndexChanged.connect(lambda: setattr(self.backendCollection, "numbering", self.ui.numbering.currentText()))
        self.ui.numbering.currentIndexChanged.connect(lambda: main.clearSaveStatus.__setitem__(self.backendCollection, False))

        self.ui.startnumber.valueChanged.connect(lambda: setattr(self.backendCollection, "startnumber", self.ui.startnumber.value()))
        self.ui.startnumber.valueChanged.connect(lambda: main.clearSaveStatus.__setitem__(self.backendCollection, False))

        self.ui.printallheader.currentIndexChanged.connect(lambda: setattr(self.backendCollection, "printallheader", self.ui.printallheader.currentIndex()))  #printallheader combobox indices: 0-Template default 1-Yes 2-No
        self.ui.printallheader.currentIndexChanged.connect(lambda: main.clearSaveStatus.__setitem__(self.backendCollection, False))

        self.ui.transposeFrom.currentIndexChanged.connect(lambda: setattr(self.backendCollection, "transposition", (api.lilypond.ly2pitch[api.lilypond.sortedNoteNameList[self.ui.transposeFrom.currentIndex()]], api.lilypond.ly2pitch[api.lilypond.sortedNoteNameList[self.ui.transposeTo.currentIndex()]])))
        self.ui.transposeFrom.currentIndexChanged.connect(lambda: main.clearSaveStatus.__setitem__(self.backendCollection, False))

        self.ui.transposeTo.currentIndexChanged.connect(lambda: setattr(self.backendCollection, "transposition", (api.lilypond.ly2pitch[api.lilypond.sortedNoteNameList[self.ui.transposeFrom.currentIndex()]], api.lilypond.ly2pitch[api.lilypond.sortedNoteNameList[self.ui.transposeTo.currentIndex()]])))
        self.ui.transposeTo.currentIndexChanged.connect(lambda: main.clearSaveStatus.__setitem__(self.backendCollection, False))

        self.ui.smfTransposition.valueChanged.connect(lambda: setattr(self.backendCollection, "smfTransposition", self.ui.smfTransposition.value()))
        self.ui.smfTransposition.valueChanged.connect(lambda: main.clearSaveStatus.__setitem__(self.backendCollection, False))

        self.ui.jackTransposition.valueChanged.connect(lambda: setattr(self.backendCollection, "jackTransposition", self.ui.jackTransposition.value()))
        self.ui.jackTransposition.valueChanged.connect(lambda: main.clearSaveStatus.__setitem__(self.backendCollection, False))

        self.ui.splitByToHeader.stateChanged.connect(lambda: setattr(self.backendCollection, "splitByToHeader", self.ui.splitByToHeader.checkState()))
        self.ui.splitByToHeader.stateChanged.connect(lambda: main.clearSaveStatus.__setitem__(self.backendCollection, False))

        #It is possible that you got an lbjs file without the template.
        warning = []
        self.lilypondTemplatesList = list(api.getTemplateList(workspace = self.backendCollection , extension = "ly"))
        self.ardourTemplatesList = list(api.getTemplateList(workspace = self.backendCollection, extension = "ardour"))

        if self.backendCollection.lytemplate in self.lilypondTemplatesList:
            self.ui.templat.setCurrentIndex(self.lilypondTemplatesList.index(self.backendCollection.lytemplate))
        else: #don't choose an index
            warning.append("Lilypond-Template: " + self.backendCollection.lytemplate)
        if self.backendCollection.ardourtemplate in self.ardourTemplatesList:
            self.ui.ardourTemplate.setCurrentIndex(self.ardourTemplatesList.index(self.backendCollection.ardourtemplate))
        else: #don't choose an index
            warning.append("Ardour Template: " + self.backendCollection.ardourtemplate)



        if warning:
            warning= _(" and ").join(warning)
            warning = _("Warning: ") + warning  + _(" could not be found! Choose a different one now or avoid to safe.")
            self.ui.warningLabel.setText("<span style='background-color:yellow'>" + warning + "</span>")
            self.ui.warningLabel.show()

        #Connect the export button and preview button
        self.ui.exportnow.clicked.connect(self.export)
        self.ui.previewButton.clicked.connect(self.preview)

        #The Preview button is only available when choosing PDF. Also hovering preview disables the path field.
        self.ui.pdf.toggled.connect(lambda: self.ui.previewButton.setEnabled(self.ui.pdf.isChecked()))
        self.ui.previewButton.setEnabled(self.ui.pdf.isChecked())

        def leavePreview(event):
            if self.ui.previewButton.isEnabled(): self.ui.path.setEnabled(True)
            event.accept()

        def enterPreview(event):
            if self.ui.previewButton.isEnabled(): self.ui.path.setEnabled(False)
            event.accept()

        self.ui.previewButton.leaveEvent = leavePreview
        self.ui.previewButton.enterEvent = enterPreview

        pdfviewer = config.pdfviewer
        #This also sets the right tab
        self.ui.pdf.click()
        self.ui.none.click()

    def up(self):
        item = self.ui.items.currentItem()
        parent = item.parent()
        if parent: #it is a child item.
            scoreIndex, partIndex = api.col_movePartUp(self.backendCollection, parent.data, item.data)
            newItemInstance = self.ui.items.topLevelItem(scoreIndex).child(partIndex)
            self.ui.items.setCurrentItem(newItemInstance) # this is not possible with the old item because it does not exist anymore.
        else: #it is a top level item, a backend score
            scoreIndex = api.col_moveScoreUp(self.backendCollection, item.data) #item data is the score itself.
            newItemInstance = self.ui.items.topLevelItem(scoreIndex)
            self.ui.items.setCurrentItem(newItemInstance) # this is not possible with the old item because it does not exist anymore.

    def down(self):
        item = self.ui.items.currentItem()
        parent = item.parent()
        if parent: #it is a child item.
            scoreIndex, partIndex = api.col_movePartDown(self.backendCollection, parent.data, item.data)
            newItemInstance = self.ui.items.topLevelItem(scoreIndex).child(partIndex)
            self.ui.items.setCurrentItem(newItemInstance) # this is not possible with the old item because it does not exist anymore.
        else: #it is a top level item, a backend score
            scoreIndex = api.col_moveScoreDown(self.backendCollection, item.data) #item data is the score itself.
            newItemInstance = self.ui.items.topLevelItem(scoreIndex)
            self.ui.items.setCurrentItem(newItemInstance) # this is not possible with the old item because it does not exist anymore.

    def createEmptyScore(self, name = ""):
        """append a new score"""
        #index = self.ui.items.currentIndex().row()
        #item = self.ui.items.currentItem()
        #parent = item.parent()
        #if parent: #We are at a parent position
        api.col_newScore(self.backendCollection, name)

    def newScore(self, name, backendData):
        """this is the button and as final step
        and also the menu action from Edit->actionNewScore"""
        item = QtGui.QTreeWidgetItem([name]) #it is a list because each column is set to a text. A simple text will be broken into chars.
        item.data = backendData
        self.ui.items.addTopLevelItem(item)
        self.ui.items.setItemExpanded(item, True)
        item.setFlags(QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsDragEnabled)
        self.ui.items.setCurrentItem(item)
        return item

    def createEmptyPart(self):
        """Create an empty part under the current item
        This is the button add part/file
        and as final step also the menu action from edit->addPart"""
        currentitem = self.ui.items.currentItem()

        #if not currentitem:
        #    QtGui.QMessageBox.information(self, "Create Score first", "Create a score section before adding parts.")
        #    return False

        parent = currentitem.parent()

        if parent: #we found a normal item (part)
            parentTreeItem = parent
            afterPartData = currentitem.data
        else: #we found a toplevel item (score)
            parentTreeItem = currentitem
            afterPartData = None

        if not self.backendCollection.lastKnownFilename: #if empty
            QtGui.QMessageBox.information(self, "Save before adding parts", "Save the file before adding parts.")
            return False #abort the loop. No harm done.

        for fileName in QtGui.QFileDialog.getOpenFileNames(self, "Open File(s)", main.last_import_dir, "Laborejo Score Files (*.lbjs);;All Files (*.*)", options = QtGui.QFileDialog.DontResolveSymlinks):
            scoreIndex, partIndex = api.col_addPart(self.backendCollection, parentTreeItem.data, afterPartData, newPartName = fileName)
            newItemInstance = self.ui.items.topLevelItem(scoreIndex).child(partIndex)
            self.ui.items.setCurrentItem(newItemInstance) # this is not possible with the old item because it does not exist anymore.
            afterPartData = newItemInstance.data
            main.last_import_dir = os.path.dirname(fileName)

    def addPart(self, parentTreeItem, relativePath):
        item = QtGui.QTreeWidgetItem([relativePath]) #it is a list because each column is set to a text. A simple text will be broken into chars.
        item.data = relativePath
        item.setFlags(QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsDragEnabled)
        parentTreeItem.addChild(item)
        return item

    def delete(self):
        """button to delete one part from the list"""
        item = self.ui.items.currentItem()
        if item:
            parent = item.parent()
            if parent: #it is a child item, a part
                scoreIndex, partIndex = api.col_delete(self.backendCollection, parent.data, item.data)
                newItemInstance = self.ui.items.topLevelItem(scoreIndex).child(partIndex)
                self.ui.items.setCurrentItem(newItemInstance) # this is not possible with the old item because it does not exist anymore.
            else: #it is a top level item, a backend score
                scoreIndex = api.col_delete(self.backendCollection, item.data, None) #item data is the score itself.
                if scoreIndex:
                    newItemInstance = self.ui.items.topLevelItem(scoreIndex)
                    self.ui.items.setCurrentItem(newItemInstance) # this is not possible with the old item because it does not exist anymore. buildList (listener) deletes all items.

    def edit(self):
        self.ui.items.editItem(self.ui.items.currentItem())

    def itemWasEdited(self, paramItem):
        parent = paramItem.parent()

        if parent: #it is a child item, a part
            partList = parent.data[1]
            partList[partList.index(paramItem.data)] = paramItem.text(0)
            parent.data[1] = partList
            self.ui.items.currentItem().data = paramItem.text(0)
        else: #it is a top level item, a backend score
            paramItem.data[0] = paramItem.text(0)

    def iterAllItems(self):
        for i in range(self.ui.items.count()):
            yield self.ui.items.item(i)

    def buildList(self):
        """Build the complete file list. No need for gui add/delete

        [["scoreNameString", ["list.lbjs", "of.lbjs", "parts.lbjs"]],
            ["second movement", ["singleFile.lbjs"]], ]
        """
        self.ui.items.clear() #first delete all old entries.
        for score in self.backendCollection.workspaceFileStrings:
            currentParentTreeItem = self.newScore(score[0], score)
            for part in score[1]: #list of parts.lbjs
                self.addPart(currentParentTreeItem, part)
        self.ui.items.setFocus(True)

    def buildMetadata(self):
        """Fill in all metadata fields like title and composer.
        THere is no performance tweak here with incremental updates,
        it is always delete all and create all from scratch."""
        for field, backendKey in self.metaFields.items():
            field.clear()
            field.setText(self.backendCollection.header.data[backendKey])


    def _getFunctionAndExtension(self):
        if self.ui.pdf.isChecked():
            function = lambda filepath, parts: api.exportPDF(filepath = filepath, workspace = self.backendCollection, parts=parts)
            extension = "pdf"
        elif self.ui.lilybin.isChecked():
            #function = api.exportLilyBin
            function = lambda filepath, parts: api.exportLilyBin(filepath = filepath, workspace = self.backendCollection, parts=parts)
            extension = "bin"
        elif self.ui.lilypond.isChecked():
            #function = api.exportLilypond
            function = lambda filepath, parts: api.exportLilypond(filepath = filepath, workspace = self.backendCollection, parts=parts)
            extension = "ly"
        elif self.ui.midijack.isChecked():
            function = lambda filepath, parts: api.exportMidi(filepath = filepath, workspace = self.backendCollection, jackMode = True, parts=parts)
            extension = "jack"
        elif self.ui.midisimple.isChecked():
            #function = api.exportMidi
            function = lambda filepath, parts: api.exportMidi(filepath = filepath, workspace = self.backendCollection, parts=parts)
            extension = "smf"
        else:
            raise ValueError("Not possible. One of the format radio buttons has to be clicked. Perhaps your Qt Version is different or broken.")
        return function, extension



    def export(self):
        """Export! button.
        Also saves paths, SplitBy and Format in the Gui session"""

        def startLyExportProcess(string):
            def reset():
                api.core.lilypond.callExternalCustom = api.core.lilypond._callExternalCustom
            #Override the lilypond export function in core so that exporting does not block the gui and we can route output to self.ui.output (QTextEdit)
            process = QtCore.QProcess(self)
            process.setProcessChannelMode(QtCore.QProcess.MergedChannels)
            process.readyReadStandardOutput.connect(lambda: self.write(str(process.readAllStandardOutput(), encoding='utf-8')))
            process.finished.connect(reset)
            process.start(string)
            process.waitForFinished()
            return process.exitCode()


        api.core.lilypond.callExternalCustom = startLyExportProcess

        self.ui.warningLabel_Lower.hide()
        #Format radio buttons
        function, extension = self._getFunctionAndExtension()

        #SplitBy radio buttons
        if self.ui.none.isChecked():
            splitby = None
        elif self.ui.exportparts.isChecked():
            splitby = "exportExtractPart"
        elif self.ui.group.isChecked():
            splitby = "group"
        elif self.ui.track.isChecked():
            splitby = "_uniqueContainerName"
        else:
            raise ValueError("Not possible. One of the Split By radio buttons has to be clicked. Perhaps your Qt Version is different or broken.")

        filepath = self.ui.path.currentText()

        self.ui.output.clear()
        self.updatePathComboBoxSessionDatabase()
        if extension == "bin" or filepath:
            self.sysOrg = sys.stdout
            sys.stdout = self
            function(filepath=filepath, parts=splitby)
            print () #a new line to make the console output nicer
            sys.stdout = self.sysOrg
        else:
            warning = _("You must provide a filepath!")
            self.ui.warningLabel_Lower.setText("<span style='background-color:orange'>" + warning + "</span>")
            self.ui.warningLabel_Lower.show()
            return False

    def preview(self):
        """Export! button.
        Also saves paths, SplitBy and Format in the Gui session"""

        def startLyExportProcess(string):
            def reset():
                api.core.lilypond.callExternalCustom = api.core.lilypond._callExternalCustom
            #Override the lilypond export function in core so that exporting does not block the gui and we can route output to self.ui.output (QTextEdit)
            process = QtCore.QProcess(self)
            process.setProcessChannelMode(QtCore.QProcess.MergedChannels)
            process.readyReadStandardOutput.connect(lambda: self.write(str(process.readAllStandardOutput(), encoding='utf-8')))
            process.finished.connect(reset)
            process.start(string)
            process.waitForFinished()
            return process.exitCode()

        api.core.lilypond.callExternalCustom = startLyExportProcess

        self.ui.warningLabel_Lower.hide()

        #SplitBy radio buttons
        if self.ui.none.isChecked():
            splitby = None
        elif self.ui.exportparts.isChecked():
            splitby = "exportExtractPart"
        elif self.ui.group.isChecked():
            splitby = "group"
        elif self.ui.track.isChecked():
            splitby = "_uniqueContainerName"
        else:
            raise ValueError("Not possible. One of the Split By radio buttons has to be clicked. Perhaps your Qt Version is different or broken.")

        self.ui.output.clear()
        self.sysOrg = sys.stdout
        sys.stdout = self
        api.previewPDF(self.backendCollection, pdfviewer = config.pdfviewer, lilypondbinary = config.lilypondbinary, parts = splitby)
        print () #a new line to make the console output nicer
        sys.stdout = self.sysOrg

    def write(self, txt):
        """Emulates sys.stdout and sys.stderr behaviour.
        Write the stdout and stderror to the output text edit"""
        self.ui.output.insertPlainText(txt)
        #self.sysOrg.write(txt)

    def openPathButton(self):
        doesntmatter, extension = self._getFunctionAndExtension()
        realFileExtensions = {"pdf":("pdf", "Portable Document Format(PDF) (*.pdf)"), "bin":("bin", "LilyBin.com "), "ly":("ly", "Lilypond Text (*.ly)"), "jack":("mid", "Midi (*.mid)"),  "smf":("mid", "Midi (*.mid)")}

        fileName = QtGui.QFileDialog.getSaveFileName(self, _("Export file name. Split Export will derive their own filen names."), main.last_export_dir, realFileExtensions[extension][1]+";;All Files (*.*)")
        if fileName:
            main.last_export_dir = os.path.dirname(fileName)
            self.ui.path.setEditText(fileName)
            self.correctCurrentPathExtension()

    def correctCurrentPathExtension(self):
        """Corrects the current path according to the chosen radio
        button, in place."""
        current = self.ui.path.currentText()
        if current:
            realFileExtensions = {"pdf":"pdf", "bin":"bin", "ly":"ly", "jack":"mid", "smf":"mid"}
            doesntmatter, extension = self._getFunctionAndExtension()
            withoutextension = os.path.splitext(current)[0]
            self.ui.path.setEditText(withoutextension+"."+realFileExtensions[extension])

    def populatePathComboBox(self):
        #Populate the export fields
        #main.session.last_export_files = {"pdf":()[], "ly":[], "jack":[], "smf":[], "bin":[]}
        function, extension = self._getFunctionAndExtension()
        current = self.ui.path.currentText()
        self.ui.path.clear()
        if main.last_export_files[extension] and  current == main.last_export_files[extension][0]:
            self.ui.path.addItems(main.last_export_files[extension])
        else:
            self.ui.path.addItems([current] + main.last_export_files[extension])

    def updatePathComboBoxSessionDatabase(self):
        function, extension = self._getFunctionAndExtension()
        if self.ui.path.currentText() in main.last_export_files[extension] or not self.ui.path.currentText():
            return False #abort. Don't create duplicate entries.
        main.last_export_files[extension].append(self.ui.path.currentText())


HOME = os.path.expanduser("~") #lin and win
