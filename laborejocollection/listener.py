main = None #is overwritten during program start (./laborejo-collection-editor) with the gui main window instance.

class Listener(object):
    def __init__(self):
        pass

class ListenerCollections(Listener):
    def __init__(self):
        super(ListenerCollections, self).__init__()

    def load(self, loadedCollection):
        main.loadBackend(loadedCollection)

    def buildList(self, loadedCollection):
        """Rebuild the fileList in the QListWidget"""
        main.editTab(loadedCollection).buildList()
        main.clearSaveStatus[loadedCollection] = False

