# -*- coding: utf-8 -*-

#No external dependencies here. These are just plain python functions,
#ready to use in any program.

def num2word(n):
    """Take a number, return a str.
    Lilypond does not allow numbers in variable names"""
    return ''.join(chr(ord(c)+17) for c in str(n))


def num2wordForIterations(stringOrNum):
    if stringOrNum in [1,2,3,4,5,6,7,8,9,0] or stringOrNum in "1234567890":
        return num2word(int(stringOrNum))
    elif stringOrNum in ("-", "_"):
        return ""
    else:
        return stringOrNum

def stringToCharsOnlyString(string):
    return "".join([num2wordForIterations(c) for c in string])

def toLyString(string):
    if value.strip().startswith("\\markup"):
        return string
    else:
        return '"' + '"'

def items2types(lst):
    """Take a flat list of items and return a list of types instead"""
    return [type(it) for it in lst]

def flatten(lst):
    """Take a nested list of any depth and make it a flat, plain
    list with only one level"""
    l = []
    for elem in lst:
        if type(elem) in (tuple, list):
            for i in flatten(elem):
                l.append(i)
        else:
            l.append(elem)
    return l
