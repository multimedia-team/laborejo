# -*- coding: utf-8 -*-
import collections
from laborejocore import pitch
from laborejocore.items import *

class NestedListIter(object):
    '''A mutable container that enables flat traversal of a nested tree of
    lists. nested_list should contain only a list-like mutable sequence.
    To preserve a clear demarcation between 'leaves' and 'branches', empty
    sequences are not allowed as toplevel objects.'''
    def __init__(self, nested_list):
        if not nested_list:
            raise ValueError ('nested_list must be a non-empty sequence')
        self.nested_list = nested_list # at some point, vet this to make sure
        self.go_to_head()              # it contains no empty sequences. #also establishes self.stack

    def _is_sequence(self, item=None):
        '''Private method to test whether an item is a non-string sequence.
        If item is None, test current item.'''
        if item is None:
            item = self._get_item_at(self.stack)
        #return isinstance(item, collections.Sequence) and not isinstance(item, basestring) #py2 ?
        return isinstance(item, collections.Iterable) and not isinstance(item, str)

    def _is_in_range(self, index_tuple=None):
        '''Private method to test whether an index is in range.
        If index is None, test current index.'''
        if index_tuple is None:
            index_tuple = self.stack
        if any(x < 0 for x in index_tuple):
            return False
        try:
            self._get_item_at(index_tuple)
        except IndexError:
            return False
        else:
            return True

    def _get_item_at(self, index_tuple):
        '''Private method to get item at an arbitrary index, with no bounds checking.'''
        item = self.nested_list
        for i in index_tuple:
            item = item[i]
        return item

    def _set_item_at(self, index_tuple, value):
        '''Private method to set item at an arbitrary index, with no bounds checking.
        Throws a ValueError if value is an empty non-string sequence.'''
        if self._is_sequence(value) and not value:
            raise ValueError("Cannot set an empty list!")
        containing_list = self._get_item_at(index_tuple[:-1])
        containing_list[index_tuple[-1]] = value

    def _insert_at(self, index_tuple, value):
        '''Private method to insert item at an arbitrary index, with no bounds checking.
        Throws a ValueError if value is an empty non-string sequence.'''
        if self._is_sequence(value) and not value:
            raise ValueError("Cannot insert an empty list!")
        containing_list = self._get_item_at(index_tuple[:-1])
        containing_list.insert(index_tuple[-1], value)

    def _delete_at(self, index_tuple):
        '''Private method to delete item at an arbitrary index, with no bounds checking.
        Recursively deletes a resulting branch of empty lists.'''
        containing_list = self._get_item_at(index_tuple[:-1])
        del containing_list[index_tuple[-1]]
        if not self._get_item_at(index_tuple[:-1]):
            self._delete_at(index_tuple[:-1])

    def _increment_stack(self):
        '''Private method that tires to increment the top value of the stack.
        Returns True on success, False on failure (empty stack).'''
        try:
            self.stack[-1] += 1
        except IndexError:
            return False
        else:
            return True

    def _decrement_stack(self):
        '''Private method that tries to decrement the top value of the stack.
        Returns True on success, False on failure (empty stack).'''
        try:
            self.stack[-1] -= 1
        except IndexError:
            return False
        else:
            return True

    def go_to_head(self):
        '''Move the cursor to the head of the nested list.'''
        self.stack = []
        while self._is_sequence():
            self.stack.append(0)

    def go_to_tail(self):
        self.stack = []
        '''Move the cursor to the tail of the nested list.'''
        while self._is_sequence():
            self.stack.append(len(self.get_item()) - 1)

    def right(self):
        '''Move cursor one step right in the nested list.'''
        while self._increment_stack() and not self._is_in_range():
            self.stack.pop()
        if not self.stack:
            self.go_to_tail()
            return False
        while self._is_sequence():
            self.stack.append(0)
        return True

    def left(self):
        '''Move cursor one step left in the nested list.'''
        while self._decrement_stack() and not self._is_in_range():
            self.stack.pop()
        if not self.stack:
            self.go_to_head()
            return False
        while self._is_sequence():
            self.stack.append(len(self.get_item()) - 1)
        return True

    def move_cursor(self, index_tuple):
        '''Move cursor to the location indicated by index_tuple.
        Raises an error if index_tuple is out of range or doesn't correspond
        to a toplevel object.'''
        item = self._get_item_at(index_tuple)
        if self._is_sequence(item):
            raise IndexError('index_tuple must point to a toplevel object')

    def get_item(self):
        '''Get the item at the cursor location.'''
        return self._get_item_at(self.stack)

    def set_item(self, value):
        '''Set the item a the cursor locaiton.'''
        return self._set_item_at(self.stack, value)

    def insert(self, value):
        '''Insert an item at the cursor location. If value is a sequence,
        cursor moves to the first toplevel object in value after insertion.
        Otherwise, cursor does not move.'''
        temp_stack = self.stack[:]
        self.left()
        self._insert_at(temp_stack, value)
        self.right()

    def delete(self):
        '''Deete an item at the cursor location. Cursor does not move.'''
        temp_stack = self.stack[:]
        self.left()
        self._delete_at(temp_stack)
        self.right()

    def iterate(self):
        '''Iterate over the values in nested_list in sequence'''
        self.go_to_head()
        yield self.get_item()
        while self.right():
            yield self.get_item()

    def iterate_left(self):
        '''Iterate over the values in nested_list in reverse.'''
        self.go_to_tail()
        yield self.get_item()
        while self.left():
            yield self.get_item()


    def iterate_incomplete(self):
        '''Iterate over the values in nested_list in sequence'''
        yield self.get_item()
        while self.right():
            yield self.get_item()

    def iterate_left_incomplete(self):
        '''Iterate over the values in nested_list in reverse.'''
        yield self.get_item()
        while self.left():
            yield self.get_item()

    def find(self, value):
        '''Search for value in nested_list; move cursor to first location of value.'''
        for i in self.iterate():
            if i == value:
                break

    def find_left(self, value):
        '''Search for value backwards in nested_list; move cursor to last location of value.'''
        for i in self.iterate_left():
            if i == value:
                break

class Cursor(NestedListIter):
    """Used to go left and right through containers. Gathers information
    like current Key Signature. A cursor always starts at position
    0 so that it has to move from the start to gather information like
    Signatures which are normaly at the beginning of a track or there
    is none at all so it defaults to Lilypond 4/4 and C Major"""
    #Leave NestedListIter class as it is. It is highly reusable so don't put any notation-crap in there! Better use this Cursor class.

    def __init__(self, track, parentScore):
        """Modified init to save the signatures and other data
        Technically its possible to have multiple cursors. But to find a usecase for that is a bigger problem. Maybe Multiplayer :)
        The cursor holds several stacks for prevailing information. Once the cursor is left of an item it's deleted from the stack.
        So the stack is always correct if the cursor score was linear."""
        if not track:
            raise ValueError('nested_list must be a non-empty sequence')
        self.nested_list = track       # at some point, vet this to make sure
        self.go_to_head()              # it contains no empty sequences. #also establishes self.stack
        self.parentScore = parentScore
        self.parentWorkspace = self.parentScore.parentWorkspace
        self.resetToDefault() # Instead of __init__ store the vars in a seperate function to reset a cursor to 0 more easily. Also protects class variables.


    def getState(self):
        return {
                #An ordered list for all signatures:
                "prevailingSignatures" : [self.prevailingTimeSignature[-1], self.prevailingKeySignature[-1], self.prevailingTempoSignature[-1], self.prevailingPerformanceSignature[-1], self.prevailingDynamicSignature[-1], self.prevailingClef[-1], self.prevailingTempoModification[-1], ] ,  #all of them for your iteration convenience!. The order is very important. For example TempoModification must come after TempoSig because they get inserted in this order.

                #Direct Access:
                "prevailingTimeSignature":self.prevailingTimeSignature[-1],
                "prevailingKeySignature":self.prevailingKeySignature[-1],
                "prevailingTempoSignature":self.prevailingTempoSignature[-1],
                "prevailingPerformanceSignature":self.prevailingPerformanceSignature[-1],
                "prevailingDynamicSignature":self.prevailingDynamicSignature[-1],
                "prevailingClef":self.prevailingClef[-1],
                "prevailingTempoModification":self.prevailingTempoModification[-1],
                "legato":self.legato, #can be tested as bool
                "noteTies":self.noteTies,
                "flatCursorIndex":self.flatCursorIndex,
                }

    def resetToDefault(self):
        #It is important to not use the same instance of the signatures as reset because you can't modify existing ones during export then. The base sigs need to be resetted as well everytime.
        self.prevailingTimeSignature = [TimeSignature(self.parentWorkspace, 4, 384)] # A stack to hold tuplets of (Tickindex, TimeSig) pairs. Default 4/4, as Lilypond dictates. The default one never gets deleted if every stack modification is done by the cursor.
        self.prevailingKeySignature = [KeySignature(self.parentWorkspace, 20, [(3,0), (0,0), (4,0), (1,0), (5,0), (2,0), (6,0)])] #C Major by default, as Lilypond dictates. The first one never gets deleted if every stack modification is done by the cursor.
        self.prevailingTempoSignature = [TempoSignature(self.parentWorkspace, 120, 384)] #A stack to hold the tempo and relative to what duration. Defaults to 120 quarter notes per minute. The other stacks above for more info. Technically it's possible to have different tempos in different tracks. Thats not how time works. This only matters for internal affairs. Midi and Lilypond get it right.
        self.prevailingPerformanceSignature = [self.parentScore.defaultPerformanceSignature] # A stack to hold performance data. How loud is "Forte" at the moment? How short a staccato note? Defaults to a score-wide default.
        self.prevailingDynamicSignature = [DynamicSignature(self.parentWorkspace, "mf")] #A stack to hold the currently valid dynamic. Which is itself just a reference to the currenctly valid PerformanceSignature
        self.prevailingClef = [Clef(self.parentWorkspace, "treble", "")] #For convenience. Has no usage in the backend, only in importers or other api based programs.
        self.prevailingTempoModification = [TempoModification(self.parentWorkspace, 0)] #Zero modification value
        self.tempoModInts = [0]
        self.tickindex = 0  #Since the cursor can only move left and right we can create the current tickindex aka. timeindex position by adding and substracting. #TODO "Go To Timeindex" is not trivial since it is possible that at timeindex X there is a 0-ticks object followed by the real object. So we always have to search for timeindex X+1 to get an object with real duration.
                            #Anything that is right of the cursor (including the current item, which is technically right of the cursor as well) does not matter for the tickindex and is not saved in here.
        self.ticksSinceLastMeasureStart = 0
        self.measureNumber = 0
        self.timeindex = 0  # time in seconds
        self.legato = [] #Makes nested slurs possible. If any object is in here the midi notes will output 100% duration. Else they will output dur * performanceSignature[-1].duration modification.
        self.noteTies = {} #During midi export this is used to indicate if a noteOn should be ommited. NoteOff is controlled by note.tie = True/False

        self.flatCursorIndex = 0 #the number of items left of the cursor.
        self.upbeatTicks = 0 #the number of upbeat ticks left of the cursor

        self.prevailingSmfPatch = [self.nested_list.smfPatch]   #midi pach number 0-127. no bank.
        self.prevailingJackPatch = [self.nested_list.jackPatch]  #midi pach number 0-127. no bank
        self.prevailingJackChannel = [self.nested_list.jackChannel[0]]  #only for jack. We are only interested in the midi note here, so we don't need the whole channel range

    _pitchindex = 1420 #The pitchindex is a CLASS variable. It is the same for all cursors in all tracks in all scores. In other words, a program wide state. not saved
    prevailingDot = False #For the sake of simplicity this is only a bool. No "four prevailing dots". not saved.
    oneTimePrevailingDot = False #For the sake of simplicity this is only a bool. No "four prevailing dots". not saved.
    prevailingDuration = 384 #Prevailing Duration is a CLASS variable. See pitchindex above. Not saved.
    slursOn = set([SlurOn, PhrasingSlurOn])
    slursOff = set([SlurOff, PhrasingSlurOff])

    @property
    def pitchindex(self):
        return Cursor._pitchindex

    @pitchindex.setter
    def pitchindex(self, value):
        """this returns "" and can't be set"""
        if value >= 3120:
            value = 3120
        elif value <= 20:
            value = 20
        Cursor._pitchindex = pitch.toWhite(value)
        return Cursor._pitchindex

    def up(self):
        """Move the cursor/pitchindex up, +50.
        The cursor always goes up and down the c major scale.
        Call with Cursor.up(). Class method """
        self.pitchindex += 50

    def down(self):
        """Move the cursor/pitchindex down, -50.
        The cursor always goes up and down the c major scale.
        Call with Cursor.down().  Class method"""
        self.pitchindex -= 50

    def upOctave(self):
        """Move the cursor/pitchindex up, +350.
        Call with Cursor.upOctave().  Class method"""
        self.pitchindex += 350

    def downOctave(self):
        """Move the cursor/pitchindex down, -350.
        Call with Cursor.downOctave().  Class method"""
        self.pitchindex -= 350

    @property
    def metricalPosition(self):
        """Conversion ticksSinceLastMeasureStart to metrical position.
        Starts from 0"""
        #Formula: (ticks / denominator + 1) / nominator  = measurenumber, rest is the position in that measure.
        return divmod(self.ticksSinceLastMeasureStart / self.prevailingTimeSignature[-1].denominator, self.prevailingTimeSignature[-1].nominator)[1]

    #def isMetricalStressed(self):
    #    """Returns if this on a relatively stressed position or not.
    #    Depends on the timesig.
    #    Since in 4/4 a quarter on the second position is not stressed
    #    but the beginning of an 8th pair is relatively stressed the
    #    function also takes into account the current rhythm layer."""
    #    self.prevailingTimeSignature[-1].denominator
    #    return True

    def _is_sequence(self, item=None):
        '''Private method to test whether an item is a non-string sequence.
        If item is None, test current item.'''
        if item is None:
            item = self._get_item_at(self.stack)
        return isinstance(item, collections.Iterable) and item.cursorWalk

    def right(self):
        """Modified cursor right to collect data for the tickindex, timeindex and others."""
        #Important note: After inserting the cursor goes right with the right() method below. This means everything you insert will automatically be parsed, be it for tick index or keysignature.
        #Add the current item.duration to the tickindex before(!) the cursor goes to the next object. This way it appears that everything left of the cursor is saved in the tickindex
        #TODO: Even at the end of a list (Appending Position) it still adds data on cursor right. Only the appending position with duration, time = 0 etc. prevents adding. This is a hack. The correct solution would be to check if we are at the end of a list. Contrary to cursor left the above happens because the adding happens before stepping right. It is possible to use gather in the same spot as flatIndex++ below. But it is unclear what happens if the item is iterable.
        def gather(current_item):
            self.timeindex += current_item.duration / ((self.prevailingTempoSignature[-1].beatsPerMinute / 60) * self.prevailingTempoSignature[-1].referenceTicks) #Gather the time for each item.  #TODO: tempo modifications
            self.tickindex += current_item.duration  #it is possible to round(current_item.duration) to get rid of 384.0 . But why? If someone does not want notation output it still can be precise here.

            #Measure Number and metrical position
            self.ticksSinceLastMeasureStart += current_item.duration
            #print (self.ticksSinceLastMeasureStart)
            tickRest = self.ticksSinceLastMeasureStart - self.prevailingTimeSignature[-1].nominator * self.prevailingTimeSignature[-1].denominator #nominator * denominator is the complete measure tick value
            if tickRest >= 0:  #this was just > for more than year. Measure NUmbers were always slightly off, but how could anything have worked!?
                #Measure Number calculation does not protect against human error.  If a user adds a Time Signature in an incomplete measure calculation will fail (not critical).
                #position and measurenumber start from 0, therefore a GUI needs to add 1 on each value.
                #Formula: (ticks / denominator + 1) / nominator  = measurenumber, rest is the position i
                self.measureNumber += 1
                self.ticksSinceLastMeasureStart = tickRest  #If one measure was overfull the rest ist chosen as next startpoint.

            #Add items to the stacks.
            if isinstance(current_item, Chord):
                return True #speed things up

            typ = type(current_item)
            if typ is TempoModification:
                self.prevailingTempoModification.append(current_item)
                self.tempoModInts[-1] += current_item.bpmModificator #this is too late. The tempo modification was already exported without this value. We keep this in for cursor reasons, but it is not used in Playback.
            elif typ is KeySignature:
                self.prevailingKeySignature.append(current_item)
            elif typ is TimeSignature:
                self.prevailingTimeSignature.append(current_item)
            elif typ is TempoSignature:
                self.prevailingTempoSignature.append(current_item)
                self.tempoModInts.append(0) #this is too late. The tempo sig was already processed by exportPlayback etc. at this point. It only works because tempoSig does export with modifications, but always plain.  See end of this right() function where the place to insert for such processing methods is.
            elif typ is PerformanceSignature:
                self.prevailingPerformanceSignature.append(current_item)
            elif typ is DynamicSignature:
                self.prevailingDynamicSignature.append(current_item)
            elif typ is Clef:
                self.prevailingClef.append(current_item)
            elif typ in self.slursOn:
                self.legato.append(True)
            elif typ in self.slursOff:
                if self.legato: #it is possible that a user just inserted a slur-close without opening one.
                    self.legato.pop()
            elif typ is Upbeat:
                self.upbeatTicks += current_item.duration
            elif typ is InstrumentChange:
                self.prevailingSmfPatch.append(current_item.smfPatch)
                self.prevailingJackPatch.append(current_item.jackPatch)
            elif typ is ProgramChangeAbsolute: #only for jack
                self.prevailingJackPatch.append(current_item.number)
            elif typ is ProgramChangeRelative: #only for jack
                self.prevailingJackPatch[-1] += current_item.number #TODO: is this also too late, as tempoModification?
            elif typ is ChannelChangeAbsolute: #only for jack. We are only interested in the midi note here, so we don't need the whole channel range
                self.prevailingJackChannel.append(current_item.number)
            elif typ is ChannelChangeRelative: #only for jack. We are only interested in the midi note here, so we don't need the whole channel range
                self.prevailingJackChannel[-1] += current_item.number #TODO: is this also too late, as tempoModification?

        if not self._is_sequence(): #gather data before stepping right.
            gather(self.get_item())

        while self._increment_stack() and not self._is_in_range():
            self.stack.pop()
        if not self.stack: #End of track
            self.go_to_tail()
            return False
        else:
            self.flatCursorIndex += 1 #one item more left of the cursor.
        while self._is_sequence():
            self.stack.append(0)

        #This is the point in time when we are on the next item and wait for further instructions.
        #During insert this will be Appending most of the time.
        #However, anything we add here will double the processing cost.
        #This will also not process the first item, after resetToDefault.
        #Also: Everything here needs a complementary action (like substraction of values) in the cursor left method, but in a place that is not possible: lookback one position before the current, left of the cursor.
        #Without the complementary action you can step ON an item, it will be processed here, then go left: nothing happens. spam left-right a few times and it will be processed each time.
        #Conclusion: Don't do anything here.

        ##print (self.get_item())
        return True

    def left(self):
        """Modified cursor left to collect data for the tickindex, timeindex and others."""
        while self._decrement_stack() and not self._is_in_range():
            self.stack.pop()
        if not self.stack:
            self.go_to_head()
            return False
        while self._is_sequence():
            self.stack.append(len(self.get_item()) - 1)

        current_item = self.get_item()

        #After stepping left substract the current item.duration from the tickindex. This way it appears that the cursor is now left of the item (eventhough technically its still selected) and is therefore not included in the tickindex.
        self.tickindex -= current_item.duration
        self.timeindex -= current_item.duration / ((self.prevailingTempoSignature[-1].beatsPerMinute / 60) * self.prevailingTempoSignature[-1].referenceTicks)
        self.flatCursorIndex -= 1 #one item more right of the cursor.
        if self.flatCursorIndex < 0:
            self.flatCursorIndex = 0
        #Measure Number and metrical position
        self.ticksSinceLastMeasureStart -= current_item.duration #can become < 0, we crossed a measure-border left.
        #print (self.ticksSinceLastMeasureStart)
        if self.ticksSinceLastMeasureStart < 0 :
            self.measureNumber -= 1
            self.ticksSinceLastMeasureStart = self.prevailingTimeSignature[-1].nominator * self.prevailingTimeSignature[-1].denominator + self.ticksSinceLastMeasureStart  #nominator * denominator is the complete measure tick value

        #Remove items from the stacks
        if isinstance(current_item, Chord):
            return True #speed things up

        typ = type(current_item)
        if typ is TempoModification:
            self.prevailingTempoModification.pop()
            self.tempoModInts[-1] -= current_item.bpmModificator
        elif typ is KeySignature:
            self.prevailingKeySignature.pop() #remove top keysig from stack which should be the one the cursor just crossed.
        elif typ is TimeSignature:
            self.prevailingTimeSignature.pop()
        elif typ is TempoSignature:
            self.prevailingTempoSignature.pop()
            self.tempoModInts.pop()
        elif typ is PerformanceSignature:
            self.prevailingPerformanceSignature.pop()
        elif typ is DynamicSignature:
            self.prevailingDynamicSignature.pop()
        elif typ is Clef:
            self.prevailingClef.pop()
        elif typ in self.slursOn:
            self.legato.pop()
        elif typ in self.slursOff:
            self.legato.append(True)
        elif typ is InstrumentChange:
            self.prevailingSmfPatch.pop()
            self.prevailingJackPatch.pop()
        elif typ is ProgramChangeAbsolute: #only for jack
            self.prevailingJackPatch.pop()
        elif typ is ProgramChangeRelative: #only for jack
            self.prevailingJackPatch[-1] -= current_item.number
        elif typ is ChannelChangeAbsolute: #only for jack. We are only interested in the midi note here, so we don't need the whole channel range
            self.prevailingJackChannel.pop()
        elif typ is ChannelChangeRelative: #only for jack. We are only interested in the midi note here, so we don't need the whole channel range
            self.prevailingJackChannel[-1] -= current_item.number
        elif typ is Upbeat:
                self.upbeatTicks -= current_item.duration
        return True

    def iterate(self):
        '''Iterate over the values in nested_list in sequence'''
        self.go_to_head()
        self.resetToDefault()
        yield self.get_item()
        while self.right():
            yield self.get_item()

    def rightToFirstDuration(self):
        """A very strict function that goes right until it finds
        an item with duration. If the current item already has duration
        it does not move the cursor"""
        if self.get_item().duration:
            return True
        while self.right(): #no fear for the end of the track
            if self.get_item().duration:
                return True
        else:
            return False

    def insert(self, value):
        '''Insert an item at the cursor location, which is insert-left
        If value is a sequence, cursor moves to the first toplevel
        object in value after insertion. Otherwise, cursor does not
        appear to move. Actually it moves from the new item to the next
        item right, which is the same item as it was before insert.

        So the cursor stays on its item during insert.

        Count instances. A fresh item has instancecount == 0, linked
        items may have more already. This insert raises by 1.
        '''
        temp_stack = self.stack[:]
        if not temp_stack == [0] and temp_stack[-1] == 0:  #this is the beginning of a container but not the beginning of the track. We assume a Start() item here and want the new_item to be inserted before this, still in the parent list.
            temp_stack = temp_stack[:-1]  #delete the last [..., 0] from the stack which results in inserting exactly before the container.

        #self.left()  #this results not in insert left

        self._insert_at(temp_stack, value)
        value.instanceCount += 1

        if self.stack[-1] == 0: #container beginning in general.
            if self.stack == [0]: #not only a container beginning but track beginning.
                pass
            elif self.stack == [0,0]:
                self.go_to_head()
                self.resetToDefault()
            else:
                #directly before container start prevent cursor gather error by wiggeling
                self.left()
                self.right()


        #Finally step right, which gathers all data and updates the
        #cursor class itself (index-counter, measure counter etc.)
        self.right()
        return True

    def delete(self):
        """Modified delete to protect Appending from deletion.
        Also decrease InstanceCount

        This is delete RIGHT so we need to go left once."""
        item = self.get_item()

        #if isinstance(item, Appending):
        #    return False #Abort. Appending, Start and End are immune to delete. the api never lets this through, but better safe than sorry.

        temp_stack = self.stack[:]
        item.deselect() #to play safe try to deselect.
        item.instanceCount -= 1
        notTrackStart = self.left()
        self._delete_at(temp_stack)
        if notTrackStart:
            self.right()
        else:
            #track beginning.
            self.go_to_head()
            self.resetToDefault()

    def delete_list(self):
        """Delete the most inner list/container.
        aka. "Delete Container Instance"

        Go left until the stack[-1] is 0 and then one step more.
        Everything right of the cursor is of no interest then and we can
        delete the complete container without resetting any data.

        Returns False if not in any container currently."""
        if len(self.stack) == 1:
            return False
        else:
            while not self.stack[-1] == 0:
                self.left()
            cont = self.nested_list.container #the whole track
            if len(self.stack) == 2: #toplevel container, directly in the track
                ret = cont.pop(self.stack[0])
            else: #example: [4, 3, 2] so we need to delete [4,3]. [-1] does not matter, [-2] is the .pop() value and [-3] is the list to pop from.
                length = len(self.stack)
                for num, coord in enumerate(self.stack): #enumerate is from 0. len is 'from' 1
                    cont = cont[coord] #recursive
                    if length-3 == num:
                        ret = cont.container.pop(self.stack[-2])
                        break


                #ret = eval(self.createDeleteFunctionAsString(self.stack[:-1])).pop()
        self.left()
        self.right()
        self.syncTickIndex()
        return ret

    def goToIndex(self, index):
        """Go to flatCursorIndex. This is always unique and correct.
        Goes nowhere if end of track or beginning is reached before
        reaching the target.

        This does not go left, it always goes right. """

        start = self.flatCursorIndex
        if start == index:
            return True

        self.go_to_head()
        self.resetToDefault() #Reset all counters and gatherers to 0.

        while self.flatCursorIndex != index:
            if not self.right(): #Track End
                self.goToIndex(start)
                return False
        return True

    def goToTickIndex(self, ticks, returnOnError = True):
        """Move the cursor to the first(!) item which is active
        at the given tick.
        For accurate score returnOnError is on.
        For multi-staff sync this will be turned off dynamically.
        For normal score returnOnError will be False.

        If returnOnError is False this will jump to the first item
        after the given tick position.
        """
        if ticks == self.tickindex: #we are there.
            return True
        else:
            start = self.flatCursorIndex
            self.go_to_head()
            self.resetToDefault() #Reset all counters and gatherers to 0.
            while self.tickindex < ticks:
                if not self.right(): #track ends
                    if returnOnError:
                        return self.goToIndex(start) #go back to the original position
                    return False #track is shorter than ticks.
            return True

    def syncTickIndex(self):
        """Stay on the same item, but update the tickindex.
        This is used if a linked item under the cursor is not the first
        one in a track.
        If you change the duration of such an item the complete cursor
        tickindex will be off by the duration difference, cumulative
        for each linked item in the track.
        So we store the index (pure object count), go to the head
        and crawl right until we are back. Then the tickindex is ok."""
        originalPosition = self.stack
        self.go_to_head()
        self.resetToDefault() #Reset all counters and gatherers to 0.
        while not originalPosition == self.stack:
            self.right()

    def get_container(self):
        """returns the parent container except the track or score.
        Returns None when technically in a container but a the start
        position which is considered outside the container for the user."""
        temp_stack = self.stack[:]
        start = temp_stack == [0] or temp_stack[-1] == 0  #this is the beginning of a container or the track start which is per definition outside of a container or at a Start(). We assume a Start() item here and want the new_item to be inserted before this, still in the parent list.
        if len(temp_stack) > 1 and not start:  #we have at least one nested container right now
            return self._get_item_at(temp_stack[:-1])
        else:
            return None #we are just in a track or at the start position

    def beginning(self):
        """Return all items up to the first one with duration.

        Uses the cursor so we don't need to care about containers.
        This also means the cursor will be resetted.
        Use only when dealing with copies where you reset the cursor
        anyway. e.g. export playback.

        Use this to figure out if a tempo sig, timesig etc. was set
        before the first real music event (note, rest etc.)"""
        l = []
        for i in self.iterate():
            if i.duration:
                return l
            else:
                l.append(i)
        return [] #if only no-duration items treat this track as non-existent. Return nothing.
