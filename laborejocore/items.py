# -*- coding: utf-8 -*-
from laborejocore import pitch, lilypond, playback
import copy, uuid, operator, random, warnings, re
from fractions import gcd, Fraction


class Lilypond(object):
    """One of the basic representation classes. Like Internal and Playback
    Export and Export-override for Lilypond"""
    def __init__(self):
        super(Lilypond, self).__init__()
        self.lilypond = "" #A string of Lilypond syntax. Used on ly export. Overrides any auto-generated ly from chords, timesigs etc. even whole containers. Set to "" (as false) for auto-generation (Default) and " "(one space) to surpress any lilypond output for this Item.
        self.lilystring = ""  #This is purely a safeguard against wait for chord functions applied to the wrong object, which is any except WaitForChord.
        #Additional Lilypond strings X. A dict with tag = value. Exported from left to right. Only one entry of the same dict.
        self.directivePre = {} #Pre: X before the note-name "Xa2" #Pre: X before the < chord "X<c e g>2"
        self.directiveMid = {} #Middle: X before duration "aX2" #Middle: X after > "<c e g>X2" ONLY for chords. You can define it for any item, but it will not be used.
        self.directivePst = {} #Post: X after duration "a2X" #Post: X after duration "<c e g>2X"
        #Hidden has a higher priority than transparent.
        self.hidden = False #Determines if the object is hidden. Each Laborejo Item needs a .lyGrob property other than None if it wants support by the hide function.
        self.transparent = False #Determines if the object is hidden.
        self.lyGrob = [] #A list of strings. Used together with self.hidden and self.transparent to determine how to instruct lilypond to make hidden/transparent. Can also be just "True" but then you have to reimplement self.getHiddenPrefix() which expects a list. A True marker is just to inform the UI or script that this item can be hidden at all.

    def generateLilypond(self):
        """if in doubt, return nothing. still compatible"""
        return ""

    def getHiddenPrefix(self):
        #apparently it has to be "Staff." and not Voice. or even without a context
        if self.hidden and self.lyGrob:
            hideOrTransparent = []
            for grob in self.lyGrob:
                hideOrTransparent.append("\\once \\override Staff."+grob+" #'stencil = ##f")
            return " ".join(hideOrTransparent) + " "
        elif self.transparent and self.lyGrob: #Hidden beats transparent
            hideOrTransparent = []
            for grob in self.lyGrob:
                hideOrTransparent.append("\\once \\override Staff."+grob+" #'transparent = ##t")
            return " ".join(hideOrTransparent) + " "
        else:
            return ""

    def exportLilypond(self):
        """Mid directives are in chords or whatever uses them.
        There is no way else to place them in between "something"
        because we don't know what "something" is here"""
        hideOrTransparent = self.getHiddenPrefix()
        if self.lilypond:
            return hideOrTransparent + " ".join(self.directivePre.values()) + self.lilypond + " ".join(self.directivePst.values())
        else:
            return hideOrTransparent + " ".join(self.directivePre.values()) + self.generateLilypond() + " ".join(self.directivePst.values()) #Maybe get lilypond and duration as seperate parameters

class EmptyDict(dict):
    """A fake always-empty dict type.
    To prevent Appending and others from having lilypond directives"""
    def __init__(self):
        super(EmptyDict, self).__init__()
    def __setitem__(self, value, something):
        return True #Just pretend it's ok.

class Internal(object):
    """One of the basic representation classes.
    Like Lilypond and Playback.
    Anything (even a container or the appending item) has a duration
    internaly. Here are the vars to hold the information."""
    def __init__(self):
        super(Internal, self).__init__()
        #Durations:
        self._base = 0 #base value. Without dots, tuplets/times , overrides etc. this is the duration for all representations
        # Tuplet multiplication is used for all representations and gets auto-merged on lilypond output to tuplet groups (if its in a chord).
        self._tuplets = [] # a list of lists: tuplets [numerator, denominator]. Each is one level of tuplets, arbitrary nesting depth. [2,3] for triplet
        self._dots = 0 #number of dots.
        self._scaleFactorNumerator = 1 # This is not a grouped lilypond tuplet but a non-printing scale factor which exports as lilypond scaling duration and internal/playback representation http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Writing-rhythms#Scaling-durations
        self._scaleFactorDenominator = 1 # belongs to scaleFactorNumerator
        self.instanceCount = 0 #this is a class var. We never use it as Internal.instanceCount, only as createdItem.instanceCount. So we can see it as a template. If a created item sets .instanceCount higher than 0 later it will be an instance var.
        self.cursorWalk = False #always off. This is against typechecking for Containers vs. Items
        self.notelist = [EmptyNote()]  #Prevents typechecking. Calling "intervalUp" on a keysig should not crash the program.
        self.container = None
        self.substitution = None
        self._triggerString = None #A string, evaluated as python code. Return True or False to allow or prevent playback execution. Exception: If it is None, which is the case most of the time, it will be processed. This can modify self variables like it wants. Playback works on copies, so no initial value will be overwritten.
        self.flatCursorIndex = None #used during export. Only for copied objects, so normal objects never have that. Even during playback some standard objects like initial default timesigs don't get one.


    @property
    def triggerString(self):
        return self._triggerString

    @triggerString.setter
    def triggerString(self, value):
        if value is "":
            self._triggerString = None
        else:
            self._triggerString = value

    def getDurations(self):
        """Return a dict that holds all durations. For diagnosis and
        corner cases. Don't save them anywhere static.
        It is not guaranteed that the variables keep the same name and
        format"""
        ret = {}
        ret["_base"] = self._base
        ret["_tuplets"] = self._tuplets
        ret["_dots"] = self._dots
        ret["_scaleFactorNumerator"] = self._scaleFactorNumerator
        ret["_scaleFactorDenominator"] = self._scaleFactorDenominator

        return ret

    def putDurations(self, durationDict):
        """Overwrite all internal duration related parameters with
        new ones from a dict from getDuration"""
        self._base = durationDict["_base"]
        self._tuplets = durationDict["_tuplets"]
        self._dots = durationDict["_dots"]
        self._scaleFactorNumerator = durationDict["_scaleFactorNumerator"]
        self._scaleFactorDenominator = durationDict["_scaleFactorDenominator"]

    def _closest(self, num, datalist):
        """only for positive numbers"""
        if num in datalist:
            return num
        else:
            positions = len(datalist)
            tmplist = [0] + sorted(datalist)
            tmplist.reverse()
            #Loop over all positions except the last 0.

            for x in range(positions):
                if tmplist[x+1] < num < tmplist[x]:
                    return tmplist[x]
            else: #the highest value in datalist was smaller
                return None


    def guessDurationAbsolute(self, completeDuration):
        """Take one absolute tick value and guess what is the best
        duration combination (base, tuplets, dots) for it."""
        durationDict = {}
        durationDict["_base"] = 0
        durationDict["_tuplets"] = []
        durationDict["_dots"] = 0
        durationDict["_scaleFactorNumerator"] = 1
        durationDict["_scaleFactorDenominator"] = 1

        durList = [12288, 6144, 3072, 1536, 768, 384, 192, 96, 48, 24, 12, 6]
        guessedBase = self._closest(completeDuration, durList)


        if guessedBase:
            if completeDuration in durList: #all basic "notehead" durations
                durationDict["_base"] = completeDuration
                whatMethodWasUsed = "direct"
            elif completeDuration/1.5 in durList:  #single dotted
                durationDict["_base"] = completeDuration/1.5
                durationDict["_dots"] = 1
                whatMethodWasUsed = "singledot"
            elif completeDuration/1.5/1.5 in durList:  #double dots
                durationDict["_base"] = completeDuration/1.5/1.5
                durationDict["_dots"] = 2
                whatMethodWasUsed = "doubledot"
            else: #tuplet. That means the value is below a standard "notehead" duration. We need to find how much much lower.
                durationDict["_base"] = guessedBase
                ratio = completeDuration / guessedBase #0.666~ for triplet
                newRatio = Fraction(ratio).limit_denominator(100000) #protects 6 or 7 decimal positions
                durationDict["_tuplets"] = [[newRatio.numerator, newRatio.denominator]]
                whatMethodWasUsed = "tuplets"
        else:
            #the base could not be guessed. In this case we scale.
            #This happens if this is a very long note, for example when using core.score.makeTracksEqualLengthWithPlaceholders which adds a skip to the end of the track to make it (much) longer
            durationDict["_base"] = 6
            f = Fraction(6, int(completeDuration))
            durationDict["_scaleFactorNumerator"] = f.denominator
            durationDict["_scaleFactorDenominator"] = f.numerator
            whatMethodWasUsed = "scale"


        self.putDurations(durationDict)
        return whatMethodWasUsed


    #SubClass methods to pass. See docstring.
    def nothing(self, *arg):
        return ""

    generateFiguredBass = generateChordsymbols =  highest = lowest = addNote = deleteNote = applyNoteMethodToChord =  augment = diminish =  addDot = removeDot = nothing

    @property
    def base(self):
        return self._base
    @base.setter
    def base(self, value):
        self._base = value
        if self.instanceCount > 1:
            #self.parentWorkspace.score.activeCursor.syncTickIndex()
            #TOOO: The problem is that with syncTickIndex() only the ACTIVE cursor becomes correct. But we need the correct cursor for each track where the instance is in.
            #Possible solution. Maybe slow in mass insert like copy and paste.
            self.parentWorkspace.score.syncCursors()
        return self._base

    @property
    def dots(self):
        return self._dots
    @dots.setter
    def dots(self, value):
        self._dots = value
        if self.instanceCount > 1:
            #workspace.score.activeCursor.syncTickIndex()
            #TOOO: The problem is that with syncTickIndex() only the ACTIVE cursor becomes correct. But we need the correct cursor for each track where the instance is in.
            self.parentWorkspace.score.syncCursors()
        return self._dots

    @property
    def tuplets(self):
        return self._tuplets
    @tuplets.setter
    def tuplets(self, value):
        self._tuplets = value
        if self.instanceCount > 1:
            #self.parentWorkspace.score.activeCursor.syncTickIndex()
            #TOOO: The problem is that with syncTickIndex() only the ACTIVE cursor becomes correct. But we need the correct cursor for each track where the instance is in.
            self.parentWorkspace.score.syncCursors()
        return self._tuplets

    @property
    def scaleFactorNumerator(self):
        return self._scaleFactorNumerator
    @scaleFactorNumerator.setter
    def scaleFactorNumerator(self, value):
        self._scaleFactorNumerator = value
        if self.instanceCount > 1:
            #self.parentWorkspace.score.activeCursor.syncTickIndex()
            #TOOO: The problem is that with syncTickIndex() only the ACTIVE cursor becomes correct. But we need the correct cursor for each track where the instance is in.
            self.parentWorkspace.score.syncCursors()
        return self._scaleFactorNumerator

    @property
    def scaleFactorDenominator(self):
        return self._scaleFactorDenominator
    @scaleFactorDenominator.setter
    def scaleFactorDenominator(self, value):
        self._scaleFactorDenominator = value
        if self.instanceCount > 1:
            #self.parentWorkspace.score.activeCursor.syncTickIndex()
            #TOOO: The problem is that with syncTickIndex() only the ACTIVE cursor becomes correct. But we need the correct cursor for each track where the instance is in.
            self.parentWorkspace.score.syncCursors()
        return self._scaleFactorDenominator

    """
    def __setattr__(self, name, value):
        object.__setattr__(self, name, value)
        print ("Set", name, " = ", value)
        if self.instanceCount > 1:
            #Interesting! This tracks every variable change in my whole Program. Remove the instance count test and we get a complete log.
            print ("========LINKED ITEM CHANGED=======", name, value)
            self.parentWorkspace.score.activeCursor.syncTickIndex()
    """

class Playback(object):
    """One of the basic representation classes. Like Internal and Lilypond.
    One data-set is a list [midi-bytes, data1, data2]
    For example [0x90, 60, 127] for a note-on, pitch 60, velocity 127.
    """
    def __init__(self):
        super(Playback, self).__init__()
        self.playback = [] #SMF data set. It is possible to override the complete playback (Instructions are still extra). If empty it will trigger normal midi generation, if positive it will use this value. if None it will supress any midi output
        self._split = 1 #  For tremolo and repetition notes. When it comes to execution
        self._durationFactor = 1 # Each note in the chord has a shifted noteOff. Does not affect internal logic or lilypond. Next NoteOn is not!! affected.
        self._velocityFactor = 1 #Each note in the chord has a modified velocity.
        self.instructionPre = {} #Before the current object interpret this midi code. Each value is a list, a smf midi message is [0] and an offset in ticks is [1]. This can be 0.
        self.instructionPst = {} #After the current object interpret this midi code. Each value is a list, a smf midi message is [0] and an offset in ticks is [1]. This should not be 0, since it must be after the PRE and the item. The item smf generation is responsible for the correct tick offset.
        self.channelOffset = 0
        self.forceChannel = -1 #We can't use None or 0 here because we need the 0 as channel number.
        self.overlapAvoider = 1
        self.smfAddDuration = 0 #Add or substract the duration during playback execution. Makes it possible to alter a complete track by shifting all following events to an earlier or later time.
        self.subitoDynamicKeyword = None #a temp value for playback export. sfz and sfp. not saved
        self.velocityKeyword = "" #for chords. sfz, sfp. one time overrides.
        self.programOffset = 0
        self.forceProgram = -1 #We can't use None or 0 here because we need the 0 as channel number.

    @property
    def split(self):
        """Chord() actually returns _slit"""
        return 1

    @split.setter
    def split(self, value):
        pass

    @property
    def durationFactor(self):
        return self._durationFactor

    @durationFactor.setter
    def durationFactor(self, value):
        """not below 0"""
        if value <= 0.1:
            value = 0.1
        self._durationFactor = round(value, 3)
        return self._durationFactor

    @property
    def velocityFactor(self):
        return self._velocityFactor

    @velocityFactor.setter
    def velocityFactor(self, value):
        """not below 0"""
        if value <= 0.1:
            value = 0.1
        self._velocityFactor = round(value, 3)
        return self._velocityFactor

    def getSmfChannel(self, channel, jackMode, workChannelForNotes = False):
        """Channel is a list with two members:
        [min, max]. For single channel, noteOn+noteOff we take min only.
        This function is for single Channel only since offset and force
        do not apply to ranges. """
        if jackMode and workChannelForNotes:
            if self.forceChannel >= 0:
                #print ("[getSmfChannel]:", self.forceChannel + self.channelOffset)
                return self.forceChannel + self.channelOffset
            else:
                #print ("[getSmfChannel]:", channel[2] + self.channelOffset)
                return channel[2] + self.channelOffset
        else:
            #print ("[getSmfChannel]:", channel[0])
            return channel[0]


    def _addSmfEvent(self, smfTrack, channel, message, jackMode, smf):
        """Execute one smf message which is a tuple consisting of:
        [0] midibytes, the midi message, a list with three members,
        still in native python encoding.
        [1] is a tick offset(int), negative or positive.

        smfTrack is the binary data from libsmf, handed us by
        score.exportPlayback.
        While smfTrack is a parameter of every midi function it is
        only used in self._addSmfEvent and in item.exportPlayback
        the only function used from smfTrack is smfTrack.add_event()
        so you can search for that in this file to get all use-cases
        of the binary libsmf data.

        channel is the midi channel 0-15

        jackMode is either the internal one or JACK.

        Does not touch the original message but works on a copy.
        """
        smfEvent = copy.deepcopy(message[0])
        #If the midi byte message is too high we got a global message like a time signature. This is not for any channel.
        if smfEvent[0] >= 256-16:
            smfTrack.add_event(smf.Event(smfEvent), pulses=self.parentWorkspace.score.activeCursor.tickindex + message[1])
            return True #abort _addSmfEvent successfully

        #If the event is note on or note off we only send on one channel, the working channel, and therefore get the midi channel plus and minus or force.
        if smfEvent[0] == 0x90 or smfEvent[0] == 0x80:
            smfEvent[0] += self.getSmfChannel(channel, jackMode, workChannelForNotes = True)
            smfTrack.add_event(smf.Event(smfEvent), pulses=self.parentWorkspace.score.activeCursor.tickindex + message[1])

        #If channel is a range we send on all involved channels.
        elif channel[1] > channel[0]:
            #channel is a list with two members [min, max].
            originalEventWithoutChannel = smfEvent[0]
            for i in range(channel[0], channel[1]+1):
                smfEvent[0] = originalEventWithoutChannel + i
                smfTrack.add_event(smf.Event(smfEvent), pulses=self.parentWorkspace.score.activeCursor.tickindex + message[1])

        #If the channel-list is wrong, e.g. it is a not the basis for a range, we just send on one channel without force or offset:
        else:
            smfEvent[0] += channel[0]
            smfTrack.add_event(smf.Event(smfEvent), pulses=self.parentWorkspace.score.activeCursor.tickindex + message[1])

    def executeSmf(self, smfTrack, channel, message, jackMode, smf):
        self._addSmfEvent(smfTrack, channel, message, jackMode, smf)

    def executeInstructionPre(self, smfTrack, channel, jackMode, smf):
        for key, value in self.instructionPre.items():
            self._addSmfEvent(smfTrack, channel, message = value, jackMode = jackMode, smf=smf)

    def executeInstructionPst(self, smfTrack, channel, jackMode, smf):
        for key, value in self.instructionPst.items():
            self._addSmfEvent(smfTrack, channel, message = value, jackMode = jackMode, smf=smf)

    def generatePlayback(self, smfTrack, channel, transpose, jackMode, smf):
        return []

    def executeGenerated(self, smfTrack, channel, transpose, jackMode, smf):
        """A chord will at least return two lists in generatePlayback,
        one for note on and one for note off. Note off brings its own
        tick offset, obviously, because it is later than our parameter
        "ticks" here."""
        messageList = self.generatePlayback(smfTrack, channel, transpose, jackMode, smf)

        if not messageList is None: #Empty Lists are still valid.
            for message in messageList:
                self._addSmfEvent(smfTrack, channel, message, jackMode, smf)
        else:
            raise RuntimeError("Tried to execute a midi message but None was returned from", self, messageList)

class Note(Lilypond, Playback):
    """A single note. Only used in chord. Not standalone. Needs at least the pitch"""
    def __init__(self, pitch):
        super(Note, self).__init__()
        self._pitch = pitch
        self.finger = ""
        self.tie = ""
        self.durationOverride = 0 #Wants one signed int which will become the level number.1 means one level up: quarter->half.-1 means quarter->eighth
        #Normaly a note has the same duration as the notelist but its possible to set a duration override for playback for a single note.
        #Achtung. Take care of the correct order of duration overrides. There is one for the whole chord as well.
        self._state = {} #For undo. Not saved.

    def __lt__(self, other):
        #<=
        """Notes are smaller if the pitch is smaller."""
        return self.pitch <= other.pitch

    @property
    def pitch(self):
        return self._pitch

    @pitch.setter
    def pitch(self, value):
        """Protect the note pitch against going over the boundaries"""
        if value >= 3120:
            value = 3120
        elif value <= 20:
            value = 20
        self._pitch = value
        return self._pitch

    @property
    def durationScale(self):
        """Return a factor which can be multiplied with a tick value"""
        return 2 ** self.durationOverride

    def generateFinger(self):
        if self.finger:
            if self.finger.find('|') >= 1:
                finger = re.sub(r'(\d+)', r'"\1"', self.finger)
                finger = re.sub(r'(t|\\thumb)', r'#:concat (#:raise 0.5 #:fontsize 3 #:musicglyph "scripts.thumb")', finger)
                L = '"-"'.join([finger for finger in finger.split("|")])
                return '-\\tweak #\'text #(markup ' + L + ' )-0'   #  -\tweak #'text #(markup #:concat (#:raise 0.5 #:fontsize 3 #:musicglyph "scripts.thumb")  " - " "4")-0
            else:
                finger = ''.join( self.finger.split() )
                if finger == "t" or finger == "\\thumb":
                    return "\\thumb"  #<c \\thumb>
                else:
                    return "-" + finger  #plain simple <c -3>
        else:
            return ""

    def generateTie(self):
        """This is just for the lilypond output.
        Tie could also be "\\laissezVibrer" which is an indication
        for midi and handled in Chord()"""
        if self.tie == "~":
            return "~"
        else:
            return ""

    """
    def noteheadTweak(self, chordBaseDuration):
        tweak = ""
        if self.durationOverride:
            log = str(playback.ticksToLyDurationLog(chordBaseDuration) - self.durationOverride)
            tweak = "\\tweak #'duration-log #"  + log + " "
        return tweak
    """

    def generateLilypond(self):
        return lilypond.pitch2ly[self.pitch] + self.generateTie() + " ".join(self.directiveMid.values()) + self.generateFinger()

    def octaveUp(self, keysig):
        self.pitch = self.pitch + 350 #this is a redundant function. But its faster than the generic interval function

    def octaveDown(self, keysig):
        self.pitch = self.pitch - 350 #this is a redundant function. But its faster than the generic interval function

    def intervalUp(self, keysig, interval, octave):
        self.pitch = pitch.intervalUp(self.pitch, (interval, octave))

    def intervalDown(self, keysig, interval, octave):
        self.pitch = pitch.intervalDown(self.pitch, (interval, octave))

    def intervalAutomatic(self, keysig, rootPitch, targetPitch):
        """Change the notepitch with the same interval that is between
        rootPitch and targetPitch"""
        self.pitch = pitch.intervalAutomatic(self.pitch, rootPitch, targetPitch)

    def toWhite(self, keysig):
        self.pitch = pitch.toWhite(self.pitch)

    def mirror(self, keysig, axis):
        """Wants two pitches"""
        self.pitch = pitch.mirror(self.pitch, axis)
        self.toScale(keysig)

    def toScale(self, keysig):
        self.pitch = pitch.toScale(self.pitch, keysig)

    def scaleStatus(self, keysig):
        return pitch.scaleStatus(self.pitch, keysig)

    def stepUpInScale(self, keysig):
        self.toScale(keysig) #adjust to keysig the first time to prevent bisis and eisis to step over the pillar of fifth
        self.intervalUp(keysig, 2,0) #two fifths, one whole tone.
        self.toScale(keysig) #adjust to keysig.

    def stepDownInScale(self, keysig):
        self.intervalDown(keysig, 2,0) #two fifths, one whole tone.
        self.toScale(keysig) #adjust to keysig.

    def enharmonicSharp(self, keysig):
        self.pitch = pitch.enharmonicSharp(self.pitch)

    def enharmonicFlat(self, keysig):
        self.pitch = pitch.enharmonicFlat(self.pitch)

    def sharpen(self, keysig):
        self.pitch = pitch.sharpen(self.pitch)

    def flat(self, keysig):
        self.pitch = pitch.flatten(self.pitch)

    #Achtung: Remember to duplicate anything in EmptyNote

    def saveState(self):
        self._state = copy.copy(self.__dict__)

    def undo(self):
        if self._state:
            self.__dict__ = self._state
        else:
            raise RuntimeError("Note: Undo beyond init state. Not possible")

    def exportSave(self):
        childDict = {}
        #Playback
        childDict["split"] = self.split
        childDict["_durationFactor"] = self.durationFactor
        childDict["_velocityFactor"] = self._velocityFactor
        childDict["instructionPre"] = self.instructionPre
        childDict["instructionPst"] = self.instructionPst
        childDict["channelOffset"] = self.channelOffset
        childDict["forceChannel"] = self.forceChannel
        childDict["programOffset"] = self.programOffset
        childDict["forceProgram"] = self.forceProgram

        #Lilypond
        childDict["lilypond"] = self.lilypond
        childDict["directivePre"] = self.directivePre
        childDict["directiveMid"] = self.directiveMid
        childDict["directivePst"] = self.directivePst
        #Note
        childDict["pitch"] = self.pitch
        childDict["finger"] = self.finger
        childDict["tie"] = self.tie
        childDict["durationOverride"] = self.durationOverride
        return (type(self).__name__, childDict)

class EmptyNote(Note):
    """A fake note that any non-chord object has to prevent errors if
    a keysig is asked toScale. Prevents lots of typechecking."""
    def __init__(self):
        super(EmptyNote, self).__init__(0)
        self._pitch = 0 #does not matter.  But should be False all the time.
        self.directivePre = EmptyDict()
        self.directiveMid = EmptyDict()
        self.directivePst = EmptyDict()
        self._state = {} #Placeholder For undo. Not saved, not modified. Doesn't matter.

    #Fake methods to prevent typechecking by higher level functions
    def nothing(self, *arg):
        return False
    saveState = undo = generateFinger = scaleStatus = generateLilypond = octaveUp = octaveDown = intervalUp = intervalDown = toWhite = toScale = stepUpInScale = stepDownInScale = enharmonicSharp = enharmonicFlat = sharpen = flatten = nothing

class Item(Internal, Playback, Lilypond):
    """The basic object. Anything in a score is an Item and can be under the cursor position.
        Used for standalone objects like tuplet markers and lilypond \override also.
        Important: Any method that is in any derived sub-class has to defined here with "pass" as well to avoid typechecking in the Gui or scripts!
        Example: Even if a Key Signature has no logical method 'Delete note from chord' it still has one technically so using DelFromChord does not raise an exception but silently does nothing.
        Regarding instance variables: The gui should never """ #What!!?? I forgot. What should it never do? A big mystery...

    def __init__(self, parentWorkspace):
        super(Item, self).__init__()
        self.selected = False #Currently selected?
        self.uniqueName = str(uuid.uuid1())  #There are cases where this may be not unique. For example generation after loading or copy and paste from another file. Unlikely but not zero. If you need to make sure this item is exactly "the one" then save the instance to compare to.
        self._chordsymbols = "" #Prevents typechecking. Used in Chord().
        self._figures = "" #Prevents typechecking. Used in figures().
        self.tempoKeyword = "" #Possible are lilypond strings as keywords: shortfermata, fermata, longfermata, verylongfermata, breathmark etc. Anything that goes in here needs an entry in PerformanceSignature . This is only for smf export. The graphic is done in the api.
        #self.cursorWalk = False #This is redundant but it is much easier to create container which are not iterable.
        self.playbackCounter = 0 #This will be set dynamically by unfoldScore, the playback pre processor. It becomes 1 before the user or a playback trigger can intercept it. So "1" is the first encounter.
        self._state = {} #For undo. Not saved.
        self.dots = 0
        self._lookaheadMarkers = {} #during unfold various data can be added here like "this is the end of a slur and not legato. not saved
        self.parentWorkspace = parentWorkspace #while it is a true that an item can be in any number of containers and tracks it is only in one workspace

    def newName(self):
        """Generate a new self.uniqueName for this item.
        Needed when duplicating, copy and paste etc."""
        self.uniqueName = str(uuid.uuid1())

    @property
    def chordsymbols(self):
        """this returns "" and can't be set"""
        return ""
    @property
    def figures(self):
        """this returns "" and can't be set"""
        return ""

    @chordsymbols.setter
    def chordsymbols(self, value):
        """this returns "" and can't be set"""
        self._chordsymbols = value
        return True
    @figures.setter
    def figures(self, value):
        """this returns "" and can't be set"""
        return True

    def makeSwing(self):
        """Typechecking avoided once more"""
        pass

    def select(self, registerInTrackSelectedItemsList = True):
        if not self.selected:
            self.selected = True
            if registerInTrackSelectedItemsList:
                self.parentWorkspace.score.currentTrack.selectedItems.append(self)
            self.parentWorkspace.score.selection += 1 #increase the selection counter
            return True
        else:
            return False

    def deselect(self, deleteMe=True):
        if self.selected:
            self.selected = False
            if deleteMe: #only for the normal manual command. Score-Deselect deletes the list itself.
                self.parentWorkspace.score.currentTrack.selectedItems.remove(self)
                self.parentWorkspace.score.selection -= 1 #decrease the selection counter
            return True
        else:
            return False

    def toggleSelect(self):
        """This is only for very special functions. Use the api
        function if you can"""
        if self.selected:
            self.deselect()
            return False
        else:
            self.select()
            return True

    @property
    def duration(self):
        """Complete Musical Duration of this item. Used to calculate sync and playback"""
        #Ticks with dots x = basic note value , n = number of dots: 2x - x/2^n
        def tupletify(self, base):
            """Multiply with all tuplets from a list"""
            dur = base
            for x in self.tuplets:
                dur = dur * x[0] / x[1]
            return dur
        with_dots = 2 * self.base - self.base / 2**self.dots
        with_scaleFactor = with_dots * (self.scaleFactorNumerator / self.scaleFactorDenominator)
        return tupletify(self, with_scaleFactor)

    @property
    def chordsymbols(self):
        """In all other item classes this returns "" and can't be set"""
        return ""

    @property
    def figures(self):
        """In all other item classes this returns "" and can't be set"""
        return ""

    @chordsymbols.setter
    def chordsymbols(self, value):
        """In all other item classes this returns "" and can't be set"""
        return True
    @figures.setter
    def figures(self, value):
        """In all other item classes this returns "" and can't be set"""
        return True

    def generateLilypond(self):
        return self.lilypond

    def generateLyDuration(self, tremolo=False): #TODO: Why is this in Item and not Lilypond? Because it needs all those self.base, self.parameter which are Item. Alternative is a function of class Lilypond which takes those as parameters.
        def generateLyScaleString():
            if self.scaleFactorDenominator != 1 or self.scaleFactorNumerator != 1:
                return "*" + str(self.scaleFactorNumerator) + "/" + str(self.scaleFactorDenominator)
            else:
                return ""

        def generateLyTremoloString():
            """<c'>2:8  means a half note with one diagonal line.
            16 is two etc.
            So Lilypond really doesn't care about what is meant here
            only how it should look like. It does not allow
            8:8 and it does not allow 1:4, but it allows 8:16.
            Laborejo however allows any split value.
            In this case we do a little trick and write the note out.""" #TODO. Then do it.
            #Half notes 768 with one line is 2:8  or 768:8 for duration. These are 4 notes, and that is the value Laborejo knows.  768/4 = 192 . And dur2ly(192) is 8.
            #Not working. Whole note as 4 quarter notes. 1536/4 = 384
            if self.split > 1:
                lilysplit = self.duration / self.split
                if lilysplit in [192, 96, 48, 24, 12, 6]:
                    return ":" + lilypond.dur2ly[lilysplit]
                else:
                    #not really an error. Just dont export the split
                    warnings.warn(str(["Error: The Split value cannot be exported to Lilypond. Programming work needs to be done here. Duration:", self.duration, "Split:", self.split, "Calculation", lilysplit, "is not one of", [192, 96, 48, 24, 12, 6]]))
                    return ""
            else:
                return ""

        if not self.base in lilypond.dur2ly:
            raise ValueError ("Item has no compatible Lilypond base duration.", self, self.base, " ".join([x.exportLilypond() for x in self.notelist]))

        if tremolo:
            trem = generateLyTremoloString()
        else:
            trem = ""
        return lilypond.dur2ly[self.base] + "." * self.dots + generateLyScaleString() + trem

    def saveState(self):
        for note in self.notelist:
            note.saveState()
        #self._state = copy.copy(self.__dict__)
        self._state = copy.deepcopy(self.__dict__)
        #self._state = self.__dict__.copy()
        #self._state["notelist"] = copy.copy(self.notelist) #Make an extra copy of the notelist because it is a mutable list.
#       self._state["notelist"] = self.notelist[:] #Make an extra copy of the notelist because it is a mutable list.

    def undo(self):
        if self._state:
            self.__dict__ = self._state
            for note in self.notelist:
                note.undo()
        else:
            #This only happens if the api.undo screws up because its undo list is wrong.
            raise RuntimeError("Item: Undo beyond init state. Not possible")


    def generateSave(self):
        """If in doubt return nothing, still compatible.
        Child classes should implement this function and return a dict"""
        return {}

    def exportSave(self):
        """Generate the dict with whatever the child class gives us
        then add the Internal Values (durations) and return as tuplet
        (ClassName, childDict) which will be exported to Json in the end"""
        childDict =  self.generateSave()
        #Internal
        childDict["base"] = self.base
        childDict["tuplets"] = self.tuplets
        childDict["dots"] = self.dots
        childDict["scaleFactorNumerator"] = self.scaleFactorNumerator
        childDict["scaleFactorDenominator"] = self.scaleFactorDenominator
        #Playback
        childDict["split"] = self.split
        childDict["_durationFactor"] = self.durationFactor
        childDict["_velocityFactor"] = self._velocityFactor
        childDict["instructionPre"] = self.instructionPre
        childDict["instructionPst"] = self.instructionPst
        childDict["channelOffset"] = self.channelOffset
        childDict["forceChannel"] = self.forceChannel
        childDict["programOffset"] = self.programOffset
        childDict["forceProgram"] = self.forceProgram
        childDict["smfAddDuration"] = self.smfAddDuration

        #Lilypond
        childDict["lilypond"] = self.lilypond
        childDict["directivePre"] = self.directivePre
        childDict["directiveMid"] = self.directiveMid
        childDict["directivePst"] = self.directivePst
        childDict["transparent"] = self.transparent
        childDict["hidden"] = self.hidden
        #Item
        #childDict["selected"] = self.selected  Don't save selection!

        childDict["instanceCount"] = self.instanceCount

        exportList = []
        for item in self.notelist:
            exportList.append(item.exportSave())

        childDict["notelist"] = exportList
        childDict["uniqueName"] = self.uniqueName
        childDict["_chordsymbols"] = self._chordsymbols
        childDict["_figures"] = self._figures
        childDict["tempoKeyword"] = self.tempoKeyword
        childDict["velocityKeyword"] = self.velocityKeyword
        childDict["_triggerString"] = self.triggerString
        return (type(self).__name__, childDict)

    def generatePlayback(self, smfTrack, channel, transpose, jackMode, smf):
        """This is the function that needs a custom version in each
        child class.
        Overwrite with something that returns a list of tuples.
        The tuplets are data-sets: ([0x90, 60, 127], tick-offset).
        Please ignore transpose if you are not a chord."""
        return [] #do nothing by default.

    def playbackTrigger(self):
        """You can add, change and use any self. variable here.
        Playback works on copies, so everything gets reverted to the
        intitial and saved value afterwards"""
        s = compile("ret=True\n" + self.triggerString, '<string>', 'exec')
        exec(s, {}, locals())
        return locals()["ret"]

    def generatePlaybackTemporaryChanges(self, jackMode):
        """Return two lists of smf messages. One to execute before the
        item and one to execute after the item.
        Execution of the smf message is left to the calling function."""
        switchOn = []
        switchOff = []
        if self.duration: #create temporary values like tempo and program and other CCs.
            if self.tempoKeyword: #Any item can have a fermata, breathmark etc. but only one. But they only make sense if the item has a duration. Else it would be just a tempo change which gets reverted immediatly.
                tempo = self.parentWorkspace.score.activeCursor.prevailingTempoSignature[-1]

                mod = getattr(self.parentWorkspace.score.activeCursor.prevailingPerformanceSignature[-1], self.tempoKeyword)
                tempoMod = playback.midiTempo(tempo.beatsPerMinute*mod  , tempo.referenceTicks) #get a new tempo, based on the current bpm but modified by the fermata modificator, defined by the current PerformanceSignature

                #Org needs the tick offset because it comes later in time
                tempoOrg = playback.midiTempo(tempo.beatsPerMinute  , tempo.referenceTicks) #original tempo
                tempoOrg = (tempoOrg[0], tempoOrg[1] + self.duration - self.overlapAvoider) #create a new tuple based on the old, only with tick offset.

                switchOn.append(tempoMod)
                switchOff.append(tempoOrg)

            #Program offset before and after.
            if jackMode:
                if 0 <= self.forceProgram <= 127:
                    base = self.forceProgram
                else:
                    base = self.parentWorkspace.score.activeCursor.prevailingJackPatch[-1]

                if self.programOffset or 0 <= self.forceProgram <= 127:
                    #program offest is 0 by default.
                    switchOn.append(([0xC0, base + self.programOffset], 0))
                    switchOff.append(([0xC0, self.parentWorkspace.score.activeCursor.prevailingJackPatch[-1]], self.duration - self.overlapAvoider))

        return switchOn, switchOff


    def exportPlayback(self, smfTrack, channel, transpose, jackMode, smf):
        """#Achtung! Appending, ChannelChangeRelative,
        ChannelChangeAbsolute have their own exportPlayback."""

        self.makeSwing() #in place. We work on a copy, no problem.

        switchOn, switchOff = self.generatePlaybackTemporaryChanges(jackMode) #returns two lists of midi messages
        #create temporary values like tempo and program and other CCs which are not midiPre and midiPst
        for on_message in switchOn:
            self.executeSmf(smfTrack, channel, on_message, jackMode, smf)

        self.executeInstructionPre(smfTrack, channel, jackMode, smf)
        if self.playback:
            smfEvent = copy.deepcopy(self.playback)
            smfEvent[0] += self.getSmfChannel(channel, jackMode)
            smfTrack.add_event(smf.Event(smfEvent), pulses=self.parentWorkspace.score.activeCursor.tickindex)
        if self.playback is None:
            pass
        else:
            self.executeGenerated(smfTrack, channel, transpose, jackMode, smf)
        self.executeInstructionPst(smfTrack, channel, jackMode, smf)

        #reset temporary values like tempo and program and other CCs which are not midiPre and midiPst
        for off_message in switchOff:
            self.executeSmf(smfTrack, channel, off_message, jackMode, smf)

        self.parentWorkspace.score.activeCursor.tickindex += self.smfAddDuration #this gets reverted after playback generation has ended.
        if self.parentWorkspace.score.activeCursor.tickindex + self.duration < 0:
            raise ValueError("Tickindex", self.parentWorkspace.score.activeCursor.tickindex, "cannot go below 0. Consider an extra measure at the beginning. This is Track", self.parentWorkspace.score.currentTrack.uniqueContainerName)

        return (smfTrack, channel, transpose, jackMode)
        #return self.parentWorkspace.score.activeCursor.tickindex + self.duration ##????? what is this? It switched it to "return False" and "return 'lol'" and nothing happened. I remember that I used to return the tickindex for the next object, but apparently that is not the case here. score.exportPlayback does not make anything with this return value and it is the only place that calls item.exportPlayback

class WaitForChord(Item):
    """A slightly different Item which exports lilypond not directly but
    waits for the next chord. Used for dynamics and others."""
    def __init__(self, parentWorkspace,  lilystring = "", pre = False):
        super(WaitForChord, self).__init__(parentWorkspace)
        self.lilystring = lilystring
        self.pre = pre
    def generateLilypond(self):
        if self.lilystring:
            if self.pre:
                self.parentWorkspace.waitForNextChordPre.append(self.lilystring)
            else: #post
                self.parentWorkspace.waitForNextChordPst.append(self.lilystring)
        return ""
    def generateSave(self):
        return {"lilystring":self.lilystring,}

class Chord(Item):
    """Holds n notes.
    The notelist is always sorted as it gets sorted after insert"""
    def __init__(self, parentWorkspace, base, pitch):
        super(Chord, self).__init__(parentWorkspace = parentWorkspace)
        if type(pitch) is list:
            self.notelist = [Note(p) for p in pitch]
        else:
            self.notelist = [Note(pitch)] # Only chords with at least one pitchlist item are allowed. So you need to start with one and if you delete the last, the chord deletes itself.
        self.base = base
        #Substitution: A string which leads to a dict with containers. If existent it will replace the chord completely but it will have the same overall-duration and use the existing chord pitch(es) to modify itself, e.g. transpose.
        #Substitutions are meant as Lilypond and Midishortcuts with a fixed pattern. This is one level above effects such as tremolo/split duration.
        #Not making the container transparent to the cursor is the core parameter of a substitution. It will be handled just like a note which can be shifted, augmented etc.
        self.lyGrob = True

    @property
    def split(self):
        """Chord() actually returns _slit"""
        return self._split

    @split.setter
    def split(self, value):
        self._split = value

    def getHiddenPrefix(self):
        """A special hide/transparent one that differs from the
        Lilypond parent class method.
        Chords have too many elements so they have a special."""
        if self.hidden or self.transparent:
            return "\\once \\hideNotes "
        else:
            return ""

    def generateFiguredBass(self):
        if self.figures:
            workCopy = self.figures
            on = off = ""
            if "@" in workCopy:
                workCopy = workCopy.replace("@", "")
                on = " \\bassFigureExtendersOn "
                off = " \\bassFigureExtendersOff "
            workCopy = workCopy.split("|") #returns a list.
            dur = self.generateLyDuration()
            scale = " * 1" + "/" + str() if len(workCopy) > 1 else ""
            #replace ? with vertical spaces. ? itself is not allowed in lilypond figured bass so it is a safe char.
            return " ".join(["<" + figure.replace("?", " \\markup \\vspace #.55 ") + ">" + dur + scale + on for figure in workCopy]) + off
            #return " ".join(["<" + figure + ">" + dur + scale + on for figure in workCopy]) + off
        else:
            return "<_>" + self.generateLyDuration()

    def generateChordsymbols(self):
        def insDur(chord, partCount):
            """insert the duration before the : or after the string if
            no :.
            Returns the whole chordstring with inserted duration"""
            scale = "*1" + "/" + str(partCount) if partCount > 1 else ""
            if ":" in chord:
                chord = chord.replace(":", self.generateLyDuration() + scale + ":")
            else:
                chord += self.generateLyDuration() + scale
            return chord.lower()

        if self.chordsymbols:
            workCopy = self.chordsymbols.split("|") #returns a list.
            partCount = len(workCopy)
            return " ".join([insDur(chord, partCount) for chord in workCopy])
        else:
            return "s" + self.generateLyDuration()

    def generateLilypond(self):
        """Export the notelist as chord. If substitution present
        override the chord with the container. All parameters like
        chordsymbol will be attached to the first container-item.
        Substitutions are only exported to lilypond if their container
        is unfold == True"""
        if self.substitution and ((self.substitution in self.parentWorkspace.score.substitutions and self.parentWorkspace.score.substitutions[self.substitution].unfold ) or (self.substitution in self.parentWorkspace.substitutiondatabase and self.parentWorkspace.substitutiondatabase[self.substitution].unfold)):
            scale = pitch.pitchlistToScaleAsIntervals([x.pitch for x in self.notelist])
            if self.substitution in self.parentWorkspace.score.substitutions:  #User-Dict of substitutions:
                substCont = self.parentWorkspace.score.substitutions[self.substitution]
            else: #Global dict of substitutions.
                substCont = self.parentWorkspace.substitutiondatabase[self.substitution]

            #This is the place to copy the host parameters like hidden or transparent into the substitution.
            #We keep it under control by choosing manually and not blindly copying self.__dict__ or so
            paramDict = {
                "hidden" : self.hidden,
                "transparent" : self.transparent,
                "directivePre" : self.directivePre,
                "directiveMid" : self.directiveMid,
                "directivePst" : self.directivePst,
            }

            return substCont.transform(keysig = self.parentWorkspace.score.activeCursor.prevailingKeySignature[-1], referenceBaseDuration = self.duration, rootpitch = self.notelist[0].pitch, scale = scale, parameterDict = paramDict).generateSubstitutionLilypond()

        else: #normal chord and also chords inside a substitution.
            waitingStringsPre = "" # this is used for dynamics etc. Logically and internally
            if self.parentWorkspace.waitForNextChordPre:
                waitingStringsPre = " ".join(self.parentWorkspace.waitForNextChordPre)
                self.parentWorkspace.waitForNextChordPre = []

            waitingStrings = "" # this is used for dynamics etc. Logically and internally
            if self.parentWorkspace.waitForNextChordPst:
                waitingStrings = " ".join(self.parentWorkspace.waitForNextChordPst)
                self.parentWorkspace.waitForNextChordPst = []

            #Prepare the substrings for the lilypond chord syntax.
            notes = " ".join([note.exportLilypond() for note in self.notelist])
            dirMid = " ".join(self.directiveMid.values())
            duraString = self.generateLyDuration(tremolo=True)

            #Too many problems with the following code. This should be done by a external Lilypond Beautifyer tool. Laborejo is only for a correct export. #TODO
            """
            #The durationstring is only needed in Lilypond when it changes. Keep track of the last one and only print it out if new.
            if duraString == self.parentWorkspace.lastDurationLyString:
                duraString = ""
            else:
                self.parentWorkspace.lastDurationLyString = duraString

            #Build the actual Lilypond Chord/Note. One note alone does not need the <chord> syntax.
            if len(self.notelist) == 1 and not self.notelist[0].tie:
                return notes + dirMid + duraString + waitingStrings
            else:
            """
            return waitingStringsPre + " " + "<" + notes + ">" + dirMid + duraString + waitingStrings

    @property
    def chordsymbols(self):
        """In all other item classes this returns "" and can't be set"""
        return self._chordsymbols
    @property
    def figures(self):
        """In all other item classes this returns "" and can't be set"""
        return self._figures

    @chordsymbols.setter
    def chordsymbols(self, value):
        """In all other item classes this returns "" and can't be set"""
        self._chordsymbols = value
        return True
    @figures.setter
    def figures(self, value):
        """In all other item classes this returns "" and can't be set"""
        self._figures = value
        return True

    def addNote(self, pitch):
        self.notelist.append(Note(pitch))
        self.notelist.sort(key=operator.attrgetter('pitch')) #sort after insert.
        return True

    def deleteNote(self, noteObject):
        self.notelist.remove(noteObject) #This should delete the exact item, an instance. If the same instance is twice in the notelist it only deletes one. No search for the pitch or crap like "highest first".
        return True

    def applyNoteMethodToChord(self, method, keysig, *args):
        for x in self.notelist:
            getattr(x, method)(keysig, *args)
        return True

    def augment(self, factor=2):
        self.base = self.base * factor
        return True

    def diminish(self, factor=2):
        self.base = self.base / factor
        return True

    def addDot(self):
        self.dots += 1

    def removeDot(self):
        if self.dots: #not for dots == 0
            self.dots -= 1

    def generateSave(self):
        """Notelist is already in Item, base also.
        Nothing to do here. But keep the function for later and
        documentation reasons."""
        return {"substitution":self.substitution,}


    def makeSwing(self):
        """
        Only Chord and derived instances, like Rest, have this.
        Upbeat has its own variant.

        Modify this note in place for swing. Naturally this
        is only for the exporting stage where we work on a copy

        swing mod is a value around 0, float.
        value must be -3 < value < 3
        Above and below are errors and avoided by the program.

        0 is off. 1/2 + 1/2 for a note pair to the rhythm layer above.
        1 is the strict swing  2/3 + 1/3
        -1 is reversed string. 1/3 + 2/3

        You can make it more extreme or laid back by going between
        these numbers. Recommended is from 0.75 to 1.25
        -3 is both notes on the same time
        and 3 is something to not care about. These values make no
        musical sense, but are technically possible.
        """
        swingMod = self.parentWorkspace.score.activeCursor.prevailingPerformanceSignature[-1].swingMod
        if -3 < swingMod < 3 :# and self.parentWorkspace.score.activeCursor.prevailingPerformanceSignature[-1]: #only for notes and rests. and swing? #TODO
            swingLayer = self.parentWorkspace.score.activeCursor.prevailingPerformanceSignature[-1].swingLayer
            modulo = 2 / (self.parentWorkspace.score.activeCursor.prevailingTimeSignature[-1].denominator / swingLayer)
            decimals = self.cachedMetricalPosition % modulo #starting metrical position.

            if self.duration == swingLayer:   #(self.duration / swingLayer) % 2 != 0:
                if decimals == 0:
                    self._scaleFactorNumerator = 3 + swingMod #We don't use the public variable because that does reset the cursors. We don't want that here.
                    self._scaleFactorDenominator = 3 #We don't use the public variable because that does reset the cursors. We don't want that here.
                #elif decimals == 0.5:
                else:
                    self._scaleFactorNumerator = 3 - swingMod #We don't use the public variable because that does reset the cursors. We don't want that here.
                    self._scaleFactorDenominator = 3 #We don't use the public variable because that does reset the cursors. We don't want that here.
            elif self.duration == swingLayer * 3:
                if decimals == 0:
                    self._scaleFactorNumerator = 9 + swingMod #We don't use the public variable because that does reset the cursors. We don't want that here.
                    self._scaleFactorDenominator = 9 #We don't use the public variable because that does reset the cursors. We don't want that here.
                #elif decimals == 0.5:
                else:
                    self._scaleFactorNumerator = 9 - swingMod #We don't use the public variable because that does reset the cursors. We don't want that here.
                    self._scaleFactorDenominator = 9 #We don't use the public variable because that does reset the cursors. We don't want that here.

    def generatePlayback(self, smfTrack, channel, transpose, jackMode, smf):
        """Returns a list of tuplets.
        tuplet[0] is a midi data-set list:  [0x90, 60, 127]
        tuplet[1] is a tick offset. Mostly for note-off.

        Staccato, Tenuto etc. are saved as lilypond post directives
        with a keyword as dict-key. We send our post dict and receive
        all modifications, already combined and sorted
        (tenuto tops staccato etc.)
        """
        activeCursor = self.parentWorkspace.score.activeCursor
        retlist = []
        splitted = self.duration / self.split #duration 384, split 4 = 96. Offset pattern:  0-96, 96-192, 192-288, 288-384. Which is (position * x) for note on and (position * x + x), counting positions from 0. -> range()

        #Velocity Modifications except explicit user modification which is done in the note loop below
        #Velocity keywords have highest priority, then subitoDynamicKeywords, then surrounding dynamic signatures.
        if self.velocityKeyword:
            velocity = activeCursor.prevailingPerformanceSignature[-1].dynamics[self.velocityKeyword] #one-time override over the current dyanmic signature. sfz, sfp etc.
        elif self.subitoDynamicKeyword:
            velocity = activeCursor.prevailingPerformanceSignature[-1].dynamics[self.subitoDynamicKeyword] #one-time override over the current dyanmic signature. sfz, sfp etc.
        else:
            velocity = activeCursor.prevailingDynamicSignature[-1].expression #the current dynamic signature
        velocity *= self.velocityFactor

        #Duration Modifications except explicit user scaling which is done in the note loop below.
        noteOffMod = 1
        if "slurEnd" in self._lookaheadMarkers:
            noteOffMod *= activeCursor.prevailingPerformanceSignature[-1].slurEndScaling
        elif activeCursor.legato:
            noteOffMod *= activeCursor.prevailingPerformanceSignature[-1].legatoScaling
        else:
            noteOffMod *= activeCursor.prevailingPerformanceSignature[-1].performanceDuration(self.base)

        #As last step send the individual noteOffMod factor to the performance sig which may add a factor or even override the whole thing (e.g. Tenuto)
        noteOffMod = activeCursor.prevailingPerformanceSignature[-1].durationKeywordFactor(noteOffMod, self.directivePst)

        #Now process each note individually
        for x in range(self.split):
            for note in self.notelist:

                #Add explicit velocity modifications. These are the last steps and beat even velocit keywords like an accent.
                noteVelocity = int(velocity * note.velocityFactor)

                #Respect midi boundaries and silently transform
                if noteVelocity > 127:
                    noteVelocity = 127
                elif noteVelocity < 0:
                    noteVelocity = 0

                #Add explicit user duration factors to the noteOffMod.
                #These are the last step and even beat Tenuto.
                noteOffMod *= self.durationFactor * note.durationFactor

                if noteOffMod <= 0:
                    raise ValueError("Chord with duration 0 or negative. Not possible. Track/Position:", activeCursor.nested_list.uniqueContainerName, "/", activeCursor.flatCursorIndex, "Chord:", self, self.exportLilypond(),)

                #ties prevent noteOn and noteOff
                midipitch = pitch.toMidi(note.pitch)
                if not midipitch in activeCursor.noteTies: #tie pending: if the entry this pitch is not empty
                    #this is a new, not previously-tied note
                    retlist.append(([0x90, midipitch + transpose, velocity], x * splitted)) #noteon
                if note.tie:
                    activeCursor.noteTies[midipitch] = True  #tie from gis to aes.
                else: #this is not tied anymore. Send the note off.
                    retlist.append(([0x80, midipitch + transpose, velocity], x * splitted + splitted * noteOffMod  - self.overlapAvoider)) #noteoff with tick offset.
                    if midipitch in activeCursor.noteTies:
                        del activeCursor.noteTies[midipitch]
        #return this to exportPlayback where it gets executed and added to the smf data immediatly. It doesn't matter if we are in a substitution loop or a plain note
        return retlist


    def unfoldSubstitution(self, keysig, flatCursorIndex = None):
        """return a flat list of items. No start, end or appending.
        If there is no substitution or the substitution is not existend
        return a list with the original chord instead.

        flatCursorIndex is an optional argument which allows various
        exports and unfolding operations to bake a flatCursorIndex
        into the returned items.
        This is only for functions that deal with copies of this item,
        not original ones! e.g. score.unfold()

        This is not playback export but it deals with Laborejo objects.

        This is intended for unfoldScore and subsequently
        exportPlayback. Items are not unique, they share the same ID
        and are not fit to be edited, saved or anything else without
        further processing.

        The unfolding for lilypond happens in self.generateLilypond

        If you intend permanent unfolding consider that this is
        recursive and nested substitution will be unfolded without
        question. """ #TODO add a "unfold nested?" switch so this function can become the basis for destructive unfolding.
        parametersToCopy = ["directivePst", "_split", "_durationFactor", "_velocityFactor", "instructionPre", "instructionPst", "channelOffset", "forceChannel", "velocityKeyword",  "programOffset", "forceProgram", "tempoKeyword",] #directivePst is needed for staccato and other keywords that are checked by the performance signature
        if self.substitution and (self.substitution in self.parentWorkspace.score.substitutions or self.substitution in self.parentWorkspace.substitutiondatabase):
            scale = pitch.pitchlistToScaleAsIntervals([x.pitch for x in self.notelist]) #pitch. is the module

            #create substCont
            if self.substitution in self.parentWorkspace.score.substitutions: #User substitutions in score
                #substCont = self.parentWorkspace.score.substitutions[self.substitution].transform(referenceBaseDuration = self.base, rootpitch = self.notelist[0].pitch, scale = scale)
                substCont = self.parentWorkspace.score.substitutions[self.substitution]
            elif self.substitution in self.parentWorkspace.substitutiondatabase: #Global dict of substitutions.
                substCont = self.parentWorkspace.substitutiondatabase[self.substitution]
            substCont = substCont.transform(keysig = keysig, referenceBaseDuration = self.duration, rootpitch = self.notelist[0].pitch, scale = scale)

            #gather all items in the container on this level. when encountering another substitution and wait for that list
            gatherList = []
            for substObj in substCont:
                if substObj.substitution:
                    gatherList += substObj.unfoldSubstitution()
                else: #A normal object
                    if flatCursorIndex:#else don't touch it.
                        substObj.flatCursorIndex = flatCursorIndex

                    #Here is the place to transfer host-note parameters,
                    #effects, scalings etc. to the substitution note.
                    #To remain in control we add those things manually.
                    for para in parametersToCopy:
                        setattr(substObj, para, getattr(self, para))

                    gatherList.append(substObj)

            filtered = list(filter(lambda item: not isinstance(item, Appending), gatherList)) #Especially for Container Start and End
            return filtered
        else: #There was a substitution string but it is not in one of the subst-storages. Just return the Chord itself.
            warnings.warn("Substitution " + self.substitution + " not found. Object ", self, self.exportLilypond(), " requested it OR Chord.unfoldSubstitution() was requested but there is none.")
            return [self]

class Ornament(Item):
    """Grace Notes, Vorhalt, Ornaments etc.
    A chord without layout duration which also interacts with the chord
    previous or after by substracting duration from them.

    But an Ornament has a logcal duration which is playback and analysis
    So only on Plyaback/UnfoldScore we need the duration in all other
    situations, like cursor score, this counts as 0.

    Allthough it is a chord it cannot have a chordsymbol, figured bass
    or a substitution."""

    #TODO
    #Wieviel stiehlt sie von der nachfolgenote
    #Wie lange dauert sie

    #Eine Gracenote vor der Zeit am Anfang des Stückes ist im Minus


    def __init__(self, parentWorkspace, base, pitch):
        super(Ornament, self).__init__(parentWorkspace = parentWorkspace)
        self.notelist = [Note(pitch)] # Only chords with at least one pitchlist item are allowed. So you need to start with one and if you delete the last, the chord deletes itself.
        self.base = base
        #eventhough the duration is always 0 we still have the complete set of duration parameters like dot etc.
        self.onTheBeat = True #before the beat or on the beat? A quite important distinction, but only for exports


    def getNewDuration(self, obj):
        """Wants a duration object- Modifies the object in place to its
        new duration"""
        return 1

    playbackDuration = Item.duration

    @property
    def duration(self):
        return 0

class MicroVoice(Item):
    """Local Voice. Like a chord, but independent Notes. Chordnotes can have different durations and stem directions.
    Furthermore chordnotes here are Chord items, not Note items
    Export to Lilypond is the same as a tuplet: merged on export. Internaly each MicroVoice "Chord" is stand alone."""
    pass
    #TODO: Do.

class Appending(Item):
    """A very simple cousin of class Item. Its simply a placeholder
    to have something at the end of a container for the cursor to insert
    before. The main goal of Appending is to be invisible as possible.
    The Texteditor equivalent is the blank cursor position at the end
    of a line."""
    def __init__(self, parentWorkspace):
        super(Appending, self).__init__(parentWorkspace = parentWorkspace)
        self.directivePre = EmptyDict()
        self.directiveMid = EmptyDict()
        self.directivePst = EmptyDict()

    def generatePlayback(self, smfTrack, channel, transpose, jackMode, smf):
        return [] #do nothing by default.

    def exportPlayback(self, smfTrack, channel, transpose, jackMode, smf):
        return (smfTrack, channel, transpose, jackMode)
        #return self.parentWorkspace.score.activeCursor.tickindex #see item.exportPlayback

    def select(self, registerInTrackSelectedItemsList = False):
        """can't be selected."""
        return False

    def deselect(self, opt = False):
        """can't be deselected. we don't need deselect since appending
        can never be in any deselect list. But it is possible that some
        madman tries to deselect appendin manually."""
        return False

    def exportLilypond(self):
        waitingStringsPre = "" # this is used for dynamics etc. Logically and internally
        if self.parentWorkspace.waitForNextChordPre:
            waitingStringsPre = " ".join(self.parentWorkspace.waitForNextChordPre)
            self.parentWorkspace.waitForNextChordPre = []
        return waitingStringsPre

class Start(Appending):
    """A straight copy of Appending with a different name.
    Used in non-track containers so you can insert both inside and
    outside the container boundaries."""
    def __init__(self, parentWorkspace, parentContainer):
        super(Start, self).__init__(parentWorkspace = parentWorkspace)
        self.parentContainer = parentContainer

class End(Appending):
    """A straight copy of Appending with a different name.
    Used in non-track containers so you can insert both inside and
    outside the container boundaries."""
    def __init__(self, parentWorkspace, parentContainer):
        super(End, self).__init__(parentWorkspace = parentWorkspace)
        self.parentContainer = parentContainer
        self.durationSwitch = True  #this gets switched off during unfold Score. The fake duration is only for the cursor and subsequently possible GUIs

    @property
    def duration(self):
        if self.durationSwitch:
            #return self.parentContainer.duration
            dur = self.parentContainer.duration
            return dur / self.parentContainer.repeatPercent * (self.parentContainer.repeatPercent -1)
        else:
            return 0

class Rest(Chord):
    """Single voiced rest output. Not a multi measure rest"""
    def __init__(self, parentWorkspace, base, pitch):
        super(Rest, self).__init__(parentWorkspace, base, pitch)
        self.notelist = [EmptyNote()]
        self.base = base
        self.pitch = pitch #Not really a pitch but the "s" or "r" symbols
        self.lyGrob = True

    @property
    def split(self):
        """Chord() actually returns _slit"""
        return 1

    @split.setter
    def split(self, value):
        pass


    def generateLilypond(self):
        if self.hidden or self.transparent:
            return "s" + self.generateLyDuration()
        else:
            return self.pitch + self.generateLyDuration()

    #SubClass methods to pass. See docstring.
    def nothing(self, *arg):
        return False

    addNote = deleteNote = applyNoteMethodToChord = nothing

    def generateSave(self):
        return {"pitch":self.pitch,}

    def generatePlayback(self, smfTrack, channel, transpose, jackMode, smf):
        return []

class MultiMeasureRest(Item):
    """A rest which spans over several measures but is one object.
    In other words its just another variant of duration/lilypond output. But here the number is directly a measure number.
    MM Rests are unique kind of objects in this program. They have completely seperated Cursor and Export routines.
    It needs special global var lilypondExportPrevailingTimeSignature which imitates cursor.PrevailingTimeSignature only for Lily Export.
    MM Rests can also be used to output Lilypond "\skip" or "s" skips. Give one optional parameter with one of these lilypond strings."""
    def __init__(self, parentWorkspace, measures, lilysyntax = "R"):
        super(MultiMeasureRest, self).__init__(parentWorkspace)
        self.measures = measures
        self.lilysyntax = lilysyntax
        self.lyGrob = True

    @property
    def duration(self):
        """Complete Musical Duration of this item. Used to calculate sync and playback. Measurenumber * current full measure duration.
        Only for cursor score based calculations! Otherwise there is a high risk that MM rest does not know the correct prevailing TimeSignature"""
        return self.measures * self.parentWorkspace.score.activeCursor.prevailingTimeSignature[-1].nominator * self.parentWorkspace.score.activeCursor.prevailingTimeSignature[-1].denominator

    def generateLilypond(self):
        if self.hidden or self.transparent:
            return "R1*" + str(self.measures) + "*" + str(self.parentWorkspace.lilypondExportPrevailingTimeSignature.nominator) + "/" + lilypond.dur2ly[self.parentWorkspace.lilypondExportPrevailingTimeSignature.denominator]
        else:
            return self.lilysyntax + "1*" + str(self.measures) + "*" + str(self.parentWorkspace.lilypondExportPrevailingTimeSignature.nominator) + "/" + lilypond.dur2ly[self.parentWorkspace.lilypondExportPrevailingTimeSignature.denominator]

    def generateSave(self):
        return {"measures":self.measures, "lilysyntax":self.lilysyntax}

    def augment(self):
        if self.selected:
            self.measures = self.measures * 2
        else:
            self.measures += 1

    def diminish(self):
        if self.selected:
            self.measures = self.measures / 2
        else:
            if self.measures > 1:
                self.measures -= 1

    def generatePlayback(self, smfTrack, channel, transpose, jackMode, smf):
        return []

class Placeholder(Item):
    """This is a hidden MultiMeasureRest. But it skips a bit more space
    in the PDF. It is for printing unfinished work and handwriting in
    the printout"""

    def __init__(self, parentWorkspace, measures):
        super(Placeholder, self).__init__(parentWorkspace)
        self.measures = measures
        self.lilysyntax = "\\skip"

    @property
    def duration(self):
        """Complete Musical Duration of this item. Used to calculate sync and playback. Measurenumber * current full measure duration.
        Only for cursor score based calculations! Otherwise there is a high risk that MM rest does not know the correct prevailing TimeSignature"""
        return self.measures * self.parentWorkspace.score.activeCursor.prevailingTimeSignature[-1].nominator * self.parentWorkspace.score.activeCursor.prevailingTimeSignature[-1].denominator

    def generateLilypond(self):
        return "\n\\set Score.proportionalNotationDuration = #(ly:make-moment 1 8) " +  self.lilysyntax + "1*" + str(self.measures) + "*" + str(self.parentWorkspace.lilypondExportPrevailingTimeSignature.nominator) + "/" + lilypond.dur2ly[self.parentWorkspace.lilypondExportPrevailingTimeSignature.denominator] + " \\unset Score.proportionalNotationDuration\n"

    def generateSave(self):
        return {"measures":self.measures}

    def augment(self):
        if self.selected:
            self.measures = self.measures * 2
        else:
            self.measures += 1

    def diminish(self):
        if self.selected:
            self.measures = self.measures / 2
        else:
            if self.measures > 1:
                self.measures -= 1

class Upbeat(Item):
    """A special duration item which plays back as 0 ticks but internal
    logic calculates with real ticks, and so does Lilypond.
    User needs to manually specify how long the upbeat should be.
    This way the correct tickindex and metrical position
    will be reported."""
    #TODO: There is a small possibility that someone wants to create a tuplet upbeat, followed by a tuplet note. These will be merged to one tuplet currently and will not work. A GUI should better use scaleFactor on the Upbeat.
    def __init__(self, parentWorkspace, base):
        super(Upbeat, self).__init__(parentWorkspace)
        self.base = base

    @property
    def instructionPre(self):
        _instructionPre = {}
        _instructionPre["timesig"] = ([0xFF, 0x58, 0x04, 1, playback.durToMidi(self.duration), 24, 0x08],0) #Before the current object set a small upbeat timesig
        return _instructionPre

    @property
    def instructionPst(self):
        _instructionPst = {}
        _instructionPst["timesig"] = ([0xFF, 0x58, 0x04, self.parentWorkspace.score.activeCursor.prevailingTimeSignature[-1].nominator, playback.durToMidi(self.parentWorkspace.score.activeCursor.prevailingTimeSignature[-1].denominator), 24, 0x08], self.parentWorkspace.score.activeCursor.prevailingTimeSignature[-1].nominator * self.parentWorkspace.score.activeCursor.prevailingTimeSignature[-1].denominator)  #After the upbeat duration reset to the original
        return _instructionPst

    @instructionPre.setter
    def instructionPre(self, value):
        pass
    @instructionPst.setter
    def instructionPst(self, value):
        pass

    @property
    def duration(self):
        """This is only for playback
        since Lilypond export does not use the cursor"""
        fullMeasureTick = self.parentWorkspace.score.activeCursor.prevailingTimeSignature[-1].nominator * self.parentWorkspace.score.activeCursor.prevailingTimeSignature[-1].denominator
        return fullMeasureTick - self.base

    def generateLilypond(self):
        return "\\partial "  + self.generateLyDuration()

    def generatePlayback(self, smfTrack, channel, transpose, jackMode, smf):
        self.parentWorkspace.score.activeCursor.tickindex -= self.duration #remove itself from the tickindex so the playback has no gaps. During midi export this is not the original active cursor anyway, just a copy, so it doesnt matter.
        return []

class Markup(WaitForChord):
    """Puts out Lilypond text. This has no meaning in itself like clef
    and is a rule breach to have only logical music items in core"""
    def __init__(self, parentWorkspace, lilystring):
        super(Markup, self).__init__(parentWorkspace,lilystring)
        self.position = "^" #^ for above or _ for under the staff

    def generateLilypond(self):
        """Generate as rehearsal mark in a master track and as markup
        in a normal track"""
        if self.parentWorkspace.lilypondExportTrackName.lower() == "master":
            return  "\\mark \\markup { " + self.lilystring + " }"
        else:
            string =  self.position + "\\markup { " + self.lilystring +  " }"
            self.parentWorkspace.waitForNextChordPst.append(string)
        return ""

    def generateSave(self):
        return {"lilystring":self.lilystring, "position":self.position,}

    def generatePlayback(self, smfTrack, channel, transpose, jackMode, smf):
        """Export Markup as SMF Text Event"""
        return [([0xFF, 0x01, len(self.lilystring)] + [ord(c) for c in self.lilystring], 0)]

class Clef(Item):
    """This is a break with my own rules: A clef has no musical meaning.
    However it is extremly important to any visualizer. They need to
    typecheck for clefs on inserting items."""
    def __init__(self, parentWorkspace, clefString, octave):
        """Octave can be _8 or _15, ^8 or ^15"""
        super(Clef, self).__init__(parentWorkspace,)
        self.clefString = clefString
        self.octave = octave
        clefDict = {"french": 1420,
                    "treble" : 1420,
                    "treble" : 1420,
                    "violin" : 1420, #?
                    "baritone" : 1420-350,
                    "tenor" : 1420-350,
                    "alto" : 1420-350,
                    "mezzosoprano" : 1420,
                    "soprano" : 1420,
                    "bass" : 1420-350,
                    "varbaritone" : 1420-350,
                    "subbass" : 1420-350,
                    "fakeDrum" : 1420-350,
                    "percussion" : 1420-350,
                    }
        octaveDict = {"": 0,
                    "_8": -350,
                    "_15": -700,
                    "^8": 350,
                    "^15": 700,}
        self.nearestC = clefDict[clefString] + octaveDict[octave]
        self.lyGrob = ["Clef"]

    def isTheSameAs(self, other):
        """Equal"""
        return (self.clefString == other.clefString) and (self.octave == other.octave)

    def generateLilypond(self):
        return "\\clef " + "\"" + self.clefString + self.octave + "\""

    def generateSave(self):
        return {"clefString":self.clefString, "octave":self.octave }

    def generatePlayback(self, smfTrack, channel, transpose, jackMode, smf):
        """Export Celfs as SMF 0xFF, 0x06 marker event.
        There are no midi clefs."""
        return [([0xFF, 0x06, len(self.clefString)] + [ord(c) for c in self.clefString], 0)]

class TimeSignature(Item):
    """Meter information. Nominator is how many notes in one measure and
    denominator is notes of which type in ticks.
    Proper math would be the other way around. :)
    Eventhough Time Signatures are logically only valid at the beginning
    of a measure it is still possible to add them anywhere because
    Lilypond allows it as well."""

    def __init__(self, parentWorkspace, nominator, denominator):
        super(TimeSignature, self).__init__(parentWorkspace)
        self.nominator = nominator   #example: x/384. Upper number
        self.denominator = denominator #example: 4/x. Lower number
        self.lyGrob = ["TimeSignature"]

    def isTheSameAs(self, other):
        """Equal"""
        return (self.nominator == other.nominator) and (self.denominator == other.denominator)
    def __ne__(self, other):
        """Not equal"""
        return (self.nominator != other.nominator) and (self.denominator != other.denominator)


    def generateLilypond(self):
        self.parentWorkspace.lilypondExportPrevailingTimeSignature = self
        return "\\time " + str(self.nominator) + "/" + lilypond.dur2ly[self.denominator]
    @property
    def oneMeasureInTicks(self):
        return self.nominator * self.denominator

    def generateSave(self):
        return {"nominator":self.nominator, "denominator":self.denominator, }


    def generatePlayback(self, smfTrack, channel, transpose, jackMode, smf):
        """The TimeSig uses 255 as first bit. This means it is not for
        a channel but global. executeMidi in Playback checks for this
        case and does not add the channel to this byte.

        This leaves the problem when Laborejo exports multi-staff
        timesigs midi will get problems. Well, who cares.
        The timesig is not critical in midi. I just implemented it
        because I can
        A list of tuplets. In this case only one tuplet."""
        return [([0xFF, 0x58, 0x04, self.nominator, playback.durToMidi(self.denominator), 24, 0x08], 0)]
       #8 32th notes in one quarter note.  #24 = Clicks. "Metro" in midi terms. I am unsure what a metronome setting has to do with a time signature.

class KeySignature(Item):
    """Combination of a root note and accidentals.
    Example: keysig = KeySignature(20, [(0, c), (1, d), (2, e), (3, f), (4, g), (5, a), (6, b)]) where each note can be , -20, -10, 0, +10, +20 for double flat to double sharp. Attention, the order of items is important. If you want explicit natural signs in your KeySig set the third optional parameter to True
    Example 2. G Major with correct order of accidentals: keysigG = KeySignature(220, [(3,10), (0,0), (4,0), (1,0), (5,0), (2,0), (6,0)])
    Example 3. F Major with correct order. keysigF = KeySignature(170, [(6,-10), (2,0), (5,0), (1,0), (4,0), (0,0), (3,0)]) """
    def __init__(self, parentWorkspace, root, keysigtuplet, explicit=False):
        super(KeySignature, self).__init__(parentWorkspace = parentWorkspace)
        self.root = pitch.plain(root) #  Functional and scale base note. Converted to plain note without octave.
        self.keysigtuplet = keysigtuplet
        self.explicit = explicit #Use explicit natural signs in the keysig. Default off.
        self.lyGrob = ["KeySignature"]

    def generateLilypond(self):
        def build(step, alteration): #generate a Scheme pair for Lilyponds GUILE interpreter
            if alteration == -10:
                return "(" + str(step) + " . ," + "FLAT)"
            elif alteration == 10:
                return "(" + str(step) + " . ," + "SHARP)"
            elif alteration == 20:
                return "(" + str(step) + " . ," + "DOUBLE-SHARP)"
            elif alteration == -20:
                return "(" + str(step) + " . ," + "DOUBLE-FLAT)"
            elif self.explicit and alteration == 0:
                return "(" + str(step) + " . ," + "NATURAL)"
            else:
                return ""
        workcopy = self.keysigtuplet[:]
        #workcopy.reverse() #Achtung! Lilypond needs reversed order.
        schemepairs = " ".join([build(x[0], x[1]) for x in workcopy])
        schemepairs = " ".join(schemepairs.split()) # split and join again to reduce all whitespaces to single ones.
        text = "\\once \\override Staff.KeySignature #'stencil = #(lambda (grob) (ly:stencil-combine-at-edge (ly:key-signature-interface::print grob) Y DOWN (grob-interpret-markup grob (markup #:small \"" + lilypond.pitch2ly[self.root].strip(",").title() + "\")) 3)) " #3 is the space between keysig and text
        return text + "\\set Staff.keySignature = #`(" + schemepairs + ")"

    def isTheSameAs(self, other):
        """Equal"""
        return (self.root == other.root) and (self.keysigtuplet == other.keysigtuplet) and (self.explicit == other.explicit)

    def generateSave(self):
        return {"root":self.root, "keysigtuplet":self.keysigtuplet, "explicit":self.explicit ,}

    def generatePlayback(self, smfTrack, channel, transpose, jackMode, smf):
        """Midi Keysigs are very limited.
        Only major (0) and minor (1)
        and the number of flats, -7 to -1 and sharps 1 to 7.

        Laborejo can handle much more, so we somehow need an
        approximation.

        We always export major keysigs. Who cares in midi..."""
        nrOfFlats = sum(1 for (a,b) in self.keysigtuplet if b == -10)
        nrOfSharps = sum(1 for (a,b) in self.keysigtuplet if b == 10)

        if nrOfSharps > nrOfFlats: #sharps
            return [([0xFF, 0x59, 2, nrOfSharps, 0], 0)]
        elif nrOfSharps < nrOfFlats: #flats
            return [([0xFF, 0x59, 2, (-1*nrOfFlats)%256 , 0], 0)] #no negativ numbers in midi. this is expected instead. It was copypaste, I don't know what modulo means.
        else: #something else including both 0 or equal number. Go with C-Major.
            return [([0xFF, 0x59, 2, 0, 0], 0)]

class TempoSignature(Item):
    """Tempo/Speed information. A.k.a. bpm or metronome, MM=."""
    def __init__(self, parentWorkspace, beatsPerMinute, referenceTicks):
        super(TempoSignature, self).__init__(parentWorkspace = parentWorkspace)
        self.beatsPerMinute = beatsPerMinute   #example: 120
        self.referenceTicks = referenceTicks   #example: 384
        self.tempostring = ""

    def isTheSameAs(self, other):
        """Equal"""
        return (self.beatsPerMinute == other.beatsPerMinute) and (self.referenceTicks == other.referenceTicks) and (self.tempostring == other.tempostring)

    def generateLilypond(self):
        if self.tempostring:
            tmpstr = '"' + self.tempostring.strip('"') + '" ' #strip any " and add our own
        else:
            tmpstr = ""

        return "\\tempo " + tmpstr + lilypond.dur2ly[self.referenceTicks] + " = " + str(self.beatsPerMinute)

    def generateSave(self):
        return {"beatsPerMinute":self.beatsPerMinute, "referenceTicks":self.referenceTicks, "tempostring":self.tempostring}

    def generatePlayback(self, smfTrack, channel, transpose, jackMode, smf):
        """The Temposig uses 255 as first bit. This means it is not for
        a channel but global. executeMidi in Playback checks for this
        case and does not add the channel to this byte.

        This is always without TempoModification (which is saved
        in the curosr. A tempo-sig is always untouched by those
        filthy modifications."""
        return [playback.midiTempo(self.beatsPerMinute, self.referenceTicks)]
        #mpqn = playback.bpmToMpqn(self.beatsPerMinute, self.referenceTicks)
        #y = (str(hex(mpqn))[2:])
        #a = ('0x' + y[-2:])
        #b = ('0x' + y[-4:-2])
        #c = ('0x' + y[-6:-4])
        #return [([0xFF, 0x51, 0x03, int(c, 16), int(b, 16), int(a, 16)],0)]

class TempoModification(Item):
    """midi tempo modification. Only for playback, but can have optional
    Lilypond like 'A bit slower'
    But really intendend for a mass of immediate tempo changes.

    A value of 0 means "A Tempo". Reset to the last known TempoSig.

    This works by taking the current TempoSignature and adding or
    substracting the mod value to its bpm and exporting it as a new
    tempo.
    """
    def __init__(self, parentWorkspace, bpmModificator = 0):
        super(TempoModification, self).__init__(parentWorkspace = parentWorkspace)
        self.bpmModificator = bpmModificator #plus or minus to bpm

    def generatePlayback(self, smfTrack, channel, transpose, jackMode, smf):
        #Return a new and complete time signature. The time signature gets the cursor tempoModification and uses it.
        if self.bpmModificator == 0:
            self.parentWorkspace.score.activeCursor.tempoModInts.append(0)

        mod = self.parentWorkspace.score.activeCursor.tempoModInts[-1] #an int. This is all modifications until now and DOES NOT INCLUDE the current one because the cursor post-processes. We need to add our own value here.
        current = self.parentWorkspace.score.activeCursor.prevailingTempoSignature[-1] #an instance
        return [playback.midiTempo(current.beatsPerMinute + mod + self.bpmModificator, current.referenceTicks)]

    def generateSave(self):
        return {"bpmModificator": self.bpmModificator, }

    def augment(self):
        """Increase tempo mod +1"""
        if not self.selected:
            self.bpmModificator += 1

    def diminish(self):
        """Decrease tempo mod -1"""
        if not self.selected:
            self.bpmModificator -= 1

class DynamicSignature(WaitForChord):
    """The current loudness, relative to the absolute volume of an
    instrument.
    Midi expression value, lilypond dynamic string.
    Default to internal/playback only. piano, forte etc. are API
    command variations.
    In Lilypond Dynamics are attached as postfix, contrary to our
    internal logic that dynamics are standalone items. We use the
    waitForNextChordPst queue to attach the dynamic to the next
    chord-item which appears in the track"""
    def __init__(self, parentWorkspace, expression, lilystring = ""):
        super(DynamicSignature, self).__init__(parentWorkspace, lilystring)
        self._expression = expression
        self.lyGrob = ["DynamicText"]

    @property
    def expression(self):
        return self.parentWorkspace.score.activeCursor.prevailingPerformanceSignature[-1].dynamic(self._expression)

    def generateSave(self):
        return {"_expression": self._expression, "lilystring":self.lilystring}

class SubitoDynamicSignature(WaitForChord):
    def __init__(self, parentWorkspace, expression, lilystring = ""):
        super(SubitoDynamicSignature, self).__init__(parentWorkspace, lilystring)
        self.expression = expression

    @property
    def _expression(self): #for compatibility
        return self.expression

    def generateSave(self):
        return {"expression": self.expression, "lilystring":self.lilystring}

class PerformanceSignature(Item):
    """A data set for performance related parameters:
    What does "Forte" mean as numeric value?
    How short is a staccato note?
    The currently active set can be changed during the score/track.
    This enables the user/a GUI to ask for the absolute loudness level.
    Achtung: All random values must be to-substract values!"""
    def __init__(self, parentWorkspace):
        super(PerformanceSignature, self).__init__(parentWorkspace = parentWorkspace)

        self.name = "" #A name to print. Like "Legato"

        #durationKeywords
        self.staccato = 0.25
        self.tenuto = 1.0  #forced full lenght notes, no matter where. If there is a section/perf-sig "Staccato" for detache notes and a tenuto note appears it will be tenuto. under a slur? tenuto.

        #tempo keywords
            #choose some odd values for fermatas so that they don't match with an augmented duration. The values mean how much of the original tempo should be used for the fermata object.
        self.shortfermata = 0.9
        self.fermata = 0.6
        self.longfermata = 0.35
        self.verylongfermata = 0.28

        #rest
        self.legatoScaling = 1.0
        self.slurEndScaling = 0.8
        self.nonLegatoScaling = 0.85 # How long is a normal note in percent (0.x) Legato is 100%/1.0. Also known as detache. Can be 0 but then we use nonLegatoScalingByDuration. Completely 0 is not possible.
        self.nonLegatoScalingByDuration = {  #How long is a normal note in percent. Used if nonLegatoScaling is False. This is needed because you hear the percent value much 'better' in slow tempos and high durations. bpm 60 and a breve is not nearly long enough.
           12288 : 0.99, # Maxima
           6144 : 0.98, # Longa
           3072 : 0.97, # Breve
           1536 : 0.95, # Whole
           768 : 0.90, #Half
           384 : 0.85, #Quarter
           192 : 0.85, #Eighth
           96 : 0.85, #Sixteenth
           48 : 0.8, #1/32
           24 : 0.8, #1/64
           12 : 0.8, #1/128
           6 : 0.8, #1/256
           }

        self.nonLegatoHumanizing = 1.0 #1 means no change to self.nonLegatoScaling. Random variation factor to nonLegatoScaling. Lower means a chance to shorten nonLegato, higher means a change to make the duration longer.
        self.velocityHumanizing = 1.0 #0-1. The lower that number the greater the range that it can get lower. Reads as. This volume is a random between (max * factor to max). 1 means no change
        self.velocityMetricalMain = 1.0 #Non-stressed positions #A fix factor for velocity changes based on a metric position. 1 means no change, 0.5 means half as loud, 0 is silent.
        self.velocityMetricalNothing = 1.0 #Non-stressed positions #A fix factor for velocity changes based on a metric position. 1 means no change, 0.5 means half as loud, 0 is silent.
        self.velocityMetricalSecondary = 1.0 #Semi-Stressed positions #A fix factor for velocity changes based on a metric position. 1 means no change, 0.5 means half as loud, 0 is silent.
        #Dynamics can be just a plain number or a string. The value of the stringed keys are mutable and can be changed by the user to change the meaning of "Forte".
        self.dynamics = { "ppppp" : 15, "pppp" : 31, "ppp" : 43, "pp" : 53,
        "p" : 69, "mp" : 77 , "mf" : 90, "f" : 98, "ff" : 108, "fff" : 116,
        "ffff" : 127, "user1":90, "user2":90, "user3":90, "tacet":0,
        "fp":98, "sf":98, "sff":108, "sp":69, "spp":53, "sfz":116,}

        """
        Swing Mod:
        swing mod is a value around 0, float.
        value must be -3 < value < 3
        Above and below are errors and avoided by the program.

        0 is off. 1/2 + 1/2 for a note pair to the rhythm layer above.
        1 is the strict swing  2/3 + 1/3
        -1 is reversed string. 1/3 + 2/3

        You can make it more extreme or laid back by going between
        these numbers. Recommended is from 0.75 to 1.25
        -3 is both notes on the same time
        and 3 is something to not care about. These values make no
        musical sense, but are technically possible.
        """
        self.swingMod = 0.0  #keep it float. is used by the function makeSwing() in self.item which only allows rest and chord.
        self.swingLayer = 192

        #TODO: missing. rfz . this is not really a dynamic but a crescendo, mixed with tenuto and maybe even accel.

    def durationKeywordFactor(self, factorUntilNow, directivePst):
        """Gets a dict from the item, do all sorts of priority
        calculation like 'Tenuto before Staccato' and return a single
        float number as multiplication factor."""
        modification = 1
        modSwitch = False
        override = 1
        overSwitch = False

        if "staccato" in directivePst:
            modSwitch = True
            modification *= self.staccato

        if "tenuto" in directivePst:
            overSwitch = True
            override *= self.tenuto

        if overSwitch:
            return override
        elif modSwitch:
            return modification * factorUntilNow
        else:
            return factorUntilNow #no modification

    def performanceDuration(self, baseDuration):
        """Return a value to multiply the note duration with"""
        if self.nonLegatoScaling:  #not 0
            tempScaling = self.nonLegatoScaling
        else: #0. Use the nonLegatoScalingByDuration dict instead.
            tempScaling = self.nonLegatoScalingByDuration[baseDuration]

        #Humanizing/Randomizing
        if self.nonLegatoHumanizing < 1:
            tempScaling *= random.uniform(self.nonLegatoHumanizing, 1)
        elif self.nonLegatoHumanizing > 1:
            tempScaling *= random.uniform(1, self.nonLegatoHumanizing)
        return tempScaling

    def dynamic(self, key):
        """Take a string or a number that is in self.dynamics
        and return a velocity value. Maybe humanized"""
        if key in self.dynamics: #is it in there? If not it may be a direct number
            velocity = self.dynamics[key]
        elif type(key) is int: #We assume it is a number, use it directly.
            velocity = key
        else:
            raise ValueError("Velocity-Key is not a supported keyword and not a number. Value:", key, type(key), self.dynamics)

        #First transformation: Humanizing the velocity.
        if self.velocityHumanizing < 1:
            velocity *= random.uniform(self.velocityHumanizing, 1)
        elif self.velocityHumanizing > 1:
            velocity *= random.uniform(1, self.velocityHumanizing)

        #Second. Metrical Velocity Value
        metrical = self.parentWorkspace.score.activeCursor.metricalPosition
        if metrical == 0: #the main position
            velocity *= self.velocityMetricalMain
        elif metrical % 1 == 0: #every other position that is a metrical main and not the first.
            velocity *= self.velocityMetricalSecondary
        else:
            velocity *= self.velocityMetricalNothing

        #Values bigger than 127 are theoretically possible. Sometimes they work out.
        if velocity > 127:
            warnings.warn("Chord velocity modified through Performance Signature" +  str(self) + self.name + ": higher than 127, maximum value. Value ignored and set to 127 for this export. Please change the signature to values between 0 and 1 or get closer to 1.")
            return 127
        elif velocity < 0:
            warnings.warn("Chord velocity modified through Performance Signature" +  str(self) + self.name + ": lower than 0, minimum value. Value ignored and set to 0 for this export. Please change the signature to values > 0.")
            return 0
        else:
            return int(velocity)

    #def generateLilypond(self):
    #    if self.name:
    #        string =  "_\\markup { \italic { " + self.name + " } }"
    #        self.parentWorkspace.waitForNextChordPst.append(string)
    #    return ""

    def generateLilypond(self):
        """Create a rehearsal mark. This leads to a much better output
        when exporting complete scores

        Exports as rehearsal mark if in master track and as free text if in normal track."""
        if self.name:
            if self.parentWorkspace.lilypondExportTrackName.lower() == "master":
                return  "\\mark \\markup { \italic { " + self.name + " } }"
            else:
                string =  "_\\markup { \italic { " + self.name + " } }"
                self.parentWorkspace.waitForNextChordPst.append(string)
        return ""


    def generateSave(self):
        return {"name":self.name,
                "staccato": self.staccato,
                "tenuto": self.tenuto,
                "dynamics":self.dynamics,
                "legatoScaling" : self.legatoScaling,
                "slurEndScaling" : self.slurEndScaling,
                "nonLegatoScaling" : self.nonLegatoScaling,
                "nonLegatoScalingByDuration" : self.nonLegatoScalingByDuration,
                "nonLegatoHumanizing" : self.nonLegatoHumanizing,
                "velocityHumanizing" : self.velocityHumanizing,
                "shortfermata": self.shortfermata,
                "fermata":self.fermata,
                "longfermata":self.longfermata,
                "verylongfermata":self.verylongfermata,
                "velocityMetricalMain":self.velocityMetricalMain,
                "velocityMetricalNothing":self.velocityMetricalNothing,
                "velocityMetricalSecondary":self.velocityMetricalSecondary,
                "swingMod":self.swingMod,
                "swingLayer":self.swingLayer,
                }

class InstrumentChange(Item):
    """A class which covers all aspects of an instrument change
    and exports them to midi and lilypond"""
    def __init__(self, parentWorkspace):
        super(InstrumentChange, self).__init__(parentWorkspace = parentWorkspace)
        #Default values are taken from Track()
        self.smfPatch = 0
        self.smfBank = 0
        self.smfVolume = 99 #max is 127. You can get up a little now.

        self.jackPatch = 0
        self.jackBank = 0
        self.jackVolume = 127 #max is 127. You can get up a little now.
        #NO port changes!

        self.longInstrumentName = ""  #long instrument name, not unique.
        self.shortInstrumentName = ""  #short instrument name, not unique.

    def generateLilypondStaffNames(self):
        """Return the name of the staff, short and long format"""
        longName = ""
        shortName = ""
        if self.longInstrumentName: #Long name
            longName = "\\set " + "Staff.instrumentName = #\"" + self.longInstrumentName + "\""
        if self.shortInstrumentName: #Short name
            shortName = "\\set " + "Staff.shortInstrumentName = #\"" + self.shortInstrumentName + "\""

        #create a markup to indicate instrument change
        if self.shortInstrumentName:
            change = "^\\markup { \\bold { " + self.shortInstrumentName + " } }"""
        elif self.longInstrumentName:
            change = "^\\markup { \\bold { " + self.longInstrumentName + " } }"""
        else:
            change = ""

        if change:
            self.parentWorkspace.waitForNextChordPst.append(change)

        return longName + " " + shortName

    def generateLilypond(self):
        names = self.generateLilypondStaffNames()
        if names.rstrip(): #maybe it was just an empty string? that means no new instrument names. we just ignore it
            return names
        else:
            return ""

    def generatePlayback(self, smfTrack, channel, transpose, jackMode, smf):
        if not jackMode:
            return [
                ([0xB0, 0x07, self.smfVolume], 0), #Volume for this channel. CC 7
                ([0xB0, 0x00, self.smfBank], 0), #Bank. CC0
                ([0xC0, self.smfPatch], 0), #Initial Patch/Instrument
                ([0xFF, 0x04, len(self.longInstrumentName)] + [ord(c) for c in self.longInstrumentName], 0)
                ]
        else:
            return [
                ([0xB0, 0x07, self.jackVolume], 0), #Volume for this channel. CC 7
                ([0xB0, 0x00, self.jackBank], 0), #Bank. CC0
                ([0xC0, self.jackPatch], 0), #Initial Patch/Instrument
                ]

    def generateSave(self):
        return {"smfPatch": self.smfPatch,
                "smfBank":self.smfBank,
                "smfVolume" : self.smfVolume,

                "jackPatch": self.jackPatch,
                "jackBank":self.jackBank,
                "jackVolume" : self.jackVolume,

                "longInstrumentName" : self.longInstrumentName,
                "shortInstrumentName" : self.shortInstrumentName,
                }

class ChannelChangeRelative(Item):
    """midi channel change object. Relative"""
    def __init__(self, parentWorkspace):
        #TODO: Jack Mode?
        super(ChannelChangeRelative, self).__init__(parentWorkspace = parentWorkspace)
        self.number = 0

    def exportPlayback(self, smfTrack, channel, transpose, jackMode, smf):
        if jackMode:
            newchannel = [channel[0], channel[1], channel[2] + self.number]
            return (smfTrack, newchannel, transpose, jackMode)
        else:
            return []

    def generateSave(self):
        return {"number": self.number,
                }

class ChannelChangeAbsolute(Item):
    """midi channel change object. Relative"""
    def __init__(self, parentWorkspace):
        super(ChannelChangeAbsolute, self).__init__(parentWorkspace = parentWorkspace)
        self.number = 0

    def exportPlayback(self, smfTrack, channel, transpose, jackMode, smf):
        if jackMode:
            if 0 <= self.number <= 15:
                newchannel = [channel[0], channel[1], self.number]
                return (smfTrack, newchannel, transpose, jackMode)
            else:
                raise ValueError("Midi Channel numbers must be bewtween 0 and 15. But value was:", self.number)
        else:
            return []

    def generateSave(self):
        return {"number": self.number,
                }

class ProgramChangeRelative(Item):
    """midi program change object. Relative"""
    def __init__(self, parentWorkspace):
        super(ProgramChangeRelative, self).__init__(parentWorkspace = parentWorkspace)
        self.number = 0

    def generatePlayback(self, smfTrack, channel, transpose, jackMode, smf):
        if jackMode:
            #prevailingJackPatch is an int. This is all modifications until now and DOES NOT INCLUDE the current one because the cursor post-processes. We need to add our own value here. See TempoModification
            value = self.parentWorkspace.score.activeCursor.prevailingJackPatch[-1] + self.number
            return [([0xC0, value], 0),] #Patch/Instrument
        else:
            return []

    def generateSave(self):
        return {"number": self.number,
                }

class ProgramChangeAbsolute(Item):
    """midi program change object. Relative"""
    def __init__(self, parentWorkspace):
        super(ProgramChangeAbsolute, self).__init__(parentWorkspace = parentWorkspace)
        self.number = 0

    def generatePlayback(self, smfTrack, channel, transpose, jackMode, smf):
        if jackMode:
            if 0 <= self.number <= 127:
                return [([0xC0, self.number], 0),] #Patch/Instrument
            else:
                raise ValueError("Midi Program numbers must be bewtween 0 and 127. But value was:", self.number)
        else:
            return []

    def generateSave(self):
        return {"number": self.number,
                }

class SpecialBarline(Item):
    """A generic barline item. Important for Playback
    Internally it has no meaning. Tickindex stays the same.
    For Lilypond it is just another Glyph."""
    def __init__(self, parentWorkspace):
        super(SpecialBarline, self).__init__(parentWorkspace = parentWorkspace)
        self.repeat = 0 #how often does it go back?
        self.lyGrob = ["BarLine"]

    def generateLilypond(self):
        return self.lilypond

    def generateSave(self):
        return {"repeat": self.repeat,
                }

class RepeatOpen(SpecialBarline):
    """||:"""
    def __init__(self, parentWorkspace):
        super(RepeatOpen, self).__init__(parentWorkspace = parentWorkspace)

class RepeatClose(SpecialBarline):
    """:||"""
    def __init__(self, parentWorkspace, repeat = 1):
        super(RepeatClose, self).__init__(parentWorkspace = parentWorkspace)
        self.repeat = repeat #int. how often?

class RepeatCloseOpen(SpecialBarline):
    """:||:"""
    def __init__(self, parentWorkspace, repeat = 1):
        super(RepeatCloseOpen, self).__init__(parentWorkspace = parentWorkspace)
        self.repeat = repeat #int. how often?

class Segno(Item):
    """The target item for GotoSegno. Just a marker for typechecking."""
    def __init__(self, parentWorkspace):
        super(Segno, self).__init__(parentWorkspace = parentWorkspace)

    def generateLilypond(self):
        return '\\mark \\markup { \\musicglyph #"scripts.segno" }'

class Coda(Item):
    """The target item for GotoCoda"""
    def __init__(self, parentWorkspace):
        super(Coda, self).__init__(parentWorkspace = parentWorkspace)

    def generateLilypond(self):
        return '\\bar "||" \\break \\mark \\markup { \\musicglyph #"scripts.coda" }'

class Fine(Item):
    """The target item for al Fine"""
    def __init__(self, parentWorkspace):
        super(Fine, self).__init__(parentWorkspace = parentWorkspace)

    def generateLilypond(self):
        return '\\mark \\markup { "Fine" }'

class AlternateEnd(Item):
    """Bracket one, two etc.
    Volta one, two in lilypond"""
    def __init__(self, parentWorkspace):
        super(AlternateEnd, self).__init__(parentWorkspace = parentWorkspace)
        self.endings = [] #a list of numbers
        self._text = None

    def generateSave(self):
        return {"endings": self.endings,
                "_text": self._text}

    @property
    def text(self):
        if self._text:
            return self._text
        else:
            return ",".join([str(x) for x in self.endings])

    @text.setter
    def text(self, value):
        self._text = value
        return self._text

    def generateLilypond(self):
        return '\\set Score.repeatCommands = #\'((volta "' + self.text + '" ))'

class AlternateEndClose(Item):
    """Only for Lilypond. This is not just a standalone because it may
    be needed later.
    No playback function. unfoldScore() does some parsing to work
    without this item."""
    def __init__(self, parentWorkspace):
        super(AlternateEndClose, self).__init__(parentWorkspace = parentWorkspace)

    def generateLilypond(self):
        return '\\set Score.repeatCommands = #\'((volta #f ))'

"""
#Better do this in playback trigger and give the goto method an uid parameter.
class Goto(Item):
    #During playback in the same track:
    #Go to a flat-cursor-index position (prefered because explicit)
    #or search for the first uid item. Linked items may confuse the user
    #here.
    def __init__(self, parentWorkspace, index = None, uid = None):
        super(Goto, self).__init__(parentWorkspace = parentWorkspace)
        self.disableRepeat = False #After the jump any repeats |: :| will be disabled.
        self.gotoIndex = index
        self.gotoUid = uid
        self.until = None # Can be Coda or Fine, the class. For a typecheck.

    def goto(self, cursor):
        if self.gotoIndex or self.gotoIndex == 0:
            cursor.goToIndex(self.gotoIndex)
        elif self.gotoUid:
            pass #TODO
        else:
            warning.warn("You have to give either a flat cursor index or an uid. Goto ignored, may result in bad playback. If you used this to reactivate Repeats ignore this warning.")

        if disableRepeat:
            return False #indicate the playback pre-processor to deactivate repeats
        else:
            return True #...or leave/switch them on again.
"""

class GotoSegno(Item):
    """Needs nothing at all. The playback pre-processor will do
    everything."""
    def __init__(self, parentWorkspace, disableRepeat = False):
        super(GotoSegno, self).__init__(parentWorkspace = parentWorkspace)
        self.disableRepeat = disableRepeat #After the jump any repeats |: :| will be disabled.
        self.until = None # Can be "Coda" or "Fine"

    def generateLilypond(self):
        #return '\\mark \\markup { { \\lower #1 "D.S. al  " { \\musicglyph #"scripts.segno"} } } '
        add = ''
        if self.until == "Fine":
            add = " al Fine"
        elif self.until == "Coda":
            add = ' al Coda'
        return '\n\\once \\override Score.RehearsalMark #\'break-visibility = #end-of-line-visible \n\\once \\override Score.RehearsalMark #\'self-alignment-X = #RIGHT \n\\mark \\markup { \\lower #1 "D.S.' + add + '" }'

    def generateSave(self):
        return {"disableRepeat": self.disableRepeat,
                "until": self.until,
                }

class GotoCapo(Item):
    """Needs nothing at all. The playback pre-processor will do
    everything."""
    def __init__(self, parentWorkspace, disableRepeat = False):
        super(GotoCapo, self).__init__(parentWorkspace = parentWorkspace)
        self.disableRepeat = disableRepeat #After the jump any repeats |: :| will be disabled.
        self.until = None # Can be "Coda" or "Fine"

    def generateLilypond(self):
        add = ''
        if self.until == "Fine":
            add = " al Fine"
        elif self.until == "Coda":
            add = ' al Coda'
        return '\n\\once \\override Score.RehearsalMark #\'break-visibility = #end-of-line-visible \n\\once \\override Score.RehearsalMark #\'self-alignment-X = #RIGHT \n\\mark \\markup { \\lower #1 "D.C.' + add + '" }'

    def generateSave(self):
        return {"disableRepeat": self.disableRepeat,
                "until": self.until,
                }

class GotoCoda(Item):
    """Needs nothing at all. The playback pre-processor will do
    everything."""
    def __init__(self, parentWorkspace):
        super(GotoCoda, self).__init__(parentWorkspace = parentWorkspace)

    def generateLilypond(self):
        return '\\mark \\markup { \\small { \\musicglyph #"scripts.coda" \\musicglyph #"scripts.tenuto" \\musicglyph #"scripts.coda"} }'


"""
class Playbacktrigger(Item):
    #If this item is encountered during playback it will execute its
    #code in the playback-pre-processor 'unfoldScore'.
    #The code is saved with the lbjs file and comes in string form.

    #The playback process will work with a copy of this object so all
    #parameters will remain the same.

    def __init__(self, parentWorkspace):
        super(Playbacktrigger, self).__init__(parentWorkspace = parentWorkspace)
        code = ""
        counter = 0 #this indicates how often the playback process encountered this instance. Default is obviously zero. This will only change in the copy.
"""

"""
#TODO: when activated remember to do the save
class LegatoOn(Item):
    def __init__(self, parentWorkspace):
        super(LegatoOn, self).__init__(lilystring = "")

class LegatoOff(Item):
    def __init__(self, parentWorkspace):
        super(LegatoOff, self).__init__(lilystring = "")
"""

class SlurOn(WaitForChord):
    """Standalone Item which switches midi legato in the cursor on
    and waits for the next chord to begin drawing a lilypond slur or
    phrasing slur. Can be nested"""
    def __init__(self, parentWorkspace, phrasing = False):
        super(SlurOn, self).__init__(parentWorkspace = parentWorkspace, lilystring = "(")

class SlurOff(Item):
    """Standalone Item which switches midi legato in the cursor off
    and waits for the next chord to stop drawing a lilypond slur or
    phrasing slur. Can be nested"""
    def __init__(self, parentWorkspace, phrasing = False):
        super(SlurOff, self).__init__(parentWorkspace = parentWorkspace)
        self.lilypond = ")"

class PhrasingSlurOn(WaitForChord):
    """Standalone Item which switches midi legato in the cursor on
    and waits for the next chord to begin drawing a lilypond slur or
    phrasing slur. Can be nested"""
    def __init__(self, parentWorkspace, phrasing = False):
        super(PhrasingSlurOn, self).__init__(parentWorkspace = parentWorkspace, lilystring = "\\(")

class PhrasingSlurOff(Item):
    """Standalone Item which switches midi legato in the cursor off
    and waits for the next chord to stop drawing a lilypond slur or
    phrasing slur. Can be nested"""
    def __init__(self, parentWorkspace, phrasing = False):
        super(PhrasingSlurOff, self).__init__(parentWorkspace = parentWorkspace)
        self.lilypond = "\\)"

class PedalSustainChange(Item):
    """A quick off and on. Use before a chord. Not printing"""
    def __init__(self, parentWorkspace):
        super(PedalSustainChange, self).__init__(parentWorkspace = parentWorkspace)
        self.instructionPre["pedal"] = [[0xB0, 0x40, 0], 0] #off
        self.instructionPst["pedal"] = [[0xB0, 0x40, 127], 1] #on

class Header(Internal, Lilypond):
    """Container for information like title or author name."""
    def __init__(self, parentWorkspace):
        super(Header, self).__init__()
        self.data = {} #keys are exported to lilypond header variables. values are either exported directly or to the special lilypond "None" value ##f if our python value is False.
        self.parentWorkspace = parentWorkspace

        #By default some values need to be empty or we get in trouble when exporting Collections.
        for m in ["title", "subtitle", "dedication","composer","subsubtitle","instrument","meter","arranger", "poet","piece","opus","copyright","tagline"]:
            self.data[m] = ""

    def generateLilypond(self, ignoreEmpty = False, ignoreKeys = []):
        gatherer = ""
        if ignoreEmpty:
            for key, value in self.data.items():
                if value and not key in ignoreKeys:
                    if value.strip().startswith("\\markup"):
                        gatherer += key + " = " + value + "\n"
                    else:
                        gatherer += key + " = \"" + value + "\"\n"
                else:
                    pass #ignore empty.
            return gatherer

        else: #default
            for key, value in self.data.items():
                if value:
                    if value.strip().startswith("\\markup"):
                        gatherer += key + " = " + value + "\n"
                    else:
                        gatherer += key + " = \"" + value + "\"\n"
                else:
                    gatherer += key + " =  ##f\n"
            return gatherer


    def generateSave(self):
        """If in doubt return nothing, still compatible.
        Child classes should implement this function and return dict"""
        return {}

    def exportSave(self):
        """Generate the dict with whatever the child class gives us
        then add the Internal Values (durations) and return as tuplet
        (ClassName, childDict) which will be exported to Json in the end"""
        childDict =  self.generateSave()

        #Internal
        childDict["base"] = self.base
        childDict["tuplets"] = self.tuplets
        childDict["dots"] = self.dots
        childDict["scaleFactorNumerator"] = self.scaleFactorNumerator
        childDict["scaleFactorDenominator"] = self.scaleFactorDenominator
        #Lilypond
        childDict["lilypond"] = self.lilypond
        childDict["directivePre"] = self.directivePre
        childDict["directiveMid"] = self.directiveMid
        childDict["directivePst"] = self.directivePst
        childDict["data"] = self.data

        return (type(self).__name__, childDict)

class LilypondDefinitions(Lilypond):
    """A class to access a dict which generates a
    long lilypond string for the export var %$$LILYPONDDEFINITIONS$$.
    Only strings as value allowed."""
    def __init__(self):
        super(LilypondDefinitions, self).__init__()
        self.container = {}

    def generateLilypond(self):
        return "\n".join(self.container.values())

    def exportSave(self):
        childDict = {}
        #Lilypond
        childDict["lilypond"] = self.lilypond
        childDict["directivePre"] = self.directivePre
        childDict["directiveMid"] = self.directiveMid
        childDict["directivePst"] = self.directivePst
        childDict["container"] = self.container
        return (type(self).__name__, childDict)

#Some constant default items. They are here because we need them to initialize other stuff, even before actual musical content gets created (score etc.)
#This is a breach of concept, but they are also here because the QT gui needs them.
DEFAULT_TIMESIG = TimeSignature(None, 4, 384 ) #4/4, Lilypond default
DEFAULT_KEYSIG = KeySignature(None, 20, [(3,0), (0,0), (4,0), (1,0), (5,0), (2,0), (6,0)]) #C-Major
DEFAULT_TEMPOSIG = TempoSignature(None, 120, 384) #120bpm, reference quarter
