��    '      T  5   �      `  [   a  L   �     
  H     S   \     �  	   �     �  
   �     �     �  m     n   z  
   �     �  <     ]   B  �   �     E     N     T     c     h     �     �  S   �     �     �     	               &  @   .  /   o     �  *   �  &   �  z   �  g  r	  n   �
  P   I  	   �  K   �  Z   �  	   K  	   U     _     {     �     �  �   �  �   ;     �     �  0   �  k     �   �     `     r     y     �  0   �     �     �  b   �     0     9     W  	   h  
   r     }  G   �  (   �     �  +     &   -  �   T     &   
   $      	                 #             !                           "                                                         %                                    '          &lt;br&gt; for a line break. Empty line for new paragraph.
More with Lilypond Markup Syntax After the duration:
<c'>2postfix

Leave field blank to delete the directive. Arranger Before the note:
prefix<c'>2

Leave field blank to delete the directive. Between notes and duration:
<c'>middle2

Leave field blank to delete the directive. Composer Copyright Create Object Directives Dedication Edit Container Edit Playback Trigger Executed after playing back this item.
Insert Midibytes as a list.
Leave field blank to delete the directive. Executed before playing back this item.
Insert Midibytes as a list.
Leave field blank to delete the directive. Instrument Laborejo Message Leave fields blank to not create any directive of this kind. Legato (Slurs) 
Smaller value means shorter duration. Must be bigger than 0. Full Length: 1.  Lilypond text to insert on this position.

This will override any generated (notes, clefs etc.) text.
An empty string, no spaces, will enable auto-generation again. Metadata Meter Optional: Name Opus Percent-Repeat incl. first Piece Poet Recursive Containers are not allowed. A container is not allowed to exist in itself Staccato Standalone Lilypond Directive Subsubtitle Subtext Subtitle Tagline The text will be used as post directive for the next chord/note. These will be visible in the print out as well. Title Translation Test. The language is English. Wait-For-Next-Chord Lilypond Directive You must specify a variable 'ret' as True or False.
This will be used to determin if this item will be played back or not. Project-Id-Version: Laborejo
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-09-08 23:02+0200
PO-Revision-Date: 2012-09-11 22:27+0100
Last-Translator: Nils Gey <info@laborejo.org>
Language-Team: Inhouse <info@laborejo.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: German
X-Poedit-SourceCharset: UTF-8
 &lt;br&gt; zum Zeilenumbruch. Leerzeile für neuen Paragraph.
Lilypond Markup Syntax für mehr Formatierungen. Nach dem Rhytmus:
<c'>2postfix

Feld leer lassen um diese Direktive zu löschen. Arrangeur Vor der Note:
prefix<c'>2

Feld leer lassen um diese Direktive zu löschen. Zwischen noten und Rhythmus:
<c'>middle2

Feld leer lassen um diese Direktive zu löschen. Komponist Copyright Erschaffe Objekt Direktiven Widmung Bearbeite Container Bearbeite Playback Trigger Wird ausgeführt nachdem dieses Objekt abgespielt wurde.
Füge midibytes als Liste ein.
Feld leer lassen um diese Direktive zu löschen. Wird ausgeführt bevor dieses Objekt abgespielt wird.
Füge midibytes als Liste ein.
Feld leer lassen um diese Direktive zu löschen. Instrument(e) Laborejo Benachrichtigung Feld leer lassen um diese Direktive zu löschen. Legato durch Haltebögen
Kleinerer Wert bedeutet kürzere Dauer. Muss größer als 0 sein. Normale Dauer: 1 Lilypond Text für das Objekt an dieser Position.

Dies wird den generierten Output (noten, schlüssel etc.) restlos überschreiben.
Ein leeres Feld, keine Leerzeichen!, schaltet den normalen Objekt-Output wieder ein. Metainformationen Metrum Optional: Name Opus Anzahl der Prozent-Wiederholung inkl. der ersten Stück Dichter Rekursive Container sind nicht erlaubt. Ein Container darf nicht in sich selbst eingefügt werden. Staccato Lilypond Direktive als Objekt Unter-Untertitel Untertext Untertitel Schlußzeile Der Text wird als Post-Direktive für den nächsten Akkord/Note benutzt Die Eingaben sind auch im Druck sichtbar Titel Übersetzungstest - Die Sprache ist Deutsch Wait-For-Next-Chord Lilypond Direktive Die Variable 'ret' muss True oder False gesetzt werden.
Sie wird dann benutzt um zu entscheiden ob dieses Objekt abgespielt wird oder nicht. 