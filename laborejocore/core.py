# -*- coding: utf-8 -*-
import os, warnings, copy, random, re
from laborejocore import cursor, lilypond, helper, pitch
from laborejocore.items import * #Internal, Playback, Lilypond, Start, End, PerformanceSignature, KeySignature, TimeSignature, Appending, LilypondDefinitions, Header, Chord, Rest, AlternateEnd, AlternateEndClose, RepeatClose, RepeatOpen, RepeatCloseOpen, Rest, Fine, Segno, GotoSegno, GotoCapo, GotoCoda, SpecialBarline, Clef, MultiMeasureRest
from string import ascii_letters
try: #TODO: Get rid of this shit. F*ck you, Debian!
    from collections import ChainMap
except ImportError:
    warnings.warn("If you experience an output error while exporting merged tracks it is because you are running an old Python version. Please upgrade to Py 3.3.")

try:
    import smf
except:
    warnings.warn("libpysmf not found. Midi export broken. Not 'disabled', broken! You can use the rest of the program just fine.")

def flatten(lst):
    """Take a nested list of any depth and make it a flat, plain
    list with only one level"""
    for elem in lst:
        #if type(elem) in (tuple, list):
        if isinstance(elem, Container):
            for i in flatten(elem):
                yield i
        else:
            yield elem

class Container(Internal, Playback, Lilypond):
    """Basic and smallest class to hold several items.
    Can be nested. Emulates list behaviour."""
    #TODO: Do not export containers as plain unfolded lilypond but put them in vars above the score. Create %$$MUSICDEFINITIONS$$
    #This is the data structure that needs the performance features.
    def __init__(self, parentWorkspace):
        super(Container, self).__init__()
        self.parentWorkspace = parentWorkspace
        self.container = [Start(parentWorkspace = parentWorkspace, parentContainer = self), End(parentWorkspace = parentWorkspace, parentContainer = self)]
        self.instanceCount = 0
        self._uniqueContainerName = self.generateNextUniqueContainerName()

        self.cursorWalk = True #The cursor walks through this as if the container content were single items. This is meant for direct access by the user. If he does not want to edit the container with the general cursor he can deactivate this. Meant for external edit-the-container modes or tools.
        self.unfold = True #For lilypond (and possible GUIs) unfold this on export. Midi will always be unfolded. This is False for effects and some substitutions. The trill will only be rendered as midi, never as lilypond, gui or analysis.
        self.group = "" #An optional group for various purposes.
        self.repeatPercent = 1 #numbers bigger than 1 will be unfolded as % repeat with lilyponds \repeat percent n {} syntax
        self.figures = self.chordsymbols = None #prevents typchecking

        self.transposition = (1420, 1420)  #[0]=what is meant, [1]=what will be written in the pdf . Usually [0] stays 1420 and [1] changes. Exports as lilypond transpose for pdf generation. Sounds the same, probably looks different. initially no transposition. c' is c'.  In human words: "I notated in C and it sounds correct but for the pdf I want it to be notated one whole tone lower for a clarinet"

        self.smfTransposition = 0 #Half Tones. negative or positive. This is just for midi export. If you export your Contrabass with -12 here it will sound correct but it will be still considered as written in Laborejo in the analysis. Simple rule: Type in what it is. This is mainly for wrong sampled instruments which transpose on their own
        self.jackTransposition = 0 #Half Tones. negative or positive. This is just for midi export. If you export your Contrabass with -12 here it will sound correct but it will be still considered as written in Laborejo in the analysis. Simple rule: Type in what it is. This is mainly for wrong sampled instruments which transpose on their own

    def append(self, value):
        self.container.append(value)
    def __len__(self):
        return len(self.container)
    def __getitem__(self, i):
        return self.container[i]
    def __iter__(self):
        return iter(self.container)
    def __setitem__(self, item, value):
        self.container[item] = value
    def __delitem__(self, item):
        del self.container[item]

    #def __del__(self):
    #    This does not work. There are some instances left in the system it seem. True for standalone container and tracks.
    #TODO: profile

    def insert(self, i, item):
        self.container.insert(i, item)

    def generateNextUniqueContainerName(self):
        """Return an 8 letter string if someone forgot to choose a name for Track. Its guaranteed to be random @ creation time."""
        try: #is this the program start? The first track is generated before the workspace.
            self.parentWorkspace
            stringy = ''.join([random.choice('abcdefghijklmnoprstuvwyxzABCDEFGHIJKLMNOPRSTUVWXYZ1234567890') for i in range(8)])
            while stringy in self.parentWorkspace.uniqueContainerNames:
              stringy = ''.join([random.choice('abcdefghijklmnoprstuvwyxzABCDEFGHIJKLMNOPRSTUVWXYZ1234567890') for i in range(8)])
            return stringy
        except:
            return ''.join([random.choice('abcdefghijklmnoprstuvwyxzABCDEFGHIJKLMNOPRSTUVWXYZ1234567890') for i in range(8)])

    @property
    def uniqueContainerName(self):
        return self._uniqueContainerName

    @uniqueContainerName.setter
    def uniqueContainerName(self, value):
        """Track has its own version because it needs to sync
        jack midi out names"""
        oldValue = self.uniqueContainerName
        value = "".join([ch for ch in value if ch in ascii_letters+"1234567890_" ]) #remove all non-chars to create valid lilypond syntax

        if oldValue not in self.parentWorkspace.uniqueContainerNames and not oldValue == "":
                self.parentWorkspace.uniqueContainerNames.append(oldValue)

        if value in self.parentWorkspace.uniqueContainerNames or value == "":
            return False
        else:
            self._uniqueContainerName = value
            if oldValue in self.parentWorkspace.uniqueContainerNames:
                self.parentWorkspace.uniqueContainerNames.remove(oldValue)
            self.parentWorkspace.uniqueContainerNames.append(value)
            return value

    @property
    def lilypondContainerName(self):
        """Laborejo can use any letters as container name.
        Lilypond is confused when you use note names or other fixed
        keywords. We prefix all laborejo containers with lbjContFoo
        so there will be no problem.

        Also exchange number with letters since lilypond doesn't
        like numbers.

        And to prevent super rare but possible naming conflicts between:
        trackname1 and tracknameA (which is the converted 1)
        also add
        """
        #return "lbjCont" + helper.stringToCharsOnlyString(self.uniqueContainerName)
        if self.uniqueContainerName.lower() == "master":
            return "master"
        else:
            return  helper.stringToCharsOnlyString(self.uniqueContainerName + str(hash(self.uniqueContainerName)))


    @property
    def selected(self):
        return False

    @selected.setter
    def selected(self, value):
        return False

    @property
    def duration(self):
        """Complete Musical Duration of this container. Used to calculate sync and playback"""
        #[:-1] means without the End() item. Else we get an recursive loop since End().duration asks for this self.duration() function again.
        return sum([x.duration for x in self.container[:-1]]) * self.repeatPercent #repeatPercent counts from 1. "No Repeat" is 1 repeat.

    def isEmpty(self):
        """Has no duration"""
        try:
            next(x for x in self.container if (x.duration and not type(x) is Upbeat))
            return False
        except StopIteration:
            return True

    def select(self, opt = False):
        pass
    def deselect(self, opt = False):
        pass
    #def select(self):
    #    """Select any item or container in a container. In the end it
    #    always results in selected items, not containers"""
    #    for x in self:
    #        x.select()

    #def deselect(self, deleteMe = False):
    #    for x in self:
    #        x.deselect(deleteMe)

    def recursiveParseWithTuplets(self, generateFunctionString):
        """Used for generate Lilypond data ("inner"), chordsymbols and
        figured bass. generateFunctionString is used to tell the
        item which function to use.
        It works with notes, figuredBass and
        chordsymbols for Chords/Items so recursiveParse is the same
        function for all these export steps"""
        returnList = []
        lastTuplet = None
        current = 0 #not needed logically, but technically the first test down here uses "current" before assignment.
        lilypondTupletLevel = 0 #For tuplet merging in Lilypond. Used to check if a new tuplet starts or if a tuplet is finished. Can be compared to chord.tuplets list length.
        for x in self:
            if x.tuplets and lilypondTupletLevel == len(x.tuplets) and (not x.tuplets == lastTuplet):
                #If an old tuplet ends and a new but differnent one start on the same level close and reopen the new one.
                returnList.append("}") #close old tuplet.
                returnList.append("\\times " + str(x.tuplets[current][0]) + "/" + str(x.tuplets[current][1]) +  "{") # Begin new tuplet
            if x.tuplets:
                lastTuplet = x.tuplets
            if x.tuplets and lilypondTupletLevel < len(x.tuplets):
                while not lilypondTupletLevel == len(x.tuplets):
                    lilypondTupletLevel += 1
                    current = lilypondTupletLevel -1
                    returnList.append("\\times " + str(x.tuplets[current][0]) + "/" + str(x.tuplets[current][1]) +  "{") # Begin a tuplet
            if lilypondTupletLevel > len(x.tuplets):
                while not lilypondTupletLevel == len(x.tuplets):
                    #returnList.append("}") #close tuplets.
                    self.parentWorkspace.waitForNextChordPre.append("}")

                    lilypondTupletLevel -= 1
            returnList.append(getattr(x, generateFunctionString)()) #the actual data gathering
        return " ".join(returnList)

    def generateInnerLilypond(self):
        """The string for recursive tuplets is used to choose a function
        which will be exported. It works with notes, figuredBass and
        chordsymbols for Chords/Items so recursiveParse is the same
        function for all these export steps"""
        if self.repeatPercent > 1:
            return "\\repeat percent " + str(self.repeatPercent) + " { " +  self.recursiveParseWithTuplets("exportLilypond") + " } "
        else:
            return self.recursiveParseWithTuplets("exportLilypond")

    def generateFiguredBass(self):
        """The string for recursive tuplets is used to choose a function
        which will be exported. It works with notes, figuredBass and
        chordsymbols for Chords/Items so recursiveParse is the same
        function for all these export steps"""
        if self.repeatPercent > 1:
            return "\\repeat percent " + str(self.repeatPercent) + " { " +  self.recursiveParseWithTuplets("generateFiguredBass") + " } "
        else:
            return self.recursiveParseWithTuplets("generateFiguredBass")

    def generateChordsymbols(self):
        """The string for recursive tuplets is used to choose a function
        which will be exported. It works with notes, figuredBass and
        chordsymbols for Chords/Items so recursiveParse is the same
        function for all these export steps"""
        if self.repeatPercent > 1:
            return "\\repeat percent " + str(self.repeatPercent) + " { " +  self.recursiveParseWithTuplets("generateChordsymbols") + " } "
        else:
            return self.recursiveParseWithTuplets("generateChordsymbols")

    def generateLilypondTranspose(self):
        """Generate the lilypond transposition string which can be used
        per track, score or even container."""
        if self.transposition == (1420, 1420):
            return ""
        else:
            return " \\transpose " + lilypond.pitch2ly[self.transposition[0]] + " " + lilypond.pitch2ly[self.transposition[1]] + " "


    def generateLilypond(self):
        """Export only the uniqueContainerName  which is the lilypond
        variable.
        But save the actual data in a temporary, non-saved,
        dictionary in self.parentWorkspace."""
        innerLy = self.generateInnerLilypond() #returns a string
        self.parentWorkspace.score.nonTrackContainer[self.lilypondContainerName] = innerLy
        return " \\" + self.lilypondContainerName

    def generateSubstitutionLilypond(self):
        """Substitutions need the real string, not a containername"""
        return self.generateInnerLilypond()

    def transform(self, keysig, referenceBaseDuration=None, rootpitch =  None, scale = [None, None, None, None, None, None, None], parameterDict = {}):
        """Return a copy of itself, destructively transformed by
        the optional parameters.
        If none are given it returns just a deepcopy.
        Every object and the structure itself is new.

        rootpitch is a pitch which will be compared with 1420
        None means no change.
        1470 means all notes are shifted in the ratio
        as c' to d' (major 2nd up)

        referenceBaseDuration is used to calculate a ratio.
        A whole note duration and a whole note container duration sum
        will result in a factor 1 (no change).
        parameter duration 768/half note and container
        duration 1536/whole note will result in 0.5.
        Every note in the new.container will have its base duration
        multiplied with 0.5.

        scales positions list:
        The lowest pitch is considered the root note.
        Return a list of 8 positions. The first is the octave, a bool that
        shows if any higher octave variants of the root are in here.
        For all other positions, if present, return the difference to white.

        All other note scale positions/intervals force the position to
        a specific alteration/accidental.
        If an interval is given for a position in the scale then the
        original pitch is forgotten and a new one gets calculated with
        the intervalUp and the rootpitch.
        """

        new = copy.deepcopy(self)
        if not len(scale) == 7:
            raise ValueError("The given scale needs to have exactly 7 positions. From root to seventh. Use None to skip one")
            return new

        def filterCheck(item):
            typ = type(item)
            if typ in [KeySignature, Clef, TimeSignature, Container, Track, Score, Appending, Start]: #End must stay in because .duration() excludes it with [:-1]
                return False
            else:
                return True
        filtered = list(filter(lambda chk: filterCheck(chk), new.container))
        new.container = filtered

        #Now all critical items are filtered out. Begin transformation process.
        if referenceBaseDuration:
            completeContainerDuration = new.duration #the complete duration of the container
            ratio = referenceBaseDuration / completeContainerDuration

        else:
            ratio = None

        for item in new.container[:-1]: #without End()
            #if ratio and not type(item) is End:
            if ratio:
                #refereceBaseDuration = the host to the substitution
                #completeContainerDuration = the duration of the complete stored substitution container.
                #item.guessDuration(referenceBaseDuration, completeContainerDuration) #changes the items duration in place.
                item.guessDurationAbsolute(item.duration * ratio)
            if rootpitch: #this is not really optional. If somehow this is avoided (certainly not by chord.exportLilypond() or playback) we just get a plain substitution with the exact container content in C.
                for note in item.notelist:
                    scalePos = pitch.diatonicIndex(note.pitch)
                    #transpose the given note to the current keysig or do a real transposition, if there is an octave.
                    note.intervalAutomatic(None, 1420, rootpitch) #None is the parameter for a keysig, which is not needed here; we don't have it available anyway.
                    if not scalePos == 0:
                        if scale[scalePos] or scale[scalePos] == 0:
                            note.toWhite(keysig)
                            note.pitch += scale[scalePos]
                        elif not scale[0]: #There was no octave in the reference scale. This means not real tranposition but shift.
                            note.toScale(keysig)
            else:
                warnings.warning("Container transform without root note. This is very uncommon.", self, self.uniqueContainerName)

            #parameterDict: key = parameter name, value = new value. for setattr
            for parameter, value in parameterDict.items():
                setattr(item, parameter, value)

        return new

    def saveState(self):
        pass

    def generateSave(self):
        """If in doubt return nothing, still compatible.
        Child classes should implement this function and return dict"""
        return {}

    def exportSave(self):
        """Generate the dict with whatever the child class gives us
        then add the Internal Values (durations) and return as tuplet
        (ClassName, childDict) which will be exported to Json in the end"""
        childDict =  self.generateSave()

        #Internal
        childDict["base"] = self.base
        childDict["tuplets"] = self.tuplets
        childDict["dots"] = self.dots
        childDict["scaleFactorNumerator"] = self.scaleFactorNumerator
        childDict["scaleFactorDenominator"] = self.scaleFactorDenominator
        #Playback
        childDict["split"] = self.split
        childDict["durationFactor"] = self.durationFactor
        childDict["instructionPre"] = self.instructionPre
        childDict["instructionPst"] = self.instructionPst
        #Lilypond
        childDict["lilypond"] = self.lilypond
        childDict["directivePre"] = self.directivePre
        childDict["directiveMid"] = self.directiveMid
        childDict["directivePst"] = self.directivePst
        childDict["transposition"] = self.transposition

        exportList = []
        for item in self.container:
            exportList.append(item.exportSave())

        childDict["smfTransposition"] = self.smfTransposition
        childDict["jackTransposition"] = self.jackTransposition
        childDict["cursorWalk"] = self.cursorWalk
        childDict["group"] = self.group
        childDict["unfold"] = self.unfold
        childDict["_uniqueContainerName"] = self._uniqueContainerName
        childDict["container"] = exportList
        childDict["repeatPercent"] = self.repeatPercent

        return (type(self).__name__, childDict)

    #No export Playback here. Export playback is done by the cursor which goes transparently through all containers.

class Track(Container):
    """Medium sized container. Can hold containers or items. Only one keysig and timesig is valid at each time-position here.
    Attention: Testing for equality or not equal with tracks is done through the unique track name. tracks which have the same
    name are considered equal. This was done by overriding __eq__ and __ne__, see below.

    Be carefull with the extra "name" parameter. There is no check.
    If it is not unique your output will be broken. """
    #StaffGroups are normally Spanners. But they can be nested at will. So we have a situation here.
    ##1)Risk users forgetting to close or open a group
    ##2.Overly complex container situation with nested containers where multiple starts and endings are in the same container.
    ##1) was chosen.
    def __init__(self, parentWorkspace, name = None):
        super(Track, self).__init__(parentWorkspace = parentWorkspace)
        if name:
            self._uniqueContainerName = name

        self.container = [Appending(parentWorkspace)] #no Start() in tracks
        self.staffType = "Staff" #exports to "\new Staff {c d e}"  #TODO: Why is a layout option in here? If staffype becomes a RhythmStaff etc. we need to elimate pitches.
        self.nrOfLines = 5
        self.mergeWithUpper = False # Is this Track a voice or a standalone staff? Default False = Standalone.
        self.longInstrumentName = ""  #long instrument name, not unique.
        self.shortInstrumentName = ""  #short instrument name, not unique.
        self.voicePreset = "" #These are the lilypond voice presets. "" means surpressed output. Can be voiceOne, voiceTwo, voiceThree, voiceFour or oneVoice. OneVoice is the explicit version of "None"
        self.trackDirectiveWith = {} # at least one entry here creates a \with {} block. Only one block is allowed per staff so we can't use the other directives.
        self.selectedItems = [] #a shortcut list to all selected Items.
        self.hideLilypond = False
        self.lilypondStaffGroups = [] #TODO: ?
        self.lyrics = "" #empty lines get converted as new verses. this means a double \n[white]\n  with any whitespace in between removed.
        self.forceLyricsAbove = False
        self.verseCountCache = 0
        self.endingBar = '\\bar "|."'
        self.lilypondSize = "normal"  #normal, big, small, tiny.
        self.ambitus = False # Ambitus engraver
        self.exportExtractPart = "" #a string like the group. If you export in a special mode you get individual PDFs with these only.

        #Simple SMF Data
        #self.smfPort does not exist. Threre is only one port in standard smf.
        #Choose a default channel:
        channelsInUse = set([track.smfChannel[0] for track in self.parentWorkspace.score])
        for num in range(16): #0-15
            if not num in channelsInUse:
                self.smfChannel = [num,num]
                break
        else: #All channels in use.
            self.smfChannel = [0,0]
        self.smfMute = False
        self.smfSolo = False
        self.smfPatch = 0
        self.smfBank = 0
        self.smfVolume = 99 #max is 127. You can get up a little now.
        self.smfPan = 63 #0-63, 64-127

        #Jack Midi Data. From 0 to 127. Special Value is -1 which means "Don't set anything and let the enviroment/last value decide"
        self.jackChannel = [0,0]
        #self.jackMute = False  #not mirrored. Mute is mute.
        #self.jackSolo = False  #not mirrored. Solo is solo.
        self.jackPatch = -1
        self.jackBank = -1
        self.jackVolume = -1 #usually the volume is done in an audio mixer or the sampler/synth.
        self.jackPan = -1
        if self.parentWorkspace.parentSession.calfbox: #The Jack Midi output UUID
            self.calfboxUuid = self.parentWorkspace.parentSession.calfbox.newMidiOutPort(self.uniqueContainerName) #does not connect anything. Just creates a port. This remains until the track is deleted
            #self.calfboxUuid = self.parentWorkspace.parentSession.calfbox.cbox.JackIO.create_midi_output(self.uniqueContainerName) #does not connect anything. Just creates a port. This remains until the track is deleted
        else:
            self.calfboxUuid = None

    @property
    def uniqueContainerName(self):
        return self._uniqueContainerName

    @uniqueContainerName.setter
    def uniqueContainerName(self, value):
        """A copy of container.uniqueContainerName.
        This version integrates jack midi-out / Calfbox names"""
        oldValue = self.uniqueContainerName
        value = "".join([ch for ch in value if ch in ascii_letters+"1234567890" ]) #remove all non-chars to create valid lilypond syntax

        if oldValue not in self.parentWorkspace.uniqueContainerNames and not oldValue == "":
            self.parentWorkspace.uniqueContainerNames.append(oldValue)

        if value in self.parentWorkspace.uniqueContainerNames or value == "":
            return False
        else:
            self._uniqueContainerName = value
            if oldValue in self.parentWorkspace.uniqueContainerNames:
                self.parentWorkspace.uniqueContainerNames.remove(oldValue)
            self.parentWorkspace.uniqueContainerNames.append(value)
            if self.parentWorkspace.parentSession.calfbox:
                self.parentWorkspace.parentSession.calfbox.renameMidiOut(self.calfboxUuid, value)
            return True

    def __eq__(self, other):
        """Tracks are equal if the unique(!) name is the same."""
        return self._uniqueContainerName == other._uniqueContainerName

    def closeCalfboxMidiOut(self):
        """Hopefully this gets called correctly and can delete calfbox
        jack midi out ports"""
        if self.calfboxUuid:
            self.parentWorkspace.parentSession.calfbox.closeMidiOutput(self.calfboxUuid)

    def select(self, registerInTrackSelectedItemsList = True):
        """Select any item or container in a container. In the end it
        always results in selected items, not containers"""
        cur = copy.copy(self.parentWorkspace.score.cursors[self.parentWorkspace.score.container.index(self)]) #only copy cursor, not the items.
        cur.go_to_head()
        cur.resetToDefault()
        for x in cur.iterate():
            x.select(registerInTrackSelectedItemsList)

    def deselect(self, deleteMe = False):
        """This list does not define what is selected and what not.
        Its just a shortcut for deselecting and counting"""
        #for x in self.selectedItems:
        #    x.deselect(False) # Do not delete by active track...
        #    self.parentWorkspace.score.selection -= 1 #decrease the counter
        #self.selectedItems = [] #...delete all at once instead

        cur = copy.copy(self.parentWorkspace.score.cursors[self.parentWorkspace.score.container.index(self)]) #only copy cursor, not the items.
        cur.go_to_head()
        cur.resetToDefault()
        for x in cur.iterate():
            x.deselect(False) # Do not delete by active track...
            self.parentWorkspace.score.selection -= 1 #decrease the counter
        self.selectedItems = [] #...delete all at once instead

    def generateLyrics(self):
        """Returns lists, each one is a verse.
        Also transforms \n\n to a new verse and
        numbers to \"123\" """

        escaped = re.sub(r'(\d+)', r'"\1"', self.lyrics)
        escaped = escaped.replace("__,", " \\extendComma") #a special command, you need to include inc_lyricsextender.ly
        escaped = escaped.replace("__!", " \\extendExclaim") #a special command, you need to include inc_lyricsextender.ly
        escaped = escaped.replace("<i>", " \\override LyricText #'font-shape = #'italic ")
        escaped = escaped.replace("<n>", " \\override LyricText #'font-shape = #'normal-text ")
        escaped = escaped.split("\n\n")
        self.verseCountCache = len(escaped)
        return escaped

    def getLyricsAsList(self):
        """Return each syllabe as List member"""
        return [verse.replace(" -- ", "-- ").split() for verse in self.lyrics.split("\n\n")]

    def generateWithBlock(self):
        size = {
                "":"", #in case of error or manual deletion
                "normal": "",
                "small": """fontSize = #-3 \override StaffSymbol #'staff-space = #(magstep -3)""",
                "big": """fontSize = #3 \override StaffSymbol #'staff-space = #(magstep 3)""",
                "tiny": """fontSize = #-7 \override VerticalAxisGroup #'minimum-Y-extent = #'(0 . 0) \override StaffSymbol #'staff-space = #(magstep -7)""",
        }[self.lilypondSize]

        ambitus = '\\consists "Ambitus_engraver"' if self.ambitus else ""
        instNames = self.generateLilypondStaffNames() if (self.longInstrumentName or self.shortInstrumentName) and not ("\\new PianoStaff <<\n" in self.directivePre.values() or "\\new GrandStaff <<\n" in self.directivePre.values()) else ""
        if self.trackDirectiveWith or size or ambitus or instNames:  #excludes normal. good.
            return " \\with { " + size  + " ".join(self.trackDirectiveWith.values()  ) + ambitus + instNames + " } "
        else:
            return ""

    def generateLilypondStaffNames(self, prefix = ""):
        """Return the name of the staff, short and long format"""
        #TODO: If merged voice the instrument names should be combined. Must work for several merged voices.
        """
        longName = ""
        shortName = ""
        if self.longInstrumentName: #Long name
            longName = "\\set " + self.staffType + ".instrumentName = #\"" + self.longInstrumentName + "\""  #TODO: maybe self.staffType is wrong here. Does it work with rhytmic staff?
        if self.shortInstrumentName: #Short name
            shortName = "\\set " + self.staffType + ".shortInstrumentName = #\"" + self.shortInstrumentName + "\""
        return longName + " " + shortName
        """
        #PIano and Grand staff names follow a differenct placement and syntax:

        #Long and short instrument names
        if self.longInstrumentName:
            if self.longInstrumentName.startswith("\\markup"):
                longName = prefix + "instrumentName = " + self.longInstrumentName
            else:
                longName = prefix + "instrumentName = \"" + self.longInstrumentName + "\""
        else:
            longName = ""

        if self.shortInstrumentName:
            if self.shortInstrumentName.startswith("\\markup"):
                shortName = prefix + "shortInstrumentName = " + self.shortInstrumentName
            else:
                shortName = prefix + "shortInstrumentName = \"" + self.shortInstrumentName + "\""
        else:
            shortName = ""

        return " " + longName + " " + shortName + " "

    def generateLilypondVoice(self):
        """Generate the actual data, indenpendent of single voice or merged"""
        #self.parentWorkspace.lastDurationLyString = "" #reset last duration cache
        figuredBass = self.lilypondContainerName + "Continuo = \\figuremode { " + self.generateFiguredBass() + " }\n" if any([item.figures for item in flatten(self)]) else ""
        chordsymbols = self.lilypondContainerName + "Chordsymbols = \\new ChordNames \\chordmode { " + self.generateChordsymbols() + " }\n" if any([item.chordsymbols for item in flatten(self)]) else ""
        lyrics = "".join([self.lilypondContainerName + "Lyrics" + helper.num2word(verse[0]) + "= \\lyricmode { " + verse[1] + " }\n" for verse in enumerate(self.generateLyrics())]) if self.lyrics else "" #Generate one defition line for each verse

        self.parentWorkspace.lilypondExportTrackName = self.uniqueContainerName


        innerLilypond = self.generateInnerLilypond()

        if self.uniqueContainerName.lower() == "master":
            override = "\\override Voice.Dots #'stencil = ##f \\override Voice.TimeSignature #'stencil = ##f \\override Voice.KeySignature#'stencil = ##f \\override Voice.Clef #'stencil = ##f \\override Voice.BarLine #'stencil = ##f  \\override Voice.NoteHead #'stencil = ##f  \\override Voice.Stem #'stencil = ##f \\override Voice.Rest  #'stencil = ##f \\override Voice.MultiMeasureRestNumber  #'stencil = ##f \\override Voice.MultiMeasureRestText  #'stencil = ##f \\override Voice.MultiMeasureRest  #'stencil = ##f  "
            return self._uniqueContainerName + "= { " + override  + innerLilypond +  "}\n" + figuredBass + chordsymbols + lyrics

        else: #normal track
            #Only Appending() in container or even broken (0 objects)?. Empty Track, add one skip to avoid broken lilypond!
            #This does not prevent empty pages if there are only clefs and such in the lilypond output. But at least that is not an error.
            fallback = " s4 " if len(self) <= 1 else ""

            #if there is no keysig at the track beginning force C Major so transpose will work correctly. C Major is the lilypond default.
            keyFallback =  "\\key c \\major " if not ( any(type(x) is KeySignature for x in self[:5])) else ""

            voicePreset = "\\" + self.voicePreset + " " if self.voicePreset else ""

            #If this the beginning of a Piano or Grand Staff group we want a centered name. In this case the normal name generation is already switched off.
            #Luckily PianoStaff and GrandStaff are somewhat compatible so we can use GrandStaff.instrumentName for both
            if ("\\new PianoStaff <<\n" in self.directivePre.values() or "\\new GrandStaff <<\n" in self.directivePre.values()):
                groupedStaffNames = self.generateLilypondStaffNames(prefix = "\\set GrandStaff.")
            else:
                groupedStaffNames = ""

            #If the music ends with a custom barline use this, if not add an ending bar.
            ending = "" if  re.search(r'(\\bar ".+"\s*)$', innerLilypond) else self.endingBar

            linesNr = "\\override Staff.StaffSymbol #'line-count = #" + str(self.nrOfLines) + " " if not self.nrOfLines == 5 else ""
            #return self._uniqueContainerName + "= { " + self.generateLilypondStaffNames() + linesNr + voicePreset + keyFallback + innerLilypond + fallback +  ending + "}\n" + figuredBass + chordsymbols + lyrics
            return self.lilypondContainerName + "= { " + groupedStaffNames + linesNr + voicePreset + keyFallback + innerLilypond + fallback +  ending + "}\n" + figuredBass + chordsymbols + lyrics

    def exportLilypond(self):
        """This is an override over items.Lilypond.exportLilypond().
        The only one in the project so far.
        We need it because tracks don't export directivePst values
        in the usual place but in track.generateLilypond()"""
        if self.lilypond:
            return " ".join(self.directivePre.values()) + self.lilypond + " ".join(self.directivePst.values())
        else:
            return " ".join(self.directivePre.values()) + self.generateLilypond() #Maybe get lilypond and duration as separate parameters

    def generateLilypond(self):
        """Generate the line that it is used in \score << >> to
        combine all music blocks, merged voices and arrange them.

        The master track is not in here, but exported
        by score.generateLilypond(). They only must be attached to
        the the last-in-a-row track in a mergeWithUpper group so they
        1)End not up n times in the export and 2)correctly merge the
        master voice to the other ones. If master is the first
        it will not use repeat signs for example."""

        #Set the current cursor so that lilypond export knows about it.
        figuredBass = " \\new FiguredBass \\" + self.lilypondContainerName + "Continuo " if any([item.figures for item in flatten(self)]) else ""
        chordsymbols = "\\" + self.lilypondContainerName + "Chordsymbols " if any([item.chordsymbols for item in flatten(self)]) else ""
        #Lyrics
        lyricsAfter = lyricsBefore = ""
        if self.lyrics:
            lyrics = "".join(["\\new Lyrics = \"" + self.lilypondContainerName + "Lyrics" + helper.num2word(verseCounter) + "\" {s4} " for verseCounter in range(self.verseCountCache)])
            if self.forceLyricsAbove:
                lyricsBefore = lyrics
            else:
                lyricsAfter = lyrics

                figuredBass = " \\new FiguredBass \\" + self.lilypondContainerName + "Continuo " if any([item.figures for item in flatten(self)]) else ""
                chordsymbols = "\\" + self.lilypondContainerName + "Chordsymbols " if any([item.chordsymbols for item in flatten(self)]) else ""

        if self.uniqueContainerName.lower() == "master":
            return chordsymbols + lyricsBefore + " \\new Voice = \"" + self.lilypondContainerName + "\" " + "\\" + self.lilypondContainerName + " " + lyricsAfter +  figuredBass

        #normal track
        if self.mergeWithUpper: #This defines only the beginning << of a merged track. The ending >> is done in score.generatelilypond()
            pre = "" # x is voice of another staff.
        else:
            pre = " \\new " + self.staffType + ' = "' + self.lilypondContainerName + '_Staff" ' + self.generateWithBlock() + " << " #x is a stand alone staff beginning

        voiceMid = " ".join(self.directiveMid.values())

        transpose = self.generateLilypondTranspose()
        #return chordsymbols + lyricsBefore + pre  + voiceMid + master + voiceMid + " \\new Voice = \"" + self._uniqueContainerName + "\" " + transpose  + "\\" + self._uniqueContainerName + " " + lyricsAfter +  figuredBass
        return chordsymbols + lyricsBefore + pre  + voiceMid + " \\new Voice = \"" + self.lilypondContainerName + "\" " + transpose  + "\\" + self.lilypondContainerName + " " + lyricsAfter +  figuredBass


    def generateSave(self):
        childDict = {}
        childDict["staffType"] = self.staffType
        childDict["nrOfLines"] = self.nrOfLines
        childDict["mergeWithUpper"] = self.mergeWithUpper
        childDict["longInstrumentName"] = self.longInstrumentName
        childDict["shortInstrumentName"] = self.shortInstrumentName
        childDict["trackDirectiveWith"] = self.trackDirectiveWith
        childDict["smfChannel"] = self.smfChannel
        #childDict["selectedItems"] = self.selectedItems Selection is not saved
        childDict["smfMute"] = self.smfMute
        childDict["smfSolo"] = self.smfSolo
        childDict["smfPatch"] = self.smfPatch
        childDict["smfBank"] = self.smfBank
        childDict["smfVolume"] = self.smfVolume
        childDict["smfPan"] = self.smfPan
        childDict["hideLilypond"] = self.hideLilypond
        childDict["voicePreset"] = self.voicePreset
        childDict["lyrics"] = self.lyrics
        childDict["forceLyricsAbove"] = self.forceLyricsAbove
        childDict["lilypondSize"] = self.lilypondSize
        childDict["exportExtractPart"] = self.exportExtractPart
        childDict["ambitus"] = self.ambitus
        childDict["jackChannel"] = self.jackChannel
        childDict["jackPatch"] = self.jackPatch
        childDict["jackBank"] = self.jackBank
        childDict["jackVolume"] = self.jackVolume
        childDict["jackPan"] = self.jackPan
        return childDict

class Score(Container):
    """The biggest and highest level container. Only Tracks in here
     Score also holds all higher level cursor functions because all cursors in a score move are in sync.
     Tracknumbers and cursor numbers are in sync if only internal functions are used to create and delete Tracks.
     Tracks are exported and parsed in the order they are created or swapped. Container index is export position."""
    #TODO: Maybe its wrong to have the complex score cursors in here and not in its own class. Only functions which involve self.container need to be in here.
    def __init__(self, parentWorkspace):
        super(Score, self).__init__(parentWorkspace = parentWorkspace)
        self.activeCursor = None #A cursor instance: What is the currently active cursor (aka. which Track is active). New cursors get added through addTrack
        self.masterCursor = None #A cursor instance. An instance is only added during exporting (playback, for example). During work the mastertrack and its cursors are just normal tracks. Not saved.
        self.cursors =  [] # All cursors instances. One for each Track.
        self.container = [] #No appending here. All Track() instances. This is also just "self"
        self.nonTrackContainers = {} #All containers used in this score. For a quick access. key is the container instance, value is the instance count.
        self.nonTrackContainer = {} #a temporary storage for non-track containers during ly export. uniqueContainerName is the key, a lilypond-string is the value. this is NOT related to self.nonTrackContainerS. If you search for something related to containers which is not export Lilypond ignore this variable and the functions connected to it. This is only for lilypond export.
        self.substitutions = {}  #Dynamic. Has Container() in it. This is saved and loaded with the score file. A gui should offer add/remove/edit options for the content.
        self.selection = 0 #0 means no selection.
        self.subtext = "" #Subtext, word wrapped under the ly score, score. Goes directly to Lilypond export.
        self.defaultPerformanceSignature = PerformanceSignature(parentWorkspace)
        self.master = None #A track. An instance is only added during exporting (playback, for example). During work the mastertrack and its cursors are just normal tracks. Not saved.
        self.stateRegistry = [] #undo stack.
        self.oldSnapshots = [] #for redo. Makes only sense if there is no data change between undo and redo. This gets cleaned by the api or other high level functions.
        self.splitByToHeader = True #If True: this score, when exported to lilypond with part-splitting enabled, will put its split-by value (like "Strings" or "Oboe" etc.) in the header to the instruments field
        self.emptyHeader = Header(parentWorkspace) #we need an always empty, never editable header for lilypond export. When it is decided if this is a standalone piece or score and which header position to use, in the \score or outside.
        self.header = Header(parentWorkspace)

        #TODO: This confused calfbox track creation and was also a waste of resources. During load of an lbjs file two extra tracks got created and deleted.
        #After a testing period, remove completely.
        #self.addTrack() #Finally add a track. Each score needs at least one.


    def longestTrackDuration(self):
        return max([tr.duration for tr in self])

    def makeTracksEqualLengthWithPlaceholders(self):
        """Make all tracks the same length by inserting placeholders
        at the end. This does modify the score in place and is intended
        for a merged load for collection.

        The only problem is that this does not use the cursor so it is
        hard to use for real editing. And we can't send any GUI signals
        from here as well, so even if we use the cursor it would not
        update."""
        longest = self.longestTrackDuration()
        for tr in self:
            restTicks = longest - tr.duration
            if restTicks:
                item = Rest(self.parentWorkspace, 384, "s")
                item.guessDurationAbsolute(restTicks) #sets the new duration as well.
                tr.container.insert(-1, item)

    def allTrackNames(self):
        return [tr.uniqueContainerName for tr in self]

    def select(self):
        """This is different from any other container because it does
        not rely on a cursor position anymore so item.select would not
        know where to place the selected item in self.selectedItems"""
        for x in self: #tracks!
            x.select(registerInTrackSelectedItemsList = False)
        #recreate the selectedItems per-track lists by copying the actual score and flattening the tracks.
        for track in self.container:
            track.selectedItems = list(flatten(track.container)) #flatten a copy(!) of the track container and make it the new selectedItems, overwriting all the old selected data.
            #track.selectedItems = [x for x in list(flatten(track.container)) if not (type(x) is Start and type(x) is End)] #flatten a copy(!) of the track container and make it the new selectedItems, overwriting all the old selected data.
            track.selectedItems.pop() #delete the last item in the list, which is the final appending position which got copied here because we did not used item.select().

    def deselect(self, opt= False):
        """Deselect any item."""
        for tr in self.container:
            tr.deselect(False)
        self.selection = 0

    #Synced Cursor Insert and score in all Tracks.
    def insertAtCursor(self, item):
        r = self.activeCursor.insert(item) #goes right afterwards so we need the other cursors to follow
        self.comeLittleCursors()
        return r

    def syncCursors(self):
        """After inserting or modifying items linked/container
        items in multiple tracks the tick positions are wrong.
        Update all tracks"""
        self.activeCursor.syncTickIndex()
        for x in [cur for cur in self.cursors if not cur is self.activeCursor]:
            x.go_to_head()
            x.resetToDefault() #Reset all counters and gatherers to 0.
        self.comeLittleCursors()

    def comeLittleCursors(self):
        """All non-active cursors try to keep up with the active one and try to reach the active tickindex.
        This is for going up and down between the tracks so you end up near your starting position.
        Technically it is to avoid a complete loop from the beginning each time you change the track to reach the old tickindex.
        This way the processing load is divided into incremental steps, executed at user action speed (much time for processing!)"""
        for x in self.cursors: # Try moving the current cursor again is redundant. But its a quick operation (just "pass" in the cursor class) and the cleaner code is worth the redundancy.
            x.goToTickIndex(self.activeCursor.tickindex, returnOnError = False)

    def left(self):
        """Cursor score in sync. The active cursor steps left, gets the tickindex and all other cursors try to reach the tickindex."""
        returnvalue = self.activeCursor.left() #executes as well
        self.comeLittleCursors()
        return returnvalue #Return True or False to see if track beginning or not.

    def right(self):
        """Cursor score in sync. he active cursor steps right, gets the tickindex and all other cursors try to reach the tickindex."""
        returnvalue = self.activeCursor.right() #executes as well
        self.comeLittleCursors()
        return returnvalue #Return True or False to see if track end or not.

    def measureLeft(self):
        """Move all cursors one measure of currently active cursor.ticks backwards.
        Going over a smaller timesig or different timesigs in tracks may result in valid but unexpected positions.
        Don't use for automated work except you know exactly what to expect (in a simple score with only one timesig)"""
        oneMeasure = self.activeCursor.prevailingTimeSignature[-1].oneMeasureInTicks
        start = self.activeCursor.tickindex
        while self.activeCursor.tickindex > start - oneMeasure: #Go right until the current tick position is equal or bigger than the destination
            if not self.activeCursor.left(): #executes as well. First move the active cursor, later sync the others. So the cursor at least tries to go left. At the beginning of track it is executed twice during this loop.
                break # beginning of track
        self.comeLittleCursors()

    def measureRight(self):
        """see score.measureLeft, only forward."""
        oneMeasure = self.activeCursor.prevailingTimeSignature[-1].oneMeasureInTicks
        start = self.activeCursor.tickindex
        while self.activeCursor.tickindex < start + oneMeasure: #Go right until the current tick position is equal or bigger than the destination
            if not self.right(): #executes as well. First move the active cursor, later sync the others. So the cursor at least tries to go right. At the end of staff it is executed twice during this loop.
                break # end of track
        self.comeLittleCursors()

    def head(self):
        for x in self.cursors:
            x.go_to_head()
            x.resetToDefault() #Reset all counters and gatherers to 0.

    def tail(self):
        while self.activeCursor.right(): #first the active cursor should go right.
            pass
        self.comeLittleCursors()

    def toTickIndex(self, ticks):
        """Go to a tickindex."""
        self.activeCursor.goToTickIndex(ticks, returnOnError = False)
        self.comeLittleCursors()

    def toIndex(self, index):
        """Go to the flat cursor index in the current track."""
        ret = self.activeCursor.goToIndex(index) #ret says if track start/end was encountered
        if ret:
            self.comeLittleCursors()
        else:
            return False

    def beginning(self):
        """Return a list of list. Each sublist is a tracks content
        which holds all items up to the first item with a duration.

        Use this to figure out if a tempo sig, timesig etc. was set
        before the first real music event (note, rest etc.)"""
        l = []
        for cursor in self.cursors:
            l.append(cursor.beginning()) #track.beginning() returns a list
        return l

    #Tracks#
    def currentTrackIndex(self):
        """Return the index of the current track,
        which is the same as the active cursor"""
        return self.cursors.index(self.activeCursor)

    @property
    def currentTrack(self):
        """return the instance of the active track"""
        return self[self.currentTrackIndex()]

    def trackDown(self):
        """Change the active cursor to a higher index"""
        now = self.currentTrackIndex()
        nowTicks = self.activeCursor.tickindex
        try:
            self.cursors[now + 1]
            hadDur = self.activeCursor.get_item().duration
            self.comeLittleCursors()
            self.activeCursor = self.cursors[now + 1]
            if hadDur: #Adjust the cursor position to real music items with duration if we are on one.
                self.activeCursor.rightToFirstDuration()
            return True
        except IndexError:
            return False  #Already the last Track
        #self.toTickIndex(nowTicks)  #TODO: never executed. But everything seems to work fine?!

    def trackUp(self):
        """Change the active cursor to a lower index"""
        now = self.currentTrackIndex()
        #nowTicks = self.activeCursor.tickindex
        if now - 1 < 0:
            self.activeCursor = self.cursors[0]
            return False
        else:
            hadDur = self.activeCursor.get_item().duration
            self.comeLittleCursors()
            self.activeCursor = self.cursors[now - 1]
            if hadDur: #Adjust the cursor position to real music items with duration if we are on one.
                self.activeCursor.rightToFirstDuration()
            return True
        #self.toTickIndex(nowTicks) #TODO: never executed. But everything seems to work fine?!

    def lastTrack(self):
        """Change the active cursor to the highest index"""
        while self.trackDown():
            pass

    def firstTrack(self):
        """Change the active cursor to the lowest index"""
        while self.trackUp():
            pass

    def removeEmptyTracks(self, removeHiddenLilypond = False, muteSolo = False):
        """Remove all tracks which have no duration items or just an
        upbeat.
        Except the master track, which will be not touched.

        Also have the option to remove hidden Lilypond Tracks.
        So far this is used by Score.convertToExportScore( only.
        Default is off since it is printing only.

        Do not use as convenience "clean my score" function!
        This method is typically used with functions that create
        copies for exporting. If used on a real score it will indeed
        delete the empty Tracks but send no message to anywhere (like
        the API does). """
        if muteSolo: #Create the solo mask.
            soloMode = any([tr.smfSolo for tr in self])

        emptyCursors = []
        for cursorEmptyTrack in self.cursors:
            if ((muteSolo and cursorEmptyTrack.nested_list.smfMute) or (muteSolo and (soloMode and not cursorEmptyTrack.nested_list.smfSolo)) or (cursorEmptyTrack.nested_list.isEmpty() or (removeHiddenLilypond and cursorEmptyTrack.nested_list.hideLilypond))) and not cursorEmptyTrack.nested_list.uniqueContainerName.lower() == "master":
                emptyCursors.append(cursorEmptyTrack)

        for emptyCursor in emptyCursors:
            self.cursors.remove(emptyCursor) #delete cursor
            self.container.remove(emptyCursor.nested_list) #delete track from score container

    def getTrackPropertyGroups(self, trackProperty):
        """Returns a set with all the different properties.
        If you ask for track.group you'll get a set with all the
        group names"""
        s = set()
        for track in self.container:
            if not track.uniqueContainerName.lower() == "master":
                s.add(getattr(track, trackProperty))
        return s

    def filterTracks(self, trackProperty, filterList):
        """Delete all tracks that do not match.
        Except the master track, which will be not touched.
        Example: trackProperty = exportExtractPart
        filterList = ["piano"] will delete everything except piano
        parts which most likely be the left and right hand tracks and
        additional merged voices."""
        emptyCursors = []
        for cursorToCheck in self.cursors:
            if not (getattr(cursorToCheck.nested_list, trackProperty) in filterList or cursorToCheck.nested_list.uniqueContainerName.lower() == "master"):
                emptyCursors.append(cursorToCheck)

        for emptyCursor in emptyCursors:
            self.cursors.remove(emptyCursor) #delete cursor
            self.container.remove(emptyCursor.nested_list) #delete track from score container

    #def getMultipleFilteredScores(self, trackProperty):
    #    filterlist = self.getTrackPropertyGroups(trackProperty)
    #actually, not. FilterTracks is in place!


    def removeWrongLilypondStaffgroups(self, showWarning = True):
        """Lilypond does not like it when you open staffgroups but do
        not close them, or vice versa. This function takes care of
        these broken enclosures to guarantee a technically working
        Lilypond Export. This is done by deleting wrong staff-group
        directives on the track, in place.
         While this function could be used to clean a broken Score
        it is intended for a copied Score during Lilyypond export,
        so the original data gets not modified.


        This can happen through a user accident, in this case the
        warning should help,
        Or it will happen most often by exporting parts, e.g. single
        orchestral instruments and not the whole score.
        Example: Often the first and second violin are grouped together
        by a grand-staff parentheses: {
        If you create PDFs for the violins alone either the beginning
        or the end for the grand staff is missing, which results in a
        lilypond error.
        Or the common Orchestral Bracket around the complete Score.
        It begins in the first staff and ends in the last. Those two
        are most definetly not on one PDF if you are doing part export.

        In these cases we need to remove the beginnings/ends. If the
        open/close situation matches, e.g. by exporting the two tracks
        of a Piano Staff, they can stay.

        Since removal is exptected and intended the warnings can be
        disabled.
        """

        #Check if nesting/open/close is correct. In the end we get a list of Start Directives that have not been closed. Closed, but not open, will be deleted directly.
        startToDelete = []
        for track in self.container:
            if track.uniqueContainerName.lower() == "master":
                for key in list(track.directivePre.keys()):
                    if key.startswith("StaffGroup"):
                        del track.directivePre[key]
                for key in list(track.directivePst.keys()):
                    if key.startswith("StaffGroupEnd"):
                        del track.directivePst[key]
                continue #next track.

            for key in list(track.directivePre.keys()):
                if key.startswith("StaffGroup"):
                    startToDelete.append((track, key))

            for key in list(track.directivePst.keys()):
                if key.startswith("StaffGroupEnd"):
                    if startToDelete: #there are still StaffGroup starts?
                        startToDelete.pop() #remove the last opened Start. This one is valid and will be exported because it will be closed as well.
                    else:
                        del track.directivePst[key]
                        if showWarning:
                            warnings.warn("Lilypond Staff Group Directive" + str(key) + " in track " + track.uniqueContainerName + " was opened but not closed and therefore temporarily disabled to gurantee Lilypond output. Please correct manually by matching StaffGroup open and close directives.")

        #There are only Opened, but not closed, Directives left. Delete them.
        for track, key in startToDelete:
            del track.directivePre[key]
            if showWarning:
                warnings.warn("Lilypond Staff Group Directive" + str(key) + " in track " + track.uniqueContainerName + " was opened but not closed and therefore temporarily disabled to gurantee Lilypond output. Please correct manually by matching StaffGroup open and close directives.")

    def parseAllTracks(self, command):
        """
        command() gets two parameters:
        the current item and the active cursor.
        The parsing stops if command returns the string "break".

        All other positive return values will be saved in a list
        which is returned in the end.
        It is a list of list, each track is one parent list.
        Return values are in the child list.

        The search is track based. Therefore the "break" keyword
        is of limited use if you want to search something until a
        certain amount of musical 'time' has passed because that search
        would be column based ("Parallel search in all tracks")

        The cursor stays at its position while parsing. This is not a
        "move cursor to found item" function.

        Best treat this function as "read only".

        Even a "simple" modifying task like "toggle Selection" could
        fail when dealing with links and containers. In this case an
        exception could be made if "Selection = True" is enforced.
        But even that has risks. If you want to select every second note
        and you only have links in the track it will constantly select
        and deselect the same note.
        The warning has been made… now I can use it myself to select :)

        In other words: you should not use "tone up" as command because
        linked items will be shifted up each time they are encountered
        The api commands like applyToSelection take care of these cases.

        Achtung! This does not work at all if the order is modified
        by the commands, especially if it is a duration change.
        In that case the old cursors will be loaded and the
        tickindex does not work anymore.
        If you want to modify the data use the api ApplyToSelection
        or a variant.
        """
        #TODO: Probably work on unfold score to eliminate container and linked problems.
        trackIndex = self.currentTrackIndex() #save the track position
        startTick =  self.activeCursor.tickindex #Save the tick index, nothing more. Work on the original cursors.
        gatherer = []
        #First prepare the structure that will be returned later. We need to build the empty one before gathering so it matches the track count.
        gatherer = [list() for tr in self.cursors] #we only need the number of cursors/tracks not the data
        try:
            for index, cursor in enumerate(self.cursors):
                self.activeCursor = cursor  #Set the active cursor so other functions (like the pitch module) can rely on it.
                self.activeCursor.resetToDefault()

                for obj in self.activeCursor.iterate():
                    ret = command(obj, self.activeCursor)
                    if ret == "break":
                        # raise an exception to break out of the nested loop
                        # this will not discard any gathered data (parentlist/childlist until now)
                        raise StopIteration()
                    elif ret:
                        gatherer[index].append(ret)
        except StopIteration:
            pass

        #Restore the old position. This should not have changed if the user/programmer read the docstring.
        self.activeCursor = self.cursors[trackIndex]
        self.toTickIndex(startTick)
        return gatherer

    def parseAllColumns(self, command):
        """
        See parseAllTracks, but column based:
        Track 2, Item 1 is considered 'earlier' than Track 1, last item.

        But it returns the same structure as parseAllTracks. Only useful
        if you use the 'break' keyword to stop the search at a certain
        point.

        Does not move the cursor

        Another difference is that the active cursor stays the same
        during parsing (contrary to parseAllTrack where it does not
        matter if you use the cursor parameter object or the active
        cursor). Here you have to use the cursor parameter object.
        """
        #TODO: replace tickindex with cursor Index.
        trackIndex = self.currentTrackIndex() #save the track position
        startTick =  self.activeCursor.tickindex #Save the tick index, nothing more. Work on the original cursors.
        #First prepare the structure that will be returned later. We need to build the empty one before gathering so it matches the track count.
        gatherer = [list() for tr in self.cursors] #we only need the number of cursors/tracks not the data
        for cursor in self.cursors:
            cursor.go_to_head()
            cursor.resetToDefault()

        #The real action. Two loops over all cursor in sequence. The first pass checks the data the second equalises unequal tick values so that we really follow a timeline.
        while True:
            currentTicks = [] #reset each time
            try:
                for index, cursor in enumerate(self.cursors):
                    ret = command(cursor.get_item(), cursor)
                    if ret == "break":
                        # raise an exception to break out of the nested loop. this will not discard any gathered data (parentlist/childlist until now)
                        raise StopIteration()
                    elif ret:
                        gatherer[index].append(ret)
                    if cursor.right(): #Only add the current tickindex if not at the end of track. Otherwise this will be always the minimum because other cursors advance further and we get an endless loop.
                        currentTicks.append(cursor.tickindex)
                if not currentTicks: #the search ends when the end of the track for every cursor was reached which means none of the cursors added its tickindex to this list.
                    raise StopIteration() #stop search. All cursors are at the end of their tracks.
                #Second pass. reset cursors that have a higher tickindex than the minimum
                for cursor in self.cursors:
                    if cursor.tickindex > min(currentTicks):
                        cursor.left()
            except StopIteration:
                #Restore the old position. This should not have changed if the user/programmer read the docstring.
                self.activeCursor = self.cursors[trackIndex]
                self.toTickIndex(startTick)
                return gatherer

    def analysisScore(self):
        """returns a special copy of the current score,
        based on unfoldscore.

        Format is one list with tuplets. All tuplets have the same
        length/the same number of items.

        Details:
        The difference is that rests get replaced with the last pitch
        in each track.

        Also all tracks are made equal tick length by repeating the last
        pitch"""
        newScore = self.unfoldScore()

        length = []
        for cursor in newScore.cursors:
            cursor.resetToDefault()
            cursor.go_to_head()
            track = cursor.nested_list
            track.container = [x for x in track.container if x.duration] #removes all non-music items. clefs and timesigs have no role here. All values like metrical position are already saved by unfoldScore
            length.append(len(track.container))  #the list we get from unfoldScore is already flat; Non-nested. This means we can use len(track.container) to get the actual number of items.
            last = Rest(self.parentWorkspace, 384, "s")
            firstRealPitch = False #once this is true
            leadingRests = []
            for obj in cursor.iterate():
                obj.last = last
                firstPitch = obj.notelist[0].pitch
                lastPitch = last.notelist[0].pitch
                if firstPitch and not (firstPitch == "r" or firstPitch == "s"): #rests have "r" or "s" , multi measure has 0
                    last = obj
                    if not firstRealPitch: #we had only rests until here aka. this is the first chord.
                        for x in leadingRests: #all rests so far are considered the same as the current note.
                            x.notelist = obj.notelist
                        firstRealPitch = True
                        leadingRests = None
                elif lastPitch and not (lastPitch == "r" or lastPitch == "s") : #current obj is a rest, last one was not (or already overwritten)
                    obj.notelist = last.notelist #copy the last valid pitches to
                else: #current obj is a rest, last one was a rest as well. only happens with leading rests from the track start.
                    if leadingRests is None: #and not a full or empty list!
                        raise RuntimeError("Rests within the track were recognized as leading rests. This should not happen.")
                    leadingRests.append(obj) #save this for later


        maxItems = max(length)
        endRest = Rest(self.parentWorkspace, 0, "s")
        #TODO
        return newScore


    def allContainerToCursorWalk(self):
        """Switches all cursor Walks to True in place"""
        def modify(lst):
            for elem in lst:
                if isinstance(elem, Container):
                    elem.cursorWalk = True
                    modify(elem)

        modify(self)

    def convertToExportScore(self, parts = None):
        """This export score keeps containers intact and is intended
        as a basis for Lilypond export.
        Returns a copy of the score which is modified:
        find the mastertrack and separate it from the other tracks.
        the masterCursor will be in score.masterCursor and the
        mastertrack in score.master.
        But the mastertrack and cursor get deleted from .cursors and
        .container

        parts is a tuple (trackProperty, value). Only those who match
        will be exported.
        """
        newScore = copy.deepcopy(self) #this copies all cursors including the prevailing sigs etc.
        #From here on everything is a copy. We can delete, add, do whatever we want:

        if parts:
            newScore.filterTracks(parts[0], parts[1]) #in place filtering.
            newScore.removeWrongLilypondStaffgroups(showWarning = False) #Repair StaffGroups. They will most likely break if exported as parts.
            if self.splitByToHeader:
                if not self.parentWorkspace.parentCollection: #not lbj export but lbjs
                    newScore.header.data["instrument"] = parts[1]
        else:
            newScore.removeWrongLilypondStaffgroups(showWarning = True) #Repair StaffGroups as a precaution, but warn the user if something is wrong. This needs manual correcting. We just export to get *any* result.

        #Don't process empty tracks. They are most likely just a place which was not edited yet but already created for later.
        newScore.removeEmptyTracks(removeHiddenLilypond = True)


        #Prevent the master track to be exported alone.
        if len(newScore.container) == 1 and newScore.container[0].uniqueContainerName.lower() == "master":
            return False

        #The following is not needed for Lilypond. The lilypond code is just a string as far as we are concerned here. There is no way back to Laborejo objects.
        #for cursor in newScore.cursors:
        #    cursor.go_to_head()
        #    cursor.resetToDefault()
        #    for obj in cursor.iterate():
        #        obj.flatCursorIndex = cursor.flatCursorIndex #for signals that go back from the unfolded Score to the original score we write down the unique position. This endObj.flatCursorIndex cannot be used in unfoldScore because it is here to remember the original structure, without any repeats.
        #        obj.uniqueContainerName = cursor.nested_list.uniqueContainerName #for the same reasons as above remember the container name.

        for tr in newScore.container:
            if tr.uniqueContainerName.lower() == "master":
                masterIndex = newScore.container.index(tr)
                newScore.masterCursor = newScore.cursors[masterIndex]
                newScore.cursors.pop(masterIndex)
                newScore.master = tr
                newScore.container.remove(tr)

                #flatten master list and filter it
                mastermembers = [KeySignature, Clef]
                newScore.master.container = [item for item in flatten(newScore.master) if type(item) not in mastermembers]
                break
        return newScore

    def convertToUnfoldedExportScore(self, parts = None):
        """This is used for Playback, Analysis etc.
        not for Lilypond since Lilypond keeps containers intact.
        Returns a copy of the score which is modified:
        find the mastertrack and seperate it from the other tracks.
        the masterCursor will be in score.masterCursor and the
        mastertrack in score.master.
        But the mastertrack and cursor get deleted from .cursors and
        .container

        parts is a tuple (trackProperty, value). Only those who match
        will be exported.
        """
        newScore = copy.deepcopy(self) #this copies all cursors including the prevailing sigs etc.
        newScore.playbackStartPosition = (self.activeCursor.nested_list.uniqueContainerName, self.activeCursor.flatCursorIndex)  #temporary variable to save the cursor position when playback was started.

        #TODO: Do we need the activeCursor for this or just a normal "for cursor in newScore.cursors" ?
        #When we are on a muted cursor we are going to be unable to find the start playback tick position later because the track gets deleted here.
        orgCur = self.activeCursor
        if self.activeCursor.nested_list.smfMute or self.activeCursor.nested_list.uniqueContainerName.lower() == "master":  #The current cursor is muted. We move the active cursor in an active
            self.firstTrack()
            if self.activeCursor.nested_list.smfMute or self.activeCursor.nested_list.uniqueContainerName.lower() == "master": #if not the first track is good and thats it.
                while self.trackDown():
                    if not self.activeCursor.nested_list.smfMute and not self.activeCursor.nested_list.uniqueContainerName.lower() == "master":
                        break
                    #In case that ALL tracks are muted! removeEmptyTracks will do the rest, we don't need to raise a warning here.
            #activecursor is now on an unmuted track or on the last track which is still muted. The first case is good, the latter bad but it doesn't matter here. export will abort soon...
            #we override the unique track name of the playbackStartPosition and the flatCursorIndex for the new active, unmuted, cursor/track
            newScore.playbackStartPosition = (self.activeCursor.nested_list.uniqueContainerName, self.activeCursor.flatCursorIndex)  #temporary variable to save the cursor position when playback was started.
        self.activeCursor = orgCur

        newScore.allContainerToCursorWalk() #switches all containers to cursorWalk in place
        if parts:
            newScore.filterTracks(parts[0], parts[1]) #in place filtering.

        #Prevent the master track to be exported alone.
        if len(newScore.container) == 1 and newScore.container[0].uniqueContainerName.lower() == "master":
            return False

        #This does not need a master cursor exchange yet because we don't deal with pitch or timesigs. If that changes don't forget to add
        #originalActiveCursor = self.activeCursor
        #self.activeCursor = cursor  #Set the active cursor so other functions (like the pitch module) can rely on it.


        #Step Two: Flatten out containers.
        #This must be done early because when we add the flatcursor index to the items later they must be
        #and when the master track items are added eventually they must not be added to containers since they get duplicated then.

        for cursor in newScore.cursors:
            cursor.go_to_head()
            cursor.resetToDefault()
            newFlatTrack = []
            tempRepeatPercents = {} #Percent Repeats for Containers
            for obj in cursor.iterate():
                typ = type(obj)
                if typ is not Start and typ is not End: #don't recursively copy the parent container in Start and End.
                    obj = copy.deepcopy(obj) #make the item unique
                obj.flatCursorIndex = cursor.flatCursorIndex #for signals that go back from the unfolded Score to the original score we write down the unique position. This endObj.flatCursorIndex cannot be used in unfoldScore because it is here to remember the original structure, without any repeats but to separate linked items.
                obj.cursorTickIndex = cursor.tickindex #For the same reasons as above remember at whick tick this was, without repeats. But handle linked items as individuals.
                obj.uniqueContainerName = cursor.nested_list.uniqueContainerName #for the same reasons as above remember the container name.

                if typ is End: #container End
                    obj.durationSwitch = False #duration is the parent container duration here. remove before processing. This is not relevant for playback or analysis, only for the cursor and possible GUIs.
                    if obj.parentContainer.repeatPercent > 1:
                        if obj in tempRepeatPercents:
                            tempRepeatPercents[obj] -= 1
                        else: #first encounter
                            tempRepeatPercents[obj] = obj.parentContainer.repeatPercent

                        if tempRepeatPercents[obj] > 1: #even after substraction there is still one round left. Go back:
                            while True:
                                if cursor.left():
                                    it = cursor.get_item() #this is not the deep copy variant.
                                    if type(it) is Start and it.parentContainer is obj.parentContainer:
                                        break
                                else: #track beginning?!
                                    raise RuntimeError("This should never have happened. You have a Container End Item without a container Start item. Please contact the developers with your file!")
                        else: #the subtraction right now was from 1 to 0. This was already the last round. Remove obj from the temp storage if we encounter it again.
                            del tempRepeatPercents[obj]
                else: #any other item
                    if not typ is Start: #container start
                        newFlatTrack.append(obj)

            cursor.nested_list.container = newFlatTrack

        for tr in newScore.container:
            if tr.uniqueContainerName.lower() == "master":
                masterIndex = newScore.container.index(tr)
                newScore.masterCursor = newScore.cursors[masterIndex]
                newScore.cursors.pop(masterIndex)
                newScore.master = tr
                newScore.container.remove(tr)
                break
        return newScore

    def unfoldScore(self, parts = None):
        """Return an unfolded version of self. Unfolded means:
        -Merge repeats and jumps of the mastertrack to all tracks.
        -Unfold repeats in each track (which can result from merged in
        master repeats)
        -Unfold repeats in the mastertrack itself.
        -Unfold all substitutions, even the no-unfold ones.
        -Execute playback triggers
        -Give Grace Notes and Ornaments a musical meaning and shorten
        other notes.
        Lookaheads:
        -Last note of a slurred section is marked as not legato.

        This unfolded score is the basis for playback export
        and analysis, but playback still needs to merge duration-objects
        from the master into main track.

        The unfolded score is a mess to work with. It is just good
        for left-to-right exporting or checking.
        """

        newScore = self.convertToUnfoldedExportScore(parts = parts)
        if not newScore: #only empty tracks, excluding master?
            return False
        originalActiveCursor = self.activeCursor

        newScore.removeEmptyTracks(removeHiddenLilypond = False, muteSolo = True)

        #Now only real tracks are in here

        if not newScore.container: #Only empty tracks.
            return False

        if newScore.masterCursor:
            #from now on work with the seperated mastertrack and all other tracks as a group.
            self.activeCursor = newScore.masterCursor  #Set the active cursor so other functions (like the pitch module) can rely on it.

            masterList = [] # a list of tuplets: (tickPos, object)

            newScore.masterCursor.resetToDefault()
            newScore.masterCursor.go_to_head()
            for obj in newScore.masterCursor.iterate():
                if obj.duration == 0 and not type(obj) in [Start, End, Appending, Clef, KeySignature, Clef]:
                    masterList.append((newScore.masterCursor.tickindex, obj))

            #Insert the content of the mastertrack to the other tracks so that the mastertrack can be ignored from now onw
            for cursor in newScore.cursors:
                self.activeCursor = cursor
                cursor.resetToDefault()
                cursor.go_to_head()
                lastPos = 0
                for pos, obj in masterList:
                    if not pos == cursor.tickindex:
                        if not cursor.goToTickIndex(pos, returnOnError = False): #Move to the right. Don't go back if the tickindex at the end is not the same as desired.
                            if cursor.tickindex >= pos:
                                raise ValueError("Target tick position", pos , "is beyond this tracks final tickindex", cursor.tickindex , " but the current tickindex is >= than the target position. This is logically not possible")
                            diff = pos - cursor.tickindex
                            cursor.insert(Rest(self.parentWorkspace, diff, "s"))

                        #If the tickindex was inside a multi measure rest or long note we have to split it, which is no problem for rests.
                        #In this case we already jumped over the tickindex and have to go one step back to get the long duration item.
                        elif cursor.tickindex > pos:
                            cursor.left()
                            it = cursor.get_item()
                            if type(it) is MultiMeasureRest:
                                cursor.delete()
                                #Since there is no way there is any change whatever inside a MMRest we only need to get the cursor prevailing data only once.
                                timeSigNominator = cursor.prevailingTimeSignature[-1].nominator #example: x/384. Upper number
                                timeSigDenominator = cursor.prevailingTimeSignature[-1].denominator #example: 4/x. Lower number
                                #Now that the MMRest is deleted insert small rests instead. A rule of thumb is
                                for measurenumber in range(it.measures): #For each measure do(counts from 0)
                                    for metricalPos in range(timeSigNominator): #do this n times per measures. for example 3 times in a 3/4 timesig.
                                        cursor.insert(Rest(self.parentWorkspace, timeSigDenominator, "s")) #insert a rest with the lower timesig number. 384 in a 3/4 timesig.

                        #Attempt a second time to go to the correct tickindex.
                        cursor.goToTickIndex(pos, returnOnError = False)
                        if cursor.tickindex != pos:
                            raise RuntimeError("The  item", type(obj), obj.exportLilypond() , "from the master track couldn't be inserted into track", cursor.nested_list.uniqueContainerName, "since there is an item:",  cursor.get_item(), cursor.get_item().exportLilypond(), " spanning over tickindex", cursor.tickindex, ". Please correct manually!")

                    cursor.insert(obj) #insert goes right automatically.


            cursorsPlusMasterCursor = newScore.cursors
            cursorsPlusMasterCursor.append(newScore.masterCursor)

        else: #no master track.
            cursorsPlusMasterCursor = newScore.cursors

        #Repeats
        #Only strictly valid repeats are considered. Every repeat close needs a repeat open. There are no implicit traditions here.

        def goToIndexAndSignatureInsert(cursor, position, jumpsAndRepeats):
            """Returns a bunch of signatuers that must be added to
            the currently created container"""
            start = cursor.tickindex
            cursor.goToIndex(position)
            ret = []
            for signature in cursor.getState()["prevailingSignatures"]: #Reset all signatures to the current state by inserting redundant items. They will be exportPlayback later.
                ret.append(signature)
            jumpsAndRepeats.append((start, cursor.tickindex))
            return ret

        maxUnfoldedTrackLenghtsForRepeats = {}  #key is the max unfolded tick length. value are the repeatLists

        for cursor in cursorsPlusMasterCursor:
            ##### First Pass ####
            #Copy linked items to unqiue items.
            #Gather Information about alternating ends [1… :|| [2…
            #Lookahead like "not legato at the end of slur" markers.
            self.activeCursor = cursor
            cursor.go_to_head()
            cursor.resetToDefault()
            firstPassContainer = [] #For first pass: a new, flat, data structure that will replace the track container in newScore
            lastPassedEnding = None
            alternateEndingGroupCounter = 0 #a counter
            alternateEndingsDict = {} # A dict of dicts. Key is the group index above, value is another dict with: key = number of ending, value = flat Cursor Index

            for endObj in cursor.iterate():
                #Create copies of new linked items, container items etc. Create copies for all items. This is much more robust.
                #Achtung: This only works because we are adding "endObj" at the end to a new list. This gets not written back to the original track list.
                endObj = copy.deepcopy(endObj)
                endTyp = type(endObj)
                plainObjectAndNotSubstituted = True # We assume a standard item, not a chord with substitution.

                if endTyp is Chord and endObj.substitution:
                    plainObjectAndNotSubstituted = False #switch so that the parent item itself will not be added.
                    firstPassContainer += endObj.unfoldSubstitution(cursor.prevailingKeySignature[-1], flatCursorIndex = cursor.flatCursorIndex) #Add the returned list of substitution items instead of the parent item. Also give all items the flatCursorIndex of the substitution.

                elif endTyp is AlternateEnd:
                    lastPassedEnding = endObj

                    if 1 in endObj.endings:
                        alternateEndingGroupCounter += 1
                    endObj.alternateEndingGroup = alternateEndingGroupCounter
                    if not alternateEndingGroupCounter in alternateEndingsDict:
                        alternateEndingsDict[alternateEndingGroupCounter] = {}
                    for end in endObj.endings:
                        alternateEndingsDict[alternateEndingGroupCounter][end] = cursor.flatCursorIndex

                elif (endTyp is RepeatClose or endTyp is RepeatCloseOpen):
                    if lastPassedEnding:# and len(lastPassedEnding.endings) >= 2: #add additional repeats to the next repeat close if this is ending [1,2,3 [4...
                        endObj.repeat = len(lastPassedEnding.endings)   #force the repeat count to be exactly the sum of ending numbers.
                    lastPassedEnding = None

                #Finally add the item if it wasn't substituted. Else it was already added a few lines above.
                if plainObjectAndNotSubstituted:
                    firstPassContainer.append(endObj)


            #Get the last ending(number) for each group and save it as [-1]
            for groupIndex, positionDict in alternateEndingsDict.items():
                flatPos = sorted(positionDict.items())[-1][1]  #from the last tuple in the sorted, converted list get the second, which is the flat cursor index of the last AlternateEnding of this group
                alternateEndingsDict[groupIndex][-1] = flatPos
            cursor.nested_list.container = firstPassContainer #replace original, nested, data list with flat, unfolded new one.
            #We now have a dict with all groups, endings and cursor positions. If we encounter an AlternateEnd object after a repeat in the main loop below we can get its group and jump to the next AlternateEnd in line


            #####Second Pass: Main Pass####
            #reset again and start unfolding
            cursor.go_to_head()
            cursor.resetToDefault()
            activated = True #Repeats are activated by default. Can be deactivated by triggers.
            openStack = [] # a stack with plain-track-indices to indicate the position of the last repeat open |:
            closeDict = {} # a dict for repeat close. key is the plain-track-index, value is the number of repeats left. The initial value comes from the repeat close object.
            segnoPos = None
            gotosegno = True #Once segno is done this is switched on so the next time D.S. is ignored.
            gotocapo = True
            breakAt = None
            alternativeEndingsGroupsStatus = {} #Key = group number, Value = Ending number status. Keeps track what of what was the last alternative ending so that nonlinear structures like [1,3 [2,4  work.
            newContainer = [] #For second pass: a new, flat, data structure that will replace the track container in newScore
            lastParsedChord = None
            subitoDynamic = None #sfz, sfp etc.
            jumpsAndRepeats = [] # a list of all jumps and repeats. tuples with (from, to) tickindex.

            ########Start Iterating over the items and deciding where to jump back and forth
            for obj in cursor.iterate():
                #print (cursor.flatCursorIndex, obj.generateLilypond())
                typ = type(obj)
                if typ is Chord:
                    #Next iteration. Save the current object
                    #previousObjWithDuration = obj
                    if subitoDynamic:
                        obj.subitoDynamicKeyword = subitoDynamic
                        subitoDynamic = None

                elif typ is Rest:
                    pass #performance. Skip the whole chain.

                #elif typ is Ornament:

                elif typ in [SlurOff, PhrasingSlurOff]:
                    #We need to mark the last encountered chord as "not legato" which is a playback exception.
                    if lastParsedChord:
                        lastParsedChord._lookaheadMarkers["slurEnd"] = True
                    else:
                        raise RuntimeError("There was a slur close without even a chord before. This makes no sense.")

                elif typ is SubitoDynamicSignature:
                    subitoDynamic = obj.expression

                ###Handling of Jumps.
                elif typ is AlternateEnd:
                    if activated:
                        if not obj.alternateEndingGroup in alternativeEndingsGroupsStatus :  #first encounter of a group, typically. ending number one
                            alternativeEndingsGroupsStatus[obj.alternateEndingGroup] = 0 #initialize
                        alternativeEndingsGroupsStatus[obj.alternateEndingGroup] += 1
                        nextPos = alternativeEndingsGroupsStatus[obj.alternateEndingGroup]
                        newContainer += goToIndexAndSignatureInsert(cursor, alternateEndingsDict[obj.alternateEndingGroup][nextPos], jumpsAndRepeats)

                    else: #not activated and we encounter an ending. We need to jump to the last ending now.
                        lastPos = alternateEndingsDict[obj.alternateEndingGroup][-1] #-1 is the key in the subdict for the position of the last Ending.
                        newContainer += goToIndexAndSignatureInsert(cursor, lastPos, jumpsAndRepeats)
                elif typ is Fine and breakAt is "Fine":
                    break
                elif typ is Segno:
                    segno = cursor.flatCursorIndex
                elif typ is GotoSegno:
                    if gotosegno and (segno or segno == 0):
                        newContainer += goToIndexAndSignatureInsert(cursor, segno, jumpsAndRepeats)
                        activated = not (obj.disableRepeat)
                        gotosegno = False
                        breakAt = obj.until
                        obj = cursor.get_item() #get the new item when for appending to newContainer a few lines further #TODO: Grammar
                        typ = type(obj)
                        closeDict = {} #reset all repeats. Jumping over repeats is done via the gotocapo switch, so this is safe and needed for alternate endings.
                        for gr, value in alternativeEndingsGroupsStatus.items(): #re-initiliaze/reset all alternate ending status
                            alternativeEndingsGroupsStatus[gr] = 0
                elif typ is GotoCapo:
                    if gotocapo:
                        activated = not (obj.disableRepeat)
                        gotocapo = False
                        breakAt = obj.until
                        newContainer += goToIndexAndSignatureInsert(cursor, 0, jumpsAndRepeats) #the problem now is that the first item needs to get processed again but it will continue with the next item. It will be added later, but not processed.
                        obj = cursor.get_item() #get the new item when for appending to newContainer a few lines further #TODO: Grammar
                        typ = type(obj)
                        closeDict = {} #reset all repeats. Jumping over repeats is done via the gotocapo switch, so this is safe and needed for alternate endings.
                        for gr, value in alternativeEndingsGroupsStatus.items(): #re-initiliaze/reset all alternate ending status
                            alternativeEndingsGroupsStatus[gr] = 0
                elif typ is GotoCoda:
                    if breakAt is "Coda":
                        #GoToCoda is not done with goToIndexAndSignature. We need to manually append jumpsAndRepeats
                        fromTick = cursor.tickindex
                        while True:
                            if cursor.right():
                                if type(cursor.get_item()) is Coda:
                                    obj = cursor.get_item() #get the new item when for appending to newContainer a few lines further
                                    jumpsAndRepeats.append((fromTick, cursor.tickindex)) #This is not done with goToIndexAndSignatureReset so we must do it manually
                                    break
                            else: #track end
                                warnings.warn("Instructed to go to Coda. But no Coda found. The coda must be right of the GotoCoda item")
                                break
                        gotocoda = False
                elif typ is Upbeat:
                    jumpsAndRepeats.append((cursor.tickindex, cursor.tickindex + obj.duration))


                ###Handling of Repeat Bars.
                if activated: #We are not in a Da Capo iteration with deactivated repeats
                    if typ is RepeatOpen:
                        openStack.append(cursor.flatCursorIndex)
                    elif typ is RepeatClose or typ is RepeatCloseOpen:
                        if cursor.flatCursorIndex in closeDict: #we were here already
                            if closeDict[cursor.flatCursorIndex]: # number of repeats > 0
                                closeDict[cursor.flatCursorIndex] -= 1
                                newContainer += goToIndexAndSignatureInsert(cursor, openStack[-1], jumpsAndRepeats)
                            else: #We were here already and there are no repeats left
                                openStack.pop()
                                del closeDict[cursor.flatCursorIndex]
                                if typ is RepeatCloseOpen: #We got behind the Repeat Close. We consider this item as new RepeatOpen and move on.
                                    openStack.append(cursor.flatCursorIndex)
                        else: #new encounter of a repeat close
                            if openStack: #we are in a valid repeat open section
                                closeDict[cursor.flatCursorIndex] = obj.repeat - 1 #add obj to the close dict as position index and number of repeats, already decreased -1 because we are here the first time right now.
                                newContainer += goToIndexAndSignatureInsert(cursor, openStack[-1], jumpsAndRepeats)
                            else:
                                warnings.warn("Repeat close without repeat open. No implicit 'Repeat Open' in Laborejo! Consider a hidden repeat open.")


                #########################################
                #####Here is the actual item insert######
                #####                              ######
                #Playback Triggers.
                obj.playbackCounter += 1
                if obj.triggerString is None or obj.playbackTrigger():
                    if not (isinstance(obj, SpecialBarline) or isinstance(obj, GotoSegno) or isinstance(obj, GotoCapo) or isinstance(obj, GotoCoda)) : #TODO: this produces clearer lists but could be a small performance issue
                        #All right. insert the normal item.
                        obj.cachedMetricalPosition = self.activeCursor.metricalPosition
                        newContainer.append(obj)

                else: #The playback trigger returned false. Insert an empty item of the same duration with manually copied data.
                    emptyItem = Rest(self.parentWorkspace, obj._base, "s") #a replacement instance
                    emptyItem._base = obj._base
                    emptyItem._tuplets = obj._tuplets
                    emptyItem._dots = obj._dots
                    emptyItem._scaleFactorNumerator = obj._scaleFactorNumerator
                    emptyItem._scaleFactorDenominator = obj._scaleFactorDenominator
                    emptyItem.flatCursorIndex = obj.flatCursorIndex

                    emptyItem.cachedMetricalPosition = self.activeCursor.metricalPosition
                    newContainer.append(emptyItem)

                lastParsedChord = obj

                #########################################
                #####    There is no Third Pass    ######
                #####                              ######
                #Now everything is in order
                #And a cursor goes straigth through
                #Make it quick.
                #cursor.go_to_head()
                #cursor.resetToDefault()
                #iterate...
                    #obj.cachedMetricalPosition = self.activeCursor.metricalPosition


            cursor.nested_list.container = newContainer #replace original, nested, data list with flat, unfolded new one.
            maxUnfoldedTrackLenghtsForRepeats[cursor.tickindex] = jumpsAndRepeats
        self.activeCursor = originalActiveCursor


        #We need to find the real playback start position since we don't know how repeats moved that item around.
        #We know on which original flat cursor it was so we search for the first item that has the matching permanent flatCursorIndex value.
        searchName, searchFlat = newScore.playbackStartPosition #temporary variable to save the cursor position when playback was started.
        for c in cursorsPlusMasterCursor:
            if c.nested_list.uniqueContainerName == searchName:
                #The track where the cursor was on playback start is still here. Look for the first instance of the original flatCursorItem and save the new tickindex.
                for x in c.iterate():
                    if not x.flatCursorIndex is None and x.flatCursorIndex >= searchFlat:
                        newScore.playbackStartPosition = c.tickindex - c.upbeatTicks #just use the same variable.
                        break
                else:
                    raise RuntimeError("The item with the flat cursor index could not be found. How is it possible that got deleted?")
                break
        else:
            #no break statement means the track where the cursor was on playback start was empty/muted/hidden and got deleted.
            #However that does NOT mean that the track was empty. If it was muted we still can get a tickindex > 0.
            newScore.playbackStartPosition = 0 #TODO:

        newScore.jumpsAndRepeats = maxUnfoldedTrackLenghtsForRepeats[max(maxUnfoldedTrackLenghtsForRepeats)]
        newScore.jumpsAndRepeats.reverse() #because we .pop() later

        #TODO: Change original active cursor in all functions to a parameter.
        #TODO: So we can work with the current cursor.
        return newScore

    def addTrack(self, name = None):
        """Add a Track as last Track"""
        tr = Track(self.parentWorkspace, name = name)
        self.append(tr) #Add the new Track to the score container. It has a unique name on init which can be replaced later by the user.
        newCur = cursor.Cursor(track = self[-1], parentScore = self) #add last track to be controlled by this cursor and give reference to self, the parentScore.
        self.cursors.append(newCur) #Create a new cursor for the new Track
        if len(self.cursors) == 1:  #for the first cursor: make it active
             self.activeCursor = self.cursors[0]
        self.lastTrack()
        self.comeLittleCursors()
        return tr

    def deleteTrack(self):
        """Delete one track. New active track is the new track on the
        same position."""
        current = self.currentTrackIndex()
        self.currentTrack.closeCalfboxMidiOut()

        if self.container[current].uniqueContainerName in self.parentWorkspace.uniqueContainerNames:
            self.parentWorkspace.uniqueContainerNames.remove(self.container[current].uniqueContainerName)

        del self.cursors[current]  #del removes by index
        del self.container[current] #remove the track by its index.

        if self.container: #Is still a Track left or was that the last one?
            try:
                self.cursors[current]  #is there still a track on the old position.
                self.activeCursor = self.cursors[current] #if yes it is fine. make the new cursor/track on this position the active one.
            except:  #if not we deleted the last track.
                self.activeCursor = self.cursors[-1]  #make the new last cursor the active one.
        else:
            self.addTrack() #Create a new empty Track instead.
            self.activeCursor = self.cursors[0] #and reset the cursor

    def swapTrackWithAbove(self):
        """Move the complete Track up which results in the upper Track moving down"""
        now = self.currentTrackIndex()
        if now == 0: #Top Track cannot move up
            return False #no swap done.
        else:
            self.container.insert(now - 1 , self.container.pop(now)) #swap the track
            self.cursors.insert(now - 1 , self.cursors.pop(now)) #swap the cursor
            self.activeCursor = self.cursors[now - 1] #new active cursor is the old one.
            return True

    def swapTrackWithBelow(self):
        """Move the complete Track down which results in the lower Track moving up"""
        now = self.currentTrackIndex()
        if now == len(self.cursors) - 1: #Bottom Track cannot move down
            return False #no swap done.
        else:
            self.container.insert(now + 1 , self.container.pop(now)) #swap the track
            self.cursors.insert(now + 1 , self.cursors.pop(now)) #swap the cursor
            self.activeCursor = self.cursors[now + 1] #new active cursor is the old one.
            return True

    def createContainerFromSelection(self):
        """Take all selected items, create a new container from them.
        Container are single-track. If this is a multi selection the
        items will be merged as single track.

        The new container will only have copies of the original items."""
        tempList = []

        start = (self.activeCursor, self.activeCursor.tickindex) #Save the cursor position for later
        for cursor in self.cursors:
            track = [i for i in cursor.iterate() if i.selected] #create a subset of the track with all selected items.
            if track: #the step above also creates empty lists for tracks with no selection.
                tempList.extend(copy.deepcopy(track)) #extend the single list with a copy of these instances

        for y in tempList:
            y.instanceCount = 0
            y.deselect(False) #set status of the item to 'not selected' but don't try to delete from the selectedItems list nor decrease selection count. The copied item is of course not in there.

        self.head() #return to the beginning, resets all cursors.
        self.activeCursor = start[0]
        self.toTickIndex(start[1])
        if tempList: #empty?
            new = Container(self.parentWorkspace)
            new.container = new.container[:1] + tempList + new.container[-1:]
            return new
        else:
            return None

    def generateSubtext(self):
        """Substitute line breaks and paragraphs."""
        escaped = self.subtext.replace("<br>", " }\n \\wordwrap-lines { ")
        escaped = escaped.split("\n\n")
        if self.subtext:
            return "\n".join(["\\wordwrap-lines { " + line + " } \\vspace #0.5" for line in escaped])
        else:
            return "\\wordwrap-lines { }"

    def generateLilypondContainer(self):
        """Generate the container definitions for Lilypond export"""
        containerDefinitionsString = "\n".join([key + " = { " + value + " } " for key, value in self.nonTrackContainer.items()])
        self.nonTrackContainer = {}
        #self.master is set during Lilypond export. It is a track.
        if self.master:
            masterVoice = self.master.generateLilypondVoice()
            masterTrack = "masterTrack = { " + self.master.exportLilypond() + " }\n"
        else:
            masterTrack = masterVoice = ""
        return masterVoice + masterTrack + containerDefinitionsString

    def generateLilypondVoice(self):
        """Lilypond Voice definitions block"""
        #Reset special Lilypond Export Data for each Score. These are not save values.
        self.parentWorkspace.lilypondExportPrevailingTimeSignature = self.parentWorkspace.score.activeCursor.prevailingTimeSignature[0] #Holds one TimeSignature at a time, defaults to Lilyponds 4/4. Is overwritten and used during Lilypond export and gets accessed and changed by Multi Measure Rests and other methods and functions. Similar to the cursor.prevailingTimeSignature but does not go left.
        #self.parentWorkspace.lilypondExportActiveCursor = self.parentWorkspace.score.activeCursor
        self.parentWorkspace.waitForNextChordPst = [] # A queue which will be attached as postfix directives to the next chord in line. Enables dynamic changes to be standalone items but still appear as attached lilypond.
        self.parentWorkspace.waitForNextChordPre = [] # A queue which will be attached as postfix directives to the next chord in line. Enables dynamic changes to be standalone items but still appear as attached lilypond.
        return "".join([track.generateLilypondVoice() for track in self.container]) + "\n"

    def generateLilypond(self):
        """Structure: Staff Groups (through directives), staffs, voices.
        Lilypond has only one voice level so we don't need nested voices
        level checking like tuplets (except in-staff micro voices which
        have another syntax here and in Lilypond)
        The internal "Tracks" are arranged as Lilypond voices and staffs

        It does not matter if the current Track is mergable or not,
        only look-ahead matters since we only deal with the ending here.
        For the starting << it would matter if the current is merge or
        not, but these are done in the tracks exportLilypond itself.

        As many methods this should be done only on the copy of the
        score and not on the real score."""

        def structGen(track, maxIndex, master):
            """Build a structure line. Decide if opening or closing <<>>
            are needed to close a staff which may have multiple voices"""

            currentStaffIndex = self.container.index(track)
            #The prefix << is in staff itself so a staffgroup can be added here.
            if (not currentStaffIndex + 1 >= maxIndex) and self[currentStaffIndex + 1].mergeWithUpper:
                post = " " #next is still a visible voice. Do not close.
            else:
                voiceMidAndMaster = " ".join(track.directiveMid.values()) + master
                post = voiceMidAndMaster + " ".join(track.directivePst.values()) + " >> %EndTrack\n" #next is present but not a merged voice. Close current staff.

            return track.exportLilypond() + post

        def lyrics(track):
            """x = track
            Build a lyrics line.
            Wants a track as parameter"""
            if track.lyrics:
                return "".join(["\\context Lyrics = \"" + track.uniqueContainerName  + "Lyrics" + helper.num2word(verseCounter) + "\" \\lyricsto \"" + track.lilypondContainerName + "\" \\" + track.lilypondContainerName + "Lyrics" + helper.num2word(verseCounter) + "\n" for verseCounter in range(track.verseCountCache)])
            else:
                return ""


        def mergeTrackDirectivesForMerged():
            """Merge all directives to the top track of each
            mergeWithAbove group.
            """
            try: #TODO: this is for Python 3.3 only.
                for trackGroup in self.getTracksAsMergedGroups():
                    if len(trackGroup) > 1:
                        trackGroup[0].directivePre = ChainMap(*[t.directivePre for t in trackGroup] +[trackGroup[0].directivePre])
                        trackGroup[0].directiveMid = ChainMap(*[t.directiveMid for t in trackGroup] +[trackGroup[0].directiveMid])
                        trackGroup[-1].directivePst = ChainMap(*[t.directivePst for t in trackGroup] +[trackGroup[0].directivePst])
                        for tr in trackGroup[1:]:
                            #And clean the other directives
                            tr.directivePre = {}
                            tr.directiveMid = {}
                        for tr in trackGroup[:-1]:
                            tr.directivePst = {}
            except:
                pass


        #First check if we have a proper first track. There can be as many mergeWithUpper as the user wants, except the first track must not be one. If that is the case we set mergeWithUpper to off.  Since Lilypond export works on a copy anyway there is no harm in doing that.
        if self.container[0].mergeWithUpper:
            self.container[0].mergeWithUpper = False
            warnings.warn("The first track is not allowed by Lilypond to be merged with the upper track (because there is no upper). Temporarily disabling the parameter for the export. Consider changing your 'mergeWithUpper' track parameter permanently")

        mergeTrackDirectivesForMerged()

        #Now build the actual string and return it. Hidden Lilypond Tracks are already deleted by self.convertToExportScore() which is the basis for Ly-Export
        maxIndex = len(self.container)
        master = self.master.exportLilypond() if self.parentWorkspace.score.master else ""
        return  "".join([structGen(track, maxIndex, master) for track in self]) + "\n" + "".join([lyrics(track) for track in self])

    def getTracksAsMergedGroups(self):
        """Return a list of lists with tracks grouped in their order and
        by mergeWithAbove status """
        l = []
        if self.container[0].mergeWithUpper:
            raise BaseException("The first track is not allowed to be merged with the upper. There is no upper. No supper for you!")
        for track in self:
            if track.mergeWithUpper:
                l[-1].append(track)
            else:
                l.append([track])
        return l

    def exportPlayback(self, unfoldedScore = None, jackMode = False,  parts = None, returnObject = None):
        """The original cursor positions are backuped and restored
        returnObject is a special return object maneuver return data
        around the return statement. It is only used in rare cases
        so that it would not be justified to return a couple or multiple
        values.

        Export Playback is both for midi file export and realtime play.
        midi export is in the module 'smf' which is loaded systemwide.
        smf is set to this but can be overriden with the drop-in
        replacement and method/class compatible calfboxwrap class."""
        if not unfoldedScore:
            unfoldedScore = self.unfoldScore(parts = parts)
            if not unfoldedScore: #only empty tracks, excluding master?
                warnings.warn("Tried to export empty score:" + str(unfoldedScore))
                return False


        originalActiveCursor = self.activeCursor #Save the cursor position
        smfStructure = smf.SMF(ppqn=384)

        trackNum = 0
        #Work on the unfolded copy, not on the original cursors
        #TODO: We still use the real active cursor. Unless all functions don't rely on this state anymore we can't change it.
        self.activeCursor =  unfoldedScore.activeCursor

        if TempoSignature in helper.items2types(helper.flatten(unfoldedScore.beginning())):
            hasOwnTempoSigs = True
        else:
            hasOwnTempoSigs = False


        for track in unfoldedScore: #this does not go through the tracks. The tracks just provide a number. The cursors point to the real data tracks.
            smfStructure.add_track()
            self.activeCursor = unfoldedScore.cursors[trackNum]
            self.activeCursor.resetToDefault()

            if trackNum == 0: #only for the first track.
                if "copyright" in self.header.data and self.header.data["copyright"]:
                    smfStructure.tracks[0].add_event(smf.Event([0xFF, 0x02, len(self.header.data["copyright"])] + [ord(c) for c in self.header.data["copyright"]]), pulses = 0)  #Copyright.

            #Decide if standard SMF or the Jack Set of parameters
            channel = track.jackChannel if jackMode else track.smfChannel
            volume = track.jackVolume if jackMode else track.smfVolume
            pan = track.jackPan if jackMode else track.smfPan
            bank = track.jackBank if jackMode else track.smfBank
            patch = track.jackPatch if jackMode else track.smfPatch
            transpose = track.jackTransposition + self.jackTransposition if jackMode else track.smfTransposition + self.smfTransposition

            smfStructure.tracks[trackNum].add_event(smf.Event([0xFF, 0x03, len(track.uniqueContainerName)] + [ord(c) for c in track.uniqueContainerName]), pulses = 0)  #Track Name
            smfStructure.tracks[trackNum].add_event(smf.Event([0xFF, 0x04, len(track.longInstrumentName)] + [ord(c) for c in track.longInstrumentName]), pulses = 0)  #Initial Instrument Name. Can be changed by the object event.

            #Is there a tempo signature anywhere in the beginning? Use that. Else fall back to 120bpm.
            if not hasOwnTempoSigs:
                initTempoSig = self.activeCursor.prevailingTempoSignature[0].generatePlayback(0,0,0, jackMode=jackMode, smf=smf)[0][0]
                smfStructure.tracks[trackNum].add_event(smf.Event(initTempoSig), pulses = 0) #Initial Tempo. Global event, not for any channel. Fake 0,0,0 parameters

            #Lyrics, 0x04, need an de-lilypondized version. Frescobaldi can do that.
            #smfStructure.tracks[trackNum].add_event(smf.Event([0xFF, 0x05, len(track.longInstrumentName)] + [ord(c) for c in track.longInstrumentName]), pulses = 0)  #Initial Instrument Name. Can be changed by the object event.

            if jackMode and channel[1] > channel[0]: #a real range
                for i in range(channel[0], channel[1]+1):
                    #bank, patch etc. can be -1. This means the user does not want to set initial values.
                    if bank >= 0:
                        smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + i, 0x00, bank]), pulses = 0,) #Initial Bank for this channel. CC0
                    if patch >= 0:
                        smfStructure.tracks[trackNum].add_event(smf.Event([0xC0 + i, patch]), pulses = 0,) #Initial Patch/Instrument
                    if volume >= 0:
                        smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + i, 0x07, volume]), pulses = 0,) #Initial Volume for this channel. CC 7
                    if pan >= 0:
                        smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + i, 0x0A, pan]), pulses = 0,) #Initial Pan (Left/Right). for this channel. CC 10
                    if bank >= 0 or patch >= 0 or volume >= 0 or pan >= 0:
                        smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + i, 0x01, 0]), pulses = 0,) #Reset modhweel to 0. CC1
            else:
                #bank, patch etc. can be -1. This means the user does not want to set initial values.
                #Internal midi cannot go below 0 (at least not if the UI did its job). So it is safe to set this for both.
                if bank >= 0 or patch >= 0 or volume >= 0 or pan >= 0:
                    smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + channel[0], 0x01, 0]), pulses = 0,) #Reset modhweel to 0. CC1
                if patch >= 0:
                    smfStructure.tracks[trackNum].add_event(smf.Event([0xC0 + channel[0], patch]), pulses = 0,) #Initial Patch/Instrument
                if volume >= 0:
                    smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + channel[0], 0x07, volume]), pulses = 0,) #Initial Volume for this channel. CC 7
                if pan >= 0:
                    smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + channel[0], 0x0A, pan]), pulses = 0,) #Initial Pan (Left/Right). for this channel. CC 10
                if bank >= 0:
                    smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + channel[0], 0x00, bank]), pulses = 0,) #Initial Bank for this channel. CC0

            #########################################
            #The music is converted and appended here
            #########################################

            #track.exportPlayback(smfStructure.tracks[trackNum], ticks = 0, channel = channel, transpose = track.smfTransposition)  #each track starts at 0 smf-ppqn and has a basechannel
            workingChannel = [channel[0], channel[1], channel[0]] #during object export we keep the original channel and also a working channel for note ond and off permanent/standalone ChannelChange items
            for obj in self.activeCursor.iterate():
                smfTrack, workingChannel, transpose, jackMode = obj.exportPlayback(smfStructure.tracks[trackNum], channel = workingChannel, transpose = transpose, jackMode = jackMode, smf=smf)

            #The cursor now is on the beginning of the last tick, which is appending. So all sounding events are over.
            #Some midi players shut off playback on the last note off or at the EndOfTrack object and don't wait for any reverbs or sounding notes.
            #We add a Sustain Pedal Off after the last event so we get a small buffer time. This also covers forgotton sustain pedals.
            if jackMode and channel[1] > channel[0]: #a real range
                for i in range(channel[0], channel[1]+1):
                    smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + i, 0x40, 0]), pulses=self.activeCursor.tickindex + 1536) #CC 64, sustain pedal off.
            else:
                smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + channel[0], 0x40, 0]), pulses=self.activeCursor.tickindex + 1536) #CC 64, sustain pedal off.

            if returnObject:
                returnObject.cachedMaxticks = int(self.activeCursor.tickindex) #this is used by some export formats, like ardour. It is ONLY useful when used with per-track export of midis. Else you always get the last track.

            trackNum += 1

        self.activeCursor = originalActiveCursor #Restore original active cursor including its position.
        return smfStructure

    def exportCalfbox(self, smf, unfoldedScore = None, jackMode = False, parts = None):
        """Live midi playback
        A copy of expportPlayback"""
        if not unfoldedScore:
            unfoldedScore = self.unfoldScore(parts = parts)
            if not unfoldedScore: #only empty tracks, excluding master?
                warnings.warn("Tried to export empty score:" + str(unfoldedScore))
                return False

        if not unfoldedScore.container: #Empty Score
            return False

        originalActiveCursor = self.activeCursor #Save the cursor position
        smfStructure = smf.SMF(ppqn=384)
        smfStructure.tracks = []

        trackNum = 0
        #Work on the unfolded copy, not on the original cursors
        #TODO: We still use the real active cursor. Unless all functions don't rely on this state anymore we can't change it.
        self.activeCursor =  unfoldedScore.activeCursor

        #Is there a tempo signature anywhere in the beginning? Use that. Else fall back to 120bpm.
        if TempoSignature in helper.items2types(helper.flatten(unfoldedScore.beginning())):
            hasOwnTempoSigs = True
        else:
            hasOwnTempoSigs = False

        for track in unfoldedScore: #this does not go through the tracks. The tracks just provide a number. The cursors point to the real data tracks.
            smfStructure.add_track(track)
            self.activeCursor = unfoldedScore.cursors[trackNum]
            self.activeCursor.resetToDefault()

            #Is there a tempo signature anywhere in the beginning? Use that. Else fall back to 120bpm.
            if not hasOwnTempoSigs:
                initTempoSig = self.activeCursor.prevailingTempoSignature[0].generatePlayback(0,0,0, jackMode=jackMode, smf=smf)[0][0]
                smfStructure.tracks[trackNum].add_event(smf.Event(initTempoSig), pulses = 0) #Initial Tempo. Global event, not for any channel. Fake 0,0,0 parameters


            #Decide if standard SMF or the Jack Set of parameters
            channel = track.jackChannel if jackMode else track.smfChannel
            volume = track.jackVolume if jackMode else track.smfVolume
            pan = track.jackPan if jackMode else track.smfPan
            bank = track.jackBank if jackMode else track.smfBank
            patch = track.jackPatch if jackMode else track.smfPatch
            transpose = track.jackTransposition + self.jackTransposition if jackMode else track.smfTransposition + self.smfTransposition

            if jackMode and channel[1] > channel[0]: #a real range
                for i in range(channel[0], channel[1]+1):
                    #bank, patch etc. can be -1. This means the user does not want to set initial values.
                    if bank >= 0:
                        smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + i, 0x00, bank]), pulses = 0,) #Initial Bank for this channel. CC0
                    if patch >= 0:
                        smfStructure.tracks[trackNum].add_event(smf.Event([0xC0 + i, patch]), pulses = 0,) #Initial Patch/Instrument
                    if volume >= 0:
                        smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + i, 0x07, volume]), pulses = 0,) #Initial Volume for this channel. CC 7
                    if pan >= 0:
                        smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + i, 0x0A, pan]), pulses = 0,) #Initial Pan (Left/Right). for this channel. CC 10
                    if bank >= 0 or patch >= 0 or volume >= 0 or pan >= 0:
                        smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + i, 0x01, 0]), pulses = 0,) #Reset modhweel to 0. CC1
            else:
                #bank, patch etc. can be -1. This means the user does not want to set initial values.
                #Internal midi cannot go below 0 (at least not if the UI did its job). So it is safe to set this for both.
                if bank >= 0 or patch >= 0 or volume >= 0 or pan >= 0:
                    smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + channel[0], 0x01, 0]), pulses = 0,) #Reset modhweel to 0. CC1
                if patch >= 0:
                    smfStructure.tracks[trackNum].add_event(smf.Event([0xC0 + channel[0], patch]), pulses = 0,) #Initial Patch/Instrument
                if volume >= 0:
                    smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + channel[0], 0x07, volume]), pulses = 0,) #Initial Volume for this channel. CC 7
                if pan >= 0:
                    smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + channel[0], 0x0A, pan]), pulses = 0,) #Initial Pan (Left/Right). for this channel. CC 10
                if bank >= 0:
                    smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + channel[0], 0x00, bank]), pulses = 0,) #Initial Bank for this channel. CC0


            #########################################
            #The music is converted and appended here
            #########################################

            #track.exportPlayback(smfStructure.tracks[trackNum], ticks = 0, channel = channel, transpose = track.smfTransposition)  #each track starts at 0 smf-ppqn and has a basechannel
            workingChannel = [channel[0], channel[1], channel[0]] #during object export we keep the original channel and also a working channel for note ond and off permanent/standalone ChannelChange items
            for obj in self.activeCursor.iterate():
                smfTrack, workingChannel, transpose, jackMode = obj.exportPlayback(smfStructure.tracks[trackNum], channel = workingChannel, transpose = transpose, jackMode = jackMode, smf = smf)

            #The cursor now is on the beginning of the last tick, which is appending. So all sounding events are over.
            #Some midi players shut off playback on the last note off or at the EndOfTrack object and don't wait for any reverbs or sounding notes.
            #We add a Sustain Pedal Off after the last event so we get a small buffer time. This also covers forgotton sustain pedals.
            if jackMode and channel[1] > channel[0]: #a real range
                for i in range(channel[0], channel[1]+1):
                    smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + i, 0x40, 0]), pulses=self.activeCursor.tickindex + 1536) #CC 64, sustain pedal off.
            else:
                smfStructure.tracks[trackNum].add_event(smf.Event([0xB0 + channel[0], 0x40, 0]), pulses=self.activeCursor.tickindex + 1536) #CC 64, sustain pedal off.

            smfStructure.tracks[trackNum].length = self.activeCursor.tickindex  #A calfboxwrap.py track

            trackNum += 1

        smfStructure.playbackStartPosition = unfoldedScore.playbackStartPosition
        smfStructure.cachedRepeatsOfLongestTrack = unfoldedScore.jumpsAndRepeats
        self.activeCursor = originalActiveCursor #Restore original active cursor including its position.
        return smfStructure

    def generateSave(self):
        childDict = {}
        childDict["subtext"] = self.subtext
        childDict["splitByToHeader"] = self.splitByToHeader
        childDict["header"] = self.header.exportSave()
        childDict["defaultPerformanceSignature"] = self.defaultPerformanceSignature.exportSave()
        subsaveDict = {}
        for key, value in self.substitutions.items():
            subsaveDict[key] = value.exportSave()
        childDict["substitutions"]  = subsaveDict
        #childDict["master"] = self.master.exportSave()
        return childDict

class Workspace(object):
    """This is one file. Open, New all results in one Workspace.
    Data exports as a %$$PLACEHOLDER$$ in Lilypond export."""
    #TODO: merge into score?
    def __init__(self):
        self.parentSession = session #once loaded or implemented via session.new this becomes the current session. not saved.
        #export data
        self.lilypondDefinitions = LilypondDefinitions()
        self.uniqueContainerNames = [] #all unique names for containers, tracks and score. they must be unique for save and load as well as exporting to Lilypond.
        #various data for lilypond export
        self.waitForNextChordPst = [] #a temp value which gets filled during Ly Export. Needed because some Laborejo standalone items are postfix strings in Lilypond
        self.waitForNextChordPre = [] #a temp value which gets filled during Ly Export. Needed because some Laborejo standalone items are prefix strings in Lilypond
        #self.lastDurationLyString = None #For cleaner Lilypond export. The duration is only needed when it changes, not all the time. class Chord uses this during Ly Export to remember the last duration.
        self.lytemplate = "basic.ly" #For the whole file. Can be changed by the user.
        self.ardourtemplate = "empty.ardour"
        self.lastKnownFilename = "" # Temporary path+filename storage for the whole File. Is not saved or used for anything critical. Just in lilypond export to write the filename in the ly file, to a comment line. Exception: SaveAll uses this to check if a file is worth saving. saveAll is a mean function anyway...
        self.lyFontSize = 20 #Can be used in a Lilypond export template.
        self.substitutiondatabase = None # filled in by session.new()
        self.parentCollection = False #during a collection file load/export this becomes True. not saved.

        #Finally, with all other values existent, create the musical structure.
        self.score = Score(parentWorkspace = self) #The current score.
        #self.lilypondExportPrevailingTimeSignature = self.score.activeCursor.prevailingTimeSignature[0]
        self.lilypondExportPrevailingTimeSignature = None #filled in during Ly export
        self.lilypondExportTrackName = None #temp during Ly export. it is sometimes important to know, during ly export, what the cusor is to derive which track currently gets exported and so on.

    def __deepcopy__(self, memo):
        """Item undo tries to save the parentWorkspace with a deepcopy.
        Prevent that from happening."""
        return self

    def exportSave(self):
        dictionary = {}
        dictionary["file"] = "laborejo" #This makes .lbjs a Laborejo file.
        dictionary["lilypondDefinitions"] = self.lilypondDefinitions.exportSave()
        dictionary["score"] = self.score.exportSave()
        dictionary["lytemplate"] = self.lytemplate
        dictionary["ardourtemplate"] = self.ardourtemplate
        dictionary["lyFontSize"] = self.lyFontSize
        dictionary["uniqueContainerNames"] = self.uniqueContainerNames
        return dictionary

class Collection(object):
    """A loaded .lbj file. This holds references to to several lbjs
    files, metadata and other information for a multi-document
    export process e.g. multi-movement Lilypond PDF or batch converted
    midi project.
    There is no editable music data in here"""
    def __init__(self):
        self.lytemplate = "basic.ly"
        self.ardourtemplate = "empty.ardour"

        self.parentSession = None #once loaded or implemented via session.new this becomes the current session. not saved.

        self.lilypondDefinitions = LilypondDefinitions()
        self.header = Header(self) #We want a complete header set, empty. Everything else is entered during api.load.

        #The following workspaceFileStrings has a very specific format:
        #Each score marker adds a new tuple (score marker name, [nested list of file names])
        #The nested list has 2 levels:
        #top level is one movement/score. Each list is one "line" in the source files There are only lists on this level.
        #lower level is the actual content. parts of this one lilypond score. In the source file it is represented by all files beeing in one line, seperated through the pipe |.
        self.workspaceFileStrings = [] #start with one empty list. Format: [["scoreNameString", ["list.lbjs", "of.lbjs", "parts.lbjs"]], ["second movement", ["singleFile.lbjs"]], ]

        self.lastKnownFilename = "" # Temporary path+filename storage for the whole File. Is not saved or used for anything critical. Just in lilypond export to write the filename in the ly file, to a comment line. Exception: SaveAll uses this to check if a file is worth saving. saveAll is a mean function anyway...
        self.lyFontSize = 18 #Can be used in a Lilypond export template.
        self.originalText = "" #TODO: for debug and testing only
        self.numbering = "" #can be any of the metadata fields or an empty string (default)
        self.startnumber = 1 #self.numbering starts here.
        self.transposition = (1420, 1420)  #[0]=what is meant, [1]=what will be written in the pdf . Usually [0] stays 1420 and [1] changes. Exports as lilypond transpose for pdf generation. Sounds the same, probably looks different. initially no transposition. c' is c'.  In human words: "I notated in C and it sounds correct but for the pdf I want it to be notated one whole tone lower for a clarinet"
        self.smfTransposition = 0 #Half Tones. negative or positive. This is just for midi export. If you export your Contrabass with -12 here it will sound correct but it will be still considered as written in Laborejo in the analysis. Simple rule: Type in what it is. This is mainly for wrong sampled instruments which transpose on their own
        self.jackTransposition = 0 #Half Tones. negative or positive. This is just for midi export. If you export your Contrabass with -12 here it will sound correct but it will be still considered as written in Laborejo in the analysis. Simple rule: Type in what it is. This is mainly for wrong sampled instruments which transpose on their own

        self._printallheader = 0 # 0-Template default 1-Yes 2-No. 0 does nothing, 1 or 2 forces lilypond print all headers on or off
        self.splitByToHeader = True #If True this score, when exported to lilypond with part-splitting enabled, will put its split-by value (like "Strings" or "Oboe" etc.) in the header to the instruments field

    def __deepcopy__(self, memo):
        """Just a precaution. Don't copy anything. Just return itself."""
        #TODO: maybe an error instead? This is not allowed as a copy. Don't copy that floppy.
        return self

    @property
    def workspaceList(self):
        """This function cannot be used to load lbjs files into the main
        session and subsequently into a GUI because it merges
        the lbjs parts._printallheader
        Also this function add things like dynamic numbering
        and modifies the metadata (e.g. piece) of loaded scores
        which is a no-go for real data."""
        from laborejocore import saveload
        #From the filenames create full path and load them as real workspaces
        #TODO: Since the files can be edited in parallel either this must be done each time an export is requested (@decorator) or the workspaces must be indeed the ones which are loaded already. The second method should be much more performant
        #TODO: importing save not good
        retlist = []

        for number, (piecetitle, partlist) in enumerate(self.workspaceFileStrings): #enumerate is from 0
            dirname =  os.path.dirname(self.lastKnownFilename)
            filenameList = [os.path.join(dirname, onlyFileName) for onlyFileName in partlist]
            if len(filenameList) > 1:
                worksp = saveload.loadMergeParts(filenameList)
            else:
                worksp = saveload.load(filenameList[0])
            worksp.substitutiondatabase = self.parentSession.build_database(worksp)
            worksp.parentCollection = self
            #If there is a name override for the piece, use this

            #We don't save anything during collection editing. So we can edit at any time.
            if piecetitle: #not ""
                worksp.score.header.data["piece"] = piecetitle
            if self.numbering:
                worksp.score.header.data[self.numbering] = str(number + self.startnumber) + " " + worksp.score.header.data[self.numbering]
            worksp.score.splitByToHeader = self.splitByToHeader

            #Override the score transpositions, no matter what they were originally. Global transpositions are always a temporary measure, even when working on score level.
            worksp.score.transposition = self.transposition
            worksp.score.smfTransposition = self.smfTransposition
            worksp.score.jackTransposition = self.jackTransposition
            retlist.append(worksp)

        return retlist

    @property
    def score(self):
        """A convenience function that returns the first score.
        Used for compatibility in the api like part extraction.
        Will prevent typechcking in the api and too many ifs"""
        return self.workspaceList[0].score

    @property
    def printallheader(self):
        return self._printallheader

    @printallheader.setter
    def printallheader(self, value):
        """0-Template default 1-Yes 2-No."""
        self._printallheader = value
        if value == 1:
            self.lilypondDefinitions.container["tempPrintAllHeaderOverride"] = "\paper { print-all-headers = ##t }"
        elif value == 2:
            self.lilypondDefinitions.container["tempPrintAllHeaderOverride"] = "\paper { print-all-headers = ##f }"
        else: #0
            if "tempPrintAllHeaderOverride" in self.lilypondDefinitions.container:
                del self.lilypondDefinitions.container["tempPrintAllHeaderOverride"]
        return self.printallheader

    def generateScoreMarkers(self):
        """Example:
        =Name of the Movement
        complete.lbjs

        =Second Move
        part one.lbjs
        part two.lbjs
        """

        lines = []
        for name, listofParts in self.workspaceFileStrings:
            lines.append("=" + name)
            for part in listofParts:
                lines.append(part)
            lines.append("\n")
        return "\n".join(lines)

    def deserialise(self, bigString):
        """Convert the content of a file into real data
        The line import works by switching modes
        Certain markers trigger a new section, mode, variable etc.
        So the order of a .lbj is important.

        Only a leading # is a comment.

        Order:
        header data
        =First score marker, overrides the "piece" lilypond header variable #TODO: do
        lbjs files which will be combined to one compound score
        another score marker etc."""

        mode = "header"
        lastScoreSectionName = ""
        for line in self.originalText.split("\n"):
            if line.startswith("#") or line == "\n" or line == "": #Ignore empty lines or full line comments
                continue

            line = line.strip()
            line = " ".join(line.split())

            if line.startswith("="):
                mode = "lbjs"
                lastScoreSectionName = line[1:] #everything except the first "="
                self.workspaceFileStrings.append([lastScoreSectionName, []])
                continue

            elif line.startswith("fontsize"):
                self.lyFontSize = int(line.split("=")[1])
                continue

            elif line.startswith("template"):
                self.lytemplate = line.split("=", maxsplit = 1)[1]
                self.lytemplate = self.lytemplate.strip()
                continue

            elif line.startswith("ardourtemplate"):
                self.ardourtemplate = line.split("=", maxsplit = 1)[1]
                self.ardourtemplate = self.ardourtemplate.strip()
                continue

            elif line.startswith("numbering"):
                self.numbering = line.split("=", maxsplit = 1)[1]
                self.numbering = self.numbering.strip()
                continue

            elif line.startswith("startnumber"):
                self.startnumber = int(line.split("=")[1])
                continue

            elif line.startswith("printallheader"):
                self.printallheader = int(line.split("=")[1])
                continue

            elif line.startswith("splitByToHeader"):
                if line.split("=")[1] == "0" or not eval(line.split("=")[1]):
                    self.splitByToHeader = False
                else:
                    self.splitByToHeader = True #how can this happen? Doesn'T matter. Keep it in if load changes in the future.
                continue

            elif line.startswith("smfTransposition"):
                self.smfTransposition = int(line.split("=")[1])
                continue

            elif line.startswith("jackTransposition"):
                self.jackTransposition = int(line.split("=")[1])
                continue

            elif line.startswith("transposition"): #lilypond transposition
                self.transposition = eval(line.split("=")[1])  # is a string two-number tuplet  "(1420, 1430)"
                continue

            if mode == "header":
                lst = line.split("=", maxsplit = 1)  #for example title = hello world.   does work with multiple = as well:   title = hello == world still results in the var "hello == world"
                self.header.data[lst[0].strip()] = lst[1].strip()
            elif mode == "lbjs":
                self.workspaceFileStrings[-1][1].append(line)  #to the last list in the list add the current filename

    def serialise(self):
        """Convert the python instance to the simple .lbj file syntax"""
        lines = []
        if not self.lytemplate == "basic.ly":
            lines.append("template = " + self.lytemplate)
        if not self.ardourtemplate == "empty.ardour":
            lines.append("ardourtemplate = " + self.ardourtemplate)
        lines.append("fontsize = " + str(self.lyFontSize))
        if self.numbering:
            lines.append("numbering = " + self.numbering)
        if self.startnumber != 1:
            lines.append("startnumber = " + str(self.startnumber))
        if self.printallheader:
            lines.append("#printallheader: 0-Template default 1-Yes 2-No. 0 does nothing, 1 or 2 forces lilypond print all headers on or off\n" + "printallheader = " + str(self.printallheader))
        if not self.splitByToHeader:
            lines.append("splitByToHeader = " + str(self.splitByToHeader)) #boolean
        if not self.transposition == (1420, 1420):
            lines.append("transposition = " + str(self.transposition))
        if self.smfTransposition:
            lines.append("smfTransposition = " + str(self.smfTransposition))
        if self.jackTransposition:
            lines.append("jackTransposition = " + str(self.jackTransposition))

        lines.append(self.header.generateLilypond(ignoreEmpty = True, ignoreKeys = ["piece"]).replace('"', ''))
        lines.append(self.generateScoreMarkers())
        return "\n".join(lines)

class SessionClipboard(object):
    """This is outside of any music structure so that different files
    and scores during the same session can share data.

    This is not saved in any file. If you close the program it is gone"""
    def __init__(self):
        self.lastScore = None #on copy save the last score. paste in api will check if it is pasting in the same score. If not it will remove links.
        self.clipboard = {"session":[],} #value is a list. temp storage if a user script wants to do copy and paste action without touching the actual clipoard
        self.linkboard = {"session":[],} #value is a list. temp storage if a user script wants to do copy and paste action without touching the actual clipoard

    def __deepcopy__(self, memo):
        """Just a precaution. Don't copy anything. Just return itself."""
        return self

    def copySelectedItemsToInternalClipboard(self, cliplink): #cliplink is a hash for the two dicts
        """Take all selected items, create a new list from them.
        No positional information is saved except in how many staffs was
        selected"""
        self.lastScore = session.workspace.score
        tmpclipboard = []
        tmplinkboard = []

        start = (self.lastScore.activeCursor, self.lastScore.activeCursor.flatCursorIndex) #Save the active cursor and its position for later
        for cursor in self.lastScore.cursors:
            track = [i for i in cursor.iterate() if i.selected] #create a subset of the track with all selected items.
            if track: #the step above also creates empty lists for tracks with no selection.
                tmpclipboard.append(copy.deepcopy(track)) #store a copy of these instances each for paste(normal). We don't know what is going to happen right now so we need both.
                tmplinkboard.append(track) #store the exact instances for paste as link. We don't know what is going to happen right now so we need both.

        for x in tmpclipboard: #each track in the clipboard
            for y in x: #each item in a track
                y.instanceCount = 0
                #y.newName() #is done during api.paste because we can paste an item more than once. Just generating a new name on copy would result in pasting the same thing over and over again.
                y.deselect(False) #set status of the clipboard-item to 'not selected' but don't try to delete from the selectedItems list nor decrease selection count. The copied item is of course not in there.

        #most of the time this is ["session"]
        self.clipboard[cliplink] = tmpclipboard
        self.linkboard[cliplink] = tmplinkboard


        self.lastScore.head() #return to the beginning, resets all cursors.
        self.lastScore.activeCursor = start[0]
        self.lastScore.toIndex(start[1])

    def generatorClipboard(self, cliplink):
        """Return a generator clipboard. Ignores all track breaks"""
        return (obj
                for track  in self.clipboard[cliplink]
                for obj in track)

    def generatorLinkboard(self, cliplink):
        """Return a generator clipboard. Ignores all track breaks"""
        return (obj
                for track  in self.linkboard[cliplink]
                for obj in track)

    def generatorReverseClipboard(self, cliplink):
        """Return a generator clipboard. Ignores all track breaks"""
        return (obj
                for track in reversed(self.clipboard[cliplink])
                for obj in reversed(track))

    def generatorReverseLinkboard(self, cliplink):
        """Return a generator clipboard. Ignores all track breaks"""
        return (obj
                for track  in reversed(self.linkboard[cliplink])
                for obj in reversed(track))


    def filterClipboard(self, cliplink, test):
        """Filter a clip+link-board  so only those items remain which
        succeed in the test.
        Test is a function that must accept one parameter, the item"""
        self.clipboard[cliplink] = [[ obj
                                    for track in self.clipboard[cliplink]
                                    for obj in track if test(obj)]]

        self.linkboard[cliplink] = [[ obj
                                    for track in self.linkboard[cliplink]
                                    for obj in track if test(obj)]]

class Session(object):
    """A container to store and manage all open workspaces.
    This level of organisation is not saved or stored anywhere.
    It just exists as long the same program session is open.
    """
    def __init__(self):
        self.workspace = None  #the current file.
        self.workspaces = [] #all lbjs files, Laborejo Scores
        self.configdir = os.path.expanduser("~") + "/.laborejo/" #the homedir on windows and linux for custom lilypond templates. This is the default but it can be changed after program start since all functions like lilypond export re-ask for this value every time.
        #self.substitutiondatabase = None #Circular dependencies demand that this is filled in after everything else was loaded. Each item can access this through the parentX chain: Item().parentWorkspace.parentSession.substitutiondatabase
        self.build_database = None #a function, filled in by __init__
        self.collections = [] #all lbj files, represented as core.Collection. There is no active cursor in a collection so there is no active collection or active anything. They are just a list.
        self.calfbox = None #this is a calfboxwrap.py Calfbox() instance if jack midi and calfbox are installed.

    def __deepcopy__(self, memo):
        """Just a precaution. Don't copy anything. Just return itself."""
        return self

    def new(self):
        """new workspace/file. Makes the new one active."""
        new = Workspace() #A Workspace has the session as temporary parent. Also creates the first score with the first track with its appending position items.
        self.workspaces.append(new)
        self.workspace = new
        self.workspace.parentSession = self
        self.workspace.substitutiondatabase = self.build_database(self.workspace) #Circular dependencies demand that this is filled in after everything else was loaded. Each item can access this through the parentX chain: Item().parentWorkspace.substitutiondatabase
        return new

    def load(self, loadedWorkspace):
        """create a new workspace, but load existing data from another
        workspace to it. It does not matter how the workspace came
        to existence. Most likely through the api and saveload module.
        This means load does not close the current active workspace.
        The old one stays open, but not active.
        Achtung. It is technically possible to open from the same
        filepath twice. Since we operate completely in RAM these two
        instances are not connected except on save and load through
        a filename.
        Lets keep it in and see where this leads to in the real world.
        The problems that arise from duplicate open files are GUI ones.
        A script might find a nice way to handle the same file twice"""
        self.workspaces.append(loadedWorkspace)
        self.workspace = loadedWorkspace #Active wsp.
        self.workspace.parentSession = self #an Workspace has the session as temporary parent. Currently only used to access the current homedir.
        self.workspace.substitutiondatabase = self.build_database(self.workspace) #Circular dependencies demand that this is filled in after everything else was loaded. Each item can access this through the parentX chain: Item().parentWorkspace.substitutiondatabase
        return loadedWorkspace

    def close(self):
        """Delete the current active workspace.
        Make the last added workspace active.
        If there is none do nothing."""
        if self.workspace:
            for track in self.workspace.score.container:
                track.closeCalfboxMidiOut()
            self.workspaces.remove(self.workspace)
            if self.workspaces:
                self.workspace = self.workspaces[-1]
            else:
                self.workspace = None
            return self.workspace
        else:
            return False

    def newCollection(self):
        col = Collection()
        self.collections.append(col)
        col.parentSession = self
        return col

    def loadCollection(self, loadedCollection):
        self.collections.append(loadedCollection)
        loadedCollection.parentSession = self
        return loadedCollection

    def closeCollection(self, collection):
        """Delete one collection, nothing more.
        Contrary to the scores there is nothing that needs state
        switching or so."""
        if collection in self.collections:
            self.collections.remove(collection)
            return True
        else:
            return False


