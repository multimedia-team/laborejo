# -*- coding: utf-8 -*-
import os
import sys, warnings

global ardourExportIDCounter
ardourExportIDCounter = 100 #100 unique ids are reseverd for default empty.ardour items

def getnum(r=True):
    global ardourExportIDCounter
    if r:
        ardourExportIDCounter += 1
    return str(ardourExportIDCounter)

def getIncludes(TEMPLATE_ROOT, USER_TEMPLATE_ROOT, COLLECTION_TEMPLATE_ROOT):
    templates = set() #all .ardour files

    #repeat the parsing three times with different paths, save in lilypondincludes{}. Each time a key/filename appears a second time it will overwrite the old one. Thats how we get the priorities mentioned in the docstring.
    def parseIncludes(PATH, templates, recursive = True):
        if os.path.isdir(PATH):
            if recursive:
                recursiveList = os.walk(PATH)
            else:
                t = os.listdir(PATH)
                recursiveList = [(PATH, [], t)]

            for rootOriginal, dirs, files in recursiveList:
                for name in files:
                    if name.lower().endswith((".ardour")): #ignore README, ly or whatever.
                        with open(os.path.join(rootOriginal, name), 'r') as f: #peak into the first line of the file
                            if rootOriginal == PATH: #a normal template. just save the name as string for future reference
                                templates.add(name)
                            else: #a normal template, but nested. not allowed. If a bug happens it is here. Maybe there is too much detection or links are not recognizes. so better give a warning
                                warnings.warn("Template file "+ name +" in nested subdir not allowed. "+  rootOriginal  + " and " + PATH + " is not the same directory.")

    parseIncludes(TEMPLATE_ROOT, templates,) #system dir
    parseIncludes(USER_TEMPLATE_ROOT, templates,)  #user/config/home dir
    parseIncludes(COLLECTION_TEMPLATE_ROOT, templates, recursive = False) #collection root dir


    return templates

def create(temp, dataList, projectName, tempoMap):
    """temp is a string with the content of the ardour template."""
    temp = temp.replace("<!--LABOREJO-SESSION-->", '<Session version="3001" name="'+projectName+'" sample-rate="48000" id-counter="1" event-counter="9">')

    sources = []
    playlists = []
    audioPlaylists = []
    audioRoutes = []
    routes = []
    regions = []
    maxLength = []
    global ardourExportIDCounter
    ardourExportIDCounter = 100 #100 unique ids are reseverd for default empty.ardour items
    nrOfTracks = len(dataList)
    alreadyProcessedBusNames = set() #only process each busName once

    #first loop, create audio routes
    audioRouteDict = {} #key = busName, value = audio Route ID
    #datalist is a list of tuplets from api.exportMidi with parts=True.
    #[0] is the smf structure
    #[1] the unique track name
    #[2] the jack midi port as string "client:port"
    #[3]jack audio return port left, [4]jack audio return port right,
    #[5] filename with dirs, [6] filename without dirs
    #[6] maxticks, the end tick index of the track (technically the index of the appending position)
    for smfdata, uniquename, fullpath, filenameOnly, maximumTickindex in dataList:
        busName = ""
        audioRouteDict[busName] = getnum()  #it doesnt matter that this changes the number several times. We only need one constant number in the end, and this is what we get.

    #second loop, create midi data and audio routes
    for smfdata, uniquename, fullpath, filenameOnly, maximumTickindex in dataList:
        busName = ""
        maxLength.append(maximumTickindex*125)
        baseNum = getnum()
        regionNum = getnum()
        sources.append('<Source name="'+filenameOnly+'" type="midi" flags="Writable,CanRename" id="'+baseNum+'" origin=""/>')
        playlist = '<Playlist id="'+getnum()+'" name="'+uniquename+'" type="midi" orig-track-id="'+getnum()+'" frozen="no" combine-ops="0">'   #name here is the midi name without .mid, which is the laborejo track name
        playlistRegion = '<Region name="'+uniquename+'.1" position="0" length="'+str(maximumTickindex*125)+'" stretch="1" muted="0" opaque="1" locked="0" automatic="0" whole-file="0" import="0" external="0" sync-marked="0" left-of-split="0" right-of-split="0" hidden="0" position-locked="0" valid-transients="0" start="0" sync-position="0" ancestral-start="0" ancestral-length="0" shift="1" positional-lock-style="MusicTime" layering-index="0" start-beats="0" length-beats="4" id="'+regionNum+'" type="midi" first-edit="nothing" bbt-position="1|1|0" source-0="'+baseNum+'" master-source-0="'+baseNum+'"/>'
        playlistEnd = '</Playlist>'
        playlists.append(playlist + "\n" + playlistRegion + "\n" + playlistEnd)

        if not busName in alreadyProcessedBusNames: #Only generate one stereo-audio channel
            audioPlaylists.append('<Playlist id="'+getnum()+'" name="'+busName+'.1" type="audio" orig-track-id="'+audioRouteDict[busName]+'" frozen="no" combine-ops="0"/>')
            s1 =  '<Route id="'+audioRouteDict[busName]+'" name="'+busName+'" default-type="audio" active="yes" phase-invert="00" denormal-protection="no" meter-point="MeterInput" order-keys="EditorSort=1:MixerSort=1" self-solo="no" soloed-by-upstream="0" soloed-by-downstream="0" solo-isolated="no" solo-safe="no" monitoring="" saved-meter-point="MeterPostFader" mode="Normal">'
            s2 =  '<IO name="'+busName+'" id="'+getnum()+'" direction="Input" default-type="audio" user-latency="0">'
            s3 =  '<Port type="audio" name="'+busName+'/audio_in 1"> <Connection other="'+audioLeft+'"/> </Port>'
            s4 =  '<Port type="audio" name="'+busName+'/audio_in 2"> <Connection other="'+audioRight+'"/> </Port></IO>'
            s5 =  '<IO name="'+busName+'" id="'+getnum()+'" direction="Output" default-type="audio" user-latency="0">'
            s6 =  '<Port type="audio" name="'+busName+'/audio_out 1"> <Connection other="master/audio_in 1"/> </Port>'
            s7 =  '<Port type="audio" name="'+busName+'/audio_out 2"> <Connection other="master/audio_in 2"/> </Port> </IO>'

            s8 =  '<Controllable name="solo" id="'+getnum()+'" flags="Toggle" value="0.000000000000"/> <Controllable name="mute" id="'+getnum()+'" flags="Toggle" value="0.000000000000"/> <MuteMaster mute-point="PreFader,PostFader,Listen,Main" muted="no"/> <RemoteControl id="'+getnum()+'"/>'
            s9 =  '<Pannable> <Controllable name="pan-azimuth" id="'+getnum()+'" flags="" value="0.500000000000"/> <Controllable name="pan-width" id="'+getnum()+'" flags="" value="1.000000000000"/> <Controllable name="pan-elevation" id="'+getnum()+'" flags="" value="0.000000000000"/> <Controllable name="pan-frontback" id="'+getnum()+'" flags="" value="0.000000000000"/> <Controllable name="pan-lfe" id="'+getnum()+'" flags="" value="0.000000000000"/> <Automation/> </Pannable>'
            s10 = '<Processor id="'+getnum()+'" name="Meter" active="yes" user-latency="0" type="meter"/>'
            s11 = '<Processor id="'+getnum()+'" name="Amp" active="yes" user-latency="0" type="amp"> <Controllable name="gaincontrol" id="'+getnum()+'" flags="GainLike" value="1.000000000000"/> </Processor>'
            s12 = '<Processor id="'+getnum()+'" name="'+busName+'" active="yes" user-latency="0" own-input="yes" own-output="no" output="'+busName+'" type="main-outs" role="Main">'
            s13 = '<PannerShell bypassed="no"> <Panner type="Equal Power Stereo"/> </PannerShell> </Processor> <Controllable name="recenable" id="'+getnum()+'" flags="Toggle" value="1.000000000000"/> <Diskstream flags="Recordable" playlist="'+busName+'.1" name="'+busName+'" id="'+getnum()+'" speed="1.000000" capture-alignment="Automatic" channels="2"/> </Route>'

            audioRoutes.append("\n".join([s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13]))
            alreadyProcessedBusNames.add(busName)

        regions.append('<Region name="'+uniquename+'" position="0" length="'+str(maximumTickindex*125)+'" stretch="1" muted="0" opaque="1" locked="0" automatic="0" whole-file="0" import="0" external="0" sync-marked="0" left-of-split="0" right-of-split="0" hidden="0" position-locked="0" valid-transients="0" start="0" sync-position="0" ancestral-start="0" ancestral-length="0" shift="1" positional-lock-style="MusicTime" layering-index="0" start-beats="0" length-beats="4" id="'+regionNum+'" type="midi" first-edit="nothing" bbt-position="1|1|0" source-0="'+baseNum+'" master-source-0="'+baseNum+'"/>')

        r1  = '<Route id="'+getnum()+'" name="'+uniquename+'" default-type="midi" active="yes" phase-invert="" denormal-protection="no" meter-point="MeterPostFader" order-keys="EditorSort=1:MixerSort=1" self-solo="no" soloed-by-upstream="0" soloed-by-downstream="0" solo-isolated="no" solo-safe="no" monitoring="" saved-meter-point="MeterPostFader" note-mode="Sustained" step-editing="no" input-active="yes">'
        r1b = '<IO name="'+uniquename+'" id="'+getnum()+'" direction="Input" default-type="midi" user-latency="0"> <Port type="midi" name="'+uniquename+'/midi_in 1"> <Connection other="alsa_pcm:Hammerfall-DSP/midi_capture_1"/> </Port> </IO>'
        r2 = '<IO name="'+uniquename+'" id="'+getnum()+'" direction="Output" default-type="midi" user-latency="0">'
        r3 = ''
        r4 = '<Controllable name="solo" id="'+getnum()+'" flags="Toggle" value="0.000000000000"/>'
        r5 = '<Controllable name="mute" id="'+getnum()+'" flags="Toggle" value="0.000000000000"/>  <MuteMaster mute-point="PreFader,PostFader,Listen,Main" muted="no"/>'
        r6 = '<RemoteControl id="'+getnum()+'"/>'
        r7 = '<Pannable> <Controllable name="pan-azimuth" id="'+getnum()+'" flags="" value="0.500000000000"/>'
        r8 = '<Controllable name="pan-width" id="'+getnum()+'" flags="" value="0.000000000000"/>'
        r9 = '<Controllable name="pan-elevation" id="'+getnum()+'" flags="" value="0.000000000000"/>'
        r10 = '<Controllable name="pan-frontback" id="'+getnum()+'" flags="" value="0.000000000000"/>'
        r11 = '<Controllable name="pan-lfe" id="'+getnum()+'" flags="" value="0.000000000000"/> <Automation/> </Pannable>'
        r12 = '<Processor id="'+getnum()+'" name="Amp" active="yes" user-latency="0" type="amp">'
        r13 = '<Controllable name="gaincontrol" id="'+getnum()+'" flags="GainLike" value="1.000000000000"/> </Processor>'
        r14 = '<Processor id="'+getnum()+'" name="Meter" active="yes" user-latency="0" type="meter"/>'
        r15 = '<Processor id="'+getnum()+'" name="'+uniquename+'" active="yes" user-latency="0" own-input="yes" own-output="no" output="'+uniquename+'" type="main-outs" role="Main"> <PannerShell bypassed="no"/> </Processor>'
        r16 = '<Controllable name="recenable" id="'+getnum()+'" flags="Toggle" value="0.000000000000"/>'
        r17 = '<Diskstream flags="Recordable" playlist="'+uniquename+'" name="'+uniquename+'" id="'+getnum()+'" speed="1.000000" capture-alignment="Automatic" channel-mode="FilterChannels" channel-mask="0xffff"/> </Route>'
        routes.append("\n".join([r1, r1b, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17]))

    temp = temp.replace("<!--LABOREJO-SOURCES-->", "\n".join(sources))
    temp = temp.replace("<!--LABOREJO-REGIONS-->", "\n".join(regions))
    temp = temp.replace("<!--LABOREJO-PLAYLISTS-->", "\n".join(playlists))
    temp = temp.replace("<!--LABOREJO-AUDIO-PLAYLISTS-->", "\n".join(audioPlaylists))
    temp = temp.replace("<!--LABOREJO-ROUTES-->", "\n".join(audioRoutes))
    temp = temp.replace("<!--LABOREJO-AUDIO-ROUTES-->", "\n".join(routes))
    temp = temp.replace("<!--LABOREJO-TEMPOMAP-->", tempoMap)
    temp = temp.replace("<!--LABOREJO-LENGTHLOOP-->", str(max(maxLength)))
    temp = temp.replace("<!--LABOREJO-LENGTHSESSION-->", str(max(maxLength)))
    #temp = temp.replace("%$$DATE$$", da.today().strftime("%A %d. %B %Y")) #The current date

    return temp

def save(ardourPath, dataList, projectName, tempoMap, templateData):
    """ardourPath is the the directory of the project,
    e.g. /home/user/ardour/foo-project/

    So we have /home/user/ardour/foo-project/snapshot.ardour
    and the midi files are already placed by the api call in
    /home/user/ardour/foo-project/interchange/foo-project/midifiles/
    """

    templateString, TEMPLATE_ROOT, USER_TEMPLATE_ROOT, COLLECTION_TEMPLATE_ROOT = templateData #(read_data, TEMPLATE_ROOT, USER_TEMPLATE_ROOT, COLLECTION_TEMPLATE_ROOT) forget everything except the string.
    finishedArdourProjectString = create(templateString, dataList, projectName, tempoMap)
    f = open(ardourPath + "/laborejo.ardour", 'w')
    f.write(finishedArdourProjectString)

def generateTempoMap(unfoldedWorkspace, TempoSignature, TimeSignature):
    """Generates an Ardour tempo map which consists of timesigs and
    temposigs.
    Ardour only allows one timesig at a time. And it allows only one
    temposig at a time, but this is true for Laborejo also."""

    #tempoList = []
    #timeList = []
    stringList = []
    defaultTempo = '<Tempo start="1|1|0" beats-per-minute="120.000000" note-type="4.000000" movable="no"/>' #laborejo default
    defaultMeter = '<Meter start="1|1|0" note-type="4.000000" divisions-per-bar="4.000000" movable="no"/>'
    for cursor in unfoldedWorkspace.cursors:
        cursor.go_to_head()
        cursor.resetToDefault()
        for obj in cursor.iterate():
            #print (cursor.measureNumber, ":", cursor.metricalPosition, obj)
            t = type(obj)
            if t is TempoSignature:
                #tempoList.append((obj, cursor.measureNumber+1, cursor.metricalPosition+1, cursor.ticksSinceLastMeasureStart))
                pos = str(cursor.measureNumber+1) + "|" + str(int(cursor.metricalPosition+1)) + "|0"  #TODO: figure out the third ardour position "ticks". No matter what I do in Ardour I always end up with "0" in the save file.
                noteType = str(int(1536 / obj.referenceTicks)) + ".000000"
                if pos == "1|1|0":
                    defaultTempo = '<Tempo start="1|1|0" beats-per-minute="'+ str(int(obj.beatsPerMinute)) + '.000000" note-type="'+ noteType +'" movable="no"/>'
                else:
                    stringList.append('<Tempo start="'+pos+'" beats-per-minute="'+ str(int(obj.beatsPerMinute)) + '.000000" note-type="'+ noteType +'" movable="yes"/>')
            elif t is TimeSignature:
                #timeList.append((obj, cursor.measureNumber+1, cursor.metricalPosition+1, cursor.ticksSinceLastMeasureStart))
                beatPos = int(cursor.metricalPosition+1)
                if beatPos != 1:
                    warnings.warn("Ardour, and music in general, allows Time Signatures only at the beginning of a measure. Please change your Laborejo file to avoid this error. Beat position was forced to 1. Measure: " + str(cursor.measureNumber+1) +  " Track: " + cursor.nested_list.uniqueContainerName)
                    beatPos = 1
                pos = str(cursor.measureNumber+1) + "|" + str(beatPos) + "|0"  #TODO: figure out the third ardour position "ticks". No matter what I do in Ardour I always end up with "0" in the save file.
                noteType = str(int(1536 / obj.denominator)) + ".000000"
                if pos == "1|1|0":
                    defaultMeter = '<Meter start="1|1|0" note-type="'+noteType+'" divisions-per-bar="'+ str(int(obj.nominator))+'.000000" movable="no"/>'
                else:
                    stringList.append('<Meter start="'+pos+'" note-type="'+noteType+'" divisions-per-bar="'+ str(int(obj.nominator))+'.000000" movable="yes"/>')
    return defaultTempo + "\n" + defaultMeter + "\n" + "\n".join(list(set(stringList))) #remove duplicates and generate final string



