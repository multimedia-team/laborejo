# -*- coding: utf-8 -*-
#This is the central place where it all comes together.
#Anything in here is the last stage before user exposure. Nothing in the whole package refers back to these API functions.
from laborejocore import core, items, pitch, saveload, listener, helper, playback, lilypond, substitutiondatabase, ardour, exceptions
import gettext, random, math, re, os.path, collections, warnings, time
from operator import attrgetter
from copy import deepcopy
global compoundTake, notDuringUndo  #From undo/redo
global cboxSmfCompatibleModule #Playback is off. You have to start it manually
cboxSmfCompatibleModule = None
compoundTake = False
notDuringUndo = True #Normally we are not during undo. But if an undo action itself normally saves an undo state we need to deactivate it. (insert <-> delete)
localepath = os.path.join(os.path.dirname(__file__), 'locale')
gettext.install('laborejocore', localepath) #installs _() into the global namespace.

#t = gettext.translation('laborejocore', localepath, codeset="utf-8")
#_ = t.lgettext

"""
"Commands" vs "Functions" vs "Methods":
    Commands are higher level functions, directly available to the user,
    normally through a GUI. There are no comands in core, they start
    here and more may be added in a GUI.
    Functions and Methods refer to the technical python term.
    There may be functions in API, but only inside commands and all
    global API functions are marked _private.

All commands that work on a single note and not on the whole item/chord
should use "Note" as first part of their name, after generic prefixes
like "toggle". Tie chord is: toggleTie, tie note is toggleNoteTie.
Item commands are the default, don't use the term "item" in a public
command. Internal is ok, to be clear. (like delete directives)

All commands that work on some "current item" should prevent
"current" in their name for the sake of shortness. Especially if
they have no parameter.

Try to mimic the Lilypond naming. It should be clear for a programmer
what the command does if he/she knows the Lilypond syntax.
"""

STANDALONE = True #switch this to false and the listener will send messages.
#Listeners
l_global = listener.ListenerGlobal()
l_cursor = listener.ListenerCursor()
l_item = listener.ListenerItem()
l_track = listener.ListenerTrack()
l_score = listener.ListenerScore()
l_form = listener.ListenerFormGenerator()
l_chord = listener.ListenerChord()
l_note = listener.ListenerNote()
l_record = listener.ListenerRecord()
l_playback = listener.ListenerPlayback()
l_collections = listener.ListenerCollections()

def l_send(method):
    if not STANDALONE:
        method() #for example lambda: l_cursor.setPitch(1920)

#API Functions
###############
def _getSession():
    return core.session

def _getConfigDir():
    return _getSession().configdir

def _getWorkspace():
    return _getSession().workspace

def _getTrackIndex():
    """return the track number. Better use _getTrack() directly"""
    return _getScore().currentTrackIndex()

def _getTrackByIndex(index):
    """Return a specific track by its index nummer"""
    return _getScore().container[index]

def _getIndexByTrack(track):
    """Return a track index/cursor number for a track"""
    return _getScore().container.index(track)

def _getTrack():
    """return the instance of the active track"""
    return _getTrackByIndex(_getTrackIndex())

def _getContainer():
    """Return the instance of the current container.
    Does not return the track or score. If we are at track level/0
    it returns None"""
    return _getActiveCursor().get_container()

def _getIndex():
    return _getActiveCursor().stack

def _getIndexFromContainerStart():
    return _getActiveCursor().stack[-1]

def _getFlatCursorIndex():
    """Returns a cursor index for the flattened active track"""
    #return(sum(_getActiveCursor().stack))
    return _getActiveCursor().flatCursorIndex

def _getScore():
    """return the instance of the active score"""
    return _getWorkspace().score

def _getHeader():
    """Return the instance of the active header for this workspace"""
    return _getScore().header

def _getActiveCursor():
    return _getScore().activeCursor

def _getTickIndex():
    return _getActiveCursor().tickindex

def _getCursorItem():
    """return the instance of the item where the cursor points to"""
    return _getActiveCursor().get_item()

def _getCursorPitch():
    return _getActiveCursor().pitchindex

def _getCursorDuration():
    return _getActiveCursor().prevailingDuration

def _getKeysig():
    return _getActiveCursor().prevailingKeySignature[-1]

def _getTimesig():
    return _getActiveCursor().prevailingTimeSignature[-1]

def _getClef():
    return _getActiveCursor().prevailingClef[-1]

def _getNotes():
    """Returns an iterator for all notes"""
    return iter(_getCursorItem().notelist)

def _getClosestNoteToCursor():
    """Return the instance of the nearest note to the cursor"""
    return min((abs(_getCursorPitch() - i.pitch), i) for i in _getCursorItem().notelist)[1]

def _getLowestNote():
    """Return the instance of the lowest note in the current chord"""
    return _getCursorItem().notelist[0]

def _getHighestNote():
    """Return the instance of the lowest note in the current chord"""
    return _getCursorItem().notelist[-1]

def _modifyNotePitch(methodAsString):
    """This can't be done by the backend or would need a function in
    class Chord which does not know the cursor as good as the api"""
    item = _getCursorItem()
    a = isinstance(item, items.Appending) #includes Start and End
    if a:
        left()
    getattr(_getClosestNoteToCursor(), methodAsString)(keysig = _getKeysig())
    item.notelist.sort(key=attrgetter('pitch')) #sort after pitchmod
    l_send(lambda: l_item.updateDynamic(item))
    if a:
        right()

def _insert(item, standaloneInsert = True):
    """The only function that inserts items into the score"""
    if isinstance(item, items.Appending):  #Don't duplicate, link ... Appending Items and Start/End items.
        #This does not exclude inserting complete containers. The start/end in there will be inserted. They are already there :)
        return False
    else:
        getContainer = _getContainer()
        if item is getContainer: #prevent recursive containers
            warnings.warning(_("Recursive Containers are not allowed. A container is not allowed to exist in itself"))
            return False
        typ = type(item)
        typQ = typ is core.Container
        if typQ: #container?
            saveState(item, "insertContainer", True) #a fake signal. Undo knows how to deal with it. # We need to save the cursor position, which is done in saveState, at the right time.
        else: #normal item
            saveState(item, "insert", True) #a fake signal. Undo knows how to deal with it. # We need to save the cursor position, which is done in saveState, at the right time.
        _getScore().insertAtCursor(item)
        if typQ:
            _getScore().nonTrackContainers.setdefault(item, []).append(True) #count the instances. Deletion is only done by the API as well
        l_send(lambda: l_item.insertAtCursor(item, getContainer = getContainer, standaloneInsert = standaloneInsert))
        if typQ or getContainer:
            syncCursors()
        l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
        return item


def _standalone(lilystring = ""):
    """A blank item. Does nothing except idling in its container"""
    item = items.Item(_getWorkspace())
    item.lilypond = lilystring
    _insert(item)

def _standaloneWaitForNextChord(lilystring):
    item = items.WaitForChord(_getWorkspace(), lilystring)
    _insert(item)

def _lilyPre(tag, lily=None):
    """Item Prefix Directive. both parameters must be strings
    Call without lily string to get a current directive,
    call with lily string to set a new directive."""
    i = _getCursorItem()
    if lily: #set
        i.directivePre[tag] = lily
        l_send(lambda: l_item.extensions(i))
        return True
    else:  #get
        if tag in i.directivePre:
            return i.directivePre #found directive, return complete dict
        else:
            return {} #no directive with this tag.

def _lilyMid(tag, lily=None):
    """Item Prefix Directive. both parameters must be strings
    Call without lily string to get a current directive,
    call with lily string to set a new directive."""
    i = _getCursorItem()
    if lily: #set
        i.directiveMid[tag] = lily
        l_send(lambda: l_item.extensions(i))
        return True
    else:  #get
        if tag in i.directiveMid:
            return i.directiveMid #found directive, return complete dict
        else:
            return {} #no directive with this tag.

def _lilyPst(tag, lily=None):
    """Item Postfix Directive. both parameters must be strings
    Call without lily string to get a current directive,
    call with lily string to set a new directive."""
    i = _getCursorItem()
    if lily: #set
        i.directivePst[tag] = lily
        l_send(lambda: l_item.extensions(i))
        return True
    else:  #get
        if tag in i.directivePst:
            return i.directivePst #found directive, return complete dict
        else:
            return {} #no directive with this tag.

def _lilyNotePre(tag, lily=None):
    """Item Postfix Directive. both parameters must be strings"""
    if lily: #set
        _getClosestNoteToCursor().directivePre[tag] = lily
        l_send(lambda: l_item.extensions(_getCursorItem()))
        return True
    else:  #get
        if tag in _getClosestNoteToCursor().directivePre:
            return _getClosestNoteToCursor().directivePre #found directive, return complete dict
        else:
            return {} #no directive with this tag.

def _lilyNoteMid(tag, lily=None):
    """Item Postfix Directive. both parameters must be strings"""
    if lily: #set
        _getClosestNoteToCursor().directiveMid[tag] = lily
        l_send(lambda: l_item.extensions(_getCursorItem()))
        return True
    else:  #get
        if tag in _getClosestNoteToCursor().directiveMid:
            return _getClosestNoteToCursor().directiveMid #found directive, return complete dict
        else:
            return {} #no directive with this tag.

def _lilyNotePst(tag, lily=None):
    """Item Postfix Directive. both parameters must be strings"""
    if lily: #set
        _getClosestNoteToCursor().directivePst[tag] = lily
        l_send(lambda: l_item.extensions(_getCursorItem()))
        return True
    else:  #get
        if tag in _getClosestNoteToCursor().directivePst:
            return _getClosestNoteToCursor().directivePst #found directive, return complete dict
        else:
            return {} #no directive with this tag.

def _playbackPre(tag, midi=None):
    i = _getCursorItem()
    if midi: #set
        i.instructionPre[tag] = midi
        l_send(lambda: l_item.extensions(i))
        return True
    else:  #get
        if tag in i.instructionPre:
            return i.instructionPre #found directive, return complete dict
        else:
            return {} #no directive with this tag.

def _playbackPst(tag, midi=None):
    i = _getCursorItem()
    if midi: #set
        i.instructionPst[tag] = midi
        l_send(lambda: l_item.extensions(i))
        return True
    else:  #get
        if tag in i.instructionPst:
            return i.instructionPst #found directive, return complete dict
        else:
            return {} #no directive with this tag.

#TODO: Note playback instructions? Technically possible, but needed? If yes don't forget to delete.

def _deleteDirective(_where, tag):
    """Delete any single directive of a given tag.
    The _where command specifies if an item, note or anything else.
    This command simply deletes the directive. it does not update
    any gui."""
    try:
        del _where(tag)[tag]
        l_send(lambda: l_chord.extensions(_getCursorItem()))
        return True
    except KeyError:
        return False #no directive with this tag.

def _deleteItemDirectives(tag): #TODO: Why private?
    """No matter if pre, middle or post. They all have to go
    For one tag"""
    #TODO: This is three times get Cursor item internally. Optimisation possible with custom code. Get the instance once, delete the tags.
    _deleteDirective(_lilyPre, tag)
    _deleteDirective(_lilyMid, tag)
    _deleteDirective(_lilyPst, tag)

def _deleteNoteDirectives(tag): #TODO: Why private?
    """Delete pre, middle and post directive of a single note under the
    cursor."""
    #TODO: This is three times get Cursor item internally. Optimisation possible with custom code. Get the instance once, delete the tags.
    _deleteDirective(_lilyNotePre, tag)
    _deleteDirective(_lilyNoteMid, tag)
    _deleteDirective(_lilyNotePst, tag)

def deleteItemPlaybackInstructions(tag):
    """Delete all pre and pst instructions"""
    _deleteDirective(_playbackPre, tag)
    _deleteDirective(_playbackPst, tag)

def deleteAllItemDirectives():
    def do():
        item = _getCursorItem()
        item.directivePre = {}
        item.directiveMid = {}
        item.directivePst = {}
        item.instructionPre = {}
        item.instructionPst = {}
    command(do, signal = l_chord.extensions)

def deleteAllNoteDirectives():
    """Delete any note directive on any note in this chord"""
    def do():
        for x in _getCursorItem().notelist:
            x.directivePre = {}
            x.directiveMid = {}
            x.directivePst = {}
            x.instructionPre = {}
            x.instructionPst = {}
    command(do, signal = l_chord.extensions)

def deleteAllDirectives():
    """Clean all note and item directives on the current item"""
    deleteAllItemDirectives()
    deleteAllNoteDirectives()

def _toggleDirective(_where, tag, lily): #Lily can also be midi data.
    """Creates a directive. If tag already present delete directive.
    If the given lily string is not the present one its overwritten.
    This means multiple commands using the same tag will not toggle each
    other but overwrite first.
    Example:
    A pizzicato[tag=pizz] command on a plain note creates a directive
    A pizzicato[tag=pizz[ command deletes a pizzicato directive.
    Bartok pizz [tag=pizz] overwrites a present pizzicato directive
    with a bartok pizzicato or deletes a bartok pizzicato.
    """
    get = _where(tag)
    if get :#is there is a directive
        if get[tag] == lily: #has it a different string?
            _deleteDirective(_where, tag) #its exactly the same. Delete
            return False
        else:
            _where(tag, lily) #overwrite existing directive value under the same tag.
            return True
    else:
        _where(tag, lily) #No directive of this tag presenet. Create new
        return True

#GUI only:
##########
def message(message, title = _("Laborejo Message"), function = None):
    """Outputs a simple warning message and waits for the user
    to accept or cancel.
    Can be used in the gui or in textmode.
    Can be used as if statement: if warning("foo").
    If function is provided the function is called to calculate the
    return message. The function needs to accept one bool parameter
    which is the Users Ok/Cancel."""
    if STANDALONE:
        raise RuntimeError("Encountered Textmode warning. Not supported. Stopping to prevent damage. Message:", message)
        exit() #TODO: Create textmode question.
    else:
        ret = l_form.generateForm({}, title, message)
        if ret is not None:
            if function:
                return function(ret)
            else:
                return True
        else:
            if function:
                return function(ret)
            else:
                return False

def _dict2dynformDict(dictionary, extraTags = None):
    """return a new dict which is compatible to dynformcreator:
    dict[k] = v  -->  dict[k] = (k, v).
    The k in the tuple (k, v) is the printed tag.

    Instead of k you can give extra Tags which will be used instead.
    extraTags key =  original dict key
    extraTags value = printed tag (no other usage)
    """
    new = {}
    if extraTags:
        for k,v in dictionary.items():
            new[k] = (extraTags[k], v)
    else:
        for k,v in dictionary.items():
            new[k] = (k, v)
    return new

def _editDictWithGui(data, title, comment):
    """Generate a gui to edit a dictionary in place.
    Leave field blank to delete the dict entry"""
    result = l_form.generateForm(_dict2dynformDict(data), title, comment)
    if result:
        for k,v in x.items():
            if v:
                data[k] = v
            else:
                del data[k]

def _editMultipleDictWitTabbedhGui(data, title):
    """Change multiple dicts in place with a generated, tabbed gui.
    Wants a list of lists:[dict, tab-title, tab-comment].
    DON'T use tuples, only lists"""
    newdata = deepcopy(data) #make a copy
    for x in newdata:
        x[0] = _dict2dynformDict(x[0]) #replace each dict in the copied data with a transformed gui dict. But leave original data intact so we can later change it.

    result = l_form.generateForm(newdata, title) #returns a list of dicts

    if result:
        for number, dictionary in enumerate(result):
            for k,v in dictionary.items():
                origDict = data[number][0]
                if v:
                    origDict[k] = v #0 is the dict position, 1 and 2 are gui strings.
                else:
                    del origDict[k]
        return True
    else:
        return False

def _deleteFromDictWithGui(dictionary, title, comment):
    """Create a shadow dict which has the same tags but "False" as value
    then delete all entries from the original dict which were switched
    to True by the user"""
    new = {}
    for k,v in dictionary.items(): #key are strings
        new[k] = (k, False)
    new["0000All"] = ("Delete All", False)

    result = l_form.generateForm(new, title, comment)
    if result:
        if result["0000All"]:
            dictionary.clear()
            return True
        else:
            del result["0000All"]
            for k,v in result.items():
                if v:
                    del dictionary[k]
            return True
    else: #Canceled GUI.
        return False

def _generateDropDownGuiFromFunctions(functionlist, default, label, title="", comment=""):
    """Creates a gui with a dropdown list to choose a function by
    its pretty string name.
    functionlist is a list of tuplets:
    [(function, "string"), ...]"""
    functionlist.insert(0, default)
    result = l_form.generateForm({"singleEntry":(label, functionlist)}, title, comment)
    if result:
        result["singleEntry"]()

def rawEditGui():
    """edit the items python __dict__ directly with a generated GUI.
    Only for debugging and testing. No checks, no safety, no warranty."""

    plainDict = {}
    data = [[plainDict, "General", ""]]
    for key, value in _getCursorItem().__dict__.items(): #key is the instance variable name and value its value, which might be another dict
        if type(value) is dict:
            data.append([value, key, ""])
        else:
            plainDict[key] = value
    #print()
    #import pprint
    #pprint.pprint (data)

    #_editMultipleDictWitTabbedhGui(data, "Raw Item Edit")
    #_editDictWithGui(plainDict, "bla", "so")

    #TODO: this is broken because fedit is not powerful enough. It can't handle dicts, empty lists etc. etc. don't work around here by filtering data. Work in fedit.





def metadataGui():
    """Provide a gui dialog to ask all lilypond metadata"""
    header = _getHeader().data

    data = {"00title":(_("Title"), header["title"]),
            "02subtitle":(_("Subtitle"), header["subtitle"]),
            "04dedication":(_("Dedication"), header["dedication"]),
            "06composer":(_("Composer"), header["composer"]),
            "08subsubtitle":(_("Subsubtitle"), header["subsubtitle"]),
            "10poet":(_("Poet"), header["poet"]),
            "12instrument":(_("Instrument"), header["instrument"]),
            "14meter":(_("Meter"), header["meter"]),
            "16arranger":(_("Arranger"), header["arranger"]),
            "18piece":(_("Piece"), header["piece"]),
            "20opus":(_("Opus"), header["opus"]),
            "22copyright":(_("Copyright"), header["copyright"]),
            "24tagline":(_("Tagline"), header["tagline"]),
            "30subtextlabel":(None, _("Subtext")),
            "31subtext":("", _getScore().subtext),
            "32subtextlabel":(None, _("&lt;br&gt; for a line break. Empty line for new paragraph.\nMore with Lilypond Markup Syntax")),
            }
    result = l_form.generateForm(data, _("Metadata"), _("These will be visible in the print out as well.\nAn entry staring with \\markup makes the field treated as literal lilypond code.\nYou have to \"quote\" strings yourself then."))
    if result: #Cancel?
        _getScore().subtext = result["31subtext"]
        #Now remove the subtexts and loop over the rest
        del result["31subtext"]

        for key, value in result.items():
            header[key[2:]] = value
        return True
    else:
        return False

def directivesGui(backendItem):
    """Provide a gui dialog to review and change all directives"""
    data = [[backendItem.directivePre, "Prefix", _("Before the note:\nprefix<c'>2\n\nLeave field blank to delete the directive.")],
            [backendItem.directiveMid, "Middle", _("Between notes and duration:\n<c'>middle2\n\nLeave field blank to delete the directive.")],
            [backendItem.directivePst, "Postfix", _("After the duration:\n<c'>2postfix\n\nLeave field blank to delete the directive.")],
            [backendItem.instructionPre, "Pre Midi", _("Executed before playing back this item.\nInsert Midibytes as a list.\nLeave field blank to delete the directive.") ],
            [backendItem.instructionPst, "Post Midi", _("Executed after playing back this item.\nInsert Midibytes as a list.\nLeave field blank to delete the directive.") ],
            ]
    if _editMultipleDictWitTabbedhGui(data, "Lilypond Directives"):
        l_send(l_score.trackChanged)
        l_send(l_score.trackDirectivesChanged)

     #"Additional Lilypond instructions before, in between and after this item"

def createDirectivesGui():
    """The directive keys get an "usr_" prefix so they don't collide
    with internal keys"""
    data = {"01a":(None, "PREFIX<c>4"),
            "01pre":("Prefix", ""),
            "02a":(None, "<c>MIDDLE4"),
            "02mid":("Middle", ""),
            "03a":(None, "<c>4POSTFIX"),
            "03pst":("Postfix", ""),
            "04midipre":("Midi Pre Instruction", ""),
            "05midipst":("Midi Post Instruction", ""),
            }
    ret = l_form.generateForm(data, _("Create Object Directives"), _("Leave fields blank to not create any directive of this kind." ))
    if ret: #cancel?
        item = _getCursorItem()
        if ret["01pre"]: #not empty
            item.directivePre["usr_" + str(random.randrange(1,1000))] = ret["01pre"]
        if ret["02mid"]: #not empty
            item.directiveMid["usr_" + str(random.randrange(1,1000))] = ret["02mid"]
        if ret["03pst"]: #not empty
            item.directivePst["usr_" + str(random.randrange(1,1000))] = ret["03pst"]
        if ret["04midipre"]: #not empty
            item.instructionPre["usr_" + str(random.randrange(1,1000))] = ret["04midipre"]
        if ret["05midipst"]: #not empty
            item.instructionPst["usr_" + str(random.randrange(1,1000))] = ret["05midipst"]
        l_send(lambda: l_item.update(item))

def editGui():
    """A general purpose edit that will detect what item is under
    the cursor and call the proper function.
    No advanced functionality like undo in here. This is just a
    convenience if/elif function"""
    types = {
            items.Chord : editDirectives,
            items.PerformanceSignature : editPerformanceSignature,
            items.Markup : editMarkup,
            items.Chord : editDirectives,
            items.RepeatClose : editRepeat,
            items.RepeatCloseOpen : editRepeat,
            items.GotoCapo : editDaCapoDalSegno,
            items.GotoSegno : editDaCapoDalSegno,
            items.Start : editContainer,
            items.End : editContainer,
            items.AlternateEnd : editAlternateEnd,
            items.ChannelChangeRelative : editChannelChangeRelative,
            items.ChannelChangeAbsolute : editChannelChangeAbsolute,
            items.ProgramChangeRelative : editProgramChangeRelative,
            items.ProgramChangeAbsolute : editProgramChangeAbsolute,
            items.TempoModification : editTempoModification,
            items.DynamicSignature : editDynamicSignature,
            items.InstrumentChange : editInstrumentChange,
            items.TempoSignature : editTempoSignature ,
            core.Container : editContainer,
            }

    typ = type(_getCursorItem())
    if typ in types:
        types[typ]()
    else:
        return False


def editContainer():
    typ = type(_getCursorItem())
    if typ is core.Container:
        cont = _getCursorItem()
    elif typ is items.Start:
        cont = _getCursorItem().parentContainer
    else:
        cont = _getContainer()
    if cont:
        #ret = l_form.generateForm({"repeatPercent":(_("Percent-Repeat incl. first"), cont.repeatPercent)}, _("Edit Container: " + cont.uniqueContainerName), "")
        ret = l_form.generateForm({"repeatPercent":(_("Percent-Repeat incl. first"), cont.repeatPercent), "cursorWalk":(_("Cursor Walk"), cont.cursorWalk), "uniqueContainerName":(_("Unique Name"), cont.uniqueContainerName), },  _("Edit Container: ") + cont.uniqueContainerName)
        if ret:
            cont.repeatPercent = ret["repeatPercent"]
            cont.uniqueContainerName = ret["uniqueContainerName"]
            #cont.uniqueContainerName = ret["uniqueContainerName"]
            walkChanged = False if cont.cursorWalk == ret["cursorWalk"] else True
            cont.cursorWalk = ret["cursorWalk"]
            if walkChanged:
                #This is a big change. Trigger a full gui redraw #TODO: currently. This is very qt gui specific.
                l_send(l_score.fullResetAndRedraw)
                return True #and exit this function

            l_send(lambda: l_item.updateStatic(cont))
            l_send(lambda: l_item.updateDynamic(cont))
            l_send(lambda: l_item.updateStatic(cont.container[0]))
            l_send(lambda: l_item.updateStatic(cont.container[-1]))

            syncCursors()
            l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
            if cont.cursorWalk:
                l_send(lambda: l_score.updateTrackFromItem(cont[-1])) #update from the End marker, which maybe has a new duration
            else:
                l_send(lambda: l_score.updateTrackFromItem(cont)) #update from the Container istself, which maybe has a new duration

def guiChooseContainer():
    """Gui Only. Offer a choice from existing containers."""
    dictSortedAfterName = {k.uniqueContainerName:k for k,v in _getScore().nonTrackContainers.items()}
    autolist = list(dictSortedAfterName.keys())
    ret = l_form.generateForm({"text":("","")}, "Insert Container", "Type in Container name", autocomplete=autolist)
    if ret and ret["text"] in autolist:
        cont = dictSortedAfterName[ret["text"]]
        insertContainer(cont)

def createContainerWorkspace(container = None, fullLoad = True):
    """Creates a new, unsavable Workspace for only one container.
    The Container is still linked with the original one.

    You get an active Cursor to operate.

    It operates, transparently, on a container with slice [1:] so that
    you can't insert anything before the core.Start instance.

    Also Playback, Ly Export etc. will be disabled.
    """ #TODO: Why? It is not needed, but does it hurt? The only real problem is the saving which must not be allowed since we loose the python instance reference.
    #TODO: Copy the user substitutions to this workspace. Or better still: link them as well. No why?! these are just strings. There is no export where they are needed.
    if not container:
        if type(_getCursorItem()) is core.Container:
            container = _getCursorItem()
        else:
            container = _getContainer()
    if not container: #Not in a real container. Use the track instead
        container = _getTrack()
    workspace = core.Workspace()
    workspace.originalStandaloneContainer = container
    tr = workspace.score.addTrack()
    tr.uniqueContainerName = container.uniqueContainerName
    tr.container = container.container #replace the data

    if fullLoad: #trigger a new GUI file/tab and fully load the file into the current session.
        core.session.load(workspace)
        workspace.lastKnownFilename = "" #TODO: Maybe something different to indicate the GUI that this is a non-savable container? But why isn't it?  This becomes easily track extract or contrainer extract this way.
        #l_send(lambda: l_global.new(workspace))
        #l_send(lambda: l_global.workspaceChanged(workspace))
    return workspace

def editTrigger():
    item = _getCursorItem()
    start = item.triggerString
    if start is None:
        start = ""
    ret = l_form.generateForm({"triggerString":("", start)}, _("Edit Playback Trigger"), _("You must specify a variable 'ret' as True or False.\nThis will be used to determin if this item will be played back or not."))
    if ret:
        saveState(item, l_item.smfTrigger, durationChanged = False)
        item.triggerString = ret["triggerString"]
    l_send(lambda: l_item.smfTrigger(item))

def editDirectives():
    directivesGui(_getCursorItem())

def editStandalone():
    """The edit variants are gui only. Any script can directly access .lilypond"""
    ret = l_form.generateForm({"standalone":(" ", str(_getCursorItem().lilypond))}, _("Standalone Lilypond Directive"), _("Lilypond text to insert on this position.\n\nThis will override any generated (notes, clefs etc.) text.\nAn empty string, no spaces, will enable auto-generation again."))
    if ret:
        _getCursorItem().lilypond = ret["standalone"]

def editStandaloneWaitForNextChord():
    """The edit variants are gui only. Any script can directly access .lilystring"""
    ret = l_form.generateForm({"standalone":(" ", str(_getCursorItem().lilystring))}, _("Wait-For-Next-Chord Lilypond Directive"), _("The text will be used as post directive for the next chord/note."))
    if ret:
        _getCursorItem().lilystring = ret["standalone"]

def editPerformanceSignature(default=False):
    """Create a tabbed gui edit which modifies all perf-sig values.
    if default = False it will edit the default one instead of
    the cursor one"""
    if default:
        item = _getScore().defaultPerformanceSignature
    else:
        item = _getCursorItem()

    if type(item) is items.PerformanceSignature:
        data1 = {
                "00name":(_("Optional: Name"), item.name),
                "00r" : (None, ""),
                "00staccato":(_("Staccato"), item.staccato),
                "00z-tenuto":(_("Tenuto"), item.tenuto),
                "01legatoScaling":(_("Legato (Slurs)\nSmaller value means shorter duration. Must be bigger than 0. Full Length: 1. "), item.legatoScaling),
                "01slurEndScaling":(_("Slur End\nSmaller value means shorter duration. Must be bigger than 0. Default: 0.5. "), item.slurEndScaling),
                "01nonLegatoScaling":(_("Non Legato/Detache\nSmaller value means shorter duration.\nSet to 0 to activate scalings by note duration (see Tab)."), item.nonLegatoScaling),
                "02nonLegatoHumanizing":(_("Non Legato Humanizing\n1 means no change.\n<1 means a random chance to shorten the duration (e.g. 0.75)\n>1 means a random chance to extend the duration. (e.g. 1.25)"), item.nonLegatoHumanizing),
                "03z" : (None, _("\nFermata values modify the tempo.\nSmaller value means longer fermata.")),
                "04shortfermata": (_("Short Fermata"), item.shortfermata),
                "05fermata": (_("Fermata"), item.fermata),
                "06longfermata": (_("Long Fermata"), item.longfermata),
                "07verylongfermata": (_("Very long Fermata"), item.verylongfermata),
                }

        velocities = {
                "00main":(_("Main Stress Velocity\nRecommended: 1:"), item.velocityMetricalMain),
                "01secondary":(_("Semi-stressed\nExample:0.95"), item.velocityMetricalSecondary),
                "02else":(_("Non-stressed\nExample:0.85"), item.velocityMetricalNothing),
                "03velocityHumanizing": (_("Velocity Humanizing\nSmaller value means more random velocity substraction. "), item.velocityHumanizing),
                }

        swing = {
                "01swingMod":(_("Swing Modificator\n0 to deactivate."), item.swingMod),
                "02swingLayer":(_("Rhythm Layer for Swing"), [item.swingLayer, (1536*4, _("1 Longa")),(1536*2, _("1 Whole / Semibreve")),(1536, _("1 Whole / Semibreve")), (768, _("2 Half / Minim")), (384, _("4 Quarter / Crotchet")), (192, _("8 Eigth / Quaver")), (96, _("16 Sixteenth / Semiquaver")), (48, _("32 Thirthy-Second / Demisemiquaver")), (24, _("64 Sixty-Fourth / Hemidemisemiquaver")), (12, _("128 Hundred Twenty-Eighth / Semihemidemisemiquaver ")), (6, _("256 Twohundred Fifty-Sixth / Demisemihemidemisemiquaver"))]),
                }

        dynamics = _dict2dynformDict(deepcopy(item.dynamics)) #replace each dict in the copied data with a transformed gui dict. But leave original data intact so we can later change it.
        nonLegatoScalingByDuration = _dict2dynformDict(deepcopy(item.nonLegatoScalingByDuration), extraTags = {12288 : "Maxima", 6144 : "Longa", 3072 : "Breve", 1536 : "Whole", 768 : "Half", 384 : "Quarter", 192 : "Eighth", 96 : "Sixteenth", 48 : "1/32", 24 : "1/64", 12 : "1/128", 6 : "1/256"})


        listoflists = [
            #Keep that list in this order! We use the indeces in the result processing.
            [data1, "Performance", _("These values influence the midi generation.")],
            [dynamics, "Dynamics", _("What midi values (0-127) should be used for dynamics?")],
            [velocities, "Velocities", _("Between a factor 0 and 1, how loud are different metrical positions. Use values near 1 for a realistic effect.")],
            [nonLegatoScalingByDuration, _("Non Legato Scalings"), _("These values are only used if the main Non Legato/Detache value is set to 0.\nChoose how long each base duration will be scalen.\nSmaller value means shorter duration.")],
            [swing, "Swing", _("How much swinging do you want?\nValues must be greater than -3 and smaller than 3 (not included)\n\n0 means binary rhythm (Default)\n1 is strict swing 2/3 + 1/3\n-1 is inverse swing. 1/2 + 2/3\nRecommended: 0.75 to 1.25 (or negative, for inverse swinging) ")],
            ]
        result = l_form.generateForm(listoflists, _("Edit Performance Signature")) #returns a list of dicts

        if result: #pressed cancel?
            saveState(item, l_item.update, durationChanged = False)
            #Most of these values must be forced to be a float value. Otherwise the formgen will put ints in there and you never get floats again via the gui.
            item.dynamics = result[1] #the second returned dict is just in the right format. We can use it to overwrite the original data.
            for k,v in result[3].items(): #convert ints to floats again
                result[3][k] = float(v)
            item.nonLegatoScalingByDuration = result[3] #the second returned dict is just in the right format. We can use it to overwrite the original data.
            item.name = result[0]["00name"]
            item.staccato = float(result[0]["00staccato"])
            item.tenuto = float(result[0]["00z-tenuto"])
            item.nonLegatoScaling = float(result[0]["01nonLegatoScaling"])
            item.legatoScaling = float(result[0]["01legatoScaling"])
            item.slurEndScaling = float(result[0]["01slurEndScaling"])
            item.nonLegatoHumanizing = float(result[0]["02nonLegatoHumanizing"])
            item.shortfermata = float(result[0]["04shortfermata"])
            item.fermata = float(result[0]["05fermata"])
            item.longfermata = float(result[0]["06longfermata"])
            item.verylongfermata = float(result[0]["07verylongfermata"])

            item.velocityMetricalMain = float(result[2]["00main"])
            item.velocityMetricalSecondary = float(result[2]["01secondary"])
            item.velocityMetricalNothing = float(result[2]["02else"])
            item.velocityHumanizing = float(result[2]["03velocityHumanizing"])

            item.swingMod = float(result[4]["01swingMod"])
            item.swingLayer = int(result[4]["02swingLayer"])
            l_send(lambda: l_item.update(item))


def editInstrumentChange():
    item = _getCursorItem()
    if type(item) is items.InstrumentChange:
        data0 = {
                "00a" : (None, _("Short Instrument is used for print-out.")),
                "00longInstrumentName": (_("Long Instrument Name"), item.longInstrumentName),
                "01shortInstrumentName": (_("Short Instrument Name"), item.shortInstrumentName),
                }
        data1 = {
                "02a" : (None, _("\nAll Midi values are 1-128")), #+1 for user display. 0-127 is original
                "02smfPatch":(_("Midi Instrument"), [item.smfPatch] + playback.instrumentList),
                "03smfVolume":(_("Midi Volume"), item.smfVolume + 1),
                "03z" : (None, _("\nAdvanced:")),
                "04smfBank":(_("Midi Bank"), item.smfBank + 1),
                }
        data2 = {
                "02a" : (None, _("\nAll values are 1-128")), #+1 for user display. 0-127 is original
                "02jackPatch":(_("Patch"), item.jackPatch +1),
                "03jackVolume":(_("Volume"), item.jackVolume + 1),
                "04jackBank":(_("Bank"), item.jackBank + 1),
                }
        listoflists = [
            [data0, "Meta-Data", _("Internal names as well as PDF and Lilypond output")],
            [data1, "Internal Sound", _("These values influence the internal sound output.")],
            [data2, "JACK Sound", _("These values influence the JACK sound output.")],
            ]
        result = l_form.generateForm(listoflists, _("Edit Instrument Change")) #returns a list of dicts

        if result: #pressed cancel? no:
            saveState(item, l_item.update, durationChanged = False)
            item.longInstrumentName = result[0]["00longInstrumentName"]
            item.shortInstrumentName = result[0]["01shortInstrumentName"]
            item.smfPatch = playback.instrumentList.index(result[1]["02smfPatch"])
            item.smfVolume = result[1]["03smfVolume"] -1 #user values are one too much for me! :(
            item.smfBank = result[1]["04smfBank"] -1
            item.jackPatch = result[2]["02jackPatch"] -1
            item.jackVolume = result[2]["03jackVolume"] -1
            item.jackBank = result[2]["04jackBank"] -1
            l_send(lambda: l_item.update(item))


def editMarkup():
    """The edit variants are gui only. Any script can directly access .lilystring and .position = ^ or _"""
    cursorItem = _getCursorItem()
    ret = l_form.generateForm({"lilystring":(_("Text"), str(cursorItem.lilystring)), "position":(_("Position"), [cursorItem.position, ("^", _("above")), ("_", "under")])}, _("Free Text"), _("Enter any text and choose above or under the track.\n\nYou can use Lilypond markup syntax"))
    if ret: #don't accept empty strings. This item can easily be deleted.
        saveState(cursorItem, l_item.update, durationChanged = False)
        i =_getCursorItem()
        i.lilystring = ret["lilystring"]
        i.position = ret["position"]
        l_send(lambda: l_item.update(i))

def editTempoSignature():
    cursorItem = _getCursorItem()

    #there are only two possibilities for standard tempo sigs. Either the tempo is dotted or not.
    if cursorItem.referenceTicks in (1536*4, 1536*2, 1536, 768, 384, 192, 96, 48, 24, 12, 6):
        realReferenceTicks = int(cursorItem.referenceTicks)
        dotted = False
    else:
        realReferenceTicks = int(cursorItem.referenceTicks / 1.5)
        dotted = True

    data = {"01bpm": (_("Beats per Minute"), cursorItem.beatsPerMinute),
        "02reference": (_("Reference Note"), [realReferenceTicks, (1536*4, _("1 Longa")),(1536*2, _("1 Whole / Semibreve")),(1536, _("1 Whole / Semibreve")), (768, _("2 Half / Minim")), (384, _("4 Quarter / Crotchet")), (192, _("8 Eigth / Quaver")), (96, _("16 Sixteenth / Semiquaver")), (48, _("32 Thirthy-Second / Demisemiquaver")), (24, _("64 Sixty-Fourth / Hemidemisemiquaver")), (12, _("128 Hundred Twenty-Eighth / Semihemidemisemiquaver ")), (6, _("256 Twohundred Fifty-Sixth / Demisemihemidemisemiquaver"))]),
        "02z" : (None, _("Optional:")),
        "03dotted": (_("Dotted"), dotted),
        "04tempostring": (_("Tempo string"), cursorItem.tempostring),
        }
    ret = l_form.generateForm(data, _("Tempo Signature"), _("Choose how many (bpm) of which note (reference) per minute. Optional string for printing."))
    if ret:
        cursorItem.beatsPerMinute = ret["01bpm"]
        cursorItem.referenceTicks = ret["02reference"]
        if ret["03dotted"]:
            cursorItem.referenceTicks = int(cursorItem.referenceTicks * 1.5)
        cursorItem.tempostring = ret["04tempostring"]
        l_send(lambda: l_item.updateStatic(cursorItem))
    else:
        return False

def editRepeat():
    """Access to the number of repeats"""
    ret = l_form.generateForm({"repeat":(" ", _getCursorItem().repeat)}, _("Edit Repeat"), _("Number of Repeats.\nThe first one is not included. Default is 1."))
    if ret:
        i = _getCursorItem()
        i.repeat = ret["repeat"]
        l_send(lambda: l_item.updateStatic(i))

def editChannelChangeRelative():
    ret = l_form.generateForm({"channel":(" ", _getCursorItem().number)}, _("Edit Relative Channel Change"), _("Give a number, negative or positve,\n which gets added to the midi channel for notes from here on."))
    if ret or ret == 0:
        i = _getCursorItem()
        i.number = ret["channel"]
        l_send(lambda: l_item.updateStatic(i))
    else:
        return False

def editChannelChangeAbsolute():
    ret = l_form.generateForm({"channel":(" ", _getCursorItem().number + 1 )}, _("Edit Absolute Channel Change"), _("Give a number 1-16,\n which is the new midi channel for notes from here on."))
    if ret or ret == 0:
        i = _getCursorItem()
        i.number = ret["channel"] - 1
        l_send(lambda: l_item.updateStatic(i))
    else:
        return False

def editProgramChangeRelative():
    ret = l_form.generateForm({"program":(" ", _getCursorItem().number)}, _("Edit Relative Program Change"), _("Give a number, negative or positve,\n which gets added to the midi program for notes from here on."))
    if ret or ret == 0:
        i = _getCursorItem()
        i.number = ret["program"]
        l_send(lambda: l_item.updateStatic(i))
    else:
        return False

def editProgramChangeAbsolute():
    ret = l_form.generateForm({"program":(" ", _getCursorItem().number + 1 )}, _("Edit Absolute Program Change"), _("Give a number 1-128,\n which is the new midi program for notes from here on."))
    if ret or ret == 0:
        i = _getCursorItem()
        i.number = ret["program"] - 1
        l_send(lambda: l_item.updateStatic(i))
    else:
        return False

def editTempoModification():
    ret = l_form.generateForm({"mod":(" ", _getCursorItem().bpmModificator )}, _("Edit Tempo Modification"), _("Give a positive or negative number to change the bpm relative to the current tempo signature.\n0 is 'A Tempo' and resets any changes."))
    if ret or ret == 0:
        i = _getCursorItem()
        i.bpmModificator = ret["mod"]
        l_send(lambda: l_item.updateStatic(i))
    else:
        return False


def editAlternateEnd():
    """Access to the alternate endings."""
    ret = l_form.generateForm({"endings":(" ", " ".join([str(x) for x in _getCursorItem().endings]))}, _("Edit Alternate End"), _("Edit alternate endings. Seperate with space."))
    if ret:
        endings = [int(x) for x in ret["endings"].split()]
        i = _getCursorItem()
        i.endings = endings
        l_send(lambda: l_item.updateStatic(i))

def editDaCapoDalSegno():
    ret = l_form.generateForm({"disableRepeat":(_("Disable Repeat"), _getCursorItem().disableRepeat),
                                "until":(_("Play until"), [_getCursorItem().until, (None, _("End")), ("Fine", "Fine"), ("Coda", "Coda")])},
                                _("Goto marking"), _("Choose if you want to disable repeat during playback after this jump.\nAlso if you want to stop playback before the end."))
    if ret: #don't accept empty strings. This item can easily be deleted.
        i =_getCursorItem()
        i.disableRepeat = ret["disableRepeat"]
        i.until = ret["until"]
        l_send(lambda: l_item.update(i))

def editDynamicSignature():
    ret = l_form.generateForm({"dyn":(" ", str(_getCursorItem()._expression) )}, _("Edit Dynamic Signature"), _("Give either a supported string like 'p' or 'f' (see Performance Signature)\nor directly a number (integer) between 0 and 127 (silence and max dynamic)"))
    if ret: #even the 0 is a string and soooo True.
        i = _getCursorItem()
        try: #see if it is a number
            i._expression = int(ret["dyn"])
        except: #it must be a string. If it is false the PerfSig and playback export will tell.
            i._expression = ret["dyn"]
        l_send(lambda: l_item.updateStatic(i)) #ironically the dynamic signature is update by the static function.
    else:
        return False


def chordSymbolMassInsert():
    """Gui only"""
    while chordSymbol():
        if not right(): #right is executed in any case except user cancelled chordSymbol
            break #end of track
        #while not (type(_getCursorItem()) is items.Rest or type(_getCursorItem()) is items.Chord):
        while not (isinstance(_getCursorItem(), items.Chord)):
            if not right():
                return False

def figuredBassMassInsert():
    """Gui only"""
    while figuredBass():
        if not right(): #right is executed in any case except user cancelled the gui
            break #end of track
        while not type(_getCursorItem()) is items.Chord:
            if not right():
                return False


#Commands:
##########

def nothing():
    """Do nothing"""
    pass

def save(filepath):
    ret = saveload.save(_getWorkspace(), filepath)
    if ret:
        _getWorkspace().lastKnownFilename = filepath
        setCompoundTake("save") #a fake signal. Undo knows how to deal with it. We use this to determine the "clean" state of a file
        return filepath
    else:
        return False

def saveAll():
    """save all open and named files. Discard all unnamed/unfiled data.
    Only use for quit."""  #TODO: Why only for quit?
    originalActive = _getSession().workspace
    returns = []
    for space in _getSession().workspaces:
        _getSession().workspace = space #change active workspace.
        if space.lastKnownFilename:
            returns.append(save(space.lastKnownFilename))
    _getSession().workspace = originalActive
    return returns and all(returns) #all with empty list returns True

def exportMidi(filepath, workspace = None, jackMode = False, parts=None):
    """Filepath can also be a directory. in fact this is expected for
    collections
    Parts is just a string, not the core.parts tuplet. We build the
    tuplet here.
    parts is a string, track property, like "group" or
    "longInstrumentName". You get several exports if you use parts,
    one for each different keyword in all tracks.

    When exporting collectiongs this will create multiple files.
    One for each score in the collection.
    When exporting parts this will export one file for each part-value.

    When exporting a collection with parts it will convert files with
    the name:
    givenFileName-score-part.mid"""

    if not workspace:
        workspace = _getWorkspace()

    directory = os.path.dirname(filepath)
    if not os.path.exists(directory):
        os.makedirs(directory)

    class ReturnObject:
        def __init__(self):
            self.cachedMaxticks = None

    if type(workspace) is core.Workspace: #lbjs
        if parts: # This is used for Ardour export with "_uniqueContainerName", formerly known as "export midi tracks"
            returnPaths = []
            returnDataList = [] #a list of tuplets. [0] is the smf structure [1] the unique track name and [2] the jack midi port as string "client:port", [3]jack audio return port left, [4]jack audio return port right, [5] filename with dirs, [6] filename without dirs,
            returnObject = ReturnObject() #returnObject is a special return object maneuver return data around the return statement. It is only used in rare cases  so that it would not be justified to return a couple or multiple values
            a =  os.path.splitext(filepath)
            for p in workspace.score.getTrackPropertyGroups(parts): #create a list of all groups
                filepath = a[0] + "-" + p + a[1]  #append the parts-value as last thing before the extension. This will even be compatible with collection export, which also generates multiple files.
                smfData = workspace.score.exportPlayback(jackMode = jackMode, parts = (parts, p), returnObject=returnObject) #core.parts is a tuple (trackProperty, value). Only those who match  will be exported.
                if smfData: #it could be an empty file. Just empty tracks or just a master track alone.
                    smfData.save(filepath)
                    print ("Exported midi part:", filepath)
                    track = workspace.score[0] #this and the returnDataList only make sense for trackExport (aka. Ardour export). It IS possible to have more than one track in part export at once, so this is only usefull if it is guaranteed to have just one track per part. Which is only with uniquecontainername.
                    returnDataList.append((smfData, track.uniqueContainerName, filepath, os.path.basename(filepath), returnObject.cachedMaxticks ))
            return returnDataList
        else:  #This is the simple and plain "all in one file" export. With jack or without.
            data = workspace.score.exportPlayback(jackMode = jackMode)
            if data: #it could be an empty file. Just empty tracks or just a master track alone.
                data.save(filepath)
                print ("Exported midi:", filepath)
            return filepath

    elif type(workspace) is core.Collection: #lbj
        returnPaths = []
        #basename = os.path.basename(workspace.lastKnownFilename) #Collections must have a filename. So we can depend on that. No, too static. We want the user to pick the base name.
        basename = os.path.basename(filepath)
        onlyfile = os.path.splitext(basename)[0]
        wspList = workspace.workspaceList
        l = len(str(len(wspList) + 1))

        for i, wsp in enumerate(wspList):
            num = str(i+1).zfill(l)
            piece = "_" + wsp.score.header.data["piece"] if wsp.score.header.data["piece"] else ""
            path = os.path.join(directory, onlyfile + "-" + num + piece + ".mid")
            if parts: #One group of part-midi-files for each score in the lbj file. OverallTitle-Score-Part  Example:  symphony3-allegro-strings.mid, symphony3-allegro-brass.mid, symphony3-menuett-strings.mid, symphony3-menuett-brass.mid,
                a =  os.path.splitext(path)
                for p in wsp.score.getTrackPropertyGroups(parts): #create a list of all groups
                    filepath = a[0] + "-" + p + a[1]  #append the parts-value as last thing before the extension. This will even be compatible with collection export, which also generates multiple files.
                    data = wsp.score.exportPlayback(jackMode = jackMode, parts = (parts, p))
                    if data: #it could be an empty file. Just empty tracks or just a master track alone.
                        data.save(filepath) #core.parts is a tuple (trackProperty, value). Only those who match  will be exported.
                        returnPaths.append(filepath)
                        print ("Exported Midi Parts:", returnPaths)
            else: #One midi file for each score in the lbj file. Simpler than parts-export. OverallTitle-Score. Example: symphony3-allegro.mid, symphony3-menuett.mid
                data = wsp.score.exportPlayback(jackMode = jackMode)
                if data: #it could be an empty file. Just empty tracks or just a master track alone.
                    returnPaths.append(path)
                    data.save(path)
                    print ("Exported Midi:", returnPaths)
        return returnPaths
    else:
        raise ValueError("Parameter workspace must be core.Workspace or core.Collection but it was:", workspace)

def getTemplateByName(templateFileNameWithExtension, workspace = None):
    """Returns tuple:
    (template string, TEMPLATE_ROOT, USER_TEMPLATE_ROOT)
    Decide which path a template name has.
    User dir, system-dir, lbj-container etc.
    Exports like Ardour and Lilypond expect the template as string.

    Laborejo has thre path positions where it looks for files.
    Priorities: 3>2>1
    1. the install/running directory of lilypond.py + /lilypondtemplates
    2. ~/.laborejo/lilypondtemplates
    3. in the dir of a saved file or in the lbj package, if existent."""


    configdir = _getSession().configdir

    ROOT = os.path.dirname(__file__)
    TEMPLATE_ROOT = os.path.join(ROOT, 'templates')
    USER_TEMPLATE_ROOT = os.path.join(configdir, 'templates')
    TEMPLATE = os.path.join(TEMPLATE_ROOT, templateFileNameWithExtension)
    USER_TEMPLATE = os.path.join(USER_TEMPLATE_ROOT, templateFileNameWithExtension)

    if workspace and type(workspace) is core.Collection and workspace.lastKnownFilename:
        COLLECTION_TEMPLATE_ROOT = os.path.dirname(workspace.lastKnownFilename)
        COLLECTION_TEMPLATE = os.path.join(COLLECTION_TEMPLATE_ROOT, templateFileNameWithExtension)
    else:
        COLLECTION_TEMPLATE_ROOT = ""
        COLLECTION_TEMPLATE = ""


    if not os.path.isdir(USER_TEMPLATE_ROOT):
        warnings.warn(_("User Templates dir not found. Creating empty ") + USER_TEMPLATE_ROOT + _(" with README file."))
        os.makedirs(USER_TEMPLATE_ROOT)
        #Create small readme
        f2 = open(os.path.join(USER_TEMPLATE_ROOT, 'README'), "w")
        f2.write(_("Place template files here.\nSee Laborejo <systemdir>/laborejocore/templates/ for examples.\nTemplates in this user directory with the same name override the system ones.\n"))
        f2.close()

    (COLLECTION_TEMPLATE, "|", USER_TEMPLATE, "|",  TEMPLATE)
    if os.path.isfile(COLLECTION_TEMPLATE): # .lbj-file-root templates
        completeTemplatePath = os.path.abspath(COLLECTION_TEMPLATE)
    elif os.path.isfile(USER_TEMPLATE): # ~/.laborejo/templates
        completeTemplatePath = os.path.abspath(USER_TEMPLATE)
    elif os.path.isfile(TEMPLATE):
        completeTemplatePath = os.path.abspath(TEMPLATE)
    else:
        #warnings.warn("Given template " + templateFileNameWithExtension + " not found in any Laborejo path (system-dir, home dir or lbj package). Using fallback template.")
        return (None, None, None, None)

    with open(completeTemplatePath, 'r') as f:
        read_data = f.read()
    f.close()
    return (read_data, TEMPLATE_ROOT, USER_TEMPLATE_ROOT, COLLECTION_TEMPLATE_ROOT)

def getTemplateList(workspace = None, extension = "ly"):
    """Return a list of all template filenames, without path.
    extension is a file extension like ly or ardour."""
    if not workspace:
        workspace = _getWorkspace()
        if not workspace:
            raise ValueError("No current workspace. Are you trying to work with collections? You must specify a workspace parameter then!")
    name = {"ly" : workspace.lytemplate, "ardour" : workspace.ardourtemplate,}[extension]
    templateString, TEMPLATE_ROOT, USER_TEMPLATE_ROOT, COLLECTION_TEMPLATE_ROOT = getTemplateByName(name, workspace = workspace)

    #First case: The file is not in any of the search paths. Use basic.ly or empty.ardour instead
    if not (templateString and TEMPLATE_ROOT and USER_TEMPLATE_ROOT and COLLECTION_TEMPLATE_ROOT):
        name = {"ly" : "basic.ly", "ardour" : "empty.ardour",}[extension]
        templateString, TEMPLATE_ROOT, USER_TEMPLATE_ROOT, COLLECTION_TEMPLATE_ROOT = getTemplateByName(name, workspace = workspace)

    if extension == "ly":
        return lilypond.getIncludes(TEMPLATE_ROOT, USER_TEMPLATE_ROOT, COLLECTION_TEMPLATE_ROOT)[0] #includes and templates
    elif extension == "ardour":
        return ardour.getIncludes(TEMPLATE_ROOT, USER_TEMPLATE_ROOT, COLLECTION_TEMPLATE_ROOT) #just a plain set for the templates. no includes in ardour
    else:
        raise ValueError ("Extension unknown:", extension)


def exportArdourThree(directory, templateName = None, workspace = None, parts=None): #parts is just for compatible resons with other export functions. It is not used.
    if type(workspace) is core.Workspace or workspace is None: #lbjs
        if not workspace:
            workspace = _getWorkspace()
        if not templateName:
            templateName = workspace.ardourtemplate
        return _exportArdourThree(directory, templateName, workspace)

    elif type(workspace) is core.Collection: #lbj
        """Create one directory for each (merged) Score"""
        if not templateName:
            templateName = workspace.ardourtemplate
        basename = os.path.basename(workspace.lastKnownFilename) #Collections must have a filename. So we can depend on that
        onlyfile = os.path.splitext(basename)[0]
        wspList = workspace.workspaceList
        l = len(str(len(wspList) + 1))
        returnPaths = []
        for i, wsp in enumerate(wspList):
            num = str(i+1).zfill(l)
            piece = "_" + wsp.score.header.data["piece"] if wsp.score.header.data["piece"] else ""
            #path = os.path.join(directory, onlyfile + "-" + num + piece)
            path = os.path.join(directory)
            r = _exportArdourThree(path, templateName, wsp, projectName = onlyfile + "-" + num + piece) #does the saving
            returnPaths.append(r)
        return returnPaths
    else:
        raise ValueError("Parameter workspace must be core.Workspace or core.Collection but it was:", workspace)


def _exportArdourThree(directory, templateName, workspace, projectName = None):
    templateData = getTemplateByName(templateName, workspace = workspace) #(read_data, TEMPLATE_ROOT, USER_TEMPLATE_ROOT, COLLECTION_TEMPLATE_ROOT)

    if not projectName:
        if workspace.lastKnownFilename:
            projectName = os.path.splitext(os.path.basename(workspace.lastKnownFilename))[0]
        else:
            projectName = "unnamed"

    ardourPath = os.path.join(directory, projectName)
    #dataList = exportMidiAsTracks(diretory = os.path.join(ardourPath, "interchange", projectName, "midifiles"), workspace = workspace, jackMode = True)  #We can't save in the directory itself. Ardour sessions actually look up their directory name and it has to be the same as the one in /interchange
    dataList = exportMidi(filepath = os.path.join(ardourPath, "interchange", projectName, "midifiles", "labo.mid"), workspace = workspace, jackMode = True, parts="uniqueContainerName")  #We can't save in the directory itself. Ardour sessions actually look up their directory name and it has to be the same as the one in /interchange

    unfoldedScore = workspace.score.unfoldScore()
    tempoMap = ardour.generateTempoMap(unfoldedScore, items.TempoSignature, items.TimeSignature)
    ardour.save(ardourPath, dataList, projectName, tempoMap, templateData )
    return os.path.join(ardourPath, "laborejo.ardour")

def generateLilypond(workspace = None, template = None, parts = None):
    """Generate only the text and return that as string
    parts is a tuple (trackProperty, value). Only those who match
    will be exported. e.g ("group", "strings") to export only strings.
    """
    if not workspace:
        workspace = _getWorkspace()
    if not template:
        template = getTemplateByName(workspace.lytemplate, workspace = workspace)
    return lilypond.create(template, workspace, parts = parts)

def exportLilypond(filepath, workspace = None, template = None, parts = None):
    """template is just a filename, not a path
    parts is a string, track property, like "group" or
    "longInstrumentName". You get several exports if you use parts,
    one for each different keyword in all tracks.
    """
    if not workspace:
        workspace = _getWorkspace()
    if not template:
        template = getTemplateByName(workspace.lytemplate, workspace = workspace)

    if parts:
        a =  os.path.splitext(filepath)
        for p in workspace.score.getTrackPropertyGroups(parts):
            finalfilepath = a[0] + "-" + p + a[1]
            lilypond.save(template, workspace, filepath = finalfilepath, parts = (parts, p))
            print ("Exported Lilypond:",  finalfilepath)
    else:
        lilypond.save(template, workspace, filepath, parts = None)
        print ("Exported Lilypond:",  filepath)


def previewPDF(workspace = None, template = None, lilypondbinary = None, pdfviewer = None, parts = None):
    """template is just a filename, not a path
    parts is a string, track property, like "group" or
    "longInstrumentName". You get several exports if you use parts,
    one for each different keyword in all tracks.
    """
    if not workspace:
        workspace = _getWorkspace()
    if not type(workspace) in [core.Workspace, core.Collection]:
        raise ValueError("Parameter workspace must be core.Workspace or core.Collection but it was:", workspace)
    if not template:
        template = getTemplateByName(workspace.lytemplate, workspace = workspace)

    if parts:
        for p in workspace.score.getTrackPropertyGroups(parts):
            print (workspace.score.getTrackPropertyGroups(parts))
            lilypond.previewPDF(template, workspace, lilypondbinary, pdfviewer, parts = (parts, p))
    else:
        lilypond.previewPDF(template, workspace, lilypondbinary, pdfviewer, parts = None)

def previewPDFSingle(workspace = None, template = None, lilypondbinary = None, pdfviewer = None):
    if _getTrack().exportExtractPart:
        parts = "exportExtractPart"
        p = _getTrack().exportExtractPart
    else:
        parts = "uniqueContainerName"
        p = _getTrack().uniqueContainerName

    if not workspace:
        workspace = _getWorkspace()
    if not type(workspace) in [core.Workspace, core.Collection]:
        raise ValueError("Parameter workspace must be core.Workspace or core.Collection but it was:", workspace)
    if not template:
        template = getTemplateByName(workspace.lytemplate, workspace = workspace)

    lilypond.previewPDF(template, workspace, lilypondbinary, pdfviewer, parts = (parts, p))

def exportPDF(filepath, workspace = None, template = None, lilypondbinary = None, parts = None):
    """template is just a filename, not a path
    parts is a string, track property, like "group" or
    "longInstrumentName". You get several exports if you use parts,
    one for each different keyword in all tracks.
    """
    if not workspace:
        workspace = _getWorkspace()
    if not type(workspace) in [core.Workspace, core.Collection]:
        raise ValueError("Parameter workspace must be core.Workspace or core.Collection but it was:", workspace)
    if not template:
        template = getTemplateByName(workspace.lytemplate, workspace = workspace)

    if parts:
        a =  os.path.splitext(filepath)
        for p in workspace.score.getTrackPropertyGroups(parts):
            filepath = a[0] + "-" + p + a[1]
            lilypond.exportPDF(template, workspace, filepath, lilypondbinary, parts = (parts, p))
    else:
        return lilypond.exportPDF(template, workspace, filepath, lilypondbinary, parts = None)

def exportLilyBin(lilybinId = None, workspace = None, template = None, parts = None, filepath = None): #filepath is only for compatibility
    """Upload the generated lilypond code/text
    to http://www.lilybin.com.
    Does not want a filename like the others but an optional lilybinId
    to reuse an existing session there.

    Returns a tuple with (urlString, id) where id is the lilybin id
    to reuse later."""
    if not workspace:
        workspace = _getWorkspace()
    if not type(workspace) in [core.Workspace, core.Collection]:
        raise ValueError("Parameter workspace must be core.Workspace or core.Collection but it was:", workspace)
    if not template:
        template = getTemplateByName(workspace.lytemplate, workspace = workspace)

    if parts:
        urlList = []
        for p in workspace.score.getTrackPropertyGroups(parts):
            createdUrlAndId = lilypond.exportLilyBin(template, workspace, lilybinId = None, parts = (parts, p)) #don't use the same id, even if given.
            urlList.append(createdUrlAndId[0])
            print (createdUrlAndId[0])
        return urlList
    else:
        createdUrlAndId = lilypond.exportLilyBin(template, workspace, lilybinId, parts = parts)
        print ("Exported to LilyBin.com:", createdUrlAndId[0])
        message = "".join(("<a href='",createdUrlAndId[0], "'>", createdUrlAndId[0], "</a>"))
        l_form.generateForm({"url":("URL", createdUrlAndId[0])}, _("Lilypbin URL"), comment = message)
        return createdUrlAndId

def new(filepath = None):
    stop() #stop playback.
    retWorkspace = core.session.new()
    retWorkspace.score.addTrack() #The initial track that any empty score needs. In opposite to load() which creates all tracks on its own.
    if filepath:
        retWorkspace.lastKnownFilename = filepath
    l_send(lambda: l_global.workspaceChanged(_getWorkspace()))
    l_send(lambda: l_global.new(_getWorkspace()))
    setCompoundTake("save") #a fake signal. Undo knows how to deal with it. We use this to determine the "clean" state of a file
    return retWorkspace

def load(filepath, force = False, asTemplate = False, autoconnect = True):
    """there are no checks for duplicate filepaths.
    See core.session.load.
    Returns a core.workspace or False if filepath was not
    an lbjs file.
    Force loads old lbjs files without a fileformat marker."""
    stop() #stop playback.
    jsonConvert = saveload.load(filepath, force, asTemplate)
    if jsonConvert:
        retWorkspace = core.session.load(jsonConvert)
        retWorkspace.lastKnownFilename = filepath #_getWorkspace() is already the loaded score.
        l_send(lambda: l_global.workspaceChanged(_getWorkspace()))
        setCompoundTake("save") #a fake signal. Undo knows how to deal with it. We use this to determine the "clean" state of a file
        return retWorkspace
    else:
        return False

def close():
    stop() #stop playback.

    ret = _getSession().close()
    if ret:  #is there a file left open
        l_send(lambda: l_global.workspaceChanged(_getWorkspace()))
    if ret is False:
        return False
    else:
        return True

def newCollection(filepath = None):
    retCollection = core.session.newCollection()
    if filepath:
        retCollection.lastKnownFilename = filepath
    l_send(lambda: l_collections.load(retCollection))
    return retCollection


def loadCollection(filepath, loadIntoSession = True):
    """Parse a lbj meta file and load every lbjs file in there.
    This is very simple load, no interaction with the saveload module."""
    col = core.Collection()
    col.lastKnownFilename = filepath

    g = open(filepath, mode="r", encoding="utf-8")
    col.originalText = g.read()
    g.close()

    col.deserialise(col.originalText)
    if loadIntoSession:
        core.session.loadCollection(col)
    #else this is just a temporary Collection, most likely for loadCollectionScores.

    l_send(lambda: l_collections.load(col))
    return col

def saveCollection(filepath, collection):
    collection.lastKnownFilename = filepath
    f = open(filepath, 'w', encoding="utf-8")
    saveString = collection.serialise()
    f.write(saveString)
    return filepath

def saveAllCollection():
    """save all open and named files. Discard all unnamed/unfiled data.
    Only use for quit."""
    for col in _getSession().collections:
        if col.lastKnownFilename:
            saveCollection(col.lastKnownFilename)

def closeCollection(collection):
    """Contrary to score close this does not need to send a signal
    that the collection has changed so that a gui can react since there
    is no 'active collection'.  The gui can do whatever it wants, there
    is no danger of asynchronous editing when dealing with collections.
    """
    return _getSession().closeCollection(collection)

def col_newScore(collection, name):
    """Append a new, empty score"""
    collection.workspaceFileStrings.append([name,[]])
    l_send(lambda: l_collections.buildList(collection))

def col_addPart(collection, score, afterPart, newPartName):
    """Insert a a new part with the given name.
    If afterPart is None we insert at the beginning position
    newPartName can be a full path or relative path"""
    if not score in collection.workspaceFileStrings:
        raise ValueError ("Given Collection instance", collection, "and score", score, "do not match")

    if not collection.lastKnownFilename:
        msg = "collection.lastKnownFilename is empty. Save before using this convenient function or add the filenames manually to the python data until you save"
        warnings.warn(msg)
        return (False, msg)

    startPath = os.path.dirname(collection.lastKnownFilename)
    justRelativePath = os.path.relpath(newPartName, startPath)

    #list.insert first shifts all items including the one already on this position to the right and fills in the new item. standard text editor behaviour, the "cursor"/index is before an item.
    #even if we only have 0,1 position and we want to insert at index 2 python will make it work and append at the end.
    if afterPart:
        insertIndex = score[1].index(afterPart) + 1
    else:
        insertIndex = 0
    score[1].insert(insertIndex, justRelativePath)

    l_send(lambda: l_collections.buildList(collection))
    return (collection.workspaceFileStrings.index(score), insertIndex)  #the new index of the item as [scoreIndex, Partindex]


def col_delete(collection, score, part):
    """This one is a little more complicated. If a part should be
    deleted it is easy, but if a score should be deleted the rest of the
    parts shall be merged to the upper score. Subsequently you can't
    delete the first score (only rename it)

    If part is None we assume deleting a score directly.
    """
    if not score in collection.workspaceFileStrings:
        raise ValueError ("Given Collection instance", collection, "and score", score, "do not match")

    if part:
        oldidx = score[1].index(part)
        if oldidx == len(score[1])-1 and not oldidx == 0: #the last item but not the only/first
            last = True
        else:
            last = False
        score[1].remove(part)
        scoreindex = collection.workspaceFileStrings.index(score)
        newidx = len(score[1])-1 if last else oldidx
        l_send(lambda: l_collections.buildList(collection))
        return (scoreindex, newidx)  #the new index of the item as [scoreIndex, Partindex]
    else:
        idx = collection.workspaceFileStrings.index(score)
        if idx == 0:
            return False #first Item can't be deleted
        else:
            oldPartList = collection.workspaceFileStrings[idx][1]
            collection.workspaceFileStrings[idx-1][1] += oldPartList
            collection.workspaceFileStrings.pop(idx)
            l_send(lambda: l_collections.buildList(collection))
            return idx-1


def col_movePartUp(collection, score, part):
    if not score in collection.workspaceFileStrings:
        raise ValueError ("Given Collection instance", collection, "and score", score, "do not match")

    #Move the item left in the list of parts
    parts = score[1]
    oldidx = parts.index(part)
    newidx = oldidx - 1
    scoreindex = collection.workspaceFileStrings.index(score)

    if newidx >= 0 or collection.workspaceFileStrings.index(score) == 0: #not 0 or in the first score
        if newidx < 0: #the top position stays the same.
            newidx = 0
        parts.insert(newidx, parts.pop(oldidx))
    else: #break the boundaries, append the part to the score left of the current one
        partsNew = collection.workspaceFileStrings[scoreindex -1][1]
        newidx = len(partsNew)
        partsNew.insert(newidx, parts.pop(oldidx))
        scoreindex -= 1

    l_send(lambda: l_collections.buildList(collection))
    return (scoreindex, newidx)  #the new index of the item as [scoreIndex, Partindex]

def col_movePartDown(collection, score, part):
    #Move the item right in the list of parts
    if not score in collection.workspaceFileStrings:
        raise ValueError ("Given Collection instance", collection, "and score", score, "do not match")

    parts = score[1]
    oldidx = parts.index(part)
    newidx = oldidx + 1
    scoreindex = collection.workspaceFileStrings.index(score)

    if newidx < len(parts) or scoreindex == len(collection.workspaceFileStrings)-1 : #len counts from 1, index is from 0. not in the last score.
        if oldidx >= len(parts) -1: #this can still happen if score is the last one
            newidx = len(parts) -1
        else:
            parts.insert(newidx, parts.pop(oldidx))
    else: #break the boundaries, append the part to the score right of the current one
        partsNew = collection.workspaceFileStrings[scoreindex +1][1] #+1 is guaranteed to exist because we tested for it earlier in the first if line.
        newidx = 0 #first position.
        partsNew.insert(newidx, parts.pop(oldidx))
        scoreindex += 1

    l_send(lambda: l_collections.buildList(collection))
    return (scoreindex, newidx)  #the new index of the item as [scoreIndex, Partindex]


def col_moveScoreUp(collection, score):
    if not score in collection.workspaceFileStrings:
        raise ValueError ("Given Collection instance", collection, "and score", score, "do not match")

    #Move the item right in the list
    oldidx = collection.workspaceFileStrings.index(score)
    newidx = oldidx - 1 if oldidx > 0 else 0
    collection.workspaceFileStrings.insert(newidx, collection.workspaceFileStrings.pop(oldidx))
    l_send(lambda: l_collections.buildList(collection))
    return newidx  #the new index of the item as scoreIndex

def col_moveScoreDown(collection, score):
    if not score in collection.workspaceFileStrings:
        raise ValueError ("Given Collection instance", collection, "and score", score, "do not match")

    #Move the item left in the list
    oldidx = collection.workspaceFileStrings.index(score)
    newidx = oldidx + 1 if oldidx < len(collection.workspaceFileStrings)-1 else len(collection.workspaceFileStrings)-1  #len counts from 1, index is from 0
    collection.workspaceFileStrings.insert(newidx, collection.workspaceFileStrings.pop(oldidx))
    l_send(lambda: l_collections.buildList(collection))
    return newidx  #the new index of the item as scoreIndex


def col_getAllFilepaths(lbjFilePath):
    """Take a lbj collection file, load all
    of the references scores/lbjs"""
    col = loadCollection(lbjFilePath, loadIntoSession = False) #load the lbj, but don't include into the current session
    dirname =  os.path.dirname(col.lastKnownFilename)
    loaded = []
    for pieceString, compoundFilenameList in col.workspaceFileStrings:
        for onlyfile in compoundFilenameList:
            filepath = os.path.join(dirname, onlyfile)
            loaded.append(filepath)
             #loaded.append(load(filepath))  #and NOT loadMergeParts. We want each file as standalone file.
    return loaded


def copy(cliplink = "session"):
    if _getWorkspace().score.selection:  #at least one selected item?
        core.clipboard.copySelectedItemsToInternalClipboard(cliplink)
        return cliplink
    else:
        return False

def _pasteItem(item, standaloneInsert = False):
    """As _insert but creates a copy first and resets internal data
    because we assume we got an already existing item as parameter."""
    new = deepcopy(item)
    new.parentWorkspace = _getWorkspace()
    new.instanceCount = 0  #reset Instance Count to 0, which is the default in item.init().
    #One could think that the generation of a new name could be done during copy, but you can paste the same clipboard multiple times.
    new.newName() #generate a new internal unique id.
    _insert(new, standaloneInsert = standaloneInsert) #Don't trigger the gui redraw if standaloneInsert is False
    return new

def paste(cliplink = "session"):
    setCompoundTake("start")
    clip = core.clipboard.clipboard[cliplink]
    start_position = _getActiveCursor().tickindex
    returnTo = getCursorPosition()
    firstPositionPerTrack = set()
    for track in clip:
        first = True # a switch to gather the first inserted item in each track. Send to the gui afterwards.
        _getScore().toTickIndex(start_position)
        l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex())) #move the gui cursor to the insert position
        for item in track:
            new = _pasteItem(item) #paste on the cursor position
            if first:
                firstPositionPerTrack.add(getCursorFlatPosition())
                first = False
        if track == clip[0]: #after the first iteration save the cursor position. this is where we want to be after pasting.
            returnTo = getCursorFlatPosition()
        if not trackDown(): # try to go down to the next track.
            break  #last track was reached. Don't continue
    for firstPosition in firstPositionPerTrack:
        l_send(lambda: l_score.updateTrackFromIndex(firstPosition[0], firstPosition[1], )) #track index and flat cursor index
    cursorFlatPosition(returnTo)
    setCompoundTake("stop")
    deselectScore()

def pasteReplaceSelection(cliplink = "session", selectNewItems = False):
    """The goal is to remove one item and add one from the clipboard
    instead. You can specify a test, for example to only replace Chords
    and Rests and not a Clef
    """
    if cliplink in core.clipboard.clipboard and core.clipboard.clipboard[cliplink] and  _getScore().selection:  #there is a clipboard and at least one selected item?
        gen = core.clipboard.generatorClipboard(cliplink)

        def test(item, cursor):
            if item.selected:
                return True
        replace(test, lambda i: _pasteItem(next(gen)), selectNewItems) #the i parameter is for replace.

def pasteAsLink(cliplink = "session"):
    if core.clipboard.lastScore is _getScore(): #no links from one score to the other
        setCompoundTake("start")
        clip = core.clipboard.linkboard[cliplink]
        start_position = _getActiveCursor().tickindex
        returnTo = getCursorPosition()
        firstItemsPerTrack = set()
        for track in clip:
            first = True # a switch to gather the first inserted item in each track. Send to the gui afterwards.
            _getScore().toTickIndex(start_position)
            for item in track:
                _insert(item, standaloneInsert = False)
                if first:
                    firstItemsPerTrack.add(item)
                    first = False
            if track == clip[0]: #after the first iteration save the cursor position. this is where we want to be after pasting.
                returnTo = getCursorPosition()
            if not trackDown(): # try to go down to the next track.
                break  #last track was reached. Don't continue
        cursorPosition(returnTo)
        for firstItem in firstItemsPerTrack:
            l_send(lambda: l_score.updateTrackFromItem(firstItem))
        cursorFlatPosition(returnTo)
        setCompoundTake("stop")
    else: #different score
        paste()


def pasteAsStreamWithoutTrackBreaks(cliplink = "session"):
    setCompoundTake("start")
    clip = core.clipboard.clipboard[cliplink]
    first = True
    firstItem = None
    for track in clip:
        for item in track:
            new = _pasteItem(item)
            if first:
                firstItem = new
                first = False
    l_send(lambda: l_score.updateTrackFromItem(firstItem))
    setCompoundTake("stop")
    deselectScore()

def cut():
    if _getWorkspace().score.selection:  #at least one selected item?
        copy()
        delete()

def applyToSelection(command, additional_test = True, signal = None, durationChanged = False):
    """Works like manual command application. The cursor goes through
    the score and on each selected note the command is called.
    Thus all commands that work on single items will work on the
    selection.
    The most problematic case is if the duration changes or items get
    deleted. Then we can't return to the old tickindex.
    If the cursor position after applyToSelection is not on the same
    item as when we started we'll go to the last selected item.
    If there is no selected item left we go to 0,0

    The return value, if there was a selection, is a dict with
    trackIndexes and tickindex for the first selected item
    in each track. Everything left of these can be of no interest of
    whatever function called applyToSelection."""

    if _getScore().selection:  #at least one selected item?
        start = getCursorPosition() #Save the cursor position
        startItem = _getCursorItem() #Save the current item
        alreadyProcessedLinkedItems = set() #Remember all processed items which had instanceCount > 1. Process them the first time, but then never again.
        firstSelectedItemInTracks = [] #if a gui is ready to update by item only this will send the items.
        for cursor in _getScore().cursors:
            _getScore().activeCursor = cursor
            if _getTrack().selectedItems:
                head()
                first = True #a switch for the first selected item
                for i in cursor.iterate():
                    #l_send(l_cursor.right)  #match the gui cursor.
                    if i.selected and (not i in alreadyProcessedLinkedItems) and additional_test:
                        if first:
                            firstSelectedItemInTracks.append(i)
                            first = False
                        if i.instanceCount > 1 or _getContainer():
                            alreadyProcessedLinkedItems.add(i)
                        if signal: #None signal means no saveState of its own. See addOctave. It calls addChordnote which already registers an undo state and has no signal of its own.
                            saveState(i, signal, durationChanged)
                        command()
                        if signal and not type(signal) is str:
                            signal(i)
        head()
        #Now everything is processed and all cursors are resetted to 0.
        #According to the docstring now comes the serious part. Check if the current item is still the same from the beginning.

        cursorPosition(start) #try to reach the old item position. This has at least the effect that we go to the starting track once again. ApplyToSelection is not able to do anything with tracks (except someone writes faulty function)
        if not _getCursorItem() == startItem: #Positions are wrong now!
            def matchStart(item, cursor): #because lambda is so limited in python
                if item == startItem:
                    return firstSelectedItemInTracks
            head() #once again to the start. Nobody said modifying lists while iteration was easy.
            searchTrack(matchStart) #if this fails (item deleted or cut) the position does not get changed by the search and we stay at head.
        return firstSelectedItemInTracks
    else:
        return False


def replace(test, replacementCommand, selectNewItems = False):
    """Same as apply to selection, only for the whole score.
    Goes through each track with the original cursor.
    This can imitate user behaviour (which search() cannot) with all
    destructive behaviour and even cursor score within the loop.

    The test is applied to each item in the score, linked items
    every time as well.
    If it is true it will get deleted and then replacementCommand
    gets executed which is meant to create new items on the cursor
    position.

    If you don't like the 'delete' part don't use this command
    but select the whole Score and apply to selection.

    The test needs to accept two parameters:
    test(item, cursor).
    Item is the current item
    cursor is the current cursor state (active).

    replacementCommand gets one parameter, the deleted item.

    example:

    def test(item, cursor):
        if "<c'>" in item.exportLilypond():
            return True
    api.replace(test, lambda i: api._insertRest(i.duration*2))

    replaces any single c' in with a rest which has
    twice the duration of the original note.
    You could of course use regular expressions in a test like this.

    or in one line:
    replace(lambda i, c: "<c'>" in i.exportLilypond(), lambda i: _insertRest(i.duration*2))
    """
    setCompoundTake("start")
    start = getCursorFlatPosition()
    firstReplacedTickIndexInTracks = []
    for cursor in _getScore().cursors:
        _getScore().activeCursor = cursor
        head()
        first = True #a switch for the first selected item
        for i in cursor.iterate():
            if test(i, cursor):
                if first:
                    firstReplacedTickIndexInTracks.append((_getTrackIndex(), _getTickIndex()))
                    first = False
                l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
                l_send(lambda: l_item.deleteAtCursor(i, _getContainer()))
                cursor.delete() #TODO: this does not work with undo.
                try:
                    replacementCommand(i) #now the cursor is already on the next item. It would go right the next iteration.
                except StopIteration: #a generator function was used in the replaceCommand and now ran out of items.
                    pass #go on deleting.
                if selectNewItems:
                    selectLeft()
                else:
                    cursor.left() #we counter the line above with stepping back once.
    setCompoundTake("stop")
    head()
    #Now everything is processed and all cursors are resetted to 0.

    cursorFlatPosition(start)

    for trackindex, tickindex in firstReplacedTickIndexInTracks:
        l_send(lambda: l_score.updateTrackFromTickindex(trackindex, tickindex))
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))


def command(command, singleCommand = False, appending = True, signal = None, durationChanged = False):
    """The highest abstraction level to call a command.
    Automatically chooses between applyToSelection or not.
    At appending position applies to the object before, can be overruled
    If needed can do a step right after execution.
    Can have a different command for selections or single Items.
    This may be the command used by the GUI most often except for
    inserts.
    Send an update signal to the GUI as well if connected."""
    if _getScore().selection and command:  #at least one selected item? Or command set to False, then its force single.
        setCompoundTake("start")
        firstSelectedItemInTracks = applyToSelection(command, signal=signal, durationChanged = durationChanged)
        if durationChanged:  #if a gui is ready to update by item only this will give the items.
            for item in firstSelectedItemInTracks:
                l_send(lambda: l_item.updateDuration(item))
                l_send(lambda: l_score.updateTrackFromItem(item))
        l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
        setCompoundTake("stop")

    else: #Single Item
        if not singleCommand:
            singleCommand = command
        inst = isinstance(_getCursorItem(), items.Appending)
        returnvalue = False
        if appending and inst: #On appending position apply the command to the previous object
            if not left(): #TODO: This happens far too often when processing at the end of a track. Can this be better? There can be nothing important between appending and the to-process item. no signatures etc. we can just process it internally instead of moving all cursors back and forth.
                return False  #There is ONLY appending here. Abort the whole command.
            cursorItem = _getCursorItem() #we need to check the cursor item exactly at this point because the cursor moved right before this.
            if signal:
                saveState(cursorItem, signal, durationChanged)
            returnvalue = singleCommand()
            if durationChanged:
                l_send(lambda:l_item.updateDuration(cursorItem))
                l_send(lambda:l_score.updateTrackFromItem(cursorItem))
            if signal and not type(signal) is str:
                l_send(lambda: signal(cursorItem))
            right()
        elif not inst: #not on appending.
            cursorItem = _getCursorItem()
            if signal:
                saveState(cursorItem, signal, durationChanged)
            returnvalue = singleCommand()
            if durationChanged:
                l_send(lambda:l_item.updateDuration(cursorItem))
                l_send(lambda:l_score.updateTrackFromItem(cursorItem))
            if signal and not type(signal) is str:
                l_send(lambda: signal(cursorItem))
        return returnvalue


def collectTrack(test, selection = False):
    """Search for all items for which test() = True and return
    a list of list from _getScore().parseAllTracks().
    If select = True all found items will be selected.

    test() needs two parameters for the current item and cursor-state.
    Let test return anything positive which will be saved,
    anything negative will be discarded. return the string "break" to
    abort the search and return the data until then, including the
    'break' item, which will be selected as well if select=True.

    This function is Track Based. The last item in Track 1 is considered
    'earlier' than the first item in Track 2.
    If you want to search in columns  (earlier in time is considered
    'earlier') use collectColumn()"""
    ret = _getScore().parseAllTracks(test)
    if selection:
        for track in ret:
            for item in track:
                item[0].select()
                l_send(lambda: l_item.select(item))
    return ret

def collectColumn(test, selection = False):
    """Like collectTrack(), only column based.
    earlier in time is considered 'earlier'
    Track 2, Item 1 is considered 'earlier' than Track 1, last item."""
    ret = _getScore().parseAllColumns(test)
    if selection:
        for track in ret:
            for item in track:
                item[0].select()
                l_send(lambda: l_item.select(item))
    return ret

def searchTrack(test):
    """Stop at the first time after the test returns True.
    Start from the current cursor position. Move the user(!) cursor
    to the first found item or to the end of the track.
    Moving the user cursor is the whole point of this function.

    There is only search right, not search left.
    There is no backend/api "next". This function will always find
    next item. If you are already on this item it will not move the
    cursor.
    The reason is that this command is meant as a basis for user
    interaction, so it needs a (G)UI, which should provide the
    "find next without asking the search parameters again" function.

    The test function receives two parameters:
    1. the current item
    2. the active cursor."""
    ret = None #in case nothing was found. This is done by python anyway, but explicit is better than implicit.
    startPos = getCursorPosition()
    for i in _getActiveCursor().iterate_incomplete():
        if test(i, _getActiveCursor()):
            ret = i
            break #End of the search

    if ret: #sync all cursors
        _getScore().comeLittleCursors()
        l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
    else: #or return to the initial position
        cursorPosition(startPos)
    return ret

def searchItemTrack(item):
    def test(i, cur):
        if i is item:
            return True

def searchColumn(test):
    """
    This works in parallel on tick values.
    Searches the complete score from the current position (see below)
    Returns the found item as tuple (tickindex, trackIndex, item)

    The test function receives two parameters:
    1. the current item
    2. the active cursor

    It searches from left to right and from top to down.
    Track 1, Item 1 -> Track 2, Item 1
    Track 1, Item 2 -> Track 2, Item 2  etc.
    Handles uneven tick-values and uneven track lengths as well.
    """
    #TODO: This is very unperformant. The items get tested over and over again, even if the end of the track is already reached.
    cursors = _getScore().cursors
    startPos = getCursorPosition()
    #The real action. Two loops over all cursor in sequence. The first pass checks the data the second equalises unequal tick values so that we really follow a timeline.
    while True:
        currentTicks = [] #reset each time
        for index, cursor in enumerate(cursors):
            testRet = test(cursor.get_item(), cursor)

            if testRet: #the search has ended! Item found.
                _getScore().activeCursor = cursor
                _getScore().comeLittleCursors()
                l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
                return testRet
            else:
                if cursor.right(): #Only add the current tickindex if not at the end of track. Otherwise this will be always the minimum because other cursors advance further and we get an endless loop.
                    currentTicks.append(cursor.tickindex)

        #if all(x == False for x in endOfTrackStatus): #the search ends when the end of the track for every cursor was reached.
        if not currentTicks: #the search ends when the end of the track for every cursor was reached which means none of the cursors added its tickindex to this list.
            cursorPosition(startPos)
            return None #stop search. All cursors are at the end of their tracks.

        for cursor in cursors: #reset cursors that have a higher tickindex than the minimum
            if cursor.tickindex > min(currentTicks):
                cursor.left()

#Directives with optional gui support

def insertStandalone(lilystring = None):
    if not lilystring or lilystring is "":
        ret = l_form.generateForm({"standalone":(" ", "")}, _("Standalone Lilypond Directive"), _("Lilypond text to insert on this position."))
        if ret:
            lilystring = ret["standalone"]
            _standalone(lilystring) #empty string is allowed as well.
    else:
        _standalone(lilystring)

def insertStandaloneWaitForNextChord(lilystring = ""):
    if not lilystring:
        ret = l_form.generateForm({"lilystring":(" ", "")}, _("Wait-For-Chord Lilypond Directive"), _("The text will be used as post directive for the next chord/note."))
        lilystring = ret["lilystring"]
        if ret:
            lilystring = ret["lilystring"]
    if lilystring:
        _standaloneWaitForNextChord(lilystring)

"""The edit variants are gui only. Any script can directly access .lilypond and .lilystring"""

#Cursor score
##Don't use names that indicate score. No "go" or "move".

"""
#The problem with the next two functions is that the active cursor can change and the tracks can change. We only need to store the position, not the cursor.
def getCursorPosition():
    cursor = _getActiveCursor()
    return (cursor, cursor.tickindex)

def cursorPosition(positionTuplet):
    _getScore().activeCursor = positionTuplet[0]
    _getScore().toTickIndex(positionTuplet[1])
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
"""


def getCursorPosition():
    return (_getTrackIndex(), _getActiveCursor().flatCursorIndex)

def getCursorFlatPosition():
    return (_getTrackIndex(), _getActiveCursor().flatCursorIndex)

def cursorFlatPosition(positionTuplet):
    """[0] is the track, [1] the tick index."""
    _getScore().activeCursor = _getScore().cursors[positionTuplet[0]]
    _getActiveCursor().goToIndex(positionTuplet[1])
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
    l_send(l_score.trackChanged)
    updateJackClientSmfChannelCache()

def cursorPosition(positionTuplet):
    """[0] is the track, [1] the tick index.
    If you give a specific item the cursor will try to find it on this
    tick position.
    This enables you to find an item in a zero-duration group."""
    _getScore().activeCursor = _getScore().cursors[positionTuplet[0]]
    _getScore().toTickIndex(positionTuplet[1])
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
    l_send(l_score.trackChanged)
    updateJackClientSmfChannelCache()

def getDuration():
    return(_getCursorItem().duration)

#Pitch Cursor

def _getRoot():
    return _getKeysig().root

def _getClefNearestC():
    """Pratically the voice-center. But only c."""
    return _getClef().nearestC

def _getOctave():
    return pitch.octave(_getCursorPitch())

def up():
    _getActiveCursor().up()
    l_send(lambda: l_cursor.setPitch(_getCursorPitch()))

def down():
    _getActiveCursor().down()
    l_send(lambda: l_cursor.setPitch(_getCursorPitch()))

def upOctave():
    _getActiveCursor().upOctave()
    l_send(lambda: l_cursor.setPitch(_getCursorPitch()))

def downOctave():
    _getActiveCursor().downOctave()
    l_send(lambda: l_cursor.setPitch(_getCursorPitch()))

def toPitch(value):
    _getActiveCursor().pitchindex = value
    l_send(lambda: l_cursor.setPitch(_getCursorPitch()))

def toRelativeScalePosition(index):
    """Needs an int position as steps away from the current clef nearest
    c. Places the pitch cursor on an in-scale position"""
    workingOctave = pitch.octave(_getClefNearestC())
    inOctave = pitch.diatonicIndexToPitch(index, workingOctave)
    final = pitch.toScale(inOctave, _getKeysig())
    toPitch(final)
    l_send(lambda: l_cursor.setPitch(final))

# Position Cursor
def right():
    a = _getScore().right()
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
    return a

def left():
    a = _getScore().left()
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
    return a

def measureRight():
    _getScore().measureRight()
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))

def measureLeft():
    _getScore().measureLeft()
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))

def head():
    """The backend does a hard reset. It resets all the values.
    The gui needs to imitate this"""
    _getScore().head()
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))

def tail():
    _getScore().tail()
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))

def trackUp():
    r = _getScore().trackUp()
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
    l_send(l_score.trackChanged)
    updateJackClientSmfChannelCache()

def trackDown():
    r = _getScore().trackDown()
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
    l_send(l_score.trackChanged)
    updateJackClientSmfChannelCache()
    return r

def trackFirst():
    r = _getScore().firstTrack()
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
    l_send(l_score.trackChanged)
    updateJackClientSmfChannelCache()
    return r

def trackLast():
    _getScore().lastTrack()
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
    l_send(l_score.trackChanged)
    updateJackClientSmfChannelCache()

def selectRight():
    toggleSelect()
    return right()

def selectLeft():
    r = left()
    if r: #only if the cursor moved. This prevents de-selecting when you are already at the beginning of a track.
        toggleSelect()
    return r

def selectTrack():
    _getTrack().select()
    l_send(l_track.select)

def selectScore():
    _getScore().select()
    l_send(l_score.selectScore)

def deselectScore():
    _getScore().deselect()
    l_send(l_score.deselectScore)

def selectMeasure():
    """Select the whole measure by move/select the cursor and later
    returning it"""
    pos = getCursorFlatPosition()
    while not _getActiveCursor().metricalPosition == 0 and left():
        pass
    selectMeasureRight()
    cursorFlatPosition(pos)

def selectMeasureRight():
    """Select from the current cursor position to the next metrical 0.
    Stops at the end of the track."""
    oneMeasure = _getActiveCursor().prevailingTimeSignature[-1].oneMeasureInTicks
    start = _getActiveCursor().tickindex
    while _getActiveCursor().tickindex < start + oneMeasure: #Go right until the current tick position is equal or bigger than the destination
        if not selectRight(): #executes as well. First move the active cursor, later sync the others. So the cursor at least tries to go right. At the end of track it is executed twice during this loop.
            break # end of track

def selectMeasureLeft():
    """Select from the current cursor position to the previous metrical 0.
    Stops at the beginning of the track."""
    oneMeasure = _getActiveCursor().prevailingTimeSignature[-1].oneMeasureInTicks
    start = _getActiveCursor().tickindex
    while _getActiveCursor().tickindex > start - oneMeasure: #Go right until the current tick position is equal or bigger than the destination
        if not selectLeft(): #executes as well. First move the active cursor, later sync the others. So the cursor at least tries to go left. At the beginning of track it is executed twice during this loop.
            break # beginning of track

def selectTail():
    """select from current position to end of track"""
    allItems = set()
    for i in _getActiveCursor().iterate_incomplete():
        i.select()
        allItems.add(i)
    l_send(lambda: l_score.selectGroup(allItems))
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))

def selectHead():
    """select from current position to end of track"""
    allItems = set()
    for i in _getActiveCursor().iterate_left_incomplete():
        i.select()
        allItems.add(i)
    l_send(lambda: l_score.selectGroup(allItems))
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))

def selectTrackUp():
    #TODO: Does this make sense at all? Selection is not a range. Better leave range selection to a gui (with a mouse).
    pass
def selectHeadTrackDown():
    #TODO: Does this make sense at all? Selection is not a range. Better leave range selection to a gui (with a mouse).
    pass

def invertSelection():
    """Toggle the selection for the whole score"""
    pos = getCursorFlatPosition()
    allItems = set()
    selection = set()
    for cur in _getScore().cursors:
        _getScore().activeCursor = cur
        for i in _getActiveCursor().iterate():
            if not i in allItems: #links and container
                allItems.add(i)
                if i.toggleSelect():
                    selection.add(i)
    l_send(l_score.deselectScore)
    l_send(lambda: l_score.selectGroup(selection))
    syncCursors()
    cursorFlatPosition(pos)

def syncCursors():
    _getScore().syncCursors()
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))

#Notes and Chords
#TODO: Here are many calls to get the cursor. If speed is needed get the active cursor state once and then all other items. For this its possible to rewrite the getCursor functions to take a cursor instance as parameter.
def insertChord(duration, pitch):
    chord = items.Chord(_getWorkspace(), duration, pitch)
    _insert(chord)

def insertChordList(duration, pitchlist):
    #TODO: not necessarry anymore. insertChord can now handle pitchlists. Remove in the future.
    """Create a chord object with multiple pitches."""
    chord = items.Chord(_getWorkspace(), duration, pitchlist[0])
    for p in pitchlist[1:]:
        chord.addNote(p)
    _insert(chord)

def _insertCursorChord(duration):
    """Insert a new chord with one note to the track.
    The intial note gets its pitch from the cursor position, to scale"""
    new_pitch = pitch.toScale(_getCursorPitch(), _getScore().activeCursor.prevailingKeySignature[-1])
    chord = items.Chord(_getWorkspace(), duration, new_pitch)
    if cursor.Cursor.oneTimePrevailingDot:
        chord.dots = 1
        cursor.Cursor.oneTimePrevailingDot = False
        l_send(l_cursor.prevailingDuration) #send an update to the gui.
    elif cursor.Cursor.prevailingDot:
        chord.dots = 1
    _insert(chord)

def _insertScalePosition(duration, index):
    """Needs an int position as steps away from the current clef nearest
    c. To scale.
    Does not move the cursor"""
    workingOctave = pitch.octave(_getClefNearestC())
    inOctave = pitch.diatonicIndexToPitch(index, workingOctave)
    final = pitch.toScale(inOctave, _getKeysig())
    insertChord(duration, final)

def insertLonga():
    _insertCursorChord(6144)

def insertBreve():
    _insertCursorChord(3072)

def insert1():
    _insertCursorChord(1536)

def insert2():
    _insertCursorChord(768)

def insert4():
    _insertCursorChord(384)

def insert8():
    _insertCursorChord(192)

def insert16():
    _insertCursorChord(96)

def insert32():
    _insertCursorChord(48)

def insert64():
    _insertCursorChord(24)

def insert128():
    _insertCursorChord(12)

def insert256():
    _insertCursorChord(6)

def prevailingLonga():
    _choosePrevailingDuration(6144)

def prevailingBreve():
    _choosePrevailingDuration(3072)

def prevailing1():
    _choosePrevailingDuration(1536)

def prevailing2():
    _choosePrevailingDuration(768)

def prevailing4():
    _choosePrevailingDuration(384)

def prevailing8():
    _choosePrevailingDuration(192)

def prevailing16():
    _choosePrevailingDuration(96)

def prevailing32():
    _choosePrevailingDuration(48)

def prevailing64():
    _choosePrevailingDuration(24)

def prevailing128():
    _choosePrevailingDuration(12)

def prevailing256():
    _choosePrevailingDuration(6)

def togglePrevailingDot():
    """The prevailing dot is used in Prevailing Duration as well as
    the default insert chord"""
    cursor.Cursor.prevailingDot = not(cursor.Cursor.prevailingDot) #Class variable from the cursor module
    l_send(l_cursor.prevailingDuration) #send an update to the gui.

def toggleNextChordBecomesDotted():
    """This is only set to True because it will be reverted by the next
    insert chord."""
    cursor.Cursor.oneTimePrevailingDot = not cursor.Cursor.oneTimePrevailingDot
    l_send(l_cursor.prevailingDuration) #send an update to the gui.

def insertPrevailingDuration(pitch = None):
    """This can either be used for midi-in or 'one-key' input."""
    if not pitch:
        pitch = pitch.toScale(_getCursorPitch(), _getActiveCursor().prevailingKeySignature[-1])
    chord = items.Chord(_getWorkspace(), cursor.Cursor.prevailingDuration, pitch)

    if cursor.Cursor.oneTimePrevailingDot:
        chord.dots = 1
        cursor.Cursor.oneTimePrevailingDot = False
        l_send(l_cursor.prevailingDuration) #send an update to the gui.
    elif cursor.Cursor.prevailingDot:
        chord.dots = 1

    _insert(chord)
    toPitch(pitch)
    return pitch

def insertPrevailingScalePosition(index):
    _insertScalePosition(cursor.Cursor.prevailingDuration, index) #cursor is the class variable from the cursor module

def _choosePrevailingDuration(duration):
    cursor.Cursor.prevailingDuration = duration #Class variable
    l_send(l_cursor.prevailingDuration) #send an update to the gui.


def _getNearestPrevailingNote(basePitch, toScale = False):
    """Returns the nearest note of the given base pitch.
    The cursor pitch is taken as reference so that octave breaks can be
    calculated.
    Mimics Lilypond behaviour"""
    distance = pitch.halfToneDistance(pitch.plain(_getCursorPitch()), basePitch)
    if distance > 6:
        octaveMod = 1
    elif distance < -6:
        octaveMod = -1
    else:
        octaveMod = 0
    workingOctave = pitch.octave(_getCursorPitch()) + octaveMod
    if toScale:
        return pitch.toScale(pitch.toOctave(basePitch, workingOctave), _getKeysig())
    else:
        return pitch.toOctave(basePitch, workingOctave)

def insertNearestPrevailingNote(basePitch, toScale = False):
    """Add the nearest note of the given base pitch.
    The cursor pitch is taken as reference so that octave breaks can be
    calculated.
    Mimics Lilypond behaviour"""
    return insertPrevailingDuration(_getNearestPrevailingNote(basePitch, toScale))

def restToChord(pitchOrPitchlist):
    """Does not work on selections or on appending position.
    Use only in special cases in a script. This is intended
    as a user input function."""
    i = _getCursorItem()
    if type(i) is items.Rest:
        delete()
        chord = items.Chord(_getWorkspace(), 0, pitchOrPitchlist)
        chord.putDurations(i.getDurations())
        _insert(chord)
        return True
    else:
        return False

def addNearestNoteToChord(basePitch, toScale = False):
    """Add the nearest note of the given base pitch.
    The cursor pitch is taken as reference so that octave breaks can be
    calculated.
    Mimics Lilypond behaviour"""
    return addNoteToChord(_getNearestPrevailingNote(basePitch, toScale))

def addNoteToChord(pitch, replaceRest = False):
    """Add a chord note to the current tick position"""
    if replaceRest:
        if not restToChord(pitch):
            command(False, singleCommand = lambda: _getCursorItem().addNote(pitch), signal = l_item.updateDynamic, appending=True)
            toPitch(pitch) #move cursor to pitch
    else:
        command(False, singleCommand = lambda: _getCursorItem().addNote(pitch), signal = l_item.updateDynamic, appending=True)
        toPitch(pitch) #move cursor to pitch
    return pitch

def addCursorNoteToChord():
    """Add the current cursor position as note to the chord under the
    cursor. Note is in scale"""
    new_pitch = pitch.toScale(_getCursorPitch(), _getActiveCursor().prevailingKeySignature[-1])
    addNoteToChord(new_pitch, replaceRest = True)

def addOctaveToChord():
    """Cursor position decided which note as base. >= is oct up, else
    is below
    Does not need an undo signal because addNoteToChord does it already
    a command with signal and undo"""
    no = _getClosestNoteToCursor().pitch
    cu = _getCursorPitch()
    if not no: #not for rests.
        return False
    if cu >= no:
        f = lambda: addNoteToChord(_getLowestNote().pitch + 350)
        command(f, appending = False)
    else:
        f = lambda: addNoteToChord(_getHighestNote().pitch - 350)
        command(f, appending = False)

def addScalePositionToChord(index):
    workingOctave = pitch.octave(_getClefNearestC())
    inOctave = pitch.diatonicIndexToPitch(index, workingOctave)
    final = pitch.toScale(inOctave, _getKeysig())
    addNoteToChord(final)

def deleteCursorNote():
    """Delete the note under the cursor from a chord.
    If the last note is recorded it is replaced with a rest.
    These are two different items """

    def do():
        i = _getCursorItem()
        i.deleteNote(_getClosestNoteToCursor())
        if not i.notelist:  #is still a note left?
            d = i.getDurations()
            delete() #if not delete the whole chord. There is no chord without notes.
            rest = items.Rest(_getWorkspace(), d["_base"], "r")
            rest.putDurations(d)
            _insert(rest)
            l_send(lambda: l_item.updateDynamic(rest))
        else:
            l_send(lambda: l_item.updateDynamic(i))
    command(False, singleCommand = do)


def deleteOuterNote(lowest = True):
    """From a chord delete the lowest or higehst note."""
    def do():
        i = _getCursorItem()
        if not len(i.notelist) == 1: #leave the last note alone. #TODO: Test if this is indeed the desired behaviour.
            if lowest:
                i.deleteNote(_getLowestNote())
            else:
                i.deleteNote(_getHighestNote())

    command(do, signal = l_item.updateDynamic)


#Rests
def _insertNonPitchDuration(duration, variant):
    """Insert a new chord with one note to the track.
    The intial note gets its pitch from the cursor position, to scale"""
    #If this changes change the custom rest in deleteCursorNote as well.
    rest = items.Rest(_getWorkspace(), duration, variant)
    _insert(rest)

def _insertRest(duration):
    _insertNonPitchDuration(duration, "r")

def _insertSkip(duration):
    _insertNonPitchDuration(duration, "s")

def insertRestLonga():
    _insertRest(6144)

def insertRestBreve():
    _insertRest(3072)

def insertRest1():
    _insertRest(1536)

def insertRest2():
    _insertRest(768)

def insertRest4():
    _insertRest(384)

def insertRest8():
    _insertRest(192)

def insertRest16():
    _insertRest(96)

def insertRest32():
    _insertRest(48)

def insertRest64():
    _insertRest(24)

def insertRest128():
    _insertRest(12)

def insertRest256():
    _insertRest(6)

def insertRestM(measures=None, placeholder = False):
    """Multi Measure Rest or Skip (non-printing)"""
    lilysyntax = "R"
    if not measures:
        ret = l_form.generateForm({"measures":(_("Measures"), 1), "skip":(_("Skip"), False), "zplace":(_("Placeholder"), False)}, _("Multi Measure Rest"), _("Rest how many measures?\nCheck Skip to print empty measures.\nCheck Placeholder for more empty space."))
        if ret: #User canceled?
            measures = ret["measures"]
            if ret["skip"]:
                lilysyntax = "\\skip"
            if ret["zplace"]:
                placeholder = True
    if measures: #Even with the Gui 'cancel" was possible.
        if placeholder:
            rest = items.Placeholder(_getWorkspace(), measures)
        else:
            rest = items.MultiMeasureRest(_getWorkspace(), measures, lilysyntax)
        _insert(rest)

def insertSkipLonga():
    _insertSkip(6144)

def insertSkipBreve():
    _insertSkip(3072)

def insertSkip1():
    _insertSkip(1536)

def insertSkip2():
    _insertSkip(768)

def insertSkip4():
    _insertSkip(384)

def insertSkip8():
    _insertSkip(192)

def insertSkip16():
    _insertSkip(96)

def insertSkip32():
    _insertSkip(48)

def insertSkip64():
    _insertSkip(24)

def insertSkip128():
    _insertSkip(12)

def insertSkip256():
    _insertSkip(6)

def insertUpbeat(ticks=None):
    """Give the number of ticks in the upbeat. Not the missing rest"""
    if not ticks:
        data = {"0nominator":(_("How many notes"), 1),
        "1denominator":(_("Of which duration"), ["4", ("1", _("1 Whole / Semibreve")), ("2", _("2 Half / Minim")), ("4", _("4 Quarter / Crotchet")), ("8", _("8 Eigth / Quaver")), ("16", _("16 Sixteenth / Semiquaver")), ("32", _("32 Thirthy-Second / Demisemiquaver")), ("64", _("64 Sixty-Fourth / Hemidemisemiquaver")), ("128", _("128 Hundred Twenty-Eighth / Semihemidemisemiquaver ")), ("256",_("256 Twohundred Fifty-Sixth / Demisemihemidemisemiquaver"))] ),
        }
        ret = l_form.generateForm(data, _("Upbeat (Anacrusis)"), _("How long?\n"))

        if ret: #User canceled?
            ticks = ret["0nominator"] * lilypond.ly2dur[ret["1denominator"]]
    if ticks: #Even with the Gui 'cancel" was possible.
        upbeat = items.Upbeat(_getWorkspace(), ticks)
        _insert(upbeat)

#Items
def _deleteProto():
    it = _getCursorItem()
    #if not isinstance(it, items.Appending) and not type(it) is core.Container:
    if not isinstance(it, items.Appending) and (not type(it) is core.Container or (type(it) is core.Container and not it.cursorWalk)): #Container deletes are only allowed if it is non cursor-walk container, which is represented as a standalone item.
        l_send(lambda: l_item.deleteAtCursor(it, _getContainer()))
        _getActiveCursor().delete()
        return True
    else:
        return False

"""
def _deleteProtoSelection():
    #The most custom and redundant function in this API.
    #Delete Selection needs most exceptions and special rules
    #so it is its own function.
    #Except pasteReplaceSelection, which is a copy of this function here
    if _getScore().selection and command:  #at least one selected item? Or command set to False, then its force single.
        setCompoundTake("start")
        firstSelectedItemInTracks = [] #if a gui is ready to update by item only this will send the items.
        allDeleted = []
        startItem = _getCursorItem() #Save the current item
        for cursor in _getScore().cursors: #for each cursor/track
            _getScore().activeCursor = cursor
            #if _getTrack().selectedItems:
            if cursor.nested_list.selectedItems: #nested_list = track. Does this track has any selectedItems? #TODO: Does this work if I select a linked item in one track and the link partner is in another track?
                allDeleted += cursor.nested_list.selectedItems
                head()
                first = True #a switch for the first selected item
                for i in cursor.iterate():
                    if i.selected:
                        if first:
                            firstSelectedItemInTracks.append(i)
                            first = False

                        saveState(i, signal, durationChanged)
                        _getActiveCursor().delete()
                        if signal and not type(signal) is str:
                            signal(i)
        head()
        #Now everything is processed and all cursors are resetted to 0.
        #According to the docstring now comes the serious part. Check if the current item is still the same from the beginning.

        cursorPosition(start) #try to reach the old item position. This has at least the effect that we go to the starting track once again. ApplyToSelection is not able to do anything with tracks (except someone writes faulty function)
        if not _getCursorItem() == startItem: #Positions are wrong now!
            def matchStart(item, cursor): #because lambda is so limited in python
                if item == startItem:
                    return firstSelectedItemInTracks
            head() #once again to the start. Nobody said modifying lists while iteration was easy.
            searchTrack(matchStart) #if this fails (item deleted or cut) the position does not get changed by the search and we stay at head.


        for item in firstSelectedItemInTracks: #if a gui supports to update by item only this will give the items.
            l_send(lambda: l_item.updateDuration(item))
            l_send(lambda: l_score.updateTrackFromItem(item))
        l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
        setCompoundTake("stop")
        return firstSelectedItemInTracks
    else:
        return False
"""

def delete():
    """Delete a single item or all selected items"""
    getContainer = _getContainer()
    if _getScore().selection and _getCursorItem().selected:  #at least one selected item and we start on a selection?
        #Go to the first item left which has no selection. ApplyToSelection will remember this and return to it after deleting.
        #Just deleting from a selected item will result in jumping to head afterwards.
        switch = True
        start = _getFlatCursorIndex()
        while _getCursorItem().selected:
            if not left():
                break
        #now we are BEFORE/on the not-selected item. After deleting we go one right again.
    else:
        switch = False

    def selection():
        indx = _getFlatCursorIndex()
        track = _getTrackIndex()
        while _getCursorItem().selected:
            #if switch: #The first time is saveStated by apply to Selection
            #saveState(_getCursorItem(), signal = "delete", True)
            #else:
            #    switch = True
            l_send(lambda: l_cursor.setPosition(_getWorkspace(), track, indx))
            #l_send(lambda: l_cursor.setPosition(track, indx))
            _deleteProto()

    def single():
        return _deleteProto()

    retvalue = command(selection, single, signal = "delete", appending = False)  #Don't delete the item left of Appending when on appending position

    if switch and not start == 0:  #at least one selected item?
        right()

    if getContainer:
        syncCursors()
    l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
    return retvalue


def backspace():
    if _getScore().selection:  #at least one selected item?
        delete()
    else:
        if _getFlatCursorIndex() != 0:
            left()
            if not delete():
                right()


def select():
    i = _getCursorItem()
    if i.select(): #no appending
        l_send(lambda: l_item.select(i))

def deselect():
    i = _getCursorItem()
    if i.deselect():
        l_send(lambda: l_item.deselect(i))

def toggleSelect():
    if _getActiveCursor().get_item().selected:
        deselect()
    else:
        select()

def remoteSelect(item, track):
    """Select a specific item. Does not use the items select method.
    This is a convenience function for a gui that wants to select
    an item with the mouse. Which means not moving the cursor there
    first.
    However, we still need to be in the same score.
    ACHTUNG: The breaks the moment the selection becomes dependent of
    the cursor stats. I do not see why this will happen, but who knows."""

    if not item.selected:
        item.selected = True
        track.selectedItems.append(item)
        _getScore().selection += 1 #increase the selection counter
        l_send(lambda: l_item.select(item))
        return True
    else:
        return False

def remoteDeselect(item, track):
    """Deselect a specific item. Does not use the items deselect method.
    This is a convenience function for a gui that wants to select
    an item with the mouse. Which means not moving the cursor there
    first.
    However, we still need to be in the same score.
    ACHTUNG: The breaks the moment the selection becomes dependent of
    the cursor stats. I do not see why this will happen, but who knows."""
    if item.selected:
        item.selected = False
        track.selectedItems.remove(item)
        _getScore().selection -= 1 #decrease the selection counter
        l_send(lambda: l_item.deselect(item))
        return True
    else:
        return False

def remoteToggleSelect(item, track):
    if item.selected:
        remoteDeselect(item, track)
    else:
        remoteSelect(item, track)

def duplicate():
    """Creates a real, deep copy of the current object and insert it.
    Not a reference, the two objects are different instances.
    Insert is done left of the current item, so the original is the
    second object in the track. Important to keep in mind when
    duplicating linked items."""
    def do():
        new = deepcopy(_getCursorItem())
        new.instanceCount = 0  #reset Instance Count to 0, which is the default in item.init().
        new.newName() #generate a new internal unique id.
        _insert(new)
    #command(False, singleCommand = do, appending=False)
    command(do, appending=True)

def splitDuplicate(number=None):
    """Create n real, deep copies of the current object.
    Each has duration/n of the original item. The original item
    gets deleted in the process so that linked items are not modified."""
    def do(item, select = False):
        new = deepcopy(item)
        new.guessDurationAbsolute(item.duration/number)
        if select:
            new.select()
        for x in range(number):
            _pasteItem(new, standaloneInsert = True)

    if not number:
        ret = l_form.generateForm({"number": (_("How Many"), 4)}, _("Split Chord") , _("Split the current chord in n new ones, the sum has the original duration/n."))
        if ret:
            number = ret["number"]
        else:
            return False #cancel GUI

    if _getScore().selection:  #there is at least one selected item?
        def test(item, cursor):
            if item.selected and type(item) is items.Chord:
                return True
        replace(test, lambda item: do(item, select = True), selectNewItems = False) #don't select the new items by replace. We do it ourselves.
    else:
        def tmp():
            i = _getCursorItem()
            if type(i) is items.Chord:
                delete()
                do(i)
                left()
        command(tmp)
        #for x in range(number):
        #    left()

def link():
    """Creates a link of the current item. Only a reference. If you
    change A, you change B."""
    item = _getCursorItem()
    _insert(item)
    l_send(lambda: l_item.updateDynamic(item)) #this is mainly to update the first item since insert takes care of updating the new item.



def joinToChordSumDurations():
    """Delete the current selection and replace it with a new chord
    made from the old notes. Duration is the sum of the deleted notes.
    Each pitch is only used once."""
    setCompoundTake("start")
    summ = 0
    pitches = set()
    for tr in _getScore():
        for i in tr.selectedItems:
            summ += i.duration
            if type(i) is items.Chord:
                for x in i.notelist:
                    pitches.add(x.pitch)
    insertChordList(summ, list(pitches))
    delete()
    setCompoundTake("stop")

def joinToChordPrevalentDurations():
    """"""
    setCompoundTake("start")
    pitches = set()
    durations = []
    for tr in _getScore():
        for i in tr.selectedItems:
            durations.append(i.duration)
            if type(i) is items.Chord:
                for x in i.notelist:
                    pitches.add(x.pitch)
    dur = max(set(durations), key=durations.count) #the most common duration in this list.
    insertChordList(dur, list(pitches))
    delete()
    setCompoundTake("stop")


def toggleHidden():
    def do():
        i = _getCursorItem()
        i.hidden = not i.hidden
    command(do, signal = l_item.updateVisibility)

def toggleTransparent():
    def do():
        i = _getCursorItem()
        i.transparent = not i.transparent
    command(do, signal = l_item.updateVisibility)

def cleanHiddenTransparent():
    def do():
        i = _getCursorItem()
        i.transparent = False
        i.hidden = False
    command(do, signal = l_item.updateVisibility)

def insertHideNotesOn():
    i = items.Item(_getWorkspace())
    i.lilypond = "\\hideNotes"
    _insert(i)

def insertHideNotesOff():
    i = items.Item(_getWorkspace())
    i.lilypond = "\\unHideNotes"
    _insert(i)

def insertHideBarlinesOn():
    i = items.Item(_getWorkspace())
    i.lilypond = "\\override Staff.BarLine #'transparent = ##t"
    _insert(i)

def insertHideBarlinesOff():
    i = items.Item(_getWorkspace())
    i.lilypond = "\\override Staff.BarLine #'transparent = ##f"
    _insert(i)

#Container

def containerFromSelection():
    """Convert the current selection to a single-track container"""
    return _getScore().createContainerFromSelection()

def containerFromFile(filepath):
    """Open an lbjs file, get the first score, first track container
    and return it as new container."""
    new = True
    return new
    #TODO

def insertContainer(cont = None):
    """No parameter to insert a new container"""
    setCompoundTake("start")
    if STANDALONE:
        if not cont:
            cont = core.Container(_getWorkspace())
        _insert(cont)
        setCompoundTake("stop")
        return cont
    else:
        if not cont:
            cont = core.Container(_getWorkspace(), )
            ret = l_form.generateForm({"01Name" : (_("Name"),cont.uniqueContainerName), "02Group" : (_("Group (optional)"), cont.group), "cursorWalk":(_("Cursor Walk"), cont.cursorWalk)}, _("Create Container"), _("Choose a name and an optional group name."))
            if ret:
                cont.uniqueContainerName = ret["01Name"]
                cont.group = ret["02Group"]
                cont.cursorWalk = ret["cursorWalk"]
                _insert(cont)
                setCompoundTake("stop")
                return cont
            else:
                setCompoundTake("stop")
                return False
        else:
            _insert(cont)
            setCompoundTake("stop")
            return cont

#Not reliable at the moment. Cursor hops around and you never know where it will be inserted.
#And in the gui the items are not re-registered in workspace.allItems
#Cloning copy and paste behaviour would be the correct thing. Insert them with the proper instructions so that the gui can keep track.
#Think of multi-track copy/paste
#def convertSelectionToContainer(cursorWalk = False):
    #"""Delete the selection and replace it with a container
    #with a copy of the selected items.
    #Insert at current cursor position"""
    #sel = containerFromSelection()

    #delete()
    #insertContainer(sel)

def deleteContainerInstance():
    """Delete the most inner container at the cursor position.
    All container deletes must go through here to keep a potential
    gui in sync because only this method deletes from
    workspace.score.nonTrackContainers"""
    cont = _getActiveCursor().delete_list()
    if cont:
        saveState(cont, "deleteContainer", True) #a fake signal. Undo knows how to deal with it.
        deselectScore()
        d = _getScore().nonTrackContainers
        d[cont].pop() #decrease the instance count.
        #if not d[cont]: #that was the last
            #Deleting the last one and then removing from the score is a convenience. If you save and re-load the container is gone as well.
        #    if cont.uniqueContainerName in _getWorkspace().uniqueContainerNames:
        #        _getWorkspace().uniqueContainerNames.remove(cont.uniqueContainerName)
        #    del d[cont]
        l_send(lambda: l_track.deleteContainerInstance(cont))
        l_send(lambda: l_cursor.setPosition(_getWorkspace(), _getTrackIndex(), _getFlatCursorIndex()))
    return cont

#Tracks
def trackAdd():
    #Todo: Undo
    tr = _getScore().addTrack()
    l_send(lambda: l_score.addLastTrack(tr))
    l_send(l_score.trackChanged)
    updateJackClientSmfChannelCache()
    return tr

def trackDelete(justDelete = False):
    #Todo: Undo
    if (not STANDALONE) and (not justDelete):
        if not message(message = _("Are you sure you want to delete this track? It cannot be undone."), title = _("Delete Track")):
            return False
    toDelete = _getTrackIndex()
    _getScore().deleteTrack()
    l_send(lambda: l_score.deleteTrack(toDelete))
    l_send(l_score.trackChanged)
    updateJackClientSmfChannelCache()

def trackSwapUp():
    #Todo: Undo
    if _getScore().swapTrackWithAbove():
        l_send(l_score.trackSwapUp)
        l_send(l_score.trackChanged)

def trackSwapDown():
    #Todo: Undo
    if _getScore().swapTrackWithBelow():
        l_send(l_score.trackSwapDown)
        l_send(l_score.trackChanged)

def trackMergedVoice(boolean):
    _getTrack().mergeWithUpper = boolean
    l_send(l_score.trackChanged)

def toggleVoice():
    #if _getTrack().mergeWithUpper:
    #    trackMergedVoice(False)
    #else:
    #    trackMergedVoice(True)
    trackMergedVoice(not _getTrack().mergeWithUpper)

def insertStaffBreak():
    _standalone("\\break")

def trackName(trackVar = None):
    if not trackVar:
        trackVar = l_form.generateForm({"name":(" ", _getTrack().uniqueContainerName)}, _("Track Name"), _("Enter a unique track name."))
        if trackVar: #User canceled?
            trackVar = trackVar['name']
    if trackVar:
        _getTrack().uniqueContainerName = trackVar
    l_send(l_score.trackChanged)

def trackChangeAttribute(attribute, value):
    setattr(_getTrack(), attribute, value)
    #l_send(l_score.trackChanged) #Don't reactivate. If you edit a text field this will reset the cursor so it jumps around making editing shitty.
    updateJackClientSmfChannelCache()

#Chords and Notes
def putDurations(base, dots = 0, tuplets = []):
    """Leaves scaleFactor as it is"""
    def do():
        i = _getCursorItem()
        ret = {}
        ret["_base"] = base
        ret["_tuplets"] = tuplets
        ret["_dots"] = dots
        ret["_scaleFactorNumerator"] = i._scaleFactorNumerator
        ret["_scaleFactorDenominator"] = i._scaleFactorDenominator
        i.putDurations(ret)
    command(do, signal=l_item.updateDynamic, durationChanged=True)

def augment():
    def do():
        _getCursorItem().augment()
    command(do, signal=l_item.updateDynamic, durationChanged=True)

def diminish():
    def do():
        _getCursorItem().diminish()
    command(do, signal=l_item.updateDynamic, durationChanged=True)

def dotAdd():
    def do():
        _getCursorItem().addDot()
    command(do, signal=l_item.updateDynamic, durationChanged=True)

def dotRemove():
    def do():
        _getCursorItem().removeDot()
    command(do, signal=l_item.updateDynamic, durationChanged=True)

def dotToggle():
    def do():
        i = _getCursorItem()
        if i.dots:
            i.dots = 0
        else:
            i.dots = 1
    command(do, signal=l_item.updateDynamic, durationChanged=True)

def sharpen():
    command(lambda: _applyNoteMethodToCurrentChord("sharpen"), singleCommand=lambda: _getClosestNoteToCursor().sharpen(_getKeysig()), signal = l_item.updateDynamic)

def flatten():
    command(lambda: _applyNoteMethodToCurrentChord("flat"), singleCommand=lambda: _getClosestNoteToCursor().flat(_getKeysig()), signal = l_item.updateDynamic)

def noteEnharmonicSharp():
    def do():
        _getClosestNoteToCursor().enharmonicSharp(_getKeysig())
    command(False, singleCommand = do, signal = l_item.updateDynamic)

def noteEnharmonicFlat():
    def do():
        _getClosestNoteToCursor().enharmonicFlat(_getKeysig())
    command(False, singleCommand = do, signal = l_item.updateDynamic)

def toggleNoteReminderAccidental():
    """Lilypond only: !"""
    def do():
        _toggleDirective(_lilyNotePst, "accidental", "!")
    command(False, singleCommand = do, signal = l_item.updateDynamic)


def toggleNoteCautionaryAccidental():
    """Lilypond only: ?"""
    def do():
        _toggleDirective(_lilyNotePst, "accidental", "?")
    command(False, singleCommand = do, signal = l_item.updateDynamic)

def insertSuggestedAccidentalsStyleOn():
    _standalone("\\set suggestAccidentals = ##t")

def insertSuggestedAccidentalsStyleOff():
    _standalone("\\set suggestAccidentals = ##f")

def _applyNoteMethodToCurrentChord(methodAsString, *args):
    _getCursorItem().applyNoteMethodToChord(methodAsString, _getKeysig(), *args)

def shiftChordUp():
    command(lambda: _applyNoteMethodToCurrentChord("stepUpInScale"), signal =  l_item.updateDynamic, durationChanged=True)

def shiftChordDown():
    command(lambda: _applyNoteMethodToCurrentChord("stepDownInScale"), signal =  l_item.updateDynamic, durationChanged=True)

def shiftChordOctaveUp():
    command(lambda: _applyNoteMethodToCurrentChord("octaveUp"), signal =  l_item.updateDynamic, durationChanged=True)

def shiftChordOctaveDown():
    command(lambda: _applyNoteMethodToCurrentChord("octaveDown"), signal =  l_item.updateDynamic, durationChanged=True)


def mirror(axis = None):
    """Mirror the current chord with the cursor as axis
    Accepts another pitch as axis as well."""
    if not axis:
        axis = _getCursorPitch()
    command(lambda: _applyNoteMethodToCurrentChord("mirror", axis), signal =  l_item.updateDynamic)


def shiftNoteUp():
    _modifyNotePitch("stepUpInScale")
    up()

def shiftNoteDown():
    _modifyNotePitch("stepDownInScale")
    down()

def shiftNoteOctaveUp():
    _modifyNotePitch("octaveUp")
    upOctave()

def shiftNoteOctaveDown():
    _modifyNotePitch("octaveDown")
    downOctave()

def transpose(rootPitch=None, targetPitch=None):
    if not (rootPitch and targetPitch):
        ret = l_form.generateForm({"01root":(_("From"), ["c'"] + lilypond.sortedNoteNameList), "02target":(_("To"), ["c'"] + lilypond.sortedNoteNameList)}, _("Transpose Chord"), _("Choose two pitches which represent an interval."))
        if ret: #User canceled?
            rootPitch = lilypond.ly2pitch[ret["01root"]]
            targetPitch = lilypond.ly2pitch[ret["02target"]]
    if rootPitch and targetPitch:
        command(lambda: _applyNoteMethodToCurrentChord("intervalAutomatic", rootPitch, targetPitch), signal =  l_item.updateDynamic)


def transposeUp(interval, octave):
    command(lambda: _applyNoteMethodToCurrentChord("intervalUp", interval, octave), signal =  l_item.updateDynamic)


def transposeDown(interval, octave):
    command(lambda: _applyNoteMethodToCurrentChord("intervalDown", interval, octave), signal =  l_item.updateDynamic)


def tupletRemove():
    """Completely removes all tuplets"""
    def do():
        _getCursorItem().tuplets[:] = []
    command(do, signal = l_chord.tupletMarkers, durationChanged=True)

def _tuplet(newtuplets):
    """args are pairs: (2,3) for triplet"""
    def do():
        _getCursorItem().tuplets.extend(newtuplets)
    command(do, signal = l_chord.tupletMarkers, durationChanged=True)

def toggleTriplet():
    """Creates or deletes a normal triplet.
    If there is a non-triplet tuplet it will get overwritten"""
    i = _getCursorItem()
    r = False
    if type(i) is items.Appending:
        left()
        i = _getCursorItem()
        r = True
    if i.tuplets == [[2,3]]:
        tupletRemove()
    else:
        i.tuplets[:] = []  #don't use tupletRemove. It triggers the command two times and sends two update signals when the next tuplet(2,3) does all that already.
        tuplet(2,3)
    if r:
        right()


def tuplet(x=None, y=None):
    if not (x and y):
        ret = l_form.generateForm({"01numerator": (_("Numerator"), 1), "02denominator": (_("Denominator"), 1),}, _("Custom Tuplet") , _("Give a fraction to multiply the notes base duration with.\n\n\"Instead of NUMERATOR number of notes to build the next higher duration level it now takes DENOMINATOR of these.\""))
        if ret:
            x = ret["01numerator"]
            y = ret["02denominator"]
    if x and y:
        _tuplet([[x, y]])

def _tremolo(split):
    """Needs an int > 1"""
    def do():
        _getCursorItem().split = split
    command(do, signal = l_chord.tremoloMarker)

def tremolo(split=None):
    if not split:
        ret = l_form.generateForm({"split" : (" ", _getCursorItem().split)}, _("Tremolo/Split"), _("Give a tremolo/split number for this note.\nYou will correctly hear any split value, but only values which result in basic durations can be exported to pdf yet."))
        if ret:
            split= ret["split"]
    if split and split > 1: #gui and user did not cancel.
        _tremolo(split)
        return True
    elif split and split <= 1 : #Empty string still True. Means reset to 1.
        _tremolo(1)
        return True
    else: #Cancel
        return False

def chordSymbol(symbolString = None):
    i = _getCursorItem()
    if not symbolString:
        ret = l_form.generateForm({"chord" : (" ", i.chordsymbols)}, _("Chord Symbol"), _("<p>Give a chord symbol for this note.<br>Lilypond Syntax.</p><p>Use the pipe letter | for more than one chord.</p><p>Example:<pre>c|c:maj7</pre></p><p>Tips:<br>Use the letter s for a skip.</p>"))
        if ret:
            symbolString = ret["chord"]
    if symbolString or symbolString == "" : #gui and user did not cancel. Empty string still True
        saveState(i, l_chord.chordSymbol, False)
        i.chordsymbols = symbolString
        l_send(lambda: l_chord.chordSymbol(i))
        return True
    else:
        return False

def chordSymbolDelete():
    """Delete all chord symbols on an item or selection"""
    def do():
        _getCursorItem().chordsymbols = ""
    command(do, signal = l_chord.chordSymbol)

def figuredBass(figuresString = None):
    if not figuresString:
        ret = l_form.generateForm({"figure" : (" ", _getCursorItem().figures)}, _("Figured Bass"), _("Give a figured bass for this note.\nUse the pipe '|' for more than one figure.\nLilypond Syntax. Example: '6 4|5 3'\n\nTips:\nUse _ for an empty position\nUse @ in the string to replace recurring numbers with a line-extender (@6 4|6 3)\nUse ? for a forced vertical space (empty row). May look wrong, depending on the font size."))
        if ret:
            figuresString = ret["figure"]
    if figuresString: #gui and user did not cancel.
        i = _getCursorItem()
        saveState(i, l_chord.figuredBass, False)
        i.figures = figuresString
        l_send(lambda: l_chord.figuredBass(i))
        return True
    elif figuresString == "":
        figuredBassDelete()
    else:
        return False


def figuredBassDelete():
    """Delete all chord symbols on an item or selection"""
    def do():
        _getCursorItem().figures = ""
    command(do, signal=l_chord.figuredBass)


#Voice Presets
def insertVoiceAutomatic():
    _standalone("\\oneVoice")
def insertVoiceOne():
    _standalone("\\voiceOne")
def insertVoiceTwo():
    _standalone("\\voiceTwo")
def insertVoiceThree():
    _standalone("\\voiceThree")
def insertVoiceFour():
    _standalone("\\voiceFour")

#StaffGroups
##They have pseudo-random tags. You can have as many starts and ends on one track as you wish. randrange(1,100000) gives us a approximately a 3 digit number of retries before hitting the same number twice. Nobody will use nearly as many group starts on the same track.
def _groupStart(what, extra=""):
    _getTrack().directivePre["StaffGroup" + what + "Start" + str(random.randrange(1,100000)) ] = "\\new " + what + " <<\n" + extra
    l_send(l_score.trackChanged)
    l_send(l_score.trackDirectivesChanged)

def groupOrchestralStart():
    _groupStart("StaffGroup")

def groupGrandStart():
    _groupStart("GrandStaff")

def groupPianoStart():
    _groupStart("PianoStaff")

def groupChoirStart():
    _groupStart("ChoirStaff")

def groupVoiceAndFiguredBassStart():
    _groupStart("ChoirStaff", "\\set ChoirStaff.systemStartDelimiter = #'SystemStartBrace \n")

def groupSquareStart():
    _groupStart("StaffGroup", "\\set StaffGroup.systemStartDelimiter = #'SystemStartSquare \n")

def groupEnd(): #One end to end them all and in the endness end them.
    _getTrack().directivePst["StaffGroupEnd" + str(random.randrange(1,100000))] = ">>\n"
    l_send(l_score.trackChanged)
    l_send(l_score.trackDirectivesChanged)

def groupDeleteAll():
    track = _getTrack()
    for key in list(track.directivePre.keys()):
        if key.startswith("StaffGroup"):
            del track.directivePre[key]

    for key in list(track.directivePst.keys()):
        if key.startswith("StaffGroupEnd"):
            del track.directivePst[key]
    l_send(l_score.trackChanged)
    l_send(l_score.trackDirectivesChanged)

#Key Signatures
def _insertKeySignature(root, keysig, lilypond=None, explicit=False):
    keysig = items.KeySignature(_getWorkspace(), root, keysig, explicit)
    if lilypond:
        keysig.lilypond = lilypond
    _insert(keysig)

def insertTypicalKeyGui(root = "", scale = ""):
    """Predefined Key Signatures like Major, Minor, Church modes etc."""
    def _createKeysigList(root, scale):
        """First tranpose C Major to the new root,
        then adjust according to the schemes databse"""

        def _sortKeysig(keysig):
            wip = sorted(keysig)
            new = [wip[3], wip[0], wip[4], wip[1], wip[5], wip[2], wip[6],]
            if wip[6][1] == -10: #Bes included? Then use flat order.
                new.reverse()
            return new

        def _rotateKeysig(keysig, num):
            r = collections.deque(keysig)
            r.rotate(num)
            return list(r)

        def _rotateToIndex(keysig, pos):
            """pos is the first number of a keysig tupet.
            Use to rotate a root note to the front"""
            r = collections.deque(keysig)
            while not r[0][0] == pos:
                r.rotate(1)
            return list(r)

        def _transposeKeysigScale(scale, stepsInPillar):
            """Transpose a list of positions/modifications like a keysig
            Pillar Of Fifth Steps.
            Sadly the method only works for the Major Keysig"""
            #new = _sortKeysig(scale)
            counter = 0
            if stepsInPillar > 0:
                modificator = 10
            else:
                modificator = -10
                scale.reverse()

            transpose =  []
            for pos, accidental in scale:
                if counter == abs(stepsInPillar):
                    transpose.extend(scale[counter:7])  #append the missing notes to the return list.
                    break
                else:
                    transpose.append((pos, accidental + modificator))
                    counter += 1
            if stepsInPillar <= 0:  #Return as it came in. Most likely the return list will be sorted anyway later, but this here is more correct.
                transpose.reverse()
            return transpose

        schemes = { "Major": [0,0,0,0,0,0,0],
                    "Minor": [0,0,-10,0,0,-10,-10],
                    "Dorian": [0,0,-10,0,0,0,-10],
                    "Phrygian": [0,-10,-10,0,0,-10,-10],
                    "Lydian": [0,0,0,10,0,0,0],
                    "Mixolydian": [0,0,0,0,0,0,-10],
                    "Locrian": [0,-10,-10,0,-10,-10,-10],
                    "Mystical/Hollywood": [0,0,0,0,0,-10,-10], #Carry On, Stargate etc.
                    }
        #Prepare some data.
        index = pitch.pillarOfFifth.index(pitch.plain(lilypond.ly2pitch[root])) #which position in the pillar has the root note?
        distance = index - 15 #distance from C.
        if distance > 7:
            raise RuntimeError("Wrong root for keysig. Too far away from the center of the pillar of 5th. The Signature would end with double sharps/flats")
        major = _transposeKeysigScale([(3,0), (0,0), (4,0), (1,0), (5,0), (2,0), (6,0)], distance) #The pure to-given-root transposed C Major scale.
        major = _rotateToIndex(sorted(major), pitch.tonalDistanceFromC((pitch.plain(lilypond.ly2pitch[root])))) #Ordered by pitch, rotated to have the root note in front
        modificator = schemes[scale]
        #Modify the transposed major scale with our modificator pattern.
        ret = []
        for maj, mod in zip(major, modificator):
            ret.append((maj[0], maj[1] + mod))
        return _sortKeysig(ret)

    #Main Function
    if not (scale and root):
        data = {"root":("Root", ['c', 'ces', 'ges', 'des', 'aes','ees', 'bes', 'f', 'c', 'g', 'd', 'a', 'e', 'b', 'fis', 'cis']),
                "scale":("Scale", ["Major", "Major", "Minor", "Dorian", "Phrygian", "Lydian", "Mixolydian", "Locrian", "Mystical/Hollywood"]),
                }
        result = l_form.generateForm(data, _("Typical Key Signature"), _("Choose a root and a scale."))
        if result:
            scale = result["scale"]
            root = result["root"]
    if scale and root: #gui and user pressed cancel?
        if scale in [_("Major"), _("Major"), _("Minor"), _("Dorian"), _("Phrygian"), _("Lydian"), _("Mixolydian"), _("Locrian")]:
            string = "\\key " + root + " \\" + scale.lower()
        else:
            string = ""
        _insertKeySignature(pitch.plain(lilypond.ly2pitch[root]), _createKeysigList(root, scale) ,string)

def insertKeyGui():
    """Completely Custom Key Signature"""
    def liste():
        return [0, (0 ,_("\u266E Natural")), (10, _("\u266F Sharp")), (-10, _("\u266D Flat")), (20, _("\U0001D12A Double Sharp")), (-20, _("\U0001D12B Double Flat")), ]
    data = {"root":("Root", ['c', 'ces', 'ges', 'des', 'aes','ees', 'bes', 'f', 'c', 'g', 'd', 'a', 'e', 'b', 'fis', 'cis']),
            "0":("c", liste()),
            "1":("d", liste()),
            "2":("e", liste()),
            "3":("f", liste()),
            "4":("g", liste()),
            "5":("a", liste()),
            "6":("b/h", liste()),
            "explicit":(_("Explicit Naturals"), False),
            }
    result = l_form.generateForm(data, _("Custom Key Signature"), _("Choose a set of accidentals. Check 'Explicit Naturals' to force naturals to be printed."))
    if result: #User pressed "ok"
        if result["6"] == -10: #Bes included? Then use flat order.
            keysigtuplet = [(6, result["6"]), (2,result["2"]), (5,result["5"]), (1,result["1"]), (4,result["4"]), (0,result["0"]), (3,result["3"])]
        else: #Else use sharp order
            keysigtuplet = [(3,result["3"]), (0,result["0"]), (4,result["4"]), (1,result["1"]), (5,result["5"]), (2,result["2"]), (6,result["6"])]
        #keysigtuplet.reverse()
        _insertKeySignature(pitch.plain(lilypond.ly2pitch[result["root"]]), keysigtuplet, result["explicit"])

#TimeSignatures
def _insertTimeSignature(nominator, denominator, lilypond=None):
    timesig = items.TimeSignature(_getWorkspace(), nominator, denominator)
    if lilypond:
        timesig.lilypond = lilypond
    _insert(timesig)

def insertTime4_4():
    _insertTimeSignature(4, 384)

def insertTime2_2():
    _insertTimeSignature(2, 768)

def insertTime3_4():
    _insertTimeSignature(3, 384)

def insertTime6_8():
    _insertTimeSignature(6, 192)

def insertTime15_8():
    _insertTimeSignature(15, 192)

def insertTimeGui():
    data = {"0nominator":("Nominator", 3),
            "1denominator":("Denominator", ["4", ("1", _("1 Whole / Semibreve")), ("2", _("2 Half / Minim")), ("4", _("4 Quarter / Crotchet")), ("8", _("8 Eigth / Quaver")), ("16", _("16 Sixteenth / Semiquaver")), ("32", _("32 Thirthy-Second / Demisemiquaver")), ("64", _("64 Sixty-Fourth / Hemidemisemiquaver")), ("128", _("128 Hundred Twenty-Eighth / Semihemidemisemiquaver ")), ("256",_("256 Twohundred Fifty-Sixth / Demisemihemidemisemiquaver"))] ),
            }
    result = l_form.generateForm(data, _("Custom Time Signature"), _("Choose Nomiantors ('how many') and Denominators ('of which duration')."))
    if result:
        _insertTimeSignature(result["0nominator"], lilypond.ly2dur[str(result["1denominator"])])

#Clefs
#TODO: Ancient clefs are still missing
def _clef(clefString, octave = "", ):
    """Octave can be _8 or _15, ^8 or ^15"""
    clef = items.Clef(_getWorkspace(), clefString, octave)
    _insert(clef)
def insertClefSoprano():
    _clef("soprano")
def insertClefTreble():
    _clef("treble")
def insertClefAlto():
    _clef("alto")
def insertClefTenor():
    _clef("tenor")
def insertClefBass():
    _clef("bass")
def insertClefTreble_8():
    _clef("treble", "_8")
def insertClefTreble8():
    _clef("treble", "^8")
def insertClefBass_8():
    _clef("bass", "_8")
def insertClefBass8():
    _clef("bass", "^8")
def insertClefFrench():
    _clef("french")
def insertClefMezzosoprano():
    _clef("mezzosoprano")
def insertClefBaritone():
    _clef("baritone")
def insertClefVarbaritone():
    _clef("varbaritone")
def insertClefSubbass():
    _clef("subbass")
def insertClefPercussion():
    _clef("percussion")
def insertClefTab():
    _clef("tab")
def insertClefFakeDrum():
    """A bass clef that disguises as percussion clef. Mostly used for MIDI drum notation."""
    clef = items.Clef(_getWorkspace(), "fakeDrum", "")
    clef.lilypond = "\\clef \"percussion\" \\set Staff.middleCPosition = #6"
    _insert(clef)

def insertClefGui():
    functionlist = [
                    (insertClefTreble, _("Treble")),
                    (insertClefTreble_8, _("Treble_8")),
                    (insertClefTreble8, _("Treble^8")),
                    (insertClefAlto, _("Alto")),
                    (insertClefBass, _("Bass")),
                    (insertClefBass_8, _("Bass_8")),
                    (insertClefBass8, _("Bass^8")),
                    (insertClefSoprano,_("Soprano")),
                    (insertClefTenor, _("Tenor")),
                    (insertClefMezzosoprano, _("Mezzosoprano")),
                    (insertClefBaritone, _("Baritone")),
                    (insertClefVarbaritone, _("Varbaritone")),
                    (insertClefSubbass, _("Subbass")),
                    (insertClefPercussion, _("Percussion")),
                    (insertClefFrench, _("French")),
                    (insertClefTab, _("Tab")),
                    (insertClefFakeDrum, _("Drum (disguised Bass)")),
                    ]
    _generateDropDownGuiFromFunctions(functionlist, insertClefTreble, _("Clef"), title=_("Clef Chooser"), comment=_("Choose a Clef. Treble is the default if no clef was chosen."))

def daCapo(disableRepeat = False):
    _insert(items.GotoCapo(_getWorkspace(), disableRepeat))

def dalSegno(disableRepeat = False):
    _insert(items.GotoSegno(_getWorkspace(), disableRepeat))

def toCoda():
    _insert(items.GotoCoda(_getWorkspace(), ))

def segno():
    _insert(items.Segno(_getWorkspace(), ))

def fine():
    _insert(items.Fine(_getWorkspace(), ))

def coda():
    _insert(items.Coda(_getWorkspace(), ))

def alternateEnd(*args):
    if args:
        endings = list(args)
    else:
        endings = None
        ret = l_form.generateForm({"endings":(" ", "1 2")}, _("Insert Alternate End"), _("Enter alternate endings. Seperate with space."))
        if ret:
            endings = [int(x) for x in ret["endings"].split()]

    if endings:
        a = items.AlternateEnd(_getWorkspace(), )
        a.endings = endings
        _insert(a)

def alternateEndClose():
    _insert(items.AlternateEndClose(_getWorkspace(), ))

def alternateEndingFullSequence():
    insertBarRepeatOpen()
    insertRestM(1)
    alternateEnd(1)
    insertRestM(1)
    insertBarRepeatClose()
    alternateEnd(2)
    insertRestM(1)
    alternateEndClose()


#Chord Markings
#TODO: Most of them need midi instructions.
def _marking(lilyStringWithoutBackslash, playbackPre = None, playbackPst = None, tempoKeyword="", velocityKeyword=""):
    """It is important that you give the playback Instructions if you
    want to remove as well as add something. Always give them.
    If you create a new marking and want it to have an effect on
    playback you need to specify what kind of change it is.

    Velocity Keywords must be in the performance sig dynamic dict.
    """
    def temp():
        item = _getCursorItem()
        if type(item) is items.Chord:
            #Both use the same tag
            _toggleDirective(_lilyPst, lilyStringWithoutBackslash, "\\" + lilyStringWithoutBackslash)
            if playbackPre:
                _toggleDirective(__playbackPre, lilyStringWithoutBackslash, playbackPre, playbackPst)
            if playbackPst:
                _toggleDirective(__playbackPst, lilyStringWithoutBackslash, playbackPre, playbackPst)

            if item.tempoKeyword:
                if item.tempoKeyword == tempoKeyword: #If it is already the same we have a toggle. Remove the existing
                    item.tempoKeyword = ""
                elif tempoKeyword: #we have a new keyword. Overwrite the old
                    item.tempoKeyword = tempoKeyword
                #else we have no new keyword. do nothing
            else: #no current keyword. Use the parameter one. If there is no parameter it gets overwritten with an empty string.
                item.tempoKeyword = tempoKeyword

            if item.velocityKeyword:
                if item.velocityKeyword == velocityKeyword: #If it is already the same we have a toggle. Remove the existing
                    item.velocityKeyword = ""
                elif velocityKeyword: #we have a new keyword. Overwrite the old
                    item.velocityKeyword = velocityKeyword
                #else we have no new keyword. do nothing
            else: #no current keyword. Use the parameter one. If there is no parameter it gets overwritten with an empty string.
                item.velocityKeyword = velocityKeyword
    command(temp, signal = l_chord.extensions)


def toggleAccent():
    _marking("accent", velocityKeyword = "sfz")
def toggleEspressivo():
    _marking("espressivo")
def toggleMarcato():
    _marking("marcato")
def togglePortato():
    _marking("portato")
def toggleStaccatissimo():
    _marking("staccatissimo")
def toggleStaccato():
    _marking("staccato")
def toggleTenuto():
    _marking("tenuto")

def togglePrall():
    _marking("prall")
def toggleMordent():
    _marking("mordent")
def togglePrallmordent():
    _marking("prallmordent")
def toggleTurn():
    _marking("turn")

def toggleUpprall():
    _marking("upprall")
def toggleDownprall():
    _marking("downprall")
def toggleUpmordent():
    _marking("upmordent")
def toggleDownmordent():
    _marking("downmordent")

def toggleLineprall():
    _marking("lineprall")
def togglePrallprall():
    _marking("prallprall")
def togglePralldown():
    _marking("pralldown")
def togglePrallup():
    _marking("prallup")

def toggleReverseturn():
    _marking("reverseturn")
def toggleTrill():
    _marking("trill")

def toggleShortfermata():
    _marking("shortfermata", tempoKeyword="shortfermata")
def toggleFermata():
    _marking("fermata", tempoKeyword="fermata")
def toggleLongfermata():
    _marking("longfermata", tempoKeyword="longfermata")
def toggleVerylongfermata():
    _marking("verylongfermata", tempoKeyword="verylongfermata")

def toggleUpbow():
    _marking("upbow")
def toggleDownbow():
    _marking("downbow")

#TODO: BartokPizzicato is not native lilypond. Needs a custom graphic.
# def toggleSnappizzicato():
#    _marking("snappizzicato")
def toggleOpen():
    _marking("open")
def toggleStopped():
    _marking("stopped")
def toggleLheel():
    _marking("lheel")
def toggleRheel():
    _marking("rheel")
def toggleLtoe():
    _marking("ltoe")
def toggleRtoe():
    _marking("rtoe")

#Fake.
"""
def toggleFakeSegno():
    _marking("segno")
def toggleFakeCoda():
    _marking("coda")
def toggleFakeVarcoda():
    _marking("varcoda")
"""

##Ancient
def togglesSignumcongruentiae():
    _marking("signumcongruentiae")
def toggleIctus():
    _marking("ictus")
def toggleAccentus():
    _marking("accentus")
def toggleCirculus():
    _marking("circulus")
def toggleSemicirculus():
    _marking("semicirculus")
def toggleAugmentum():
    _marking("augmentum")

##Other
def toggleUndressWhilePlaying():
    _toggleDirective(_lilyPst, "undress", "^\\markup { \\italic \\small \"Undress while playing\" }") #Lily can also be midi data.

def toggleEyeglasses():
    _toggleDirective(_lilyPst, "eyeglasses", "^\\markup { \\eyeglasses }") #Lily can also be midi data.

#TODO: Research. Does not work. Maybe \fermataMarkup was a userscript?
"""def toggleFermataMarkup():
    _marking("fermataMarkup")"""
#TODO: pizz?


#Pedals
def _pedalProto(lilystring, midiPre, waitForChord = True):
    if waitForChord:
        item = items.WaitForChord(_getWorkspace(), lilystring)
    else:
        item = items.Item(_getWorkspace())
        item.lilypond = lilystring
    item.instructionPre["pedal"] = midiPre
    _insert(item)

def insertPedalSustainOn():
    """Right Pedal"""
    _pedalProto("\\sustainOn", [[0xB0, 0x40, 127], 0])

def insertPedalSustainOff():
    """Right Pedal"""
    _pedalProto("\\sustainOff", [[0xB0, 0x40, 0], -1], waitForChord = False)

def insertPedalSustainChange():
    _insert(items.PedalSustainChange(_getWorkspace(), ))

def insertPedalSostenutoOn():
    """Middle Pedal"""
    _pedalProto("\\sostenutoOn", [[0xB0, 0x42, 127], 0])

def insertPedalSostenutoOff():
    """Middle Pedal"""
    _pedalProto("\\sostenutoOff", [[0xB0, 0x42, 0], -1], waitForChord = False)

def insertPedalUnaCordaOn():
    """Left Pedal, aka. Soft Pedal"""
    _pedalProto("\\unaCorda", [[0xB0, 0x43, 127], 0])

def insertPedalUnaCordaOff():
    """Left Pedal, aka. Soft Pedal"""
    _pedalProto("\\unaCorda", [[0xB0, 0x43, 0], -1], waitForChord = False)



#Fingerings

def fingeringAllDelete():
    """Delete finger, rightHandfinger and string number"""
    def do():
        for n in _getNotes():
            n.finger = ""
            if "StringNumber" in n.directivePst:
                del n.directivePst["StringNumber"]
            if "RightHandFinger" in n.directivePst:
                del n.directivePst["RightHandFinger"]
    command(do, signal = l_item.update)


def finger(numberOrString = None):
    """A number or t or \\thumb for thumb"""
    if not numberOrString:
        ret = l_form.generateForm({"finger" : (" ", _getClosestNoteToCursor().finger)}, _("Finger"), _("Give a finger for this note.\nLeave/Make empty to delete.\nUse the pipe '|' for more a change.\nUse t for thumb.\nLilypond Syntax. Example: '4|t'"))
        if ret:
            numberOrString = ret["finger"]

    if numberOrString or numberOrString == "": #gui and user did not cancel. Empty string still True
        def do(numberOrString=numberOrString):
            numberOrString = ''.join(str(numberOrString).split())
            _getClosestNoteToCursor().finger = numberOrString
        command(do, signal = l_note.finger)
        return True
    else:
        return False

def insertFingeringDirection(directionString = None):
    """parameter is a combination of the keywords left, right, up down.
    This is also the priority sorting.
    Example: "down right up" """

    if not directionString:
        ret = l_form.generateForm({"dir" : (" ", "")}, _("Fingering Direction"), _("To control how the fingerings are placed combine the words:\nup down left right"))
        if ret:
            directionString = ret["dir"]

    if directionString: #gui and user did not cancel?
        _standalone("\\set fingeringOrientations = #'(" + directionString + ")")
        return True
    else:
        return False


def toggleFingeringOverrideAllowInStaff():
    _toggleDirective(_lilyPre, "FingeringAllowInStaff", "\\once \\override Fingering #'staff-padding = #'()")

def insertFingeringOverrideAddStemSupport():
    _standalone("\\override Fingering #'add-stem-support = ##t")

def insertFingeringOverrideDisableStemSupport():
    _standalone("\\override Fingering #'add-stem-support = ##f")

#String Numbers
def stringNumber(numberOrString = None):
    """A number or t or \\thumb for thumb"""
    if not numberOrString:
        tmp = _lilyNotePst("StringNumber")
        if tmp:
            tmp = tmp["StringNumber"].split("\\")[-1]
        else:
            tmp = ""
        ret = l_form.generateForm({"stringNumber" : (" ", tmp )}, _("String Number"), _("Give a string number for this note.\nLeave/Make empty to delete."))
        if ret:
            numberOrString = ret["stringNumber"]

    if numberOrString or numberOrString == "": #gui and user did not cancel. Empty string still True, but not "None"
        def do(numberOrString=numberOrString):
            numberOrString = ''.join(str(numberOrString).split())
            if numberOrString:
                _lilyNotePst("StringNumber", lily= "\\" + numberOrString)
            else: # empty string
                _deleteNoteDirectives("StringNumber")
        command(do, signal = l_note.finger)
        return True
    else:
        return False

def toggleStringNumberOverrideAllowInStaff():
    _toggleDirective(_lilyPre, "StringNumberAllowInStaff", "\\once \\override StringNumber #'staff-padding = #'()")

def insertStringNumberAddStemSupport():
    _standalone("\\override StringNumber #'add-stem-support = ##t")

def insertStringNumberDisableStemSupport():
    _standalone("\\override StringNumber #'add-stem-support = ##f")

#Right Hand Fingering, Stroke Finge, p-i-m-a
def rightHandFinger(numberOrString = None):
    """Right hand fingers are chars: p, i, m, a. 1-4.
    And >= 5 is "x"""
    if not numberOrString:
        tmp = _lilyNotePst("RightHandFinger")
        if tmp:
            tmp = tmp["RightHandFinger"].split("#")[-1]
        else:
            tmp = ""
        ret = l_form.generateForm({"rightHandFinger" : (" ", tmp )}, _("Right Hand Finger"), _("Right Hand Finger or Stroke Finger.\n1=p, 2=i, 3=m, 4=a, >4=x\n\nLeave/Make empty to delete."))
        if ret:
            numberOrString = ret["rightHandFinger"]

    if numberOrString or numberOrString == "": #gui and user did not cancel. Empty string still True, but not "None"
        def do(numberOrString=numberOrString):
            numberOrString = ''.join(str(numberOrString).split())
            if numberOrString:
                _lilyNotePst("RightHandFinger", lily= "-\\rightHandFinger #" + numberOrString + " ")#the tailing space is very important.
            else: # empty string
                _deleteNoteDirectives("RightHandFinger")
        command(do, signal = l_note.finger)
        return True
    else:
        return False

def toggleRightHandFingerOverrideAllowInStaff():
    _toggleDirective(_lilyPre, "StringNumberAllowInStaff", "\\once \\override StrokeFinger #'staff-padding = #'()")

def insertRightHandFingerAddStemSupport():
    """Lilypond term: StrokeFinger"""
    _standalone("\\override StrokeFinger #'add-stem-support = ##t")

def insertRightHandFingerDisableStemSupport():
    """Lilypond term: StrokeFinger"""
    _standalone("\\override StrokeFinger #'add-stem-support = ##f")

def insertRightHandFingeringDirection(directionString=None):
    """Same as left hand Fingering style.:
    parameter is a combination of the keywords left, right, up down.
    This is also the priority sorting.
    Example: "down right up" """
    if not directionString:
        ret = l_form.generateForm({"dir" : (" ", "")}, _("Fingering Direction"), _("To control how the fingerings are placed combine the words:\nup down left right"))
        if ret:
            directionString = ret["dir"]

    if directionString: #gui and user did not cancel?
        _standalone("\\set strokeFingerOrientations = #'(" + directionString + ")")
        return True
    else:
        return False

#Beaming
def toggleBeamOpen():
    _toggleDirective(_lilyPst, "beam", "[")

def toggleBeamClose():
    _toggleDirective(_lilyPst, "beam", "]")

def toggleBeamForceNone():
    _toggleDirective(_lilyPst, "beam", "\\noBeam")

def toggleBeamForce():
    _toggleDirective(_lilyPst, "beam", "[]")

#Ties
#TODO: midi instructions, pre and pst for switching on and off legato
#def toggleTie():
#    _toggleDirective(_lilyPst, "tie", "~")
def toggleLaissezVibrer():
    ret = _toggleDirective(_lilyPst, "tie", "\\laissezVibrer")
#    now for midi:
    if ret: #directive was created
        for note in _getCursorItem().notelist:
            note.tie = "\\laissezVibrer"  #the backend is smart enough to interpret this
    else: #directive was deleted
         for note in _getCursorItem().notelist:
            note.tie = ""
    l_send(lambda: l_item.update(_getCursorItem()))

#Ties for Chords are too unpredictable in behaviour, especially for midi.
#You need immensive parsing and backward/forward probing to make these things
#good. Lilypond can do that, I won't. The user has to work with note ties
#alone.


def toggleNoteTie():
    """Tie for a single note which may be part of a chord.
    There is no difference for just one note, so use Item for one note."""
    def single():
        i = _getCursorItem()
        saveState(i, l_item.update ,False)
        n = _getClosestNoteToCursor()
        if n.tie:
            n.tie = ""
        else:
            n.tie = "~"

    def chord():
        new = "" if all(note.tie for note in _getNotes()) else "~" #True means all notes have ties
        for n in _getNotes():
            n.tie = new

    command(chord, single, signal = l_item.update)

def repeatTie():
    _toggleDirective(_lilyPst, "tie", "\\repeatTie")

#def toggleNoteLaissezVibrer():
#    """Endless tie for a single note which may be part of a chord.
#    There is no difference for just one note, so use Item for one note."""
#    _getClosestNoteToCursor().tie = "\\laissezVibrer"
#    #_toggleDirective(_lilyNotePst, "tie", "\\laissezVibrer")

def insertTieStyleUp():
    _standalone("\\tieUp")
def insertTieStyleDown():
    _standalone("\\tieDown")
def insertTieStyleNeutral():
    _standalone("\\tieNeutral")
def insertTieStyleDotted():
    _standalone("\\tieDotted")
def insertTieStyleDashed():
    _standalone("\\tieDashed")
def insertTieStyleSolid():
    _standalone("\\tieSolid")
def insertTieSetStyleWaitForNoteOn():
    _standalone("\\set tieWaitForNote = ##t")
def insertTieSetStyleWaitForNoteOff():
    _standalone("\\set tieWaitForNote = ##f")

#Breathing Signs
#TODO: Midi would be something like shorten the previous note or a little tempo variance. one tiny moment everything is slower.
def _breathingSign(override=None):
    """Any markup can be used as glyph. Here are some examples
    Lilypond output will be extremly ugly, but flexibility is higher
    than using standalone directive items"""
    #TODO: Provide a GUI command where the user can specify the glyph.
    if override:
        _standalone("\\once \\override BreathingSign #'text = \\markup { \\musicglyph #\"" + override + "\" } \\breathe")
    else:
        _standalone("\\breathe")

def _insertBreathe():
    _breathingSign()

def insertBreatheRvarComma():
    _breathingSign("scripts.rvarcomma")

def insertBreatheUpbow():
    _breathingSign("scripts.upbow")

def insertBreatheCaesuraCurved():
    _breathingSign("scripts.caesura.curved")


#Slurs
def insertSlurOpen():
    _insert(items.SlurOn(_getWorkspace(), ))

def insertSlurClose():
    _insert(items.SlurOff(_getWorkspace(), ))

def insertPhrasingSlurOpen():
    _insert(items.PhrasingSlurOn(_getWorkspace(), ))

def insertPhrasingSlurClose():
    _insert(items.PhrasingSlurOff(_getWorkspace(), ))

def insertSlurStyleUp():
    _standalone("\\slurUp")
def insertSlurStyleDown():
    _standalone("\\slurDown")
def insertSlurStyleNeutral():
    _standalone("\\slurNeutral")
def insertSlurStyleDashed():
    _standalone("\\slurDashed")
def insertSlurStyleDotted():
    _standalone("\\slurDotted")
def insertSlurStyleSolid():
    _standalone("\\slurSolid")
def insertSlurStyleDoubleOn():
    _standalone("\\set doubleSlurs = ##t")
def insertSlurStyleDoubleOff():
    _standalone("\\set doubleSlurs = ##f")

def insertPhrasingSlurStyleUp():
    _standalone("\\phrasingSlurUp")
def insertPhrasingSlurStyleDown():
    _standalone("\\phrasingSlurDown")
def insertPhrasingSlurStyleNeutral():
    _standalone("\\phrasingSlurNeutral")
def insertPhrasingSlurStyleDashed():
    _standalone("\\phrasingSlurDashed")
def insertPhrasingSlurStyleDotted():
    _standalone("\\phrasingSlurDotted")
def insertPhrasingSlurStyleSolid():
    _standalone("\\phrasingSlurSolid")

#Barline
#TODO: Some of these barline need to be core classes because they reset the metrical position.

def _barline(lilystring):
    _standalone("\\bar \"" + lilystring + "\"")

def insertBarInvisible():
    """An incorrect duration can cause line breaks to be inhibited,
    leading to a line of highly compressed music or music which flows
    off the page. Line breaks are also permitted at manually inserted
    bar lines even within incomplete measures. To allow a line break
    without printing a bar line, use \bar \"\""""
    _barline("")
def insertBarNormal():
    """Force Barline"""
    _barline("|")
def insertBarDouble():
    _barline("||")
def insertBarOpen():
    _barline(".|")
def insertBarEnd():
    _barline("|.")
def insertBarOpenEnd():
    _barline(".|.")
def insertBarEndOpen():
    _barline("|.|")
def insertBarDotted():
    _barline(":")
def insertBarDashed():
    _barline("dashed")
def insertBarHalf():
    _standalone("\\once \\override Staff . BarLine #'bar-size = #2 \\bar \"|\"")

#Repeats
def _repeatOpen(lilystring):
    new = items.RepeatOpen(_getWorkspace(), )
    new.lilypond = "\\bar \"" + lilystring + "\""
    _insert(new)

def repeatClose(lilystring):
    new = items.RepeatClose(_getWorkspace(), )
    new.lilypond = "\\bar \"" + lilystring + "\""
    _insert(new)

def repeatCloseOpen(lilystring):
    new = items.RepeatCloseOpen(_getWorkspace(), )
    new.lilypond = "\\bar \"" + lilystring + "\""
    _insert(new)

def insertBarRepeatSpecialOpen():
    """On Staffbreak: Outputs a double barline on the end of a line and the open repeat on the next"""
    _repeatOpen("||:")
def insertBarRepeatOpen():
    _repeatOpen("|:")
def insertBarRepeatClose():
    repeatClose(":|")
def insertBarRepeatCloseOpen():
    repeatCloseOpen(":|:")
def insertBarRepeatClose_EndOpen_RepeatOpen():
    repeatCloseOpen(":|.|:") #This is better looking than RepeatCloseOpen
def insertBarRepeatClose_End_RepeatOpen():
    repeatCloseOpen(":|.:")

#Dynamics

def _insertDynamic(expression, lilystring):
    _insert(items.DynamicSignature(_getWorkspace(), expression, lilystring))

def insertDynamicPPPPP():
    _insertDynamic("ppppp", "\\ppppp")

def insertDynamicPPPP():
    _insertDynamic("pppp", "\\pppp")

def insertDynamicPPP():
    _insertDynamic("ppp", "\\ppp")

def insertDynamicPP():
    _insertDynamic("pp", "\\pp")

def insertDynamicP():
    _insertDynamic("p", "\\p")

def insertDynamicMP():
    _insertDynamic("mp", "\\mp")

def insertDynamicMF():
    _insertDynamic("mf", "\\mf")

def insertDynamicF():
    _insertDynamic("f", "\\f")

def insertDynamicFF():
    _insertDynamic("ff", "\\ff")

def insertDynamicFFF():
    _insertDynamic("fff", "\\fff")

def insertDynamicFFFF():
    _insertDynamic("ffff", "\\ffff")

def insertDynamicUser1():
    _insertDynamic("user1", "")

def insertDynamicUser2():
    _insertDynamic("user2", "")

def insertDynamicUser3():
    _insertDynamic("user3", "")

def insertDynamicTacet():
    _insertDynamic("tacet", "^\\markup{Tacet}")

def _insertSubitoDynamic(expression, lilystring):
    """only for the next chord"""
    _insert(items.SubitoDynamicSignature(_getWorkspace(), expression, lilystring))

#\fp, \sf, \sff, \sp, \spp, \sfz, and \rfz

def insertSubitoDynamicFP():
    _insertSubitoDynamic("fp", "\\fp") #TODO: this is not trivial to implement as playback.

def insertSubitoDynamicSF():
    _insertSubitoDynamic("sf", "\\sf")

def insertSubitoDynamicSFF():
    _insertSubitoDynamic("sff", "\\sff")

def insertSubitoDynamicSP():
    _insertSubitoDynamic("sp", "\\sp")

def insertSubitoDynamicSPP():
    _insertSubitoDynamic("spp", "\\spp")

def insertSubitoDynamicSFZ():
    _insertSubitoDynamic("sfz", "\\sfz")

def insertSubitoDynamicRFZ():
    _insertSubitoDynamic("rfz", "\\rfz")

#dynamic changes

def insertDynamicCrescendo():
    _standaloneWaitForNextChord("\\cr")

def insertDynamicDecrescendo():
    _standaloneWaitForNextChord("\\decr")

def insertDynamicEndCrescendo():
    """A hack to make sure that an \! end can even be placed after a
    non-chord item. Otherwise lilypond would break because it
    expects \! after a note/chord. We can use \! as standalone item :)"""
    _standalone("s4*0 \\!")

insertDynamicEndDecrescendo = insertDynamicEndCrescendo

def insertDynamicStyleUp():
    _standalone("\\dynamicUp")

def insertDynamicStyleDown():
    _standalone("\\dynamicDown")

def insertDynamicStyleNeutral():
    _standalone("\\dynamicNeutral")

def insertDynamicStyleCrescendoText_Cresc():
    _standalone("\\crescTextCresc")

def insertDynamicStyleDecrescendoText_Dim():
    _standalone("\\dimTextDim")

def insertDynamicStyleDecrescendoText_Decr():
    _standalone("\\dimTextDecr")

def insertDynamicStyleDecrescendoText_Decresc():
    _standalone("\\dimTextDecresc")

def insertDynamicStyleCrescondoHairpin():
    _standalone("\\crescHairpin")

def insertDynamicStyleDecrescondoHairpin():
    _standalone("\\dimHairpin")

def insertPerformanceSignature():
    """Insert a default signature. Nothing special"""
    _insert(items.PerformanceSignature(parentWorkspace = _getWorkspace()))

def insertInstrumentChange():
    """Insert a default signature. Nothing special"""
    _insert(items.InstrumentChange(parentWorkspace = _getWorkspace()))

#Fine tuning duration and velocity for whole chords. No note finetuning via the api. We don't want to encourage users to work on a single tracks when it belongs to different tracks

def durationFactorPlus(value=0.1):
    def do():
        _getCursorItem().durationFactor += value
    command(do, signal= l_item.updateDynamic) #duration changed, but not for the internal logic and the gui. No durationChanged = True

def durationFactorMinus(value=0.1):
    def do():
        _getCursorItem().durationFactor -= value
    command(do, signal= l_item.updateDynamic) #duration changed, but not for the internal logic and the gui.No durationChanged = True

def resetDurationFactor(value=1):
    """doubles as 'reset to' """
    def do():
        _getCursorItem().durationFactor = value
    command(do, signal= l_item.updateDynamic) #duration changed, but not for the internal logic and the gui.No durationChanged = True

def velocityFactorPlus(value=0.1):
    def do():
        _getCursorItem().velocityFactor += value
    command(do, signal= l_item.updateDynamic)

def velocityFactorMinus(value=0.1):
    def do():
        _getCursorItem().velocityFactor -= value
    command(do, signal= l_item.updateDynamic)

def resetVelocityFactor(value=1):
    """doubles as 'reset to' """
    def do():
        _getCursorItem().velocityFactor = value
    command(do, signal= l_item.updateDynamic)

def smfChannelPlus():
    def do():
        _getCursorItem().channelOffset += 1
    command(do, signal = l_item.smfChannel)

def smfChannelMinus():
    def do():
        _getCursorItem().channelOffset -= 1
    command(do, signal = l_item.smfChannel)

def smfChannelReset():
    """Reset all channel forces and offsets"""
    def do():
        i = _getCursorItem()
        i.channelOffset = 0
        i.forceChannel = -1 #-1 means default in the backend
    command(do, signal = l_item.smfChannel)

def smfChannelForce(value=None):
    def do():
        _getCursorItem().forceChannel = value
    if not value:
        ret = l_form.generateForm({"channel" : (" ", _getCursorItem().forceChannel+1)}, _("Force Midi Channel"), _("Choose an absolute midi channel number (1-16).\nZero or Negative numbers mean back to default value."))
        if ret:
            value = ret["channel"] -1
    if not value == None: #accept anything but None. negative numbers and 0 are ok.
        command(do, signal = l_item.smfChannel)
        return True
    else:
        return False

def insertChannelChangeRelative(value = None):
    """A standalone item. Changes the channel from here to the right"""
    if not value:
        ret = l_form.generateForm({"channel":(" ", 0)}, _("Insert Relative Channel Change"), _("Give a number, negative or positve,\n which gets added to the midi channel for notes from here on."))
        if ret or ret == 0:
            value = ret["channel"]
        else:
            return False

    #now there is a value.
    i = items.ChannelChangeRelative(_getWorkspace(), )
    i.number = value
    _insert(i)

def insertChannelChangeAbsolute(value = None):
    """A standalone item. Changes the channel from here to the right"""
    if not value:
        ret = l_form.generateForm({"channel":(" ", 1)}, _("Insert Absolute Channel Change"), _("Give a number 1-16,\n which is the new midi channel for notes from here on."))
        if ret or ret == 0:
            value = ret["channel"] -1
        else:
            return False

    #now there is a value.
    i = items.ChannelChangeAbsolute(_getWorkspace(), )
    i.number = value
    _insert(i)

def smfProgramPlus():
    """Results in a program change before and back after the note"""
    def do():
        _getCursorItem().programOffset += 1
    command(do, signal = l_item.smfProgram)

def smfProgramMinus():
    """Results in a program change before and back after the note"""
    def do():
        _getCursorItem().programOffset -= 1
    command(do, signal = l_item.smfProgram)

def smfProgramReset():
    """Reset all program forces and offsets"""
    def do():
        i = _getCursorItem()
        i.programOffset = 0
        i.forceProgram = -1 #-1 means default in the backend
    command(do, signal = l_item.smfProgram)

def smfProgramForce(value=None):
    """Set to 1 to 128"""
    def do():
        _getCursorItem().forceProgram = value
    if not value:
        ret = l_form.generateForm({"program" : (" ", _getCursorItem().forceProgram+1)}, _("Force Midi Program"), _("Choose an absolute midi program number (1-128).\nZero or Negative numbers mean back to default value."))
        if ret:
            value = ret["program"] -1
    if not value == None: #accept anything but None. negative numbers and 0 are ok.
        command(do, signal = l_item.smfProgram)
        return True
    else:
        return False

def insertProgramChangeRelative(value = None):
    """A standalone item. Changes the program from here to the right"""
    if not value:
        ret = l_form.generateForm({"program":(" ", 0)}, _("Insert Relative Program Change"), _("Give a number, negative or positve,\n which gets added to the midi program for notes from here on."))
        if ret or ret == 0:
            value = ret["program"]
        else:
            return False

    #now there is a value.
    i = items.ProgramChangeRelative(_getWorkspace(), )
    i.number = value
    _insert(i)

def insertProgramChangeAbsolute(value = None):
    """A standalone item. Changes the program from here to the right"""
    if not value:
        ret = l_form.generateForm({"program":(" ", 1)}, _("Insert Absolute Program Change"), _("Give a number 1-128,\n which is the new midi program for notes from here on."))
        if ret or ret == 0:
            value = ret["program"] -1
        else:
            return False

    #now there is a value.
    i = items.ProgramChangeAbsolute(_getWorkspace(), )
    i.number = value
    _insert(i)


def _insertTempo(bpm, referenceTick, tempostring=""):
    tempo = items.TempoSignature(_getWorkspace(), bpm,referenceTick)
    tempo.tempostring = tempostring
    _insert(tempo)

def insertTempo(bpm=None, referenceTick=None, tempostring=""):
    if not (bpm or referenceTick):
        if tempostring:
            tmpstr = tempostring
        else:
            tmpstr = _getActiveCursor().prevailingTempoSignature[-1].tempostring
        data = {"01bpm": (_("Beats per Minute"), 120),
            "02reference": (_("Reference Note"), [384, (1536*4, _("1 Longa")),(1536*2, _("1 Whole / Semibreve")),(1536, _("1 Whole / Semibreve")), (768, _("2 Half / Minim")), (384, _("4 Quarter / Crotchet")), (192, _("8 Eigth / Quaver")), (96, _("16 Sixteenth / Semiquaver")), (48, _("32 Thirthy-Second / Demisemiquaver")), (24, _("64 Sixty-Fourth / Hemidemisemiquaver")), (12, _("128 Hundred Twenty-Eighth / Semihemidemisemiquaver ")), (6, _("256 Twohundred Fifty-Sixth / Demisemihemidemisemiquaver"))]),
            "02z" : (None, _("Optional:")),
            "03dotted": (_("Dotted"), False),
            "04tempostring": (_("Tempo string"), tmpstr),
            }
        ret = l_form.generateForm(data, _("Tempo Signature"), _("Choose how many (bpm) of which note (reference) per minute. Optional string for printing."))
        if ret:
            bpm = ret["01bpm"]
            referenceTick = ret["02reference"]
            if ret["03dotted"]:
                referenceTick *= 1.5
            tempostring = ret["04tempostring"]
    if bpm and referenceTick:
        _insertTempo(bpm, referenceTick, tempostring)
        return True
    else:
        return False

def insertTempoModification(bpm = 0):
    _insert(items.TempoModification(bpm))


def markup(text=""):
    """Create a new markup"""
    if not text:
        ret = l_form.generateForm({"lilystring":(_("Text"), ""), "position":(_("Position"), ["^", ("^", _("above")), ("_", _("under"))])}, _("Free Text"), _("Enter any text and choose above or under the track.\n\nYou can use Lilypond markup syntax"))
        if ret: #don't accept empty strings. This item can easily be deleted.
            text= ret["lilystring"]
            pos = ret["position"]
    if ret and pos: #canceled?
        new = items.Markup(_getWorkspace(), text)
        new.position = pos
        _insert(new)


#The next four functions only change one track in the GUI but we assume you need to change the track in a GUI which triggers the next trackChanged
def unsetMute():
    for tr in _getScore():
        tr.smfMute = False
    l_send(l_score.trackChanged)
def invertMute():
    for tr in _getScore():
        tr.smfMute = not tr.smfMute
    l_send(l_score.trackChanged)
def unsetSolo():
    for tr in _getScore():
        tr.smfSolo = False
    l_send(l_score.trackChanged)
def invertSolo():
    for tr in _getScore():
        tr.smfSolo = not tr.smfSolo
    l_send(l_score.trackChanged)

def unsetSoloMute():
    unsetSolo()
    unsetMute()

#NotationMagick

def randomPitch(lowest, highest, mode = 2):
    """lowest is a pitch value like 1420
    Modes:
    -1 = full spectrum, reserved for microintervals
    0 = enharmonic spectrum.
    1 = without double-b and x
    2 = in Scale, default"""

    note =  random.randrange(lowest, highest, 10) #step-size 10
    if mode == 2:
        return pitch.toScale(note, _getKeysig())
    elif mode == 1:
        #A conversion to midi which is just an abstract frequency and back to the current keysig
        a = pitch.toMidi(note)
        b = pitch.midiToPitch(pitch.toMidi(note), _getKeysig())
        return pitch.midiToPitch(pitch.toMidi(note), _getKeysig())
    else:
        return note

def randomBaseDuration(lowest, highest):
    """Choose a base duration randomly""" #TODO. What about tuplets? Dots?  Or better base duration and the rest is done by a higher level that can control that tuplets come in a row etc.
    l = math.log(lowest/6, 2)
    h = math.log(highest/6, 2) + 1
    ret = 6 * 2**(random.randrange(l, h))
    return ret

def randomPitchAuthenticModeCursor():
    return randomPitch(_getCursorPitch(), _getCursorPitch() + 350, mode = 2)

def randomPitchHypoModeCursor():
    return pitch.toScale(pitch.intervalDown(randomPitch(_getCursorPitch(), _getCursorPitch() + 350, mode = 2), (-1,0)), _getKeysig()) #4th in the same octave

def rearrangeReverse():
    if copy("magick"):
        startIndex = getCursorFlatPosition() #this function does not change the index so it is safe to return to the old index afterwards.
        for tr in core.clipboard.clipboard["magick"]:
            tr.reverse()
        pasteReplaceSelection("magick", selectNewItems = True)
        cursorFlatPosition(startIndex)
        del core.clipboard.clipboard["magick"]
        del core.clipboard.linkboard["magick"]
    else:
        return False

def rearrangeShuffle():
    if copy("magick"):
        startIndex = getCursorFlatPosition() #this function does not change the index so it is safe to return to the old index afterwards.
        for tr in core.clipboard.clipboard["magick"]:
            random.shuffle(tr)  #in place
        pasteReplaceSelection("magick", selectNewItems = True)
        cursorFlatPosition(startIndex)
        del core.clipboard.clipboard["magick"]
        del core.clipboard.linkboard["magick"]
    else:
        return False

def rearrangeSortDescending():
    if copy("magick"):
        startIndex = getCursorFlatPosition() #this function does not change the index so it is safe to return to the old index afterwards.
        for tr in core.clipboard.clipboard["magick"]:
            tr.sort(key=lambda item: item.notelist[0].pitch) #in place
            tr.reverse()
        pasteReplaceSelection("magick", selectNewItems = True)
        cursorFlatPosition(startIndex)
        del core.clipboard.clipboard["magick"]
        del core.clipboard.linkboard["magick"]
    else:
        return False

def rearrangeSortAscending():
    if copy("magick"):
        startIndex = getCursorFlatPosition() #this function does not change the index so it is safe to return to the old index afterwards.
        for tr in core.clipboard.clipboard["magick"]:
            tr.sort(key=lambda item: item.notelist[0].pitch) #in place
        pasteReplaceSelection("magick", selectNewItems = True)
        cursorFlatPosition(startIndex)
        del core.clipboard.clipboard["magick"]
        del core.clipboard.linkboard["magick"]
    else:
        return False

def reversePitch():
    """Takes all chords in the selection and reverts only the pitch
    leaving duration and other parameters intact"""
    def test(item):
        return type(item) is items.Chord

    if copy("magick"):
        core.clipboard.filterClipboard("magick", test)
        gen = core.clipboard.generatorReverseClipboard("magick")

        def do():
            item = _getCursorItem()
            if test(item):
                item.notelist = next(gen).notelist

        command(do, singleCommand = nothing, signal = l_item.updateDynamic)
    else:
        return False

def reverseRhythm():
    """Takes all chords in the selection and reverts only the pitch
    leaving duration and other parameters intact"""
    def test(item):
        return type(item) is items.Chord

    if copy("magick"):
        core.clipboard.filterClipboard("magick", test)
        gen = core.clipboard.generatorReverseClipboard("magick")

        def do():
            item = _getCursorItem()
            if test(item):
                item.putDurations(next(gen).getDurations())

        command(do, singleCommand = nothing, signal = l_item.updateDynamic, durationChanged = True)
    else:
        return False

#Substitutions
#The substitutions themselves are not here but in substitutiondatabase.py and saved in each score.
def substitution(keyword):
    def do():
        i = _getCursorItem()
        if type(i) is items.Chord:
            i.substitution = keyword
    command(do, signal = l_chord.substitutionMarker)

""" #The gui did use the the cursor position in the end.
def substitutionRemote(item, keyword):
    if type(item) is items.Chord:
        item.substitution = keyword
        l_send(l_item.updateDynamic(item)
"""

def substitutionDelete():
    """Delete the substitution string from the current chord"""
    def do():
        i = _getCursorItem()
        if isinstance(i, core.Chord):
            i.substitution = None
    command(do, signal = l_chord.substitutionMarker)

#All substitutions have to go through these two.
def substitutionsScoreAdd(key, container):
    """Add a new subst. to this score.
    Silently overwrite if key exists."""
    _getScore().substitutions[key] = container
    l_send(l_score.substitutionsChanged)


def substitutionsScoreDelete(key = None):
    if not key:
        _deleteFromDictWithGui(_getScore().substitutions, _("Delete Substitution from this score"), _("Choose which Substitution to delete.\n\nChoose 'Delete All' to override\nall individual choices and\nsimply delete all substitutions."))
        l_send(l_score.substitutionsChanged)
    else:
        del _getScore().substitutions[key]
        l_send(l_score.substitutionsChanged)

def substitutionScoreFromSelection(key = None, unfold = True):
    if not key:
        ret = l_form.generateForm({"01Name" : (_("Name"),""), "02Group" : (_("Group (optional)"), ""), "03Unfold" : (_("Unfold"), True)}, _("Create Substitution from Selection"), _("Choose a name and an optional group name.\n Uncheck unfold if you don't want Lilypond output, only Playback."))
        if ret:
            key = ret["01Name"]
            group = ret["02Group"]
            unfold = ret ["03Unfold"]
    if key: #gui and user did not cancel.
        cont = containerFromSelection()
        cont.unfold = unfold
        cont.group = group
        cont._uniqueContainerName = key
        substitutionsScoreAdd(key, cont)

def substitutionsScoreFromFile(filepath):
    """Load a Laborejo save file and for any track create a substitution
    with the unique trackname as key and the inverted track.hideLilypond
    as unfold. track.group is used as substitution group name."""
    tempWorkspace = saveload.load(filepath)
    tempWorkspace.score.removeEmptyTracks()
    for track in tempWorkspace.score.container:
        newCont = core.Container(_getWorkspace())
        if track.container[0] is items.Start:
            newCont.container = track.container
        else:
            newCont.container = newCont[:1] + track.container + newCont[-1:] #Start() and End()
        newCont.unfold = not track.hideLilypond
        newCont.group = track.group
        newCont._uniqueContainerName = track.uniqueContainerName
        _getScore().substitutions[track.uniqueContainerName] = newCont
    l_send(l_score.substitutionsChanged)

def substitutionsScoreToFile(filepath = None):
    tmpSubst = _getScore().substitutions
    new(filepath) #also changes all active cursors and such to the new file.
    for key, container in tmpSubst.items():
        tr = _getTrack()
        tr.hideLilypond = not container.unfold
        tr.group = container.group
        tr._uniqueContainerName = container.uniqueContainerName
        for item in [i for i in container.container[1:-1]]:
            _pasteItem(item, standaloneInsert = True)
        trackAdd()
    if not _getTrackIndex() == 0:
        trackDelete(justDelete = True) #remove the last track which is empty. No warning for the gui.

    if filepath:
        save(filepath)

def substitutionUnfold():
    """Replace an item with its substitution permanently.
    This means deleting the original note and pasting new notes
    from the container."""
    def do(i):
        replaceCont = i.unfoldSubstitution(_getKeysig())
        for item in replaceCont:
            _insert(item)

    if _getScore().selection:  #there is at least one selected item?
        def test(item, cursor):
            if item.selected and type(item) is items.Chord and item.substitution:
                return True
        replace(test, lambda item: do(item), selectNewItems = False) #don't select the new items by replace. We do it ourselves.
    else:
        def tmp():
            i = _getCursorItem()
            if i.substitution:
                delete()
                do(i)
        command(tmp)

#Playback Trigger and Switches
def triggerOnlyFirstTime():
    def do():
        _getCursorItem().triggerString = "if self.playbackCounter > 1: ret = False"
    command(do, signal = l_item.smfTrigger)


def tiggerNeverFirstTime():
    def do():
        _getCursorItem().triggerString = "if self.playbackCounter == 1: ret = False"
    command(do, signal = l_item.smfTrigger)

def triggerDelete():
    def do():
        _getCursorItem().triggerString = None
    command(do, signal = l_item.smfTrigger)


"""
#Undo and Redo
def undo():
    mov = _getScore()
    if mov.snapshots:
        mov.oldSnapshots.append(mov.snapshotCreate()) #save the current state on the redo stack. oldSnapshots is cleared very often here in api.
        snap = mov.snapshots.pop()
        mov.snapshotLoad(snap)
        l_send(lambda: l_score.loadSnapshot(snap[0]))
        return True
    else:
        return False

def redo():
    mov = _getScore()
    if mov.oldSnapshots: #no data change since the last undo?
        snap = mov.oldSnapshots.pop()
        mov.snapshotLoad(snap)
        l_send(lambda: l_score.loadSnapshot(snap[0]))
        return True
    else:
        return False

def take():
    #Every data insert/delete/mod function must use this.
    #Functions can turn off the snapshot creation for their child
    #functions.
    global compoundTake
    if not compoundTake: #if there is a snapshot already recorded don't do a second.
        mov = _getScore()
        mov.oldSnapshots = [] #no redo possible
        identifier = mov.snapshotSave() #returns a uuid
        l_send(lambda: l_score.saveSnapshot(identifier))

"""

def saveState(item, signal, durationChanged):
    """A state is a tuple which consists of
    [0]flatCursorposition, itself a tuple (trackIndex, flatCursorIndex)
    [1]the item
    [2]the signal, which implies what changes need to be send to the gui.
    [3] duration changed or not. Important for gui signals.
    This assumes that the reversed process of a command is really the
    reverse for the gui. """
    #print ("[State]", getCursorFlatPosition(), item, signal, durationChanged, len(item.notelist))
    global notDuringUndo
    if notDuringUndo:
        if item: #"save" state uses None.
            item.saveState()
        _getScore().stateRegistry.append((getCursorFlatPosition(), item, signal,durationChanged))

def undo():
    """A state is a tuple which consists of
    [0]flatCursorposition, itself a tuple (trackIndex, flatCursorIndex)
    [1]the item
    [2]the signal, which implies what changes need to be send to the gui.
    This assumes that the reversed process of a command is really the
    reverse for the gui.
    [3] duration changed or not. Important for gui signals.

    This assumes that the reversed process of a command is really the
    reverse for the gui.

    The main principle is that the GUI should not have anything to do
    with undo. Augment a note is just a transformation with a signal.
    Undo is the same for the gui. It looses the special meaning that
    we already were at that state once.

    Eventhough most of the time it may be not required to return to the
    cursor in some cases it is required. Plus it creates a better
    user experience because you see where the undo happened. And
    it IS the place where you edited, back then.
    """
    def subUndo(state):
        global notDuringUndo
        cursorFlatPosition(state[0])
        i = _getCursorItem()
        if state[2] is None:
            raise RuntimeError("Signal was None, which is the default in 'command'. Undo can't handle that. Change calling function.", state)
        elif i is state[1]:
            #Test, broadly, which type of action needs to be undone. Bascially there is insert, delete and change.
            if state[2] is "insert": #The action was insert, Undo means delete.
                notDuringUndo = False #don't register any undos during the next steps.
                deselectScore() # just a precaution. Especially if delete is involved.
                delete() #has all the signals in it.
                notDuringUndo = True #from now on register undo again.
                return True
            elif state[2] is "delete":
                print ("first delete")
                notDuringUndo = False #don't register any undos during the next steps.
                _insert(state[1]) #insert the deleted item.
                notDuringUndo = True #from now on register undo again.
                return True
            elif state[2] is "insertContainer": #undo means delete container
                notDuringUndo = False #don't register any undos during the next steps.
                deselectScore() # just a precaution. Especially if delete is involved.
                deleteContainerInstance()
                notDuringUndo = True #from now on register undo again.
            elif state[2] is "deleteContainer": #undo means insert the deleted container
                notDuringUndo = False #don't register any undos during the next steps.
                deselectScore() # just a precaution. Especially if delete is involved.
                insertContainer(state[1])
                notDuringUndo = True #from now on register undo again.
            else: #The item itself was changed somehow. But nothing deleted or inserted. We can re-use the internal item memory to undo. That includes deletion and creation of notes in the item.notelist.
                i.undo()
                if state[3]:
                    l_send(lambda:l_item.updateDuration(i))
                    l_send(lambda:l_score.updateTrackFromItem(i))
                l_send(lambda: state[2](i))
                return True
        #Delete: not i == state[1]. The i item is not there anymore.
        elif state[2] is "delete": #The action was delete. 0 and not None.
            notDuringUndo = False #don't register any undos during the next steps.
            _insert(state[1]) #insert the deleted item.
            notDuringUndo = True #from now on register undo again.
            return True
        elif state[2] is "insertContainer": #undo means delete container
            notDuringUndo = False #don't register any undos during the next steps.
            deselectScore() # just a precaution. Especially if delete is involved.
            deleteContainerInstance()
            notDuringUndo = True #from now on register undo again.
        elif state[2] is "deleteContainer": #undo means insert the deleted container
            notDuringUndo = False #don't register any undos during the next steps.
            deselectScore() # just a precaution. Especially if delete is involved.
            insertContainer(state[1])
            notDuringUndo = True #from now on register undo again.
        else:
            raise RuntimeError("Instructed to do undo but the saved item reference and the item which is now on the saved position are not the same. Please inform the developers.", state)

    reg = _getScore().stateRegistry
    if reg:
        state = reg.pop()
        if state is "stop":  #Start compound take loop. "stop" marks the end. We are going backwards here, that means we have to stop when "start"
            while True:
                state = reg.pop()
                if state is "start":
                    break
                else:
                    subUndo(state)
            #print ("Next:", reg[-1]) #we reached a save marker. Inform that the file is now 'clean' again and delete the save marker.
            return True #End undo. Don't continue here.

        elif state is "save":
            #This only happens when we are at the file beginning. Just pass. Keep this code in to make the case explicitely clear.
            pass

        elif state is "start":
            raise RuntimeError("Encountered standalone 'start' marker while doing undo. Start and Stop must always appear in pairs. Stop always after Start.", state)
        else:
            subUndo(state)
            if reg and reg[-1] is "save": #we reached a save marker. Inform that the file is now 'clean' again and delete the save marker.
                l_send(l_global.cleanAgain)
                reg.pop() #remove the save signal-
    else:
        return False #but go on. There is simply no undo left.

def redo():
    """The command was bound in the gui"""
    #print (_getScore().stateRegistry)
    pass

def setCompoundTake(signal):
    """True means start compound take, False means stop compound take."""
    _getScore().stateRegistry.append(signal)


#Importer
def importLisalo(filepath, trackPerBus = False, newSavePath = None):
    f = open(filepath, mode="r")
    lines = f.readlines()

    midiChanCounter = 0
    midiChans = {}
    jack_port_name = "lisalo" #it is an .lsl file after all
    sampleList = []
    lastPortName = ""

    for line in lines:
        if "#" in line:
            line = line[:line.index("#")].strip()
        if "|" in line:
            line = line[:line.index("|")].strip()

        if line.startswith("#") or line == "\n" or line == "": #Ignore empty lines or comments
            pass
        elif line.startswith("!"): #A jack midi portname. Overwrite the default lisalo or commandline option
            line = line.strip("!").strip()
            jack_port_name = line
        elif line.startswith("%"):# A directory to load waves or oggs directly via lisalo-sfzgenerator
            line = line.strip("%").strip()
            sampleList.append((lastPortName, midiChanCounter, line.title()))
            midiChanCounter += 1

        elif line.startswith("="): #New Midi In Port
            line = line.strip()
            midiChans[lastPortName] = midiChanCounter #save the channel count for the last port before switching to the new one.
            lastPortName = line.lstrip("=")
            midiChanCounter = 0
        else: #it is a sample filename. If not the file was wrong.
            line = line.strip()
            sampleList.append((lastPortName, midiChanCounter, line[:-4].title()))
            midiChanCounter += 1
    midiChans[lastPortName] = midiChanCounter #save the remaining, last, channelcount
    f.close()

    new(filepath=newSavePath) #new file
    uniqueInstruments = set() #help the unique container name.
    uniquePortsSlashBusses = set()
    first = True


    #Decide what creates the track. One lsl sample line/a channel or the lsl-busses/midi-in-ports.
    smfChannelCounter = 0
    if trackPerBus:
        for portname, channel, instrument in sampleList:
            #Since we iterate over channels/sample-lines the port will show up multiple times.
            #We only want one track per bus/midi-in-port
            if not portname in uniquePortsSlashBusses:
                if not first: #The first track is already added.
                    trackAdd()
                first = False
                tr = _getTrack()

                #Midi Channel
                tr.jackChannel = [0,midiChans[portname]-1]
                if smfChannelCounter <= 15:
                    tr.smfChannel = [smfChannelCounter, smfChannelCounter]
                    smfChannelCounter += 1
                else:
                    tr.smfChannel = [15, 15]

                tr.uniqueContainerName = portname
                uniquePortsSlashBusses.add(portname)


    else:
        for number, (portname, channel, instrument) in enumerate(sampleList):
            if not first: #The first track is already added.
                trackAdd()

            first = False
            tr = _getTrack()


            #Midi Channels
            tr.jackChannel = [channel, channel]
            if number <= 15:
                tr.smfChannel = [number, number]
            else:
                tr.smfChannel = [15, 15]

            #It is possible that one instrument is used more than once
            #In that case add a name variation
            if not instrument in uniqueInstruments:
                tr.uniqueContainerName = instrument
                uniqueInstruments.add(instrument)
            else:
                tr.uniqueContainerName = instrument + helper.num2word(random.randint(1,26))

    #Prepare for user entry
    trackFirst()
    return True


#Experimental
"""Experimental functions are working just fine, but they are not
covenient.
For example: They add several directives and change properties but you
have to revert those manually

Or they work for a track with chords which have all 4 notes but fail
if less or more."""


def experimental_addAutoChange():
    _getTrack().staffType = "PianoStaff"
    _getTrack().directiveMid["autochange"] = "\\autochange"


def _deleteAllNotesExceptVoice(number, totalVoices):
    deselectScore()
    selectTrack()

    #Delete from bottom
    for x in range(totalVoices-number):
        deleteOuterNote(lowest = True)

    for y in range(number-1):
        deleteOuterNote(lowest = False)

    deselectScore()

def experimental_splitChordTrackToSingleVoiceTrack(numberOfVoices = False):
    if not numberOfVoices:
        ret = l_form.generateForm({"numberOfVoices":(" ", 0)}, _("Split into single voice tracks"), _("How many voices are in this track?"))
        if ret and not ret["numberOfVoices"] == 0: #0 is not valid
            numberOfVoices = ret["numberOfVoices"]
        else:
            return False

    #now there is a value. Create a copy
    deselectScore()
    selectTrack()
    copy()

    for i in range(numberOfVoices): #counts from 0, we need the actual number.
        trackAdd() #switches to the new track automatically
        paste()
        _deleteAllNotesExceptVoice(i+1, totalVoices=numberOfVoices) #deselects and selects automatically



def insertLySequence(bigstring):
    """This needs the cursor duration for the first note."""
    prevailingDuration = _getCursorDuration()
    for item in parseLySequence(bigstring, prevailingDuration = prevailingDuration):
        _pasteItem(item, standaloneInsert = True)

def parseLySequence(string, prevailingDuration):
    """
    prevailing duration is in lilypond format!

    In: "c'4 d8 < e' g' >16 fis'4 a,, <g, b'> c''1"
    out: ('4', ["c'"]), ('8', ['d']), ('16', ["e'", "g'"]),
    ('4', ["fis'"]), (0, ['a,,']), (0, ['g,', "b'"]), ('1', ["c''"])]
    and this transformed to chords

    Syntax:
    note  e'
    <note note>  <c' e'>
    noteDuration   e'4
    <note note>Duration  <c' e'>4

    r for rest:  r4
    m for MMRest with nr of m. :  m2  (2 measures)
    k for invisible MMRest (Skip)
    p for invisible MMRest with extra printing space (Placeholder)

    q to repeat the last item. No duration allowed!
    """

    s = string
    q2 = re.compile(r"(?:<)\s*[^>]*\s*(?:>)\d*|(?<!<)[^\d\s<>]+\d+|(?<!<)[^\d\s<>]+")
    s2 = q2.findall(s)
    s3 = [re.sub(r"\s*[><]\s*", '', x) for x in s2]
    s4 = [y.split() if ' ' in y else y for y in s3]
    q3 = re.compile(r"([^\d]+)(\d*)")

    s = []
    for item in s4:
        if type(item) == list:
            lis = []
            for elem in item:
                lis.append(q3.search(elem).group(1).lower())
                if q3.search(elem).group(2) != '':
                    num = q3.search(elem).group(2)
            if q3.search(elem).group(2) != '':
                s.append((num, lis))
            else:
                s.append((0, lis))
        else:
            if q3.search(item).group(2) != '':
                s.append((q3.search(item).group(2), [q3.search(item).group(1).lower()]))
            else:
                s.append((0, [q3.search(item).group(1).lower()]))


    returnSequence = []
    for duration, pitchlist in s:
        if duration: #not 0
            prevailingDuration = duration
        else:
            if "." in pitchlist[0]: #any number of dots
                returnSequence[-1].dots = len(pitchlist[0])
                continue #nothing to append this round
            elif pitchlist[0] == "~": #TODO this might fail for chords <e~ g>4 e8
                for n in returnSequence[-1].notelist:
                    n.tie = "~"
                continue #nothing to append this round
            else:
                duration = prevailingDuration

        #it was not a standalone dot or tie. So we either got:
        #A note, rest, skip, multi measure rest, mm-skip, mm-placeholder
        #Only for notes the list is len>1
        first = pitchlist[0]

        if first in ["r", "s", "m", "k", "p", "q"]:
            if first == "r" or first == "s":
                item = items.Rest(_getWorkspace(), lilypond.ly2dur[duration], first)
            elif first =="m":
                item = items.MultiMeasureRest(_getWorkspace(), measures = int(duration), lilysyntax = "R")
            elif first =="k":
                item = items.MultiMeasureRest(_getWorkspace(), measures = int(duration), lilysyntax = '\\skip')
            elif first =="p":
                item = items.Placeholder(_getWorkspace(), int(duration))
            elif first =="q":
                pass #the last item variable is still valid
        else:
            pitchlist = [lilypond.ly2pitch[note] for note in pitchlist]
            item = items.Chord(_getWorkspace(), lilypond.ly2dur[duration], pitchlist)
            #for p in pitchlist[1:]:
            #    item.addNote(p)

        returnSequence.append(item)

    return returnSequence

def extractAndRenderChordSymbols():
    """
    This scripts creates a new track with renderings of chords,
    derived from chord symbols.

    Select a track, execute the command. It will create a new track
    with a naive rendering of the chords.
    """
    #TODO:
    #Piped chords in one item.
    #Chords on multi measure rest?
    #Or at least copy MM rests/skips

    def convertChordSymbol(chordString):
        """Parse a chordsymbol string
        returns a transposed list of pitches,
        ready to use with lbj chords"""
        colon = ":" in chordString
        slash = "/" in chordString

        #first separate the bass note from the chord.
        if slash:
            rest, bass = chordString.split("/")
        else:
            rest = chordString
            bass = None

        #Test if it is just major or a different chord.
        if colon:
            root, suffix = rest.split(":")
        else: #just a major chord
            root, suffix = rest, "dur"

        #intervalAutomatic(originalPitch, rootPitch, targetPitch)
        #Return the original pitch, transposed by the interval
        #between rootPitch and targetPitch
        plainRoot = pitch.plain(lilypond.ly2pitch[root.lower()])
        first = pitch.plain(lilypond.untransposedChords[suffix][0])
        pitchlist = [pitch.intervalAutomatic(originalPitch, first, plainRoot)+350 for originalPitch in lilypond.untransposedChords[suffix]]
        if bass: #add the bass note. We always place chords in the first octave. So it is safe to place the bass one octave lower)
            pitchlist.append(lilypond.ly2pitch[bass])

        return pitchlist


    head() #Move the cursor to the start pos.
    realChords = []
    for i in _getActiveCursor().iterate(): #loop over all items
        if i.duration: #Only parse items which have a duration.
            if i.chordsymbols:
                pitchlist = convertChordSymbol(i.chordsymbols) #get the list of transposed pitches
                new = items.Chord(_getWorkspace(), 384, pitchlist) #384 doesn't matter. just a temporary one.
            else: #No chordsymbol. Use a rest.
                new = items.Rest(_getWorkspace(), 384, "r")
            new.putDurations(i.getDurations()) #copy the old durations to the new item
            realChords.append(new)

    trackAdd() #selects the new track as well.
    _getTrack().hideLilypond = True
    l_send(l_score.trackChanged)
    for newItem in realChords:
        _insert(newItem)



#Calfbox
def calfboxInit(soundfont = None, autoconnect = True, midiInPort = None, clientName = "Laborejo"):
    global cboxSmfCompatibleModule
    if not soundfont:
        soundfont = os.path.join(os.path.dirname(__file__), 'gm.sf2')
    try:
        from laborejocore import calfboxwrap
        cboxSmfCompatibleModule = calfboxwrap.Calfbox(384, soundfont, autoconnect, midiInPort, clientName) #Release the kraken!
        #We got this far, that means calfbox is now running. At least we have the classes/instances. Save it in core.Session.
        _getSession().calfbox =  cboxSmfCompatibleModule
    except ImportError:
       warnings.warn("calfbox not found. It is probably not installed as Python3 module. Playback broken. Not 'disabled', broken! You can use the rest of the program just fine.")
    except exceptions.JackRunningError:
        warnings.warn("calfbox not initialised. Probably JACK is not running. Playback broken. Not 'disabled', broken! You can use the rest of the program just fine.")

def calfboxShutdown():
    global cboxSmfCompatibleModule
    if cboxSmfCompatibleModule: #maybe it was not running
        calfboxwrap.cbox.stop_audio()
        calfboxwrap.cbox.shutdown_engine()

    #else:
    #    pass #doesn't matter. the program will exit after that.

def jackMidiInToggleMute(boolean = None):
    if cboxSmfCompatibleModule:
        if not boolean is None and boolean: #but true and false
            cboxSmfCompatibleModule.jack_midi_in_is_open = True
        elif not boolean is None and not boolean:
            cboxSmfCompatibleModule.jack_midi_in_is_open = False
        else: #toggle
            cboxSmfCompatibleModule.jack_midi_in_is_open = not (cboxSmfCompatibleModule.jack_midi_in_is_open)

        l_send(lambda: l_global.jackMidiInToggleMute(cboxSmfCompatibleModule.jack_midi_in_is_open))
        return cboxSmfCompatibleModule.jack_midi_in_is_open #Allow midi in: True or False

def updateJackClientSmfChannelCache():
    if cboxSmfCompatibleModule:
        tr = _getTrack()
        cboxSmfCompatibleModule.smfChannelCache = tr.smfChannel[0]
        cboxSmfCompatibleModule.jackChannelCache = tr.jackChannel[0]

def _calfboxPlayNote(laborejoPitch, jackMode = False):
    """Do not call directly.
    Use l_playback.playNote(laborejoPitch, jackMode = False).
    Don't forget to set l_playback.enabled = True then."""
    if cboxSmfCompatibleModule:
        if jackMode:
            instrumentPatch = _getActiveCursor().prevailingJackPatch[-1] #an int
            jackMode = _getTrack().calfboxUuid
        else:
            instrumentPatch = _getActiveCursor().prevailingSmfPatch[-1] #an int
            jackMode = None #calfbox wants none for the UUID. not false
        cboxSmfCompatibleModule.sendNoteEvent(pitch.toMidi(laborejoPitch), instrumentPatch, jackMode=jackMode)

l_playback._playNote = _calfboxPlayNote

def calfboxGetPlaybackTicks():
    """Part of calfboxProcessor"""
    cboxTicks = cboxSmfCompatibleModule.transport.status().pos_ppqn
    laborejoTicks = cboxTicks + cboxSmfCompatibleModule.negativePlaybackTickOffset

    if cboxSmfCompatibleModule._SMF.cachedRepeatsOfLongestTrack and laborejoTicks >= cboxSmfCompatibleModule._SMF.cachedRepeatsOfLongestTrack[-1][0]: #the next repeat close
        fromTick, toTick = cboxSmfCompatibleModule._SMF.cachedRepeatsOfLongestTrack.pop() #get the next tuple. The list is reversed in order already so we can .pop()
        cboxSmfCompatibleModule.negativePlaybackTickOffset += toTick - fromTick

    l_score.playbackSetPosition(laborejoTicks) #THIS is the only exception of l_send. Because this whole function is wrapped in l_send by calfboxProcessor()

def calfboxSetTransportKeepRolling(boolean):
    if cboxSmfCompatibleModule:
        cboxSmfCompatibleModule.keepRolling = boolean


def calfboxProcessor():
    """Query calfbox for new midi data and react accordingly.
    Intended to use periodically in a GUI idle loop, event timer or so.
    It is expected to be called several times per second, so keep
    any changes brief and small"""
    #For performance reasons: No check if the calfbox module is running. Nobody should be that stupid and call that function in a script or manually.
    events = cboxSmfCompatibleModule.cbox.get_new_events()
    for d in events:
        data = d[2] #just the midi stuff.
        if data and len(data) == 3:
            event, midipitch, velocity = data #of course it doesn't need to be pitch and velocity. But this is easier to remember than "data2, data3"
            channel = event & 0x0F
            mode = event & 0xF0

            ##Midi Thru. The listener. Has nothing to do with note entry. It can be used to pre-playback sounds to your sampler.
            ##Sends only note on and off.
            #if mode == 0x90: #on
            #    jack_midi_out_data.put_nowait((mode + smfChannelCache, data2, data3)) #create a note on the currents track channel.
            #elif mode == 0x80: #off
            #    jack_midi_out_data.put_nowait((mode + smfChannelCache, data2, data3))

            if mode == 0x90 and cboxSmfCompatibleModule.jack_midi_in_is_open:  #the midi in port can be muted: #note on. Note off gets ignored.
                if cboxSmfCompatibleModule.states.cc[0x07]: #volume not 0?
                    if cboxSmfCompatibleModule.states.digitalCC(0x40): #pedal down?
                        if cboxSmfCompatibleModule.states.duringChordEntry: #there was a note before the current one with a pedal down. We are in a chord adding process
                            addNoteToChord(pitch.midiToPitch(midipitch, _getKeysig()))
                        else:
                            cboxSmfCompatibleModule.states.duringChordEntry = True
                            insertPrevailingDuration(pitch.midiToPitch(midipitch, _getKeysig()))
                            left() #executeCC for pedal up does the right() function in the end
                    else:
                        insertPrevailingDuration(pitch.midiToPitch(midipitch, _getKeysig()))
                else:
                   _insertRest(_getCursorDuration())
            elif mode == 0xB0:  #CC
                #cboxSmfCompatibleModule.states.cc[midipitch] = velocity #save the controller data
                if midipitch in cboxSmfCompatibleModule.executeCC:
                    #The executeCC function receives the new second midi byte ("Velocity") and the velocity value exactly before this midi byte, so you can compare with the old one.
                    returnfunction = cboxSmfCompatibleModule.executeCC[midipitch](velocity, cboxSmfCompatibleModule.states.cc[midipitch]) #execute the bound function, if existent.
                    if returnfunction:
                        eval(returnfunction)()
                cboxSmfCompatibleModule.states.cc[midipitch] = velocity #save the controller data

    #cboxSmfCompatibleModule.transport.status().playing is for the internal state. It does not reflect the jack transport status. Thats why we check our song position and length instead.
    ppqn = cboxSmfCompatibleModule.transport.status().pos_ppqn
    if ppqn is None: #Before the first playback this is None (no file) or 0 (no playback).
        return True
    if (not cboxSmfCompatibleModule.keepRolling) and ppqn >= cboxSmfCompatibleModule._SMF.lastMaxTicks:
        cboxSmfCompatibleModule.transport.stop()
        l_send(l_score.playbackStop)
    elif cboxSmfCompatibleModule.transport.status().playing:
        l_send(calfboxGetPlaybackTicks)
    elif not cboxSmfCompatibleModule.keepRolling: #and not playing
        l_send(l_score.playbackStop) #this hides the playback bar. Calfbox itself does not move the playback cursor further than our max ticks, even if transports keeps rolling.
    #else: #playback stopped but keepTransportRolling is True



def play(startTick = None, workspace = None, jackMode = False, standaloneMode = False, parts = None, waitForStart = False, keepRolling = True):
    """Start the playback through calfbox/midi
    Playback starts at the cursor position in its own thread.
    Does not move the cursor.

    Returns (cbox module, smf/calf-class,
    and maxTicks) which is the end of the song.

    If used in a non-gui script use waitForEnd = True.
    This will set play into a loop and wait until playback has ended
    before continuing the script. Else your script will end before
    playback has ended and shut down the playback.
    """

    if not cboxSmfCompatibleModule:
        #A GUI would not allow that but a wrong userscript might still try this.
        raise RuntimeError("You have to initialize calfbox before using playback.")

    if not workspace:
        workspace = _getWorkspace()
        if not workspace:
            raise ValueError("Parameter workspace must be core.Workspace or core.Collection but it was:", workspace)

    if cboxSmfCompatibleModule.transport.status().playing: #Pause
        l_send(l_score.playbackStop)
        cboxSmfCompatibleModule.running = False
        cboxSmfCompatibleModule.transport.stop()
        return True

    smfStructure = workspace.score.exportCalfbox(smf = cboxSmfCompatibleModule, jackMode = jackMode, parts = parts) # "smfStructure" is an relict from libsmf. We use it here as well to make comparison with exportMidi easier.
    if not smfStructure: #empty Score
        return False
    maxTicks = smfStructure.update(jackMode= jackMode) #convert to binary format and send to RT thread. Returns the overall length of the song.

    # Start playback. From this moment on playback is done in a seperate track.
    #The only important thing is not to close the whole program until the playback has finished.
    #The most simple way to do this is just by letting the main program, Laborejo, sleep.
    #Instead of libsmfs "save()" method for the returned smfStructure/calfData we have smf.play()
    if startTick is None:
        startTick = smfStructure.playbackStartPosition

    cboxSmfCompatibleModule.negativePlaybackTickOffset = 0
    cboxSmfCompatibleModule.running = True
    cboxSmfCompatibleModule.keepRolling = keepRolling

    if not waitForStart:
        l_send(l_score.playbackStart)
        cboxSmfCompatibleModule.play(startTick = startTick)

    if standaloneMode: #Don't call from a GUI!
    #No, don't run from a GUI!
    #This part blocks, has sleep() in it.
        endTick = int(cboxSmfCompatibleModule._SMF.lastMaxTicks)
        endMinutes, endSeconds = divmod(cboxSmfCompatibleModule._SMF.getSongLengthInSeconds(), 60)
        endTime = "".join((str(int(endMinutes)), ":", str(int(endSeconds)).zfill(2)))
        endMinuteLen = len(str(int(endMinutes)))
        def printStatus():
            status = cboxSmfCompatibleModule.transport.status()
            now = status.pos_ppqn
            seconds = status.pos/status.sample_rate
            minutes, seconds = divmod(seconds, 60)
            steps = int(round(now/endTick,2)*30)
            bar = "".join(("\r",'[', steps*"#", (30-steps)*" ", '] ', str(int(minutes)).zfill(endMinuteLen), ":", str(int(seconds)).zfill(2), "/", endTime))
            print (bar, end="")
        while smfStructure.transport.status().pos_ppqn < endTick and cboxSmfCompatibleModule.running:
            # Get transport information - current position (samples and pulses), current tempo etc.
            #master = smfStructure.transport.status().pos_ppqn #'pos', 'pos_ppqn', 'tempo', 'timesig', 'sample_rate'
            printStatus()
            smfStructure.cbox.call_on_idle() # Query JACK ports, new USB devices etc.
            time.sleep(0.2)
        printStatus() #a last time because the while loop is inaccurate and ends more or less "randomly" at a certain tick.
        if not keepRolling:
            cboxSmfCompatibleModule.transport.stop()
    return (smfStructure, maxTicks) #This enables any frontend or script to query the status. See the loop below:

def playOnce(startTick = 0, jackMode=False, waitForStart = False, keepRolling = True):
    """A tool for the commandline or script files.
    Just play it once, block while its playing."""
    play(startTick = startTick, jackMode=jackMode, standaloneMode = True, waitForStart = waitForStart, keepRolling = keepRolling)

def playCurrentTrack(jackMode = False, waitForStart = False, keepRolling = True):
    play(jackMode=jackMode, parts = ("_uniqueContainerName", _getTrack().uniqueContainerName ), waitForStart = waitForStart, keepRolling = keepRolling) #core.parts is a tuple (trackProperty, value). Only those which match will be exported.

def playCurrentGroup(jackMode = False, waitForStart = False, keepRolling = True):
    play(jackMode=jackMode, parts = ("group", _getTrack().group ), waitForStart = waitForStart, keepRolling = keepRolling) #core.parts is a tuple (trackProperty, value). Only those which match will be exported.

def stop():
    if cboxSmfCompatibleModule: #several functions like new, load, close call stop to prevent crazy playback behaviour.
        cboxSmfCompatibleModule.running = False
        cboxSmfCompatibleModule.transport.stop()
        l_send(l_score.playbackPanic)
        cboxSmfCompatibleModule.panic()

def setSoundfont(filepath):
    """Load a new soundfont. Only one at a time."""
    if os.path.exists(filepath):
        cboxSmfCompatibleModule.newInternalInstrument(filepath)
    else:
        warnings.warn("Soundfont " + filepath + " does not exist")

def scaleTempo(factor):
    """Scale the tempo. Works during playback.
    Everytime you use factor it will be scaled from 1 again,
    not from the current tempo"""
    cboxSmfCompatibleModule._SMF.scaleTempoLive(factor)

def getMidiPorts():
    """Just forwards to the calfbox module. Returns a list of strings"""
    if cboxSmfCompatibleModule:
        return cboxSmfCompatibleModule.getMidiPorts()
    else:
        return []

def getAudioPorts():
    """Just forwards to the calfbox module. Returns a list of strings"""
    if cboxSmfCompatibleModule:
        return cboxSmfCompatibleModule.getAudioPorts()
    else:
        return []

#Debug and Tests

def test_material():
    """Insert 8 random quarter notes"""
    setCompoundTake("start")
    print ("Snapshots:", len(_getScore().snapshots))
    for i in range(8):
        insertChord(384, randomPitchHypoModeCursor())
    setCompoundTake("stop")

def withTest():
    _getTrack().trackDirectiveWith["line-count"] = "\\override StaffSymbol #'line-count = #3"
    _getTrack().trackDirectiveWith["accepts-lyrics"] = "\\accepts Lyrics"


def showSelection():
    """Show the selection which is not ordered"""
    for x in _getScore():
        for y in x.selectedItems:
            print (y.exportLilypond())

def showData(lily = False):
    if lily:
        for x in _getScore():
            print (x.container.exportLilypond())
    else:
        for x in _getScore():
            for item in x:
                print (item.exportLilypond())

def testForm():
    data = {}
    data["finger"] = ("Finger", 1)
    result = l_form.generateForm(data, "Fingering", "Please give a finger number")
    _finger(result["finger"])

def testGen():
    functionlist = [(insertKeyEesMajor, "Es Dur"), (insertKeyCMajor, "C Dur")]
    _generateDropDownGuiFromFunctions(functionlist, insertKeyCMajor, "Do it!", title="Keysig Chooser", comment="Choose a keysig.")

def transtest():
    print (_("Translation Test. The language is English."))


def showUnfolded():
    workspace = core.Workspace()
    workspace.score = _getScore().unfoldScore()
    core.session.load(workspace)
    l_send(lambda: l_global.workspaceChanged(_getWorkspace()))
    l_send(lambda: l_global.new(_getWorkspace()))


##################################################
#Initial Execution, program start. Only the basics. The rest is up to scripts or a gui (which is a script itself).
core.clipboard = core.SessionClipboard()
core.session = core.Session()
#core.session.substitutiondatabase = substitutiondatabase.build_database() #Circular dependencies demand that this is filled in after everything else was loaded. Each item can access this through the parentX chain: Item().parentWorkspace.parentSession.substitutiondatabase
core.session.build_database = substitutiondatabase.build_database #Circular dependencies demand that this is filled in after everything else was loaded. Each item can access this through the parentX chain: Item().parentWorkspace.parentSession.substitutiondatabase

#prepare the lilypond chord dict in module lilypond. We can't execute the following code  in the lilypond module because it needs api. functions
for chordString, lilyChord in lilypond.chordnamesSuffix.items():
    a = parseLySequence(lilyChord, "4")[0] #"4" is a lilypond duration. This functions returns a list. In this case with just one member. So we want that.
    lilypond.untransposedChords[chordString] = [n.pitch for n in a.notelist]
    #now lilypond.untransposedChords is available for the rest of the program
