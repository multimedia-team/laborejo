# -*- coding: utf-8 -*-
#This is a a template. Copy and implement those functions into your own
#script or GUI.

#Then call the Laborejo command (like insert4()) and you will receive
#instructions from the listener what to do

class Listener(object):
    def __init__(self):
        pass

class ListenerGlobal(Listener):
    def __init__(self):
        super(ListenerGlobal, self).__init__()

    def jackMidiInToggleMute(self, onOrOff):
        pass

    def workspaceChanged(self, newActiveWorkspace):
        pass

    def new(self, backendWorkspace):
        pass

    def cleanAgain(self):
        """The undo system reached a point where it got clean again"""
        pass


class ListenerCollections(Listener):
    def __init__(self):
        super(ListenerCollections, self).__init__()

    def load(self, loadedCollection):
        pass


class ListenerRecord(Listener):
    """Midi in"""
    def __init__(self):
        super(ListenerRecord, self).__init__()

    def midiIn(self, pitch):
        """When midi in happens this can be queried by the gui to e.g.
        highlight a virtual keyboard or a simple LED graphic for midi-in"""
        pass

class ListenerPlayback(Listener):
    """This is one of the few listeners that are already implemented
    by default through Laborejo Core. If you just want that to work
    don't create your own version, just send a message to api.l_playback

    However, you have to set api.l_playback.enabled = True manually
    if you want to hear something. By default it is off so that scripts,
    load and copy/paste do not make sounds and steal CPU"""

    def __init__(self):
        super(ListenerPlayback, self).__init__()
        self.enabled = False

    def playNote(self, pitch, jackMode):
        if self.enabled:
            self._playNote(pitch, jackMode)

    def noteOn(self, pitch, jackMode):
        if self.enabled:
            self._playNote(pitch, jackMode)

    def noteOff(self, pitch, jackMode):
        if self.enabled:
            self._playNote(pitch, jackMode)

    #These are the real functions. Reimplent those, if you need.
    def _playNote(self, pitch, jackMode = False):
        """Highlevel function to play back a single note with a fixed
        0.5 sec duration
        Look in laborejocore.__init__ to see how this function
        is implemented."""
        pass

    def _noteOn(self, pitch, jackMode = False):
        """A lower level function. Combined with noteOff this allows
        you to create a real note duration. But you have to time the
        length yourself
        Look in laborejocore.__init__ to see how this function
        is implemented."""
        pass

    def _noteOff(self, pitch, jackMode = False):
        """A lower level function. Combined with noteOn this allows
        you to create a real note duration. But you have to time the
        length yourself
        Look in laborejocore.__init__ to see how this function
        is implemented."""
        pass

class ListenerCursor(Listener):
    def __init__(self):
        super(ListenerCursor, self).__init__()

    def setPitch(self, value = None):
        pass

    def setPosition(self, trackIndex, horizontalIndex):
        pass

    def prevailingDuration(self):
        """Signal that the cursor prevailing duration has changed"""
        pass

    def pitchIndex(self):
        """Signal that the cursor pitchindex has changed"""
        pass

    def right(self):
        pass

    def left(self):
        pass

class ListenerItem(Listener):
    """All track items including chords.
    Space of the item is derived from its duration.
    For chords leave the text blank."""
    def __init__(self):
        super(ListenerItem, self).__init__()

    def insertAtCursor(self, item, currentTrack = None):
        pass
    def deleteAtCursor(self, item):
        pass

    def update(self, backendItem):
        """Redraw the item at cursor position.
        For changes which do not affect spacing on a track.
        This is the complete update, even if unnecessarry.
        For dynamic (pitches) call changePitch.
        For statics (clef, performanceSig-name) call updateStatic"""
        pass

    def updateStatic(self, backenItem):
        """Redraw the part of a guiItem which changes only seldomly.
        Like the displayed name of a performance sig.
        This is the "leftover" signal for anything which has no signal
        of their own. FiguredBass and ChordSymbol have."""
        pass

    def updateDuration(self, backendItem):
        pass

    def updateDynamic(self, backendItem):
        """Redraw only parts that move on the Y axis.
        Only chords and the keysig need this currently because they
        depend on the clef position.
        If your item needs a clef, use this."""
        pass

    def updateVisibility(self, backendItem):
        """Update if the item is normal, transparent or hidden"""
        pass


    def select(self, item):
        pass
    def deselect(self, item):
        pass

class ListenerTrack(Listener):
    def __init__(self):
        super(ListenerTrack, self).__init__()

    def select(self):
        pass

    def deselect(self):
        pass

class ListenerScore(Listener):
    """Add and remove tracks etc."""
    def __init__(self):
        super(ListenerScore, self).__init__()
    def addLastTrack(self):
        pass
    def deleteTrack(self, tracknumber):
        pass
    def trackChanged(self):
        """Signal that the current backend track changed"""
        pass
    def _update(self):
        pass
    def deselectScore(self):
        pass
    def trackSwapUp(self):
        pass
    def trackSwapDown(self):
        pass
    def reset(self):
        """This is essentially a data reset.
        In all tracks move all items to rightPart and reset tick count"""
        pass

    def updateTrackFromItem(self, backendItem):
        """Give an item, the gui has to figure out how to update
        from there.
        This is for duration changes so that following items
        can be re-arranged."""
        pass
    def selectScore(self):
        pass

    def playbackStart(self):
        """The playback has started"""
        pass

    def playbackStop(self):
        """The playback has stopped"""
        pass

    def playbackSetPosition(self, absoluteUnfoldedTicks):
        """Update the playhead/playback cursor position.
        This will only be called when playback is rolling.
        This receives the absolute, unfolded and global ticks.

        It is up to the gui to handle repeats, jumps and containers."""
        pass

    def playbackPanic(self):
        """A special midi panic signal. Also calls stop. If in doubt
        just call stop and leave it be"""
        pass

class ListenerNote(Listener):
    """Updates for one note.
    Like note-ties or accidentals"""
    def __init__(self):
        super(ListenerNote, self).__init__()
    def finger(self):
        pass
    def pitch(self):
        pass

class ListenerChord(Listener):
    """Used to send updates for one chord or rest."""
    def __init__(self):
        super(ListenerChord, self).__init__()

    def figuredBass(self, item):
        pass
    def chordSymbol(self, item):
        pass
    def directivePst(self, item):
        pass
    def tupletMarkers(self, item):
        pass
    def tremoloMarker(self, item):
        pass
    def substitutionMarker(self, item):
        pass

class ListenerFormGenerator(Listener):
    """Add and remove tracks etc."""
    def __init__(self):
        super(ListenerFormGenerator, self).__init__()

    def generateForm(self, dictionary, title, comment):
        raise RuntimeError("Tried to call a Gui in a non-gui enviroment.\nCheck if ListenerFormGenerator was properly overwritten by the GUI.\nOr probably you forgot a parameter for this function which is needed without a gui, but the command tried to ask the gui.")
