# -*- coding: utf-8 -*-
import os
import tempfile
import subprocess
import sys, warnings
import urllib.request, urllib.parse, json #for lilyBin export
from datetime import date
da = date.fromordinal(730920) # 730920th day after 1. 1. 0001

def getIncludes(TEMPLATE_ROOT, USER_TEMPLATE_ROOT, COLLECTION_TEMPLATE_ROOT):
    """
    You can use includes, not the Lilypond ones but Laborejo based.
    These will be included and unfolded into the export.ly so it becomes
    self-contained. It is a system completely indenpendent of lilyponds
    \include command.
    Include files"""

    #include files. .ly files which were explicitly marked %$$INCLUDE$$ as include snippet by the user.
    lilypondincludes = {} #key is the simple filename without path, value is the file content, as string. Only those with marker.
    lilypondtemplatenames = set() #All .ly files without a marker, real templates.

    #repeat the parsing three times with different paths, save in lilypondincludes{}. Each time a key/filename appears a second time it will overwrite the old one. Thats how we get the priorities mentioned in the docstring.
    def parseIncludes(PATH, lilypondincludes, lilypondtemplatenames, recursive = True):
        if os.path.isdir(PATH):
            if recursive:
                recursiveList = os.walk(PATH)
            else:
                t = os.listdir(PATH)
                recursiveList = [(PATH, [], t)]

            for rootOriginal, dirs, files in recursiveList:
                for name in files:
                    if name.lower().endswith((".ly")): #ignore README or whatever.
                        with open(rootOriginal + "/" +  name, 'r') as f: #peak into the first line of the file
                            first_line = f.readline()
                            if first_line.startswith("%$$INCLUDE$$"):
                                lilypondincludes[name] = f.read()
                            elif rootOriginal == PATH: #a normal template. just save the name as string for future reference
                                lilypondtemplatenames.add(name)
                            else: #a normal template, but nested. not allowed. If a bug happens it is here. Maybe there is too much detection or links are not recognizes. so better give a warning
                                warnings.warn("Template file "+ name +" in nested subdir not allowed. "+  rootOriginal  + " and " + PATH + " is not the same directory.")
    parseIncludes(TEMPLATE_ROOT, lilypondincludes, lilypondtemplatenames) #system dir
    parseIncludes(USER_TEMPLATE_ROOT, lilypondincludes, lilypondtemplatenames)  #user/config/home dir, overwrites dict keys from the line above
    parseIncludes(COLLECTION_TEMPLATE_ROOT, lilypondincludes, lilypondtemplatenames, recursive = False)  #lbj container override as first priority. again, overwrites keys from the two lines above

    return (lilypondtemplatenames, lilypondincludes)


def _createProto(templateData, workspace, standaloneMode = True):
    """Replace template markers with Laborejo data.

    There are single markers and Collection() markers.
    In case of a single .lbjs file (standalone) they are the same

    Everything that needs to be replaced only once (like the date,
    fontsize or the global header is done here.

    Workspace can be core.Workspace() as well as core.Collection()
    They are different, but this difference only matters in the step
    after _createProto.
    """
    templateString, TEMPLATE_ROOT, USER_TEMPLATE_ROOT, COLLECTION_TEMPLATE_ROOT  = templateData
    includes = getIncludes(TEMPLATE_ROOT, USER_TEMPLATE_ROOT, COLLECTION_TEMPLATE_ROOT)

    lilypondtemplatenames, lilypondincludes = includes

    #try to replace all all include markers
    for filename, includeString in lilypondincludes.items():
        incMarker = "%$$" + filename  +"$$"
        templateString = templateString.replace(incMarker, includeString)

    #Now replace all non-include markers. This order gives includes the chance to use markers themselves.
    templateString = templateString.replace("%$$DATE$$", da.today().strftime("%A %d. %B %Y")) #The current date
    templateString = templateString.replace("%$$FONTSIZE$$", str(workspace.lyFontSize)) #Lilypond Fontsize from core.score
    templateString = templateString.replace("%$$LILYPONDDEFINITIONS$$", workspace.lilypondDefinitions.exportLilypond()) #Extra definitions, like Lilypond-Scheme

    if not standaloneMode:
        templateString = templateString.replace("%$$GLOBALHEADER$$", workspace.header.exportLilypond())

    if workspace.lastKnownFilename:
        templateString = templateString.replace("%$$FILENAME$$", workspace.lastKnownFilename)
    else:
        templateString = templateString.replace("%$$FILENAME$$", "Unsaved")

    return templateString

def _createMusicBlock(templateString, workspace, standaloneMode = True, parts = None):
    """Replaces only the music block
    A real core.Workspace() is required
    """
    orgScore  = workspace.score
    tempScore = workspace.score.convertToExportScore(parts = parts)   #with mastertrack included.
    if not tempScore: #the export score is empty or only a master track
        return tempScore
    workspace.score = tempScore #we need original cursors in the right place. TODO: All cursors as parameters.

    if standaloneMode:
        #the only header data we need is global header.
        #But we need to block the extra score header block with empty data (which means empty lilypond syntax)
        #If not certain meta data will show up twice, once for the global header and once for score header.
        templateString = templateString.replace("%$$HEADER$$", workspace.score.emptyHeader.exportLilypond())
        templateString = templateString.replace("%$$GLOBALHEADER$$", workspace.score.header.exportLilypond()) #Header data like title and composer.
    else:
        #There is already a global header in place but we also need the score header because this is only a part of a core.Collection()
        #print ("music block header", workspace.score.header.data)
        templateString = templateString.replace("%$$HEADER$$", workspace.score.header.exportLilypond())

    #Voices must be generated before container, eventhough the voices contain Container. Only by recursing through voices the container-data generation is triggered.
    templateString = templateString.replace("%$$VOICES$$",  tempScore.generateLilypondVoice()) #Score music data.
    templateString = templateString.replace("%$$CONTAINER$$", tempScore.generateLilypondContainer()) #Score container music data.
    templateString = templateString.replace("%$$SUBTEXT$$", tempScore.generateSubtext()) #Subtext, word wrapped under the score.
    templateString = templateString.replace("%$$STRUCTURE$$",  tempScore.exportLilypond()) #Score music structure. (staffs, staffgroups and voices)
    templateString = templateString.replace("%$$SCORETRANSPOSE$$",  tempScore.generateLilypondTranspose()) #Score music structure. (staffs, staffgroups and voices)

    workspace.score = orgScore
    return templateString


def createWorkspace(templateData, workspace, parts):
    """Convert one single workspace (.lbjs) to a lilypond string.
    Works on a copy of the score to embedd the mastertrack.
    The workspace must be a real core.Workspace() and not Collection()"""

    #Replace all generic markers
    templateString = _createProto(templateData, workspace)
    templateString = _createMusicBlock(templateString, workspace, standaloneMode = True, parts = parts)
    return templateString


def createCollection(templateData, collection, parts):
    """Create a multi-score document
    This requires the template to have music section markers:  %$$!%
    One before and one after the music. The whole block will be exported
    multiple times.
    """
    #If this is a collection plus parts export we can safely edit the data directly.
    if parts and collection.score.splitByToHeader: #during collection edit nothing is saved so we can safely modify anything. The problem is that we must set the global header to the local instrument each time.
        collection.header.data["instrument"] = parts[1] #this needs to be done before calling createProto.

    #Create the basic template with all generic markers replaced
    templateString = _createProto(templateData, collection, standaloneMode = False)

    #Check if we have a valid template with two music markers %$$!%
    if not templateString.count("%$$!%") == 2:
        raise ValueError("The template must contain exactly two markers %$$!%, each in a seperate line, to enclose the music block with header, voice, container, subtext and structure")

    #Cut and save everything between the two %$$!% markers.
    blockAbove, musicBlock, blockBelow = templateString.split("%$$!%")
    #blockAbove and blockBelow are already finished through _createProto

    combined = [_createMusicBlock(musicBlock, workspace, standaloneMode = False, parts = parts) for workspace in collection.workspaceList]
    #_createMusicBlock can have None or False in it if the score was empty or had only a master track in it.
    combinedFiltered = list(filter(lambda item: item, combined))
    if not combinedFiltered: #only empty scores?
        return None
    musicDataAsLilypondString = "\n%%%%%%%%%%\n%%%%%%%%%%\n".join(combinedFiltered)
    return blockAbove + musicDataAsLilypondString + blockBelow

def create(templateData, workspace, parts):
    """A function that decides if we have a multi-score export or a
    single, standalone, export.
    This is done by typechecking workspace if core.Workspace() or
    core.Collection()"""

    #We cannot import the core module to lilypond. Thats why we are checking for the name directly.
    try: #Py 3.2 compatibility #TODO
        typ = type(workspace).__qualname__
    except:
        typ = "Workspace"
        warnings.warn("You need Python 3.3 to export a Laborejo Collection")


    if typ is "Workspace":
        r = createWorkspace(templateData, workspace, parts)
        if not r:
            raise ValueError(workspace, "There is no music to export. Empty tracks and the master track do not count.")
        else:
            return r

    elif typ is "Collection":
        r = createCollection(templateData, workspace, parts)
        if not r:
            raise ValueError(workspace, "There is no music to export. Empty tracks and the master track do not count")
        else:
            return r

    else:
        raise ValueError("workspace must be either core.Workspace or core.Collection. But it was:", typ)


def save(templateData, workspace, filepath, parts = None): #needs an instance of Workspace or Collection
    """Create a lilypond file with the given template, run lilypond and show it in a pdf viewer.
    Substitute template variables with generated data. If a variable is ommited no error is raised. The data gets discarded."""
    temp = create(templateData, workspace, parts)
    if not os.path.isdir(os.path.dirname(filepath)):
        os.makedirs(os.path.dirname(filepath))
    f = open(filepath, 'w')
    f.write(temp)


def _callExternalCustom(string): #don't overwrite! don't change the name, don't change the number of arguments. External programs like the collection editor depend on that function
    return subprocess.call(string, shell=True)
#_callExternalCustom = lambda string: subprocess.Popen(string, shell=True) #don't overwrite!

callExternalCustom = _callExternalCustom

def previewPDF(templateData, workspace, lilypondbinary = None, pdfviewer = None, parts = None):
    if not pdfviewer:
        pdfviewer = "xdg-open"
    if not lilypondbinary:
        lilypondbinary = "lilypond"
    temp = create(templateData, workspace, parts)
    tempi = tempfile.NamedTemporaryFile(delete=False, mode="w", encoding = "utf-8")
    tempi.write(temp)
    tempi.close()
    #     ret = os.system(lilypondbinary + ' --output=' + tempi.name + " " + tempi.name)

    #ret = subprocess.call(lilypondbinary + ' --output=' + tempi.name + " " + tempi.name, shell=True)
    programString = lilypondbinary + ' --output=' + tempi.name + " " + tempi.name
    ret = callExternalCustom(programString)
    if ret == 0:
        subprocess.Popen(pdfviewer + " " +  tempi.name + ".pdf", shell=True)
    else:
        #raise SystemError("Error: Could not create temp file")
        raise RuntimeError("Could not create lilypond output. Most likely you have Lilypond errors. Either you did override things manually or it is an inconvenience the developers can possibly solve. Please send your fail to the Laborejo team.")


def exportLilyBin(templateData, workspace, lilybinId, parts = None):
    #TODO: lilybin.com does not support the reusing of IDs yet. But we are ready.
    temp = create(templateData, workspace, parts)

    params = urllib.parse.urlencode({'code': temp})
    params = params.encode('utf-8')
    f = urllib.request.urlopen("http://lilybin.com/save", params)
    a = f.read().decode('utf-8') #a dict with id an revision
    jsonReturnDict = json.loads(a)
    lilybinUrl = "http://www.lilybin.com/" + jsonReturnDict["id"]
    return (lilybinUrl, jsonReturnDict["id"])



def exportPDF(templateData, workspace, filepath, lilypondbinary = None, parts = None):
    if not lilypondbinary:
        lilypondbinary = "lilypond"
    temp = create(templateData, workspace, parts)
    tempi = tempfile.NamedTemporaryFile(delete=False, mode="w", encoding = "utf-8")
    tempi.write(temp)
    tempi.close()
    if not os.path.isdir(os.path.dirname(filepath)):
        os.makedirs(os.path.dirname(filepath))
    biny = "lilypond"
    arg1 = '--output="' + os.path.splitext(filepath)[0]+'"' #lilypond adds .pdf no matter what you give. So if we receive a filename .pdf we need to strip the extension first.
    arg2 = tempi.name
    programString = " ".join([biny, arg1, arg2])
    #subprocess.Popen(lilypondbinary + ' --output="' + finalFilepath + '" ' + tempi.name , shell=True)  #does not work with guis rerouting stdout because it ignores stdout and stderr and uses its own thing. There is a python workaround but then it blocks.
    callExternalCustom(programString)
    return (os.path.splitext(filepath)[0] + ".pdf")

#Tables and Lists#####
######################

#The table to convert internal pitches into lilypond and back.
#0 - A tone humans cannot hear anymore.
#1420 - "Middle" c'
#2130 - Soprano-Singers high C
#3150 - Goes beyond the range of a modern piano
#+inf.0 - A rest

#<10 is reserved for microtones, in the future.
#+10 One accidental up jumps over to the next note after cisis
#+50 One diatonic step, preserve accidentals
#+350 One Octave
ly2pitch = {
    "ceses,,," : 00,
    "ces,,," : 10,
    "c,,," : 20,
    "cis,,," : 30,
    "cisis,,," : 40,
    "deses,,," : 50,
    "des,,," : 60,
    "d,,," : 70,
    "dis,,," : 80,
    "disis,,," : 90,
    "eeses,,," : 100,
    "ees,,," : 110,
    "e,,," : 120,
    "eis,,," : 130,
    "eisis,,," : 140,
    "feses,,," : 150,
    "fes,,," : 160,
    "f,,," : 170,
    "fis,,," : 180,
    "fisis,,," : 190,
    "geses,,," : 200,
    "ges,,," : 210,
    "g,,," : 220,
    "gis,,," : 230,
    "gisis,,," : 240,
    "aeses,,," : 250,
    "aes,,," : 260,
    "a,,," : 270,
    "ais,,," : 280,
    "aisis,,," : 290,
    "beses,,," : 300,
    "bes,,," : 310,
    "b,,," : 320,
    "bis,,," : 330,
    "bisis,,," : 340,
    "ceses,," : 350,
    "ces,," : 360,
    "c,," : 370,
    "cis,," : 380,
    "cisis,," : 390,
    "deses,," : 400,
    "des,," : 410,
    "d,," : 420,
    "dis,," : 430,
    "disis,," : 440,
    "eeses,," : 450,
    "ees,," : 460,
    "e,," : 470,
    "eis,," : 480,
    "eisis,," : 490,
    "feses,," : 500,
    "fes,," : 510,
    "f,," : 520,
    "fis,," : 530,
    "fisis,," : 540,
    "geses,," : 550,
    "ges,," : 560,
    "g,," : 570,
    "gis,," : 580,
    "gisis,," : 590,
    "aeses,," : 600,
    "aes,," : 610,
    "a,," : 620,
    "ais,," : 630,
    "aisis,," : 640,
    "beses,," : 650,
    "bes,," : 660,
    "b,," : 670,
    "bis,," : 680,
    "bisis,," : 690,
    "ceses," : 700,
    "ces," : 710,
    "c," : 720,
    "cis," : 730,
    "cisis," : 740,
    "deses," : 750,
    "des," : 760,
    "d," : 770,
    "dis," : 780,
    "disis," : 790,
    "eeses," : 800,
    "ees," : 810,
    "e," : 820,
    "eis," : 830,
    "eisis," : 840,
    "feses," : 850,
    "fes," : 860,
    "f," : 870,
    "fis," : 880,
    "fisis," : 890,
    "geses," : 900,
    "ges," : 910,
    "g," : 920,
    "gis," : 930,
    "gisis," : 940,
    "aeses," : 950,
    "aes," : 960,
    "a," : 970,
    "ais," : 980,
    "aisis," : 990,
    "beses," : 1000,
    "bes," : 1010,
    "b," : 1020,
    "bis," : 1030,
    "bisis," : 1040,
    "ceses" : 1050,
    "ces" : 1060,
    "c" : 1070,
    "cis" : 1080,
    "cisis" : 1090,
    "deses" : 1100,
    "des" : 1110,
    "d" : 1120,
    "dis" : 1130,
    "disis" : 1140,
    "eeses" : 1150,
    "ees" : 1160,
    "e" : 1170,
    "eis" : 1180,
    "eisis" : 1190,
    "feses" : 1200,
    "fes" : 1210,
    "f" : 1220,
    "fis" : 1230,
    "fisis" : 1240,
    "geses" : 1250,
    "ges" : 1260,
    "g" : 1270,
    "gis" : 1280,
    "gisis" : 1290,
    "aeses" : 1300,
    "aes" : 1310,
    "a" : 1320,
    "ais" : 1330,
    "aisis" : 1340,
    "beses" : 1350,
    "bes" : 1360,
    "b" : 1370,
    "bis" : 1380,
    "bisis" : 1390,
    "ceses'" : 1400,
    "ces'" : 1410,
    "c'" : 1420,
    "cis'" : 1430,
    "cisis'" : 1440,
    "deses'" : 1450,
    "des'" : 1460,
    "d'" : 1470,
    "dis'" : 1480,
    "disis'" : 1490,
    "eeses'" : 1500,
    "ees'" : 1510,
    "e'" : 1520,
    "eis'" : 1530,
    "eisis'" : 1540,
    "feses'" : 1550,
    "fes'" : 1560,
    "f'" : 1570,
    "fis'" : 1580,
    "fisis'" : 1590,
    "geses'" : 1600,
    "ges'" : 1610,
    "g'" : 1620,
    "gis'" : 1630,
    "gisis'" : 1640,
    "aeses'" : 1650,
    "aes'" : 1660,
    "a'" : 1670,
    "ais'" : 1680,
    "aisis'" : 1690,
    "beses'" : 1700,
    "bes'" : 1710,
    "b'" : 1720,
    "bis'" : 1730,
    "bisis'" : 1740,
    "ceses''" : 1750,
    "ces''" : 1760,
    "c''" : 1770,
    "cis''" : 1780,
    "cisis''" : 1790,
    "deses''" : 1800,
    "des''" : 1810,
    "d''" : 1820,
    "dis''" : 1830,
    "disis''" : 1840,
    "eeses''" : 1850,
    "ees''" : 1860,
    "e''" : 1870,
    "eis''" : 1880,
    "eisis''" : 1890,
    "feses''" : 1900,
    "fes''" : 1910,
    "f''" : 1920,
    "fis''" : 1930,
    "fisis''" : 1940,
    "geses''" : 1950,
    "ges''" : 1960,
    "g''" : 1970,
    "gis''" : 1980,
    "gisis''" : 1990,
    "aeses''" : 2000,
    "aes''" : 2010,
    "a''" : 2020,
    "ais''" : 2030,
    "aisis''" : 2040,
    "beses''" : 2050,
    "bes''" : 2060,
    "b''" : 2070,
    "bis''" : 2080,
    "bisis''" : 2090,
    "ceses'''" : 2100,
    "ces'''" : 2110,
    "c'''" : 2120,
    "cis'''" : 2130,
    "cisis'''" : 2140,
    "deses'''" : 2150,
    "des'''" : 2160,
    "d'''" : 2170,
    "dis'''" : 2180,
    "disis'''" : 2190,
    "eeses'''" : 2200,
    "ees'''" : 2210,
    "e'''" : 2220,
    "eis'''" : 2230,
    "eisis'''" : 2240,
    "feses'''" : 2250,
    "fes'''" : 2260,
    "f'''" : 2270,
    "fis'''" : 2280,
    "fisis'''" : 2290,
    "geses'''" : 2300,
    "ges'''" : 2310,
    "g'''" : 2320,
    "gis'''" : 2330,
    "gisis'''" : 2340,
    "aeses'''" : 2350,
    "aes'''" : 2360,
    "a'''" : 2370,
    "ais'''" : 2380,
    "aisis'''" : 2390,
    "beses'''" : 2400,
    "bes'''" : 2410,
    "b'''" : 2420,
    "bis'''" : 2430,
    "bisis'''" : 2440,
    "ceses''''" : 2450,
    "ces''''" : 2460,
    "c''''" : 2470,
    "cis''''" : 2480,
    "cisis''''" : 2490,
    "deses''''" : 2500,
    "des''''" : 2510,
    "d''''" : 2520,
    "dis''''" : 2530,
    "disis''''" : 2540,
    "eeses''''" : 2550,
    "ees''''" : 2560,
    "e''''" : 2570,
    "eis''''" : 2580,
    "eisis''''" : 2590,
    "feses''''" : 2600,
    "fes''''" : 2610,
    "f''''" : 2620,
    "fis''''" : 2630,
    "fisis''''" : 2640,
    "geses''''" : 2650,
    "ges''''" : 2660,
    "g''''" : 2670,
    "gis''''" : 2680,
    "gisis''''" : 2690,
    "aeses''''" : 2700,
    "aes''''" : 2710,
    "a''''" : 2720,
    "ais''''" : 2730,
    "aisis''''" : 2740,
    "beses''''" : 2750,
    "bes''''" : 2760,
    "b''''" : 2770,
    "bis''''" : 2780,
    "bisis''''" : 2790,
    "ceses'''''" : 2800,
    "ces'''''" : 2810,
    "c'''''" : 2820,
    "cis'''''" : 2830,
    "cisis'''''" : 2840,
    "deses'''''" : 2850,
    "des'''''" : 2860,
    "d'''''" : 2870,
    "dis'''''" : 2880,
    "disis'''''" : 2890,
    "eeses'''''" : 2900,
    "ees'''''" : 2910,
    "e'''''" : 2920,
    "eis'''''" : 2930,
    "eisis'''''" : 2940,
    "feses'''''" : 2950,
    "fes'''''" : 2960,
    "f'''''" : 2970,
    "fis'''''" : 2980,
    "fisis'''''" : 2990,
    "geses'''''" : 3000,
    "ges'''''" : 3010,
    "g'''''" : 3020,
    "gis'''''" : 3030,
    "gisis'''''" : 3040,
    "aeses'''''" : 3050,
    "aes'''''" : 3060,
    "a'''''" : 3070,
    "ais'''''" : 3080,
    "aisis'''''" : 3090,
    "beses'''''" : 3100,
    "bes'''''" : 3110,
    "b'''''" : 3120,
    "bis'''''" : 3130,
    "bisis'''''" : 3140,
    #"r" : float('inf'), a rest is not a pitch
    }

#reverse Integer to Lilypond dict
#pitch2ly = {v: k for k, v in ly2pitch.items()} #python2
pitch2ly = dict((ly2pitch[k], k) for k in ly2pitch)


dur2ly = {
   12288 : "\\maxima", # Maxima
   6144 : "\\longa", # Longa
   3072 : "\\breve", # Breve
   1536 : "1", # Whole
   768 : "2", #Half
   384 : "4", #Quarter
   192 : "8", #Eighth
   96 : "16", #Sixteenth
   48 : "32", #1/32
   24 : "64", #1/64
   12 : "128", #1/128
   6 : "256", #1/256
   #Dotted versions are needed mostly for tempo strings.
   1536*1.5 : "1.", # Whole
   768*1.5 : "2.", #Half
   384*1.5 : "4.", #Quarter
   192*1.5 : "8.", #Eighth
   96*1.5 : "16.", #Sixteenth
   48*1.5 : "32.", #1/32
   24*1.5 : "64.", #1/64
   12*1.5 : "128.", #1/128
   6*1.5 : "256.", #1/256
   #Double Dotted versions are needed mostly for tempo strings.
   1536*1.75 : "1..", # Whole
   768*1.75 : "2..", #Half
   384*1.75 : "4..", #Quarter
   192*1.75 : "8..", #Eighth
   96*1.75 : "16..", #Sixteenth
   48*1.75 : "32..", #1/32
   24*1.75 : "64..", #1/64
   12*1.75 : "128..", #1/128
   6*1.75 : "256..", #1/256
}

#reverse Duration to Lilypond dict
ly2dur = dict((dur2ly[k], k) for k in dur2ly)

#; table to translate the human readable "m2" for minor second into steps of fifth left or right in the pillar of fifths
intervall2steps = {
    "P1" : 0,
    "m2" : -5,
    "M2" : 2,
    "m3" : -3,
    "M3" : 4,
    "P4" : -1,
    "T" : 6,
    "P5" : 1,
    "m6" : -4,
    "M6" : 3,
    "m7" : -2,
    "M7" : 5,

    #Augmentend, Diminished
    "A1" : 7,
    "D1" : -7,
    "A2" : 9,
    "D2" : -12,
    "A3" : 11,
    "D3" : -10,
    "A4" : 6,
    "D4" : -8,
    "A5" : 8,
    "D5" : -6,
    "A6" : 10,
    "D6" : -11,
    "A7" : 12,
    "D7" : -9,

    #Double Augmented, Double Diminished
    "AA1" : 14,
    "DD1" : -14,
    "AA2" : 16,
    "DD2" : -19,
    "AA3" : 18,
    "DD3" : -17,
    "AA4" : 13,
    "DD4" : -15,
    "AA5" : 15,
    "DD5" : -13,
    "AA6" : 17,
    "DD6" : -18,
    "AA7" : 19,
    "DD7" : -16,

    #Triple Augmented, Triple Diminished
    "AAA1" : 21,
    "DDD1" : -21,
    "AAA2" : 23,
    "DDD2" : -26,
    "AAA3" : 25,
    "DDD3" : -24,
    "AAA4" : 20,
    "DDD4" : -22,
    "AAA5" : 22,
    "DDD5" : -20,
    "AAA6" : 24,
    "DDD6" : -25,
    "AAA7" : 26,
    "DDD7" : -23,

    #Quadruple Augmented, Quadruple Diminished
    "AAAA1" : 28,
    "DDDD1" : -28,
    "AAAA2" : 30,
    "DDDD2" : -33,
    "AAAA3" : 32,
    "DDDD3" : -31,
    "AAAA4" : 27,
    "DDDD4" : -29,
    "AAAA5" : 29,
    "DDDD5" : -27,
    "AAAA6" : 31,
    "DDDD6" : -32,
    "AAAA7" : 33,
    "DDDD7" : -30,

    #Quintuple Augmented, Quintuple Diminished. Only feses to bisis, the Final Frontier.
    "AAAAA4" : 34,
    "DDDDD5" : -34,

    #IntervalGetSteps variation to stay in key. These can be compared to the special rest symbol. They are only an indicator for other functions.
    #"Q1" : 0,
    #"Q2" : 102,
    #"Q3" : 104,
    #"Q4" : 106,
    #"Q5" : 101,
    #"Q6" : 103,
    #"Q7" : 105,
}

#For a GUI convenience the note names as list.
sortedNoteNameList = [
    "ceses,,," ,
    "ces,,," ,
    "c,,," ,
    "cis,,," ,
    "cisis,,," ,
    "deses,,," ,
    "des,,," ,
    "d,,," ,
    "dis,,," ,
    "disis,,," ,
    "eeses,,," ,
    "ees,,," ,
    "e,,," ,
    "eis,,," ,
    "eisis,,," ,
    "feses,,," ,
    "fes,,," ,
    "f,,," ,
    "fis,,," ,
    "fisis,,," ,
    "geses,,," ,
    "ges,,," ,
    "g,,," ,
    "gis,,," ,
    "gisis,,," ,
    "aeses,,," ,
    "aes,,," ,
    "a,,," ,
    "ais,,," ,
    "aisis,,," ,
    "beses,,," ,
    "bes,,," ,
    "b,,," ,
    "bis,,," ,
    "bisis,,," ,
    "ceses,," ,
    "ces,," ,
    "c,," ,
    "cis,," ,
    "cisis,," ,
    "deses,," ,
    "des,," ,
    "d,," ,
    "dis,," ,
    "disis,," ,
    "eeses,," ,
    "ees,," ,
    "e,," ,
    "eis,," ,
    "eisis,," ,
    "feses,," ,
    "fes,," ,
    "f,," ,
    "fis,," ,
    "fisis,," ,
    "geses,," ,
    "ges,," ,
    "g,," ,
    "gis,," ,
    "gisis,," ,
    "aeses,," ,
    "aes,," ,
    "a,," ,
    "ais,," ,
    "aisis,," ,
    "beses,," ,
    "bes,," ,
    "b,," ,
    "bis,," ,
    "bisis,," ,
    "ceses," ,
    "ces," ,
    "c," ,
    "cis," ,
    "cisis," ,
    "deses," ,
    "des," ,
    "d," ,
    "dis," ,
    "disis," ,
    "eeses," ,
    "ees," ,
    "e," ,
    "eis," ,
    "eisis," ,
    "feses," ,
    "fes," ,
    "f," ,
    "fis," ,
    "fisis," ,
    "geses," ,
    "ges," ,
    "g," ,
    "gis," ,
    "gisis," ,
    "aeses," ,
    "aes," ,
    "a," ,
    "ais," ,
    "aisis," ,
    "beses," ,
    "bes," ,
    "b," ,
    "bis," ,
    "bisis," ,
    "ceses" ,
    "ces" ,
    "c" ,
    "cis" ,
    "cisis" ,
    "deses" ,
    "des" ,
    "d" ,
    "dis" ,
    "disis" ,
    "eeses" ,
    "ees" ,
    "e" ,
    "eis" ,
    "eisis" ,
    "feses" ,
    "fes" ,
    "f" ,
    "fis" ,
    "fisis" ,
    "geses" ,
    "ges" ,
    "g" ,
    "gis" ,
    "gisis" ,
    "aeses" ,
    "aes" ,
    "a" ,
    "ais" ,
    "aisis" ,
    "beses" ,
    "bes" ,
    "b" ,
    "bis" ,
    "bisis" ,
    "ceses'" ,
    "ces'" ,
    "c'" ,
    "cis'" ,
    "cisis'" ,
    "deses'" ,
    "des'" ,
    "d'" ,
    "dis'" ,
    "disis'" ,
    "eeses'" ,
    "ees'" ,
    "e'" ,
    "eis'" ,
    "eisis'" ,
    "feses'" ,
    "fes'" ,
    "f'" ,
    "fis'" ,
    "fisis'" ,
    "geses'" ,
    "ges'" ,
    "g'" ,
    "gis'" ,
    "gisis'" ,
    "aeses'" ,
    "aes'" ,
    "a'" ,
    "ais'" ,
    "aisis'" ,
    "beses'" ,
    "bes'" ,
    "b'" ,
    "bis'" ,
    "bisis'" ,
    "ceses''" ,
    "ces''" ,
    "c''" ,
    "cis''" ,
    "cisis''" ,
    "deses''" ,
    "des''" ,
    "d''" ,
    "dis''" ,
    "disis''" ,
    "eeses''" ,
    "ees''" ,
    "e''" ,
    "eis''" ,
    "eisis''" ,
    "feses''" ,
    "fes''" ,
    "f''" ,
    "fis''" ,
    "fisis''" ,
    "geses''" ,
    "ges''" ,
    "g''" ,
    "gis''" ,
    "gisis''" ,
    "aeses''" ,
    "aes''" ,
    "a''" ,
    "ais''" ,
    "aisis''" ,
    "beses''" ,
    "bes''" ,
    "b''" ,
    "bis''" ,
    "bisis''" ,
    "ceses'''" ,
    "ces'''" ,
    "c'''" ,
    "cis'''" ,
    "cisis'''" ,
    "deses'''" ,
    "des'''" ,
    "d'''" ,
    "dis'''" ,
    "disis'''" ,
    "eeses'''" ,
    "ees'''" ,
    "e'''" ,
    "eis'''" ,
    "eisis'''" ,
    "feses'''" ,
    "fes'''" ,
    "f'''" ,
    "fis'''" ,
    "fisis'''" ,
    "geses'''" ,
    "ges'''" ,
    "g'''" ,
    "gis'''" ,
    "gisis'''" ,
    "aeses'''" ,
    "aes'''" ,
    "a'''" ,
    "ais'''" ,
    "aisis'''" ,
    "beses'''" ,
    "bes'''" ,
    "b'''" ,
    "bis'''" ,
    "bisis'''" ,
    "ceses''''" ,
    "ces''''" ,
    "c''''" ,
    "cis''''" ,
    "cisis''''" ,
    "deses''''" ,
    "des''''" ,
    "d''''" ,
    "dis''''" ,
    "disis''''" ,
    "eeses''''" ,
    "ees''''" ,
    "e''''" ,
    "eis''''" ,
    "eisis''''" ,
    "feses''''" ,
    "fes''''" ,
    "f''''" ,
    "fis''''" ,
    "fisis''''" ,
    "geses''''" ,
    "ges''''" ,
    "g''''" ,
    "gis''''" ,
    "gisis''''" ,
    "aeses''''" ,
    "aes''''" ,
    "a''''" ,
    "ais''''" ,
    "aisis''''" ,
    "beses''''" ,
    "bes''''" ,
    "b''''" ,
    "bis''''" ,
    "bisis''''" ,
    "ceses'''''" ,
    "ces'''''" ,
    "c'''''" ,
    "cis'''''" ,
    "cisis'''''" ,
    "deses'''''" ,
    "des'''''" ,
    "d'''''" ,
    "dis'''''" ,
    "disis'''''" ,
    "eeses'''''" ,
    "ees'''''" ,
    "e'''''" ,
    "eis'''''" ,
    "eisis'''''" ,
    "feses'''''" ,
    "fes'''''" ,
    "f'''''" ,
    "fis'''''" ,
    "fisis'''''" ,
    "geses'''''" ,
    "ges'''''" ,
    "g'''''" ,
    "gis'''''" ,
    "gisis'''''" ,
    "aeses'''''" ,
    "aes'''''" ,
    "a'''''" ,
    "ais'''''" ,
    "aisis'''''" ,
    "beses'''''" ,
    "bes'''''" ,
    "b'''''" ,
    "bis'''''" ,
    "bisis'''''" ,
    ]


#The lilypond chord format is notename:suffix. Eg. c:m6
#The following list, contributed by Stjepan Horvat zvanstefan@gmail.com
#gives us the real notes, assuming the pitch is just c.

chordnamesSuffix = {
    "dur" : "< c e g >",
    "dim" : "< c ees ges >",
    "m" : "< c ees g >",

    #% accords 1ere tierce mineure  4 notes
    "7dim" : "< c ees ges beses >",
    "dim7" : "< c ees ges beses >",
    "aug" : "< c ees gis >",
    "m.5+" : "< c ees gis >",
    "m6" : "< c ees g a >",
    "m7.5" : "< c ees ges bes >",
    "m7" : "< c ees g bes >",
    "m7.5+" : "< c ees gis bes >",
    "m7+" : "< c ees g b >",
    "m5.9" : "< c ees g d' >",

    #% accords 1ere tierce mineure 5 notes et +
    "m6.9" : "< c ees g a d' >",
    "m7.9" : "< c ees g bes des' >",
    "m9" : "< c ees g bes d' >",
    "m9.5" : "< c ees ges bes d' >",
    "m9.7+" : "< c ees g b d' >",
    "m7.9+" : "< c ees g bes dis' >",
    "m7.11" : "< c ees g bes f' >",
    "m7.13" : "< c ees g bes a' >",
    "m11" : "< c ees g bes d' f' >",
    "m11.5" : "< c ees ges bes d' f' >",
    "m13" : "< c ees g bes d' f' a' >",

    #% accords 1ere tierce majeure 3 notes
    "dim" : "< c e ges >",
    "aug" : "< c e gis >",

    #% accords 1ere tierce majeure 4 notes
    "6.9" : "< c e g a d' >",
    #%accord spécial altéré
    "7" : "< c e g bes >",
    "8" : "< c e g bes c'>",
    "6.9" : "< c e g a d'>",
    "maj" : "< c e g b >",
    "maj.5" : "< c e ges b >",
    "maj.5+" : "< c e gis b >",

    "7.5" : "< c e ges bes >",
    "7.5+" : "< c e gis bes >",
    "5.9" : "< c e g d' >",

    #% accords 1ere tierce majeure 5 notes et +
    "maj9" : "< c e g b d' >",

    "9" : "< c e g bes des' >",
    "9.5" : "< c e ges bes des' >",
    "9.5+" : "< c e gis bes des' >",
    "9.11+" : "< c e g bes d' fis' >",
    "9+" : "< c e g bes dis' >",
    "9+.5" : "< c e ges bes dis' >",
    "9+.5+" : "< c e gis bes dis' >",

    "7.11+" : "< c e g bes fis' >",
    "7.13" : "< c e g bes aes' >",

    "maj11" : "< c e g b d' f' >",

    "maj13" : "< c e g b d' a' >",
    "13" : "< c e g bes d' a' >",
    "13.9" : "< c e g bes des' a' >",

    #% accord sus.
    "sus2" : "< c d g >",
    "sus4" : "< c f g >",
    "sus4.7" : "< c f g bes >",
    "sus4.7.9" : "< c f g bes d' >",
}

#You cannot use the following function here because of circular dependencies.
#But it is to fill in the untransposedChords from the api, which is happening indeed.
#Transform the lily string dict to a laborejo pitch dict

untransposedChords = {}
