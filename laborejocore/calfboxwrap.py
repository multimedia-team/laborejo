# -*- coding: utf-8 -*-
from calfbox import cbox #calfbox is a globally installed module.
from time import sleep
from threading import Thread
from random import choice
from warnings import warn
from laborejocore import exceptions

"""This file creates classes that are compatible with libsmf calls.
You can override the default libsmf instances and functions to
generate live output through calfbox instead. Only use it temporary
though.
Laborejo expects libsmf to be the standard playback module.
In the future we might even change the smf module and classes
to be a parameter.

There is a module check in items and core to see if cbox is installed.
cboxwrap is only imported after that. If you want to import from
another place you need to check if cbox exists. Laborejo should run
without any playback features."""

class Calfbox(object):
    """Drop in replacement for libsmf smf, the module directly.
    We can't name calfboxwrapper.py smf because that would lead to
    conflicts. And I also don't want to overwrite and reload modules."""
    def __init__(self, ppqn, soundfont, autoconnect = True, midiInPort = None, clientName = "Laborejo"):
        self.clientName = clientName
        cbox.init_engine("")
        cbox.Config.set("io", "client_name", self.clientName)
        #cbox.Config.set("debug", "jack_transport", "1")
        try:
            cbox.start_audio()
        except OSError:
            warn("JACK is not running. Please start the Jack Audio connection kit or don't start the Laborejo audio engine.")
            raise exceptions.JackRunningError #We still go on. api.calfboxinit is smart enough from this point on.

        self.keepRolling = True #Can be changed at runtime. Decides if the jack transport is stopped if our playback ends or if it continues.
        self._SMF = self.Smf(ppqn)
        self.transport = cbox.Transport
        #cbox.do_cmd("/master/set_tempo", None, [120]) # Set tempo
        self.scene = self._SMF.calfBoxDocument.get_scene()
        self.newInternalInstrument(soundfont)
        self.cbox = cbox
        self.autoconnect = autoconnect
        self.jack_midi_in_is_open = True #A switch to 'mute' midi in for Laborejo without destroying any connections. The midi in data is just discarded.
        self.states = States()
        #The executeCC function receives the new second midi byte ("Velocity") and the velocity value exactly before this midi byte, so you can compare with the old one.
        self.executeCC = {} #populate with key[CCnumber]:function(midiVelocity, velocityBeforetheCurrent). If there is any controller change.
        self.executeCC[0x40] = self.states.setDuringChordEntry #pedal down

        self.running = False #playback status. One step nearer than the playback status from cbox
        self.negativePlaybackTickOffset = 0 # A temp value during playback.
        self.allMidiOutPorts = set() #program wide storage of all midi out uniqueContainerNames. The pretty names, used for the jack port names.
        self.uuidToUniqueContainerName = {}

        if self.autoconnect:
            outputs = cbox.JackIO.get_ports(".*", cbox.JackIO.AUDIO_TYPE, cbox.JackIO.PORT_IS_SINK | cbox.JackIO.PORT_IS_PHYSICAL)
            cbox.JackIO.port_connect(self.clientName+":out_1", outputs[0]) #system:playback_1
            cbox.JackIO.port_connect(self.clientName+":out_2", outputs[1]) #system:playback_2
        if midiInPort and self.autoconnect:
            cbox.JackIO.port_connect(midiInPort, self.clientName+":midi") #maybe that is already connected or the port was wrong.

    def __deepcopy__(self, memo):
        """Just a precaution. Don't copy anything. Just return itself."""
        return self

    def newInternalInstrument(self, filepath):
        self.scene.clear()
        self.instrument = self.scene.add_new_instrument_layer("internalGeneralMidi", "fluidsynth").get_instrument()
        self.instrument.cmd("/engine/load_soundfont", None, filepath)

    def Event(self, event):
        """libsmf converts to binary when creating an event. Here we just
        return what we got."""
        return event

    def SMF(self, ppqn):
        """ppqn is ignored. It is 384 laborejo wide anyway"""
        return self._SMF

    def play(self, startTick):
        self.transport.stop()
        self.transport.seek_ppqn(startTick)
        self.transport.play()

    def stop(self):
        self.transport.stop()

    def panic(self):
        """Also does stop"""
        self.transport.panic()

    def getUniqueMidiOutName(self, uniqueContainerName):
        while uniqueContainerName in self.allMidiOutPorts:
            uniqueContainerName += choice('abcdefghijklmnoprstuvwyxz')
        return uniqueContainerName

    def newMidiOutPort(self, uniqueContainerName):
        """Problem: The uniqueContainerName is only unique per
        File/WS. If a second file creates the same names we must
        intercept it. Does not connect anything. Just creates a port
        which remains until the track is deleted"""

        uniqueContainerName = self.getUniqueMidiOutName(uniqueContainerName)

        self.allMidiOutPorts.add(uniqueContainerName)

        uuid = cbox.JackIO.create_midi_output(uniqueContainerName)
        self.uuidToUniqueContainerName[uuid] = uniqueContainerName
        return uuid

    def renameMidiOut(self, uuid, uniqueContainerName):
        cbox.JackIO.rename_midi_output(uuid, self.getUniqueMidiOutName(uniqueContainerName))

    def closeMidiOutput(self, calfboxUuid):
        cbox.JackIO.delete_midi_output(calfboxUuid)
        self.allMidiOutPorts.remove(self.uuidToUniqueContainerName[calfboxUuid])
        del self.uuidToUniqueContainerName[calfboxUuid]

    def connectMidiOutput(self, uuid, externalPort):
        """A wrapper to test if autoconnection is on or off.
        Session managers don't like auto-connection.
        externalPort is a string "x:y"""
        if self.autoconnect:
            cbox.JackIO.autoconnect_midi_output(uuid, externalPort)

    def getMidiPorts(self):
        l = cbox.JackIO.get_ports(".*", cbox.JackIO.MIDI_TYPE, cbox.JackIO.PORT_IS_SINK)
        l.remove(self.clientName+":midi")
        return l

    def getAudioPorts(self):
        l = cbox.JackIO.get_ports(".*", cbox.JackIO.AUDIO_TYPE, cbox.JackIO.PORT_IS_SOURCE)
        l.remove(self.clientName+":out_1")
        l.remove(self.clientName+":out_2")
        return l

    def sendNoteEvent(self, midiPitch, programNumber, jackMode = None): #TODO: Bank numbers. also needed in api._calfboxPlayNote
        """called by api._calfboxPlayNote"""
        #TODO:
        #Krzysztof Foltman
        #I don't know if Python threads are proper threads, but if they are, you can possibly run into problems - cbox is not designed for multiple control threads.
        #Better have a settable callback in the core to allow deferred execution.
        #Anything that's not properly integrating with application's main event loop is going to be of questionable use

        def simpleThread():
            #cbox.send_midi_event(0xB0+self.chanRightNow, 0x00, self.instrumentTupleRightNow[0], output = self.jackSinkRightNow) #Bank Change
            cbox.send_midi_event(0xC0+self.chanRightNow, self.patchRightNow, output = self.jackSinkRightNow) #Program Change
            cbox.send_midi_event(0x90+self.chanRightNow, midiPitch, 100, output = self.jackSinkRightNow)
            sleep(0.5)
            cbox.send_midi_event(0x80+self.chanRightNow, midiPitch, 100, output = self.jackSinkRightNow)

        self.jackSinkRightNow = jackMode #is a cbox track UUID if not None.
        self.chanRightNow = self.jackChannelCache if jackMode else self.smfChannelCache
        #self.instrumentTupleRightNow = instrumentTuple #is already jack or not jack by the api.
        self.patchRightNow = programNumber #is already jack or not by the api.
        Thread(target=simpleThread).start() #Quits automatically after the simpleThread() function has ended.

    class Smf(object):
        """Drop-In for smfStructure. used in Laborejo as smf.SMF(384)
         The whole midi document"""
        def __init__(self, ppqn):
            self.ppqn = ppqn
            #song and scene are singletons within a document, and get_song() returns the song singleton
            self.calfBoxDocument = cbox.Document
            #self.calfBoxDocument.dump() #gives you the list of all objects in the document, so it might be used to debug some object lifecycle issues
            self.song = self.calfBoxDocument.get_song()
            self.song.clear() # Delete all the tracks and patterns #TODO. Really necessary for a new song?
            #self.song.set_loop(pattern_len, pattern_len) # Stop the song at the end
            self.tracks = []
            self.transport = cbox.Transport
            self.cbox = cbox
            self.lastMaxTicks = -1 #It is -1 instead of None because it needs to be compared right from the start, before the first midi data was generated. -1 is always smaller then any song length, which could be 0 in theory, so it is a safe value.
            self.cachedRepeatsOfLongestTrack = [] #a temp value during transport rolling
            self.cachedTempoTrack = []
            #master = smfStructure.transport.status() #'pos', 'pos_ppqn', 'tempo', 'timesig', 'sample_rate'


        def __deepcopy__(self, memo):
            """Just a precaution. Don't copy anything. Just return itself."""
            return self

        def save(self, filepath):
            """used by the api functions
            Is not implemented and probably will never be used
            since we have libsmf to create midi files.
            Calling this functions means you tried to export midi
            and somehow ended up using calfbox. Error. """
            raise RuntimeError("Attempt to save a midi file thru Calfbox. This is not possible and should not have happened. Only save to files with the libsmf module""")

        def add_track(self, backendTrack):
            """Add a new track to the midi structure"""
            cboxTr = CalfboxTrack(backendTrack)
            self.tracks.append(cboxTr)
            return cboxTr

        def update(self, jackMode):
            """take the currently cached CalfboxTracks and convert them
            into realtime data
            """
            lengths = []
            self.song.clear()
            for cboxTr in self.tracks:
                binaryTrack = self.song.add_track()
                if jackMode:
                    binaryTrack.set_external_output(cboxTr.backendTrack.calfboxUuid) #this replaces the internal routing as well. So we don't need to deactivate sf2 output manually. Has NOTHING to do with external synth/sampler connections.
                # Create a new pattern object using events from the blob
                pattern = self.song.pattern_from_blob(cboxTr.pblob, cboxTr.length)
                lengths.append(cboxTr.length)

                # Add an instance (clip) of the pattern to the track at position 0
                # The clip will contain the whole pattern (it is also possible to insert
                # a single slice of the pattern)
                clip = binaryTrack.add_clip(0, 0, cboxTr.length, pattern)  #we do nothing with the return value for now.

                #Handle failed to parsed events
                #Add tempo changes to the master calfbox track
                for ev, pulses in cboxTr.failedToParseList:
                    #print (ev[:3])
                    if ev[:3] == [0xFF, 0x51, 0x03]: #tempo signature
                        #print ("Tempo:", ev[:3], ev[3:])
                        #numeric = ev[3] + 256 * ev[4] + 65536 * ev[5]
                        numeric = 65536 * ev[3] + 256 * ev[4] + ev[5]
                        #print (pulses, ":CalfTempo:", float(int(60000000 / numeric)))
                        #self.song.set_mti(int(pulses), 8 * float(int(60000000 / numeric))) #TODO: x8 is only because calfbox uses another quarter tick value than laborejo
                        t = 8 * 60000000 / numeric
                        self.song.set_mti(pulses, t) #TODO: x8 is only because calfbox uses another quarter tick value than laborejo
                        self.cachedTempoTrack.append((pulses, t))
                    #elif ev[0] & 0xF0 == 0xC0: #patch /instrument change, cut the channel out and see if it is a program change.
                    #    instrument.engine.set_patch(1, 0)

            # Stop the song at the end
            max_pattern_len = max(lengths) + 384 #one quarter note safety
            self.song.set_loop(max_pattern_len, max_pattern_len)
            self.song.update_playback()  #Send the updated song data to the realtime thread
            self.lastMaxTicks = max_pattern_len
            return max_pattern_len

        def getSongLengthInSeconds(self):
            return self.transport.ppqn_to_samples(int(self.lastMaxTicks)) / self.transport.status().sample_rate

        def scaleTempoLive(self, factor):
            """Scale the tempo. Works during playback.
            Everytime you use factor it will be scaled from 1 again,
            not from the current tempo"""
            for pulses, t in self.cachedTempoTrack:
                self.song.set_mti(pulses, factor * t)
            self.song.update_playback()  #Send the updated song data to the realtime thread


class CalfboxTrack(object):
    """Drop in replacement for libsmf smfTrack"""
    def __init__(self, backendTrack):
        self.pblob = bytes()  # Create a binary blob that contains the MIDI events
        self.length = 0 #set during exportPlayback
        self.failedToParseList = []
        self.backendTrack = backendTrack

    def __deepcopy__(self, memo):
        """Just a precaution. Don't copy anything. Just return itself."""
        return self

    #def add_event(smf.Event(smfEvent), pulses=self.parentWorkspace.score.activeCursor.tickindex + message[1])
    def add_event(self, laborejoEvent, pulses):
        """This is supposed to directly add something on the final
        midi stream.

        Pulses is the start point. It is an int,
        often constructed by the cursor index plus an offset,
        negative or positve

        convertedEvent is in fact not converted but the original
        python data structue. In lismf add_event wants an binary
        encoded event, but we need the raw data."""
        #(384, 0x90, 64, 127),
        #StartTick, type, pitch, velocity
        try:
            if len(laborejoEvent) == 3:
                smfType, param1, param2 = laborejoEvent
            else:
                smfType, param1  = laborejoEvent  #program change has only 2 parameters
                param2 = 0
            #print ("True:", laborejoEvent, pulses)
            self.pblob += cbox.Pattern.serialize_event(pulses, smfType, param1, param2)
        except:
            #print ("False:", laborejoEvent, pulses)
            self.failedToParseList.append((laborejoEvent, pulses))

class States(object):
    def __init__(self):
        """Midi in modes"""
        self.pitchbend = 64 #middle
        self.cc = dict((i, 0) for i in range(128))
        self.cc[0x07] = 127 #Volume is high

        self.duringChordEntry = False #set AFTER the first note with pedal down. So that you can push the pedal down and then begin to enter a chord.

    def setDuringChordEntry(self, midiCCValue, velocityBeforetheCurrent):
        if  self.duringChordEntry and not self.digitalCC(midiCCValue):
            self.duringChordEntry = False
            return "right" #will be execute by the api
        #no else. Don't touch the Value


    def digitalCC(self, cc):
        if self.cc[cc] >= 64:
            return True
        else:
            return False
