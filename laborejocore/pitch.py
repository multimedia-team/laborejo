# -*- coding: utf-8 -*-

def plain(pitch):
    """ Extract the note from a note-number, without any octave but with the tailing zero.
    This means we double-use the lowest octave as abstract version."""
    #Dividing through the octave, 350, results in the number of the octave and the note as remainder.
    return divmod(pitch, 350)[1]

def octave(pitch):
    """Return the octave of given note. Lowest 0 is X,,,"""
    return divmod(pitch, 350)[0]

def toOctave(pitch, octave):
    """Take a plain note and give the octave variant. Starts with 0"""
    return pitch + octave * 350

def toWhite(pitch):
    """Return the natural, "white key" version of a note"""
    # /50 without rest to get an multiplier that will...
    # *50 return the "...eses" version which is a multiple of 50.
    # +20 from "...eses" to white key.
    return divmod(pitch, 50)[0] * 50 + 20

def mirror(pitch, axis):
    """Calculate the distance between the pitch and the axis-pitch and
    set the new pitch twice as far, which creates a mirror effect:
    Half the distancen is object->mirror and then mirror->object on the
    other side again."""
    #1420 = c', 1520 = e', 1620 = g'

    #1420 to 1520 is a third so the mirror would be a fifth.
    #1420 + 2 * (1520 - 1420)
    #1420 + 2 * 100
    #1420 + 200 = 1620
    return pitch + 2 * (axis - pitch)

def diatonicIndex(pitch):
    """Return an int between 0 and 6, resembling the diatonic position
    of the given pitch without octave, accidentals. 0 is c"""
    return divmod(plain(toWhite(pitch)), 50)[0]

def diatonicIndexToPitch(index, octave):
    """supports indices from - to +. """
    while index < 0:
        index += 7 #plus one octave. index -1 becomes b
        octave -= 1
    while index > 6:
        index -= 7 #minus one octave. index 7 becomes c again.
        octave += 1
    return toOctave(index * 50 + 20, octave) #0 is cesces, 20 is c

def upStepsFromRoot(root, pitch):
    """Like diatonicIndex but assumes a different root than C. So it is:
    'stepcount upward in white keys from root to pitch'.
    It is always assumed it should go up. if root > pitch an error will
    be raised"""
    if root > pitch:
        raise ValueError("rootPitch", root, "must be lower than", pitch)
    elif root == pitch:
        return 0
    else:
        r = diatonicIndex(root)
        p = diatonicIndex(pitch)
        if plain(root) > plain(pitch): #we have an octave break  g' to d''
            p += 7
        return p - r

def interval(pitch1, pitch2):
    """Return the distance between two pitches as steps in the pillar of fifths.
    Intervals are tuplets with two members x = 1,0  #fifth in the same octave
    x[0] = interval. Steps in the pillar of fifths.
    x[1] = octave in between.  Octave is always >= 0 because an interval has no direction."""
    if pitch1 > pitch2: #bring the notes in right order. We want to calculate from higher to lower
        return (pillarOfFifth.index(plain(pitch1)) - pillarOfFifth.index(plain(pitch2)), octave(pitch1 - pitch2))
    else:
        return (pillarOfFifth.index(plain(pitch2)) - pillarOfFifth.index(plain(pitch1)), octave(pitch2 - pitch1))

def intervalUp(pitch, interval, midiIn = False):
    """Return a pitch which is _interval_ higher than the given pitch"""
    octv = octave(pitch)
    indexNumber = pillarOfFifth.index(plain(pitch))
    targetPitch = pillarOfFifth[indexNumber + interval[0]] + octv*350 #return to the old octave
    if not midiIn and targetPitch < pitch:  #the new note is lower than where we started. This is wrong. +1 octave!. Reason: it was the break between two octaves.
        targetPitch += 350
    targetPitch += interval[1]*350
    return targetPitch

def intervalDown(pitch, interval):
    """Return a pitch which is _interval_ lower than the given pitch"""
    #print intervalUp(20, (-12,1)) #c,,, to deses,,
    octv = octave(pitch)
    indexNumber = pillarOfFifth.index(plain(pitch))
    targetPitch = pillarOfFifth[indexNumber - interval[0]] + octv*350  #return to the old octave.
    if targetPitch > pitch: #the new note is higher than where we started. This is wrong. -1 octave!. Reason: it was the break between two octaves.
        targetPitch -= 350
    targetPitch -= interval[1]*350
    return targetPitch

def intervalAutomatic(originalPitch, rootPitch, targetPitch):
    """Return the original pitch, transposed by the interval
    between rootPitch and targetPitch"""
    iv = interval(rootPitch, targetPitch)
    if rootPitch >= targetPitch:
        return intervalDown(originalPitch, iv)
    elif rootPitch < targetPitch:
        return intervalUp(originalPitch, iv)
    else:
        raise ValueError("Interval", rootPitch, targetPitch, iv, "could not handle pitches")
        return rootPitch

def toScale(pitch, keysig): #the old diatonicNote
    """Return a pitch which is the in-scale variant of the given one. Needs a Key Signature as second parameter"""
    #Keysig (self.root, self.keysigtuplet, self.explicit)
    workingcopy = keysig.keysigtuplet[:] #create a copy. else the original keysig would be changed.
    workingcopy.sort() # sort first.
    mod = workingcopy[tonalDistanceFromC(pitch)]   # tonalDistanceFromC has the same syntax as the keysig step/list position. mod becomes the (step, value) tuplet
    return toWhite(pitch) + mod[1]

def scaleStatus(pitch, keysig):
    """Return if a note is natural, sharp, flat etc.
    Ret-value is a Triplet:
    (difference to white, diff to keysig, keysig version of this note)
    Same syntax as Key Signature:
    -20 double flat, 0 natural, +20 d-sharp.
    It is optional to give a keysig as parameter to check the note
    in a hypothetical different keysig-enviroment."""
    diffFromWhite = pitch - toWhite(pitch)
    diffFromKey = pitch - toScale(pitch, keysig)
    keyVersionOfThisNote = sorted(keysig.keysigtuplet)[diatonicIndex(pitch)]
    return (diffFromWhite, diffFromKey, keyVersionOfThisNote[1])

def toMidi(pitch):
    """Return a midi pitch."""
    octOffset = (octave(pitch) +1) * 12 #twelve tones per midi octave
    return octOffset +  halfToneDistanceFromC(pitch)

def midiToPitch(midipitch, keysig):
    """Convert a midi pitch to internal pitch.
    Nearest to pillar of fifth"""
    calc = divmod(midipitch, 12)
    midioctave = calc[0]
    pitch = calc[1]
    table = [
            20, #c
            60, #des
            70, #d
            110, #ees
            120, #e
            170, #f
            180, #fis
            220, #g
            260, #aas
            270, #a
            310, #bes
            320, #b / h
            ]

    #Sample note Gis/Aes in D Major should become Gis, which is the same as Fis in C Maj.
    midiRoot = toMidi(keysig.root) - 12 #in D Major thats 2 (halftone steps away from C)
    simpleConverted = toOctave(table[pitch], midioctave -1) #in D Maj it is still Aes
    fromC = halfToneDistanceFromC(simpleConverted)
    soundingDistanceToKeysigRoot =  fromC - midiRoot #8 half tone steps - 2 from root = 6 (Tritonus)
    #We now need to know how the 6 steps / tritonus look like in the current keysignature/root enviroment. This is told by the table above.
    pitchInCMaj = table[soundingDistanceToKeysigRoot] #fis

    #Interval between keysig root and c.
    intervalToC = interval(20, keysig.root)
    newInterval = (intervalToC[0], midioctave -1 ) #tuplets are immutable

    #Transpose it by this interval
    pitchInOriginalKey =  intervalUp(pitchInCMaj, newInterval, midiIn = True)

    #Back to the correct Octave. This is what we wanted.
    #bugfix/workaround. 320 b/h in keysigs <= Bes Major is "ces", but somehow one octave to low. Compensate here.
    if pillarOfFifth.index(keysig.root) <= 13 and pitch == 11:
        return pitchInOriginalKey + 350
    else:
        return pitchInOriginalKey
    return pitchInOriginalKey

def sharpen(pitch):
    """Sharpen the pitch until double crossed"""
    sharper = pitch + 10
    if toWhite(sharper) == toWhite(pitch): #still the same base note?
        return sharper
    else:
        return pitch #too sharp, do nothing.

def flatten(pitch):
    """Flatten the pitch until double flat"""
    flatter = pitch - 10
    if toWhite(flatter) == toWhite(pitch): #still the same base note?
        return flatter
    else:
        return pitch #too flat, do nothing.

def tonalDistanceFromC(pitch):
    """Tonal step from C. One from 7 (0-6).
    1330 ("ais") ->  1320 ("a") -> 270 ("a" plain) -> 5 (5th notestep from 0 = c)"""
    return divmod(plain(toWhite(pitch)), 50)[0]

def halfToneDistanceFromC(pitch):
    """Return the half-tone step distance from C. The "sounding" interval"""
    number = plain(pitch)
    return {
        #00 : 10, # ceses,,, -> bes
        #10 : 11, # ces,,, -> b

        00 : -2, # ceses,,, -> bes
        10 : -1, # ces,,, -> b
        20 : 0, # c,,,
        30 : 1, # cis,,,
        40 : 2, # cisis,,, -> d ...
        50 : 0, # deses,,,
        60 : 1, # des,,,
        70 : 2, # d,,,
        80 : 3, # dis,,,
        90 : 4, # disis,,,
        100 : 2, # eeses,,,
        110 : 3, # ees,,,
        120 : 4, # e,,,
        130 : 5, # eis,,,
        140 : 6, # eisis,,,
        150 : 3, # feses,,,
        160 : 4, # fes,,,
        170 : 5, # f,,,
        180 : 6, # fis,,,
        190 : 7, # fisis,,,
        200 : 5, # geses,,,
        210 : 6, # ges,,,
        220 : 7, # g,,,
        230 : 8, # gis,,,
        240 : 9, # gisis,,,
        250 : 7, # aeses,,,
        260 : 8, # aes,,,
        270 : 9, # a,,,
        280 : 10, # ais,,,
        290 : 11, # aisis,,,
        300 : 9, # beses,,,
        310 : 10, # bes,,,
        320 : 11, # b,,,
        330 : 12, # bis,,,
        340 : 13, # bisis,,,
        #330 : 0, # bis,,,
        #340 : 1, # bisis,,,
        }[number]

def halfToneDistance(pitch1, pitch2):
    """Return the halftone distance bewtween the two pitches."""
    return toMidi(pitch1) - toMidi(pitch2)

def enharmonicFlat(pitch):
    """Change the pitch to a same sounding variant, different ortography. A to Beses.
    This is the function where my arrangenment of notes fails.
    We have to check 3 extra cases for eis, bis and bes."""
    htd = halfToneDistanceFromC(pitch)
    htd40 = halfToneDistanceFromC(pitch + 40)
    htd30 = halfToneDistanceFromC(pitch + 30)
    #We search for a note which has the same sounding/halftone distance from C.
    ##These are either +40 or +30 due the irregular note order. If there is no note we are already as sharp/flat as possible
    if htd == htd40 or htd == 12 or htd40 ==  -1 or htd40 == -2 or htd == 13: # special rule for irregular notes         #13 is bisis to cis
        return pitch + 40
    elif htd == htd30: #all other notes
        return pitch + 30
    else: #else just stay on the same note
        return pitch

def enharmonicSharp(pitch):
    """Change the pitch to a same sounding variant, different ortography. A to Gisiis.
    See enharmonicFlat docstring."""
    htd = halfToneDistanceFromC(pitch)
    htd40 = halfToneDistanceFromC(pitch - 40)
    htd30 = halfToneDistanceFromC(pitch - 30)
    #We search for a note which has the same sounding/halftone distance from C.
    ##These are either +40 or +30 due the irregular note order. If there is no note we are already as sharp/flat as possible
    if htd == htd40 or htd40 == 12 or htd40 == 13 or htd == 0: # special rule for irregular notes
        return pitch - 40
    elif htd == htd30: #all other notes
        return pitch - 30
    else: #else just stay on the same note
        return pitch

def pitchlistToScaleAsIntervals(pitchlist):
    """Assuming the lowest pitch it the first pitch.
    The lowest pitch is considered the root note.
    Return a list of 8 positions. The first is the octave, a bool that
    shows if any higher octave variants of the root are in here.
    For all other positions, if present, return the difference to white."""
    scale =  [False, None, None, None, None, None, None ] #for each note in the chord
    rootPitch = pitchlist[0]
    for eachNote in pitchlist:
        if plain(eachNote) == plain(rootPitch) and not eachNote == rootPitch: #is it an octave variant of the root?
            scale[0] = True
        else: #not the octave. find out which position in a hypothetical scale with the rootPitch as root the current note has.
            index = upStepsFromRoot(rootPitch, eachNote)
            if not index == 0: #discard any variant of the root note. c' root and cis' is not allowed.
                diff = eachNote - toWhite(eachNote) #the difference. You can use this value directly to make a white pitch to the current one.
                scale[index] =  diff #replace the scale position with the difference to white. If there is more than one variant of the note the last one (higher) is taken.
    return scale

# Lists and Tables

#Ordered pitches in fifths.
#To calculate real and correct intervals you need the pillar of fifth with 35 steps for each of the 31 realistic notenames (and 4 unrealistic ones)"""
pillarOfFifth =[
    #pillarOfFifth.index(260) -> 11
    150, #feses 0
    0, #ceses   1
    200, #geses 2
    50, #deses  3
    250, #aeses 4
    100, #eeses 5
    300, #beses 6
    160, #fes   7
    10, #ces    8
    210, #ges   9
    60, #des    10
    260, #aes   11
    110, #ees   12
    310, #bes   13
    170, #f     14
    20, #c      15
    220, #g     16
    70, #d      17
    270, #a     18
    120, #e     19
    320, #b     20
    180, #fis   21
    30, #cis    22
    230, #gis   23
    80, #dis    24
    280, #ais   25
    130, #eis   26
    330, #bis   27
    190, #fisis 28
    40, #cisis  29
    240, #gisis 30
    90, #disis  31
    290, #aisis 32
    140, #eisis 33
    340, #bisis 34
    ]
