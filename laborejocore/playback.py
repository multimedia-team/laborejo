# -*- coding: utf-8 -*-
import math, struct

def durToMidi(ticks):
    """n =  -(log(y/3)-9*log(2))/log(2)"""
    partOne = -1 * (math.log(ticks/3) - 9 * math.log(2))
    return math.floor( partOne / math.log(2) )

def bpmToMpqn(beatsPerMinute, referenceTicks,  microseconds_per_minute = 60000000):
    """MICROSECONDS_PER_MINUTE = 60000000
    BPM = MICROSECONDS_PER_MINUTE / MPQN
    MPQN = MICROSECONDS_PER_MINUTE / BPM
    mpqn means Microseconds per Quarternote."""
    bpm = beatsPerMinute * 384 / referenceTicks #always recalculate to quarter notes per minute
    mpqn = microseconds_per_minute / bpm # MPQN: 0 - 8355711.
    return math.floor(mpqn)


def midiTempo(beatsPerMinute, referenceTicks):
    mpqn = bpmToMpqn(beatsPerMinute, referenceTicks)
    #y = (str(hex(mpqn))[2:])
    #a = ('0x' + y[-2:])
    #b = ('0x' + y[-4:-2])
    #c = ('0x' + y[-6:-4])
    #return ([0xFF, 0x51, 0x03, int(c, 16), int(b, 16), int(a, 16)],0)

    a, b, c, d = struct.pack("<l", mpqn)
    return ([0xFF, 0x51, 0x03, c, b, a], 0)


def lyDurationLogToTicks(durationLog):
    return 2**(8 - durationLog) * 6

ticksToLyDurationLogDict = {
    12288: -3, # Maxima
    6144: -2, # Longa
    3072: -1, # Breve
    1536: 0, # Whole
    768: 1, #Half
    384: 2, #Quarter
    192: 3, #Eighth
    96: 4, #Sixteenth
    48: 5, #1/32
    24: 6, #1/64
    12: 7, #1/128
    6: 8, #1/256
    }

def ticksToLyDurationLog(ticks):
    return ticksToLyDurationLogDict[ticks]

def midiToDur(midiTicks):
    if midiTicks in _midiToDurDict:
        return _midiToDurDict[midiTicks]
    else:
        raise IndexError(midiTicks, "cannot be converted to Laborejo duration")
        #TODO

_midiToDurDict = {
#return value is a tuple with 3 positions: [0]base [1]dots [3]tuplet-factor as list of tuples (numerator, denominator)
    #Plain
    12288: (12288,0,[]), # Maxima
    6144: (6144,0,[]), # Longa
    3072: (3072,0,[]), # Breve
    1536: (1536,0,[]), # Whole
    768: (768,0,[]), #Half
    384: (384,0,[]), #Quarter
    192: (192,0,[]), #Eighth
    96: (96,0,[]), #Sixteenth
    48: (38,0,[]), #1/32
    24: (24,0,[]), #1/64
    12: (12,0,[]), #1/128
    6: (6,0,[]), #1/256

    #Dotted
    12288+12288/2: (12288,1,[]), # Maxima
    6144+6144/2: (6144,1,[]), # Longa
    3072+3072/2: (3072,1,[]), # Breve
    1536+1536/2: (1536,1,[]), # Whole
    768+768/2: (768,1,[]), #Half
    384+384/2: (384,1,[]), #Quarter
    192+129/2: (192,1,[]), #Eighth
    96+96/2: (96,1,[]), #Sixteenth
    48+48/2: (38,1,[]), #1/32
    24+24/2: (24,1,[]), #1/64
    12+12/2: (12,1,[]), #1/128
    6+6/2: (6,1,[]), #1/256

    #Double Dotted
    12288+12288/2+12288/4: (12288,2,[]), # Maxima
    6144+6144/2+6144/4: (6144,2,[]), # Longa
    3072+3072/2+3072/4: (3072,2,[]), # Breve
    1536+1536/2+1536/4: (1536,2,[]), # Whole
    768+768/2+768/4: (768,2,[]), #Half
    384+384/2+384/4: (384,2,[]), #Quarter
    192+129/2+129/4: (192,2,[]), #Eighth
    96+96/2+96/4: (96,2,[]), #Sixteenth
    48+48/2+48/4: (38,2,[]), #1/32
    24+24/2+24/4: (24,2,[]), #1/64
    12+12/2+12/4: (12,2,[]), #1/128
    6+6/2+6/4: (6,2,[]), #1/256

    #Triplets
    12288*2/3: (12288,0,[(2,3)]), # Maxima
    6144*2/3: (6144,0,[(2,3)]), # Longa
    3072*2/3: (3072,0,[(2,3)]), # Breve
    1536*2/3: (1536,0,[(2,3)]), # Whole
    768*2/3: (768,0,[(2,3)]), #Half
    384*2/3: (384,0,[(2,3)]), #Quarter
    192*2/3: (192,0,[(2,3)]), #Eighth
    96*2/3: (96,0,[(2,3)]), #Sixteenth
    48*2/3: (38,0,[(2,3)]), #1/32
    24*2/3: (24,0,[(2,3)]), #1/64
    12*2/3: (12,0,[(2,3)]), #1/128
    6*2/3: (6,0,[(2,3)]), #1/256

    #Quintuplets
    12288*4/5: (12288,0,[(4,5)]), # Maxima
    6144*4/5: (6144,0,[(4,5)]), # Longa
    3072*4/5: (3072,0,[(4,5)]), # Breve
    1536*4/5: (1536,0,[(4,5)]), # Whole
    768*4/5: (768,0,[(4,5)]), #Half
    384*4/5: (384,0,[(4,5)]), #Quarter
    192*4/5: (192,0,[(4,5)]), #Eighth
    96*4/5: (96,0,[(4,5)]), #Sixteenth
    48*4/5: (38,0,[(4,5)]), #1/32
    24*4/5: (24,0,[(4,5)]), #1/64
    12*4/5: (12,0,[(4,5)]), #1/128
    6*4/5: (6,0,[(4,5)]), #1/256


    #7th
    12288*4/7: (12288,0,[(4,7)]), # Maxima
    6144*4/7: (6144,0,[(4,7)]), # Longa
    3072*4/7: (3072,0,[(4,7)]), # Breve
    1536*4/7: (1536,0,[(4,7)]), # Whole
    768*4/7: (768,0,[(4,7)]), #Half
    384*4/7: (384,0,[(4,7)]), #Quarter
    192*4/7: (192,0,[(4,7)]), #Eighth
    96*4/5: (96,0,[(4,7)]), #Sixteenth
    48*4/7: (38,0,[(4,7)]), #1/32
    24*4/7: (24,0,[(4,7)]), #1/64
    12*4/7: (12,0,[(4,7)]), #1/128
    6*4/7: (6,0,[(4,7)]), #1/256

    #9th
    12288*8/9: (12288,0,[(8,9)]), # Maxima
    6144*8/9: (6144,0,[(8,9)]), # Longa
    3072*8/9: (3072,0,[(8,9)]), # Breve
    1536*8/9: (1536,0,[(8,9)]), # Whole
    768*8/9: (768,0,[(8,9)]), #Half
    384*8/9: (384,0,[(8,9)]), #Quarter
    192*8/9: (192,0,[(8,9)]), #Eighth
    96*4/5: (96,0,[(8,9)]), #Sixteenth
    48*8/9: (38,0,[(8,9)]), #1/32
    24*8/9: (24,0,[(8,9)]), #1/64
    12*8/9: (12,0,[(8,9)]), #1/128
    6*8/9: (6,0,[(8,9)]), #1/256
    }

instrumentList = [
    "Acoustic Grand Piano",
    "Bright Acoustic Piano",
    "Electric Grand Piano",
    "Honky-tonk Piano",
    "Electric Piano 1",
    "Electric Piano 2",
    "Harpsichord",
    "Clavinet",

    "Celesta",
    "Glockenspiel",
    "Music Box",
    "Vibraphone",
    "Marimba",
    "Xylophone",
    "Tubular Bells",
    "Dulcimer",

    "Drawbar Organ",
    "Percussive Organ",
    "Rock Organ",
    "Church Organ",
    "Reed Organ",
    "Accordion",
    "Harmonica",
    "Tango Accordion",

    "Acoustic Guitar (nylon)",
    "Acoustic Guitar (steel)",
    "Electric Guitar (jazz)",
    "Electric Guitar (clean)",
    "Electric Guitar (muted)",
    "Overdriven Guitar",
    "Distortion Guitar",
    "Guitar harmonics",

    "Acoustic Bass",
    "Electric Bass (finger)",
    "Electric Bass (pick)",
    "Fretless Bass",
    "Slap Bass 1",
    "Slap Bass 2",
    "Synth Bass 1",
    "Synth Bass 2",

    "Violin",
    "Viola",
    "Cello",
    "Contrabass",
    "Tremolo Strings",
    "Pizzicato Strings",
    "Orchestral Harp",
    "Timpani",

    "String Ensemble 1",
    "String Ensemble 2",
    "Synth Strings 1",
    "Synth Strings 2",
    "Choir Aahs",
    "Voice Oohs",
    "Synth Voice",
    "Orchestra Hit",

    "Trumpet",
    "Trombone",
    "Tuba",
    "Muted Trumpet",
    "French Horn",
    "Brass Section",
    "Synth Brass 1",
    "Synth Brass 2",

    "Soprano Sax",
    "Alto Sax",
    "Tenor Sax",
    "Baritone Sax",
    "Oboe",
    "English Horn",
    "Bassoon",
    "Clarinet",

    "Piccolo",
    "Flute",
    "Recorder",
    "Pan Flute",
    "Blown Bottle",
    "Shakuhachi",
    "Whistle",
    "Ocarina",

    "Lead 1 (square)",
    "Lead 2 (sawtooth)",
    "Lead 3 (calliope)",
    "Lead 4 (chiff)",
    "Lead 5 (charang)",
    "Lead 6 (voice)",
    "Lead 7 (fifths)",
    "Lead 8 (bass + lead)",

    "Pad 1 (new age)",
    "Pad 2 (warm)",
    "Pad 3 (polysynth)",
    "Pad 4 (choir)",
    "Pad 5 (bowed)",
    "Pad 6 (metallic)",
    "Pad 7 (halo)",
    "Pad 8 (sweep)",

    "FX 1 (rain)",
    "FX 2 (soundtrack)",
    "FX 3 (crystal)",
    "FX 4 (atmosphere)",
    "FX 5 (brightness)",
    "FX 6 (goblins)",
    "FX 7 (echoes)",
    "FX 8 (sci-fi)",

    "Sitar",
    "Banjo",
    "Shamisen",
    "Koto",
    "Kalimba",
    "Bag pipe",
    "Fiddle",
    "Shanai",

    "Tinkle Bell",
    "Agogo",
    "Steel Drums",
    "Woodblock",
    "Taiko Drum",
    "Melodic Tom",
    "Synth Drum",

    "Reverse Cymbal",
    "Guitar Fret Noise",
    "Breath Noise",
    "Seashore",
    "Bird Tweet",
    "Telephone Ring",
    "Helicopter",
    "Applause",
    "Gunshot",
]

instrumentListNumbered = [
    "1 Acoustic Grand Piano",
    "2 Bright Acoustic Piano",
    "3 Electric Grand Piano",
    "4 Honky-tonk Piano",
    "5 Electric Piano 1",
    "6 Electric Piano 2",
    "7 Harpsichord",
    "8 Clavinet",

    "9 Celesta",
    "10 Glockenspiel",
    "11 Music Box",
    "12 Vibraphone",
    "13 Marimba",
    "14 Xylophone",
    "15 Tubular Bells",
    "16 Dulcimer",

    "17 Drawbar Organ",
    "18 Percussive Organ",
    "19 Rock Organ",
    "20 Church Organ",
    "21 Reed Organ",
    "22 Accordion",
    "23 Harmonica",
    "24 Tango Accordion",

    "25 Acoustic Guitar (nylon)",
    "26 Acoustic Guitar (steel)",
    "27 Electric Guitar (jazz)",
    "28 Electric Guitar (clean)",
    "29 Electric Guitar (muted)",
    "30 Overdriven Guitar",
    "31 Distortion Guitar",
    "32 Guitar harmonics",

    "33 Acoustic Bass",
    "34 Electric Bass (finger)",
    "35 Electric Bass (pick)",
    "36 Fretless Bass",
    "37 Slap Bass 1",
    "38 Slap Bass 2",
    "39 Synth Bass 1",
    "40 Synth Bass 2",

    "41 Violin",
    "42 Viola",
    "43 Cello",
    "44 Contrabass",
    "45 Tremolo Strings",
    "46 Pizzicato Strings",
    "47 Orchestral Harp",
    "48 Timpani",

    "49 String Ensemble 1",
    "50 String Ensemble 2",
    "51 Synth Strings 1",
    "52 Synth Strings 2",
    "53 Choir Aahs",
    "54 Voice Oohs",
    "55 Synth Voice",
    "56 Orchestra Hit",

    "57 Trumpet",
    "58 Trombone",
    "59 Tuba",
    "60 Muted Trumpet",
    "61 French Horn",
    "62 Brass Section",
    "63 Synth Brass 1",
    "64 Synth Brass 2",

    "65 Soprano Sax",
    "66 Alto Sax",
    "67 Tenor Sax",
    "68 Baritone Sax",
    "69 Oboe",
    "70 English Horn",
    "71 Bassoon",
    "72 Clarinet",

    "73 Piccolo",
    "74 Flute",
    "75 Recorder",
    "76 Pan Flute",
    "77 Blown Bottle",
    "78 Shakuhachi",
    "79 Whistle",
    "80 Ocarina",

    "81 Lead 1 (square)",
    "82 Lead 2 (sawtooth)",
    "83 Lead 3 (calliope)",
    "84 Lead 4 (chiff)",
    "85 Lead 5 (charang)",
    "86 Lead 6 (voice)",
    "87 Lead 7 (fifths)",
    "88 Lead 8 (bass + lead)",

    "89 Pad 1 (new age)",
    "90 Pad 2 (warm)",
    "91 Pad 3 (polysynth)",
    "92 Pad 4 (choir)",
    "93 Pad 5 (bowed)",
    "94 Pad 6 (metallic)",
    "95 Pad 7 (halo)",
    "96 Pad 8 (sweep)",

    "97 FX 1 (rain)",
    "98 FX 2 (soundtrack)",
    "99 FX 3 (crystal)",
    "100 FX 4 (atmosphere)",
    "101 FX 5 (brightness)",
    "102 FX 6 (goblins)",
    "103 FX 7 (echoes)",
    "104 FX 8 (sci-fi)",

    "105 Sitar",
    "106 Banjo",
    "107 Shamisen",
    "108 Koto",
    "109 Kalimba",
    "110 Bag pipe",
    "111 Fiddle",
    "112 Shanai",

    "113 Tinkle Bell",
    "114 Agogo",
    "115 Steel Drums",
    "116 Woodblock",
    "117 Taiko Drum",
    "118 Melodic Tom",
    "119 Synth Drum",

    "120 Reverse Cymbal",
    "121 Guitar Fret Noise",
    "122 Breath Noise",
    "123 Seashore",
    "124 Bird Tweet",
    "125 Telephone Ring",
    "126 Helicopter",
    "127 Applause",
    "128 Gunshot",
]
