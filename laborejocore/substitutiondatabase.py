# -*- coding: utf-8 -*-
from laborejocore.core import Container
from laborejocore.items import Start, End, Chord
from laborejocore.lilypond import ly2pitch


"""
Each Substitution is a dict-entry. The tag/name can be freely chosen,
as well as the variable name. Only respect the following naming rules.

The dict value is a tuple of two: (Optional Group, Container())
The group is only used for documentation and for possible guis. You can
use None or an Empty String if you want.

The substitution has always 1420, c' as reference pitch.

Naming rules:
Each substitution has to start with subst_ , no exception.
If it is not some special like a test or temporary you have to
give a prefix if the reference tone is meant to be the
functional root or the base note.
subst_functional_foobar
subst_bass_foobar

The names come from the Functional Symbols vs. Figured Bass Syntax.
Best is to define both so a user can choose the way he likes.

Example in C Major:
Alberti Bass for the Tonic is {c8 g e g}. This is the same for either
functional or figured bass view.

Alberti Bass Dominant7th is {d8 g f g}.
Figured bass would save it exactly like this so the substitution is
"on d" which means you have to use it on the note d.

Functional would be {g c' bes c'}. Because all subst are in C we now
have to assume that C is the Dominant Chord. We want a dominant alberti
which starts on the 5th of the Dominant.
Result: We can now use this substitution for the dominant root note.
In C Major we would use this on a G to finally get {d8 g f g}.
It is the same as the symbol D7/5 . Dominant7th with its 5th in bass.

It is advisable, if you only choose to create one variant, to choose
the functional approach for harmonic patterns and the basenote approach
for melodic patterns.

It is also nice to add some indicator which scale positions are used:
subst_functional_albertiTonic_1535
we have the name here followed by 1535 to make it easy to remember
"c g e g"

This is of course dependent on bass vs functional again.
{d8 g f g} can be functional 5878 or bass 2545

9 and 0 are second and third plus octave. But this is only a a naming
convention.
"""

def build_database(parentWorkspace):
    subst_laborejo = {}

    def create(nameString, chordTuples, group = ""):
        """You can use chordTuples or just simple pitch ints. Don't mix.
        Also possible: absolute lilpond pitches as a list"""
        n = Container(parentWorkspace = parentWorkspace)
        if type(chordTuples[0]) is int:
            n.container = [Start(parentWorkspace, n)] + [Chord(parentWorkspace, 384, tup) for tup in chordTuples] + [End(parentWorkspace, n)]
        elif type(chordTuples[0]) is str:
            n.container = [Start(parentWorkspace, n)] + [Chord(parentWorkspace, 384, ly2pitch[tup]) for tup in chordTuples] + [End(parentWorkspace, n)]
        else:
            n.container = [Start(parentWorkspace, n)] + [Chord(parentWorkspace, tup[0], tup[1]) for tup in chordTuples] + [End(parentWorkspace, n)]
        n.group = group if group else ""
        n._uniqueContainerName = "subst_" + nameString
        subst_laborejo[nameString] = n


    create("1231", [1420, 1470, 1520, 1420] ,"boundaryNotes")
    create("8768", [1420, 1370, 1320, 1420] ,"boundaryNotes")
    create("123", [1420, 1470, 1520] ,"triplets")
    create("12345", [1420, 1470, 1520, 1570, 1620] ,"quintuplets")
    create("321", [1420, 1370, 1320] ,"triplets")
    create("r211", [(384, 1420), (192, 1420), (192, 1420), ] , "sameNote")
    create("135653", "c' e' g' a' g' e'".split() , "arpeggios")
    create("FF-2oct", "c' d' e' g' c'' d'' e'' g'' c''' g'' e'' d'' c'' g' e' d'".split() , "arpeggios")
    create("FF-4oct", """c' d' e' g'
                         c'' d'' e'' g''
                         c''' d''' e''' g'''
                         c'''' d'''' e'''' g''''

                         c''''' g''''  e'''' d''''
                         c'''' g''' e''' d'''
                         c''' g'' e'' d''
                         c'' g' e' d'
                         """.split() , "arpeggios")
    return subst_laborejo
