# -*- coding: utf-8 -*-
import os, json, warnings
from laborejocore import items, core, cursor, helper

def save(workspace, filepath):
    try:
        exported = workspace.exportSave()
        f = open(filepath, 'w', encoding="utf-8")
        save = json.dumps(exported, sort_keys=True, indent=2)
        f.write(save)
        return filepath
    except:
        raise RuntimeError("Could not create save data. Your original file remains intact. Check the console output for other errors")

# Functions for load:
global asTemplate
asTemplate = False

class LoadSession():
    """During a load session an instance of this will be used to store
    temp data such as keeping track of already processed containers
    and items"""

    def __init__(self, currentWorkspace):
        self.asTemplate = False
        self.currentParentContainer = None #Changed by addContainer()
        self.nonTrackContainers = {} #key is the container instance, value is the instance count.
        self.alreadyProcessedContainers = {}
        self.alreadyLoadedHighInstanceCountItems = {} #Create a dict which will be used when loading items. Needed for linked items.
        self.currentWorkspace = currentWorkspace #filled in by load() directly
        self.filepath = ""

def Note(saveStructure):
    new = items.Note(saveStructure[1]["pitch"])
    if "tie" in saveStructure[1]: new.tie = saveStructure[1]["tie"]
    if "finger" in saveStructure[1]: new.finger = saveStructure[1]["finger"]
    if "durationOverride" in saveStructure[1]: new.durationOverride = saveStructure[1]["durationOverride"]
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    return new

def EmptyNote(saveStructure):
    new = items.EmptyNote()
    addPlayback(new, saveStructure)
    if "lilypond" in saveStructure[1]: new.lilypond = saveStructure[1]["lilypond"]
    return new

def Item(saveStructure):
    new = items.Item(currentLoadSession.currentWorkspace)
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    return _return(new)

def WaitForChord(saveStructure):
    new = items.WaitForChord(currentLoadSession.currentWorkspace)
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "lilystring" in saveStructure[1]: new.lilystring = saveStructure[1]["lilystring"]
    if "pre" in saveStructure[1]: new.pre = saveStructure[1]["pre"]
    return _return(new)

def Chord(saveStructure):
    new = items.Chord(currentLoadSession.currentWorkspace, 0,0) #Just use any value that is a valid pitch and duration. The whole notelist and duration will be replaced in addItem and addInternal anyway.
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "substitution" in saveStructure[1]: new.substitution = saveStructure[1]["substitution"]
    return _return(new)

def Appending(saveStructure):
    new = items.Appending(currentLoadSession.currentWorkspace)
    #Custom Lilypond. EmptyDict class needs not to be saved because its fine on init.
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "lilypond" in saveStructure[1]: new.lilypond = saveStructure[1]["lilypond"] #Whoever uses this violates Appending extremly. But technically it is correct to save and restore that value.
    return new

def Start(saveStructure):
    new = items.Start(currentLoadSession.currentWorkspace, currentLoadSession.currentParentContainer)
    #Custom Lilypond. EmptyDict class needs not to be saved because its fine on init.
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "lilypond" in saveStructure[1]: new.lilypond = saveStructure[1]["lilypond"] #Whoever uses this violates Start extremly. But technically it is correct to save and restore that value.
    return new

def End(saveStructure):
    new = items.End(currentLoadSession.currentWorkspace, currentLoadSession.currentParentContainer) #the parent container instance cannot be saved. But we recreate it everytime here.
    #Custom Lilypond. EmptyDict class needs not to be saved because its fine on init.
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "lilypond" in saveStructure[1]: new.lilypond = saveStructure[1]["lilypond"] #Whoever uses this violates End extremly. But technically it is correct to save and restore that value.
    return new

def Rest(saveStructure):
    new = items.Rest(currentLoadSession.currentWorkspace, saveStructure[1]["base"], saveStructure[1]["pitch"])
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "pitch" in saveStructure[1]: new.pitch = saveStructure[1]["pitch"]
    return _return(new)

def MultiMeasureRest(saveStructure):
    new = items.MultiMeasureRest(currentLoadSession.currentWorkspace, saveStructure[1]["measures"], saveStructure[1]["lilysyntax"])
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "measures" in saveStructure[1]: new.measures = saveStructure[1]["measures"]
    if "lilysyntax" in saveStructure[1]: new.lilysyntax = saveStructure[1]["lilysyntax"]
    return _return(new)

def Placeholder(saveStructure):
    new = items.Placeholder(currentLoadSession.currentWorkspace, saveStructure[1]["measures"])
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "measures" in saveStructure[1]: new.measures = saveStructure[1]["measures"]
    return _return(new)

def Upbeat(saveStructure):
    new = items.Upbeat(currentLoadSession.currentWorkspace, saveStructure[1]["base"])
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    return _return(new)

def Markup(saveStructure):
    new = items.Markup(currentLoadSession.currentWorkspace, saveStructure[1]["lilystring"])
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "position" in saveStructure[1]: new.position = saveStructure[1]["position"]
    if "lilystring" in saveStructure[1]: new.lilystring = saveStructure[1]["lilystring"]
    return _return(new)

def SpecialBarline(saveStructure):
    new = items.SpecialBarline(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "repeat" in saveStructure[1]: new.repeat = saveStructure[1]["repeat"]
    return _return(new)

def RepeatOpen(saveStructure):
    new = items.RepeatOpen(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "repeat" in saveStructure[1]: new.repeat = saveStructure[1]["repeat"]
    return _return(new)

def RepeatClose(saveStructure):
    new = items.RepeatClose(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "repeat" in saveStructure[1]: new.repeat = saveStructure[1]["repeat"]
    return _return(new)

def RepeatCloseOpen(saveStructure):
    new = items.RepeatCloseOpen(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "repeat" in saveStructure[1]: new.repeat = saveStructure[1]["repeat"]
    return _return(new)

def GotoSegno(saveStructure):
    new = items.GotoSegno(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "disableRepeat" in saveStructure[1]: new.disableRepeat = saveStructure[1]["disableRepeat"]
    if "until" in saveStructure[1]: new.until = saveStructure[1]["until"]
    return _return(new)

def GotoCapo(saveStructure):
    new = items.GotoCapo(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "disableRepeat" in saveStructure[1]: new.disableRepeat = saveStructure[1]["disableRepeat"]
    if "until" in saveStructure[1]: new.until = saveStructure[1]["until"]
    return _return(new)

def GotoCoda(saveStructure):
    new = items.GotoCoda(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    return _return(new)

def Segno(saveStructure):
    new = items.Segno(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    return _return(new)

def Coda(saveStructure):
    new = items.Coda(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    return _return(new)

def Fine(saveStructure):
    new = items.Fine(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    return _return(new)

def AlternateEnd(saveStructure):
    new = items.AlternateEnd(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "endings" in saveStructure[1]: new.endings = saveStructure[1]["endings"]
    if "_text" in saveStructure[1]: new._text = saveStructure[1]["_text"]
    return _return(new)

def AlternateEndClose(saveStructure):
    new = items.AlternateEndClose(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    return _return(new)

def PedalSustainChange(saveStructure):
    new = items.PedalSustainChange(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    return _return(new)

def Clef(saveStructure):
    new = items.Clef(currentLoadSession.currentWorkspace, saveStructure[1]["clefString"], saveStructure[1]["octave"])
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "clefString" in saveStructure[1]: new.clefString = saveStructure[1]["clefString"]
    if "octave" in saveStructure[1]: new.octave = saveStructure[1]["octave"]
    return _return(new)

def TimeSignature(saveStructure):
    new = items.TimeSignature(currentLoadSession.currentWorkspace, saveStructure[1]["nominator"], saveStructure[1]["denominator"])
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "nominator" in saveStructure[1]: new.nominator = saveStructure[1]["nominator"]
    if "denominator" in saveStructure[1]: new.denominator = saveStructure[1]["denominator"]
    return _return(new)

def KeySignature(saveStructure):
    new = items.KeySignature(currentLoadSession.currentWorkspace, saveStructure[1]["root"], saveStructure[1]["keysigtuplet"], saveStructure[1]["explicit"])
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "root" in saveStructure[1]: new.root = saveStructure[1]["root"]
    if "keysigtuplet" in saveStructure[1]: new.keysigtuplet = saveStructure[1]["keysigtuplet"]
    if "explicit" in saveStructure[1]: new.explicit = saveStructure[1]["explicit"]
    return _return(new)

def TempoSignature(saveStructure):
    new = items.TempoSignature(currentLoadSession.currentWorkspace, saveStructure[1]["beatsPerMinute"], saveStructure[1]["referenceTicks"])
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "beatsPerMinute" in saveStructure[1]: new.beatsPerMinute = saveStructure[1]["beatsPerMinute"]
    if "referenceTicks" in saveStructure[1]: new.referenceTicks = saveStructure[1]["referenceTicks"]
    if "tempostring" in saveStructure[1]: new.tempostring = saveStructure[1]["tempostring"]
    return _return(new)

def TempoModification(saveStructure):
    new = items.TempoModification(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "bpmModificator" in saveStructure[1]: new.bpmModificator = saveStructure[1]["bpmModificator"]
    return _return(new)

def DynamicSignature(saveStructure):
    new = items.DynamicSignature(currentLoadSession.currentWorkspace, saveStructure[1]["_expression"] , saveStructure[1]["lilystring"])
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "_expression" in saveStructure[1]: new._expression = saveStructure[1]["_expression"]
    if "lilystring" in saveStructure[1]: new.lilystring = saveStructure[1]["lilystring"]
    return _return(new)

def SubitoDynamicSignature(saveStructure):
    new = items.SubitoDynamicSignature(currentLoadSession.currentWorkspace, saveStructure[1]["expression"] , saveStructure[1]["lilystring"])
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "expression" in saveStructure[1]: new.expression = saveStructure[1]["expression"]
    if "lilystring" in saveStructure[1]: new.lilystring = saveStructure[1]["lilystring"]
    return _return(new)

def PerformanceSignature(saveStructure):
    new = items.PerformanceSignature(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    addPerformance(new, saveStructure)
    return _return(new)

def ChannelChangeRelative(saveStructure):
    new = items.ChannelChangeRelative(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "number" in saveStructure[1]: new.number = saveStructure[1]["number"]
    return _return(new)

def ChannelChangeAbsolute(saveStructure):
    new = items.ChannelChangeAbsolute(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "number" in saveStructure[1]: new.number = saveStructure[1]["number"]
    return _return(new)

def ProgramChangeRelative(saveStructure):
    new = items.ProgramChangeRelative(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "number" in saveStructure[1]: new.number = saveStructure[1]["number"]
    return _return(new)

def ProgramChangeAbsolute(saveStructure):
    new = items.ProgramChangeAbsolute(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "number" in saveStructure[1]: new.number = saveStructure[1]["number"]
    return _return(new)

def InstrumentChange(saveStructure):
    new = items.InstrumentChange(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "smfPatch" in saveStructure[1]: new.smfPatch = saveStructure[1]["smfPatch"]
    if "smfBank" in saveStructure[1]: new.smfBank = saveStructure[1]["smfBank"]
    if "smfVolume" in saveStructure[1]: new.smfVolume = saveStructure[1]["smfVolume"]
    if "jackPatch" in saveStructure[1]: new.jackPatch = saveStructure[1]["jackPatch"]
    if "jackBank" in saveStructure[1]: new.jackBank = saveStructure[1]["jackBank"]
    if "jackVolume" in saveStructure[1]: new.jackVolume = saveStructure[1]["jackVolume"]
    if "longInstrumentName" in saveStructure[1]: new.longInstrumentName = saveStructure[1]["longInstrumentName"]
    if "shortInstrumentName" in saveStructure[1]: new.shortInstrumentName = saveStructure[1]["shortInstrumentName"]
    return _return(new)

def SlurOn(saveStructure):
    new = items.SlurOn(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "lilystring" in saveStructure[1]: new.lilystring = saveStructure[1]["lilystring"]
    return _return(new)

def SlurOff(saveStructure):
    new = items.SlurOff(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    return _return(new)

def PhrasingSlurOn(saveStructure):
    new = items.PhrasingSlurOn(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    if "lilystring" in saveStructure[1]: new.lilystring = saveStructure[1]["lilystring"]
    return _return(new)

def PhrasingSlurOff(saveStructure):
    new = items.PhrasingSlurOff(currentLoadSession.currentWorkspace, )
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addItem(new, saveStructure)
    return _return(new)

def Container(saveStructure, substitution=False):
    """Standalone Container
    nonTrackContainers is the score-wide shortcut to all standalone-
    containers. key is the container instance, value is the instance count.

    If Substitution is True don't add to the score container
    index."""
    if "_uniqueContainerName" in saveStructure[1] and saveStructure[1]["_uniqueContainerName"] in currentLoadSession.alreadyProcessedContainers:
        old = currentLoadSession.alreadyProcessedContainers[saveStructure[1]["_uniqueContainerName"]]
        currentLoadSession.nonTrackContainers[old].append(True) #add one to the instance count
        return currentLoadSession.alreadyProcessedContainers[saveStructure[1]["_uniqueContainerName"]]
    else:
        new = core.Container(parentWorkspace = currentLoadSession.currentWorkspace)
        addLilypond(new, saveStructure)
        addPlayback(new, saveStructure)
        addInternal(new, saveStructure)
        addContainer(new, saveStructure)
        if not substitution:
            currentLoadSession.alreadyProcessedContainers[new._uniqueContainerName] = new
            currentLoadSession.nonTrackContainers[new] = [True] #add as new container with one instance count (one "True")
        return new

def Track(saveStructure):
    new = core.Track(parentWorkspace = currentLoadSession.currentWorkspace, name = saveStructure[1]["_uniqueContainerName"])
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addContainer(new, saveStructure) #this converts all items and nested containers
    if "staffType" in saveStructure[1]: new.staffType = saveStructure[1]["staffType"]
    if "nrOfLines" in saveStructure[1]: new.nrOfLines = saveStructure[1]["nrOfLines"]
    if "mergeWithUpper" in saveStructure[1]: new.mergeWithUpper = saveStructure[1]["mergeWithUpper"]
    if "longInstrumentName" in saveStructure[1]: new.longInstrumentName = saveStructure[1]["longInstrumentName"]
    if "shortInstrumentName" in saveStructure[1]: new.shortInstrumentName = saveStructure[1]["shortInstrumentName"]
    if "trackDirectiveWith" in saveStructure[1]: new.trackDirectiveWith = saveStructure[1]["trackDirectiveWith"]
    if "hideLilypond" in saveStructure[1]: new.hideLilypond = saveStructure[1]["hideLilypond"]
    if "voicePreset" in saveStructure[1]: new.voicePreset = saveStructure[1]["voicePreset"]
    if "lyrics" in saveStructure[1]: new.lyrics = saveStructure[1]["lyrics"]
    if "forceLyricsAbove" in saveStructure[1]: new.forceLyricsAbove = saveStructure[1]["forceLyricsAbove"]
    if "lilypondSize" in saveStructure[1]: new.lilypondSize = saveStructure[1]["lilypondSize"]
    if "exportExtractPart" in saveStructure[1]: new.exportExtractPart = saveStructure[1]["exportExtractPart"]
    if "ambitus" in saveStructure[1]: new.ambitus = saveStructure[1]["ambitus"]

    if "smfMute" in saveStructure[1]: new.smfMute = saveStructure[1]["smfMute"]
    if "smfSolo" in saveStructure[1]: new.smfSolo = saveStructure[1]["smfSolo"]

    if "smfChannel" in saveStructure[1]: new.smfChannel = saveStructure[1]["smfChannel"]
    if "smfPatch" in saveStructure[1]: new.smfPatch = saveStructure[1]["smfPatch"]
    if "smfBank" in saveStructure[1]: new.smfBank = saveStructure[1]["smfBank"]
    if "smfVolume" in saveStructure[1]: new.smfVolume = saveStructure[1]["smfVolume"]
    if "smfPan" in saveStructure[1]: new.smfPan = saveStructure[1]["smfPan"]

    if "jackChannel" in saveStructure[1]: new.jackChannel = saveStructure[1]["jackChannel"]
    if "jackPatch" in saveStructure[1]: new.jackPatch = saveStructure[1]["jackPatch"]
    if "jackBank" in saveStructure[1]: new.jackBank = saveStructure[1]["jackBank"]
    if "jackVolume" in saveStructure[1]: new.jackVolume = saveStructure[1]["jackVolume"]
    if "jackPan" in saveStructure[1]: new.jackPan = saveStructure[1]["jackPan"]

    #selected items are not saved
    return new

def Score(saveStructure):
    new = core.Score(parentWorkspace = currentLoadSession.currentWorkspace) #will be filled in later by load, at the end of this file.
    addLilypond(new, saveStructure)
    addPlayback(new, saveStructure)
    addInternal(new, saveStructure)
    addContainer(new, saveStructure)

    #Score Parameters
    if "subtext" in saveStructure[1]: new.subtext = saveStructure[1]["subtext"]
    if "splitByToHeader" in saveStructure[1]: new.splitByToHeader = saveStructure[1]["splitByToHeader"]

    if "header" in saveStructure[1]: new.header = convertHeader(saveStructure[1]["header"])

    if "defaultPerformanceSignature" in saveStructure[1]:
        defaultSig = items.PerformanceSignature(parentWorkspace = currentLoadSession.currentWorkspace)
        addLilypond(defaultSig, saveStructure[1]["defaultPerformanceSignature"])
        addPlayback(defaultSig, saveStructure[1]["defaultPerformanceSignature"])
        addInternal(defaultSig, saveStructure[1]["defaultPerformanceSignature"])
        #addItem(new, saveStructure) we don't need custom item parameters here. Default ones will be generated anyway.
        addPerformance(defaultSig, saveStructure[1]["defaultPerformanceSignature"])
        new.defaultPerformanceSignature = defaultSig #Add to the score

    if "substitutions" in saveStructure[1]:
        new.substitutions = {}
        #saveStructure[1]["substitutions"] is a dictionary.
        for movSubstKey, movSubstContainer in saveStructure[1]["substitutions"].items():
            movSubstContainer[0] = "SubstitutionContainer"
            new.substitutions[movSubstKey] =  convert(movSubstContainer)

    #Create a new set of Cursors
    new.cursors = [] #reset first
    for track in new.container:
        new.cursors.append(cursor.Cursor(track = track, parentScore = new))
    new.activeCursor = new.cursors[0]

    #Add shortcuts to all standalone containers
    new.nonTrackContainers = currentLoadSession.nonTrackContainers
    return new

#Helper- or Subfunctions
def addLilypond(new, saveStructure):
    """In place modifications to recreate Lilypond class info"""
    if "lilypond" in saveStructure[1]:new.lilypond = saveStructure[1]["lilypond"]
    if "directivePre" in saveStructure[1]: new.directivePre = saveStructure[1]["directivePre"]
    if "directiveMid" in saveStructure[1]: new.directiveMid = saveStructure[1]["directiveMid"]
    if "directivePst" in saveStructure[1]: new.directivePst = saveStructure[1]["directivePst"]
    if "hidden" in saveStructure[1]: new.hidden = saveStructure[1]["hidden"]
    if "transparent" in saveStructure[1]: new.transparent = saveStructure[1]["transparent"]

def addInternal(new, saveStructure):
    """In place modifications to recreate Internal class info"""
    if "base" in saveStructure[1]: new.base = saveStructure[1]["base"]
    if "tuplets" in saveStructure[1]: new.tuplets = saveStructure[1]["tuplets"]
    if "dots" in saveStructure[1]: new.dots = saveStructure[1]["dots"]
    if "scaleFactorNumerator" in saveStructure[1]: new.scaleFactorNumerator = saveStructure[1]["scaleFactorNumerator"]
    if "scaleFactorDenominator" in saveStructure[1]: new.scaleFactorDenominator = saveStructure[1]["scaleFactorDenominator"]

def addPlayback(new, saveStructure):
    """In place modifications to recreate Playback class info"""
    if "split" in saveStructure[1]: new.split = saveStructure[1]["split"]
    if "_durationFactor" in saveStructure[1]: new._durationFactor = saveStructure[1]["_durationFactor"]
    if "_velocityFactor" in saveStructure[1]: new._velocityFactor = saveStructure[1]["_velocityFactor"]
    if "instructionPre" in saveStructure[1]: new.instructionPre = saveStructure[1]["instructionPre"]
    if "instructionPre" in saveStructure[1]: new.instructionPre = saveStructure[1]["instructionPre"]
    if "instructionPst" in saveStructure[1]: new.instructionPst = saveStructure[1]["instructionPst"]
    if "channelOffset" in saveStructure[1]: new.channelOffset = saveStructure[1]["channelOffset"]
    if "forceChannel" in saveStructure[1]: new.forceChannel = saveStructure[1]["forceChannel"]
    if "programOffset" in saveStructure[1]: new.programOffset = saveStructure[1]["programOffset"]
    if "forceProgram" in saveStructure[1]: new.forceProgram = saveStructure[1]["forceProgram"]

def addPerformance(new, saveStructure):
    """In place modifications to recreate Playback class info
    Performance Signature is used twice, one workspace default,
    one object. So we place this directly in load."""
    if "name" in saveStructure[1]: new.name = saveStructure[1]["name"]
    if "staccato" in saveStructure[1]: new.staccato = saveStructure[1]["staccato"]
    if "tenuto" in saveStructure[1]: new.tenuto = saveStructure[1]["tenuto"]
    if "dynamics" in saveStructure[1]: new.dynamics = saveStructure[1]["dynamics"]
    if "legatoScaling" in saveStructure[1]: new.legatoScaling = saveStructure[1]["legatoScaling"]
    if "slurEndScaling" in saveStructure[1]: new.slurEndScaling = saveStructure[1]["slurEndScaling"]
    if "nonLegatoScaling" in saveStructure[1]: new.nonLegatoScaling = saveStructure[1]["nonLegatoScaling"]
    if "nonLegatoHumanizing" in saveStructure[1]: new.nonLegatoHumanizing = saveStructure[1]["nonLegatoHumanizing"]
    if "velocityHumanizing" in saveStructure[1]: new.velocityHumanizing = saveStructure[1]["velocityHumanizing"]
    if "shortfermata" in saveStructure[1]: new.shortfermata = saveStructure[1]["shortfermata"]
    if "fermata" in saveStructure[1]: new.fermata = saveStructure[1]["fermata"]
    if "longfermata" in saveStructure[1]: new.longfermata = saveStructure[1]["longfermata"]
    if "verylongfermata" in saveStructure[1]: new.verylongfermata = saveStructure[1]["verylongfermata"]
    if "velocityMetricalMain" in saveStructure[1]: new.velocityMetricalMain = saveStructure[1]["velocityMetricalMain"]
    if "velocityMetricalNothing" in saveStructure[1]: new.velocityMetricalNothing = saveStructure[1]["velocityMetricalNothing"]
    if "velocityMetricalSecondary" in saveStructure[1]: new.velocityMetricalSecondary = saveStructure[1]["velocityMetricalSecondary"]
    if "swingMod" in saveStructure[1]: new.swingMod = saveStructure[1]["swingMod"]
    if "swingLayer" in saveStructure[1]: new.swingLayer = saveStructure[1]["swingLayer"]

#Main functions
def convertLilypondDefinitions(saveStructure):
    new = items.LilypondDefinitions()
    if "lilypond" in saveStructure[1]: new.lilypond = saveStructure[1]["lilypond"]
    if "directivePre" in saveStructure[1]: new.directivePre = saveStructure[1]["directivePre"]
    if "directiveMid" in saveStructure[1]: new.directiveMid = saveStructure[1]["directiveMid"]
    if "directivePst" in saveStructure[1]: new.directivePst = saveStructure[1]["directivePst"]
    if "container" in saveStructure[1]: new.container = saveStructure[1]["container"]
    return new

def convertHeader(saveStructure):
    def convert(saveStructure):
        if saveStructure[0] == "Header":
            new = items.Header(parentWorkspace = currentLoadSession.currentWorkspace)
            addLilypond(new, saveStructure)
            addInternal(new, saveStructure)
            if "data" in saveStructure[1]:
                new.data = saveStructure[1]["data"] #a simple dict
            else: #TODO: convert old files. Delete soon.
                print ("[" + currentLoadSession.filepath + "] Old metadata format detected. Save before editing the file to convert to new format")
                for key, metavalue in saveStructure[1]["container"]: #list of tuplets. [(metavalue-key, metavalue-instance), ...]
                    new.data[key] = convert(metavalue)
            return new

        elif saveStructure[0] == "Metavalue":
            return saveStructure[1]["value"]
        else:
            print ("Warning: Saved structure not recognized: ", saveStructure[0])
            #go on nevertheless.

    return convert(saveStructure)  #This sets a recursive process in motion which recreates a score

def addItem(new, saveStructure):
    if "instanceCount" in saveStructure[1]: new.instanceCount = saveStructure[1]["instanceCount"]
    if "uniqueName" in saveStructure[1]: new.uniqueName = saveStructure[1]["uniqueName"]
    if "_chordsymbols" in saveStructure[1]: new._chordsymbols = saveStructure[1]["_chordsymbols"]
    if "_figures" in saveStructure[1]: new._figures = saveStructure[1]["_figures"]
    if "tempoKeyword" in saveStructure[1]: new.tempoKeyword = saveStructure[1]["tempoKeyword"]
    if "_triggerString" in saveStructure[1]: new._triggerString = saveStructure[1]["_triggerString"]

    #Selection was not saved.
    noteList = []
    for note in saveStructure[1]["notelist"]:
        noteList.append(convert(note))  #recursive call
    new.notelist = noteList

def addContainer(new, saveStructure):
    """when asTemplate is true the loading stops before the
    first item with duration: Chords, Rests, Container"""
    if "_uniqueContainerName" in saveStructure[1]: new._uniqueContainerName = saveStructure[1]["_uniqueContainerName"]
    if "cursorWalk" in saveStructure[1]: new.cursorWalk = saveStructure[1]["cursorWalk"]
    if "unfold" in saveStructure[1]: new.unfold = saveStructure[1]["unfold"]
    if "group" in saveStructure[1]: new.group = saveStructure[1]["group"]
    if "repeatPercent" in saveStructure[1]: new.repeatPercent = saveStructure[1]["repeatPercent"]
    if "smfTransposition" in saveStructure[1]: new.smfTransposition = saveStructure[1]["smfTransposition"]
    if "jackTransposition" in saveStructure[1]: new.jackTransposition = saveStructure[1]["jackTransposition"]
    if "transposition" in saveStructure[1]: new.transposition = saveStructure[1]["transposition"]

    containerList = [] #This makes sure that all previous auto-created containers from new classes get deleted.
    stopForTemplate = [items.RepeatOpen, items.RepeatClose, items.RepeatCloseOpen, items.Segno, items.Fine, items.SlurOff, items.SlurOn, items.MultiMeasureRest, items.Placeholder, items.Upbeat, items.Start, items.End, items.Appending] #We cant use a set because we have unhashable items.
    #goTemplate = [items.Clef, items.SpecialBarline, items.TimeSignature, items.TempoSignature, items.Upbeat, items.Markup, items.KeySignature, items.TempoModification, items.DynamicSignature, items.PerformanceSignature, items.InstrumentChange, items.ChannelChangeRelative, items.ChannelChangeAbsolute]
    global asTemplate
    currentLoadSession.currentParentContainer = new
    for item in saveStructure[1]["container"]:
        i = convert(item)
        typ = type(i)
        if asTemplate and (typ in stopForTemplate or i.duration):
            #Since we stop before the end of a Track we need a custom appending
            app = items.Appending(currentLoadSession.currentWorkspace)
            containerList.append(app)
            currentLoadSession.nonTrackContainers = {} #Delete the old containers.
            break
        containerList.append(i) #recursive call
    new.container = containerList

def _return(new):
    """Technically there is a non-zero change that a uniqueName
    was given twice by the uuid module. It is small but above
    zero. It goes even more towards zero because we test for the
    unique name, or add it to the dict, only after testing
    for >1 instance counts. And linked items are note the
    regular case. So we take that Risk"""
    if new.instanceCount > 1: #We catched a linked item here.
        if new.uniqueName in currentLoadSession.alreadyLoadedHighInstanceCountItems: #Is this the first occurence of this item?
            return currentLoadSession.alreadyLoadedHighInstanceCountItems[new.uniqueName] #get the exact instance that was saved in the dict
        else:
            currentLoadSession.alreadyLoadedHighInstanceCountItems[new.uniqueName] = new #use the new item because it is the first occurence, but save it in a dict for later.
            return new
    else:
        return new

def handleDefault(*args):
    #warnings.warning("Warning: Saved structure not recognized: ", saveStructure[0])
    print ("Warning: Saved structure not recognized")
    #go on nevertheless.

loadingDict =  {
            "Note": Note,
            "EmptyNote" : EmptyNote ,
            "Item" : Item ,
            "WaitForChord" : WaitForChord ,
            "Chord" : Chord ,
            "Appending" : Appending ,
            "Start" : Start ,
            "End" : End ,
            "Rest" : Rest ,
            "MultiMeasureRest" : MultiMeasureRest ,
            "Placeholder" : Placeholder ,
            "Upbeat" : Upbeat ,
            "Markup" : Markup ,
            "SpecialBarline" : SpecialBarline ,
            "RepeatOpen" : RepeatOpen ,
            "RepeatClose" : RepeatClose ,
            "RepeatCloseOpen" : RepeatCloseOpen ,
            "GotoSegno" : GotoSegno ,
            "GotoCapo" : GotoCapo ,
            "GotoCoda" : GotoCoda ,
            "Segno" : Segno ,
            "Coda" : Coda ,
            "Fine" : Fine ,
            "AlternateEnd" : AlternateEnd ,
            "AlternateEndClose" : AlternateEndClose ,
            "PedalSustainChange" : PedalSustainChange ,
            "Clef" : Clef ,
            "TimeSignature" : TimeSignature ,
            "KeySignature" : KeySignature ,
            "TempoSignature" : TempoSignature ,
            "TempoModification" : TempoModification ,
            "DynamicSignature" : DynamicSignature ,
            "SubitoDynamicSignature" : SubitoDynamicSignature ,
            "PerformanceSignature" : PerformanceSignature ,
            "ChannelChangeRelative" : ChannelChangeRelative ,
            "ChannelChangeAbsolute" : ChannelChangeAbsolute ,
            "ProgramChangeRelative" : ProgramChangeRelative ,
            "ProgramChangeAbsolute" : ProgramChangeAbsolute ,
            "InstrumentChange" : InstrumentChange ,
            "SlurOn" : SlurOn ,
            "SlurOff" : SlurOff ,
            "PhrasingSlurOn" : PhrasingSlurOn ,
            "PhrasingSlurOff" : PhrasingSlurOff ,
            "Container" : Container ,
            "SubstitutionContainer" : lambda savestruct: Container(savestruct, substitution=True),
            "Track" : Track ,
            "Score" : Score ,
            "Movement" : Score , #TODO: remove once old files have been converted
            }

def convert(saveStructure):
    """Returns a workspace.
    Recursive function
    Each level, down to Item, is a tuplet which has a key in [0].
    Check for the key and build a new structure, new instances."""

    # the string in saveStructure[0] gets a function from the dict which is then executed and returns an object which we return outside of convert.
    return loadingDict.get(saveStructure[0], handleDefault)(saveStructure)

def load(filepath, force = False, loadAsTemplate = False, newSession = True):
    """Load a single file into a new workspace.
    asTemplate loads each track until an item with duration."""

    global asTemplate
    asTemplate = loadAsTemplate

    f = open(filepath, 'r', encoding="utf-8")

    try:
        saveStructure = json.loads(f.read())
        f.close()
    except UnicodeDecodeError: #not utf8
        warnings.warn("This is not a Laborejo score (.lbjs) save file. It is not even utf8")
        f.close()
        return False
    except ValueError: #text, but not json
        warnings.warn("This is not a Laborejo score (.lbjs) save file. No JSON data found.")
        f.close()
        return False

    if force or ("file" in saveStructure and saveStructure["file"].lower() == "laborejo"): #force is a load parameter.
        newWorkspace = core.Workspace()
        global currentLoadSession
        if newSession:
            currentLoadSession = LoadSession(newWorkspace)
        #else:
        #    currentLoadSession.currentWorkspace = newWorkspace
        currentLoadSession.filepath = filepath
        newWorkspace.lilypondDefinitions = convertLilypondDefinitions(saveStructure["lilypondDefinitions"])
        if "score" in saveStructure:
            newWorkspace.score = convert(saveStructure["score"])
        else: #TODO: old format. convert files, then remove this code.
            print ("[" + currentLoadSession.filepath + "]Old save format detected. Save before editing the file to convert to new format")
            newWorkspace.score = convert(saveStructure["movement"])

        if "header" in saveStructure: #The header data changed from workspace to score between 0.7 and 0.8
            newWorkspace.score.header = convertHeader(saveStructure["header"])
            print ("[" + currentLoadSession.filepath + "]Old save format detected. Save before editing the file to convert to new format")

        if "lytemplate" in saveStructure: newWorkspace.lytemplate = saveStructure["lytemplate"]
        if "ardourtemplate" in saveStructure: newWorkspace.ardourtemplate = saveStructure["ardourtemplate"]
        if "uniqueContainerNames" in saveStructure: newWorkspace.uniqueContainerNames = saveStructure["uniqueContainerNames"]
        if "lyFontSize" in saveStructure: newWorkspace.lyFontSize = saveStructure["lyFontSize"]
        return newWorkspace
    else:
        warnings.warn("This is not a Laborejo score (.lbjs) save file (eventhough it is JSON). Use load(filepath, force = True) to load anyway.")
        return False

def loadMergeParts(listOfFilepaths):
    """Load several lbjs files and merge matching uniqueTrackNames.
    Returns one normal workspace with a single score which has no
    filepath.

    Before reading the following keep in mind that this function is
    intended to split a huge piece into several files and later
    seamlessly add them togther again. This means the user has to take
    care that the unique track names match each other.

    It is intended for lbj collections and not as a way to stitch
    files permanently together, eventhough this is possible.

    First load all paths as real files but don't return them all.
    Instead merge the tracks after the following scheme:

    Tracks are added in the order they come in, when they are first
    encountered. When encountering a track with the same unique name
    again it is added at the end of the already existing track.

    The beginning area, everything up to the first duration item,
    of a track (except the first) gets cleaned of signatures if they
    are the same as the last prevailing of the previous track.

    Also
    Add non-track containers and score(lbjs) substitutions.
    Create cursors for all new tracks.
    """

    if len(listOfFilepaths) <= 1:
        raise ValueError("loadMergeParts needs at least two files. But it was only", listOfFilepaths)

    typesOfPrevailingSignature = {
        items.TimeSignature: "prevailingTimeSignature",
        items.KeySignature: "prevailingKeySignature",
        items.TempoSignature: "prevailingTempoSignature",
        items.Clef: "prevailingClef",
        }

    def filterTrackBeginning(currentCursor, lastTrackCursor):
        currentCursor.go_to_head()
        currentCursor.resetToDefault()

        lastTrackCursor.go_to_head()
        lastTrackCursor.resetToDefault()
        while lastTrackCursor.right(): #go to tail
            pass

        lastCursorState = lastTrackCursor.getState() #a dict

        for item in currentCursor.iterate():
            if item.duration:
                break
            typ = type(item)
            if typ in typesOfPrevailingSignature and item.isTheSameAs(lastCursorState[typesOfPrevailingSignature[typ]]):
                #We can't delete items while iterating over them but we can switch their lilypond and midi off. This even keeps pre and post directives and midi instructionm
                item.lilypond = " "
                item.playback = None #supresses any midi output

    allTracks = {} #key is the unique track name, value a list which gets expanded.
    lastCursors = {} #key is the unique track name, value the cursor.
    realAppendings = {} #key is the unique track name, value the appending of the first tracks appending which has the correct parent instances.
    lyrics = {} #key is the unique track name, value is a list of strings, each one is a verse.
    allWorkspaces = []


    #First load the first file. This needs to be done separately because we need to create a new session
    first = load(listOfFilepaths[0])
    allWorkspaces.append(first)

    #Then load the other workspaces and store their tracks
    for filepath in listOfFilepaths[1:]:
        wsp = load(filepath, newSession = False)
        allWorkspaces.append(wsp)

    #Get all names in all listOfFilepaths
    allAllTrackNames =  set()
    [allAllTrackNames.update(wsp.score.allTrackNames()) for wsp in allWorkspaces]

    #Add missing tracks to each file.
    for wsp in allWorkspaces:
        myNames = wsp.score.allTrackNames()
        for name in allAllTrackNames:
            if not name in myNames: #if the track does not exist, add it.
                wsp.score.addTrack(name)


    for wsp in allWorkspaces:
        wsp.score.makeTracksEqualLengthWithPlaceholders()

    ## Now all scores/workspaces have the same amount of tracks with the same unique names, even if they are empty. All tracks have the same lenght, per score.
    ## We now combine these nice uniform set of scores.

    for cursor in first.score.cursors:
        track = cursor.nested_list
        allTracks[track.uniqueContainerName] = track
        lastCursors[cursor.nested_list.uniqueContainerName] = cursor
        realAppendings[track.uniqueContainerName] = track.container.pop(-1) #remove appending and store it for later.
        lyrics[track.uniqueContainerName] = track.lyrics.split("\n\n")

    for wsp in allWorkspaces[1:]:
        first.score.subtext += "\n" + wsp.score.subtext
        first.score.substitutions = dict(list(wsp.score.substitutions.items()) + list(first.score.substitutions.items()))
        for cursor in wsp.score.cursors:
            track = cursor.nested_list
            name = cursor.nested_list.uniqueContainerName
            filterTrackBeginning(cursor, lastCursors[name]) #in place filtering, see docstring
            allTracks[name].container += track.container[:-1] #without appending
            lastCursors[name] = cursor
            for index, verse in enumerate(track.lyrics.split("\n\n")):
                lyrics[name][index] += " " + verse
            allTracks[name].endingBar = track.endingBar #the last one stays in.

        #Finally add the real appendings, joined lyrics etc.
        if len(filepath) > 1:
            for name, track in allTracks.items():
                track.container.append(realAppendings[name])
                track.lyrics = "\n\n".join(lyrics[name])
                #eleminate spaces, newlines, tabs only.
                if track.lyrics.isspace():
                    track.lyrics = ""

    return first
