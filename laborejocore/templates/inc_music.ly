%$$INCLUDE$$
%Music definitions. Containers, Staff, Parts.
%$$CONTAINER$$

%$$VOICES$$

\score {
%$$SCORETRANSPOSE$$
 <<
\accidentalStyle #'Score "neo-modern-voice"
%How the definitions are arranged. Only staffgroups (staff prefix) and staffs, merged as voices.
%$$STRUCTURE$$
>>

    \header{
        %$$HEADER$$
    }
}

\markuplist{
  \column {
        %$$SUBTEXT$$
  }
}

