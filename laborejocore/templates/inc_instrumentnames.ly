%$$INCLUDE$$
%Better instrument names by Nicolas Sceaux
#(define-public (instrument-name::print grob)
   (let* ((left-bound (ly:spanner-bound grob LEFT))
          (left-mom (ly:grob-property left-bound 'when))
          (name (if (moment<=? left-mom ZERO-MOMENT)
                    (ly:grob-property grob 'long-text)
                    (ly:grob-property grob 'text))))
     (if (and (markup? name)
              (!= (ly:item-break-dir left-bound) CENTER))
         (let* ((layout (ly:grob-layout grob))
                (defs (ly:output-def-lookup layout 'text-font-defaults))
                (props (ly:grob-alist-chain grob defs))
                (indent (ly:output-def-lookup
                         layout
                         (if (moment<=? left-mom ZERO-MOMENT)
                             'indent
                             'short-indent)
                         0.0)))
           (interpret-markup
            layout
            (cons `((line-width . ,indent)) props)
            name))
         (ly:grob-suicide! grob))))

\layout{
    \context { \Staff
        \override InstrumentName #'self-alignment-X = #LEFT
        \override InstrumentName #'stencil = #instrument-name::print
  }}
