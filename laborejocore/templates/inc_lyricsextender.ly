%$$INCLUDE$$
%Custom lyric extenders, first with extra padding.
%__, and __! in Laborejo get substituted to \extendComma and \extendExclaim

%LSR contributed by Neil Puttock
#(define (extend text . padding)
   (let ((extender (make-music 'ExtenderEvent))
     ;; optional padding
     (padding (if (pair? padding)
              (car padding)
              0)))

     (set! (ly:music-property extender 'tweaks)
       (acons 'stencil (lambda (grob)
                 (let* ((orig (ly:grob-original grob))
                    (siblings (ly:spanner-broken-into orig)))

                   (if (or (null? siblings)
                       (and (>= (length siblings) 2)
                        (eq? (car (last-pair siblings))
                         grob)))
                   (ly:stencil-combine-at-edge
                    (ly:lyric-extender::print grob)
                    X RIGHT
                    (grob-interpret-markup grob text)
                    padding)
                   (ly:lyric-extender::print grob))))
          (ly:music-property extender 'tweaks)))
     extender))

extendComma = #(extend "," 0.2)
extendExclaim = #(extend "!")
