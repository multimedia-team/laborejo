%$$INCLUDE$$
correctFraction = #(lambda (grob)
  (let ((ev (event-cause grob)))
    (if (and (= (ly:event-property ev 'numerator) 2) (= (ly:event-property ev 'denominator) 3))

    (format #f "~a" 3)

    (format #f "~a/~a"
            (ly:event-property ev 'numerator)
            (ly:event-property ev 'denominator))
    )))

\layout{
    \context { \Score
    \override TupletNumber #'text = \correctFraction %traditional but wrong: #tuplet-number::calc-fraction-text
  }}
